const Timestamp = new Date().getTime();
module.exports = {
    publicPath: './',
    productionSourceMap: false,
    outputDir:"../resources/app",
    devServer: {
        host: '0.0.0.0',
        port: 8080,
        compress: true,
        disableHostCheck: true,
        proxy: "http://127.0.0.1:8080/TestMob",
    },
    configureWebpack: config => {
        // 打包文件名添加时间戳
        Object.assign(config, {
            output: {
                ...config.output,
               filename: `js/[name]-${Timestamp}.js`,
               chunkFilename: `js/[name]-${Timestamp}.js`
            },
        });
    }
}