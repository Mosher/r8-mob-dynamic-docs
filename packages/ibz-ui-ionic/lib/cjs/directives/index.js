"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "Badge", {
  enumerable: true,
  get: function () {
    return _badge.Badge;
  }
});
Object.defineProperty(exports, "Format", {
  enumerable: true,
  get: function () {
    return _format.Format;
  }
});

var _badge = require("./badge/badge");

var _format = require("./format/format");