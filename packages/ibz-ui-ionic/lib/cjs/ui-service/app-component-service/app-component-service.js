"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppComponentService = void 0;

var _layout = require("../../components/layout");

var _view = require("../../components/view");

var _widget = require("../../components/widget");

var _editor = require("../../components/editor");

//  视图布局面板组件
//  视图组件
//  部件组件
//  编辑器组件

/**
 * 应用组件服务
 *
 * @memberof AppComponentService
 */
class AppComponentService {
  /**
   * Creates an instance of AppComponentService.
   * @memberof AppComponentService
   */
  constructor() {
    /**
     * @description 视图类型组件Map
     * @protected
     * @type {Map<string, any>}
     * @memberof AppComponentService
     */
    this.viewTypeMap = new Map();
    /**
     * @description 视图布局组件Map
     * @protected
     * @type {Map<string, any>}
     * @memberof AppComponentService
     */

    this.layoutMap = new Map();
    /**
     * @description 部件组件Map
     * @protected
     * @type {Map<string, any>}
     * @memberof AppComponentService
     */

    this.controlMap = new Map();
    /**
     * 控件组件Map
     *
     * @memberof AppComponentService
     */

    this.editorMap = new Map();
    this.registerAppComponents();
  }
  /**
   * 获取 AppComponentService 单例对象
   *
   * @static
   * @returns {AppComponentService}
   * @memberof AppComponentService
   */


  static getInstance() {
    if (!AppComponentService.AppComponentService) {
      AppComponentService.AppComponentService = new AppComponentService();
    }

    return this.AppComponentService;
  }
  /**
   * @description 注册应用组件
   * @memberof AppComponentService
   */


  registerAppComponents() {
    this.registerViewTypeComponents();
    this.registerLayoutComponent();
    this.registerControlComponents();
    this.registerEditorComponents();
  }
  /**
   * @description 注册布局组件
   * @memberof AppComponentService
   */


  registerLayoutComponent() {
    this.layoutMap.set('APPINDEXVIEW_DEFAULT', _layout.AppDefaultIndexViewLayoutComponent);
    this.layoutMap.set('APPPORTALVIEW_DEFAULT', _layout.AppDefaultPortalViewLayoutComponent);
    this.layoutMap.set('DEMOBEDITVIEW_DEFAULT', _layout.AppDefaultMobEditViewLayoutComponent);
    this.layoutMap.set('DEMOBMDVIEW_DEFAULT', _layout.AppDefaultMobMDViewLayoutComponent);
    this.layoutMap.set('DEMOBPANELVIEW_DEFAULT', _layout.AppDefaultMobPanelViewLayoutComponent);
    this.layoutMap.set('DEMOBCUSTOMVIEW_DEFAULT', _layout.AppDefaultMobCustomViewLayoutComponent);
    this.layoutMap.set('DEMOBPICKUPVIEW_DEFAULT', _layout.AppDefaultMobPickupViewLayoutComponent);
    this.layoutMap.set('DEMOBMPICKUPVIEW_DEFAULT', _layout.AppDefaultMobMPickupViewLayoutComponent);
    this.layoutMap.set('DEMOBPICKUPMDVIEW_DEFAULT', _layout.AppDefaultMobPickUpMDViewLayoutComponent);
    this.layoutMap.set('DEMOBCALENDARVIEW_DEFAULT', _layout.AppDefaultMobCalendarViewLayoutComponent);
    this.layoutMap.set('DEMOBTREEVIEW_DEFAULT', _layout.AppDefaultMobTreeViewLayoutComponent);
    this.layoutMap.set('DEMOBMAPVIEW_DEFAULT', _layout.AppDefaultMobMapViewLayoutComponent);
    this.layoutMap.set('DEMOBCHARTVIEW_DEFAULT', _layout.AppDefaultMobChartViewLayoutComponent);
    this.layoutMap.set('DEMOBHTMLVIEW_DEFAULT', _layout.AppDefaultMobHtmlViewLayoutComponent);
    this.layoutMap.set('DEMOBPICKUPTREEVIEW_DEFAULT', _layout.AppDefaultMobPickupTreeViewLayoutComponent);
    this.layoutMap.set('DEMOBTABEXPVIEW_DEFAULT', _layout.AppDefaultMobTabExpViewLayoutComponent);
    this.layoutMap.set('DEMOBMEDITVIEW9_DEFAULT', _layout.AppDefaultMobMEditViewLayoutComponent);
    this.layoutMap.set('DEMOBPORTALVIEW_DEFAULT', _layout.AppDefaultMobDEDashboardViewLayoutComponent);
    this.layoutMap.set('DEMOBWFDYNAEDITVIEW_DEFAULT', _layout.AppDefaultMobWFDynaEditViewLayoutComponent);
    this.layoutMap.set('DEMOBWFDYNAACTIONVIEW_DEFAULT', _layout.AppDefaultMobWFDynaActionViewLayoutComponent);
    this.layoutMap.set('DEMOBLISTEXPVIEW_DEFAULT', _layout.AppDefaultMobListExpViewLayoutComponent);
    this.layoutMap.set('DEMOBOPTVIEW_DEFAULT', _layout.AppDefaultMobOptViewLayoutComponent);
    this.layoutMap.set('DEMOBWIZARDVIEW_DEFAULT', _layout.AppDefaultMobWizardViewLayoutComponent);
    this.layoutMap.set('DEMOBCHARTEXPVIEW_DEFAULT', _layout.AppDefaultMobChartExpViewLayoutComponent);
    this.layoutMap.set('DEMOBTREEEXPVIEW_DEFAULT', _layout.AppDefaultMobTreeExpViewLayoutComponent);
    this.layoutMap.set('DEMOBMAPEXPVIEW_DEFAULT', _layout.AppDefaultMobMapExpViewLayoutComponent);
  }
  /**
   * @description 获取布局组件
   * @param {string} viewType 布局类型
   * @param {string} viewStyle 布局样式
   * @param {string} [pluginCode] 插件代码
   * @return {string}
   * @memberof AppComponentService
   */


  getLayoutComponent(viewType, viewStyle, pluginCode) {
    return this.layoutMap.get(`${viewType}_${viewStyle}`);
  }
  /**
   * @description 注册视图类型组件
   * @return {*}
   * @memberof AppComponentService
   */


  registerViewTypeComponents() {
    this.viewTypeMap.set('APPINDEXVIEW_DEFAULT', _view.AppIndexViewComponent);
    this.viewTypeMap.set('APPPORTALVIEW_DEFAULT', _view.AppPortalViewComponent);
    this.viewTypeMap.set('DEMOBEDITVIEW_DEFAULT', _view.AppMobEditViewComponent);
    this.viewTypeMap.set('DEMOBMDVIEW_DEFAULT', _view.AppMobMDViewComponent);
    this.viewTypeMap.set('DEMOBPANELVIEW_DEFAULT', _view.AppMobPanelViewComponent);
    this.viewTypeMap.set('DEMOBCUSTOMVIEW_DEFAULT', _view.AppMobCustomViewComponent);
    this.viewTypeMap.set('DEMOBPICKUPVIEW_DEFAULT', _view.AppMobPickUpViewComponent);
    this.viewTypeMap.set('DEMOBMPICKUPVIEW_DEFAULT', _view.AppMobMPickUpViewComponent);
    this.viewTypeMap.set('DEMOBPICKUPMDVIEW_DEFAULT', _view.AppMobPickUpMDViewComponent);
    this.viewTypeMap.set('DEMOBCALENDARVIEW_DEFAULT', _view.AppMobCalendarViewComponent);
    this.viewTypeMap.set('DEMOBTREEVIEW_DEFAULT', _view.AppMobTreeViewComponent);
    this.viewTypeMap.set('DEMOBMAPVIEW_DEFAULT', _view.AppMobMapViewComponent);
    this.viewTypeMap.set('DEMOBCHARTVIEW_DEFAULT', _view.AppMobChartViewComponent);
    this.viewTypeMap.set('DEMOBHTMLVIEW_DEFAULT', _view.AppMobHtmlViewComponent);
    this.viewTypeMap.set('DEMOBPICKUPTREEVIEW_DEFAULT', _view.AppMobPickupTreeViewComponent);
    this.viewTypeMap.set('DEMOBTABEXPVIEW_DEFAULT', _view.AppMobTabExpViewComponent);
    this.viewTypeMap.set('DEMOBMEDITVIEW9_DEFAULT', _view.AppMobMEditViewComponent);
    this.viewTypeMap.set('DEMOBPORTALVIEW_DEFAULT', _view.AppMobDEDashboardViewComponent);
    this.viewTypeMap.set('DEMOBWFDYNAEDITVIEW_DEFAULT', _view.AppMobWFDynaEditViewComponent);
    this.viewTypeMap.set('DEMOBWFDYNAACTIONVIEW_DEFAULT', _view.AppMobWFDynaActionViewComponent);
    this.viewTypeMap.set('DEMOBLISTEXPVIEW_DEFAULT', _view.AppMobListExpViewComponent);
    this.viewTypeMap.set('DEMOBOPTVIEW_DEFAULT', _view.AppMobOptViewComponent);
    this.viewTypeMap.set('DEMOBWIZARDVIEW_DEFAULT', _view.AppMobWizardViewComponent);
    this.viewTypeMap.set('DEMOBCHARTEXPVIEW_DEFAULT', _view.AppMobChartExpViewComponent);
    this.viewTypeMap.set('DEMOBTREEEXPVIEW_DEFAULT', _view.AppMobTreeExpViewComponent);
    this.viewTypeMap.set('DEMOBMAPEXPVIEW_DEFAULT', _view.AppMobMapExpViewComponent);
  }
  /**
   * @description 获取视图类型组件
   * @param {string} viewType 视图类型
   * @param {string} viewStyle 视图样式
   * @param {string} [pluginCode] 插件代码
   * @return {string}
   * @memberof AppComponentService
   */


  getViewTypeComponent(viewType, viewStyle = 'DEFAULT', pluginCode) {
    let component = _view.AppMobNotSupportedViewComponent;

    if (pluginCode) {
      component = this.viewTypeMap.get(`${viewType}_${pluginCode}`);
    } else {
      component = this.viewTypeMap.get(`${viewType}_${viewStyle}`);
    }

    return component || _view.AppMobNotSupportedViewComponent;
  }
  /**
   * @description 注册部件组件
   * @return {*}
   * @memberof AppComponentService
   */


  registerControlComponents() {
    this.controlMap.set('APPMENU_DEFAULT', _widget.AppMobMenuComponent);
    this.controlMap.set('APPMENU_ICONVIEW', _widget.AppMobMenuComponent);
    this.controlMap.set('APPMENU_LISTVIEW', _widget.AppMobMenuComponent);
    this.controlMap.set('TOOLBAR_DEFAULT', _widget.AppMobToolbarComponent);
    this.controlMap.set('FORM_DEFAULT', _widget.AppMobFormComponent);
    this.controlMap.set('SEARCHFORM_DEFAULT', _widget.AppMobSearchFormComponent);
    this.controlMap.set('MOBMDCTRL_LISTVIEW', _widget.AppMobMDCtrlComponent);
    this.controlMap.set('MOBMDCTRL_ICONVIEW', _widget.AppMobMDCtrlComponent);
    this.controlMap.set('LIST_DEFAULT', _widget.AppMobMDCtrlComponent);
    this.controlMap.set('PANEL_DEFAULT', _widget.AppMobPanelComponent);
    this.controlMap.set('CALENDAR_DEFAULT', _widget.AppMobCalendarComponent);
    this.controlMap.set('PICKUPVIEWPANEL_DEFAULT', _widget.AppMobPickUpViewPanelComponent);
    this.controlMap.set('TREEVIEW_DEFAULT', _widget.AppMobTreeComponent);
    this.controlMap.set('MAP_DEFAULT', _widget.AppMobMapComponent);
    this.controlMap.set('CHART_DEFAULT', _widget.AppMobChartComponent);
    this.controlMap.set('CONTEXTMENU_DEFAULT', _widget.AppMobContextMenuComponent);
    this.controlMap.set('TABEXPPANEL_DEFAULT', _widget.AppMobTabExpPanelComponent);
    this.controlMap.set('TABVIEWPANEL_DEFAULT', _widget.AppMobTabViewPanelComponent);
    this.controlMap.set('MULTIEDITVIEWPANEL_DEFAULT', _widget.AppMobMEditViewPanelComponent);
    this.controlMap.set('DASHBOARD_DEFAULT', _widget.AppMobDashboardComponent);
    this.controlMap.set('PORTLET_DEFAULT', _widget.AppMobPortletComponent);
    this.controlMap.set('LISTEXPBAR_DEFAULT', _widget.AppMobListExpBarComponent);
    this.controlMap.set('WIZARDPANEL_DEFAULT', _widget.AppMobWizardPanelComponent);
    this.controlMap.set('WIZARDPANEL_STATE', _widget.AppMobStateWizardPanelComponent);
    this.controlMap.set('CHARTEXPBAR_DEFAULT', _widget.AppMobChartExpBarComponent);
    this.controlMap.set('TREEEXPBAR_DEFAULT', _widget.AppMobTreeExpBarComponent);
    this.controlMap.set('MAPEXPBAR_DEFAULT', _widget.AppMobMapExpBarComponent);
  }
  /**
   * @description 获取部件组件
   * @param {string} ctrlType 部件类型
   * @param {string} ctrlStyle 部件样式
   * @param {string} [pluginCode] 插件标识
   * @return {string}
   * @memberof AppComponentService
   */


  getControlComponent(ctrlType, ctrlStyle = 'DEFAULT', pluginCode) {
    let component = _widget.AppMobNotSupportedControlComponent;

    if (pluginCode) {
      component = this.controlMap.get(`${ctrlType}_${pluginCode}`);
    } else {
      component = this.controlMap.get(`${ctrlType}_${ctrlStyle}`);
    }

    return component || _widget.AppMobNotSupportedControlComponent;
  }
  /**
   * @description 注册编辑器组件
   * @protected
   * @memberof AppComponentService
   */


  registerEditorComponents() {
    this.editorMap.set('MOBTEXT_DEFAULT', _editor.AppTextboxEditorComponent);
    this.editorMap.set('MOBNUMBER_DEFAULT', _editor.AppTextboxEditorComponent);
    this.editorMap.set('MOBTEXTAREA_DEFAULT', _editor.AppTextboxEditorComponent);
    this.editorMap.set('MOBPASSWORD_DEFAULT', _editor.AppTextboxEditorComponent);
    this.editorMap.set('MOBSWITCH_DEFAULT', _editor.AppSwitchEditorComponent);
    this.editorMap.set('MOBSLIDER_DEFAULT', _editor.AppSliderEditorComponent);
    this.editorMap.set('MOBRADIOLIST_DEFAULT', _editor.AppCheckBoxEditorComponent);
    this.editorMap.set('MOBDROPDOWNLIST_DEFAULT', _editor.AppDropdownListEditorComponent);
    this.editorMap.set('MOBCHECKLIST_DEFAULT', _editor.AppDropdownListEditorComponent);
    this.editorMap.set('SPAN_DEFAULT', _editor.AppSpanEditorComponent);
    this.editorMap.set('MOBPICKER_DEFAULT', _editor.AppDataPickerEditorComponent);
    this.editorMap.set('MOBMPICKER_DEFAULT', _editor.AppDataPickerEditorComponent);
    this.editorMap.set('MOBPICKER_DROPDOWNVIEW_DEFAULT', _editor.AppDataPickerEditorComponent);
    this.editorMap.set('MOBDATE_DEFAULT', _editor.AppDatePickerEditorComponent);
    this.editorMap.set('MOBSINGLEFILEUPLOAD_DEFAULT', _editor.AppUploadEditorComponent);
    this.editorMap.set('MOBMULTIFILEUPLOAD_DEFAULT', _editor.AppUploadEditorComponent);
    this.editorMap.set('MOBPICTURE_DEFAULT', _editor.AppUploadEditorComponent);
    this.editorMap.set('MOBPICTURELIST_DEFAULT', _editor.AppUploadEditorComponent);
    this.editorMap.set('MOBRATING_DEFAULT', _editor.AppRatingEditorComponent);
    this.editorMap.set('MOBSTEPPER_DEFAULT', _editor.AppStepperEditorComponent);
    this.editorMap.set('MOBHTMLTEXT_DEFAULT', _editor.AppRichTextEditorComponent); // 精确分钟

    this.editorMap.set('MOBDATE_Auto1', _editor.AppDatePickerEditorComponent); // 只有小时分钟

    this.editorMap.set('MOBDATE_Auto3', _editor.AppDatePickerEditorComponent); // 精确小时

    this.editorMap.set('MOBDATE_Auto8', _editor.AppDatePickerEditorComponent); // 精确天

    this.editorMap.set('MOBDATE_Auto6', _editor.AppDatePickerEditorComponent); // 电子签名

    this.editorMap.set('MOBPICTURE_DZQM', _editor.AppUploadEditorComponent);
  }
  /**
   * @description 获取编辑器组件
   * @param {string} editorType 编辑器类型
   * @param {string} editorStyle 编辑器样式
   * @param {string} [pluginCode] 编辑器插件标识
   * @return {string}
   * @memberof AppComponentService
   */


  getEditorComponent(editorType, editorStyle = 'DEFAULT', pluginCode, infoMode = false) {
    let component = _editor.AppMobNotSupportedEditorComponent;

    if (pluginCode) {
      component = this.editorMap.get(`${pluginCode}`);
    } else {
      if (infoMode) {
        component = this.computeInfoModeEditor(editorType, editorStyle);
      } else {
        component = this.editorMap.get(`${editorType}_${editorStyle}`);
      }
    }

    return component || _editor.AppMobNotSupportedEditorComponent;
  }
  /**
   * @description 计算信息模式编辑器
   * @param {string} editorType 编辑器类型
   * @param {string} editorStyle 编辑器样式
   * @return {*}  {string}
   * @memberof AppComponentService
   */


  computeInfoModeEditor(editorType, editorStyle) {
    //  TODO目前返回span，等待后续编辑器补充
    let component = _editor.AppSpanEditorComponent;
    return component;
  }

}

exports.AppComponentService = AppComponentService;