"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppThirdPartyService = void 0;

var _dingTalkService = require("./ding-talk-service");

var _weChatService = require("./we-chat-service");

/**
 * 第三方服务
 *
 * @export
 * @class AppThirdPartyService
 */
class AppThirdPartyService {
  /**
   * Creates an instance of AppThirdPartyService.
   * @memberof AppThirdPartyService
   */
  constructor() {
    this.thirdPartyInit();
  }
  /**
   *  第三方初始化
   *
   * @memberof AppThirdPartyService
   */


  thirdPartyInit() {
    this.initAppPlatForm();
    this.initPlatformService();
  }
  /**
   * 获取实例
   *
   * @static
   * @return {*}  {AppThirdPartyService}
   * @memberof AppThirdPartyService
   */


  static getInstance() {
    if (!this.instance) {
      this.instance = new AppThirdPartyService();
    }

    return AppThirdPartyService.instance;
  }
  /**
   * 初始化搭载平台
   *
   * @memberof AppThirdPartyService
   */


  initPlatformService() {
    if (Object.is(this.platform, 'dd')) {
      this.platformService = _dingTalkService.DingTalkService.getInstance();
    } else if (Object.is(this.platform, 'wx')) {
      this.platformService = _weChatService.WeChatService.getInstance();
    }
  }
  /**
   * 初始化搭载平台
   *
   * @memberof AppThirdPartyService
   */


  initAppPlatForm() {
    let platform = 'web';
    const info = window.navigator.userAgent.toUpperCase(); // 钉钉

    if (info.indexOf('DINGTALK') !== -1) {
      platform = 'dd';
    } // 桌面端
    else if (info.indexOf('ELECTRON') !== -1) {
      platform = 'desktop';
    } // 微信 (小程序或者公众号)
    else if (info.indexOf('MICROMESSENGER') !== -1) {
      platform = 'wx';
    } // 安卓应用
    else if (info.indexOf('ANDROID') !== -1) {
      platform = 'android';
    } // 苹果应用 (未测试)
    // else if (info.indexOf('IPHONE') !== -1) {
    //     platform = 'ios';
    // }


    this.platform = platform;
  }
  /**
   * 获取APP搭载平台
   *
   * @memberof AppThirdPartyService
   */


  async getAppPlatform() {
    let platform;

    if (this.platform === 'wx') {
      const isMini = await this.platformService.isMini();
      platform = isMini ? 'wx-mini' : 'wx-pn';
    } else {
      platform = this.platform;
    }

    return platform;
  }
  /**
   * 获取当前搭载平台服务类
   *
   * @return {*}
   * @memberof AppThirdPartyService
   */


  getPlatformService() {
    return this.platformService;
  }
  /**
   * 清楚登录用户信息
   *
   * @memberof AppThirdPartyService
   */


  clearUserInfo() {
    return this.platformService.clearUserInfo();
  }
  /**
   * 获取用户信息
   *
   * @returns {*}
   * @memberof AppThirdPartyService
   */


  async getUserInfo() {
    return this.platformService.getUserInfo();
  }
  /**
   * 关闭应用
   *
   * @memberof AppThirdPartyService
   */


  close() {
    this.platformService.close();
  }
  /**
   * 第三方事件
   *
   * @param {string} tag
   * @param {*} [arg={}]
   * @return {*}  {Promise<any>}
   * @memberof AppThirdPartyService
   */


  async thirdPartyEvent(tag, arg = {}) {
    return await this.platformService.event(tag, arg);
  }

}

exports.AppThirdPartyService = AppThirdPartyService;