"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppNoticeService = void 0;

var _core = require("@ionic/core");

/**
 * 消息提示
 *
 * @export
 * @class AppNoticeService
 */
class AppNoticeService {
  /**
   * Creates an instance of AppNoticeService.
   * @memberof AppNoticeService
   */
  constructor() {
    if (AppNoticeService.instance) {
      return AppNoticeService.instance;
    }
  }
  /**
   * 消息提示
   *
   * @param {string} message
   * @param {number} [time]
   * @memberof AppNoticeService
   */


  info(message, time) {
    const type = 'secondary';
    this.createToast(type, message, time);
  }
  /**
   * 成功提示
   *
   * @param {string} message
   * @param {number} [time]
   * @memberof AppNoticeService
   */


  success(message, time) {
    const type = 'success';
    this.createToast(type, message, time);
  }
  /**
   * 警告提示
   *
   * @param {string} message
   * @param {number} [time]
   * @memberof AppNoticeService
   */


  warning(message, time) {
    const type = 'warning';
    this.createToast(type, message, time);
  }
  /**
   * 错误提示
   *
   * @param {string} message
   * @param {number} [time]
   * @memberof AppNoticeService
   */


  error(message, time) {
    const type = 'danger';
    this.createToast(type, message, time);
  }
  /**
   * 创建对象
   *
   * @private
   * @param {string} type
   * @param {string} message
   * @param {number} [time]
   * @memberof AppNoticeService
   */


  async createToast(type, message, time) {
    const toast = await _core.toastController.create({
      position: 'top',
      color: type ? type : 'primary',
      duration: time ? time : 2000,
      message: message
    });
    await toast.present();
  }
  /**
   * 获取实例
   *
   * @static
   * @returns {AppNoticeService}
   * @memberof AppNoticeService
   */


  static getInstance() {
    return this.instance;
  }

}
/**
 * 唯一实例
 *
 * @private
 * @static
 * @type {AppNoticeService}
 * @memberof AppNoticeService
 */


exports.AppNoticeService = AppNoticeService;
AppNoticeService.instance = new AppNoticeService();