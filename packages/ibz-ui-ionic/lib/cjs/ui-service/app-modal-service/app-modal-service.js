"use strict";

var _interopRequireDefault = require("D:/IbizWork/r8-mob-dynamic-docs/packages/ibz-ui-ionic/node_modules/@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppModalService = void 0;

var _vue = require("vue");

var _appModal = _interopRequireDefault(require("../../components/common/app-modal"));

var _core = require("@ionic/core");

var _ibzCore = require("ibz-core");

var _rxjs = require("rxjs");

var _vue2 = require("@ionic/vue");

var _register = require("../../register");

var _locale = require("../../locale");

/**
 * 模态框工具
 *
 * @export
 * @class AppModalService
 */
class AppModalService {
  /**
   * Creates an instance of AppModalService.
   *
   * @memberof AppModalService
   */
  constructor() {
    if (AppModalService.modal) {
      return AppModalService.modal;
    }
  }
  /**
   * 获取单例对象
   *
   * @static
   * @returns {AppModalService}
   * @memberof AppModalService
   */


  static getInstance() {
    if (!AppModalService.modal) {
      AppModalService.modal = new AppModalService();
    }

    return AppModalService.modal;
  }
  /**
   * 创建 Vue 实例对象
   *
   * @private
   * @param {{ viewComponent?: any,viewPath:string, viewModel: IParam, customClass?: string, customStyle?: IParam, }} view
   * @param {*} [navContext={}]
   * @param {*} [navParam={}]
   * @param {Array<any>} [navDatas=[]]
   * @param {*} viewCtx
   * @param {string} uuid
   * @return {*}  {Promise<any>}
   * @memberof AppModalService
   */


  async createVueExample(view, navContext = {}, navParam = {}, navDatas = [], otherParam, uuid, subject) {
    var _a, _b;

    const model = await this.createModal(null, uuid);
    const el = (_b = (_a = document.getElementById(`${uuid}`)) === null || _a === void 0 ? void 0 : _a.getElementsByClassName('ion-page')) === null || _b === void 0 ? void 0 : _b[0];
    const props = {
      view,
      navContext,
      navParam,
      navDatas,
      model,
      otherParam,
      subject,
      viewShowMode: 'MODEL'
    };
    const vm = (0, _vue.createApp)(_appModal.default, props).use(_register.ComponentRegister).use(App.getUserRegister()).use(_vue2.IonicVue).use(App.getI18n()); // 添加全局翻译api

    vm.config.globalProperties.$tl = function (key, value) {
      return (0, _locale.translate)(key, this, value);
    };

    vm.config.warnHandler = (msg, vm, trace) => {
      if (!msg.startsWith('Extraneous non-emits') && !msg.startsWith('Extraneous non-props') && !msg.startsWith('injection')) {
        console.warn(msg, trace);
      }
    };

    const subscribe = subject.subscribe(() => {
      vm.unmount();
      subscribe.unsubscribe();
    });

    if (el) {
      vm.mount(el);
    }
  }
  /**
   * 打开 ionic 模式模态框
   *
   * @private
   * @param {Element} ele
   * @returns {Promise<any>}
   * @memberof AppModalService
   */


  async createModal(ele, uuid) {
    const modal = await _core.modalController.create({
      component: ele,
      id: uuid
    });
    await modal.present();
    return modal;
  }
  /**
   * 打开模态视图
   *
   * @param {{ viewComponent?: any, viewPath: string, viewModel: IParam, customClass?: string, customStyle?: IParam }} view
   * @param {*} [navContext={}]
   * @param {*} [navParam={}]
   * @param {Array<any>} [navDatas=[]]
   * @param {*} [otherParam={}]
   * @return {*}  {Subject<any>}
   * @memberof AppModalService
   */


  openModal(view, navContext = {}, navParam = {}, navDatas = [], otherParam = {}) {
    const subject = new _rxjs.Subject();

    try {
      const _navContext = {};
      Object.assign(_navContext, navContext);

      if (!view.viewComponent) {
        this.fillView(view);
      }

      const uuid = _ibzCore.Util.createUUID();

      this.createVueExample(view, _navContext, navParam, navDatas, otherParam, uuid, subject);
      return subject;
    } catch (error) {
      _ibzCore.LogUtil.warn(error);

      return subject;
    }
  }
  /**
   *  初始化视图名称
   *
   * @memberof AppModalService
   */


  fillView(view) {
    var _a, _b;

    if (_ibzCore.Util.isEmpty(view.viewModel)) {
      return;
    } else {
      view.viewComponent = App.getComponentService().getViewTypeComponent(view.viewModel.viewType, view.viewModel.viewStyle, (_b = (_a = view.viewModel) === null || _a === void 0 ? void 0 : _a.getPSSysPFPlugin()) === null || _b === void 0 ? void 0 : _b.pluginCode);
      view.viewPath = view.viewModel.modelPath;
    }
  }

}
/**
 * 实例对象
 *
 * @private
 * @static
 * @memberof AppModalService
 */


exports.AppModalService = AppModalService;
AppModalService.modal = new AppModalService();