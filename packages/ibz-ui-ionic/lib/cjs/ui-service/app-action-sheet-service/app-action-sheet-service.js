"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppActionSheetService = void 0;

var _vue = require("@ionic/vue");

/**
 * 动作面板服务
 *
 * @class AppActionSheetService
 * @description 底部弹出的模态面板
 */
class AppActionSheetService {
  /**
   * @description 获取动态面板服务实例对象
   * @static
   * @return {*}  {IAppActionSheetService}
   * @memberof AppActionSheetService
   */
  static getInstance() {
    if (!this.instance) {
      this.instance = new AppActionSheetService();
    }

    return this.instance;
  }
  /**
   * @description 创建动态面板
   * @param {IAppActionSheetOptions} options 动态面板配置
   * @return {*}  {Promise<ICtrlActionResult>}
   * @memberof AppActionSheetService
   */


  async create(options) {
    try {
      const ionicASOptions = this.configConvert(options);
      const actionSheet = await _vue.actionSheetController.create(ionicASOptions);
      await actionSheet.present();
      return {
        ret: true,
        data: []
      };
    } catch (error) {
      return {
        ret: false,
        data: [error]
      };
    }
  }
  /**
   * @description 配置转换
   * @private
   * @param {IAppActionSheetOptions} options 动态面板配置
   * @return {*}  {IParam}
   * @memberof AppActionSheetService
   */


  configConvert(options) {
    const convertOptions = {
      id: options.id,
      header: options.header,
      subHeader: options.subHeader,
      cssClass: options.cssClass,
      buttons: [],
      backdropDismiss: options.backdropDismiss,
      translucent: options.translucent
    };
    options.buttons.forEach(button => {
      const ionicASButton = {
        text: button.text,
        role: button.role,
        icon: button.icon,
        cssClass: button.cssClass,
        handler: () => {
          button.handler();
        }
      };
      convertOptions.buttons.push(ionicASButton);
    });
    return convertOptions;
  }

}

exports.AppActionSheetService = AppActionSheetService;