"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ComponentRegister = void 0;

var _vue = require("@ionic/vue");

var _common = require("./components/common");

var _directives = require("./directives");

//  编辑器组件
// UI绘制组件
//指令引入
const ComponentRegister = {
  install(vue) {
    // 通用组件
    vue.component('ion-checkbox', _vue.IonCheckbox);
    vue.component('ion-popover', _vue.IonPopover);
    vue.component('ion-searchbar', _vue.IonSearchbar);
    vue.component('ion-app', _vue.IonApp);
    vue.component('ion-router-outlet', _vue.IonRouterOutlet);
    vue.component('ion-icon', _vue.IonIcon);
    vue.component('ion-label', _vue.IonLabel);
    vue.component('ion-item', _vue.IonItem);
    vue.component('ion-input', _vue.IonInput);
    vue.component('ion-page', _vue.IonPage);
    vue.component('ion-tab-bar', _vue.IonTabBar);
    vue.component('ion-tab-button', _vue.IonTabButton);
    vue.component('ion-tabs', _vue.IonTabs);
    vue.component('ion-badge', _vue.IonBadge);
    vue.component('ion-header', _vue.IonHeader);
    vue.component('ion-toolbar', _vue.IonToolbar);
    vue.component('ion-title', _vue.IonTitle);
    vue.component('ion-content', _vue.IonContent);
    vue.component('ion-row', _vue.IonRow);
    vue.component('ion-item-divider', _vue.IonItemDivider);
    vue.component('ion-segment', _vue.IonSegment);
    vue.component('ion-segment-button', _vue.IonSegmentButton);
    vue.component('ion-textarea', _vue.IonTextarea);
    vue.component('ion-radio', _vue.IonRadio);
    vue.component('ion-radio-group', _vue.IonRadioGroup);
    vue.component('ion-select', _vue.IonSelect);
    vue.component('ion-datetime', _vue.IonDatetime);
    vue.component('ion-toggle', _vue.IonToggle);
    vue.component('ion-range', _vue.IonRange);
    vue.component('ion-list', _vue.IonList);
    vue.component('ion-list-header', _vue.IonListHeader);
    vue.component('ion-item-group', _vue.IonItemGroup);
    vue.component('ion-button', _vue.IonButton);
    vue.component('ion-col', _vue.IonCol);
    vue.component('ion-backdrop', _vue.IonBackdrop);
    vue.component('ion-footer', _vue.IonFooter);
    vue.component('ion-buttons', _vue.IonButtons);
    vue.component('ion-select-option', _vue.IonSelectOption);
    vue.component('ion-menu', _vue.IonMenu);
    vue.component('ion-infinite-scroll', _vue.IonInfiniteScroll);
    vue.component('ion-infinite-scroll-content', _vue.IonInfiniteScrollContent);
    vue.component('ion-refresher', _vue.IonRefresher);
    vue.component('ion-refresher-content', _vue.IonRefresherContent);
    vue.component('ion-item-sliding', _vue.IonItemSliding);
    vue.component('ion-item-option', _vue.IonItemOption);
    vue.component('ion-item-options', _vue.IonItemOptions);
    vue.component('ion-grid', _vue.IonGrid);
    vue.component('ion-action-sheet', _vue.IonActionSheet);
    vue.component('ion-card', _vue.IonCard);
    vue.component('ion-card-header', _vue.IonCardHeader);
    vue.component('ion-card-title', _vue.IonCardTitle);
    vue.component('ion-card-content', _vue.IonCardContent); // 视图组件
    // UI绘制组件

    vue.component('app-calendar', _common.AppCalendarComponent);
    vue.component('app-menu-icon', _common.AppMenuIconComponent);
    vue.component('app-menu-list', _common.AppMenuListComponent);
    vue.component('app-quick-search', _common.AppQuickSearchComponent);
    vue.component('app-list-item', _common.AppListItemComponent);
    vue.component('app-icon-item', _common.AppIconItemComponent);
    vue.component('app-mob-alert', _common.AppMobAlert);
    vue.component('app-icon', _common.AppIcon);
    vue.component('app-quick-group', _common.AppQuickGroup);
    vue.component('app-actionbar', _common.AppActionBar);
    vue.component('app-dashboard-design', _common.AppDashboardDesign);
    vue.component('app-anchor', _common.AppAnchorComponent);
    vue.component('app-steps', _common.AppStepsComponent); //指令注册

    vue.directive('badge', _directives.Badge);
    vue.directive('format', _directives.Format);
  }

};
exports.ComponentRegister = ComponentRegister;