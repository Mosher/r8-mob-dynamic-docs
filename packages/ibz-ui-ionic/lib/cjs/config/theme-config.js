"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ThemeOptions = exports.PresetTheme = void 0;

/**
 * @description 预置主题
 */
const PresetTheme = [{
  name: 'default',
  title: '默认',
  lanResTag: 'theme.preset.default',
  color: '#f4f5f8'
}, {
  name: 'dark',
  title: '灰暗',
  lanResTag: 'theme.preset.dark',
  color: '#343d46'
}];
/**
 * @description 主题配置项
 */

exports.PresetTheme = PresetTheme;
const ThemeOptions = [{
  name: 'color',
  title: '基础色',
  lanResTag: 'theme.color.base',
  items: [{
    name: 'Primary',
    title: '主要色',
    lanResTag: 'theme.color.primary',
    default: '',
    shade: '',
    tint: ''
  }, {
    name: 'Secondary',
    title: '次要色',
    lanResTag: 'theme.color.second',
    default: '',
    shade: '',
    tint: ''
  }, {
    name: 'Tertiary',
    title: '三次色',
    lanResTag: 'theme.color.third',
    default: '',
    shade: '',
    tint: ''
  }, {
    name: 'Success',
    title: '成功',
    lanResTag: 'theme.color.success',
    default: '',
    shade: '',
    tint: ''
  }, {
    name: 'Warning',
    title: '警告',
    lanResTag: 'theme.color.warning',
    default: '',
    shade: '',
    tint: ''
  }, {
    name: 'Danger',
    title: '危险',
    lanResTag: 'theme.color.danger',
    default: '',
    shade: '',
    tint: ''
  }, {
    name: 'Dark',
    title: '暗色',
    lanResTag: 'theme.color.dark',
    default: '',
    shade: '',
    tint: ''
  }, {
    name: 'Medium',
    title: '中色',
    lanResTag: 'theme.color.medium',
    default: '',
    shade: '',
    tint: ''
  }, {
    name: 'Light',
    title: '亮色',
    lanResTag: 'theme.color.light',
    default: '',
    shade: '',
    tint: ''
  }]
}, {
  name: 'tab-bar',
  title: '分页栏',
  lanResTag: 'theme.tabbar',
  items: [{
    name: 'background',
    title: '背景色',
    lanResTag: 'theme.color.background',
    default: ''
  }, {
    name: 'background-focused',
    title: '背景色（聚焦）',
    lanResTag: 'theme.color.backgroundfocus',
    default: ''
  }, {
    name: 'color',
    title: '颜色',
    lanResTag: 'theme.color.font',
    default: ''
  }, {
    name: 'color-selected',
    lanResTag: 'theme.color.selected',
    title: '选中色',
    default: ''
  }]
}, {
  name: 'toolbar',
  title: '工具栏',
  lanResTag: 'theme.toolbar',
  items: [{
    name: 'background',
    title: '背景色',
    lanResTag: 'theme.color.background',
    default: ''
  }, {
    name: 'color',
    lanResTag: 'theme.color.font',
    title: '颜色',
    default: ''
  }]
}];
exports.ThemeOptions = ThemeOptions;