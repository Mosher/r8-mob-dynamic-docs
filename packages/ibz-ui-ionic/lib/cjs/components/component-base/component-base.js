"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ComponentBase = void 0;

var _vue = require("vue");

var _vueRouter = require("vue-router");

/**
 * 组件基类
 *
 * @export
 * @class ComponentBase
 * @template Props 输入属性接口
 */
class ComponentBase {
  /**
   * Creates an instance of ComponentBase.
   *
   * @param {Props} props
   * @param {SetupContext<EmitsOptions>} ctx
   */
  constructor(props, ctx) {
    /**
     * 路由解析工具类
     *
     * @protected
     * @type {*}
     */
    this.pathToRegExp = require('path-to-regexp');
    this.props = props;
    this.ctx = ctx;
    this.route = (0, _vueRouter.useRoute)();
    this.router = (0, _vueRouter.useRouter)();
    (0, _vue.onMounted)(this.mounted.bind(this));
  }
  /**
   * 构建组件
   *
   * @memberof ComponentBase
   */


  setup() {}
  /**
   * 初始化
   *
   * @memberof ComponentBase
   */


  init() {}
  /**
   * 组件挂载完成
   *
   * @memberof ComponentBase
   */


  mounted() {}
  /**
   * 绘制内容
   *
   * @return {*}  {*}
   * @memberof ComponentBase
   */


  render() {}
  /**
   * 强制更新
   *
   * @memberof ComponentBase
   */


  forceUpdate() {}
  /**
   * 输入属性变更执行
   *
   * @memberof ComponentBase
   */


  watchEffect() {}
  /**
   * 组件销毁
   *
   * @memberof ComponentBase
   */


  unmounted() {}

  renderSlot(name) {
    var _a;

    if (name && ((_a = this.ctx.slots) === null || _a === void 0 ? void 0 : _a[name])) {
      return this.ctx.slots[name]();
    }
  }

}

exports.ComponentBase = ComponentBase;