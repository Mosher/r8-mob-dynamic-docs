"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.GenerateComponent = GenerateComponent;

var _vue = require("vue");

/**
 * 生成组件
 *
 * @author chitanda
 * @date 2021-05-18 11:05:46
 * @export
 * @param {*} ComponentClass 组件类
 * @param {*} [props] 传入的输入参数
 * @return {*}
 */
function GenerateComponent(ComponentClass, props, components) {
  return (0, _vue.defineComponent)({
    name: ComponentClass.name,
    props,
    components,

    setup(props, ctx) {
      const c = new ComponentClass(props, ctx);
      c.setup();
      return {
        c
      };
    },

    beforeMount() {
      this.c.forceUpdate = () => {
        this.$forceUpdate();
      };

      this.c.$tl = (key, value) => {
        if (this.$tl) {
          return this.$tl(key, value);
        } else {
          return value;
        }
      };

      (0, _vue.watchEffect)(() => {
        this.c.watchEffect();
      });
      this.c.init();
    },

    unmounted() {
      this.c.unmounted();
    },

    render() {
      return this.c.render();
    }

  });
}