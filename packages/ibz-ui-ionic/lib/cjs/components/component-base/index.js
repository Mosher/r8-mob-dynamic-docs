"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "ComponentBase", {
  enumerable: true,
  get: function () {
    return _componentBase.ComponentBase;
  }
});
Object.defineProperty(exports, "GenerateComponent", {
  enumerable: true,
  get: function () {
    return _generateComponent.GenerateComponent;
  }
});

var _componentBase = require("./component-base");

var _generateComponent = require("./generate-component");