"use strict";

var _interopRequireWildcard = require("D:/IbizWork/r8-mob-dynamic-docs/packages/ibz-ui-ionic/node_modules/@babel/runtime/helpers/interopRequireWildcard").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppIcon = void 0;

var _vue = require("vue");

var ionicons = _interopRequireWildcard(require("ionicons/icons"));

var _ibzCore = require("ibz-core");

const AppIconProps = {
  /**
   * 图标名称
   *
   * @type {string}
   * @memberof AppIconProps
   */
  name: String,

  /**
   * 图标对象
   *
   * @type {string}
   * @memberof AppIconProps
   */
  icon: Object,

  /**
   * 图标路径
   *
   * @type {string}
   * @memberof AppIconProps
   */
  iconSrc: String
};
const AppIcon = (0, _vue.defineComponent)({
  name: 'AppIcon',
  props: AppIconProps,
  computed: {
    /**
     * iconName
     *
     * @returns string
     */
    iconName() {
      var _a;

      let name = '';

      if (this.name) {
        name = _ibzCore.Util.formatCamelCase(this.name);
      } else if ((_a = this.icon) === null || _a === void 0 ? void 0 : _a.cssClass) {
        name = _ibzCore.Util.formatCamelCase(_ibzCore.ViewTool.setIcon(this.icon.cssClass));
      }

      return name ? ionicons[name] : null;
    },

    /**
     * imagePath
     *
     * @returns string
     */
    imagePath() {
      var _a;

      let src = '';

      if (this.iconSrc) {
        src = this.iconSrc;
      } else {
        src = (_a = this.icon) === null || _a === void 0 ? void 0 : _a.imagePath;
      }

      return src;
    }

  },

  render() {
    if (this.imagePath) {
      return (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-icon"), {
        "class": 'app-icon',
        "src": this.imagePath
      }, null);
    } else if (this.iconName) {
      return (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-icon"), {
        "class": 'app-icon',
        "icon": this.iconName
      }, null);
    } else {
      return null;
    }
  }

});
exports.AppIcon = AppIcon;