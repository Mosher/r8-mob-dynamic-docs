"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppLayoutContainer = void 0;

var _vue = require("vue");

function _isSlot(s) {
  return typeof s === 'function' || Object.prototype.toString.call(s) === '[object Object]' && !(0, _vue.isVNode)(s);
}

const AppLayoutContainerProps = {
  /**
   * @description 样式类名对象
   * @type {*}
   * @memberof AppLayoutContainerProps
   */
  class: {
    type: Object
  },

  /**
   * @description 样式对象
   * @type {*}
   * @memberof AppLayoutContainerProps
   */
  style: {
    type: String,
    default: ''
  },

  /**
   * @description 样式对象
   * @type {*}
   * @memberof AppLayoutContainerProps
   */
  layout: {
    type: Object,
    default: {}
  }
};
const AppLayoutContainer = (0, _vue.defineComponent)({
  name: 'AppLayoutContainer',
  props: AppLayoutContainerProps,
  methods: {
    /**
     * @description 合并样式类名
     * @param {*} arg1 类名对象
     * @param {(any | string)} arg2 类名字符串或对象
     */
    mergeClassNames(arg1, arg2) {
      if (typeof arg2 == 'string') {
        arg2.split(' ').map(item => {
          Object.assign(arg1, {
            [item]: true
          });
        });
      } else {
        Object.assign(arg1, arg2);
      }
    }

  },

  render() {
    var _a, _b; // 插槽内容


    const defaultContent = (_a = this.$slots.default) === null || _a === void 0 ? void 0 : _a();
    let layoutMode = (_b = this.layout) === null || _b === void 0 ? void 0 : _b.layout;

    if (this.layout && layoutMode == 'FLEX') {
      //  FLEX布局
      // 样式处理
      let cssStyle = 'width: 100%; height: 100%; overflow: auto; display: flex;';
      cssStyle += this.layout.dir ? `flex-direction: ${this.layout.dir};` : '';
      cssStyle += this.layout.align ? `justify-content: ${this.layout.align};` : '';
      cssStyle += this.layout.vAlign ? `align-items: ${this.layout.vAlign};` : '';
      cssStyle += this.style; // 类名处理

      let classNames = {
        'app-layout-container': true,
        'app-layout-container--flex': true
      };
      this.mergeClassNames(classNames, this.class); // 绘制flex的父容器

      return (0, _vue.createVNode)("div", {
        "class": classNames,
        "style": cssStyle
      }, [defaultContent]);
    } else {
      //  栅格布局
      // 样式处理
      let cssStyle = 'height: 100%;';
      cssStyle += this.style; // 类名处理

      let classNames = {
        'app-layout-container': true,
        'app-layout-container--grid': true
      };
      this.mergeClassNames(classNames, this.class); // 绘制栅格的父容器

      return (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-row"), {
        "class": classNames,
        "style": cssStyle
      }, _isSlot(defaultContent) ? defaultContent : {
        default: () => [defaultContent]
      });
    }
  }

});
exports.AppLayoutContainer = AppLayoutContainer;