"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppCtrlContainer = void 0;

var _vue = require("vue");

/**
 * @description 部件容器输入参数
 */
const AppCtrlContainerProps = {
  ctrlName: {
    type: String,
    default: ''
  }
};
/**
 * @description 部件容器组件
 */

const AppCtrlContainer = (0, _vue.defineComponent)({
  name: 'AppCtrlContainer',
  props: AppCtrlContainerProps,

  setup(props, {
    slots
  }) {
    return () => {
      return (0, _vue.h)('div', {
        class: 'ctrl-container'
      }, slots.default && slots.default());
    };
  }

});
exports.AppCtrlContainer = AppCtrlContainer;