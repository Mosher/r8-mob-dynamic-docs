"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _vue = require("vue");

var _core = require("@ionic/core");

var _ibzCore = require("ibz-core");

function _isSlot(s) {
  return typeof s === 'function' || Object.prototype.toString.call(s) === '[object Object]' && !(0, _vue.isVNode)(s);
}

const ModelProps = {
  /**
   * 导航上下文
   *
   * @type {any}
   * @memberof ModelProps
   */
  navContext: {
    type: Object,
    default: {}
  },

  /**
   * 视图上下文参数
   *
   * @type {any}
   * @memberof ModelProps
   */
  navParam: {
    type: Object,
    default: {}
  },

  /**
   * 导航数据
   *
   * @type {*}
   * @memberof ModelProps
   */
  navDatas: {
    type: Array,
    default: []
  },
  view: {
    type: Object
  },
  model: {
    type: Object
  },
  subject: {
    type: Object
  }
};

var _default = (0, _vue.defineComponent)({
  props: ModelProps,

  /**
   * vue 生命周期
   *
   * @param {*} props
   * @return {*}
   */
  setup(props) {
    const {
      view
    } = (0, _vue.toRefs)(props);
    /**
     * 临时结果
     *
     * @type {any}
     * @memberof AppDrawer
     */

    const tempResult = {
      ret: ''
    };
    /**
     * 视图名称
     *
     * @type {string}
     * @memberof AppDrawer
     */

    const viewComponent = view.value.viewComponent;
    /**
     *  视图动态路径
     *
     * @type {*}
     */

    const viewPath = view.value.viewPath;
    /**
     *  打开模式
     *
     * @type {*}
     */

    const side = view.value.placement == 'DRAWER_RIGHT' ? 'end' : 'start';
    /**
     * 视图层级
     *
     * @type {any}
     * @memberof AppDrawer
     */

    const zIndex = null;

    const uuid = _ibzCore.Util.createUUID();

    const menu = (0, _vue.ref)('menu');
    /**
     * 自定义类名
     */

    const customClass = view.value.customClass;
    /**
     * 自定义样式
     */

    const customStyle = view.value.customStyle;
    return {
      tempResult,
      viewPath,
      viewComponent,
      zIndex,
      menu,
      side,
      customClass,
      uuid,
      customStyle
    };
  },

  methods: {
    /**
     * 视图事件
     *
     * @param viewName 视图名
     * @param action 视图行为
     * @param data 行为数据
     */
    handleViewEvent(viewName, action, data) {
      switch (action) {
        case _ibzCore.AppViewEvents.CLOSE:
          this.close(data);
          break;

        case _ibzCore.AppViewEvents.DATA_CHANGE:
          this.dataChange(data);
          break;
      }
    },

    /**
     * 视图关闭
     *
     * @memberof AppDrawer
     */
    close(result) {
      if (result && Array.isArray(result) && result.length > 0) {
        Object.assign(this.tempResult, {
          ret: 'OK',
          datas: _ibzCore.Util.deepCopy(result)
        });
      }

      this.handleCloseDrawer();
    },

    /**
     * 视图数据变化
     *
     * @memberof AppDrawer
     */
    dataChange(result) {
      this.tempResult = {
        ret: ''
      };

      if (result && Array.isArray(result) && result.length > 0) {
        Object.assign(this.tempResult, {
          ret: 'OK',
          datas: _ibzCore.Util.deepCopy(result)
        });
      }
    },

    /**
     * 处理数据，向外抛值
     *
     * @memberof AppDrawer
     */
    handleCloseDrawer() {
      if (this.subject && this.tempResult) {
        this.subject.next(this.tempResult);
      }

      _core.menuController.close(this.uuid).then(() => {
        var _a;

        (_a = this.model) === null || _a === void 0 ? void 0 : _a.dismiss();
      });
    }

  },

  async mounted() {
    await _core.menuController.enable(true, this.uuid);

    _core.menuController.open(this.uuid);
  },

  /**
   * 绘制内容
   *
   * @memberof AppFromGroup
   */
  render() {
    let _slot;

    return (0, _vue.createVNode)("div", {
      "style": {
        zIndex: this.zIndex,
        height: '100%'
      }
    }, [(0, _vue.createVNode)("div", {
      "id": this.uuid
    }, null), (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-content"), null, {
      default: () => [(0, _vue.createVNode)((0, _vue.resolveComponent)("ion-menu"), {
        "ref": 'menu',
        "side": this.side,
        "onIonDidClose": $event => {
          setTimeout(() => {
            this.close(null);
          }, 1000);
        },
        "contentId": this.uuid,
        "menuId": this.uuid,
        "style": this.customStyle
      }, _isSlot(_slot = (0, _vue.h)(this.viewComponent, {
        viewShowMode: 'MODEL',
        navContext: this.navContext,
        navParam: this.navParam,
        navDatas: this.navDatas,
        viewPath: this.viewPath,
        class: this.customClass,
        onViewEvent: ({
          viewName,
          action,
          data
        }) => {
          this.handleViewEvent(viewName, action, data);
        }
      })) ? _slot : {
        default: () => [_slot]
      })]
    })]);
  }

});

exports.default = _default;