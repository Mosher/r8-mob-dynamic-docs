"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppQuickSearchComponent = exports.AppQuickSearch = exports.AppQuickSearchProps = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

class AppQuickSearchProps {}

exports.AppQuickSearchProps = AppQuickSearchProps;

class AppQuickSearch extends _componentBase.ComponentBase {
  /**
   * @description 快速搜索
   * @param {*} $event
   * @memberof AppQuickSearch
   */
  quickSearch($event) {
    this.ctx.emit('quickSearch', $event.detail.value);
  }
  /**
   * @description 绘制快速搜索栏
   * @return {*}
   * @memberof AppQuickSearch
   */


  render() {
    return (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-header"), {
      "class": 'app-quick-search'
    }, {
      default: () => [(0, _vue.createVNode)((0, _vue.resolveComponent)("ion-toolbar"), null, {
        default: () => [(0, _vue.createVNode)((0, _vue.resolveComponent)("ion-searchbar"), {
          "showClearButton": true,
          "debounce": '500',
          "onIonChange": $event => (0, _ibzCore.throttle)(this.quickSearch, [$event], this)
        }, null)]
      })]
    });
  }

}

exports.AppQuickSearch = AppQuickSearch;
const AppQuickSearchComponent = (0, _componentBase.GenerateComponent)(AppQuickSearch);
exports.AppQuickSearchComponent = AppQuickSearchComponent;