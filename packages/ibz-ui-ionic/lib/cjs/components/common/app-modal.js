"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

const ModelProps = {
  /**
   * 导航上下文
   *
   * @type {any}
   * @memberof ModelProps
   */
  navContext: {
    type: Object,
    default: {}
  },

  /**
   * 视图上下文参数
   *
   * @type {any}
   * @memberof ModelProps
   */
  navParam: {
    type: Object,
    default: {}
  },

  /**
   * 导航数据
   *
   * @type {*}
   * @memberof ModelProps
   */
  navDatas: {
    type: Array,
    default: []
  },
  view: {
    type: Object
  },
  model: {
    type: Object
  },
  subject: {
    type: Object
  }
};

var _default = (0, _vue.defineComponent)({
  props: ModelProps,

  /**
   * vue 生命周期
   *
   * @memberof AppFormGroup
   */
  setup(props) {
    const {
      view
    } = (0, _vue.toRefs)(props);
    /**
     * 临时结果
     */

    const tempResult = {
      ret: ''
    };
    /**
     * 视图组件
     *
     * @type {any}
     * @memberof AppModal
     */

    const viewComponent = view.value.viewComponent;
    /**
     *  视图动态路径
     *
     * @type {*}
     */

    const viewPath = view.value.viewPath;
    /**
     * 自定义类名
     */

    const customClass = view.value.customClass;
    /**
     * 自定义样式
     */

    const customStyle = view.value.customStyle;
    /**
     * 视图层级
     *
     * @type {any}
     * @memberof AppModal
     */

    const zIndex = null;
    return {
      tempResult,
      viewPath,
      viewComponent,
      customClass,
      zIndex,
      customStyle
    };
  },

  mounted() {
    if (this.model && this.customStyle) {
      const modal = this.model;
      Object.keys(this.customStyle).forEach(key => {
        modal.style[key] = this.customStyle[key];
      });
    }
  },

  methods: {
    /**
     * 视图事件
     *
     * @param viewName 视图名
     * @param action 视图行为
     * @param data 抛出数据
     */
    handleViewEvent(viewName, action, data) {
      switch (action) {
        case _ibzCore.AppViewEvents.CLOSE:
          this.close(data);
          break;

        case _ibzCore.AppViewEvents.DATA_CHANGE:
          this.dataChange(data);
          break;
      }
    },

    /**
     * 视图关闭
     *
     * @memberof AppModal
     */
    close(result) {
      if (result && Array.isArray(result) && result.length > 0) {
        Object.assign(this.tempResult, {
          ret: 'OK',
          datas: _ibzCore.Util.deepCopy(result)
        });
      }

      this.handleCloseModel();
    },

    /**
     * 视图数据变化
     *
     * @memberof AppModal
     */
    dataChange(result) {
      this.tempResult = {
        ret: ''
      };

      if (result && Array.isArray(result) && result.length > 0) {
        Object.assign(this.tempResult, {
          ret: 'OK',
          datas: _ibzCore.Util.deepCopy(result)
        });
      }
    },

    /**
     * 处理数据，向外抛值
     *
     * @memberof AppModal
     */
    handleCloseModel() {
      var _a;

      if (this.subject && this.tempResult) {
        this.subject.next(this.tempResult);
      }

      (_a = this.model) === null || _a === void 0 ? void 0 : _a.dismiss();
    }

  },

  /**
   * 绘制内容
   *
   * @memberof AppFromGroup
   */
  render() {
    return (0, _vue.h)(this.viewComponent, {
      viewShowMode: 'MODEL',
      navContext: this.navContext,
      navParam: this.navParam,
      navDatas: this.navDatas,
      viewPath: this.viewPath,
      class: this.customClass,
      onViewEvent: ({
        viewName,
        action,
        data
      }) => {
        this.handleViewEvent(viewName, action, data);
      }
    });
  }

});

exports.default = _default;