"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppMenuIconComponent = exports.AppMenuIcon = exports.AppMenuIconProps = void 0;

var _vue = require("vue");

var _componentBase = require("../component-base");

class AppMenuIconProps {
  constructor() {
    /**
     * @description 菜单
     * @type {any[]}
     * @memberof AppMenuIconProps
     */
    this.menu = [];
    /**
     * @description 应用界面服务对象
     * @type {IParam}
     * @memberof AppMenuIconProps
     */

    this.appUIService = {};
    /**
     * 模型服务
     *
     * @type {IParam}
     * @memberof AppMenuIconProps
     */

    this.modelService = {};
  }

}

exports.AppMenuIconProps = AppMenuIconProps;

class AppMenuIcon extends _componentBase.ComponentBase {
  constructor() {
    super(...arguments);
    /**
     * 菜单项集合
     *
     * @type IPSAppMenuItem[]
     * @memberof AppMenuIcon
     */

    this.menuItems = [];
  }
  /**
   * vue 生命周期
   *
   * @memberof AppMenuIcon
   */


  setup() {
    this.menuItems = this.props.menu;
  }
  /**
   * 打开视图
   *
   * @public
   * @memberof AppMenuIcon
   */


  openView(item) {
    this.ctx.emit('menuClick', item);
  }
  /**
   * @description 根据菜单项获取菜单权限
   * @param {IPSAppMenuItem} menuItem
   * @return {*}
   * @memberof AppMenuIcon
   */


  getMenusPermission(menuItem) {
    if (!App.isPreviewMode() && menuItem.accessKey) {
      return this.props.appUIService.getResourceOPPrivs(menuItem.accessKey);
    } else {
      return true;
    }
  }
  /**
   * 绘制快速菜单图标项
   *
   * @public
   * @param {IPSAppMenuItem} item
   * @memberof AppMenuIcon
   */


  renderQuickMenuIconItem(item) {
    var _a, _b;

    if (item.hidden || !this.getMenusPermission(item)) {
      return null;
    }

    const cssName = ((_a = item.getPSSysCss()) === null || _a === void 0 ? void 0 : _a.cssName) || '';
    const icon = item.getPSSysImage();
    return (0, _vue.createVNode)("div", {
      "class": [cssName, 'menu-item'],
      "onClick": () => this.openView(item)
    }, [icon ? (0, _vue.createVNode)((0, _vue.resolveComponent)("app-icon"), {
      "icon": icon
    }, null) : (0, _vue.createVNode)((0, _vue.resolveComponent)("app-icon"), {
      "name": 'home'
    }, null), (0, _vue.createVNode)("span", {
      "class": 'menu-item-label'
    }, [this.$tl((_b = item.getCapPSLanguageRes()) === null || _b === void 0 ? void 0 : _b.lanResTag, item.caption)])]);
  }
  /**
   * 绘制内容
   *
   * @memberof AppMenuIcon
   */


  render() {
    if (this.menuItems.length > 0) {
      return (0, _vue.createVNode)("div", {
        "class": 'app-menu-icon'
      }, [this.menuItems.map(item => {
        return this.renderQuickMenuIconItem(item);
      })]);
    }
  }

}

exports.AppMenuIcon = AppMenuIcon;
const AppMenuIconComponent = (0, _componentBase.GenerateComponent)(AppMenuIcon, Object.keys(new AppMenuIconProps()));
exports.AppMenuIconComponent = AppMenuIconComponent;