"use strict";

var _interopRequireDefault = require("D:/IbizWork/r8-mob-dynamic-docs/packages/ibz-ui-ionic/node_modules/@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppDatePicker = void 0;

var _vue = require("vue");

var _moment = _interopRequireDefault(require("moment"));

const AppDatePickerProps = {
  /**
   * 双向绑定值
   * @type {any}
   * @memberof AppDatePickerProps
   */
  value: [String, Number],

  /**
   * placeholder值
   * @type {String}
   * @memberof AppDatePickerProps
   */
  placeholder: String,

  /**
   * 是否禁用
   * @type {boolean}
   * @memberof AppDatePickerProps
   */
  disabled: Boolean,

  /**
   * 只读模式
   *
   * @type {boolean}
   * @memberof AppDatePickerProps
   */
  readonly: Boolean,

  /**
   * 日期格式
   *
   * @type {string}
   * @memberof AppDatePickerProps
   */
  displayFormat: {
    type: String,
    default: 'YYYY-MM-DD HH:mm:ss'
  }
};
const AppDatePicker = (0, _vue.defineComponent)({
  name: 'AppDatePicker',
  props: AppDatePickerProps,
  emits: ['editorValueChange'],
  setup: () => {
    const currentDate = new Date().getFullYear();
    const min = currentDate - 100;
    const max = currentDate + 100;
    return {
      min,
      max
    };
  },
  methods: {
    /**
     * 输入框值改变事件
     *
     * @param {*} e
     */
    valueChange(event) {
      let tempValue = null;
      tempValue = (0, _moment.default)(event.detail.value).format(this.displayFormat);

      if (Object.is(tempValue, 'Invalid date')) {
        tempValue = event.detail.value;
      }

      if (Object.is(this.value, tempValue)) {
        return;
      }

      this.$emit('editorValueChange', tempValue);
    }

  },

  render() {
    return (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-datetime"), {
      "class": 'app-date-picker',
      "value": this.value,
      "disabled": this.disabled,
      "readonly": this.readonly,
      "min": this.min,
      "max": this.max,
      "placeholder": this.placeholder,
      "displayFormat": this.displayFormat,
      "onIonChange": e => {
        this.valueChange(e);
      }
    }, null);
  }

});
exports.AppDatePicker = AppDatePicker;