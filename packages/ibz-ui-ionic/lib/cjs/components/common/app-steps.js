"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppStepsComponent = void 0;

var _vue = require("vue");

const AppStepsProps = {
  steps: {
    type: Array
  },
  active: {
    type: String || Number,
    default: ''
  },
  panelInstance: Object
};
/**
 * @description 步骤条组件
 */

const AppStepsComponent = (0, _vue.defineComponent)({
  name: 'AppSteps',
  props: AppStepsProps,

  setup(props, {
    emit
  }) {
    const stepItemClick = (step, event) => {
      event.stopPropagation();
      emit('stepClick', step, event);
    };

    const renderStep = (step, index) => {
      const {
        active
      } = props;
      let _index = 0;

      if (typeof active == 'number') {
        _index = active >= 0 ? active : 0;
      } else {
        _index = isNaN(Number(active)) ? 0 : Number(active);
      }

      return (0, _vue.createVNode)("div", {
        "class": ['step-item', _index >= index ? _index == index ? 'step-item--active' : 'step-item--finish' : ''],
        "onClick": event => {
          stepItemClick(step, event);
        }
      }, [(0, _vue.createVNode)("div", {
        "class": ["step-item__title", step.className]
      }, [step.icon ? (0, _vue.createVNode)((0, _vue.resolveComponent)("app-icon"), {
        "name": step.icon
      }, null) : null, props.panelInstance.$tl(step.titleLanResTag, step.title), step.subTitle ? [(0, _vue.createVNode)("br", null, null), (0, _vue.createVNode)("span", {
        "class": "step-item__subtitle"
      }, [props.panelInstance.$tl(step.subTitleLanResTag, step.subTitle)])] : null]), (0, _vue.createVNode)("div", {
        "class": "step-item__container"
      }, [(0, _vue.createVNode)("i", {
        "class": _index == index ? "step-item__circle--active" : 'step-item__circle'
      }, null)]), (0, _vue.createVNode)("div", {
        "class": "step-item__line"
      }, null)]);
    };

    return () => {
      const {
        steps
      } = props;
      return (0, _vue.createVNode)("div", {
        "class": "app-steps"
      }, [(0, _vue.createVNode)("div", {
        "class": ["steps-items", steps.find(step => step.subTitle) ? 'has-subtitle' : '']
      }, [steps ? steps.map((step, index) => {
        return renderStep(step, index);
      }) : null])]);
    };
  }

});
exports.AppStepsComponent = AppStepsComponent;