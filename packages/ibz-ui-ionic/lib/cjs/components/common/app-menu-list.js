"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppMenuListComponent = exports.AppMenuList = exports.AppMenuListProps = void 0;

var _vue = require("vue");

var _componentBase = require("../component-base");

function _isSlot(s) {
  return typeof s === 'function' || Object.prototype.toString.call(s) === '[object Object]' && !(0, _vue.isVNode)(s);
}

class AppMenuListProps {
  constructor() {
    /**
     * @description 菜单
     * @type {any[]}
     * @memberof AppMenuListProps
     */
    this.menu = [];
    /**
     * @description 应用界面服务对象
     * @type {IParam}
     * @memberof AppMenuIconProps
     */

    this.appUIService = {};
    /**
     * 模型服务
     *
     * @type {IParam}
     * @memberof AppMenuIconProps
     */

    this.modelService = {};
  }

}

exports.AppMenuListProps = AppMenuListProps;

class AppMenuList extends _componentBase.ComponentBase {
  constructor() {
    super(...arguments);
    /**
     * 菜单项集合
     *
     * @type IPSAppMenuItem[]
     * @memberof AppMenuList
     */

    this.menuItems = [];
  }
  /**
   * vue 生命周期
   *
   * @memberof AppMenuList
   */


  setup() {
    this.menuItems = this.props.menu;
  }
  /**
   * 打开视图
   *
   * @public
   * @memberof AppMenuList
   */


  openView(item) {
    this.ctx.emit('menuClick', item);
  }
  /**
   * @description 根据菜单项获取菜单权限
   * @param {IPSAppMenuItem} menuItem
   * @return {*}
   * @memberof AppMenuList
   */


  getMenusPermission(menuItem) {
    if (!App.isPreviewMode() && menuItem.accessKey) {
      return this.props.appUIService.getResourceOPPrivs(menuItem.accessKey);
    } else {
      return true;
    }
  }
  /**
   * 绘制快速菜单图标项
   *
   * @public
   * @param {IPSAppMenuItem} item
   * @memberof AppMenuList
   */


  renderQuickMenuListItem(item) {
    let _slot;

    var _a, _b;

    if (item.hidden || !this.getMenusPermission(item)) {
      return null;
    }

    const cssName = ((_a = item.getPSSysCss()) === null || _a === void 0 ? void 0 : _a.cssName) || '';
    const icon = item.getPSSysImage();
    return (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-item"), {
      "class": [cssName, 'menu-list-item'],
      "onClick": () => this.openView(item)
    }, {
      default: () => [icon ? (0, _vue.createVNode)((0, _vue.resolveComponent)("app-icon"), {
        "icon": icon
      }, null) : (0, _vue.createVNode)((0, _vue.resolveComponent)("app-icon"), {
        "name": 'home'
      }, null), (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-label"), null, _isSlot(_slot = this.$tl((_b = item.getCapPSLanguageRes()) === null || _b === void 0 ? void 0 : _b.lanResTag, item.caption)) ? _slot : {
        default: () => [_slot]
      }), (0, _vue.createVNode)((0, _vue.resolveComponent)("app-icon"), {
        "name": 'chevron-forward-outline'
      }, null)]
    });
  }
  /**
   * 绘制内容
   *
   * @memberof AppMenuList
   */


  render() {
    if (this.menuItems.length > 0) {
      let _slot2;

      return (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-list"), {
        "class": 'app-menu-list'
      }, _isSlot(_slot2 = this.menuItems.map(item => {
        return this.renderQuickMenuListItem(item);
      })) ? _slot2 : {
        default: () => [_slot2]
      });
    }
  }

}

exports.AppMenuList = AppMenuList;
const AppMenuListComponent = (0, _componentBase.GenerateComponent)(AppMenuList, Object.keys(new AppMenuListProps()));
exports.AppMenuListComponent = AppMenuListComponent;