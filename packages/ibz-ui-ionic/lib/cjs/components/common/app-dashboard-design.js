"use strict";

var _interopRequireDefault = require("D:/IbizWork/r8-mob-dynamic-docs/packages/ibz-ui-ionic/node_modules/@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppDashboardDesign = void 0;

var _vue = require("vue");

var _vuedraggable = _interopRequireDefault(require("vuedraggable"));

var _uiService = require("../../ui-service");

const AppDashboardDesignProps = {
  /**
   * @description 应用上下文
   * @type {*}
   * @memberof AppDashboardDesignProps
   */
  navContext: {
    type: Object
  },

  /**
   * @description 视图参数
   * @type {*}
   * @memberof AppDashboardDesignProps
   */
  navParam: {
    type: Object
  },

  /**
   * 模型服务
   * @type {Object}
   * @memberof InputBox
   */
  modelService: Object
}; //  数据看板自定义组件

const AppDashboardDesign = (0, _vue.defineComponent)({
  name: 'AppDashboardDesign',
  props: AppDashboardDesignProps,
  components: {
    draggable: _vuedraggable.default
  },

  setup(props, ctx) {
    /**
     * @description 选中门户集合
     * @type {IParam[]}
     */
    const selections = (0, _vue.reactive)([]);
    /**
     * @description 未选中门户集合
     * @type {IParam[]}
     */

    const notSelections = (0, _vue.reactive)([]);
    /**
     * @description 显示状态
     * @type {boolean}
     */

    const titleStatus = (0, _vue.ref)(true);
    /**
     * @description 面板设计服务
     * @type {AppDashboardDesignService}
     */

    const designService = _uiService.AppDashboardDesignService.getInstance();
    /**
     * @description 部件挂载之前
     */


    (0, _vue.onBeforeMount)(() => {
      const {
        navContext,
        navParam
      } = (0, _vue.toRefs)(props);
      designService.loadPortletList(navContext.value, navParam.value).then(result => {
        handleSelections(result.data);
      });
    });
    /**
     * @description 监听
     */

    (0, _vue.watchEffect)(() => {
      if (selections.length > 0) {
        selections.forEach((selection, index) => {
          selection.id = index + 1;
        });
      }

      if (notSelections.length > 0) {
        notSelections.forEach((notSelection, index) => {
          notSelection.id = index + 1;
        });
      }
    });
    /**
     * @description 处理选中数据
     */

    const handleSelections = items => {
      var _a;

      const {
        navParam
      } = (0, _vue.toRefs)(props);
      const customModel = ((_a = navParam.value) === null || _a === void 0 ? void 0 : _a.customModel) || [];
      items.forEach(item => {
        const custom = customModel.find(_custom => {
          return _custom.codeName == item.portletCodeName;
        });

        if (custom) {
          selections.push(item);
        } else {
          notSelections.push(item);
        }
      });
    };
    /**
     * @description 保存模型数据
     */


    const saveModel = (isReset = false) => {
      var _a;

      const {
        navParam,
        navContext
      } = (0, _vue.toRefs)(props);
      const tempViewParam = {};
      Object.assign(tempViewParam, Object.assign(Object.assign({}, navParam.value), {
        model: isReset ? [] : selections
      }));
      designService.saveModelData((_a = navParam.value) === null || _a === void 0 ? void 0 : _a.utilServiceName, navContext.value, tempViewParam);
    };
    /**
     * @description 删除选中项
     */


    const deleteItem = id => {
      const index = selections.findIndex(item => item.id === id);

      if (index !== -1) {
        notSelections.push(selections[index]);
        selections.splice(index, 1);
        saveModel();
      }
    };
    /**
     * @description 添加选中项
     */


    const addItem = id => {
      const index = notSelections.findIndex(item => item.id === id);

      if (index !== -1) {
        selections.push(notSelections[index]);
        notSelections.splice(index, 1);
        saveModel();
      }
    };
    /**
     * @description 拖拽结束
     */


    const dragEnd = () => {
      saveModel();
    };
    /**
     * @description 视图关闭
     */


    const closeView = event => {
      ctx.emit('viewEvent', {
        viewName: 'app-dashboard-design',
        action: 'onClose',
        data: []
      });
    };

    return {
      closeView,
      selections,
      dragEnd,
      deleteItem,
      notSelections,
      titleStatus,
      addItem
    };
  },

  methods: {
    renderHeader() {
      return (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-header"), null, {
        default: () => [(0, _vue.createVNode)((0, _vue.resolveComponent)("ion-toolbar"), {
          "class": 'view-header'
        }, {
          default: () => [(0, _vue.createVNode)((0, _vue.resolveComponent)("ion-buttons"), {
            "slot": 'start'
          }, {
            default: () => [(0, _vue.createVNode)((0, _vue.resolveComponent)("ion-button"), {
              "onClick": event => {
                this.closeView(event);
              }
            }, {
              default: () => [(0, _vue.createVNode)((0, _vue.resolveComponent)("app-icon"), {
                "name": 'chevron-back'
              }, null), `${this.$tl('common.dashboard.close', '关闭')}`]
            })]
          }), (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-title"), null, {
            default: () => [(0, _vue.createVNode)("label", {
              "class": 'title-label'
            }, [`${this.$tl('common.dashboard.customdatakanban', '自定义数据看板')}`])]
          })]
        })]
      });
    },

    renderContent() {
      return (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-content"), null, {
        default: () => [(0, _vue.createVNode)("div", {
          "class": 'dashboard-list'
        }, [(0, _vue.createVNode)("div", {
          "class": 'dashboard-list-item list-item--added'
        }, [(0, _vue.createVNode)("div", {
          "class": 'list-item-header'
        }, [`${this.$tl('common.dashboard.existcard', '已经添加的卡片')}`]), (0, _vue.createVNode)(_vuedraggable.default, {
          "list": this.selections,
          "handle": '.list-item-content-end',
          "itemKey": 'id',
          "animation": 200,
          "onEnd": this.dragEnd
        }, {
          item: ({
            element,
            index
          }) => {
            return (0, _vue.createVNode)("div", {
              "class": 'list-item-content',
              "key": element.componentName
            }, [(0, _vue.createVNode)("div", {
              "class": 'list-item-content-start'
            }, [(0, _vue.createVNode)((0, _vue.resolveComponent)("app-icon"), {
              "onClick": () => {
                this.deleteItem(element.id);
              },
              "name": 'remove-circle-outline'
            }, null)]), (0, _vue.createVNode)("div", {
              "class": 'list-item-pic'
            }, [(0, _vue.createVNode)("img", {
              "src": element.portletImage ? element.portletImage : 'assets/images/portlet.jpg',
              "alt": ''
            }, null)]), (0, _vue.createVNode)("div", {
              "class": 'list-item-text'
            }, [(0, _vue.createVNode)("div", null, [(0, _vue.createVNode)("span", null, [element.portletName]), (0, _vue.createVNode)("div", null, [element.detailText ? element.detailText : `${this.$tl('common.dashboard.nodescription', '暂无描述')}`])])]), (0, _vue.createVNode)("div", {
              "class": 'list-item-content-end'
            }, [(0, _vue.createVNode)((0, _vue.resolveComponent)("app-icon"), {
              "name": 'swap-vertical-outline'
            }, null)])]);
          }
        })]), (0, _vue.createVNode)("div", {
          "class": 'dashboard-list-item dashboard-list-item--add'
        }, [(0, _vue.createVNode)("div", {
          "class": 'list-item-header'
        }, [`${this.$tl('common.dashboard.noexistcard', '可添加的卡片')}`]), this.notSelections.map((item, index) => {
          return (0, _vue.createVNode)("div", {
            "class": 'list-item-content',
            "key": index
          }, [(0, _vue.createVNode)("div", {
            "class": 'list-item-content-start'
          }, [(0, _vue.createVNode)((0, _vue.resolveComponent)("app-icon"), {
            "onClick": () => {
              this.addItem(item.id);
            },
            "name": 'add-circle-outline'
          }, null)]), (0, _vue.createVNode)("div", {
            "class": 'list-item-pic'
          }, [(0, _vue.createVNode)("img", {
            "src": item.portletImage ? item.portletImage : 'assets/images/portlet.jpg',
            "alt": ''
          }, null)]), (0, _vue.createVNode)("div", {
            "class": 'list-item-text'
          }, [(0, _vue.createVNode)("div", null, [(0, _vue.createVNode)("span", null, [item.portletName]), (0, _vue.createVNode)("div", null, [item.detailText ? item.detailText : `${this.$tl('common.dashboard.nodescription', '暂无描述')}`])])]), (0, _vue.createVNode)("div", {
            "class": 'list-item-content-end'
          }, null)]);
        })])])]
      });
    }

  },

  render() {
    return (0, _vue.createVNode)("div", {
      "class": 'app-dashboard-design'
    }, [this.titleStatus ? this.renderHeader() : null, this.renderContent()]);
  }

});
exports.AppDashboardDesign = AppDashboardDesign;