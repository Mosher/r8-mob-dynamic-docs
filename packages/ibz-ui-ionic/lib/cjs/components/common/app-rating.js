"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppRating = void 0;

var _vue = require("vue");

const AppRatingProps = {
  /**
   * 双向绑定值
   * @type {String}
   * @memberof AppRatingProps
   */
  value: {
    type: String
  },

  /**
   * 表单项名称
   * @type {String}
   * @memberof AppRatingProps
   */
  name: String,

  /**
   * 图标大小
   * @type {String}
   * @memberof AppRatingProps
   */
  size: {
    type: Number,
    default: 20
  },

  /**
   * 是否禁用
   * @type {boolean}
   * @memberof AppRatingProps
   */
  disabled: Boolean,

  /**
   * 只读模式
   * @type {boolean}
   * @memberof AppSlider
   */
  readonly: Boolean,

  /**
   * 最大值
   * @type {string}
   * @memberof AppSlider
   */
  maxValue: {
    type: String,
    default: '5'
  },

  /**
   * 最小值
   * @type {string}
   * @memberof AppSlider
   */
  minValue: {
    type: String,
    default: '0'
  },

  /**
   * 步进值
   * @type {string}
   * @memberof AppSlider
   */
  stepValue: String,

  /**
   * 浮点精度
   * @type {string}
   * @memberof AppSlider
   */
  precision: String
};
const AppRating = (0, _vue.defineComponent)({
  name: 'AppRating',
  props: AppRatingProps,
  emits: ['editorValueChange'],

  data() {
    const starList = [];
    return {
      starList: starList
    };
  },

  methods: {
    /**
     * 值改变事件
     *
     * @param {*} e
     */
    valueChange(value, e) {
      if (Object.is(this.stepValue, '0.5') && 2 * e.offsetX < this.size) {
        this.$emit('editorValueChange', value < Number(this.minValue) ? this.formatValue(Number(this.minValue)) : this.formatValue(value - 0.5));
      } else {
        this.$emit('editorValueChange', value < Number(this.minValue) ? this.formatValue(Number(this.minValue)) : this.formatValue(value));
      }
    },

    formatValue(value) {
      if (this.precision) {
        return value.toFixed(Number(this.precision));
      } else {
        return value.toString();
      }
    }

  },

  mounted() {
    for (let i = 1; i <= Number(this.maxValue); i++) {
      this.starList.push({
        cssName: 'star',
        isSelect: false,
        isHalf: false,
        value: i
      });
    }
  },

  updated() {
    this.starList.forEach((star, index) => {
      star.isSelect = index < Number(this.value) ? true : false;
      star.isHalf = index + 0.5 === Number(this.value) ? true : false;
    });
  },

  render() {
    return (0, _vue.createVNode)("div", {
      "class": ['app-rating', this.disabled ? 'app-rating--disabled' : '']
    }, [this.starList.map(star => {
      return (0, _vue.createVNode)("div", {
        "class": {
          'rating-item': true,
          'rating-item--active': star.isSelect,
          'rating-item--half': star.isHalf
        },
        "style": {
          fontSize: this.size / 50 + 'rem',
          pointerEvents: this.readonly ? 'none' : 'auto'
        }
      }, [(0, _vue.createVNode)((0, _vue.resolveComponent)("app-icon"), {
        "onClick": e => {
          this.valueChange(star.value, e);
        },
        "name": star.cssName
      }, null), star.isHalf ? (0, _vue.createVNode)((0, _vue.resolveComponent)("app-icon"), {
        "iconSrc": 'assets/icon/star-half.svg'
      }, null) : null]);
    })]);
  }

});
exports.AppRating = AppRating;