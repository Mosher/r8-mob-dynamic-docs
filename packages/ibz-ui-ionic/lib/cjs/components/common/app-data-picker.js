"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppDataPicker = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

const AppDataPickerProps = {
  /**
   * 双向绑定值
   * @type {string}
   * @memberof AppDataPickerProps
   */
  value: {
    type: String
  },

  /**
   * 是否禁用
   * @type {boolean}
   * @memberof AppDataPickerProps
   */
  disabled: Boolean,

  /**
   * 上下文
   *
   * @type {Object}
   * @memberof AppDataPickerProps
   */
  context: Object,

  /**
   * 视图参数
   *
   * @type {Object}
   * @memberof AppDataPickerProps
   */
  viewParam: Object,

  /**
   * 应用实体主信息属性名称
   *
   * @type {string}
   * @memberof AppDataPickerProps
   */
  deMajorField: {
    type: String,
    default: 'srfmajortext'
  },

  /**
   * 应用实体主键属性名称
   *
   * @type {string}
   * @memberof AppDataPickerProps
   */
  deKeyField: {
    type: String,
    default: 'srfkey'
  },

  /**
   * 上下文data数据（表单数据）
   *
   * @type {*}
   * @memberof AppDataPickerProps
   */
  contextData: {
    type: Object,
    default: {}
  },

  /**
   * 属性项名称
   *
   * @type {string}
   * @memberof AppDataPickerProps
   */
  name: {
    type: String
  },

  /**
   * 选择视图
   *
   * @type {*}
   * @memberof AppDataPickerProps
   */
  pickUpView: {
    type: Object
  },

  /**
   * 数据链接参数
   *
   * @type {*}
   * @memberof AppDataPickerProps
   */
  linkView: {
    type: Object
  },

  /**
   * 值项名称
   *
   * @type {string}
   * @memberof AppDataPickerProps
   */
  valueItem: {
    type: String
  },

  /**
   * 局部上下文参数
   *
   * @type {Object}
   * @memberof AppDataPickerProps
   */
  localContext: Object,

  /**
   * 局部导航参数
   *
   * @type {Object}
   * @memberof AppDataPickerProps
   */
  localParam: Object,

  /**
   * 下拉视图宽度
   *
   * @type {String}
   * @memberof AppDataPickerProps
   */
  dropdownViewWidght: String,

  /**
   * 下拉视图高度
   *
   * @type {String}
   * @memberof AppDataPickerProps
   */
  dropdownViewHeight: String,

  /**
   * 只读模式
   * @type {boolean}
   * @memberof AppDataPickerProps
   */
  readonly: Boolean,

  /**
   * placeholder值
   * @type {String}
   * @memberof AppDataPickerProps
   */
  placeholder: String,

  /**
   * 模型服务
   * @type {Object}
   * @memberof AppDataPickerProps
   */
  modelService: Object
};
/**
 * 数据选择编辑器
 *
 * @class AppDataPicker
 */

const AppDataPicker = (0, _vue.defineComponent)({
  name: 'AppDataPicker',
  props: AppDataPickerProps,
  emits: ['editorValueChange'],
  computed: {
    /**
     * 当前选中数据集
     *
     * @returns any[]
     * @memberof AppDataPicker
     */
    curValue() {
      var _a, _b;

      let values = [];
      let valueItems = [];
      const curValue = [];

      if (this.name && this.valueItem) {
        values = (_a = this.contextData[this.name]) === null || _a === void 0 ? void 0 : _a.split(',');
        valueItems = (_b = this.contextData[this.valueItem]) === null || _b === void 0 ? void 0 : _b.split(',');
      }

      if ((values === null || values === void 0 ? void 0 : values.length) > 0 && (values === null || values === void 0 ? void 0 : values.length) == (valueItems === null || valueItems === void 0 ? void 0 : valueItems.length)) {
        values.forEach((value, index) => {
          curValue.push({
            [this.deMajorField]: value,
            [this.deKeyField]: valueItems[index]
          });
        });
      }

      return curValue;
    }

  },
  methods: {
    /**
     * 公共参数处理
     *
     * @param {*} arg
     * @memberof AppDataPicker
     */
    handlePublicParams(arg) {
      // 合并表单参数
      arg.viewParam = this.viewParam ? _ibzCore.Util.deepCopy(this.viewParam) : {};
      arg.context = this.context ? _ibzCore.Util.deepCopy(this.context) : {}; // 附加参数处理

      if (this.localContext && Object.keys(this.localContext).length > 0) {
        const _context = _ibzCore.Util.computedNavData(this.contextData, arg.context, arg.viewParam, this.localContext);

        Object.assign(arg.context, _context);
      }

      if (this.localParam && Object.keys(this.localParam).length > 0) {
        const _param = _ibzCore.Util.computedNavData(this.contextData, arg.context, arg.viewParam, this.localParam);

        Object.assign(arg.viewParam, _param);
      }
    },

    /**
     * 打开选择视图
     *
     * @memberof AppDataPicker
     */
    openPickUpView() {
      if (this.disabled || this.readonly) {
        return;
      } // 公共参数处理


      const data = {};
      this.handlePublicParams(data); // 参数处理

      const view = this.pickUpView;

      if (!view) {
        App.getNoticeService().error(`${this.$tl('common.datapicker.nopickupview', '没有配置选择视图')}`);
        return;
      }

      const _context = data.context;
      let _param = data.viewParam;
      _param = Object.assign(_param, {
        selectedData: [...this.curValue]
      }); // 判断打开方式

      if ((view === null || view === void 0 ? void 0 : view.openMode) == 'POPUPMODAL') {
        this.openPopupModal(view, _context, _param);
      } else {
        this.openDrawer(view, _context, _param);
      }
    },

    /**
     * 打开链接视图
     *
     * @memberof AppPicker
     */
    openLinkView() {
      if (!this.contextData || !this.valueItem || !this.contextData[this.valueItem]) {
        return;
      } // 公共参数处理


      const data = {};
      this.handlePublicParams(data); // 参数处理

      const view = this.linkView;

      if (!view) {
        App.getNoticeService().error(`${this.$tl('common.datapicker.nolinkview', '没有配置链接视图')}`);
        return;
      }

      const _context = data.context;
      let _param = data.viewParam;
      Object.assign(_context, {
        [this.deKeyField]: this.contextData[this.valueItem]
      });

      if ((view === null || view === void 0 ? void 0 : view.openMode) == 'POPUPMODAL') {
        this.openPopupModal(view, _context, _param);
      } else {
        this.openDrawer(view, _context, _param);
      }
    },

    /**
     * 模态打开
     *
     * @param view 视图模型
     * @param context 上下文
     * @param viewParam 视图参数
     * @memberof AppDataPicker
     */
    openPopupModal(view, context, viewParam) {
      const customStyle = {};

      if (this.dropdownViewHeight) {
        Object.assign(customStyle, {
          transform: `translateY(calc(100% - ${_ibzCore.Util.calcBoxSize(Number(this.dropdownViewHeight))}))`
        });
      }

      const container = App.getOpenViewService().openModal({
        viewModel: view,
        customStyle: customStyle
      }, context, viewParam);
      container.subscribe(result => {
        if (!result || !Object.is(result.ret, 'OK')) {
          return;
        }

        this.openViewClose(result.datas);
      });
    },

    /**
     * 抽屉打开
     *
     * @param view 视图模型
     * @param context 上下文
     * @param viewParam 视图参数
     * @memberof AppDataPicker
     */
    openDrawer(view, context, viewParam) {
      const customStyle = {};

      if (this.dropdownViewWidght) {
        Object.assign(customStyle, {
          '--width': _ibzCore.Util.calcBoxSize(Number(this.dropdownViewWidght))
        });
      }

      const container = App.getOpenViewService().openDrawer({
        viewModel: view,
        customStyle: customStyle
      }, context, viewParam);
      container.subscribe(result => {
        if (!result || !Object.is(result.ret, 'OK')) {
          return;
        }

        this.openViewClose(result.datas);
      });
    },

    /**
     * 视图关闭
     *
     * @param selects 选中数据集
     * @memberof AppDataPicker
     */
    openViewClose(selects) {
      const values = [];
      const valueItems = [];

      if (selects.length > 0) {
        selects.forEach(select => {
          values.push(select[this.deMajorField]);
          valueItems.push(select[this.deKeyField]);
        });
      }

      if (this.name) {
        this.$emit('editorValueChange', {
          name: this.name,
          value: values.join(',')
        });
      }

      if (this.valueItem) {
        this.$emit('editorValueChange', {
          name: this.valueItem,
          value: valueItems.join(',')
        });
      }
    },

    /**
     * 清空
     *
     * @memberof AppDataPicker
     */
    onClear() {
      if (this.name) {
        this.$emit('editorValueChange', {
          name: this.name,
          value: null
        });
      }

      if (this.valueItem) {
        this.$emit('editorValueChange', {
          name: this.valueItem,
          value: null
        });
      }
    },

    renderICons() {
      var _a;

      if (this.disabled || this.readonly) {
        return null;
      }

      return ((_a = this.curValue) === null || _a === void 0 ? void 0 : _a.length) > 0 ? (0, _vue.createVNode)((0, _vue.resolveComponent)("app-icon"), {
        "name": 'close',
        "onClick": () => (0, _ibzCore.throttle)(this.onClear, [], this)
      }, null) : [(0, _vue.createVNode)((0, _vue.resolveComponent)("app-icon"), {
        "name": 'search',
        "onClick": () => (0, _ibzCore.throttle)(this.openPickUpView, [], this)
      }, null), this.linkView ? (0, _vue.createVNode)((0, _vue.resolveComponent)("app-icon"), {
        "name": 'open',
        "onClick": () => (0, _ibzCore.throttle)(this.openLinkView, [], this)
      }, null) : null];
    }

  },

  render() {
    return (0, _vue.createVNode)("div", {
      "class": 'app-data-picker'
    }, [(0, _vue.createVNode)((0, _vue.resolveComponent)("ion-input"), {
      "readonly": true,
      "value": this.value,
      "disabled": this.disabled,
      "placeholder": this.placeholder,
      "onClick": () => (0, _ibzCore.throttle)(this.openPickUpView, [], this)
    }, null), this.renderICons()]);
  }

});
exports.AppDataPicker = AppDataPicker;