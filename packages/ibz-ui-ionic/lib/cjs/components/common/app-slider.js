"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppSlider = void 0;

var _vue = require("vue");

const AppSliderProps = {
  /**
   * 双向绑定值
   * @type {any}
   * @memberof AppSlider
   */
  value: {
    type: String
  },

  /**
   * 名称
   * @type {any}
   * @memberof AppSlider
   */
  name: String,

  /**
   * 是否禁用
   * @type {boolean}
   * @memberof AppSlider
   */
  disabled: Boolean,

  /**
   * 只读模式
   * @type {boolean}
   * @memberof AppSlider
   */
  readonly: Boolean,

  /**
   * 最大值
   * @type {string}
   * @memberof AppSlider
   */
  maxValue: String,

  /**
   * 最小值
   * @type {string}
   * @memberof AppSlider
   */
  minValue: String,

  /**
   * 步进值
   * @type {string}
   * @memberof AppSlider
   */
  stepValue: String,

  /**
   * 浮点精度
   * @type {string}
   * @memberof AppSlider
   */
  precision: String
};
const AppSlider = (0, _vue.defineComponent)({
  name: 'AppSlider',
  props: AppSliderProps,
  methods: {
    /**
     * 滑动条值改变事件
     *
     * @param {*} e
     */
    valueChange(e) {
      this.$emit('editorValueChange', this.precision ? e.detail.value.toFixed(Number(this.precision)) : e.detail.value.toString());
    }

  },

  render() {
    return (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-range"), {
      "class": 'app-slider',
      "style": {
        'pointer-events': this.readonly ? 'none' : 'auto'
      },
      "value": this.value,
      "min": this.minValue,
      "max": this.maxValue,
      "step": this.stepValue,
      "pin": true,
      "disabled": this.disabled,
      "onIonChange": e => {
        this.valueChange(e);
      }
    }, null);
  }

});
exports.AppSlider = AppSlider;