"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppRadioList = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

function _isSlot(s) {
  return typeof s === 'function' || Object.prototype.toString.call(s) === '[object Object]' && !(0, _vue.isVNode)(s);
}

const AppRadioListProps = {
  name: {
    type: String
  },
  codeList: {
    type: Object
  },
  context: {
    type: Object,
    default: {}
  },
  value: {
    type: String
  },
  disabled: {
    type: Boolean,
    default: false
  },
  readonly: {
    type: Boolean,
    default: false
  },
  modelService: Object
};
const AppRadioList = (0, _vue.defineComponent)({
  name: 'AppRadioList',
  props: AppRadioListProps,
  emits: ['editorValueChange'],

  data() {
    const codeListService = App.getCodeListService();
    return {
      codeListService: codeListService,
      options: []
    };
  },

  created() {
    if (this.codeList) {
      this.loadItems();
    }
  },

  methods: {
    async loadItems() {
      var _a;

      const tag = this.codeList.codeName;
      const type = this.codeList.codeListType;
      (_a = this.codeListService) === null || _a === void 0 ? void 0 : _a.getDataItems({
        tag: tag,
        type: type,
        navContext: this.context
      }).then(codeListItems => {
        this.options = codeListItems;
      }).catch(error => {
        _ibzCore.LogUtil.log(`----${tag}----${this.$tl('common.radiolist.notfount', '未找到')}`);
      });
    },

    valueChange(event) {
      if (event && event.detail && event.detail.value) {
        this.$emit('editorValueChange', event.detail.value.toString());
      }
    }

  },

  render() {
    let _slot;

    return (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-radio-group"), {
      "class": 'app-radio-list',
      "disabled": this.disabled || this.readonly,
      "value": this.value,
      "onIonChange": this.valueChange.bind(this)
    }, _isSlot(_slot = this.options.map((option, index) => {
      return (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-item"), {
        "class": 'radio-list-item',
        "key": index
      }, {
        default: () => [(0, _vue.createVNode)((0, _vue.resolveComponent)("ion-label"), null, {
          default: () => [option.label]
        }), (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-radio"), {
          "name": option.value,
          "value": option.value
        }, null)]
      });
    })) ? _slot : {
      default: () => [_slot]
    });
  }

});
exports.AppRadioList = AppRadioList;