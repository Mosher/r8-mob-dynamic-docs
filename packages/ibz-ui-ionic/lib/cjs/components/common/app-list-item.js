"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppListItemComponent = exports.AppListItem = exports.AppListItemProps = void 0;

var _vue = require("vue");

var _componentBase = require("../component-base");

class AppListItemProps {
  constructor() {
    /**
     * @description 项数据
     * @type {IParam}
     * @memberof AppListItemProps
     */
    this.item = {};
    /**
     * @description 索引
     * @type {number}
     * @memberof AppListItemProps
     */

    this.index = 0;
    /**
     * @description 值格式化
     * @type {string}
     * @memberof AppListItemProps
     */

    this.valueFormat = '';
  }

}

exports.AppListItemProps = AppListItemProps;

class AppListItem extends _componentBase.ComponentBase {
  constructor() {
    super(...arguments);
    /**
     * @description 项数据
     * @type {IParam}
     * @memberof AppListItem
     */

    this.item = {};
    /**
     * @description 索引
     * @type {number}
     * @memberof AppListItem
     */

    this.index = 0;
    /**
     * @description 值格式化
     * @type {string}
     * @memberof AppListItem
     */

    this.valueFormat = '';
  }
  /**
   * @description 设置响应式
   * @memberof AppListItem
   */


  setup() {
    this.initInputData(this.props);
  }
  /**
   * @description 初始化输入属性
   * @param {AppListItemProps} opts 输入对象
   * @memberof AppListItem
   */


  initInputData(opts) {
    this.item = opts.item;
    this.index = opts.index;
    this.valueFormat = opts.valueFormat;
  }
  /**
   * @description 输入属性值变更
   * @memberof AppListItem
   */


  watchEffect() {
    this.initInputData(this.props);
    this.getIndexText();
  }
  /**
   * @description 获取索引样式
   * @memberof AppListItem
   */


  getIndexText() {
    var _a;

    const colorArray = ['#ffa600', '#498cf2', '#f76e9a', '#f56ef7', '#a56ef7'];

    if ((_a = this.item) === null || _a === void 0 ? void 0 : _a.srfmajortext) {
      this.item.indexText = this.item.srfmajortext[0];
    }

    this.item.indexColor = {
      'background-color': colorArray[this.index % colorArray.length]
    };
  }
  /**
   * @description 点击事件
   * @memberof AppListItem
   */


  onClick() {
    this.ctx.emit('itemClick', this.item);
  }
  /**
   * @description 绘制
   * @return {*}
   * @memberof AppListItem
   */


  render() {
    var _a, _b, _c;

    return (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-item"), null, {
      default: () => [(0, _vue.createVNode)("div", {
        "class": 'app-list-item',
        "onClick": () => this.onClick()
      }, [(0, _vue.createVNode)("div", {
        "class": 'list-item-container',
        "style": (_a = this.item) === null || _a === void 0 ? void 0 : _a.indexColor
      }, [(_b = this.item) === null || _b === void 0 ? void 0 : _b.indexText]), (0, _vue.createVNode)("div", {
        "class": 'list-item-title'
      }, [(0, _vue.withDirectives)((0, _vue.createVNode)("span", null, [(_c = this.item) === null || _c === void 0 ? void 0 : _c.srfmajortext]), [[(0, _vue.resolveDirective)("format"), this.valueFormat]])])])]
    });
  }

}

exports.AppListItem = AppListItem;
const AppListItemComponent = (0, _componentBase.GenerateComponent)(AppListItem, Object.keys(new AppListItemProps()));
exports.AppListItemComponent = AppListItemComponent;