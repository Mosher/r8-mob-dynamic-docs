"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppSwitch = void 0;

var _vue = require("vue");

const AppSwitchProps = {
  /**
   * 双向绑定值
   * @type {any}
   * @memberof InputBox
   */
  value: {
    type: String
  },

  /**
   * 是否禁用
   * @type {boolean}
   * @memberof InputBox
   */
  disabled: Boolean,

  /**
   * 只读模式
   *
   * @type {boolean}
   */
  readonly: Boolean
};
const AppSwitch = (0, _vue.defineComponent)({
  name: 'AppSwitch',
  props: AppSwitchProps,
  emits: ['editorValueChange'],
  methods: {
    /**
     * 开关值改变事件
     *
     * @param {*} e
     */
    valueChange(e) {
      this.$emit('editorValueChange', e.detail.checked == true ? '1' : '0');
    }

  },
  computed: {
    curValue() {
      return this.value == '1' ? true : false;
    }

  },

  render() {
    return (0, _vue.createVNode)("div", {
      "class": 'app-switch'
    }, [(0, _vue.createVNode)((0, _vue.resolveComponent)("ion-toggle"), {
      "style": {
        'pointer-events': this.readonly ? 'none' : 'auto'
      },
      "disabled": this.disabled,
      "checked": this.curValue,
      "onIonChange": e => {
        this.valueChange(e);
      }
    }, null)]);
  }

});
exports.AppSwitch = AppSwitch;