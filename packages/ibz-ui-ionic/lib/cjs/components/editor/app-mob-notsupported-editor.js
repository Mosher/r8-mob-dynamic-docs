"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppMobNotSupportedEditorComponent = exports.AppMobNotSupportedEditor = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _editorComponentBase = require("./editor-component-base");

/**
 * 开关编辑器
 *
 * @export
 * @class AppMobNotSupportedEditor
 * @extends {EditorComponentBase}
 */
class AppMobNotSupportedEditor extends _editorComponentBase.EditorComponentBase {
  /**
   * 设置响应式
   *
   * @public
   * @memberof AppMobNotSupportedEditor
   */
  setup() {
    this.c = (0, _vue.shallowReactive)(new _ibzCore.EditorControllerBase(this.props));
    super.setup();
  }
  /**
   * 绘制内容
   *
   * @public
   * @memberof AppMobNotSupportedEditor
   */


  render() {
    if (!this.c.editorIsLoaded) {
      return null;
    }

    const flexStyle = 'width: 100%; height: 100%; overflow: hidden; display: flex;justify-content:center;align-items:center;';
    return (0, _vue.createVNode)("div", {
      "class": 'control-container',
      "style": flexStyle
    }, [(0, _vue.createTextVNode)("\u6682\u672A\u652F\u6301\u7F16\u8F91\u5668")]);
  }

} // Slider组件


exports.AppMobNotSupportedEditor = AppMobNotSupportedEditor;
const AppMobNotSupportedEditorComponent = (0, _componentBase.GenerateComponent)(AppMobNotSupportedEditor, new _ibzCore.AppEditorProps());
exports.AppMobNotSupportedEditorComponent = AppMobNotSupportedEditorComponent;