"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppStepperEditorComponent = exports.AppStepperEditor = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _common = require("../common");

var _componentBase = require("../component-base");

var _editorComponentBase = require("./editor-component-base");

/**
 * 评分编辑器
 *
 * @export
 * @class AppStepperEditor
 * @extends {ComponentBase}
 */
class AppStepperEditor extends _editorComponentBase.EditorComponentBase {
  /**
   * @description 设置响应式
   * @memberof AppStepperEditor
   */
  setup() {
    this.c = (0, _vue.shallowReactive)(this.getEditorControllerByType('MOBSTEPPER'));
    super.setup();
  }
  /**
   * @description 设置编辑器组件
   * @memberof AppStepperEditor
   */


  setEditorComponent() {
    this.editorComponent = _common.AppStepper;
  }
  /**
   * @description 绘制内容
   * @return {*}
   * @memberof AppStepperEditor
   */


  render() {
    if (!this.c.editorIsLoaded) {
      return null;
    }

    return (0, _vue.h)(this.editorComponent, Object.assign(Object.assign({
      value: this.c.value,
      disabled: this.c.disabled
    }, this.c.customProps), {
      onEditorValueChange: $event => {
        this.c.changeValue($event);
      }
    }));
  }

} // Stepper组件


exports.AppStepperEditor = AppStepperEditor;
const AppStepperEditorComponent = (0, _componentBase.GenerateComponent)(AppStepperEditor, new _ibzCore.AppStepperProps());
exports.AppStepperEditorComponent = AppStepperEditorComponent;