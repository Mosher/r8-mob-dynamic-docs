"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.EditorComponentBase = void 0;

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base/component-base");

/**
 * 编辑器组件基类
 *
 * @export
 * @class EditorComponentBase
 * @template Props 输入属性接口
 */
class EditorComponentBase extends _componentBase.ComponentBase {
  /**
   * @description 编辑器输入属性值变更
   * @memberof EditorComponentBase
   */
  watchEffect() {
    this.c.initInputData(this.props);
  }
  /**
   * @description 编辑器钩子
   * @readonly
   * @type {IAppEditorHooks}
   * @memberof EditorComponentBase
   */


  get hooks() {
    return this.c.hooks;
  }
  /**
   * @description 编辑器实例
   * @readonly
   * @memberof EditorComponentBase
   */


  get editorInstance() {
    return this.c.editorInstance;
  }
  /**
   * @description 根据编辑器类型获取编辑器控制器
   * @param {string} type 编辑器类型
   * @return {*}
   * @memberof EditorComponentBase
   */


  getEditorControllerByType(type) {
    switch (type) {
      case 'MOBTEXT':
        return new _ibzCore.MobTextboxEditorController(this.props);

      case 'MOBNUMBER':
        return new _ibzCore.MobTextboxEditorController(this.props);

      case 'MOBTEXTAREA':
        return new _ibzCore.MobTextboxEditorController(this.props);

      case 'MOBPASSWORD':
        return new _ibzCore.MobTextboxEditorController(this.props);

      case 'MOBSWITCH':
        return new _ibzCore.MobSwitchEditorController(this.props);

      case 'MOBSLIDER':
        return new _ibzCore.MobSliderEditorController(this.props);

      case 'MOBRADIOLIST':
        return new _ibzCore.MobCheckBoxEditorController(this.props);

      case 'MOBDROPDOWNLIST':
        return new _ibzCore.MobDropdownListController(this.props);

      case 'MOBCHECKLIST':
        return new _ibzCore.MobDropdownListController(this.props);

      case 'SPAN':
        return new _ibzCore.MobSpanEditorController(this.props);

      case 'MOBDATE':
        return new _ibzCore.MobDatePickerEditorController(this.props);

      case 'MOBRATING':
        return new _ibzCore.MobRatingEditorController(this.props);

      case 'MOBSTEPPER':
        return new _ibzCore.MobStepperEditorController(this.props);

      case 'PICKER':
        return new _ibzCore.MobDataPickerEditorController(this.props);

      case 'MOBPICTURE':
        return new _ibzCore.MobUploadEditorController(this.props);

      case 'MOBPICTURELIST':
        return new _ibzCore.MobUploadEditorController(this.props);

      case 'MOBSINGLEFILEUPLOAD':
        return new _ibzCore.MobUploadEditorController(this.props);

      case 'MOBMULTIFILEUPLOAD':
        return new _ibzCore.MobUploadEditorController(this.props);

      case 'MOBHTMLTEXT':
        return new _ibzCore.MobRichTextEditorController(this.props);

      default:
        _ibzCore.LogUtil.log(`暂未实现${type}类型编辑器`);

        return null;
    }
  }
  /**
   * @description 构建组件
   * @memberof EditorComponentBase
   */


  setup() {
    super.setup();
    this.setEditorComponent();
    this.emitValueChange = this.emitValueChange.bind(this);
    this.hooks.valueChange.tap(this.emitValueChange);
  }
  /**
   * @description 初始化编辑器
   * @memberof EditorComponentBase
   */


  init() {
    super.init();
    this.c.editorInit().then(result => {
      this.emitEditorEvent({
        editorName: this.c.editorInstance.name,
        action: _ibzCore.AppEditorEvents.INITED,
        data: result
      });
    });
  }
  /**
   * @description 部件销毁
   * @memberof EditorComponentBase
   */


  unmounted() {
    this.c.editorDestroy();
    this.hooks.valueChange.removeTap(this.emitValueChange);
  }
  /**
   * @description 设置编辑器组件
   * @memberof EditorComponentBase
   */


  setEditorComponent() {}
  /**
   * @description 抛出编辑器值change事件
   * @param {*} args 参数
   * @memberof EditorComponentBase
   */


  emitValueChange(args) {
    const {
      arg
    } = args;
    this.emitEditorEvent({
      editorName: this.c.editorInstance.name,
      action: _ibzCore.AppEditorEvents.VALUE_CHANGE,
      data: arg
    });
  }
  /**
   * @description 执行编辑器事件
   * @param {IEditorEventParam} args
   * @memberof EditorComponentBase
   */


  emitEditorEvent(args) {
    this.ctx.emit('editorEvent', args);
  }

}

exports.EditorComponentBase = EditorComponentBase;