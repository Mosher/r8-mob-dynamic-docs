"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppSwitchEditorComponent = exports.AppSwitchEditor = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _common = require("../common");

var _componentBase = require("../component-base");

var _editorComponentBase = require("./editor-component-base");

/**
 * 开关编辑器
 *
 * @export
 * @class AppSwitchEditor
 * @extends {ComponentBase}
 */
class AppSwitchEditor extends _editorComponentBase.EditorComponentBase {
  /**
   * @description 设置响应式
   * @memberof AppSwitchEditor
   */
  setup() {
    this.c = (0, _vue.shallowReactive)(this.getEditorControllerByType('MOBSWITCH'));
    super.setup();
  }
  /**
   * @description 设置编辑器组件
   * @memberof AppTextboxEditor
   */


  setEditorComponent() {
    this.editorComponent = _common.AppSwitch;
  }
  /**
   * @description 绘制内容
   * @return {*}
   * @memberof AppSwitchEditor
   */


  render() {
    if (!this.c.editorIsLoaded) {
      return null;
    }

    return (0, _vue.h)(this.editorComponent, Object.assign(Object.assign({
      value: this.c.value,
      disabled: this.c.disabled
    }, this.c.customProps), {
      onEditorValueChange: $event => {
        this.c.changeValue($event);
      }
    }));
  }

} // Switch组件


exports.AppSwitchEditor = AppSwitchEditor;
const AppSwitchEditorComponent = (0, _componentBase.GenerateComponent)(AppSwitchEditor, new _ibzCore.AppSwitchProps());
exports.AppSwitchEditorComponent = AppSwitchEditorComponent;