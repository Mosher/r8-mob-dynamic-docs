"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppRichTextEditorComponent = exports.AppRichTextEditor = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _common = require("../common");

var _componentBase = require("../component-base");

var _editorComponentBase = require("./editor-component-base");

/**
 * @description 富文本编辑器
 * @export
 * @class AppRichTextEditor
 * @extends {EditorComponentBase<AppRichTextProps>}
 */
class AppRichTextEditor extends _editorComponentBase.EditorComponentBase {
  /**
   * @description 设置响应式
   * @memberof AppRichTextEditor
   */
  setup() {
    this.c = (0, _vue.shallowReactive)(this.getEditorControllerByType('MOBHTMLTEXT'));
    super.setup();
  }
  /**
   * @description 设置编辑器组件
   * @memberof AppRichTextEditor
   */


  setEditorComponent() {
    this.editorComponent = _common.AppRichText;
  }
  /**
   * @description 绘制内容
   * @return {*}
   * @memberof AppRichTextEditor
   */


  render() {
    if (!this.c.editorIsLoaded) {
      return null;
    }

    return (0, _vue.h)(this.editorComponent, Object.assign(Object.assign({
      value: this.c.value,
      disabled: this.c.disabled
    }, this.c.customProps), {
      onEditorValueChange: $event => {
        this.c.changeValue($event);
      }
    }));
  }

} // AppRichText组件


exports.AppRichTextEditor = AppRichTextEditor;
const AppRichTextEditorComponent = (0, _componentBase.GenerateComponent)(AppRichTextEditor, new _ibzCore.AppRichTextProps());
exports.AppRichTextEditorComponent = AppRichTextEditorComponent;