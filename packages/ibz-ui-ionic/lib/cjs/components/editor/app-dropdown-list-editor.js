"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppDropdownListEditorComponent = exports.AppDropdownListEditor = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _common = require("../common");

var _componentBase = require("../component-base");

var _editorComponentBase = require("./editor-component-base");

/**
 * 时间选择编辑器
 *
 * @export
 * @class AppDropdownListEditor
 * @extends {ComponentBase}
 */
class AppDropdownListEditor extends _editorComponentBase.EditorComponentBase {
  /**
   * @description 设置响应式
   * @memberof AppDropdownListEditor
   */
  setup() {
    this.c = (0, _vue.shallowReactive)(this.getEditorControllerByType('MOBDROPDOWNLIST'));
    super.setup();
  }
  /**
   * @description 设置编辑器组件
   * @memberof AppDropdownListEditor
   */


  setEditorComponent() {
    this.editorComponent = _common.AppDropdownList;
  }
  /**
   * @description 绘制内容
   * @return {*}
   * @memberof AppDropdownListEditor
   */


  render() {
    if (!this.c.editorIsLoaded) {
      return null;
    }

    return (0, _vue.h)(this.editorComponent, Object.assign(Object.assign({
      value: this.c.value,
      disabled: this.c.disabled
    }, this.c.customProps), {
      onEditorValueChange: $event => {
        this.c.changeValue($event);
      }
    }));
  }

} // 下拉列表组件


exports.AppDropdownListEditor = AppDropdownListEditor;
const AppDropdownListEditorComponent = (0, _componentBase.GenerateComponent)(AppDropdownListEditor, new _ibzCore.AppDropdownListProps());
exports.AppDropdownListEditorComponent = AppDropdownListEditorComponent;