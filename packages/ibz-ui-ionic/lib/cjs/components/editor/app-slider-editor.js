"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppSliderEditorComponent = exports.AppSliderEditor = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _common = require("../common");

var _componentBase = require("../component-base");

var _editorComponentBase = require("./editor-component-base");

/**
 * 开关编辑器
 *
 * @export
 * @class AppSliderEditor
 * @extends {ComponentBase}
 */
class AppSliderEditor extends _editorComponentBase.EditorComponentBase {
  /**
   * @description 设置响应式
   * @memberof AppSliderEditor
   */
  setup() {
    this.c = (0, _vue.shallowReactive)(this.getEditorControllerByType('MOBSLIDER'));
    super.setup();
  }
  /**
   * @description 设置编辑器组件
   * @memberof AppSliderEditor
   */


  setEditorComponent() {
    this.editorComponent = _common.AppSlider;
  }
  /**
   * @description 绘制内容
   * @return {*}
   * @memberof AppSliderEditor
   */


  render() {
    if (!this.c.editorIsLoaded) {
      return null;
    }

    return (0, _vue.h)(this.editorComponent, Object.assign(Object.assign({
      value: this.c.value,
      disabled: this.c.disabled
    }, this.c.customProps), {
      onEditorValueChange: $event => {
        this.c.changeValue($event);
      }
    }));
  }

} // Slider组件


exports.AppSliderEditor = AppSliderEditor;
const AppSliderEditorComponent = (0, _componentBase.GenerateComponent)(AppSliderEditor, new _ibzCore.AppSliderProps());
exports.AppSliderEditorComponent = AppSliderEditorComponent;