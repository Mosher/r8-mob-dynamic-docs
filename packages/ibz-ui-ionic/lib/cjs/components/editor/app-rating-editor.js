"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppRatingEditorComponent = exports.AppRatingEditor = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _common = require("../common");

var _componentBase = require("../component-base");

var _editorComponentBase = require("./editor-component-base");

/**
 * 评分编辑器
 *
 * @export
 * @class AppRatingEditor
 * @extends {ComponentBase}
 */
class AppRatingEditor extends _editorComponentBase.EditorComponentBase {
  /**
   * @description 设置响应式
   * @memberof AppRatingEditor
   */
  setup() {
    this.c = (0, _vue.shallowReactive)(this.getEditorControllerByType('MOBRATING'));
    super.setup();
  }
  /**
   * @description 设置编辑器组件
   * @memberof AppRatingEditor
   */


  setEditorComponent() {
    this.editorComponent = _common.AppRating;
  }
  /**
   * @description 绘制内容
   * @return {*}
   * @memberof AppRatingEditor
   */


  render() {
    if (!this.c.editorIsLoaded) {
      return null;
    }

    return (0, _vue.h)(this.editorComponent, Object.assign(Object.assign({
      value: this.c.value,
      disabled: this.c.disabled
    }, this.c.customProps), {
      onEditorValueChange: $event => {
        this.c.changeValue($event);
      }
    }));
  }

} // Rate组件


exports.AppRatingEditor = AppRatingEditor;
const AppRatingEditorComponent = (0, _componentBase.GenerateComponent)(AppRatingEditor, new _ibzCore.AppRatingProps());
exports.AppRatingEditorComponent = AppRatingEditorComponent;