"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppUploadEditorComponent = exports.AppUploadEditor = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _common = require("../common");

var _componentBase = require("../component-base");

var _editorComponentBase = require("./editor-component-base");

/**
 * 上传组件编辑器
 *
 * @export
 * @class AppUploadEditor
 * @extends {ComponentBase}
 */
class AppUploadEditor extends _editorComponentBase.EditorComponentBase {
  /**
   * @description 设置响应式
   * @memberof AppUploadEditor
   */
  setup() {
    this.c = (0, _vue.shallowReactive)(this.getEditorControllerByType('MOBPICTURE'));
    super.setup();
  }
  /**
   * @description 设置编辑器组件
   * @memberof AppUploadEditor
   */


  setEditorComponent() {
    const {
      editorType: type,
      editorStyle: style
    } = this.editorInstance;
    const editorTypeStyle = `${type}${style && style != 'DEFAULT' ? '_' + style : ''}`;

    switch (editorTypeStyle) {
      case 'MOBPICTURE':
      case 'MOBSINGLEFILEUPLOAD':
      case 'MOBPICTURELIST':
      case 'MOBMULTIFILEUPLOAD':
        this.editorComponent = _common.AppUpload;
        break;

      case 'MOBPICTURE_DZQM':
        this.editorComponent = _common.AppSignature;
        break;
    }
  }
  /**
   * @description 绘制内容
   * @return {*}
   * @memberof AppUploadEditor
   */


  render() {
    if (!this.c.editorIsLoaded) {
      return null;
    }

    return (0, _vue.h)(this.editorComponent, Object.assign(Object.assign({
      value: this.c.value,
      disabled: this.c.disabled
    }, this.c.customProps), {
      onEditorValueChange: $event => {
        this.c.changeValue($event);
      }
    }));
  }

} // FileUpload组件


exports.AppUploadEditor = AppUploadEditor;
const AppUploadEditorComponent = (0, _componentBase.GenerateComponent)(AppUploadEditor, new _ibzCore.AppUploadProps());
exports.AppUploadEditorComponent = AppUploadEditorComponent;