"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppDataPickerEditorComponent = exports.AppDataPickerEditor = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _common = require("../common");

var _componentBase = require("../component-base");

var _editorComponentBase = require("./editor-component-base");

/**
 * 时间选择编辑器
 *
 * @export
 * @class AppDataPickerEditor
 * @extends {ComponentBase}
 */
class AppDataPickerEditor extends _editorComponentBase.EditorComponentBase {
  /**
   * @description 设置响应式
   * @memberof AppDataPickerEditor
   */
  setup() {
    this.c = (0, _vue.shallowReactive)(this.getEditorControllerByType('PICKER'));
    super.setup();
  }
  /**
   * @description 设置编辑器组件
   * @memberof AppDataPickerEditor
   */


  setEditorComponent() {
    this.editorComponent = _common.AppDataPicker;
  }
  /**
   * @description 绘制内容
   * @return {*}
   * @memberof AppDataPickerEditor
   */


  render() {
    if (!this.c.editorIsLoaded) {
      return null;
    }

    return (0, _vue.h)(this.editorComponent, Object.assign(Object.assign({
      value: this.c.value,
      disabled: this.c.disabled
    }, this.c.customProps), {
      onEditorValueChange: $event => {
        this.c.changeValue($event);
      }
    }));
  }

} // 数据选择组件


exports.AppDataPickerEditor = AppDataPickerEditor;
const AppDataPickerEditorComponent = (0, _componentBase.GenerateComponent)(AppDataPickerEditor, new _ibzCore.AppDataPickerProps());
exports.AppDataPickerEditorComponent = AppDataPickerEditorComponent;