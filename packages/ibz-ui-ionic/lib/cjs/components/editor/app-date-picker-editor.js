"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppDatePickerEditorComponent = exports.AppDatePickerEditor = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _common = require("../common");

var _componentBase = require("../component-base");

var _editorComponentBase = require("./editor-component-base");

/**
 * 时间选择编辑器
 *
 * @export
 * @class AppDatePickerEditor
 * @extends {ComponentBase}
 */
class AppDatePickerEditor extends _editorComponentBase.EditorComponentBase {
  /**
   * @description 设置响应式
   * @memberof AppDatePickerEditor
   */
  setup() {
    this.c = (0, _vue.shallowReactive)(this.getEditorControllerByType('MOBDATE'));
    super.setup();
  }
  /**
   * @description 设置编辑器组件
   * @memberof AppDatePickerEditor
   */


  setEditorComponent() {
    this.editorComponent = _common.AppDatePicker;
  }
  /**
   * @description 绘制内容
   * @return {*}
   * @memberof AppDatePickerEditor
   */


  render() {
    if (!this.c.editorIsLoaded) {
      return null;
    }

    return (0, _vue.h)(this.editorComponent, Object.assign(Object.assign({
      value: this.c.value,
      disabled: this.c.disabled
    }, this.c.customProps), {
      onEditorValueChange: $event => {
        this.c.changeValue($event);
      }
    }));
  }

} // 时间选择组件


exports.AppDatePickerEditor = AppDatePickerEditor;
const AppDatePickerEditorComponent = (0, _componentBase.GenerateComponent)(AppDatePickerEditor, new _ibzCore.AppDatePickerProps());
exports.AppDatePickerEditorComponent = AppDatePickerEditorComponent;