"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppSpanEditorComponent = exports.AppSpanEditor = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _common = require("../common");

var _componentBase = require("../component-base");

var _editorComponentBase = require("./editor-component-base");

/**
 * 标签编辑器
 *
 * @export
 * @class AppSpanEditor
 * @extends {ComponentBase}
 */
class AppSpanEditor extends _editorComponentBase.EditorComponentBase {
  /**
   * @description 设置响应式
   * @memberof AppSpanEditor
   */
  setup() {
    this.c = (0, _vue.shallowReactive)(this.getEditorControllerByType('SPAN'));
    super.setup();
  }
  /**
   * @description 设置编辑器组件
   * @memberof AppSpanEditor
   */


  setEditorComponent() {
    this.editorComponent = _common.AppSpan;
  }
  /**
   * @description 绘制内容
   * @return {*}
   * @memberof AppSpanEditor
   */


  render() {
    if (!this.c.editorIsLoaded) {
      return null;
    }

    return (0, _vue.h)(this.editorComponent, Object.assign(Object.assign({
      value: this.c.value,
      disabled: this.c.disabled
    }, this.c.customProps), {
      onEditorValueChange: $event => {
        this.c.changeValue($event);
      }
    }));
  }

} // 标签组件


exports.AppSpanEditor = AppSpanEditor;
const AppSpanEditorComponent = (0, _componentBase.GenerateComponent)(AppSpanEditor, new _ibzCore.AppSpanProps());
exports.AppSpanEditorComponent = AppSpanEditorComponent;