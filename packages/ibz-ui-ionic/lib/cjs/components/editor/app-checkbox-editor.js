"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppCheckBoxEditorComponent = exports.AppCheckBoxEditor = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _common = require("../common");

var _componentBase = require("../component-base");

var _editorComponentBase = require("./editor-component-base");

class AppCheckBoxEditor extends _editorComponentBase.EditorComponentBase {
  /**
   * @description 设置响应式
   * @memberof AppCheckBoxEditor
   */
  setup() {
    this.c = (0, _vue.shallowReactive)(this.getEditorControllerByType('MOBRADIOLIST'));
    super.setup();
  }
  /**
   * @description 设置编辑器组件
   * @memberof AppCheckBoxEditor
   */


  setEditorComponent() {
    this.editorComponent = _common.AppRadioList;
  }
  /**
   * @description 绘制内容
   * @return {*}
   * @memberof AppCheckBoxEditor
   */


  render() {
    if (!this.c.editorIsLoaded) {
      return null;
    }

    return (0, _vue.h)(this.editorComponent, Object.assign(Object.assign({
      value: this.c.value,
      disabled: this.c.disabled
    }, this.c.customProps), {
      onEditorValueChange: $event => {
        this.c.changeValue($event);
      }
    }));
  }

} // CheckBox组件


exports.AppCheckBoxEditor = AppCheckBoxEditor;
const AppCheckBoxEditorComponent = (0, _componentBase.GenerateComponent)(AppCheckBoxEditor, new _ibzCore.AppCheckBoxProps());
exports.AppCheckBoxEditorComponent = AppCheckBoxEditorComponent;