"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppTextboxEditorComponent = exports.AppTextboxEditor = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _common = require("../common");

var _componentBase = require("../component-base");

var _editorComponentBase = require("./editor-component-base");

/**
 * 文本框编辑器
 *
 * @export
 * @class AppTextboxEditor
 * @extends {ComponentBase}
 */
class AppTextboxEditor extends _editorComponentBase.EditorComponentBase {
  /**
   * @description 设置响应式
   * @memberof AppTextboxEditor
   */
  setup() {
    this.c = (0, _vue.shallowReactive)(this.getEditorControllerByType('MOBTEXT'));
    super.setup();
  }
  /**
   * @description 设置编辑器组件
   * @memberof AppTextboxEditor
   */


  setEditorComponent() {
    const type = this.editorInstance.editorType;

    switch (type) {
      case 'MOBTEXT':
      case 'MOBNUMBER':
      case 'MOBPASSWORD':
        this.editorComponent = _common.AppInput;
        break;

      case 'MOBTEXTAREA':
        this.editorComponent = _common.AppTextArea;
        break;

      default:
        this.editorComponent = _common.AppInput;
        break;
    }
  }
  /**
   * @description 绘制内容
   * @return {*}
   * @memberof AppTextboxEditor
   */


  render() {
    if (!this.c.editorIsLoaded) {
      return null;
    }

    return (0, _vue.h)(this.editorComponent, Object.assign(Object.assign({
      value: this.c.value,
      disabled: this.c.disabled
    }, this.c.customProps), {
      onEditorValueChange: $event => {
        this.c.changeValue($event);
      }
    }));
  }

} // Textbox组件


exports.AppTextboxEditor = AppTextboxEditor;
const AppTextboxEditorComponent = (0, _componentBase.GenerateComponent)(AppTextboxEditor, new _ibzCore.AppTextboxProps());
exports.AppTextboxEditorComponent = AppTextboxEditorComponent;