"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppFormPageComponent = exports.default = void 0;

var _vue = require("vue");

var _rxjs = require("rxjs");

var _componentBase = require("../../component-base");

/**
 * 表单分页组件输入参数
 *
 * @class AppFormPageProps
 */
class AppFormPageProps {
  constructor() {
    /**
     * 表单分页控制器
     *
     * @type {FormButtonController}
     * @memberof AppFormPageProps
     */
    this.c = {};
    /**
     * @description 部件name
     * @type {string}
     * @memberof AppFormPageProps
     */

    this.name = '';
    /**
     * @description 表单数据
     * @type {IParam}
     * @memberof AppFormPageProps
     */

    this.data = {};
    /**
     * 表单状态
     *
     * @type {Subject<IViewStateParam>}
     * @memberof AppFormPageProps
     */

    this.formState = null;
    /**
     * 当前分页
     *
     * @type {string}
     * @memberof AppFormPageProps
     */

    this.currentPage = '';
    /**
     * 模型服务
     *
     * @type {IParam}
     * @memberof AppFormPageProps
     */

    this.modelService = {};
  }

}
/**
 * 表单分页
 *
 * @class AppFormPage
 */


class AppFormPage extends _componentBase.ComponentBase {
  constructor() {
    super(...arguments);
    /**
     * @description 部件name
     * @type {string}
     * @memberof AppFormPage
     */

    this.name = '';
    /**
     * @description 表单状态
     * @type {Subject<IViewStateParam>}
     * @memberof AppFormPage
     */

    this.formState = new _rxjs.Subject();
    /**
     * @description 表单旧数据
     * @type {IParam}
     * @memberof AppFormPage
     */

    this.oldFormData = {};
    /**
     * @description 表单数据
     * @type {IParam}
     * @memberof AppFormPage
     */

    this.data = {};
    /**
     * @description 当前页
     * @type {IParam}
     * @memberof AppFormPage
     */

    this.currentPage = (0, _vue.ref)('');
  }
  /**
   * 设置响应式
   *
   * @memberof AppFormPage
   */


  setup() {
    this.c = this.props.c;
    this.currentPage = (0, _vue.shallowRef)(this.props.currentPage);
    this.name = this.props.name;
  }
  /**
   * 初始化
   *
   * @memberof AppFormPage
   */


  init() {
    this.c.initInputData(this.props);
  }

  renderContent() {
    var _a;

    const {
      noTabHeader
    } = this.c.parentController.controlInstance;
    const {
      codeName
    } = this.c.model;
    const customClass = (_a = this.c.model.getPSSysCss()) === null || _a === void 0 ? void 0 : _a.cssName;

    if (noTabHeader) {
      return (0, _vue.createVNode)("div", {
        "class": [`${this.name}-page`, customClass]
      }, [(0, _vue.renderSlot)(this.ctx.slots, 'default')]);
    } else {
      const customStyle = this.c.visible && this.currentPage.value == codeName ? '' : 'display: none;';
      return (0, _vue.createVNode)("div", {
        "class": [`${this.name}-page`, customClass],
        "style": customStyle
      }, [(0, _vue.renderSlot)(this.ctx.slots, 'default')]);
    }
  }

  render() {
    var _a, _b;

    const plugin = (_b = (_a = this.c.model).getPSSysPFPlugin) === null || _b === void 0 ? void 0 : _b.call(_a);

    if (plugin && plugin.pluginCode) {
      const ctrlItemPluginInstance = App.getPluginService().getCtrlItemPluginByTag(plugin.pluginCode || '');

      if (ctrlItemPluginInstance) {
        return ctrlItemPluginInstance.renderItem(this.c, this.c.parentController, this);
      }
    } else {
      return this.renderContent();
    }
  }

} //  表单分页组件


exports.default = AppFormPage;
const AppFormPageComponent = (0, _componentBase.GenerateComponent)(AppFormPage, Object.keys(new AppFormPageProps()));
exports.AppFormPageComponent = AppFormPageComponent;