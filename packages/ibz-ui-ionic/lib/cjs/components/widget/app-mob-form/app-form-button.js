"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppFormButtonComponent = exports.AppFormButton = void 0;

var _vue = require("vue");

var _rxjs = require("rxjs");

var _componentBase = require("../../component-base");

class AppFormButtonProps {
  constructor() {
    /**
     * @description 表单按钮控制器实例对象
     * @type {IParam}
     * @memberof AppFormButtonProps
     */
    this.c = {};
    /**
     * @description 名称
     * @type {string}
     * @memberof AppFormButtonProps
     */

    this.name = '';
    /**
     * @description 表单数据
     * @type {IParam}
     * @memberof AppFormButtonProps
     */

    this.data = {};
    /**
     * @description 表单状态
     * @type {(Subject<IViewStateParam> | null)}
     * @memberof AppFormButtonProps
     */

    this.formState = null;
    /**
     * 模型服务
     *
     * @type {IParam}
     * @memberof AppFormButtonProps
     */

    this.modelService = {};
  }

}
/**
 * 表单按钮
 */


class AppFormButton extends _componentBase.ComponentBase {
  constructor() {
    super(...arguments);
    /**
     * @description 名称
     * @type {string}
     * @memberof AppFormButton
     */

    this.name = '';
    /**
     * @description 表单状态
     * @type {Subject<IViewStateParam>}
     * @memberof AppFormButton
     */

    this.formState = new _rxjs.Subject();
    /**
     * @description 表单旧数据
     * @type {IParam}
     * @memberof AppFormButton
     */

    this.oldFormData = {};
    /**
     * @description 表单数据
     * @type {IParam}
     * @memberof AppFormButton
     */

    this.data = {};
  }
  /**
   * @description 设置响应式
   * @memberof AppFormButton
   */


  setup() {
    this.c = this.props.c;
  }
  /**
   * @description 初始化
   * @memberof AppFormButton
   */


  init() {
    this.c.initInputData(this.props);
  }
  /**
   * @description 表单按钮点击
   * @param event 源事件
   */


  buttonClick(event) {
    this.c.buttonClick(event);
  }
  /**
   * @description 渲染表单按钮
   * @memberof AppFormButton
   */


  render() {
    var _a, _b, _c;

    const {
      width,
      height,
      showCaption,
      caption
    } = this.c.model;
    const sysImage = this.c.model.getPSSysImage();
    const sysCss = this.c.model.getPSSysCss();
    const captionClass = (_a = this.c.model.getLabelPSSysCss()) === null || _a === void 0 ? void 0 : _a.cssName;
    const btnClass = width > 0 && height > 0 ? {
      width: `${width}px`,
      height: `${height}px`
    } : ''; // 自定义类名

    const controlClassNames = {
      'form-button': true
    };

    if (sysCss === null || sysCss === void 0 ? void 0 : sysCss.cssName) {
      Object.assign(controlClassNames, {
        [sysCss.cssName]: true
      });
    }

    return (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-button"), {
      "class": controlClassNames,
      "style": btnClass,
      "onClick": $event => this.buttonClick($event),
      "disabled": this.c.disabled
    }, {
      default: () => [sysImage && (0, _vue.createVNode)((0, _vue.resolveComponent)("app-icon"), {
        "icon": sysImage
      }, null), showCaption && (0, _vue.createVNode)("span", {
        "class": captionClass
      }, [this.$tl((_c = (_b = this.c.model) === null || _b === void 0 ? void 0 : _b.getCapPSLanguageRes()) === null || _c === void 0 ? void 0 : _c.lanResTag, caption)])]
    });
  }

} //  表单按钮组件


exports.AppFormButton = AppFormButton;
const AppFormButtonComponent = (0, _componentBase.GenerateComponent)(AppFormButton, Object.keys(new AppFormButtonProps()));
exports.AppFormButtonComponent = AppFormButtonComponent;