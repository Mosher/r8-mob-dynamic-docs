"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppMobMEditViewPanelComponent = exports.AppMobMEditViewPanel = void 0;

var _vue = require("vue");

var _mdCtrlComponentBase = require("./md-ctrl-component-base");

var _componentBase = require("../component-base");

var _ibzCore = require("ibz-core");

function _isSlot(s) {
  return typeof s === 'function' || Object.prototype.toString.call(s) === '[object Object]' && !(0, _vue.isVNode)(s);
}

class AppMobMEditViewPanel extends _mdCtrlComponentBase.MDCtrlComponentBase {
  constructor() {
    super(...arguments);
    /**
     * @description 激活项
     * @private
     * @type {Ref<string>}
     * @memberof AppMobMEditViewPanel
     */

    this.activeItem = (0, _vue.ref)('');
  }
  /**
   * @description 设置响应式
   * @memberof AppMobMEditViewPanel
   */


  setup() {
    this.c = (0, _vue.shallowReactive)(this.getCtrlControllerByType('MULTIEDITVIEWPANEL'));
    super.setup();
  }
  /**
   * @description 分页节点切换
   * @private
   * @param {*} $event 数据源
   * @return {*}
   * @memberof AppMobMEditViewPanel
   */


  ionChange($event) {
    const {
      detail: _detail
    } = $event;

    if (!_detail) {
      return;
    }

    const {
      value: _value
    } = _detail;

    if (_value) {
      this.activeItem.value = _value;
    }
  }
  /**
   * @description 处理视图事件
   * @private
   * @param {string} viewname 视图名
   * @param {string} action 行为
   * @param {*} data 数据
   * @memberof AppMobMEditViewPanel
   */


  handleViewEvent(viewname, action, data) {
    switch (action) {
      case _ibzCore.AppViewEvents.DATA_CHANGE:
        this.c.viewDataChange(data);
        break;
    }
  }
  /**
   * @description 绘制嵌入视图
   * @param {*} item 项数据
   * @return {*}
   * @memberof AppMobMEditViewPanel
   */


  renderEmbedView(item) {
    var _a;

    const embedView = this.c.controlInstance.getEmbeddedPSAppView();
    const viewComponent = App.getComponentService().getViewTypeComponent(embedView.viewType, embedView.viewStyle, (_a = embedView.getPSSysPFPlugin()) === null || _a === void 0 ? void 0 : _a.pluginCode);

    if (viewComponent) {
      return (0, _vue.h)(viewComponent, {
        key: item.id,
        viewPath: embedView.modelPath,
        navContext: item.context,
        navParam: item.viewParam,
        navDatas: this.c.navDatas,
        viewState: this.c.viewState,
        isShowCaptionBar: false,
        viewShowMode: 'EMBEDDED',
        onViewEvent: ({
          viewname,
          action,
          data
        }) => {
          this.handleViewEvent(viewname, action, data);
        }
      });
    } else {
      return (0, _vue.createVNode)("div", {
        "class": 'no-embedded-view'
      }, [this.$tl('widget.mobmeditviewpanel.noembeddedview', '无嵌入视图')]);
    }
  }
  /**
   * @description 绘制行记录样式
   * @return {*}
   * @memberof AppMobMEditViewPanel
   */


  renderRow() {
    return this.c.items.map(item => {
      let _slot;

      return [(0, _vue.createVNode)("div", {
        "class": 'multieditviewpanel-row'
      }, [(0, _vue.createVNode)((0, _vue.resolveComponent)("ion-card"), null, {
        default: () => [(0, _vue.createVNode)((0, _vue.resolveComponent)("ion-card-content"), null, _isSlot(_slot = this.renderEmbedView(item)) ? _slot : {
          default: () => [_slot]
        })]
      })])];
    });
  }
  /**
   * @description 绘制上分页样式
   * @return {*}
   * @memberof AppMobMEditViewPanel
   */


  renderTabtop() {
    return (0, _vue.createVNode)("div", {
      "class": 'multieditviewpanel-tabtop'
    }, [this.renderPageHead(), this.renderTabTopContent()]);
  }
  /**
   * @description 绘制分页头
   * @return {*}
   * @memberof AppMobMEditViewPanel
   */


  renderPageHead() {
    let _slot2;

    if (!this.activeItem.value) {
      this.activeItem.value = this.c.items[0].id;
    }

    return (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-segment"), {
      "scrollable": true,
      "value": this.activeItem.value,
      "onIonChange": $event => this.ionChange($event)
    }, _isSlot(_slot2 = this.c.items.map(item => {
      return (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-segment-button"), {
        "value": item.id
      }, {
        default: () => [(0, _vue.createVNode)((0, _vue.resolveComponent)("ion-label"), null, {
          default: () => [item.srfmajortext]
        })]
      });
    })) ? _slot2 : {
      default: () => [_slot2]
    });
  }
  /**
   * @description 绘制上分页内容区
   * @return {*}
   * @memberof AppMobMEditViewPanel
   */


  renderTabTopContent() {
    return (0, _vue.createVNode)("div", {
      "class": 'tabtop-content'
    }, [this.c.items.map(item => {
      if (item.id == this.activeItem.value) {
        return this.renderEmbedView(item);
      }
    })]);
  }
  /**
   * @description 绘制内容区
   * @return {*}
   * @memberof AppMobMEditViewPanel
   */


  renderContent() {
    var _a;

    if (((_a = this.c.items) === null || _a === void 0 ? void 0 : _a.length) > 0) {
      if (Object.is('ROW', this.c.panelStyle)) {
        return this.renderRow();
      } else {
        return this.renderTabtop();
      }
    }
  }
  /**
   * @description 绘制多表单编辑面板
   * @return {*}
   * @memberof AppMobMEditViewPanel
   */


  render() {
    if (!this.controlIsLoaded.value) {
      return null;
    }

    const {
      width,
      height
    } = this.c.controlInstance;
    const controlstyle = {
      width: width ? `${width}px` : '100%',
      height: height ? `${height}px` : '100%'
    };
    return (0, _vue.createVNode)("div", {
      "class": Object.assign({}, this.classNames),
      "style": controlstyle
    }, [this.renderPullDownRefresh(), this.renderContent(), (0, _vue.createVNode)("div", {
      "class": 'meditviewpanel-add-icon'
    }, [(0, _vue.createVNode)((0, _vue.resolveComponent)("app-icon"), {
      "onClick": () => this.c.add(),
      "name": 'add'
    }, null)])]);
  }

} // 多表单编辑视图面板


exports.AppMobMEditViewPanel = AppMobMEditViewPanel;
const AppMobMEditViewPanelComponent = (0, _componentBase.GenerateComponent)(AppMobMEditViewPanel, Object.keys(new _ibzCore.AppMobMEditViewPanelProps()));
exports.AppMobMEditViewPanelComponent = AppMobMEditViewPanelComponent;