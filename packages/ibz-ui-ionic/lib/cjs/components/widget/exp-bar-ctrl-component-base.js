"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ExpBarCtrlComponentBase = void 0;

var _vue = require("vue");

var _ctrlComponentBase = require("./ctrl-component-base");

class ExpBarCtrlComponentBase extends _ctrlComponentBase.CtrlComponentBase {
  /**
   * @description 初始化响应式属性
   * @memberof ExpBarCtrlComponentBase
   */
  initReactive() {
    super.initReactive();
    this.c.selection = (0, _vue.reactive)(this.c.selection);
  }
  /**
   * @description 渲染多数据部件
   * @return {*}
   * @memberof ExpBarCtrlComponentBase
   */


  renderXDataControl() {
    if (this.c.xDataControl) {
      const otherParams = {
        expMode: true
      };
      return this.computeTargetCtrlData(this.c.xDataControl, otherParams);
    }
  }
  /**
   * @description 渲染导航视图
   * @return {*}
   * @memberof ExpBarCtrlComponentBase
   */


  renderNavView() {
    if (this.c.selection) {
      const {
        viewComponent,
        navContext,
        navParam
      } = this.c.selection;

      if (viewComponent) {
        return (0, _vue.h)(viewComponent, {
          navContext: navContext,
          navParam: navParam,
          viewPath: navContext === null || navContext === void 0 ? void 0 : navContext.viewpath
        });
      }
    }
  }
  /**
   * @description 渲染导航栏部件
   * @return {*}
   * @memberof ExpBarCtrlComponentBase
   */


  render() {
    if (!this.controlIsLoaded.value) {
      return null;
    }

    const {
      width,
      height
    } = this.c.controlInstance;
    const controlStyle = {
      width: width ? width + 'px' : '',
      height: height ? height + 'px' : ''
    };
    return (0, _vue.createVNode)("div", {
      "class": Object.assign({
        'exp-bar': true
      }, this.classNames),
      "style": controlStyle
    }, [(0, _vue.createVNode)("div", {
      "class": 'exp-bar-container'
    }, [(0, _vue.createVNode)("div", {
      "class": "container__multi_data_ctrl"
    }, [this.renderXDataControl()]), (0, _vue.createVNode)("div", {
      "class": "container__nav_view"
    }, [this.renderNavView()])])]);
  }

}

exports.ExpBarCtrlComponentBase = ExpBarCtrlComponentBase;