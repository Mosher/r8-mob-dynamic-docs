"use strict";

var _interopRequireDefault = require("D:/IbizWork/r8-mob-dynamic-docs/packages/ibz-ui-ionic/node_modules/@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppMobCalendarComponent = exports.AppMobCalendar = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _mdCtrlComponentBase = require("./md-ctrl-component-base");

var _moment = _interopRequireDefault(require("moment"));

function _isSlot(s) {
  return typeof s === 'function' || Object.prototype.toString.call(s) === '[object Object]' && !(0, _vue.isVNode)(s);
}

class AppMobCalendar extends _mdCtrlComponentBase.MDCtrlComponentBase {
  constructor() {
    super(...arguments);
    /**
     * @description 当前年份
     * @private
     * @type {Ref<number>}
     * @memberof AppMobCalendar
     */

    this.year = (0, _vue.ref)(0);
    /**
     * @description 当前月份(0~11)
     * @private
     * @type {Ref<number>}
     * @memberof AppMobCalendar
     */

    this.month = (0, _vue.ref)(0);
    /**
     * @description 当前日期
     * @private
     * @type {Ref<number>}
     * @memberof AppMobCalendar
     */

    this.day = (0, _vue.ref)(0);
    /**
     * @description 当前时间
     * @private
     * @type {Ref<Date>}
     * @memberof AppMobCalendar
     */

    this.currentDate = (0, _vue.ref)(new Date());
    /**
     * @description 当前激活数据
     * @type {*}
     * @memberof AppMobCalendar
     */

    this.activeData = (0, _vue.reactive)({});
    /**
     * @description 默认选中值
     * @private
     * @type {Ref<any[]>}
     * @memberof AppMobCalendar
     */

    this.value = (0, _vue.ref)([]);
    /**
     * @description 事程内容
     * @private
     * @type {IParam[]}
     * @memberof AppMobCalendar
     */

    this.tileContent = [];
    /**
     * @description 时间轴加载条数
     * @private
     * @type {IParam[]}
     * @memberof AppMobCalendar
     */

    this.count = [];
    /**
     * @description 选中数组
     * @private
     * @type {IParam[]}
     * @memberof AppMobCalendar
     */

    this.selectedArray = [];
    /**
     * @description 开始拖动位置
     * @private
     * @type {number}
     * @memberof AppMobCalendar
     */

    this.StarttouchLength = 0;
    /**
     * @description 事程数据
     * @private
     * @type {*}
     * @memberof AppMobCalendar
     */

    this.eventsDate = {};
    /**
     * @description 日历组件引用
     * @private
     * @type {Ref<any>}
     * @memberof AppMobCalendar
     */

    this.calendar = (0, _vue.ref)('calendar');
    /**
     * @description 长按定时器
     * @type {Ref<any>}
     * @memberof AppMobCalendar
     */

    this.touchTimer = (0, _vue.ref)(0);
    /**
     * @description 是否打开上下文菜单
     * @type {*}
     * @memberof AppMobCalendar
     */

    this.isShowContextMenu = true;
  }
  /**
   * @description 设置响应式
   * @memberof AppMobCalendar
   */


  setup() {
    this.c = (0, _vue.shallowReactive)(this.getCtrlControllerByType('CALENDAR'));
    this.initCurrentTime();
    this.initTimeLineCount();
    super.setup();
  }
  /**
   * @description 初始化响应式属性
   * @memberof AppMobCalendar
   */


  initReactive() {
    super.initReactive();
    this.c.show = (0, _vue.reactive)(this.c.show);
    this.c.activeItem = (0, _vue.reactive)(this.c.activeItem);
    this.c.sign = (0, _vue.reactive)(this.c.sign);
  }
  /**
   * @description 初始化当前时间
   * @private
   * @param {*} [curTime=new Date()] 当前时间
   * @memberof AppMobCalendar
   */


  initCurrentTime(curTime = new Date()) {
    this.currentDate.value = curTime;
    this.year.value = curTime.getFullYear();
    this.month.value = curTime.getMonth();
    this.day.value = curTime.getDate();
  }
  /**
   * @description 清除定时器
   * @memberof AppMobCalendar
   */


  clearTimer() {
    clearInterval(this.touchTimer.value);
    this.touchTimer.value = 0;
  }
  /**
   * @description 长按开始
   * @param {*} item 节点数据
   * @param {*} event 事件源
   * @memberof AppMobCalendar
   */


  onTouchStart(item, event) {
    this.touchTimer.value = setInterval(() => {
      this.activeData.value = {
        mouseEvent: event,
        data: item
      };
      this.isShowContextMenu = true;
      this.clearTimer();
    }, 500);
  }
  /**
   * @description 初始化时间轴加载条数
   * @private
   * @memberof AppMobCalendar
   */


  initTimeLineCount() {
    const days = (0, _moment.default)(this.currentDate.value).daysInMonth();
    this.count = [];

    for (let i = 1; i <= days; i++) {
      this.count.push(i);
    }
  }
  /**
   * @description 分页节点切换
   * @private
   * @param {*} $event 数据源
   * @return {*}
   * @memberof AppMobCalendar
   */


  ionChange($event) {
    const {
      detail: _detail
    } = $event;

    if (!_detail) {
      return;
    }

    const {
      value: _value
    } = _detail;

    if (_value) {
      this.c.activeItemChange(_value);
    }
  }
  /**
   * @description 查询天数
   * @private
   * @param {number} year 年
   * @param {number} month 月
   * @param {number} weekIndex 周索引
   * @memberof AppMobCalendar
   */


  selectday(year, month, weekIndex) {
    this.value.value = [year, month, this.day];
  }
  /**
   * @description 上个
   * @private
   * @param {number} year 年
   * @param {number} month 月
   * @param {number} weekIndex 周索引
   * @memberof AppMobCalendar
   */


  prev(year, month, weekIndex) {
    if (this.c.calendarStyle == 'MONTH_TIMELINE' || this.c.calendarStyle == 'MONTH') {
      this.selectday(year, month, this.day.value);
      this.initCurrentTime(new Date(year + '/' + month + '/' + '1'));
      this.c.formatDate(this.currentDate.value);
    }

    if (this.c.calendarStyle == 'WEEK_TIMELINE' || this.c.calendarStyle == 'WEEK') {
      this.countWeeks(year, month, weekIndex);
    }
  }
  /**
   * @description 下个
   * @private
   * @param {number} year 年
   * @param {number} month 月
   * @param {number} weekIndex 周索引
   * @memberof AppMobCalendar
   */


  next(year, month, weekIndex) {
    if (this.c.calendarStyle == 'MONTH_TIMELINE' || this.c.calendarStyle == 'MONTH') {
      this.selectday(year, month, this.day.value);
      this.initCurrentTime(new Date(year + '/' + month + '/' + '1'));
      this.c.formatDate(this.currentDate.value);
    }

    if (this.c.calendarStyle == 'WEEK_TIMELINE' || this.c.calendarStyle == 'WEEK') {
      this.countWeeks(year, month, weekIndex);
    }
  }
  /**
   * @description 根据周下标计算事件
   * @private
   * @param {number} year 年
   * @param {number} month 月
   * @param {number} week 周
   * @memberof AppMobCalendar
   */


  countWeeks(year, month, week) {
    const date = new Date(year + '/' + month + '/' + 1);
    const weekline = date.getDay();

    if (weekline == 0) {
      this.initCurrentTime(new Date(year + '/' + month + '/' + (week * 7 + 1)));
    } else {
      this.initCurrentTime(new Date(year + '/' + month + '/' + (week * 7 - weekline + 1)));
    }

    this.c.formatDate(this.currentDate.value);
  }
  /**
   * @description 选择年份事件的回调方法
   * @private
   * @param {number} year 年
   * @memberof AppMobCalendar
   */


  selectYear(year) {
    this.value.value = [year, this.month, this.day];
    this.initCurrentTime(new Date(year + '/' + this.month + '/' + this.day));
    this.c.formatDate(this.currentDate.value);
  }
  /**
   * @description 选择月份事件的回调方法
   * @private
   * @param {number} month 月
   * @param {number} year 年
   * @memberof AppMobCalendar
   */


  selectMonth(month, year) {
    this.selectday(year, month, this.day.value);
    this.initCurrentTime(new Date(year + '/' + month + '/' + this.day));
    this.c.formatDate(this.currentDate.value);
  }
  /**
   * @description 点击前一天
   * @private
   * @memberof AppMobCalendar
   */


  prevDate() {
    const preDate = new Date(this.currentDate.value.getTime() - 24 * 60 * 60 * 1000); //前一天

    this.initCurrentTime(preDate);
    this.c.formatDate(this.currentDate.value);
  }
  /**
   * @description 点击后一天
   * @private
   * @memberof AppMobCalendar
   */


  nextDate() {
    const nextDate = new Date(this.currentDate.value.getTime() + 24 * 60 * 60 * 1000); //后一天

    this.initCurrentTime(nextDate);
    this.c.formatDate(this.currentDate.value);
  }
  /**
   * @description 日历部件数据选择日期回调
   * @private
   * @param {*} data 日期数据
   * @memberof AppMobCalendar
   */


  clickDay(data) {
    if (data) {
      const reTime = data.join('/');
      const temptime = new Date(reTime);
      this.year.value = temptime.getFullYear();
      this.month.value = temptime.getMonth();
      this.day.value = temptime.getDate();
      const start = (0, _moment.default)(temptime).startOf('day').format('YYYY-MM-DD HH:mm:ss');
      const end = (0, _moment.default)(temptime).endOf('day').format('YYYY-MM-DD HH:mm:ss');
      const args = {
        start: start,
        end: end
      };
      this.c.load({}, args, false);
    }
  }
  /**
   * @description 选中或取消事件
   * @private
   * @param {*} item 事件项
   * @memberof AppMobCalendar
   */


  checkboxSelect(item) {
    const count = this.selectedArray.findIndex(i => {
      return i.mobile_entity1id == item.mobile_entity1id;
    });

    if (count == -1) {
      this.selectedArray.push(item);
    } else {
      this.selectedArray.splice(count, 1);
    }
  }
  /**
   * @description 开始滑动
   * @private
   * @param {*} e 事件源
   * @memberof AppMobCalendar
   */


  gotouchstart(e) {
    const touch = e.touches[0];
    const startY = touch.pageY;
    this.StarttouchLength = startY;
  }
  /**
   * @description 触摸移动
   * @private
   * @param {*} e 事件源
   * @memberof AppMobCalendar
   */


  gotouchmove(e) {
    const touch = e.touches[0];
    const startY = touch.pageY;
    const calendar = this.calendar;

    if (calendar) {
      if (startY - this.StarttouchLength < 0) {
        calendar.changeStyle2(false);
      } else {
        calendar.changeStyle2(true);
      }
    }
  }
  /**
   * @description 日程点击
   * @private
   * @param {IParam} $event 事件信息
   * @memberof AppMobCalendar
   */


  onEventClick($event) {
    this.c.openView($event);
  }
  /**
   * @description 删除事程
   * @private
   * @param {any[]} data 事程数据集
   * @memberof AppMobCalendar
   */


  remove(data) {
    this.c.remove(data).then(() => {
      this.c.formatDate(this.currentDate.value);
    });
  }
  /**
   * @description 绘制日历样式----月
   * @return {*}
   * @memberof AppMobCalendar
   */


  renderMonthCalendar() {
    return (0, _vue.createVNode)((0, _vue.resolveComponent)("app-calendar"), {
      "ref": this.calendar,
      "value": this.value.value,
      "sign": this.c.sign,
      "responsive": true,
      "isChangeStyle": true,
      "events": this.eventsDate,
      "tileContent": this.tileContent,
      "illustration": this.c.illustration,
      "onPrev": this.prev.bind(this),
      "onNext": this.next.bind(this),
      "onSelect": this.clickDay.bind(this),
      "onSelectYear": this.selectYear.bind(this),
      "onSelectMonth": this.selectMonth.bind(this)
    }, null);
  }
  /**
   * @description 绘制日历样式----天
   * @return {*}
   * @memberof AppMobCalendar
   */


  renderDayCalendar() {
    // TODO 左右图标名错误
    return (0, _vue.createVNode)("div", {
      "class": 'calendar-tools'
    }, [(0, _vue.createVNode)("div", {
      "class": 'calendar-prev',
      "onClick": () => this.prevDate()
    }, [(0, _vue.createVNode)((0, _vue.resolveComponent)("app-icon"), {
      "name": 'chevron-back-outline'
    }, null)]), (0, _vue.createVNode)("div", {
      "class": 'calendar-next',
      "onClick": () => this.nextDate()
    }, [(0, _vue.createVNode)((0, _vue.resolveComponent)("app-icon"), {
      "name": 'chevron-forward-outline'
    }, null)]), (0, _vue.createVNode)("div", {
      "class": 'calendar-info'
    }, [`${this.year.value} ${this.$tl('share.year', '年')} ${this.month.value + 1} ${this.$tl('share.month', '月')} ${this.day.value} ${this.$tl('share.day', '日')}`])]);
  }
  /**
   * @description 绘制日历样式----周
   * @return {*}
   * @memberof AppMobCalendar
   */


  renderWeekCalendar() {
    return (0, _vue.createVNode)((0, _vue.resolveComponent)("app-calendar"), {
      "ref": this.calendar,
      "weekSwitch": true,
      "sign": this.c.sign,
      "value": this.value.value,
      "tileContent": this.tileContent,
      "illustration": this.c.illustration,
      "responsive": true,
      "onPrev": this.prev.bind(this),
      "onNext": this.next.bind(this),
      "onSelect": this.clickDay.bind(this),
      "onSelectYear": this.selectYear.bind(this),
      "onSelectMonth": this.selectMonth.bind(this)
    }, null);
  }
  /**
   * @description 绘制分页头
   * @param {IPSSysCalendarItem[]} calendarItems 日历项
   * @return {*}
   * @memberof AppMobCalendar
   */


  renderSegment(calendarItems) {
    let _slot2;

    return (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-segment"), {
      "scrollable": true,
      "value": this.c.activeItem,
      "onIonChange": $event => this.ionChange($event)
    }, _isSlot(_slot2 = calendarItems.map(calendarItem => {
      let _slot;

      var _a, _b;

      return (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-segment-button"), {
        "value": (_a = calendarItem === null || calendarItem === void 0 ? void 0 : calendarItem.itemType) === null || _a === void 0 ? void 0 : _a.toLowerCase()
      }, {
        default: () => [(0, _vue.createVNode)((0, _vue.resolveComponent)("ion-label"), null, _isSlot(_slot = this.$tl((_b = calendarItem.getNamePSLanguageRes()) === null || _b === void 0 ? void 0 : _b.lanResTag, calendarItem.name)) ? _slot : {
          default: () => [_slot]
        })]
      });
    })) ? _slot2 : {
      default: () => [_slot2]
    });
  }
  /**
   * @description 绘制日历样式----时间轴
   * @param {*} calendarStyle 日历样式
   * @return {*}
   * @memberof AppMobCalendar
   */


  renderTimelineCalendar(calendarStyle) {
    switch (calendarStyle) {
      case 'TIMELINE':
        return this.renderTimeline();

      case 'MONTH_TIMELINE':
      case 'WEEK_TIMELINE':
        return this.renderWMTimeline();

      default:
        return this.renderEventList();
    }
  }
  /**
   * @description 绘制时间轴
   * @return {*}
   * @memberof AppMobCalendar
   */


  renderTimeline() {
    return (0, _vue.createVNode)("div", {
      "class": 'calendar-timeline'
    }, [this.count.map(i => {
      var _a;

      return (0, _vue.createVNode)("div", {
        "class": 'timeline-item'
      }, [(0, _vue.createVNode)("p", null, [`${this.year.value}-${this.month.value + 1}-${i}`]), (_a = this.c.sign) === null || _a === void 0 ? void 0 : _a.map(it => {
        var _a, _b;

        if (it.time == this.year.value + '-' + (this.month.value + 1) + '-' + i || it.time == this.year.value + '-' + '0' + (this.month.value + 1) + '-' + i) {
          const show = (_a = it.evens) === null || _a === void 0 ? void 0 : _a.find(item => Object.is(this.c.activeItem, item.itemType));

          if (show) {
            return (0, _vue.createVNode)("div", {
              "class": 'even-container'
            }, [(_b = it.evens) === null || _b === void 0 ? void 0 : _b.map(item => {
              if (this.c.activeItem == item.itemType) {
                return (0, _vue.createVNode)("div", {
                  "class": 'even-item'
                }, [this.renderEventContent(item), (0, _vue.createVNode)((0, _vue.resolveComponent)("app-icon"), {
                  "onClick": () => this.remove([item]),
                  "name": 'close'
                }, null)]);
              }
            })]);
          }
        }
      })]);
    })]);
  }
  /**
   * @description 绘制月-周时间轴
   * @return {*}
   * @memberof AppMobCalendar
   */


  renderWMTimeline() {
    var _a;

    return (0, _vue.createVNode)("div", {
      "class": 'calendar-composite-timeline'
    }, [(_a = this.c.sign) === null || _a === void 0 ? void 0 : _a.map(i => {
      var _a;

      return (0, _vue.createVNode)("div", {
        "class": 'timeline-item'
      }, [(0, _vue.createVNode)("p", null, [i.time]), (_a = i.evens) === null || _a === void 0 ? void 0 : _a.map(item => {
        if (this.c.activeItem == item.itemType) {
          return (0, _vue.createVNode)("div", {
            "class": 'multiple-select'
          }, [this.c.isChoose && (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-checkbox"), {
            "class": 'checkbox',
            "onClick": () => this.checkboxSelect(item)
          }, null), (0, _vue.createVNode)("div", {
            "class": 'even-container'
          }, [this.renderEventContent(item), !this.c.isChoose && (0, _vue.createVNode)((0, _vue.resolveComponent)("app-icon"), {
            "onClick": () => this.remove([item]),
            "name": 'close'
          }, null)])]);
        }
      })]);
    })]);
  }
  /**
   * @description 绘制事件内容
   * @param {*} item 日程事件
   * @return {*}
   * @memberof AppMobCalendar
   */


  renderEventContent(item) {
    const calendarItems = this.c.controlInstance.getPSSysCalendarItems() || [];
    const appDataEntity = this.c.controlInstance.getPSAppDataEntity();

    const renderTempJSX = calendarItem => {
      var _a, _b, _c, _d, _e;

      if (calendarItem.getTextPSAppDEField()) {
        return (0, _vue.createVNode)("div", {
          "class": 'evenname'
        }, [item['title']]);
      } else if (calendarItem.getContentPSAppDEField()) {
        return (0, _vue.createVNode)("div", {
          "class": 'evenname'
        }, [item[(_b = (_a = calendarItem === null || calendarItem === void 0 ? void 0 : calendarItem.getContentPSAppDEField()) === null || _a === void 0 ? void 0 : _a.name) === null || _b === void 0 ? void 0 : _b.toLowerCase()]]);
      } else if (calendarItem.getIconPSAppDEField()) {
        return (0, _vue.createVNode)("div", {
          "class": 'evenname'
        }, [item[(_d = (_c = calendarItem === null || calendarItem === void 0 ? void 0 : calendarItem.getIconPSAppDEField()) === null || _c === void 0 ? void 0 : _c.name) === null || _d === void 0 ? void 0 : _d.toLowerCase()]]);
      } else {
        const majorField = _ibzCore.ModelTool.getAppEntityMajorField(appDataEntity);

        return (0, _vue.createVNode)("div", {
          "class": 'evenname'
        }, [majorField ? (_e = majorField === null || majorField === void 0 ? void 0 : majorField.codeName) === null || _e === void 0 ? void 0 : _e.toLowerCase() : '']);
      }
    };

    return calendarItems.map(calendarItem => {
      if (this.c.activeItem == calendarItem.itemType.toLowerCase()) {
        return (0, _vue.createVNode)("div", {
          "onClick": () => this.onEventClick(item)
        }, [renderTempJSX(calendarItem)]);
      }
    });
  }
  /**
   * @description 绘制事程列表
   * @return {*}
   * @memberof AppMobCalendar
   */


  renderEventList() {
    var _a;

    if (this.c.isEnableGroup) {
      const pluginCode = (_a = this.c.controlInstance.getGroupPSSysPFPlugin()) === null || _a === void 0 ? void 0 : _a.pluginCode;

      if (pluginCode) {
        const ctrlItemPluginInstance = App.getPluginService().getCtrlItemPluginByTag(pluginCode);

        if (ctrlItemPluginInstance) {
          return ctrlItemPluginInstance.renderItem(this.c.groupData, this.c, this);
        }
      } else {
        return this.renderGroupEvent();
      }
    } else {
      return this.renderNoGroupEvent();
    }
  }
  /**
   * @description 绘制分组事程
   * @memberof AppMobCalendar
   */


  renderGroupEvent() {
    var _a;

    const groupClassName = (_a = this.c.controlInstance.getGroupPSSysCss()) === null || _a === void 0 ? void 0 : _a.cssName;
    const groupData = this.c.groupData;

    if ((groupData === null || groupData === void 0 ? void 0 : groupData.length) > 0) {
      let _slot3;

      return (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-list"), {
        "class": groupClassName
      }, _isSlot(_slot3 = groupData.map(data => {
        var _a;

        return (0, _vue.createVNode)("div", {
          "class": 'calendar-group'
        }, [(0, _vue.createVNode)((0, _vue.resolveComponent)("ion-item-group"), null, {
          default: () => [(0, _vue.createVNode)((0, _vue.resolveComponent)("ion-item-divider"), null, {
            default: () => [(0, _vue.createVNode)((0, _vue.resolveComponent)("ion-label"), null, {
              default: () => [data.text]
            })]
          }), (_a = data.items) === null || _a === void 0 ? void 0 : _a.map((item, index) => {
            var _a;

            const calendarItem = (_a = this.c.controlInstance.getPSSysCalendarItems()) === null || _a === void 0 ? void 0 : _a.find(_calendarItem => {
              var _a;

              return Object.is((_a = item.itemType) === null || _a === void 0 ? void 0 : _a.toLowerCase(), _calendarItem.itemType.toLowerCase());
            });
            const itemStyle = {
              '--color': item.textColor,
              '--background': item.color
            };
            return (calendarItem === null || calendarItem === void 0 ? void 0 : calendarItem.getPSLayoutPanel()) ? this.renderCalendarItemLayoutPanel(calendarItem, item, itemStyle) : this.renderCalendarItemDefault(calendarItem, item, itemStyle);
          })]
        })]);
      })) ? _slot3 : {
        default: () => [_slot3]
      });
    } else {
      return [this.renderNoData(), this.renderQuickToolbar()];
    }
  }
  /**
   * @description 绘制无分组事程
   * @memberof AppMobCalendar
   */


  renderNoGroupEvent() {
    var _a, _b;

    const arr = this.c.calendarItems[this.c.activeItem.toString()];
    const calendarItem = (_a = this.c.controlInstance.getPSSysCalendarItems()) === null || _a === void 0 ? void 0 : _a.find(_calendarItem => Object.is(this.c.activeItem.toString(), _calendarItem.itemType.toLowerCase()));

    if ((arr === null || arr === void 0 ? void 0 : arr.length) > 0) {
      let _slot4;

      return (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-list"), {
        "class": (_b = calendarItem === null || calendarItem === void 0 ? void 0 : calendarItem.getPSSysCss()) === null || _b === void 0 ? void 0 : _b.cssName
      }, _isSlot(_slot4 = arr.map(item => {
        const itemStyle = {
          '--color': item.textColor,
          '--background': item.color
        };
        return (calendarItem === null || calendarItem === void 0 ? void 0 : calendarItem.getPSLayoutPanel()) ? this.renderCalendarItemLayoutPanel(calendarItem, item, itemStyle) : this.renderCalendarItemDefault(calendarItem, item, itemStyle);
      })) ? _slot4 : {
        default: () => [_slot4]
      });
    } else {
      return [this.renderNoData(), this.renderQuickToolbar()];
    }
  }
  /**
   * @description 绘制日历项默认样式
   * @param {IPSSysCalendarItem} calendarItem 日历项
   * @param {IParam} item 日历项数据
   * @param {string} itemStyle 日历项样式
   * @memberof AppMobCalendar
   */


  renderCalendarItemDefault(calendarItem, item, itemStyle) {
    return (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-item"), {
      "class": 'calendar-text-item',
      "style": itemStyle,
      "onTouchstart": event => {
        this.onTouchStart(item, event);
      },
      "onTouchend": event => {
        this.clearTimer();
      },
      "obTouchmove": event => {
        this.clearTimer();
      },
      "onClick": () => this.onEventClick(item)
    }, {
      default: () => [(0, _vue.createVNode)((0, _vue.resolveComponent)("ion-label"), {
        "class": 'calendar-text'
      }, {
        default: () => [(0, _vue.createVNode)("h2", null, [(0, _vue.createVNode)((0, _vue.resolveComponent)("app-icon"), {
          "iconSrc": item.iconSrc
        }, null), item.title]), (0, _vue.createVNode)("p", null, [item.content])]
      })]
    });
  }
  /**
   * @description 绘制日历项布局面板
   * @param {IPSSysCalendarItem} calendarItem 日历项
   * @param {IParam} item 日历项数据
   * @param {string} itemStyle 日历项样式
   * @return {*}
   * @memberof AppMobCalendar
   */


  renderCalendarItemLayoutPanel(calendarItem, item, itemStyle) {
    let _slot5;

    var _a, _b;

    const _context = _ibzCore.Util.deepCopy(this.c.context);

    const entityCodeName = (_b = (_a = calendarItem.getPSAppDataEntity()) === null || _a === void 0 ? void 0 : _a.codeName) === null || _b === void 0 ? void 0 : _b.toLowerCase();
    Object.assign(_context, {
      [entityCodeName]: item[entityCodeName]
    });
    const otherParams = {
      navDatas: [item],
      navContext: _context
    };
    return (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-item"), {
      "class": "calendar-item-layout",
      "style": itemStyle
    }, _isSlot(_slot5 = this.computeTargetCtrlData(calendarItem.getPSLayoutPanel(), otherParams)) ? _slot5 : {
      default: () => [_slot5]
    });
  }
  /**
   * @description 绘制上下文菜单
   * @return {*}
   * @memberof AppMobCalendar
   */


  renderContextMenu() {
    var _a, _b, _c, _d, _e;

    const mouseEvent = (_a = this.activeData.value) === null || _a === void 0 ? void 0 : _a.mouseEvent;
    const data = (_b = this.activeData.value) === null || _b === void 0 ? void 0 : _b.data;

    if (this.activeData.value) {
      const calendarItem = (_c = this.c.controlInstance.getPSSysCalendarItems()) === null || _c === void 0 ? void 0 : _c.find(_calendarItem => Object.is(data.itemType.toLowerCase(), _calendarItem.itemType.toLowerCase()));

      const contextMenu = _ibzCore.ModelTool.findPSControlByName((_e = (_d = calendarItem === null || calendarItem === void 0 ? void 0 : calendarItem.getPSDEContextMenu) === null || _d === void 0 ? void 0 : _d.call(calendarItem)) === null || _e === void 0 ? void 0 : _e.name, this.c.controlInstance.getPSControls());

      if (contextMenu && this.isShowContextMenu) {
        this.isShowContextMenu = false;
        const otherParams = {
          key: _ibzCore.Util.createUUID(),
          navDatas: [data.curData],
          mouseEvent: mouseEvent,
          contextMenuActionModel: {}
        };
        return this.computeTargetCtrlData(contextMenu, otherParams);
      }
    }
  }
  /**
   * @description 绘制日历部件
   * @return {*}
   * @memberof AppMobCalendar
   */


  render() {
    if (!this.controlIsLoaded.value) {
      return null;
    }

    const calendarStyle = this.c.calendarStyle;
    const calendarItems = this.c.controlInstance.getPSSysCalendarItems() || [];
    return (0, _vue.createVNode)("div", {
      "class": Object.assign({}, this.classNames),
      "onTouchmove": $event => this.gotouchmove($event),
      "onTouchstart": $event => this.gotouchstart($event)
    }, [this.renderPullDownRefresh(), this.renderContextMenu(), this.c.show && (0, _vue.createVNode)("div", {
      "class": ['calender-content', this.c.activeItem]
    }, [calendarStyle == 'MONTH' || calendarStyle == 'MONTH_TIMELINE' ? this.renderMonthCalendar() : null, calendarStyle == 'DAY' ? this.renderDayCalendar() : null, calendarStyle == 'WEEK' || calendarStyle == 'WEEK_TIMELINE' ? this.renderWeekCalendar() : null, calendarItems && this.c.groupData.length == 0 ? this.renderSegment(calendarItems) : null, (0, _vue.createVNode)("div", {
      "class": 'calendar-events'
    }, [this.renderTimelineCalendar(calendarStyle), this.renderBatchToolbar()])])]);
  }

} // 移动端日历部件


exports.AppMobCalendar = AppMobCalendar;
const AppMobCalendarComponent = (0, _componentBase.GenerateComponent)(AppMobCalendar, Object.keys(new _ibzCore.AppMobCalendarProps()));
exports.AppMobCalendarComponent = AppMobCalendarComponent;