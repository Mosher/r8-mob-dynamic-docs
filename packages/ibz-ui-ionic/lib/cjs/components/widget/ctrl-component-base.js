"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CtrlComponentBase = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _common = require("../common");

/**
 * 部件组件基类
 *
 * @export
 * @class CtrlComponentBase
 */
class CtrlComponentBase extends _componentBase.ComponentBase {
  constructor() {
    super(...arguments);
    /**
     * @description vue ref引用集合
     * @type {Map<string, any>}
     * @memberof CtrlComponentBase
     */

    this.refsMap = new Map();
  }
  /**
   * @description 部件样式名
   * @readonly
   * @type {any}
   * @memberof CtrlComponentBase
   */


  get classNames() {
    var _a, _b;

    const {
      controlType,
      codeName
    } = this.c.controlInstance;
    const classNames = {
      'container-margin': true,
      'container-padding': true,
      'app-ctrl': true,
      [controlType === null || controlType === void 0 ? void 0 : controlType.toLowerCase()]: true,
      [_ibzCore.Util.srfFilePath2(codeName)]: true
    };
    const sysCss = (_b = (_a = this.c.controlInstance).getPSSysCss) === null || _b === void 0 ? void 0 : _b.call(_a);

    if (sysCss) {
      Object.assign(classNames, {
        [sysCss.cssName]: true
      });
    }

    return classNames;
  }
  /**
   * @description 设置响应式
   * @memberof CtrlComponentBase
   */


  setup() {
    this.controlIsLoaded = (0, _vue.toRef)(this.c, 'controlIsLoaded');
    this.initReactive();
    this.emitCtrlEvent = this.emitCtrlEvent.bind(this);
    this.c.hooks.event.tap(this.emitCtrlEvent);
    this.emitCloseView = this.emitCloseView.bind(this);
    this.c.hooks.closeView.tap(this.emitCloseView);
    this.ctrlMounted = this.ctrlMounted.bind(this);
    this.c.hooks.mounted.tap(this.ctrlMounted);
  }
  /**
   * @description 初始化响应式属性
   * @memberof CtrlComponentBase
   */


  initReactive() {
    this.c.context = (0, _vue.reactive)(this.c.context);
    this.c.viewParam = (0, _vue.reactive)(this.c.viewParam);
    this.c.navDatas = (0, _vue.reactive)(this.c.navDatas);
    this.c.viewCtx = (0, _vue.reactive)(this.c.viewCtx);

    if (this.c && this.c.actionModel) {
      this.c.actionModel = (0, _vue.reactive)(this.c.actionModel);
    }
  }
  /**
   * @description 部件初始化
   * @memberof CtrlComponentBase
   */


  init() {
    this.c.controlInit().then(result => {
      var _a;

      this.emitCtrlEvent({
        controlname: (_a = this.c.controlInstance) === null || _a === void 0 ? void 0 : _a.name,
        action: _ibzCore.AppCtrlEvents.INITED,
        data: result
      });
    });
  }
  /**
   * @description 部件输入属性值变更
   * @memberof CtrlComponentBase
   */


  watchEffect() {
    this.c.initInputData(this.props);
  }
  /**
   * @description 部件销毁
   * @memberof CtrlComponentBase
   */


  unmounted() {
    var _a;

    this.c.controlDestroy();
    this.c.hooks.event.removeTap(this.emitCtrlEvent);
    this.c.hooks.closeView.removeTap(this.emitCloseView);
    this.c.hooks.mounted.removeTap(this.ctrlMounted);
    this.emitCtrlEvent({
      controlname: (_a = this.c.controlInstance) === null || _a === void 0 ? void 0 : _a.name,
      action: _ibzCore.AppCtrlEvents.DESTROYED,
      data: true
    });
  }
  /**
   * @description 执行部件事件
   * @param {ICtrlEventParam} arg 事件参数
   * @memberof CtrlComponentBase
   */


  emitCtrlEvent(arg) {
    this.ctx.emit('ctrlEvent', arg);
  }
  /**
   * @description 抛出关闭视图事件
   * @param {IParam} arg 事件参数
   * @memberof CtrlComponentBase
   */


  emitCloseView(arg) {
    var _a;

    this.emitCtrlEvent({
      controlname: (_a = this.c.controlInstance) === null || _a === void 0 ? void 0 : _a.name,
      action: _ibzCore.AppCtrlEvents.CLOSE,
      data: arg
    });
  }
  /**
   * @description 部件挂载完成
   * @param {IParam} arg 事件参数
   * @memberof CtrlComponentBase
   */


  ctrlMounted(args) {}
  /**
   * @description 根据部件类型获取部件控制器
   * @param {string} type 部件类型
   * @return {*}
   * @memberof CtrlComponentBase
   */


  getCtrlControllerByType(type) {
    switch (type) {
      case 'APPMENU':
        return new _ibzCore.MobMenuCtrlController(this.props);

      case 'TOOLBAR':
        return new _ibzCore.MobToolbarCtrlController(this.props);

      case 'FORM':
        return new _ibzCore.MobFormCtrlController(this.props);

      case 'SEARCHFORM':
        return new _ibzCore.MobSearchFormCtrlController(this.props);

      case 'MOBMDCTRL':
        return new _ibzCore.MobMDCtrlController(this.props);

      case 'PANEL':
        return new _ibzCore.MobPanelController(this.props);

      case 'PICKUPVIEWPANEL':
        return new _ibzCore.MobPickUpViewPanelCtrlController(this.props);

      case 'CALENDAR':
        return new _ibzCore.MobCalendarCtrlController(this.props);

      case 'TREE':
        return new _ibzCore.MobTreeCtrlController(this.props);

      case 'CHART':
        return new _ibzCore.MobChartCtrlController(this.props);

      case 'MAP':
        return new _ibzCore.MobMapCtrlController(this.props);

      case 'CONTEXTMENU':
        return new _ibzCore.MobContextMenuCtrlController(this.props);

      case 'TABEXPPANEL':
        return new _ibzCore.MobTabExpPanelCtrlController(this.props);

      case 'TABVIEWPANEL':
        return new _ibzCore.MobTabViewPanelCtrlController(this.props);

      case 'MULTIEDITVIEWPANEL':
        return new _ibzCore.MobMEditViewPanelCtrlController(this.props);

      case 'DASHBOARD':
        return new _ibzCore.MobDashboardController(this.props);

      case 'PORTLET':
        return new _ibzCore.MobPortletController(this.props);

      case 'LISTEXPBAR':
        return new _ibzCore.MobListExpBarController(this.props);

      case 'WIZARDPANEL':
        return new _ibzCore.MobWizardPanelController(this.props);

      case 'CHARTEXPBAR':
        return new _ibzCore.MobChartExpBarController(this.props);

      case 'WIZARDPANEL_STATE':
        return new _ibzCore.MobStateWizardPanelController(this.props);

      case 'TREEEXPBAR':
        return new _ibzCore.MobTreeExpBarController(this.props);

      case 'MAPEXPBAR':
        return new _ibzCore.MobMapExpBarController(this.props);

      default:
        console.log(`暂未实现${type}类型部件`);
        return null;
    }
  }
  /**
   * @description 计算目标部件所需参数
   * @param {IPSControl} controlInstance 部件模型实例
   * @return {*}
   * @memberof CtrlComponentBase
   */


  computeTargetCtrlData(controlInstance, otherParams = {}) {
    var _a;

    const targetCtrlComponent = App.getComponentService().getControlComponent(controlInstance === null || controlInstance === void 0 ? void 0 : controlInstance.controlType, (controlInstance === null || controlInstance === void 0 ? void 0 : controlInstance.controlStyle) ? controlInstance === null || controlInstance === void 0 ? void 0 : controlInstance.controlStyle : 'DEFAULT', controlInstance.getPSSysPFPlugin() ? `${(_a = controlInstance.getPSSysPFPlugin()) === null || _a === void 0 ? void 0 : _a.pluginCode}` : undefined);
    this.refsMap.set(controlInstance.name, (0, _vue.ref)(null));
    const targetCtrlParams = {
      ref: this.refsMap.get(controlInstance.name),
      controlInstance: controlInstance,
      viewState: this.c.viewState,
      viewCtx: this.c.viewCtx,
      navContext: _ibzCore.Util.deepCopy(this.c.context),
      navParam: _ibzCore.Util.deepCopy(this.c.viewParam),
      modelService: this.c.modelService,
      onCtrlEvent: ({
        controlname,
        action,
        data
      }) => {
        this.c.handleCtrlEvent(controlname, action, data);
      }
    };

    if (otherParams && Object.keys(otherParams).length > 0) {
      Object.assign(targetCtrlParams, otherParams);
    }

    return (0, _vue.h)(_common.AppCtrlContainer, null, {
      default: () => {
        return (0, _vue.h)(targetCtrlComponent, targetCtrlParams);
      }
    });
  }
  /**
   * @description 下拉刷新
   * @param {*} event 事件源
   * @memberof CtrlComponentBase
   */


  pullDownRefresh(event) {
    if (this.c.pullDownRefresh && this.c.pullDownRefresh instanceof Function) {
      this.c.pullDownRefresh().then(response => {
        event.target.complete();
      }).catch(error => {
        event.target.complete();
      });
    }
  }
  /**
   * @description 绘制下拉刷新
   * @return {*}
   * @memberof CtrlComponentBase
   */


  renderPullDownRefresh() {
    if (this.c.enablePullDownRefresh) {
      return (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-refresher"), {
        "slot": 'fixed',
        "onIonRefresh": event => {
          this.pullDownRefresh(event);
        }
      }, {
        default: () => [(0, _vue.createVNode)((0, _vue.resolveComponent)("ion-refresher-content"), null, null)]
      });
    }
  }
  /**
   * @description 绘制无数据
   * @return {*}
   * @memberof CtrlComponentBase
   */


  renderNoData() {
    var _a, _b, _c;

    return (0, _vue.createVNode)("div", {
      "class": 'no-data'
    }, [this.$tl((_c = (_b = (_a = this.c.controlInstance) === null || _a === void 0 ? void 0 : _a.getEmptyTextPSLanguageRes) === null || _b === void 0 ? void 0 : _b.call(_a)) === null || _c === void 0 ? void 0 : _c.lanResTag, this.c.controlInstance.emptyText) || this.$tl('share.emptytext', '暂无数据')]);
  }

}

exports.CtrlComponentBase = CtrlComponentBase;