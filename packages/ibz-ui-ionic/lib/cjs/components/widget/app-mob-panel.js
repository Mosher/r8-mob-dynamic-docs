"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppMobPanelComponent = exports.AppMobPanel = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _ctrlComponentBase = require("./ctrl-component-base");

var _appLayoutContainer = require("../common/app-layout-container");

var _appLayoutItem = require("../common/app-layout-item");

function _isSlot(s) {
  return typeof s === 'function' || Object.prototype.toString.call(s) === '[object Object]' && !(0, _vue.isVNode)(s);
}

class AppMobPanel extends _ctrlComponentBase.CtrlComponentBase {
  constructor() {
    super(...arguments);
    /**
     * @description 当前选中分页
     * @type {*}
     * @memberof AppMobPanel
     */

    this.currentTab = (0, _vue.ref)({});
  }
  /**
   * @description 设置响应式
   * @memberof AppMobPanel
   */


  setup() {
    this.c = (0, _vue.shallowReactive)(this.getCtrlControllerByType('PANEL'));
    super.setup();
  }
  /**
   * @description 初始化响应式属性
   * @memberof AppMobPanel
   */


  initReactive() {
    super.initReactive();
    this.c.detailsModel = (0, _vue.reactive)(this.c.detailsModel);
    this.c.dataMap = (0, _vue.reactive)(this.c.dataMap);
  }
  /**
   * @description 获取部件样式名
   * @return {*}
   * @memberof AppMobPanel
   */


  getCtrlClassNames() {
    var _a, _b, _c;

    const cssName = (_c = (_b = (_a = this.c.controlInstance).getPSSysCss) === null || _b === void 0 ? void 0 : _b.call(_a)) === null || _c === void 0 ? void 0 : _c.cssName;
    const classNames = {
      [`app-${this.c.panelMode}-panel`]: true
    };

    if (cssName) {
      Object.assign(classNames, {
        [cssName]: true
      });
    }

    return classNames;
  }
  /**
   * 计算容器尺寸
   *
   * @param {number} value
   * @return {*}  {string}
   */


  calcBoxSize(value, enableRem = false) {
    if (value > 1) {
      return enableRem ? value / 50 + 'rem' : value + 'px';
    } else if (value > 0) {
      return value * 100 + '%';
    }

    return '100%';
  }
  /**
   * @description 获取顶级容器样式
   * @return {*}
   * @memberof AppMobPanel
   */


  getRootStyle() {
    const height = this.c.controlInstance.height;
    const width = this.c.controlInstance.panelWidth;
    const style = {};

    if (width > 0) {
      Object.assign(style, {
        width: this.calcBoxSize(width, true),
        height: this.calcBoxSize(height, true)
      });
    } else {
      Object.assign(style, {
        width: '100%',
        height: '100%'
      });
    }

    return style;
  }
  /**
   * @description 获取面板项样式名
   * @param {IPSPanelItem} item 面板项
   * @return {*}
   * @memberof AppMobPanel
   */


  getDetailClass(item) {
    var _a;

    const {
      itemType,
      name
    } = item;
    const detailClass = {
      [`app-${this.c.panelMode}panel-${itemType.toLowerCase()}`]: true,
      [`panel-${itemType.toLowerCase()}-${name.toLowerCase()}`]: true
    };
    const css = (_a = item.getPSSysCss) === null || _a === void 0 ? void 0 : _a.call(item);

    if (css) {
      Object.assign(detailClass, {
        [`${css.cssName}`]: true
      });
    }

    return detailClass;
  }
  /**
   * @description 面板分页栏切换
   * @param {string} key 分页栏标识
   * @param {*} event 源事件对象
   * @memberof AppMobPanel
   */


  segmentChanged(key, event) {
    if (key) {
      this.currentTab.value[key] = event.detail.value;
    }
  }
  /**
   * @description 面板按钮点击
   * @param {IParam} detail 数据
   * @param {*} event 源事件对象
   * @memberof AppMobPanel
   */


  buttonClick(detail, event) {
    if (this.c.handleActionClick && this.c.handleActionClick instanceof Function) {
      this.c.handleActionClick(this.c.data, event, detail);
    }
  }
  /**
   * @description 渲染面板按钮
   * @param {IPSPanelButton} modelJson 按钮模型数据
   * @param {number} index 序列标识
   * @return {*}
   * @memberof AppMobPanel
   */


  renderButton(modelJson, index) {
    var _a, _b, _c, _d, _e, _f;

    const {
      caption,
      name,
      showCaption
    } = modelJson;
    const detailModel = (_a = this.c.detailsModel) === null || _a === void 0 ? void 0 : _a[name]; //  模型显示

    if (detailModel && !detailModel.visible) {
      return;
    }

    const icon = modelJson.getPSSysImage();
    const uiAction = modelJson.getPSUIAction();
    const iconName = (icon === null || icon === void 0 ? void 0 : icon.cssClass) ? icon.cssClass : ((_b = uiAction === null || uiAction === void 0 ? void 0 : uiAction.getPSSysImage()) === null || _b === void 0 ? void 0 : _b.cssClass) ? (_c = uiAction.getPSSysImage()) === null || _c === void 0 ? void 0 : _c.cssClass : null;
    return (0, _vue.createVNode)(_appLayoutItem.AppLayoutItem, {
      "class": this.getDetailClass(modelJson),
      "layoutMode": (_d = modelJson.getParentPSModelObject().getPSLayout()) === null || _d === void 0 ? void 0 : _d.layout,
      "layoutPos": modelJson.getPSLayoutPos() || undefined
    }, {
      default: () => [(0, _vue.createVNode)((0, _vue.resolveComponent)("ion-button"), {
        "disabled": detailModel && ((_e = detailModel[name]) === null || _e === void 0 ? void 0 : _e.disabled),
        "onClick": event => {
          this.buttonClick(modelJson, event);
        }
      }, {
        default: () => [iconName ? (0, _vue.createVNode)((0, _vue.resolveComponent)("app-icon"), {
          "name": iconName
        }, null) : null, showCaption ? this.$tl((_f = modelJson.getCapPSLanguageRes()) === null || _f === void 0 ? void 0 : _f.lanResTag, caption) : '']
      })]
    });
  }
  /**
   * @description 渲染编辑器
   * @param {IPSEditor} editor 编辑器实例对象
   * @param {IPSPanelItem} parentItem 面板项对象
   * @return {*}
   * @memberof AppMobPanel
   */


  renderEditor(editor, parentItem) {
    var _a;

    const targetCtrlComponent = App.getComponentService().getEditorComponent(editor === null || editor === void 0 ? void 0 : editor.editorType, (editor === null || editor === void 0 ? void 0 : editor.editorStyle) ? editor === null || editor === void 0 ? void 0 : editor.editorStyle : 'DEFAULT', editor.getPSSysPFPlugin() ? `${editor === null || editor === void 0 ? void 0 : editor.editorType}_${editor === null || editor === void 0 ? void 0 : editor.editorStyle}` : undefined);
    return (0, _vue.h)(targetCtrlComponent, {
      editorInstance: editor,
      containerCtrl: this.c.controlInstance,
      parentItem: parentItem,
      contextData: this.c.data,
      navContext: _ibzCore.Util.deepCopy(this.c.context),
      navParam: _ibzCore.Util.deepCopy(this.c.viewParam),
      disabled: (_a = this.c.detailsModel[editor.name]) === null || _a === void 0 ? void 0 : _a.disabled,
      value: this.c.data[(editor === null || editor === void 0 ? void 0 : editor.name) || ''],
      onEditorEvent: event => {
        this.c.handleEditorEvent(event);
      }
    });
  }
  /**
   * @description 渲染面板属性
   * @param {IPSPanelField} modelJson 面板属性对象
   * @param {number} index 序列标识
   * @return {*}
   * @memberof AppMobPanel
   */


  renderField(modelJson, index) {
    let _slot;

    var _a, _b, _c, _d; //  模型显示


    const detailModel = (_a = this.c.detailsModel) === null || _a === void 0 ? void 0 : _a[modelJson.name];

    if (detailModel && !detailModel.visible) {
      return;
    }

    const {
      caption
    } = modelJson;
    const editor = modelJson.getPSEditor();
    let customCode = false;

    if (this.c.dataMap && this.c.dataMap.get(modelJson.name)) {
      customCode = (_b = this.c.dataMap.get(modelJson.name)) === null || _b === void 0 ? void 0 : _b.customCode;
    }

    return (0, _vue.createVNode)(_appLayoutItem.AppLayoutItem, {
      "class": this.getDetailClass(modelJson),
      "layoutMode": (_c = modelJson.getParentPSModelObject().getPSLayout()) === null || _c === void 0 ? void 0 : _c.layout,
      "layoutPos": modelJson.getPSLayoutPos() || undefined
    }, {
      default: () => [caption && (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-label"), {
        "class": 'panel-item-field-label'
      }, _isSlot(_slot = this.$tl((_d = modelJson.getCapPSLanguageRes()) === null || _d === void 0 ? void 0 : _d.lanResTag, caption)) ? _slot : {
        default: () => [_slot]
      }), editor && !customCode ? this.renderEditor(editor, modelJson) : null, customCode && (0, _vue.h)('div', {
        innerHTML: this.c.data[modelJson.name]
      })]
    });
  }
  /**
   * @description 渲染直接内容
   * @param {IPSPanelButton} modelJson 直接内容模型数据
   * @param {number} index 序列标识
   * @return {*}
   * @memberof AppMobPanel
   */


  renderRawItem(modelJson, index) {
    var _a, _b, _c; //  模型显示


    const detailModel = (_a = this.c.detailsModel) === null || _a === void 0 ? void 0 : _a[modelJson.name];

    if (detailModel && !detailModel.visible) {
      return;
    }

    const {
      contentType,
      htmlContent,
      getPSSysImage,
      rawContent
    } = modelJson;

    if (rawContent) {//  TODO  国际化内容支持
      // const items = rawContent.match(/\{{(.+?)\}}/g);
      // if (items) {
      //     items.forEach((item: string) => {
      //       rawContent = rawContent.replace(/\{{(.+?)\}}/, item.substring(2, item.length - 2));
      //     });
      // }
    }

    let element = undefined;

    if (contentType == 'HTML' && htmlContent) {
      element = (0, _vue.h)('div', {
        innerHTML: htmlContent
      });
    }

    let content;

    switch (contentType) {
      case 'HTML':
        content = element ? element : htmlContent;
        break;

      case 'RAW':
        content = rawContent;
        break;

      case 'IMAGE':
        content = (0, _vue.createVNode)("img", {
          "src": (_b = getPSSysImage === null || getPSSysImage === void 0 ? void 0 : getPSSysImage()) === null || _b === void 0 ? void 0 : _b.imagePath
        }, null);
        break;

      case 'MARKDOWN':
        content = (0, _vue.createVNode)("div", null, [this.$tl('widget.mobpanel.nomarkdown', 'MARKDOWN直接内容暂未支持')]);
        break;
    }

    return (0, _vue.createVNode)(_appLayoutItem.AppLayoutItem, {
      "class": this.getDetailClass(modelJson),
      "layoutMode": (_c = modelJson.getParentPSModelObject().getPSLayout()) === null || _c === void 0 ? void 0 : _c.layout,
      "layoutPos": modelJson.getPSLayoutPos() || undefined
    }, _isSlot(content) ? content : {
      default: () => [content]
    });
  }
  /**
   * @description 渲染分页部件
   * @param {IPSPanelButton} modelJson 分页部件模型数据
   * @param {number} index 序列标识
   * @return {*}
   * @memberof AppMobPanel
   */


  renderTabPage(modelJson, index) {
    var _a, _b, _c; //  模型显示


    const detailModel = (_a = this.c.detailsModel) === null || _a === void 0 ? void 0 : _a[modelJson.name];

    if (detailModel && !detailModel.visible) {
      return;
    }

    const parent = (_b = modelJson.getParentPSModelObject) === null || _b === void 0 ? void 0 : _b.call(modelJson);
    const currentKey = this.currentTab.value[parent === null || parent === void 0 ? void 0 : parent.name];

    if (currentKey == modelJson.name) {
      return (0, _vue.createVNode)("div", {
        "class": this.getDetailClass(modelJson),
        "style": [detailModel && !detailModel.visible ? 'display: none;' : '']
      }, [(0, _vue.createVNode)(_appLayoutContainer.AppLayoutContainer, {
        "layout": modelJson.getPSLayout() || undefined
      }, {
        default: () => [(_c = modelJson.getPSPanelItems()) === null || _c === void 0 ? void 0 : _c.map((item, index) => {
          return this.renderByDetailType(item, index);
        })]
      })]);
    }
  }
  /**
   * @description 渲染分页面板
   * @param {IPSPanelButton} modelJson 分页面板模型数据
   * @param {number} index 序列标识
   * @return {*}
   * @memberof AppMobPanel
   */


  renderTabPanel(modelJson, index) {
    var _a, _b; //  模型显示


    const detailModel = (_a = this.c.detailsModel) === null || _a === void 0 ? void 0 : _a[modelJson.name];

    if (detailModel && !detailModel.visible) {
      return;
    }

    const tabPages = modelJson.getPSPanelTabPages() || [];

    if (tabPages.length) {
      let _slot3; //  默认选中第一个分页


      if (!this.currentTab.value[modelJson.name]) {
        this.currentTab.value[modelJson.name] = tabPages[0].name;
      } // 绘制作为子项的布局绘制


      return (0, _vue.createVNode)(_appLayoutItem.AppLayoutItem, {
        "class": this.getDetailClass(modelJson),
        "layoutMode": (_b = modelJson.getParentPSModelObject().getPSLayout()) === null || _b === void 0 ? void 0 : _b.layout,
        "layoutPos": modelJson.getPSLayoutPos() || undefined
      }, {
        default: () => [(0, _vue.createVNode)("div", {
          "class": 'app-tabpanel__header'
        }, [(0, _vue.createVNode)((0, _vue.resolveComponent)("ion-segment"), {
          "value": this.currentTab.value[modelJson.name],
          "onIonChange": $event => this.segmentChanged(modelJson.name, $event)
        }, _isSlot(_slot3 = tabPages.map((page, index) => {
          let _slot2;

          var _a;

          return (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-segment-button"), {
            "value": page.name
          }, {
            default: () => [(0, _vue.createVNode)((0, _vue.resolveComponent)("ion-label"), null, _isSlot(_slot2 = this.$tl((_a = page.getCapPSLanguageRes()) === null || _a === void 0 ? void 0 : _a.lanResTag, page.caption)) ? _slot2 : {
              default: () => [_slot2]
            })]
          });
        })) ? _slot3 : {
          default: () => [_slot3]
        })]), (0, _vue.createVNode)("div", {
          "class": 'app-tabpanel__content'
        }, [tabPages.map((page, index) => {
          return this.renderByDetailType(page, index);
        })])]
      });
    }
  }
  /**
   * @description 渲染面板容器
   * @param {IPSPanelButton} modelJson 面板容器模型数据
   * @param {number} index 序列标识
   * @return {*}
   * @memberof AppMobPanel
   */


  renderContainer(modelJson, index) {
    let _slot4;

    var _a, _b, _c, _d;

    const {
      showCaption,
      caption,
      name
    } = modelJson;
    const detailModel = (_a = this.c.detailsModel) === null || _a === void 0 ? void 0 : _a[name]; //  模型显示

    if (detailModel && !detailModel.visible) {
      return null;
    }

    return (0, _vue.createVNode)(_appLayoutItem.AppLayoutItem, {
      "class": this.getDetailClass(modelJson),
      "layoutMode": (_b = modelJson.getParentPSModelObject().getPSLayout()) === null || _b === void 0 ? void 0 : _b.layout,
      "layoutPos": modelJson.getPSLayoutPos() || undefined
    }, {
      default: () => [showCaption ? (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-label"), null, _isSlot(_slot4 = this.$tl((_c = modelJson.getCapPSLanguageRes()) === null || _c === void 0 ? void 0 : _c.lanResTag, caption)) ? _slot4 : {
        default: () => [_slot4]
      }) : null, (0, _vue.createVNode)(_appLayoutContainer.AppLayoutContainer, {
        "layout": modelJson.getPSLayout() || undefined
      }, {
        default: () => [(_d = modelJson.getPSPanelItems()) === null || _d === void 0 ? void 0 : _d.map((item, index) => {
          return this.renderByDetailType(item, index);
        })]
      })]
    });
  }
  /**
   * @description 根据类型渲染面板项
   * @param {*} modelJson 面板项对象
   * @param {number} index 序列标识
   * @return {*}
   * @memberof AppMobPanel
   */


  renderByDetailType(modelJson, index) {
    var _a, _b;

    if ((_a = modelJson.getPSSysPFPlugin()) === null || _a === void 0 ? void 0 : _a.pluginCode) {
      const ctrlItemPluginInstance = App.getPluginService().getCtrlItemPluginByTag((_b = modelJson.getPSSysPFPlugin()) === null || _b === void 0 ? void 0 : _b.pluginCode);

      if (ctrlItemPluginInstance) {
        return ctrlItemPluginInstance.renderItem(this.c.data[modelJson.name], this.c, this);
      }
    } else {
      switch (modelJson.itemType) {
        case 'CONTAINER':
          return this.renderContainer(modelJson, index);

        case 'BUTTON':
          return this.renderButton(modelJson, index);

        case 'FIELD':
          return this.renderField(modelJson, index);

        case 'RAWITEM':
          return this.renderRawItem(modelJson, index);

        case 'TABPANEL':
          return this.renderTabPanel(modelJson, index);

        case 'TABPAGE':
          return this.renderTabPage(modelJson, index);
      }
    }
  }
  /**
   * @description 渲染面板部件根节点
   * @return {*}
   * @memberof AppMobPanel
   */


  renderRootPSPanelItems() {
    let _slot5;

    var _a, _b;

    const panelItems = ((_b = (_a = this.c.controlInstance).getRootPSPanelItems) === null || _b === void 0 ? void 0 : _b.call(_a)) || [];

    if (!panelItems.length) {
      return null;
    }

    return (0, _vue.createVNode)(_appLayoutContainer.AppLayoutContainer, {
      "class": this.getCtrlClassNames(),
      "layout": this.c.controlInstance.getPSLayout() || undefined
    }, _isSlot(_slot5 = panelItems.map((item, index) => {
      return this.renderByDetailType(item, index);
    })) ? _slot5 : {
      default: () => [_slot5]
    });
  }
  /**
   * @description 渲染面板部件
   * @return {*}
   * @memberof AppMobPanel
   */


  render() {
    if (!this.controlIsLoaded.value || !this.c.controlInstance) {
      return null;
    }

    return (0, _vue.createVNode)("div", {
      "class": Object.assign({}, this.classNames),
      "style": this.getRootStyle()
    }, [this.renderPullDownRefresh(), this.renderRootPSPanelItems()]);
  }

}

exports.AppMobPanel = AppMobPanel;
const AppMobPanelComponent = (0, _componentBase.GenerateComponent)(AppMobPanel, Object.keys(new _ibzCore.AppMobPanelProps()));
exports.AppMobPanelComponent = AppMobPanelComponent;