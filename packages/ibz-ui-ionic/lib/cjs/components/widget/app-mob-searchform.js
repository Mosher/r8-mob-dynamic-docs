"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppMobSearchFormComponent = exports.AppMobSearchForm = void 0;

var _vue = require("vue");

var _core = require("@ionic/core");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _appMobForm = require("./app-mob-form/app-mob-form");

/**
 * 移动端搜索表单
 *
 * @author chitanda
 * @date 2021-08-06 10:08:39
 * @export
 * @class AppMobSearchForm
 * @extends {ComponentBase}
 */
function _isSlot(s) {
  return typeof s === 'function' || Object.prototype.toString.call(s) === '[object Object]' && !(0, _vue.isVNode)(s);
}

class AppMobSearchForm extends _appMobForm.AppMobForm {
  constructor() {
    super(...arguments);
    /**
     * @description 搜索表单uuid
     * @type {string}
     * @memberof AppMobSearchForm
     */

    this.uuid = _ibzCore.Util.createUUID();
  }
  /**
   * @description 设置响应式
   * @memberof AppMobSearchForm
   */


  setup() {
    this.c = (0, _vue.shallowReactive)(this.getCtrlControllerByType('SEARCHFORM'));
    (0, _vue.watch)(() => this.c.isExpandSearchForm, () => {
      if (this.c.isExpandSearchForm) {
        this.openSearchForm();
      }
    }, {
      immediate: true
    }); // TODO 不能使用super.setup 这会导致c指向问题

    this.controlIsLoaded = (0, _vue.toRef)(this.c, 'controlIsLoaded');
    this.initReactive();
    this.emitCtrlEvent = this.emitCtrlEvent.bind(this);
    this.c.hooks.event.tap(this.emitCtrlEvent);
    this.emitCloseView = this.emitCloseView.bind(this);
    this.c.hooks.closeView.tap(this.emitCloseView);
  }
  /**
   * @description 初始化响应数据
   * @memberof AppMobSearchForm
   */


  initReactive() {
    super.initReactive();
    this.c.isExpandSearchForm = (0, _vue.reactive)(this.c.isExpandSearchForm);
  }
  /**
   * @description 重置
   * @memberof AppMobSearchForm
   */


  onReset() {
    this.c.onReset();
  }
  /**
   * @description 搜索
   * @memberof AppMobSearchForm
   */


  onSearch() {
    this.c.onSearch();
    this.closeSearchForm();
  }
  /**
   * @description 打开搜索表单
   * @memberof AppMobSearchForm
   */


  async openSearchForm() {
    await _core.menuController.enable(true, this.uuid);

    _core.menuController.open(this.uuid);
  }
  /**
   * @description 关闭搜索表单
   * @memberof AppMobSearchForm
   */


  closeSearchForm() {
    _core.menuController.close(this.uuid);
  }
  /**
   * @description 根据成员类型绘制
   * @param {*} modelJson 模型数据
   * @param {number} index 索引
   * @return {*}
   * @memberof AppMobSearchForm
   */


  renderByDetailType(modelJson, index) {
    var _a, _b;

    if ((_a = modelJson.getPSSysPFPlugin()) === null || _a === void 0 ? void 0 : _a.pluginCode) {
      const ctrlItemPluginInstance = App.getPluginService().getCtrlItemPluginByTag(((_b = modelJson.getPSSysPFPlugin()) === null || _b === void 0 ? void 0 : _b.pluginCode) || '');

      if (ctrlItemPluginInstance) {
        return ctrlItemPluginInstance.renderItem(this.c.data[modelJson.name], this.c, this);
      }
    } else {
      switch (modelJson.detailType) {
        case 'FORMPAGE':
          return this.renderFormPage(modelJson, index);

        case 'GROUPPANEL':
          return this.renderGroupPanel(modelJson, index);

        case 'FORMITEM':
          return this.renderFormItem(modelJson, index);

        default:
          _ibzCore.LogUtil.log(`${this.$tl('share.notsupported', '暂未实现')} ${modelJson.detailType} ${this.$tl('widget.mobsearchform.detailtype', '类型表单成员')}`);

      }
    }
  }
  /**
   * @description 绘制搜索表单
   * @param {*} className 类名
   * @param {*} controlStyle 样式
   * @return {*}
   * @memberof AppMobSearchForm
   */


  renderSearchForm(className, controlStyle) {
    let _slot, _slot2, _slot3;

    return [(0, _vue.createVNode)((0, _vue.resolveComponent)("ion-header"), {
      "class": 'searchForm-header'
    }, {
      default: () => [(0, _vue.createVNode)((0, _vue.resolveComponent)("ion-toolbar"), null, {
        default: () => [(0, _vue.createVNode)((0, _vue.resolveComponent)("ion-buttons"), {
          "slot": 'end'
        }, {
          default: () => [(0, _vue.createVNode)((0, _vue.resolveComponent)("ion-button"), {
            "onClick": () => this.openSearchForm(),
            "id": 'searchForm'
          }, _isSlot(_slot = this.$tl('widget.mobsearchform.filter', '过滤')) ? _slot : {
            default: () => [_slot]
          })]
        })]
      })]
    }), (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-menu"), {
      "side": 'end',
      "menuId": this.uuid,
      "contentId": 'searchForm'
    }, {
      default: () => [(0, _vue.createVNode)((0, _vue.resolveComponent)("ion-content"), null, {
        default: () => [(0, _vue.createVNode)("div", {
          "class": className,
          "style": controlStyle
        }, [this.renderFormContent()])]
      }), (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-footer"), {
        "class": 'searchForm-footer'
      }, {
        default: () => [(0, _vue.createVNode)((0, _vue.resolveComponent)("ion-toolbar"), null, {
          default: () => [(0, _vue.createVNode)((0, _vue.resolveComponent)("ion-button"), {
            "size": 'small',
            "onClick": () => this.onSearch()
          }, _isSlot(_slot2 = this.$tl('widget.mobsearchform.search', '搜索')) ? _slot2 : {
            default: () => [_slot2]
          }), (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-button"), {
            "size": 'small',
            "onClick": () => this.onReset()
          }, _isSlot(_slot3 = this.$tl('widget.mobsearchform.reset', '重置')) ? _slot3 : {
            default: () => [_slot3]
          })]
        })]
      })]
    })];
  }
  /**
   * @description 绘制快速搜索表单
   * @param {*} className 类名
   * @param {*} controlStyle 样式
   * @memberof AppMobSearchForm
   */


  renderQuickSearchForm(className, controlStyle) {
    return (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-header"), null, {
      default: () => [(0, _vue.createVNode)("div", {
        "class": className,
        "style": controlStyle
      }, [this.renderFormContent()])]
    });
  }
  /**
   * @description 绘制搜索表单内容
   * @return {*}
   * @memberof AppMobSearchForm
   */


  renderSearchFormContent() {
    const controlStyle = {
      width: this.c.controlInstance.width ? `${this.c.controlInstance.width}px` : this.c.controlInstance.formWidth ? `${this.c.controlInstance.formWidth}` : '',
      height: this.c.controlInstance.height ? `${this.c.controlInstance.height}px` : ''
    };
    const className = Object.assign({}, this.classNames);

    if (Object.is(this.c.controlInstance.name, 'quicksearchform') || this.c.expandSearchForm) {
      return this.renderQuickSearchForm(className, controlStyle);
    } else {
      return this.renderSearchForm(className, controlStyle);
    }
  }
  /**
   * @description 绘制
   * @return {*}
   * @memberof AppMobSearchForm
   */


  render() {
    if (!this.controlIsLoaded.value) {
      return null;
    }

    return this.renderSearchFormContent();
  }

} // 搜索表单部件


exports.AppMobSearchForm = AppMobSearchForm;
const AppMobSearchFormComponent = (0, _componentBase.GenerateComponent)(AppMobSearchForm, Object.keys(new _ibzCore.AppMobSearchFormProps()));
exports.AppMobSearchFormComponent = AppMobSearchFormComponent;