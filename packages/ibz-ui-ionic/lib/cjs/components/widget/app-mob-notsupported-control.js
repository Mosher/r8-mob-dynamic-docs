"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppMobNotSupportedControlComponent = exports.AppMobNotSupportedControl = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _ctrlComponentBase = require("./ctrl-component-base");

/**
 * 未支持部件
 *
 * @export
 * @class AppMobNotSupportedControl
 * @extends {ComponentBase}
 */
class AppMobNotSupportedControl extends _ctrlComponentBase.CtrlComponentBase {
  /**
   * 设置响应式
   *
   * @public
   * @memberof AppMobNotSupportedControl
   */
  setup() {
    this.c = (0, _vue.shallowReactive)(new _ibzCore.AppCtrlControllerBase(this.props));
    super.setup();
  }
  /**
   * 绘制内容
   *
   * @public
   * @memberof AppMobNotSupportedControl
   */


  render() {
    var _a, _b;

    if (!this.controlIsLoaded.value) {
      return null;
    }

    const flexStyle = 'width: 100%; height: 100%; overflow: auto; display: flex;justify-content:center;align-items:center;';
    return (0, _vue.createVNode)("div", {
      "class": 'app-ctrl',
      "style": flexStyle
    }, [`${this.$tl('share.notsupported', '暂未支持')} ${(_b = (_a = this.c) === null || _a === void 0 ? void 0 : _a.controlInstance) === null || _b === void 0 ? void 0 : _b.controlType} ${this.$tl('share.widget', '部件')}`]);
  }

} // 应用菜单组件


exports.AppMobNotSupportedControl = AppMobNotSupportedControl;
const AppMobNotSupportedControlComponent = (0, _componentBase.GenerateComponent)(AppMobNotSupportedControl, Object.keys(new _ibzCore.AppCtrlProps()));
exports.AppMobNotSupportedControlComponent = AppMobNotSupportedControlComponent;