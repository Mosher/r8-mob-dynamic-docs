"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppMobPortletComponent = exports.AppMobPortlet = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _ctrlComponentBase = require("./ctrl-component-base");

var _componentBase = require("../component-base");

/**
 * 移动端门户部件
 *
 * @class AppMobPortlet
 */
function _isSlot(s) {
  return typeof s === 'function' || Object.prototype.toString.call(s) === '[object Object]' && !(0, _vue.isVNode)(s);
}

class AppMobPortlet extends _ctrlComponentBase.CtrlComponentBase {
  constructor() {
    super(...arguments);
    /**
     * UUID
     *
     * @type {string}
     * @memberof AppMobPortlet
     */

    this.uuid = _ibzCore.Util.createUUID();
    /**
     * 操作按钮集合
     *
     * @type {Ref<IParam[]}
     * @memberof AppMobPortlet
     */

    this.actionSheetButtons = (0, _vue.ref)([]);
    /**
     * 行为操作显示状态
     *
     * @type {Ref<IParam[]}
     * @memberof AppMobPortlet
     */

    this.actionSheetShowStatus = (0, _vue.ref)(false);
  }
  /**
   * 设置响应式
   *
   * @memberof AppMobPortlet
   */


  setup() {
    this.c = (0, _vue.shallowReactive)(this.getCtrlControllerByType('PORTLET'));
    super.setup();
  }
  /**
   * 获取门户部件大小
   * @memberof AppMobPortlet
   */


  getSize() {
    const size = {};
    const {
      height,
      width
    } = this.c.controlInstance;

    if (height) {
      Object.assign(size, {
        height: height + 'px'
      });
    }

    if (width) {
      Object.assign(size, {
        width: width + 'px'
      });
    }

    return size;
  }
  /**
   * 打开动作面板
   *
   * @param event 源事件对象
   * @memberof AppMobPortlet
   */


  openActionSheet(event) {
    const buttons = [];
    this.c.actionBarModel.forEach(model => {
      buttons.push({
        text: model.name,
        icon: model.icon,
        handler: () => {
          this.onActionBarItemClick(model.viewLogicName, event);
        }
      });
    });
    buttons.push({
      text: this.$tl('share.cancel', '取消'),
      role: 'cancel',
      handler: () => {}
    });
    const options = {
      buttons: buttons
    };
    App.getActionSheetService().create(options).then(result => {});
  }
  /**
   * 操作栏按钮点击
   *
   * @param {string} tag 标识
   * @param {*} event 源事件对象
   * @memberof AppMobPortlet
   */


  onActionBarItemClick(tag, event) {
    if (this.c.actionBarItemClick && this.c.actionBarItemClick instanceof Function) {
      this.c.actionBarItemClick(tag, event);
    }
  }
  /**
   * 渲染部件
   *
   * @memberof AppMobPortlet
   */


  renderControl() {
    var _a, _b;

    const controls = (_b = (_a = this.c.controlInstance).getPSControls) === null || _b === void 0 ? void 0 : _b.call(_a);

    if (controls) {
      return controls.map(control => {
        return this.computeTargetCtrlData(control);
      });
    }
  }
  /**
   * 渲染直接内容
   *
   * @memberof AppMobPortlet
   */


  renderRawItem() {
    const {
      rawItemHeight,
      rawItemWidth,
      contentType,
      rawContent,
      htmlContent
    } = this.c.controlInstance;
    const sysCss = this.c.controlInstance.getPSSysCss();
    const sysImage = this.c.controlInstance.getPSSysImage();
    const rawStyle = (rawItemHeight > 0 ? `height:${rawItemHeight}px` : '') + (rawItemWidth > 0 ? `width:${rawItemWidth}px` : '');
    const newRawContent = rawContent;

    if (rawContent) {//  TODO  国际化内容支持
      // const items = rawContent.match(/\{{(.+?)\}}/g);
      // if (items) {
      //     items.forEach((item: string) => {
      //       rawContent = rawContent.replace(/\{{(.+?)\}}/, item.substring(2, item.length - 2));
      //     });
      // }
    }

    let element = undefined;

    if (contentType == 'HTML' && htmlContent) {
      element = (0, _vue.h)('div', {
        innerHTML: htmlContent
      });
    }

    let content;

    switch (contentType) {
      case 'HTML':
        content = element ? element : htmlContent;
        break;

      case 'RAW':
        content = rawContent;
        break;

      case 'IMAGE':
        content = (0, _vue.createVNode)("img", {
          "src": sysImage === null || sysImage === void 0 ? void 0 : sysImage.imagePath
        }, null);
        break;

      case 'MARKDOWN':
        content = (0, _vue.createVNode)("div", null, [this.$tl('widget.mobportlet.nomarkdown', 'MARKDOWN直接内容暂未支持')]);
        break;
    }

    return content;
  }
  /**
   * 渲染HTML内容
   *
   * @memberof AppMobPortlet
   */


  renderHtml() {
    const height = this.c.controlInstance.height > 0 ? this.c.controlInstance.height : 400;
    const pageUrl = this.c.controlInstance.pageUrl;
    const iframeStyle = `height: ${height}px; width: 100%; border-width: 0px;`;
    return (0, _vue.createVNode)("iframe", {
      "src": pageUrl,
      "style": iframeStyle
    }, null);
  }
  /**
   * 渲染工具栏
   *
   * @memberof AppMobPortlet
   */


  renderToolbar() {
    return (0, _vue.createVNode)("div", null, [this.$tl('widget.mobportlet.toolbar', '工具栏')]);
  }
  /**
   * 渲染操作栏
   *
   * @memberof AppMobPortlet
   */


  renderActionBar() {
    return (0, _vue.h)((0, _vue.resolveComponent)('app-actionbar'), {
      items: this.c.actionBarModel,
      modelService: this.c.modelService,
      onItemClick: (tag, event) => {
        this.onActionBarItemClick(tag, event);
      }
    });
  }
  /**
   * 渲染自定义内容
   *
   * @memberof AppMobPortlet
   */


  renderCustom() {
    return (0, _vue.createVNode)("div", null, [this.$tl('widget.mobportlet.rendercustom', '绘制自定义')]);
  }
  /**
   * 渲染应用菜单
   *
   * @memberof AppMobPortlet
   */


  renderAppMenu() {
    var _a;

    const menu = (_a = this.c.controlInstance.getPSControls()) === null || _a === void 0 ? void 0 : _a[0];

    if (menu) {
      return this.computeTargetCtrlData(menu, {
        ctrlShowMode: 'QUICKMENU'
      });
    }
  }
  /**
   * 渲染视图
   *
   * @memberof AppMobPortlet
   */


  renderView() {
    var _a, _b;

    const portletView = (_a = this.c.controlInstance) === null || _a === void 0 ? void 0 : _a.getPortletPSAppView();
    const targetViewComponent = portletView ? App.getComponentService().getViewTypeComponent(portletView === null || portletView === void 0 ? void 0 : portletView.viewType, portletView === null || portletView === void 0 ? void 0 : portletView.viewStyle, (_b = portletView === null || portletView === void 0 ? void 0 : portletView.getPSSysPFPlugin()) === null || _b === void 0 ? void 0 : _b.pluginCode) : '';
    return (0, _vue.createVNode)("div", {
      "class": 'portlet-view-container'
    }, [targetViewComponent ? (0, _vue.h)(targetViewComponent, {
      key: this.uuid,
      class: `portlet-view`,
      viewPath: portletView === null || portletView === void 0 ? void 0 : portletView.modelPath,
      viewShowMode: 'EMBEDDED',
      navContext: _ibzCore.Util.deepCopy(this.c.context),
      navParam: _ibzCore.Util.deepCopy(this.c.viewParam),
      isLoadDefault: false,
      viewState: this.c.viewState,
      onViewEvent: ({
        viewName,
        action,
        data
      }) => {
        this.c.handleViewEvent(viewName, action, data);
      }
    }) : null]);
  }
  /**
   * 根据门户部件类型渲染内容
   *
   * @memberof AppMobPortlet
   */


  renderByPortletType() {
    switch (this.c.controlInstance.portletType) {
      case 'VIEW':
        return this.renderView();

      case 'APPMENU':
        return this.renderAppMenu();

      case 'CUSTOM':
        return this.renderCustom();

      case 'ACTIONBAR':
        return this.renderActionBar();

      case 'TOOLBAR':
        return this.renderToolbar();

      case 'HTML':
        return this.renderHtml();

      case 'RAWITEM':
        return this.renderRawItem();

      default:
        return this.renderControl();
    }
  }
  /**
   * 渲染标题
   *
   * @memberof AppMobPortlet
   */


  renderTitle() {
    var _a, _b;

    const {
      title,
      showTitleBar
    } = this.c.controlInstance;
    const sysImage = this.c.controlInstance.getPSSysImage();
    const showTitle = showTitleBar && title || this.c.actionBarModel.length > 0;

    if (showTitle) {
      return (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-card-header"), {
        "class": 'portlet-header'
      }, {
        default: () => [(0, _vue.createVNode)((0, _vue.resolveComponent)("app-icon"), {
          "icon": sysImage
        }, null), (0, _vue.createVNode)("span", null, [this.$tl((_b = (_a = this.c.controlInstance) === null || _a === void 0 ? void 0 : _a.getTitlePSLanguageRes()) === null || _b === void 0 ? void 0 : _b.lanResTag, title)]), this.c.actionBarModel.length > 0 ? (0, _vue.createVNode)("div", {
          "class": 'portlet-header-action'
        }, [(0, _vue.createVNode)((0, _vue.resolveComponent)("app-icon"), {
          "name": 'ellipsis-horizontal-outline',
          "onClick": event => {
            this.openActionSheet(event);
          }
        }, null)]) : null]
      });
    }
  }
  /**
   * 渲染
   *
   * @memberof AppMobPortlet
   */


  render() {
    let _slot;

    if (!this.c.controlIsLoaded) {
      return;
    }

    Object.assign(this.classNames, {
      [`portlet-${this.c.controlInstance.portletType.toLowerCase()}`]: true
    });
    return (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-card"), {
      "class": Object.assign({}, this.classNames),
      "style": this.getSize()
    }, {
      default: () => [this.renderTitle(), (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-card-content"), {
        "class": 'portlet-content'
      }, _isSlot(_slot = this.renderByPortletType()) ? _slot : {
        default: () => [_slot]
      })]
    });
  }

} //  移动端门户部件组件


exports.AppMobPortlet = AppMobPortlet;
const AppMobPortletComponent = (0, _componentBase.GenerateComponent)(AppMobPortlet, Object.keys(new _ibzCore.AppMobPortletProps()));
exports.AppMobPortletComponent = AppMobPortletComponent;