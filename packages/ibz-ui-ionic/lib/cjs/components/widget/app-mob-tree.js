"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppMobTreeComponent = exports.AppMobTree = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _mdCtrlComponentBase = require("./md-ctrl-component-base");

/**
 * 移动端树部件
 *
 * @export
 * @class AppMobTree
 * @extends MDCtrlComponentBase
 */
function _isSlot(s) {
  return typeof s === 'function' || Object.prototype.toString.call(s) === '[object Object]' && !(0, _vue.isVNode)(s);
}

class AppMobTree extends _mdCtrlComponentBase.MDCtrlComponentBase {
  constructor() {
    super(...arguments);
    /**
     * @description 长按定时器
     * @type {Ref<any>}
     * @memberof AppMobTree
     */

    this.touchTimer = (0, _vue.ref)(0);
    /**
     * @description 当前激活节点
     * @type {*}
     * @memberof AppMobTree
     */

    this.activeNode = (0, _vue.reactive)({});
    /**
     * @description 是否打开上下文菜单
     * @type {*}
     * @memberof AppMobTree
     */

    this.isShowContextMenu = true;
  }
  /**
   * @description 初始化响应式
   * @memberof AppMobTree
   */


  setup() {
    this.c = (0, _vue.shallowReactive)(this.getCtrlControllerByType('TREE'));
    super.setup();
  }
  /**
   * @description 根据类型加载节点模型
   * @param {string} type
   * @return {*}  {(IPSDETreeNode | undefined)}
   * @memberof AppMobTree
   */


  getNodeByType(type) {
    const treeNodes = this.c.controlInstance.getPSDETreeNodes() || [];
    return treeNodes.find(node => node.nodeType.toLowerCase() == type.toLowerCase());
  }
  /**
   * @description 清除定时器
   * @memberof AppMobTree
   */


  clearTimer() {
    clearInterval(this.touchTimer.value);
    this.touchTimer.value = 0;
  }
  /**
   * @description 长按开始
   * @param {*} item 节点数据
   * @param {*} event 事件源
   * @memberof AppMobTree
   */


  onTouchStart(item, event) {
    this.touchTimer.value = setInterval(() => {
      this.activeNode.value = {
        key: item.id,
        type: item.id.split(';')[0],
        mouseEvent: event,
        navDatas: item.curData,
        show: true
      };
      this.isShowContextMenu = true;
      this.clearTimer();
    }, 500);
  }
  /**
   * @description 处理树节点点击事件
   * @param {IParam} item 树节点数据
   * @param {IPSDETreeNode} node 树节点模型
   * @param {*} event 源事件对象
   * @memberof AppMobTree
   */


  handleTreeNodeClick(item, node, event) {
    var _a;

    const hasChildren = (_a = node.hasPSDETreeNodeRSs) === null || _a === void 0 ? void 0 : _a.call(node);

    if (hasChildren) {
      this.loadChildNodeData(item, event);
    } else {
      if (Object.is('SELECT', this.c.ctrlShowMode) && this.c.isMultiple) {
        this.treeNodeChecked(item, event);
      } else {
        this.treeNodeSelect(item, event);
      }
    }
  }
  /**
   * @description 单选选中数据变化
   * @param {IParam} item 节点数据
   * @param {MouseEvent} event 事件源
   * @memberof AppMobTree
   */


  treeNodeSelect(item, event) {
    if (this.c.handleTreeNodeSelect && this.c.handleTreeNodeSelect instanceof Function) {
      this.c.handleTreeNodeSelect(item, event);
    }
  }
  /**
   * @description 多项选择选中数据
   * @param {*} event 数据源
   * @memberof AppMobTree
   */


  treeNodeChecked(item, event) {
    event.stopPropagation();

    if (this.c.handleTreeNodeChecked && this.c.handleTreeNodeChecked instanceof Function) {
      this.c.handleTreeNodeChecked(item, event);
    }
  }
  /**
   * @description 树节点点击
   * @param {IParam} item 节点数据
   * @param {MouseEvent} event 事件源
   * @memberof AppMobTree
   */


  loadChildNodeData(item, event) {
    if (this.c.loadChildNodeData && this.c.loadChildNodeData instanceof Function) {
      this.c.loadChildNodeData(item, event);
    }
  }
  /**
   * @description 渲染单选树
   * @return {*}
   * @memberof AppMobTree
   */


  renderSingleTree() {
    let _slot;

    var _a;

    const value = (_a = this.c.selections[0]) === null || _a === void 0 ? void 0 : _a.id;
    return (0, _vue.createVNode)("div", {
      "class": "tree-node--container is-single"
    }, [(0, _vue.createVNode)((0, _vue.resolveComponent)("ion-radio-group"), {
      "class": "tree-node--radio-group",
      "value": value
    }, _isSlot(_slot = this.c.items.map(item => {
      const node = this.getNodeByType(item.nodeType);
      return this.renderTreeNode(node, item);
    })) ? _slot : {
      default: () => [_slot]
    })]);
  }
  /**
   * @description 渲染多选树
   * @return {*}
   * @memberof AppMobTree
   */


  renderMultipleTree() {
    return (0, _vue.createVNode)("div", {
      "class": "tree-node--container is-multiple"
    }, [this.c.items.map(item => {
      const node = this.getNodeByType(item.nodeType);
      return this.renderTreeNode(node, item);
    })]);
  }
  /**
   * @description 渲染默认树
   * @return {*}
   * @memberof AppMobTree
   */


  renderDefaultTree() {
    return (0, _vue.createVNode)("div", {
      "class": "tree-node--container"
    }, [this.c.items.map(item => {
      const node = this.getNodeByType(item.nodeType);
      return this.renderTreeNode(node, item);
    })]);
  }
  /**
   * @description 渲染树主体内容
   * @return {*}
   * @memberof AppMobTree
   */


  renderTreeMainContent() {
    if (Object.is('SELECT', this.c.ctrlShowMode)) {
      if (this.c.isMultiple) {
        return this.renderMultipleTree();
      } else {
        return this.renderSingleTree();
      }
    } else {
      return this.renderDefaultTree();
    }
  }
  /**
   * @description
   * @param {IParam} node
   * @return {*}
   * @memberof AppMobTree
   */

  /**
   * @description 渲染树节点标题
   * @param {IParam} item 树节点数据
   * @param {boolean} [hasChildren] 是否有子节点
   * @return {*}
   * @memberof AppMobTree
   */


  renderNodeLabel(item, hasChildren) {
    // 绘制图标
    let iconElement = null;

    if (item.iconCustomCode) {
      const icon = '';

      if (item.iconScriptCode.indexOf('return') !== -1) {
        item.iconScriptCode = item.iconScriptCode.replace(new RegExp('return', 'g'), `icon =`);
      }

      eval(item.iconScriptCode);
      iconElement = (0, _vue.h)('span', {
        innerHTML: icon
      });
    } else if (item.icon) {
      iconElement = (0, _vue.createVNode)((0, _vue.resolveComponent)("app-icon"), {
        "icon": item.icon
      }, null);
    } else if (item.iconSrc) {
      iconElement = (0, _vue.createVNode)((0, _vue.resolveComponent)("app-icon"), {
        "iconSrc": item.iconSrc
      }, null);
    } else if (this.c.controlInstance.outputIconDefault) {
      iconElement = (0, _vue.createVNode)((0, _vue.resolveComponent)("app-icon"), {
        "name": 'home'
      }, null);
    } // 绘制显示文本


    let textElement = null;

    if (item.textCustomCode) {
      const text = '';

      if (item.textScriptCode.indexOf('return') !== -1) {
        item.textScriptCode = item.textScriptCode.replace(new RegExp('return', 'g'), `text =`);
      }

      eval(item.textScriptCode);
      textElement = (0, _vue.h)('span', {
        innerHTML: text
      });
    } else {
      textElement = (0, _vue.createVNode)("span", null, [item.text]);
    } // 树节点计数器


    const nodeCountBadge = {
      count: this.c.getCounterData(item),
      showZero: item.counterMode !== 1,
      offset: [10, 10]
    };
    return (0, _vue.withDirectives)((0, _vue.createVNode)((0, _vue.resolveComponent)("ion-label"), {
      "title": item.tooltip ? item.tooltip : null,
      "class": 'tree-node--label'
    }, {
      default: () => [iconElement ? (0, _vue.createVNode)("span", {
        "class": "tree-node--label-icon"
      }, [iconElement]) : null, (0, _vue.createVNode)("span", {
        "class": "tree-node--label-content"
      }, [item.isUseLangRes ? this.$tl(item.lanResTag, textElement) : textElement]), hasChildren ? (0, _vue.createVNode)((0, _vue.resolveComponent)("app-icon"), {
        "class": "tree-node--select-icon",
        "name": item.expanded ? 'chevron-down-outline' : 'chevron-forward-outline'
      }, null) : null]
    }), [[(0, _vue.resolveDirective)("badge"), nodeCountBadge]]);
  }
  /**
   * @description 渲染树节点
   * @param {IPSDETreeNode} node
   * @param {IParam} item
   * @return {*}
   * @memberof AppMobTree
   */


  renderTreeNode(node, item) {
    var _a;

    const hasChildren = (_a = node === null || node === void 0 ? void 0 : node.hasPSDETreeNodeRSs) === null || _a === void 0 ? void 0 : _a.call(node);
    return (0, _vue.createVNode)("div", {
      "class": ["tree-node", item.cssName]
    }, [(0, _vue.createVNode)((0, _vue.resolveComponent)("ion-item"), {
      "class": "tree-node--content",
      "onTouchstart": event => {
        this.onTouchStart(node, event);
      },
      "onTouchend": event => {
        this.clearTimer();
      },
      "onTouchmove": event => {
        this.clearTimer();
      },
      "onClick": event => {
        this.handleTreeNodeClick(item, node, event);
      }
    }, {
      default: () => [Object.is('SELECT', this.c.ctrlShowMode) && !hasChildren ? this.c.isMultiple ? (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-checkbox"), {
        "slot": "start",
        "value": item.selected
      }, null) : (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-radio"), {
        "slot": "start",
        "value": item.id
      }, null) : null, this.renderNodeLabel(item, hasChildren ? hasChildren : false)]
    }), hasChildren && item.expanded && item.children && item.children.length > 0 ? (0, _vue.createVNode)("div", {
      "class": "tree-node--children"
    }, [item.children.map(_item => {
      const _node = this.getNodeByType(_item.nodeType);

      return this.renderTreeNode(_node, _item);
    })]) : null]);
  }
  /**
   * @description 渲染上下文菜单
   * @return {*}
   * @memberof AppMobTree
   */


  renderContextMenu() {
    var _a, _b, _c, _d, _e;

    const type = ((_a = this.activeNode.value) === null || _a === void 0 ? void 0 : _a.type) || '';
    const mouseEvent = (_b = this.activeNode.value) === null || _b === void 0 ? void 0 : _b.mouseEvent;
    const navDatas = (_c = this.activeNode.value) === null || _c === void 0 ? void 0 : _c.navDatas;

    if (type && mouseEvent) {
      const treeNode = (this.c.controlInstance.getPSDETreeNodes() || []).find(node => {
        return node.nodeType.toLowerCase() == type.toLowerCase();
      });

      const contextMenu = _ibzCore.ModelTool.findPSControlByName((_e = (_d = treeNode === null || treeNode === void 0 ? void 0 : treeNode.getPSDEContextMenu) === null || _d === void 0 ? void 0 : _d.call(treeNode)) === null || _e === void 0 ? void 0 : _e.name, this.c.controlInstance.getPSControls());

      if (treeNode && contextMenu && this.isShowContextMenu) {
        this.isShowContextMenu = false;
        const otherParams = {
          key: _ibzCore.Util.createUUID(),
          navDatas: [navDatas],
          mouseEvent: mouseEvent,
          contextMenuActionModel: {}
        };
        return this.computeTargetCtrlData(contextMenu, otherParams);
      }
    }
  }
  /**
   * @description 渲染树部件
   * @return {*}
   * @memberof AppMobTree
   */


  render() {
    if (!this.controlIsLoaded.value) {
      return;
    }

    const {
      width,
      height
    } = this.c.controlInstance;
    const controlstyle = {
      width: width ? `${width}px` : '100%',
      height: height ? `${height}px` : '100%'
    };
    return (0, _vue.createVNode)("div", {
      "class": Object.assign({}, this.classNames),
      "style": controlstyle
    }, [this.renderPullDownRefresh(), this.renderContextMenu(), this.renderTreeMainContent()]);
  }

}

exports.AppMobTree = AppMobTree;
const AppMobTreeComponent = (0, _componentBase.GenerateComponent)(AppMobTree, Object.keys(new _ibzCore.AppMobTreeProps()));
exports.AppMobTreeComponent = AppMobTreeComponent;