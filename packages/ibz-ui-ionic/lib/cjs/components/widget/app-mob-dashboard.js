"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppMobDashboardComponent = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _ctrlComponentBase = require("./ctrl-component-base");

/**
 * 移动端数据看板部件
 *
 * @class AppMobDashboard
 */
class AppMobDashboard extends _ctrlComponentBase.CtrlComponentBase {
  /**
   * @description 设置响应式
   * @public
   * @memberof AppMobDashboard
   */
  setup() {
    this.c = (0, _vue.shallowReactive)(this.getCtrlControllerByType('DASHBOARD'));
    super.setup();
  }
  /**
   * @description 处理自定义面板
   * @memberof AppMobDashboard
   */


  handleCustom(event) {
    if (this.c.handleCustom && this.c.handleCustom instanceof Function) {
      const view = {
        viewComponentName: 'app-dashboard-design'
      };
      this.c.handleCustom(view, event).then(ret => {});
    }
  }
  /**
   * @description 渲染门户部件
   * @memberof AppMobDashboard
   */


  renderPortlet(item, isCustom = false) {
    return this.computeTargetCtrlData(item, isCustom ? {
      isAdaptiveSize: true,
      class: 'dashboard-item user-customize'
    } : undefined);
  }
  /**
   * @description 渲染自定义数据看板部件
   * @memberof AppMobDashboard
   */


  renderCustomizedDashboard() {
    if (this.c.customDashboardModelData.length > 0) {
      return this.c.customDashboardModelData.map(item => {
        return this.renderPortlet(item.modelData, true);
      });
    }
  }
  /**
   * @description 渲染静态数据看板内容
   * @memberof AppMobDashboard
   */


  renderStaticDashboard() {
    const controls = this.c.controlInstance.getPSControls() || [];
    return controls.map((control, index) => {
      return this.renderPortlet(control);
    });
  }
  /**
   * @description 渲染
   * @memberof AppMobDashboard
   */


  render() {
    if (!this.controlIsLoaded) {
      return;
    }

    return (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-grid"), {
      "class": Object.assign({}, this.classNames)
    }, {
      default: () => [this.c.enableCustomized ? (0, _vue.createVNode)("div", {
        "class": 'dashboard-customized',
        "onClick": event => {
          this.handleCustom(event);
        }
      }, [this.$tl('widget.mobdashboard.title', '定制仪表盘'), (0, _vue.createVNode)((0, _vue.resolveComponent)("app-icon"), {
        "name": 'settings-outline'
      }, null)]) : null, this.c.hasCustomized ? this.renderCustomizedDashboard() : this.renderStaticDashboard()]
    });
  }

} //  移动端数据看板部件组件


const AppMobDashboardComponent = (0, _componentBase.GenerateComponent)(AppMobDashboard, Object.keys(new _ibzCore.AppMobDashBoardProps()));
exports.AppMobDashboardComponent = AppMobDashboardComponent;