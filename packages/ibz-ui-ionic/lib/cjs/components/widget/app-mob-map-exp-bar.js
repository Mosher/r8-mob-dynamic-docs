"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppMobMapExpBarComponent = exports.AppMobMapExpBar = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _expBarCtrlComponentBase = require("./exp-bar-ctrl-component-base");

/**
 * @description 移动端地图导航部件
 * @export
 * @class AppMobMapExpBar
 * @extends {DEViewComponentBase<AppMobMapExpBarProps>}
 */
class AppMobMapExpBar extends _expBarCtrlComponentBase.ExpBarCtrlComponentBase {
  /**
   * 设置响应式
   *
   * @public
   * @memberof AppMobMapExpBar
   */
  setup() {
    this.c = (0, _vue.shallowReactive)(this.getCtrlControllerByType('MAPEXPBAR'));
    super.setup();
  }
  /**
   * @description 渲染导航栏部件
   * @return {*}
   * @memberof AppMobChartExpBar
   */


  render() {
    if (!this.controlIsLoaded.value) {
      return null;
    }

    const {
      width,
      height
    } = this.c.controlInstance;
    const controlStyle = {
      width: width ? width + 'px' : '',
      height: height ? height + 'px' : ''
    };
    return (0, _vue.createVNode)("div", {
      "class": Object.assign({
        'exp-bar': true
      }, this.classNames),
      "style": controlStyle
    }, [(0, _vue.createVNode)("div", {
      "class": 'exp-bar-container'
    }, [(0, _vue.createVNode)("div", {
      "class": 'container__multi_data_ctrl'
    }, [this.renderXDataControl()])])]);
  }

} // 移动端地图导航部件 组件


exports.AppMobMapExpBar = AppMobMapExpBar;
const AppMobMapExpBarComponent = (0, _componentBase.GenerateComponent)(AppMobMapExpBar, Object.keys(new _ibzCore.AppMobMapExpBarProps()));
exports.AppMobMapExpBarComponent = AppMobMapExpBarComponent;