"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppMobListExpBarComponent = exports.AppMobListExpBar = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _expBarCtrlComponentBase = require("./exp-bar-ctrl-component-base");

/**
 * @description 移动端列表导航部件
 * @export
 * @class AppMobListExpBar
 * @extends {DEViewComponentBase<AppMobListExpBarProps>}
 */
class AppMobListExpBar extends _expBarCtrlComponentBase.ExpBarCtrlComponentBase {
  /**
   * 设置响应式
   *
   * @public
   * @memberof AppMobListExpBar
   */
  setup() {
    this.c = (0, _vue.shallowReactive)(this.getCtrlControllerByType('LISTEXPBAR'));
    super.setup();
  }

} // 移动端列表导航部件 组件


exports.AppMobListExpBar = AppMobListExpBar;
const AppMobListExpBarComponent = (0, _componentBase.GenerateComponent)(AppMobListExpBar, Object.keys(new _ibzCore.AppMobListExpBarProps()));
exports.AppMobListExpBarComponent = AppMobListExpBarComponent;