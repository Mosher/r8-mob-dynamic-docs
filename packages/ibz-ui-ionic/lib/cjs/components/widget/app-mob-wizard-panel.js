"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppMobWizardPanelComponent = exports.AppMobWizardPanel = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _ctrlComponentBase = require("./ctrl-component-base");

/**
 * @description 移动端向导面板部件
 * @export
 * @class AppMobWizardPanel
 * @extends {DEViewComponentBase<AppMobWizardPanelProps>}
 */
class AppMobWizardPanel extends _ctrlComponentBase.CtrlComponentBase {
  /**
   * @description 设置响应式
   * @memberof AppMobWizardPanel
   */
  setup() {
    this.c = (0, _vue.shallowReactive)(this.getCtrlControllerByType('WIZARDPANEL'));
    super.setup();
  }
  /**
   * @description 获取激活步骤表单实例对象
   * @protected
   * @param {string} name
   * @return {*}  {(IPSDEEditForm | undefined)}
   * @memberof AppMobWizardPanel
   */


  getActiveFormInstance(name) {
    const editForms = this.c.controlInstance.getPSDEEditForms() || [];
    return editForms.find(_form => {
      return _form.name.toLowerCase() === name.toLowerCase();
    });
  }
  /**
   * @description 获取激活步骤
   * @protected
   * @return {*}  {number}
   * @memberof AppMobWizardPanel
   */


  getActiveStep() {
    return this.c.wizardForms.indexOf(this.c.activeForm);
  }
  /**
   * @description 按钮显示状态
   * @protected
   * @param {string} type 按钮步骤类型
   * @return {*}  {boolean}
   * @memberof AppMobWizardPanel
   */


  buttonStatus(type) {
    var _a;

    const actions = ((_a = this.c.stepActions[this.c.activeForm]) === null || _a === void 0 ? void 0 : _a.actions) || [];

    if (actions && actions.indexOf(type) < 0) {
      return false;
    }

    return true;
  }
  /**
   * @description 上一步
   * @protected
   * @param {*} event 源事件对象
   * @memberof AppMobWizardPanel
   */


  onClickPrev(event) {
    this.c.handlePrevious();
  }
  /**
   * @description 下一步
   * @protected
   * @param {*} event 源事件对象
   * @memberof AppMobWizardPanel
   */


  onClickNext(event) {
    this.c.handleNext();
  }
  /**
   * @description 完成
   * @protected
   * @param {*} event 源事件对象
   * @memberof AppMobWizardPanel
   */


  onClickFinish(event) {
    this.c.handleFinish();
  }
  /**
   * @description 渲染向导步骤表单
   * @memberof AppMobWizardPanel
   */


  renderWizardStepForm() {
    const formInstance = this.getActiveFormInstance(this.c.activeForm);

    if (formInstance) {
      const otherParams = {
        viewState: this.c.wizardState,
        key: `${this.c.controlInstance.codeName}-${formInstance.codeName}`
      };
      return this.computeTargetCtrlData(formInstance, otherParams);
    }
  }
  /**
   * @description 渲染向导面板组件
   * @return {*}
   * @memberof AppMobWizardPanel
   */


  render() {
    var _a, _b, _c;

    if (!this.controlIsLoaded.value) {
      return;
    }

    const {
      width,
      height
    } = this.c.controlInstance;
    const controlStyle = {
      width: width ? width + 'px' : '',
      height: height ? height + 'px' : ''
    };
    const wizard = this.c.controlInstance.getPSDEWizard();
    return (0, _vue.createVNode)("div", {
      "class": Object.assign({}, this.classNames),
      "style": controlStyle
    }, [this.c.controlInstance.showStepBar ? (0, _vue.createVNode)("div", {
      "class": "wizard__steps_container"
    }, [(0, _vue.createVNode)((0, _vue.resolveComponent)("app-steps"), {
      "active": this.getActiveStep(),
      "steps": this.c.steps,
      "panelInstance": this
    }, null)]) : null, (0, _vue.createVNode)("div", {
      "class": "wizard__step_form_container"
    }, [this.renderWizardStepForm()]), (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-footer"), null, {
      default: () => [(0, _vue.createVNode)((0, _vue.resolveComponent)("ion-buttons"), null, {
        default: () => [this.buttonStatus('PREV') && wizard ? (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-button"), {
          "onClick": event => {
            this.onClickPrev(event);
          }
        }, {
          default: () => [this.$tl((_a = wizard.getPrevCapPSLanguageRes()) === null || _a === void 0 ? void 0 : _a.lanResTag, wizard.prevCaption) || this.$tl('share.previous', '上一步')]
        }) : null, this.buttonStatus('NEXT') && wizard ? (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-button"), {
          "onClick": event => {
            this.onClickNext(event);
          }
        }, {
          default: () => [this.$tl((_b = wizard.getNextCapPSLanguageRes()) === null || _b === void 0 ? void 0 : _b.lanResTag, wizard.nextCaption) || this.$tl('share.next', '下一步')]
        }) : null, this.buttonStatus('FINISH') && wizard ? (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-button"), {
          "onClick": event => {
            this.onClickFinish(event);
          }
        }, {
          default: () => [this.$tl((_c = wizard.getFinishCapPSLanguageRes()) === null || _c === void 0 ? void 0 : _c.lanResTag, wizard.finishCaption) || this.$tl('share.finish', '完成')]
        }) : null]
      })]
    })]);
  }

} // 移动端向导面板部件 组件


exports.AppMobWizardPanel = AppMobWizardPanel;
const AppMobWizardPanelComponent = (0, _componentBase.GenerateComponent)(AppMobWizardPanel, Object.keys(new _ibzCore.AppMobWizardPanelProps()));
exports.AppMobWizardPanelComponent = AppMobWizardPanelComponent;