"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MDCtrlComponentBase = void 0;

var _ibzCore = require("ibz-core");

var _vue = require("vue");

var _ctrlComponentBase = require("./ctrl-component-base");

/**
 * 多数据部件组件基类
 *
 * @export
 * @class MDCtrlComponentBase
 */
class MDCtrlComponentBase extends _ctrlComponentBase.CtrlComponentBase {
  /**
   * @description 初始化响应式属性
   * @memberof MDCtrlComponentBase
   */
  initReactive() {
    super.initReactive();
    this.c.items = (0, _vue.reactive)(this.c.items);
    this.c.sort = (0, _vue.reactive)(this.c.sort);
    this.c.groupDetail = (0, _vue.reactive)(this.c.groupDetail);
    this.c.groupData = (0, _vue.reactive)(this.c.groupData);
    this.c.selections = (0, _vue.reactive)(this.c.selections);
    this.c.dataMap = (0, _vue.reactive)(this.c.dataMap);
  }
  /**
   * @description 绘制快速操作栏
   * @memberof MDCtrlComponentBase
   */


  renderQuickToolbar() {
    const quickToolbar = _ibzCore.ModelTool.findPSControlByName(`${this.c.controlInstance.name}_quicktoolbar`, this.c.controlInstance.getPSControls());

    if (quickToolbar) {
      return this.computeTargetCtrlData(quickToolbar);
    }
  }
  /**
   * @description 绘制批操作工具栏
   * @memberof MDCtrlComponentBase
   */


  renderBatchToolbar() {
    if (this.c.selections.length > 0) {
      const batchToolbar = _ibzCore.ModelTool.findPSControlByName(`${this.c.controlInstance.name}_batchtoolbar`, this.c.controlInstance.getPSControls());

      if (batchToolbar) {
        return this.computeTargetCtrlData(batchToolbar);
      }
    }
  }

}

exports.MDCtrlComponentBase = MDCtrlComponentBase;