"use strict";

var _interopRequireDefault = require("D:/IbizWork/r8-mob-dynamic-docs/packages/ibz-ui-ionic/node_modules/@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppMobChartComponent = exports.AppMobChart = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _chartDetail = require("ibz-core/src/ui-core/controllers/chart-detail");

var _componentBase = require("../component-base");

var _mdCtrlComponentBase = require("./md-ctrl-component-base");

var _echarts = require("echarts/lib/echarts");

var _moment = _interopRequireDefault(require("moment"));

/**
 * 图表部件
 *
 * @export
 * @class AppMobChart
 * @extends {MDCtrlComponentBase}
 */
class AppMobChart extends _mdCtrlComponentBase.MDCtrlComponentBase {
  /**
   * @description 设置响应式
   * @memberof AppMobChart
   */
  setup() {
    this.c = (0, _vue.shallowReactive)(this.getCtrlControllerByType('CHART'));
    super.setup();
  }
  /**
   * @description 部件基础数据初始化
   * @memberof MobChartCtrlController
   */


  init() {
    super.init();
    this.initSeriesModel();
    this.initChartOption();
    this.initChartUserParams();
  }
  /**
   * @description 执行部件事件
   * @param {ICtrlEventParam} arg 事件参数
   * @memberof AppMobMap
   */


  emitCtrlEvent(arg) {
    this.ctx.emit('ctrlEvent', arg);

    if (arg.action == _ibzCore.MobChartEvents.LOAD_SUCCESS) {
      const {
        data
      } = arg;
      this.transformToBasicChartSetData(data.data, () => {
        this.drawCharts();
      });
    }
  }
  /**
   * @description 初始化序列模型
   * @private
   * @return {*}
   * @memberof MobChartCtrlController
   */


  async initSeriesModel() {
    var _a;

    if (!this.c.controlInstance.getPSDEChartSerieses()) {
      return;
    }

    for (let index = 0; index < ((_a = this.c.controlInstance.getPSDEChartSerieses()) === null || _a === void 0 ? void 0 : _a.length); index++) {
      const series = this.c.controlInstance.getPSDEChartSerieses()[index];

      if (series) {
        this.initChartSeries(await this.getSeriesModelParam(series), series);
      }
    }
  }
  /**
   * @description 填充序列模型数据
   * @private
   * @param {*} opts 序列参数
   * @param {*} series 序列
   * @memberof MobChartCtrlController
   */


  initChartSeries(opts, series) {
    var _a, _b, _c, _d, _e, _f, _g, _h, _j;

    switch (series.eChartsType) {
      // 折线图
      case 'line':
        this.c.seriesModel[(_a = series.name) === null || _a === void 0 ? void 0 : _a.toLowerCase()] = new _chartDetail.ChartLineSeriesController(opts);
        break;
      // 漏斗图

      case 'funnel':
        this.c.seriesModel[(_b = series.name) === null || _b === void 0 ? void 0 : _b.toLowerCase()] = new _chartDetail.ChartFunnelSeriesController(opts);
        break;
      // 饼图

      case 'pie':
        this.c.seriesModel[(_c = series.name) === null || _c === void 0 ? void 0 : _c.toLowerCase()] = new _chartDetail.ChartPieSeriesController(opts);
        break;
      // 柱状图

      case 'bar':
        this.c.seriesModel[(_d = series.name) === null || _d === void 0 ? void 0 : _d.toLowerCase()] = new _chartDetail.ChartBarSeriesController(opts);
        break;
      // 雷达图

      case 'radar':
        this.c.seriesModel[(_e = series.name) === null || _e === void 0 ? void 0 : _e.toLowerCase()] = new _chartDetail.ChartRadarSeriesController(opts);
        break;
      // 散点图

      case 'scatter':
        this.c.seriesModel[(_f = series.name) === null || _f === void 0 ? void 0 : _f.toLowerCase()] = new _chartDetail.ChartScatterSeriesController(opts);
        break;
      // 仪表盘

      case 'gauge':
        this.c.seriesModel[(_g = series.name) === null || _g === void 0 ? void 0 : _g.toLowerCase()] = new _chartDetail.ChartGaugeSeriesController(opts);
        break;
      // K线图

      case 'candlestick':
        this.c.seriesModel[(_h = series.name) === null || _h === void 0 ? void 0 : _h.toLowerCase()] = new _chartDetail.ChartCandlestickSeriesController(opts);
        break;
      // 自定义（暂时地图）

      case 'custom':
        this.c.seriesModel[(_j = series.name) === null || _j === void 0 ? void 0 : _j.toLowerCase()] = new _chartDetail.ChartMapSeriesController(opts);
        break;
    }
  }
  /**
   * @description 初始化图表参数
   * @private
   * @memberof MobChartCtrlController
   */


  initChartOption() {
    var _a, _b, _c, _d;

    const series = []; // 填充series

    const indicator = [];
    (_a = this.c.controlInstance.getPSDEChartSerieses()) === null || _a === void 0 ? void 0 : _a.forEach(_series => {
      series.push(this.fillSeries(_series, indicator));
    }); // 填充xAxis

    const xAxis = [];
    (_b = this.c.controlInstance.getPSChartXAxises()) === null || _b === void 0 ? void 0 : _b.forEach(_xAxis => {
      xAxis.push(this.fillAxis(_xAxis));
    }); // 填充yAxis

    const yAxis = [];
    (_c = this.c.controlInstance.getPSChartYAxises()) === null || _c === void 0 ? void 0 : _c.forEach(_yAxis => {
      yAxis.push(this.fillAxis(_yAxis));
    }); // 填充grid

    const grid = [];
    (_d = this.c.controlInstance.getPSChartGrids()) === null || _d === void 0 ? void 0 : _d.forEach(_grid => {
      grid.push(Object.assign({}, _grid.baseOptionJOString ? new Function('return {' + _grid.baseOptionJOString + '}')() : {}));
    }); // chartOption参数

    const opt = {
      tooltip: {
        show: true
      },
      dataset: [],
      series: series,
      xAxis: xAxis,
      yAxis: yAxis // grid: grid,

    };
    this.fillTitleOption(opt);
    this.fillLegendOption(opt);
    this.registerMap(); // 合并chartOption

    Object.assign(this.c.chartOption, opt); // 雷达图特殊参数

    Object.assign(this.c.chartOption, {
      radar: {
        indicator
      }
    });
  }
  /**
   * @description 初始化图表用户自定义参数
   * @private
   * @memberof MobChartCtrlController
   */


  initChartUserParams() {
    this.fillUserParam(this.c.controlInstance, this.c.chartUserParams, 'EC');
  }
  /**
   * @description 获取序列模型参数
   * @private
   * @param {*} series 序列
   * @return {*}  {Promise<IParam>}
   * @memberof MobChartCtrlController
   */


  getSeriesModelParam(series) {
    var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k;

    const seriesValueField = (_a = series.valueField) === null || _a === void 0 ? void 0 : _a.toLowerCase();
    const seriesCatalogField = (_b = series.catalogField) === null || _b === void 0 ? void 0 : _b.toLowerCase(); // 构造dataSetFields属性

    const opts = {
      name: (_c = series.name) === null || _c === void 0 ? void 0 : _c.toLowerCase(),
      categorField: seriesCatalogField,
      valueField: seriesValueField,
      seriesValues: [],
      seriesIndex: series.index | 0,
      data: [],
      seriesMap: {},
      categorCodeList: {
        type: (_d = series.getCatalogPSCodeList()) === null || _d === void 0 ? void 0 : _d.codeListType,
        tag: (_e = series.getCatalogPSCodeList()) === null || _e === void 0 ? void 0 : _e.codeName,
        emptycode: 'empty',
        emptytext: (_f = series.getCatalogPSCodeList()) === null || _f === void 0 ? void 0 : _f.emptyText
      },
      dataSetFields: this.getDataSetFields(series),
      ecxObject: {
        label: {
          show: true,
          position: 'top'
        },
        labelLine: {
          length: 10,
          lineStyle: {
            width: 1,
            type: 'solid'
          }
        },
        itemStyle: {
          borderWidth: 1
        },
        emphasis: {
          label: {
            show: true,
            fontSize: 20
          }
        }
      },
      seriesCodeList: series.getSeriesPSCodeList() ? {
        type: (_g = series.getSeriesPSCodeList()) === null || _g === void 0 ? void 0 : _g.codeListType,
        tag: (_h = series.getSeriesPSCodeList()) === null || _h === void 0 ? void 0 : _h.codeName,
        emptycode: 'empty',
        emptytext: (_j = series.getSeriesPSCodeList()) === null || _j === void 0 ? void 0 : _j.emptyText
      } : null,
      seriesNameField: (_k = series.seriesField) === null || _k === void 0 ? void 0 : _k.toLowerCase(),
      ecObject: {},
      seriesTemp: {
        type: series.eChartsType
      },
      type: series.eChartsType,
      seriesLayoutBy: series.seriesLayoutBy,
      baseOption: {}
    }; // 饼图引导线默认配置

    if (Object.is(series.eChartsType, 'pie')) {
      Object.assign(opts.ecxObject.label, {
        position: 'outside',
        formatter: `{b}: {d}%({@${opts.valueField}})`
      });
      Object.assign(opts.ecxObject.labelLine, {
        show: true
      });
    } // 漏斗图默认配置


    if (Object.is(series.eChartsType, 'funnel')) {
      Object.assign(opts.ecxObject.label, {
        position: 'outside',
        formatter: `{b}: {d}%({@${opts.valueField}})`
      });
    } // 地图默认配置（暂时自定义）


    if (Object.is(series.eChartsType, 'custom')) {
      Object.assign(opts.ecxObject.label, {
        formatter: `{b}: {@${opts.valueField}}`
      });
      opts.ecxObject.tooltip = {
        trigger: 'item',
        formatter: '{b}:  {c}'
      };
    } // 处理自定义ECX参数


    this.fillUserParam(series, opts.ecxObject, 'ECX');
    this.fillUserParam(series, opts.ecObject, 'EC');
    Object.assign(opts, series.baseOptionJOString ? new Function('return {' + series.baseOptionJOString + '}')() : {});
    return opts;
  }
  /**
   * @description 获取序列dataset属性
   * @private
   * @param {*} series 序列
   * @return {*}
   * @memberof MobChartCtrlController
   */


  getDataSetFields(series) {
    var _a, _b, _c;

    const seriesData = [];
    const dataSet = ((_a = this.c.controlInstance.getPSChartDataSets()) === null || _a === void 0 ? void 0 : _a.find(item => {
      var _a, _b;

      return item.id === ((_b = (_a = series === null || series === void 0 ? void 0 : series.M) === null || _a === void 0 ? void 0 : _a.getPSChartDataSet) === null || _b === void 0 ? void 0 : _b.id) || null;
    })) || null;

    if (!dataSet && !dataSet.getPSChartDataSetFields()) {
      return null;
    }

    for (let index = 0; index < ((_b = dataSet.getPSChartDataSetFields()) === null || _b === void 0 ? void 0 : _b.length); index++) {
      const dataFile = dataSet.getPSChartDataSetFields()[index];
      const data = {};

      if (dataFile.getPSCodeList()) {
        const codelist = dataFile.getPSCodeList();
        Object.assign(data, {
          codelist: codelist
        });
      }

      data['isGroupField'] = dataFile.groupField;
      data['name'] = (_c = dataFile.name) === null || _c === void 0 ? void 0 : _c.toLowerCase();
      data['groupMode'] = dataFile.groupMode ? dataFile.groupMode : ''; // 只读不合并数据（扩展值属性4）

      if (series.extValue4Field && Object.is(dataFile.name, series.extValue4Field)) {
        data['isReadOnly'] = true;
      }

      seriesData.push(data);
    }

    return seriesData;
  }
  /**
   * @description 填充标题配置
   * @private
   * @param {*} opts 图表参数
   * @memberof MobChartCtrlController
   */


  fillTitleOption(opts) {
    var _a, _b;

    if (this.c.controlInstance.getPSDEChartTitle()) {
      const _titleModel = this.c.controlInstance.getPSDEChartTitle();

      const title = {
        show: _titleModel === null || _titleModel === void 0 ? void 0 : _titleModel.showTitle,
        text: this.$tl((_a = _titleModel === null || _titleModel === void 0 ? void 0 : _titleModel.getTitlePSLanguageRes()) === null || _a === void 0 ? void 0 : _a.lanResTag, _titleModel === null || _titleModel === void 0 ? void 0 : _titleModel.title),
        subtext: this.$tl((_b = _titleModel === null || _titleModel === void 0 ? void 0 : _titleModel.getSubTitlePSLanguageRes()) === null || _b === void 0 ? void 0 : _b.lanResTag, _titleModel === null || _titleModel === void 0 ? void 0 : _titleModel.subTitle)
      };

      if (_titleModel === null || _titleModel === void 0 ? void 0 : _titleModel.titlePos) {
        switch (_titleModel === null || _titleModel === void 0 ? void 0 : _titleModel.titlePos) {
          case 'LEFT':
            Object.assign(title, {
              left: 'left'
            });
            break;

          case 'RIGHT':
            Object.assign(title, {
              left: 'right'
            });
            break;

          case 'BOTTOM':
            Object.assign(title, {
              left: 'center',
              top: 'bottom'
            });
            break;
        }
      }

      Object.assign(opts, {
        title
      });
    }
  }
  /**
   * @description 填充图例配置
   * @private
   * @param {*} opts 图表参数
   * @memberof MobChartCtrlController
   */


  fillLegendOption(opts) {
    const legendModel = this.c.controlInstance.getPSDEChartLegend();
    const legend = {
      show: legendModel === null || legendModel === void 0 ? void 0 : legendModel.showLegend
    };

    if (legendModel === null || legendModel === void 0 ? void 0 : legendModel.legendPos) {
      switch (legendModel.legendPos) {
        case 'LEFT':
          Object.assign(legend, {
            left: 'left',
            top: 'middle',
            orient: 'vertical'
          });
          break;

        case 'RIGHT':
          Object.assign(legend, {
            left: 'right',
            top: 'middle',
            orient: 'vertical'
          });
          break;

        case 'BOTTOM':
          Object.assign(legend, {
            top: 'bottom'
          });
          break;
      }
    }

    Object.assign(opts, {
      legend
    });
  }
  /**
   * @description 注册地图
   * @private
   * @memberof MobChartCtrlController
   */


  registerMap() {// TODO 地图暂时不支持
    // const userParams: any = this.c.controlInstance.userParams || {};
    // if (userParams?.mapName) {
    //     const geoJson = require(`@/assets/json/map/${userParams?.mapName}.json`);
    //     registerMap(userParams?.mapName, geoJson);
    // }
  }
  /**
   * @description 处理用户自定义参数
   * @private
   * @param {*} param 用户参数
   * @param {*} opts 绘制参数
   * @param {string} tag 标识
   * @return {*}
   * @memberof MobChartCtrlController
   */


  fillUserParam(param, opts, tag) {
    if (!param.userParams) {
      return;
    }

    const userParam = param.userParams;

    switch (tag) {
      case 'ECX':
        if (userParam['ECX.label']) {
          opts['label'] = eval('(' + userParam['ECX.label'] + ')');
        }

        if (userParam['ECX.labelLine']) {
          opts['labelLine'] = eval('(' + userParam['ECX.labelLine'] + ')');
        }

        if (userParam['ECX.itemStyle']) {
          opts['itemStyle'] = eval('(' + userParam['ECX.itemStyle'] + ')');
        }

        if (userParam['ECX.emphasis']) {
          opts['emphasis'] = eval('(' + userParam['ECX.emphasis'] + ')');
        }

        for (const key in userParam) {
          if (Object.prototype.hasOwnProperty.call(userParam, key)) {
            if (key.indexOf('EC.') != -1) {
              const value = userParam[key].trim();
              opts[key.replace('EC.', '')] = value;
            }
          }
        }

        break;

      case 'EC':
        for (const key in userParam) {
          if (Object.prototype.hasOwnProperty.call(userParam, key)) {
            if (key.indexOf('EC.') != -1) {
              const value = userParam[key].trim();
              opts[key.replace('EC.', '')] = this.isJson(value) ? this.deepJsonParseFun(JSON.parse(value)) : this.isArray(value) ? eval(value) : value;
            }
          }
        }

        break;

      default:
        break;
    }
  }
  /**
   * @description 填充序列
   * @private
   * @param {*} series 序列
   * @param {*} [indicator=[]] 雷达图参数
   * @return {*}
   * @memberof MobChartCtrlController
   */


  fillSeries(series, indicator = []) {
    var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m;

    const encode = {};
    let customType = '';

    const assginCodeList = codeList => {
      var _a, _b;

      (_b = (_a = codeList.getPSCodeItems) === null || _a === void 0 ? void 0 : _a.call(codeList)) === null || _b === void 0 ? void 0 : _b.forEach(_item => {
        var _a, _b, _c;

        const item = {
          name: _item.text,
          max: ((_a = _item.userParams) === null || _a === void 0 ? void 0 : _a.MAXVALUE) ? _item.userParams.MAXVALUE : null
        };
        indicator.push(item);

        if (((_c = (_b = _item === null || _item === void 0 ? void 0 : _item.getPSCodeItems) === null || _b === void 0 ? void 0 : _b.call(_item)) === null || _c === void 0 ? void 0 : _c.length) > 0) {
          assginCodeList(_item);
        }
      });
    };

    switch (series.eChartsType) {
      case 'line':
      case 'bar':
        const cSCartesian2DEncode = series.getPSChartSeriesEncode();
        encode.x = this.arrayToLowerCase(cSCartesian2DEncode.getX());
        encode.y = this.arrayToLowerCase(cSCartesian2DEncode.getY());
        break;

      case 'pie':
      case 'funnel':
        const CSNoneEncode = series.getPSChartSeriesEncode();
        encode.itemName = (_a = CSNoneEncode.category) === null || _a === void 0 ? void 0 : _a.toLowerCase();
        encode.value = (_b = CSNoneEncode.value) === null || _b === void 0 ? void 0 : _b.toLowerCase();
        break;

      case 'radar':
        encode.itemName = 'type';
        const catalogCodeList = (_c = series.getCatalogPSCodeList) === null || _c === void 0 ? void 0 : _c.call(series);

        if (catalogCodeList) {
          assginCodeList(catalogCodeList);
        }

        break;

      case 'scatter':
        break;

      case 'gauge':
        break;

      case 'candlestick':
        const candlestickEncode = series.getPSChartSeriesEncode();
        encode.x = this.arrayToLowerCase(candlestickEncode.getX());
        encode.y = this.arrayToLowerCase(candlestickEncode.getY());
        break;

      case 'custom':
        customType = 'map';
        break;

      default:
        break;
    }

    return Object.assign({
      id: (_d = series === null || series === void 0 ? void 0 : series.name) === null || _d === void 0 ? void 0 : _d.toLowerCase(),
      name: series.caption,
      type: customType ? customType : series.eChartsType,
      xAxisIndex: ((_g = (_f = (_e = series === null || series === void 0 ? void 0 : series.getPSChartSeriesEncode()) === null || _e === void 0 ? void 0 : _e.M) === null || _f === void 0 ? void 0 : _f.getPSChartXAxis) === null || _g === void 0 ? void 0 : _g.id) | 0,
      yAxisIndex: ((_k = (_j = (_h = series === null || series === void 0 ? void 0 : series.getPSChartSeriesEncode()) === null || _h === void 0 ? void 0 : _h.M) === null || _j === void 0 ? void 0 : _j.getPSChartYAxis) === null || _k === void 0 ? void 0 : _k.id) | 0,
      datasetIndex: ((_m = (_l = series === null || series === void 0 ? void 0 : series.M) === null || _l === void 0 ? void 0 : _l.getPSChartDataSet) === null || _m === void 0 ? void 0 : _m.id) | 0,
      encode: Object.keys(encode).length > 0 ? encode : null
    }, series.baseOptionJOString ? new Function('return {' + series.baseOptionJOString + '}')() : {});
  }
  /**
   * @description 填充坐标
   * @private
   * @param {*} axis 坐标
   * @return {*}  {IParam}
   * @memberof MobChartCtrlController
   */


  fillAxis(axis) {
    const _axis = {
      // gridIndex: axis.index,
      position: axis.position,
      type: axis.eChartsType,
      name: axis.caption
    };
    const dataShowMode = axis.dataShowMode;

    if (dataShowMode === 1) {
      //  纵向显示
      Object.assign(_axis, {
        axisLabel: {
          formatter: value => {
            if (value.length > 4) {
              return value.substr(0, 4).split('').join('\n') + '\n...';
            } else {
              return value.split('').join('\n');
            }
          }
        }
      });
    } else if (dataShowMode === 2) {//  横向显示
    } else if (dataShowMode === 3) {
      //  斜向显示
      Object.assign(_axis, {
        axisLabel: {
          rotate: 45,
          formatter: value => {
            if (value.length > 4) {
              return value.substr(0, 4) + '...';
            } else {
              return value;
            }
          }
        }
      });
    } // 填充用户自定义参数


    this.fillUserParam(axis, _axis, 'EC');

    if (axis.minValue) {
      _axis['min'] = axis.minValue;
    }

    if (axis.maxValue) {
      _axis['max'] = axis.maxValue;
    }

    return _axis;
  }
  /**
   * @description 绘制图表
   * @private
   * @memberof MobChartCtrlController
   */


  drawCharts() {
    if (!this.c.myChart) {
      const element = document.getElementById(this.c.chartId);

      if (element) {
        this.c.myChart = (0, _echarts.init)(element);
      }
    } //判断刷新时dom是否存在


    if (!Object.is(this.c.myChart._dom.offsetHeight, 0) && !Object.is(this.c.myChart._dom.offsetWidth, 0)) {
      const _chartOption = this.handleChartOPtion();

      this.c.chartRenderOption = Object.assign({}, _chartOption);

      if (this.c.myChart) {
        this.c.myChart.setOption(_chartOption);
        this.onChartEvents();
        this.handleDefaultSelect();
        this.c.myChart.resize();
      }
    }
  }
  /**
   * @description 图标事件
   * @memberof MobChartCtrlController
   */


  onChartEvents() {
    this.c.myChart.on('click', e => {
      this.onChartClick(e);
    });
    this.c.myChart.on('selectchanged', e => {
      if (this.c.isSelectFirstDefault) {
        if (e && e.fromActionPayload) {
          const _event = {
            seriesId: e.fromActionPayload.seriesId,
            data: e.fromActionPayload.curData,
            name: e.fromActionPayload.seriesId
          };
          this.onChartClick(_event);
        }
      }
    });
  }
  /**
   * @description 处理默认选中
   * @private
   * @memberof MobChartCtrlController
   */


  handleDefaultSelect() {
    var _a, _b, _c, _d;

    if (this.c.isSelectFirstDefault) {
      const options = this.handleChartOPtion();
      const selectSeriesId = ((_b = (_a = this.c.controlInstance.getPSDEChartSerieses()) === null || _a === void 0 ? void 0 : _a[0]) === null || _b === void 0 ? void 0 : _b.name) || null;
      const curData = ((_d = (_c = options === null || options === void 0 ? void 0 : options.dataset[0]) === null || _c === void 0 ? void 0 : _c.source) === null || _d === void 0 ? void 0 : _d[0]) || undefined;

      if (selectSeriesId && curData) {
        this.c.myChart.dispatchAction({
          type: 'select',
          seriesId: selectSeriesId.toLowerCase(),
          dataIndex: 0,
          curData: curData
        });
      }
    }
  }
  /**
   * @description 图表单击事件
   * @private
   * @param {*} event 数据源
   * @return {*}
   * @memberof MobChartCtrlController
   */


  onChartClick(event) {
    if (!event || !event.name) {
      return;
    }

    const data = event.data;
    Object.assign(data, {
      _chartName: event.seriesId
    });
    this.c.selections = [data];
    this.c.ctrlEvent(_ibzCore.MobChartEvents.SELECT_CHANGE, this.c.selections);
  }
  /**
   * @description 处理图表参数
   * @private
   * @return {*}  {IParam}
   * @memberof MobChartCtrlController
   */


  handleChartOPtion() {
    const _chartOption = _ibzCore.Util.deepCopy(this.c.chartOption);

    if (Object.keys(this.c.seriesModel).length > 0) {
      const tempDataSourceMap = new Map();

      for (let i = 0; i < Object.keys(this.c.seriesModel).length; i++) {
        Object.values(this.c.seriesModel).forEach(seriesvalue => {
          if (seriesvalue.seriesIndex === i) {
            tempDataSourceMap.set(seriesvalue.name, seriesvalue.data);
          }
        });
      }

      if (tempDataSourceMap.size > 0) {
        tempDataSourceMap.forEach(item => {
          _chartOption.dataset.push({
            source: item
          });
        });
      }

      Object.keys(this.c.seriesModel).forEach(seriesName => {
        var _a, _b;

        if (_chartOption && _chartOption.series.length > 0) {
          _chartOption.series.forEach(item => {
            if (this.c.seriesModel[seriesName].ecxObject && Object.is(seriesName, item.id)) {
              item = _ibzCore.Util.deepObjectMerge(item, this.c.seriesModel[seriesName].ecxObject);
            }

            if (this.c.seriesModel[seriesName].baseOption && Object.keys(this.c.seriesModel[seriesName].baseOption).length > 0 && Object.is(seriesName, item.id)) {
              item = _ibzCore.Util.deepObjectMerge(item, this.c.seriesModel[seriesName].baseOption);
            }

            if (this.c.seriesModel[seriesName].ecObject && Object.is(seriesName, item.id)) {
              item = _ibzCore.Util.deepObjectMerge(item, this.c.seriesModel[seriesName].ecObject);
            }
          });
        } //设置多序列


        const tempSeries = this.c.seriesModel[seriesName];

        const returnIndex = _chartOption.series.findIndex(item => {
          return Object.is(item.id, seriesName);
        }); // 仪表盘和雷达图数据特殊处理


        const source = _chartOption.dataset[0].source;
        const maxValue = this.calcSourceMaxValue(source);

        if (tempSeries && Object.is(tempSeries.type, 'gauge')) {
          _chartOption.series.splice(returnIndex, 1);

          const temSeries = {
            type: 'gauge',
            title: {
              fontSize: 14
            },
            progress: {
              show: true,
              overlap: false,
              roundCap: true
            },
            max: maxValue,
            detail: {
              width: 40,
              height: 14,
              fontSize: 14,
              color: '#fff',
              backgroundColor: 'auto',
              borderRadius: 3,
              formatter: '{value}'
            },
            data: this.transformToChartSeriesData(tempSeries, source, 'gauge')
          };

          _chartOption.series.push(temSeries);
        } else if (tempSeries && Object.is(tempSeries.type, 'radar')) {
          if (((_a = _chartOption.radar.indicator) === null || _a === void 0 ? void 0 : _a.length) > 0) {
            (_b = _chartOption.radar.indicator) === null || _b === void 0 ? void 0 : _b.forEach(item => {
              item.max = item.max ? item.max : maxValue;
            });
          } else {
            const indicator = [];
            source.forEach(sourceItem => {
              const item = {
                name: sourceItem[tempSeries.categorField],
                max: maxValue
              };
              indicator.push(item);
            });
            Object.assign(_chartOption, {
              radar: {
                indicator
              }
            });
          }

          _chartOption.series[returnIndex].data = this.transformToChartSeriesData(tempSeries, source, 'radar', _chartOption.radar.indicator);
        } else if (tempSeries && tempSeries.seriesIdField && tempSeries.seriesValues.length > 0) {
          const series = _chartOption.series[returnIndex];

          _chartOption.series.splice(returnIndex, 1);

          delete series.id;
          tempSeries.seriesValues.forEach(seriesvalueItem => {
            const tempSeriesTemp = _ibzCore.Util.deepCopy(tempSeries.seriesTemp);

            Object.assign(tempSeriesTemp, series);
            tempSeriesTemp.name = tempSeries.seriesMap[seriesvalueItem];
            tempSeriesTemp.datasetIndex = tempSeries.seriesIndex;
            tempSeriesTemp.encode = {
              x: tempSeries.categorField,
              y: `${seriesvalueItem}`
            };

            _chartOption.series.push(tempSeriesTemp);
          });
        }
      });
    }

    if (Object.keys(this.c.chartBaseOPtion).length > 0) {
      Object.assign(_chartOption, this.c.chartBaseOPtion);
    }

    if (Object.keys(this.c.chartUserParams).length > 0) {
      Object.assign(_chartOption, this.c.chartUserParams);
    }

    return _chartOption;
  }
  /**
   * @description 实体数据集转化为图表数据集
   *  1.获取图表所有代码表值
   *  2.查询集合映射图表数据集
   *  3.补全图表数据集
   *  4.图表数据集分组求和
   *  5.排序图表数据集
   * @private
   * @param {*} data 实体数据集
   * @param {Function} callback 回调
   * @return {*}
   * @memberof MobChartCtrlController
   */


  async transformToBasicChartSetData(data, callback) {
    if (!data || !Array.isArray(data) || data.length === 0) {
      this.c.isNoData = true;

      if (this.c.myChart) {
        this.c.myChart.dispose();
        this.c.myChart = undefined;
      }

      return;
    }

    this.c.isNoData = false; //获取代码表值

    const allCodeList = await this.getChartAllCodeList();

    if (Object.values(this.c.seriesModel).length > 0) {
      Object.values(this.c.seriesModel).forEach((singleSeries, index) => {
        // 值属性为srfcount设置{srfcount:1}到data
        const valueField = singleSeries.dataSetFields.find(datasetField => {
          return datasetField.name === singleSeries.valueField;
        });

        if (valueField && valueField.name && Object.is(valueField.name, 'srfcount')) {
          data.forEach(singleData => {
            Object.assign(singleData, {
              srfcount: 1
            });
          });
        } // 分组属性


        const groupField = singleSeries.dataSetFields.find(datasetField => {
          return datasetField.name === singleSeries.categorField;
        });
        const tempChartSetData = [];
        let tempSeriesValues = new Map();
        data.forEach(item => {
          const tempChartSetDataItem = {}; // 序列属性不存在

          if (!singleSeries.seriesIdField) {
            Object.assign(tempChartSetDataItem, {
              name: singleSeries.name
            });

            if (singleSeries.dataSetFields && singleSeries.dataSetFields.length > 0) {
              singleSeries.dataSetFields.forEach(singleDataSetField => {
                this.handleSingleDataSetField(item, singleDataSetField, allCodeList, tempChartSetDataItem, groupField);
              });
            }
          } else {
            // 序列属性存在时
            // 序列代码表存在时,翻译tempSeriesValues的键值对
            if (singleSeries.seriesCodeList) {
              const seriesCodeList = allCodeList.get(singleSeries.seriesCodeList.tag);
              const tempSeriesValueItem = tempSeriesValues.get(seriesCodeList.get(item[singleSeries.seriesIdField]));

              if (!tempSeriesValueItem) {
                tempSeriesValues.set(seriesCodeList.get(item[singleSeries.seriesIdField]), seriesCodeList.get(item[singleSeries.seriesNameField]));
              }
            } else {
              const tempSeriesValueItem = tempSeriesValues.get(item[singleSeries.seriesIdField]);

              if (!tempSeriesValueItem) {
                tempSeriesValues.set(item[singleSeries.seriesIdField], item[singleSeries.seriesNameField]);
              }
            }

            Object.assign(tempChartSetDataItem, {
              name: item[singleSeries.seriesIdField]
            });

            if (singleSeries.dataSetFields && singleSeries.dataSetFields.length > 0) {
              singleSeries.dataSetFields.forEach(singleDataSetField => {
                this.handleSingleDataSetField(item, singleDataSetField, allCodeList, tempChartSetDataItem, groupField);
              });
            }
          }

          tempChartSetData.push(tempChartSetDataItem);
        }); // 补全数据集合

        this.completeDataSet(tempChartSetData, singleSeries, allCodeList); // 序列代码表存在时,补全序列

        if (singleSeries.seriesCodeList) {
          const seriesCodeList = allCodeList.get(singleSeries.seriesCodeList.tag);
          tempSeriesValues = new Map();
          seriesCodeList.forEach(item => {
            tempSeriesValues.set(item, item);
          });
        }

        singleSeries.seriesValues = [...tempSeriesValues.keys()];
        const tempSeriesMapObj = {};
        tempSeriesValues.forEach((value, key) => {
          tempSeriesMapObj[key] = value;
        });
        singleSeries.seriesMap = tempSeriesMapObj;
        const callbackFunction = index === Object.values(this.c.seriesModel).length - 1 ? callback : null;
        this.transformToChartSeriesDataSet(tempChartSetData, singleSeries, callbackFunction, allCodeList);
      });
    }
  }
  /**
   * @description 构建图表序列数据集合
   *  1.分组求和
   *  2.排序求和数组
   * @private
   * @param {*} data 传入数据
   * @param {*} item 单个序列
   * @param {Function} callback 回调
   * @param {*} allCodeList 所有代码表
   * @memberof MobChartCtrlController
   */


  transformToChartSeriesDataSet(data, item, callback, allCodeList) {
    if (item.seriesIdField) {
      // 多序列
      const groupField = item.dataSetFields.filter(datasetField => {
        return datasetField.name === item.categorField;
      });
      const tempGroupField = groupField.map(item => {
        return item.name;
      });
      const seriesField = item.dataSetFields.filter(datasetField => {
        return datasetField.name === item.seriesIdField;
      });
      const tempSeriesField = seriesField.map(item => {
        return item.name;
      });
      const valueField = item.dataSetFields.filter(datasetField => {
        return datasetField.name === item.valueField;
      });
      const tempValueField = valueField.map(item => {
        return item.name;
      });
      item.data = this.groupAndAdd(tempGroupField, tempSeriesField, tempValueField, data, item, groupField, allCodeList);
    } else {
      //单序列
      const groupField = item.dataSetFields.filter(datasetField => {
        return datasetField.name === item.categorField;
      });
      const tempGroupField = groupField.map(item => {
        return item.name;
      });
      const valueField = item.dataSetFields.filter(datasetField => {
        return datasetField.name === item.valueField;
      });
      const tempValueField = valueField.map(item => {
        return item.name;
      });
      item.data = this.groupAndAdd(tempGroupField, [], tempValueField, data, item, groupField, allCodeList);
    }

    if (callback && callback instanceof Function) {
      callback(allCodeList);
    }
  }
  /**
   * @description 分组和求和
   * @private
   * @param {Array<any>} groupField 分组属性
   * @param {Array<any>} seriesField 序列
   * @param {Array<any>} valueField 值属性
   * @param {*} data 传入数据
   * @param {*} item 项数据
   * @param {*} groupFieldModel 分组属性模型
   * @param {*} allCodeList 所有代码表
   * @return {*} {IParam[]}
   * @memberof MobChartCtrlController
   */


  groupAndAdd(groupField, seriesField, valueField, data, item, groupFieldModel, allCodeList) {
    const tempMap = new Map();
    const groupMode = groupFieldModel[0].groupMode;
    let groupKeyStr = '';
    data.forEach(item => {
      const tempGroupField = groupField[0];
      groupKeyStr = item[tempGroupField];
      const tempMapItem = tempMap.get(groupKeyStr);

      if (tempMapItem) {
        tempMapItem.push(item);
        tempMap.set(groupKeyStr, tempMapItem);
      } else {
        tempMap.set(groupKeyStr, [item]);
      }
    }); // 处理多序列

    if (seriesField.length > 0 && tempMap.size > 0) {
      const tempSeriesField = seriesField[0];
      tempMap.forEach((item, key) => {
        const tempItemMap = new Map();
        item.forEach(singleItem => {
          const seriesValueArray = tempItemMap.get(singleItem[tempSeriesField]);

          if (seriesValueArray) {
            seriesValueArray.push(singleItem);
            tempItemMap.set(singleItem[tempSeriesField], seriesValueArray);
          } else {
            tempItemMap.set(singleItem[tempSeriesField], [singleItem]);
          }
        });
        tempMap.set(key, tempItemMap);
      });
    }

    let returnArray = [];

    if (seriesField.length == 0) {
      //单序列
      tempMap.forEach(tempItem => {
        if (tempItem.length > 0) {
          const curObject = {};
          const valueResult = {};
          let categorResult;
          tempItem.forEach(singleItem => {
            categorResult = singleItem[groupField[0]];
            valueResult[valueField[0]] = valueResult[valueField[0]] ? valueResult[valueField[0]] + singleItem[valueField[0]] : singleItem[valueField[0]];
            item.dataSetFields.forEach(dataSetField => {
              // 只读不合并数据（扩展值属性4）
              if (dataSetField.isReadOnly) {
                valueResult[dataSetField.name] = singleItem[dataSetField.name];
              } else {
                if (!Object.is(dataSetField.name, groupField[0]) && !Object.is(dataSetField.name, valueField[0])) {
                  valueResult[dataSetField.name] = valueResult[dataSetField.name] ? valueResult[dataSetField.name] + singleItem[dataSetField.name] : singleItem[dataSetField.name];
                }
              }
            });
          });
          Object.defineProperty(curObject, groupField[0], {
            value: categorResult,
            writable: true,
            enumerable: true,
            configurable: true
          });

          for (const value in valueResult) {
            Object.defineProperty(curObject, value, {
              value: valueResult[value],
              writable: true,
              enumerable: true,
              configurable: true
            });
          }

          returnArray.push(curObject);
        }
      });
    } else {
      // 多序列
      const seriesValuesArray = item.seriesValues;
      tempMap.forEach((groupItem, groupKey) => {
        //求和
        const curObject = {};
        Object.defineProperty(curObject, groupField[0], {
          value: groupKey,
          writable: true,
          enumerable: true,
          configurable: true
        });
        seriesValuesArray.forEach(seriesValueItem => {
          Object.defineProperty(curObject, seriesValueItem, {
            value: 0,
            writable: true,
            enumerable: true,
            configurable: true
          });
        });
        groupItem.forEach((seriesItem, seriesKey) => {
          let seriesNum = 0;
          seriesItem.forEach(dataItem => {
            seriesNum += dataItem[valueField[0]];
          });
          curObject[seriesKey] = seriesNum;
        });
        returnArray.push(curObject);
      });
    } // 补全空白分类


    if (returnArray.length > 0) {
      const emptyText = groupFieldModel[0] && groupFieldModel[0].codeList ? groupFieldModel[0].codeList.emptytext : '未定义';
      returnArray.forEach(item => {
        if (!item[groupField[0]]) {
          item[groupField[0]] = emptyText;
        }
      });
    }

    returnArray = this.sortReturnArray(returnArray, groupFieldModel, allCodeList); // 雷达图数据格式处理

    if (Object.is(item.type, 'radar') && returnArray.length > 0) {
      const tempReturnArray = [];
      const seriesValues = item.seriesValues;

      if (seriesValues && seriesValues.length > 0) {
        seriesValues.forEach(singleSeriesName => {
          const singleSeriesObj = {};
          returnArray.forEach(item => {
            Object.assign(singleSeriesObj, {
              [item[groupField[0]]]: item[singleSeriesName]
            });
          });
          Object.assign(singleSeriesObj, {
            type: singleSeriesName
          });
          tempReturnArray.push(singleSeriesObj);
        });
      }

      returnArray = tempReturnArray;
    }

    return returnArray;
  }
  /**
   * @description 排序数组
   * @private
   * @param {Array<any>} arr 传入数组
   * @param {*} groupField 分组属性
   * @param {*} allCodeList 所有代码表
   * @return {*}  {IParam[]}
   * @memberof MobChartCtrlController
   */


  sortReturnArray(arr, groupField, allCodeList) {
    let returnArray = []; // 分组属性有代码表的情况(最后执行)

    if (groupField[0].codelist) {
      const curCodeList = allCodeList.get(groupField[0].codelist.codeName);
      curCodeList.forEach(codelist => {
        arr.forEach(item => {
          if (Object.is(item[groupField[0].name], codelist)) {
            returnArray.push(item);
            item.hasused = true;
          }
        });
      });
      arr.forEach((item, index) => {
        if (!item.hasused) {
          returnArray.push(item);
        }
      });
      returnArray.forEach(item => {
        delete item.hasused;
      });
    } else {
      // 分组为年份
      if (Object.is(groupField[0].groupMode, 'YEAR')) {
        returnArray = arr.sort((a, b) => {
          return Number(a[groupField[0].name]) - Number(b[groupField[0].name]);
        });
      } else if (Object.is(groupField[0].groupMode, 'QUARTER')) {
        returnArray = this.handleSortGroupData(arr, groupField, '季度');
      } else if (Object.is(groupField[0].groupMode, 'MONTH')) {
        returnArray = this.handleSortGroupData(arr, groupField, '月');
      } else if (Object.is(groupField[0].groupMode, 'YEARWEEK')) {
        returnArray = this.handleSortGroupData(arr, groupField, '周');
      } else if (Object.is(groupField[0].groupMode, 'DAY')) {
        returnArray = arr.sort((a, b) => {
          return (0, _moment.default)(a[groupField[0].name]).unix() - (0, _moment.default)(b[groupField[0].name]).unix();
        });
      } else {
        const groupFieldName = groupField[0].name;
        let isConvert = true;
        arr.forEach(item => {
          if (isNaN(item[groupFieldName])) {
            isConvert = false;
          }
        });

        if (isConvert) {
          returnArray = arr.sort((a, b) => {
            return a[groupFieldName] - b[groupFieldName];
          });
        } else {
          returnArray = arr;
        }
      }
    }

    return returnArray;
  }
  /**
   * @description 排序分组模式下的数据
   * @private
   * @param {Array<any>} arr 传入数据
   * @param {*} groupField 分组属性
   * @param {string} label label标签
   * @return {*}  {IParam[]}
   * @memberof MobChartCtrlController
   */


  handleSortGroupData(arr, groupField, label) {
    arr.forEach(item => {
      const sortFieldValue = item[groupField[0].name].split('-');
      Object.assign(item, {
        sortField: Number(sortFieldValue[0]) * 10000 + Number(sortFieldValue[1])
      }); //  分组为月份时，月份+1

      if (Object.is(label, '月')) {
        item[groupField[0].name] = sortFieldValue[0] + '年' + (Number(sortFieldValue[1]) + 1) + label;
      } else {
        item[groupField[0].name] = sortFieldValue[0] + '年' + sortFieldValue[1] + label;
      }
    });
    arr.sort((a, b) => {
      return Number(a.sortField) - Number(b.sortField);
    });
    arr.forEach(item => {
      delete item.sortField;
    });
    return arr;
  }
  /**
   * @description 补全数据集
   * @private
   * @param {*} data 传入数据
   * @param {*} item 单个序列
   * @param {*} allCodeList 所有的代码表
   * @return {*}
   * @memberof MobChartCtrlController
   */


  completeDataSet(data, item, allCodeList) {
    // 分组属性
    const groupField = item.dataSetFields.find(datasetField => {
      return datasetField.name === item.categorField;
    });

    if (!groupField || Object.is(groupField.groupMode, '')) {
      return;
    } //分组模式为代码表（补值）


    if (Object.is(groupField.groupMode, 'CODELIST')) {
      this.completeCodeList(data, item, allCodeList);
    } //分组模式为年/季度/月份（最大值，最小值，分组，补值）


    if (Object.is(groupField.groupMode, 'YEAR') || Object.is(groupField.groupMode, 'QUARTER') || Object.is(groupField.groupMode, 'MONTH') || Object.is(groupField.groupMode, 'YEARWEEK') || Object.is(groupField.groupMode, 'DAY')) {
      this.handleTimeData(data, item, allCodeList, groupField);
    }
  }
  /**
   * @description 获取最大值最小值
   * @private
   * @param {Array<any>} tempTimeArray 传入数组
   * @return {*} {IParam[]}
   * @memberof MobChartCtrlController
   */


  getRangeData(tempTimeArray) {
    tempTimeArray.forEach(item => {
      const tempParams = item._i.split('-');

      item.sortField = Number(tempParams[0] + tempParams[1]);
    });
    tempTimeArray.sort((a, b) => {
      return Number(a.sortField) - Number(b.sortField);
    });
    tempTimeArray.forEach(item => {
      delete item.sortField;
    });
    return tempTimeArray;
  }
  /**
   * @description 补全时间类型数据集
   * @private
   * @param {*} data 传入数据
   * @param {*} item 单个序列
   * @param {*} allCodeList 所有的代码表
   * @param {*} groupField 分组属性
   * @memberof MobChartCtrlController
   */


  handleTimeData(data, item, allCodeList, groupField) {
    const valueField = item.dataSetFields.find(datasetField => {
      return datasetField.name === item.valueField;
    });
    const groupMode = groupField.groupMode; // 排序数据，找到最大值、最小值

    let tempTimeArray = [];

    if (data && data.length > 0) {
      data.forEach(dataItem => {
        // 判断时间类型是否为空，为空不处理
        if (dataItem[groupField.name]) {
          tempTimeArray.push((0, _moment.default)(dataItem[groupField.name]));
        }
      });
    }

    let maxTime;
    let minTime;

    if (Object.is(groupMode, 'YEAR') || Object.is(groupMode, 'DAY')) {
      maxTime = _moment.default.max(tempTimeArray);
      minTime = _moment.default.min(tempTimeArray);
    }

    if (Object.is(groupMode, 'QUARTER')) {
      tempTimeArray = this.getRangeData(tempTimeArray);
      minTime = (0, _moment.default)().year(tempTimeArray[0]._i.split('-')[0]).quarters(tempTimeArray[0]._i.split('-')[1]);
      maxTime = (0, _moment.default)().year(tempTimeArray[tempTimeArray.length - 1]._i.split('-')[0]).quarters(tempTimeArray[tempTimeArray.length - 1]._i.split('-')[1]);
    }

    if (Object.is(groupMode, 'MONTH')) {
      tempTimeArray = this.getRangeData(tempTimeArray);
      minTime = (0, _moment.default)().year(tempTimeArray[0]._i.split('-')[0]).month(tempTimeArray[0]._i.split('-')[1]);
      maxTime = (0, _moment.default)().year(tempTimeArray[tempTimeArray.length - 1]._i.split('-')[0]).month(tempTimeArray[tempTimeArray.length - 1]._i.split('-')[1]);
    }

    if (Object.is(groupMode, 'YEARWEEK')) {
      tempTimeArray = this.getRangeData(tempTimeArray);
      minTime = (0, _moment.default)().year(tempTimeArray[0]._i.split('-')[0]).week(tempTimeArray[0]._i.split('-')[1]);
      maxTime = (0, _moment.default)().year(tempTimeArray[tempTimeArray.length - 1]._i.split('-')[0]).week(tempTimeArray[tempTimeArray.length - 1]._i.split('-')[1]);
    }

    const timeFragmentArray = [];
    const tempGrounpData = new Map(); // 时间分段
    //groupMode为"YEAR"

    if (Object.is(groupMode, 'YEAR')) {
      let curTime = minTime;

      while (curTime) {
        if (curTime.isSameOrBefore(maxTime)) {
          const tempcurTime = curTime.clone();
          timeFragmentArray.push(tempcurTime.year().toString());
          curTime = tempcurTime.clone().add(1, 'years');
        } else {
          curTime = null;
        }
      }
    } //groupMode为"QUARTER"


    if (Object.is(groupMode, 'QUARTER')) {
      let curTime = minTime;

      while (curTime) {
        if (curTime.isSameOrBefore(maxTime)) {
          const tempcurTime = curTime.clone();
          timeFragmentArray.push(tempcurTime.year().toString() + '-' + tempcurTime.quarter().toString());
          curTime = tempcurTime.clone().add(1, 'quarters');
        } else {
          curTime = null;
        }
      }
    } //groupMode为"MONTH"


    if (Object.is(groupMode, 'MONTH')) {
      let curTime = minTime;

      while (curTime) {
        if (curTime.isSameOrBefore(maxTime)) {
          const tempcurTime = curTime.clone();
          timeFragmentArray.push(tempcurTime.year().toString() + '-' + tempcurTime.month().toString());
          curTime = tempcurTime.clone().add(1, 'months');
        } else {
          curTime = null;
        }
      }
    } //groupMode为"YEARWEEK"


    if (Object.is(groupMode, 'YEARWEEK')) {
      let curTime = minTime;

      while (curTime) {
        if (curTime.isSameOrBefore(maxTime)) {
          const tempcurTime = curTime.clone();
          timeFragmentArray.push(tempcurTime.year().toString() + '-' + tempcurTime.week().toString());
          curTime = tempcurTime.clone().add(1, 'weeks');
        } else {
          curTime = null;
        }
      }
    } //groupMode为"DAY"


    if (Object.is(groupMode, 'DAY')) {
      let curTime = minTime;

      while (curTime) {
        if (curTime.isSameOrBefore(maxTime)) {
          const tempcurTime = curTime.clone();
          timeFragmentArray.push(tempcurTime.format('YYYY-MM-DD'));
          curTime = tempcurTime.clone().add(1, 'days');
        } else {
          curTime = null;
        }
      }
    }

    data.forEach(item => {
      const tempKeyStr = item[groupField.name];
      const tempGrounpItem = tempGrounpData.get(tempKeyStr);

      if (!tempGrounpItem) {
        tempGrounpData.set(tempKeyStr, item);
      }
    });
    timeFragmentArray.forEach(timeFragment => {
      if (!tempGrounpData.get(timeFragment)) {
        const copyTemp = _ibzCore.Util.deepCopy(data[0]);

        const curObj = {};
        curObj[groupField.name] = timeFragment;
        curObj[valueField.name] = 0;
        Object.assign(copyTemp, curObj);
        data.push(copyTemp);
      }
    });
  }
  /**
   * @description 补全代码表
   * @private
   * @param {*} data 传入数据
   * @param {*} item 单个序列
   * @param {*} allCodeList 所有的代码表
   * @return {*}
   * @memberof MobChartCtrlController
   */


  completeCodeList(data, item, allCodeList) {
    const groupField = item.dataSetFields.find(datasetField => {
      return datasetField.name === item.categorField;
    });

    if (!groupField.codelist) {
      return;
    }

    const valueField = item.dataSetFields.find(datasetField => {
      return datasetField.name === item.valueField;
    });
    const curCodeList = allCodeList.get(groupField.codelist.codeName); // 对分类实现分组

    const tempGrounpData = new Map();
    data.forEach(item => {
      const tempGrounpItem = tempGrounpData.get(item[groupField.name + '_srfvalue']);

      if (!tempGrounpItem) {
        tempGrounpData.set(item[groupField.name + '_srfvalue'], item);
      }
    });

    if (curCodeList.size !== tempGrounpData.size) {
      curCodeList.forEach((text, value) => {
        if (!tempGrounpData.get(value)) {
          const copyTemp = _ibzCore.Util.deepCopy(data[0]);

          const curObj = {};
          curObj[groupField.name + '_srfvalue'] = value;
          curObj[groupField.name] = text;
          curObj[valueField.name] = 0;
          Object.assign(copyTemp, curObj);
          data.push(copyTemp);
        }
      });
    }
  }
  /**
   * @description 处理单个属性
   * @private
   * @param {*} input 输入值
   * @param {*} field 属性值
   * @param {*} allCodeList 所有代码表
   * @param {*} result 结果值
   * @param {*} groupField 分组属性
   * @memberof MobChartCtrlController
   */


  handleSingleDataSetField(input, field, allCodeList, result, groupField) {
    const tempFieldObj = {}; //存在代码表的情况(自动转化值)

    if (field.codelist) {
      //获取代码表
      const curCodeList = allCodeList.get(field.codelist.codeName);
      tempFieldObj[field.name] = curCodeList.get(input[field.name]);
      tempFieldObj[field.name + '_srfvalue'] = input[field.name];
    } else {
      // 不存在代码表的情况
      if (groupField && Object.is(groupField.name, field.name)) {
        if (Object.is(groupField.groupMode, 'YEAR')) {
          tempFieldObj[field.name] = (0, _moment.default)(input[field.name]).year().toString();
        } else if (Object.is(groupField.groupMode, 'QUARTER')) {
          tempFieldObj[field.name] = (0, _moment.default)(input[field.name]).year().toString() + '-' + (0, _moment.default)(input[field.name]).quarters().toString();
        } else if (Object.is(groupField.groupMode, 'MONTH')) {
          tempFieldObj[field.name] = (0, _moment.default)(input[field.name]).year().toString() + '-' + (0, _moment.default)(input[field.name]).month().toString();
        } else if (Object.is(groupField.groupMode, 'YEARWEEK')) {
          tempFieldObj[field.name] = (0, _moment.default)(input[field.name]).year().toString() + '-' + (0, _moment.default)(input[field.name]).week().toString();
        } else if (Object.is(groupField.groupMode, 'DAY')) {
          tempFieldObj[field.name] = (0, _moment.default)(input[field.name]).format('YYYY-MM-DD');
        } else {
          tempFieldObj[field.name] = input[field.name];
        }
      } else {
        tempFieldObj[field.name] = input[field.name];
      }
    }

    Object.assign(result, tempFieldObj);
  }
  /**
   * @description 获取图表所需代码表
   * @private
   * @return {*}  {Promise<IParam>}
   * @memberof MobChartCtrlController
   */


  getChartAllCodeList() {
    return new Promise((resolve, reject) => {
      const codeListMap = new Map();

      if (Object.values(this.c.seriesModel).length > 0) {
        let tempFlag = true;
        Object.values(this.c.seriesModel).forEach(singleSeries => {
          if (singleSeries.dataSetFields && singleSeries.dataSetFields.length > 0) {
            const promiseArray = [];
            const promiseKeyArray = [];
            singleSeries.dataSetFields.forEach((singleDataSetField, index) => {
              if (singleDataSetField.codelist) {
                tempFlag = false;

                if (!codeListMap.get(singleDataSetField.codelist.codeName)) {
                  promiseArray.push(this.getCodeList(singleDataSetField.codelist));
                  promiseKeyArray.push(singleDataSetField.codelist.codeName);
                  Promise.all(promiseArray).then(result => {
                    if (result && result.length > 0) {
                      result.forEach(codeList => {
                        const tempCodeListMap = new Map();

                        if (codeList.length > 0) {
                          codeList.forEach(codeListItem => {
                            tempCodeListMap.set(codeListItem.value, codeListItem.text);
                          });
                        }

                        codeListMap.set(singleDataSetField.codelist.codeName, tempCodeListMap);
                      });
                      resolve(codeListMap);
                    }
                  });
                }
              }
            });
          }
        });

        if (tempFlag) {
          resolve(codeListMap);
        }
      } else {
        resolve(codeListMap);
      }
    });
  }
  /**
   * @description 获取代码表
   * @private
   * @param {*} codeListObject 代码表对象
   * @return {*}  {Promise<IParam>}
   * @memberof MobChartCtrlController
   */


  getCodeList(codeListObject) {
    return new Promise((resolve, reject) => {
      App.getCodeListService().getDataItems({
        tag: codeListObject.codeName,
        type: codeListObject.codeListType,
        navContext: this.c.context,
        navViewParam: this.c.viewParam
      }).then(res => {
        resolve(res);
      }).catch(error => {
        _ibzCore.LogUtil.error('代码表获取异常');
      });
    });
  }
  /**
   * @description 数组元素小写
   * @private
   * @param {*} arr 数组
   * @return {*} {IParam[]}
   * @memberof MobChartCtrlController
   */


  arrayToLowerCase(arr) {
    if (!arr || arr.length == 0) {
      return [];
    }

    for (let index = 0; index < arr.length; index++) {
      arr[index] = arr[index].toLowerCase();
    }

    return arr;
  }
  /**
   * @description 计算数据集最大数
   * @private
   * @param {any[]} source 传入数据
   * @return {*} {number}
   * @memberof MobChartCtrlController
   */


  calcSourceMaxValue(source) {
    const data = [];
    source.forEach(item => {
      if (item.data) {
        data.push(item.data);
      } else {
        const itemData = [];

        for (const key in item) {
          if (!isNaN(item[key])) {
            itemData.push(item[key]);
          }
        }

        data.push(Math.max(...itemData));
      }
    });
    return Math.max(...data);
  }
  /**
   * @description 是否为数组字符串
   * @private
   * @param {string} str 字符串
   * @return {*}  {boolean}
   * @memberof MobChartCtrlController
   */


  isArray(str) {
    try {
      eval(str);
      return true;
    } catch (error) {
      return false;
    }
  }
  /**
   * @description 是否为json字符串
   * @private
   * @param {*} str 字符串
   * @return {*}  {boolean}
   * @memberof MobChartCtrlController
   */


  isJson(str) {
    try {
      JSON.parse(str);
      return true;
    } catch (error) {
      return false;
    }
  }
  /**
   * @description 解析字符串函数
   * @private
   * @param {*} data 传入数据
   * @return {*}
   * @memberof MobChartCtrlController
   */


  deepJsonParseFun(data) {
    switch (typeof data) {
      case 'string':
      case 'number':
      case 'boolean':
      case 'undefined':
      case 'function':
      case 'symbol':
        return data;
    }

    for (const key in data) {
      const item = data[key];
      let res;

      if (item.isFun && item.functionBody) {
        res = new Function(item.arg, item.functionBody);
      } else {
        res = this.deepJsonParseFun(item);
      }

      data[key] = res;
    }

    return data;
  }
  /**
   * @description 整合图表数据集data
   * @private
   * @param {any[]} source 传入数据
   * @param {string} series 序列
   * @param {any[]} [indicator] 雷达图参数
   * @return {*}
   * @memberof MobChartCtrlController
   */

  /**
   *
   * @description 整合图表数据集data
   * @private
   * @param {*} series 序列
   * @param {any[]} source 传入数据
   * @param {string} type 类型
   * @param {any[]} [indicator] 雷达图参数
   * @return {*}  {*}
   * @memberof AppMobChart
   */


  transformToChartSeriesData(series, source, type, indicator) {
    var _a, _b;

    const seriesValueField = (_a = series.valueField) === null || _a === void 0 ? void 0 : _a.toLowerCase();
    const seriesCatalogField = (_b = series.catalogField) === null || _b === void 0 ? void 0 : _b.toLowerCase();

    if (Object.is(type, 'gauge')) {
      const seriesData = [];
      const offsetLength = 100 / (source.length - 1);
      source.forEach((sourceItem, index) => {
        const data = {
          name: sourceItem[seriesCatalogField],
          value: sourceItem[seriesValueField],
          title: {
            offsetCenter: [index * offsetLength - 50 + '%', '80%']
          },
          detail: {
            offsetCenter: [index * offsetLength - 50 + '%', '95%']
          }
        };
        seriesData.push(data);
      });
      return seriesData;
    } else if (Object.is(type, 'radar')) {
      if (!indicator || indicator.length == 0) {
        _ibzCore.LogUtil.log('雷达图indicator不存在，无法转换数据集！');

        return;
      }

      const seriesData = [];
      source.forEach(sourceItem => {
        const name = sourceItem.type;
        const data = [];
        indicator.forEach(item => {
          data.push(sourceItem[item.name]);
        });
        seriesData.push({
          name,
          value: data
        });
      });
      return seriesData;
    }
  }
  /**
   * @description 绘制图表
   * @return {*}
   * @memberof AppMobChart
   */


  render() {
    if (!this.controlIsLoaded.value) {
      return null;
    }

    const {
      width,
      height
    } = this.c.controlInstance;
    const chartControlstyle = {
      width: width ? `${width}px` : '100%',
      height: height ? `${height}px` : '100%'
    };
    return (0, _vue.createVNode)("div", {
      "class": Object.assign({}, this.classNames),
      "style": chartControlstyle
    }, [this.renderPullDownRefresh(), this.c.isNoData ? this.renderNoData() : (0, _vue.createVNode)("div", {
      "class": 'echarts',
      "id": this.c.chartId
    }, null)]);
  }

} // 移动端图表部件


exports.AppMobChart = AppMobChart;
const AppMobChartComponent = (0, _componentBase.GenerateComponent)(AppMobChart, Object.keys(new _ibzCore.AppMobChartProps()));
exports.AppMobChartComponent = AppMobChartComponent;