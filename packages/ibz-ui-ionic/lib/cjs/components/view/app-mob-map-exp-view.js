"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppMobMapExpViewComponent = exports.AppMobMapExpView = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _deExpViewComponentBase = require("./de-exp-view-component-base");

/**
 * @description 移动端地图导航视图
 * @export
 * @class AppMobMapExpView
 * @extends {DEViewComponentBase<AppMobMapExpViewProps>}
 */
class AppMobMapExpView extends _deExpViewComponentBase.DEExpViewComponentBase {
  /**
    * 设置响应式
    *
    * @public
    * @memberof AppMobMapExpView
    */
  setup() {
    this.c = (0, _vue.shallowReactive)(this.getViewControllerByType('DEMOBMAPEXPVIEW'));
    super.setup();
  }

} // 移动端地图导航视图组件


exports.AppMobMapExpView = AppMobMapExpView;
const AppMobMapExpViewComponent = (0, _componentBase.GenerateComponent)(AppMobMapExpView, new _ibzCore.AppMobMapExpViewProps());
exports.AppMobMapExpViewComponent = AppMobMapExpViewComponent;