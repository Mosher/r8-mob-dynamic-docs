"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppPortalViewComponent = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _viewComponentBase = require("./view-component-base");

/**
 * 移动端应用看板视图
 *
 * @class AppPortalView
 * @extends ViewComponentBase
 */
class AppPortalView extends _viewComponentBase.ViewComponentBase {
  /**
   * 设置响应式
   *
   * @memberof AppPortalView
   */
  setup() {
    this.c = (0, _vue.shallowReactive)(this.getViewControllerByType('APPPORTALVIEW'));
    super.setup();
  }

} //  移动端应用看板视图组件


const AppPortalViewComponent = (0, _componentBase.GenerateComponent)(AppPortalView, new _ibzCore.AppPortalViewProps());
exports.AppPortalViewComponent = AppPortalViewComponent;