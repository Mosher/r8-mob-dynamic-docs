"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppMobListExpViewComponent = exports.AppMobListExpView = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _deExpViewComponentBase = require("./de-exp-view-component-base");

/**
 * @description 移动端列表导航视图
 * @export
 * @class AppMobListExpView
 * @extends {DEViewComponentBase<AppMobListExpViewProps>}
 */
class AppMobListExpView extends _deExpViewComponentBase.DEExpViewComponentBase {
  /**
    * 设置响应式
    *
    * @public
    * @memberof AppMobListExpView
    */
  setup() {
    this.c = (0, _vue.shallowReactive)(this.getViewControllerByType('DEMOBLISTEXPVIEW'));
    super.setup();
  }

} // 移动端列表导航视图组件


exports.AppMobListExpView = AppMobListExpView;
const AppMobListExpViewComponent = (0, _componentBase.GenerateComponent)(AppMobListExpView, new _ibzCore.AppMobListExpViewProps());
exports.AppMobListExpViewComponent = AppMobListExpViewComponent;