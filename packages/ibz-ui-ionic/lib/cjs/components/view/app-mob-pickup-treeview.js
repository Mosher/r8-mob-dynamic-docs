"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppMobPickupTreeViewComponent = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _deMultiDataViewComponentBase = require("./de-multi-data-view-component-base");

/**
 * 移动端选择树视图
 *
 * @class AppMobPickupTreeView
 */
class AppMobPickupTreeView extends _deMultiDataViewComponentBase.DEMultiDataViewComponentBase {
  /**
   * @description 设置响应式
   * @memberof AppMobPickupTreeView
   */
  setup() {
    this.c = (0, _vue.shallowReactive)(this.getViewControllerByType('DEMOBPICKUPTREEVIEW'));
    super.setup();
  }
  /**
   * @description 额外部件参数
   * @param {*} ctrlProps 部件参数
   * @param {IPSControl} controlInstance 部件实例
   * @memberof AppMobPickupTreeView
   */


  extraCtrlParam(ctrlProps, controlInstance) {
    super.extraCtrlParam(ctrlProps, controlInstance);

    if ((controlInstance === null || controlInstance === void 0 ? void 0 : controlInstance.controlType) == 'TREEVIEW') {
      Object.assign(ctrlProps, {
        isMultiple: this.c.isMultiple,
        ctrlShowMode: 'SELECT'
      });
    }
  }

} //  移动端选择树视图组件


const AppMobPickupTreeViewComponent = (0, _componentBase.GenerateComponent)(AppMobPickupTreeView, new _ibzCore.AppMobPickupTreeViewProps());
exports.AppMobPickupTreeViewComponent = AppMobPickupTreeViewComponent;