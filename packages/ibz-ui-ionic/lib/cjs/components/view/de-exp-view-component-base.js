"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DEExpViewComponentBase = void 0;

var _deViewComponentBase = require("./de-view-component-base");

/**
 * @description 移动端实体导航视图部件基类
 * @export
 * @class DEExpViewComponentBase
 * @extends {DEViewComponentBase<AppDEExpViewProps>}
 * @template Props
 */
class DEExpViewComponentBase extends _deViewComponentBase.DEViewComponentBase {}

exports.DEExpViewComponentBase = DEExpViewComponentBase;