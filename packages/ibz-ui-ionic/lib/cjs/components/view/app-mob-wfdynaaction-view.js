"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppMobWFDynaActionViewComponent = exports.AppMobWFDynaActionView = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _deViewComponentBase = require("./de-view-component-base");

/**
 * @description 移动端动态工作流操作视图
 * @export
 * @class AppMobWFDynaActionView
 * @extends {DEViewComponentBase<AppMobWFDynaActionViewProps>}
 */
class AppMobWFDynaActionView extends _deViewComponentBase.DEViewComponentBase {
  /**
    * 设置响应式
    *
    * @public
    * @memberof AppMobWFDynaActionView
    */
  setup() {
    this.c = (0, _vue.shallowReactive)(this.getViewControllerByType('DEMOBWFDYNAACTIONVIEW'));
    super.setup();
  }
  /**
   * @description 提交
   * @param {*} event
   * @memberof AppMobWFDynaActionView
   */


  submit(event) {
    this.c.handleOk();
  }
  /**
   * @description 取消
   * @param {*} event
   * @memberof AppMobWFDynaActionView
   */


  cancel(event) {
    this.c.handleCancel();
  }
  /**
   * @description 渲染视图底部
   * @return {*}
   * @memberof AppMobWFDynaActionView
   */


  renderViewFooter() {
    return (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-footer"), {
      "class": ['view-footer', 'wf-action-view-footer']
    }, {
      default: () => [(0, _vue.createVNode)((0, _vue.resolveComponent)("ion-buttons"), null, {
        default: () => [(0, _vue.createVNode)((0, _vue.resolveComponent)("ion-button"), {
          "expand": 'full',
          "size": 'large',
          "onClick": event => {
            this.submit(event);
          }
        }, {
          default: () => [`${this.$tl('view.wfdynaactionview.submit', '提交')}`]
        }), (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-button"), {
          "expand": 'full',
          "size": 'large',
          "onClick": event => {
            this.cancel(event);
          }
        }, {
          default: () => [`${this.$tl('share.cancel', '取消')}`]
        })]
      })]
    });
  }
  /**
   * @description 渲染表单
   * @return {*}
   * @memberof AppMobWFDynaActionView
   */


  renderForm() {
    if (this.c.editFormInstance) {
      return this.computeTargetCtrlData(this.c.editFormInstance);
    } else {
      return null;
    }
  }
  /**
   * @description 渲染视图组件
   * @return {*}
   * @memberof AppMobWFDynaActionView
   */


  renderViewControls() {
    const controlObject = super.renderViewControls();
    Object.assign(controlObject, {
      viewFooter: () => this.renderViewFooter(),
      form: () => this.renderForm()
    });
    return controlObject;
  }

} // 移动端动态工作流操作视图组件


exports.AppMobWFDynaActionView = AppMobWFDynaActionView;
const AppMobWFDynaActionViewComponent = (0, _componentBase.GenerateComponent)(AppMobWFDynaActionView, new _ibzCore.AppMobWFDynaActionViewProps());
exports.AppMobWFDynaActionViewComponent = AppMobWFDynaActionViewComponent;