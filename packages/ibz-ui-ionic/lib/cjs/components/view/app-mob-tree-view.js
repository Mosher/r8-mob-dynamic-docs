"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppMobTreeViewComponent = exports.AppMobTreeView = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _deMultiDataViewComponentBase = require("./de-multi-data-view-component-base");

/**
 * 移动端树视图
 *
 * @class AppMobTreeView
 * @extends DEMultiDataViewComponentBase
 */
class AppMobTreeView extends _deMultiDataViewComponentBase.DEMultiDataViewComponentBase {
  /**
   * @description 设置响应式
   * @memberof AppMobTreeView
   */
  setup() {
    this.c = (0, _vue.shallowReactive)(this.getViewControllerByType('DEMOBTREEVIEW'));
    super.setup();
  }

} //  移动端树视图组件


exports.AppMobTreeView = AppMobTreeView;
const AppMobTreeViewComponent = (0, _componentBase.GenerateComponent)(AppMobTreeView, new _ibzCore.AppMobTreeViewProps());
exports.AppMobTreeViewComponent = AppMobTreeViewComponent;