"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppMobMDViewComponent = exports.AppMobMDView = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _deMultiDataViewComponentBase = require("./de-multi-data-view-component-base");

/**
 * 移动端多数据视图
 *
 * @class AppMobMDView
 * @extends ViewComponentBase
 */
class AppMobMDView extends _deMultiDataViewComponentBase.DEMultiDataViewComponentBase {
  /**
   * @description 设置响应式
   * @memberof AppMobMDView
   */
  setup() {
    this.c = (0, _vue.shallowReactive)(this.getViewControllerByType('DEMOBMDVIEW'));
    super.setup();
  }

} //  移动端多数据视图组件


exports.AppMobMDView = AppMobMDView;
const AppMobMDViewComponent = (0, _componentBase.GenerateComponent)(AppMobMDView, new _ibzCore.AppMobMDViewProps());
exports.AppMobMDViewComponent = AppMobMDViewComponent;