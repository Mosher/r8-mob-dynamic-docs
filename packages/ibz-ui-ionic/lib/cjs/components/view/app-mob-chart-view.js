"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppMobChartViewComponent = exports.AppMobChartView = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _deMultiDataViewComponentBase = require("./de-multi-data-view-component-base");

/**
 * 移动端图表视图
 *
 * @class AppMobChartView
 * @extends DEMultiDataViewComponentBase
 */
class AppMobChartView extends _deMultiDataViewComponentBase.DEMultiDataViewComponentBase {
  /**
   * @description 设置响应式
   * @memberof AppMobChartView
   */
  setup() {
    this.c = (0, _vue.shallowReactive)(this.getViewControllerByType('DEMOBCHARTVIEW'));
    super.setup();
  }

} //  移动端日历视图组件


exports.AppMobChartView = AppMobChartView;
const AppMobChartViewComponent = (0, _componentBase.GenerateComponent)(AppMobChartView, new _ibzCore.AppMobChartViewProps());
exports.AppMobChartViewComponent = AppMobChartViewComponent;