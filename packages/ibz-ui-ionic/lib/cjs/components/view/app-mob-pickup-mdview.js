"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppMobPickUpMDViewComponent = exports.AppMobPickUpMDView = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _deMultiDataViewComponentBase = require("./de-multi-data-view-component-base");

/**
 * 移动端多数据视图
 *
 * @class AppMobMDView
 * @extends ViewComponentBase
 */
class AppMobPickUpMDView extends _deMultiDataViewComponentBase.DEMultiDataViewComponentBase {
  /**
   * @description 设置响应式
   * @memberof AppMobPickUpMDView
   */
  setup() {
    this.c = (0, _vue.shallowReactive)(this.getViewControllerByType('DEMOBPICKUPMDVIEW'));
    super.setup();
  }
  /**
   * 额外部件参数
   *
   * @param ctrlProps 部件参数
   * @param controlInstance 部件
   */


  extraCtrlParam(ctrlProps, controlInstance) {
    super.extraCtrlParam(ctrlProps, controlInstance);

    if ((controlInstance === null || controlInstance === void 0 ? void 0 : controlInstance.controlType) == 'MOBMDCTRL') {
      Object.assign(ctrlProps, {
        isMultiple: this.c.isMultiple,
        ctrlShowMode: 'SELECT'
      });
    }
  }

} //  移动端选择多数据视图组件


exports.AppMobPickUpMDView = AppMobPickUpMDView;
const AppMobPickUpMDViewComponent = (0, _componentBase.GenerateComponent)(AppMobPickUpMDView, new _ibzCore.AppMobPickUpMDViewProps());
exports.AppMobPickUpMDViewComponent = AppMobPickUpMDViewComponent;