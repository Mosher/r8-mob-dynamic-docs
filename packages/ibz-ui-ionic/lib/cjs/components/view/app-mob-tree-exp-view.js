"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppMobTreeExpViewComponent = exports.AppMobTreeExpView = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _deExpViewComponentBase = require("./de-exp-view-component-base");

/**
 * @description 移动端树导航视图
 * @export
 * @class AppMobTreeExpView
 * @extends {DEViewComponentBase<AppMobTreeExpViewProps>}
 */
class AppMobTreeExpView extends _deExpViewComponentBase.DEExpViewComponentBase {
  /**
    * 设置响应式
    *
    * @public
    * @memberof AppMobTreeExpView
    */
  setup() {
    this.c = (0, _vue.shallowReactive)(this.getViewControllerByType('DEMOBTREEEXPVIEW'));
    super.setup();
  }

} // 移动端树导航视图组件


exports.AppMobTreeExpView = AppMobTreeExpView;
const AppMobTreeExpViewComponent = (0, _componentBase.GenerateComponent)(AppMobTreeExpView, new _ibzCore.AppMobTreeExpViewProps());
exports.AppMobTreeExpViewComponent = AppMobTreeExpViewComponent;