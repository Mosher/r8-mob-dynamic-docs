"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppMobDEDashboardViewComponent = exports.AppMobDEDashboardView = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _viewComponentBase = require("./view-component-base");

/**
 * 移动端实体数据看板视图
 *
 * @class AppMobDEDashboardView
 * @extends ViewComponentBase
 */
class AppMobDEDashboardView extends _viewComponentBase.ViewComponentBase {
  /**
   * 设置响应式
   *
   * @memberof AppMobDEDashboardView
   */
  setup() {
    this.c = (0, _vue.shallowReactive)(this.getViewControllerByType('DEMOBPORTALVIEW'));
    super.setup();
  }
  /**
   * @description 渲染信息栏
   * @return {*}
   * @memberof AppMobDEDashboardView
   */


  renderDataInfoBar() {
    var _a;

    if (this.c.viewInstance.showDataInfoBar) {
      return (0, _vue.createVNode)("span", {
        "class": "view-info-bar"
      }, [(_a = this.c.getData()) === null || _a === void 0 ? void 0 : _a.srfmajortext]);
    }
  }
  /**
   * @description 渲染视图组件
   * @return {*}
   * @memberof AppMobDEDashboardView
   */


  renderViewControls() {
    const controlObject = super.renderViewControls();
    Object.assign(controlObject, {
      'dataInfoBar': () => this.renderDataInfoBar()
    });
    return controlObject;
  }

} //  移动端实体数据看板视图组件


exports.AppMobDEDashboardView = AppMobDEDashboardView;
const AppMobDEDashboardViewComponent = (0, _componentBase.GenerateComponent)(AppMobDEDashboardView, new _ibzCore.AppMobDEDashboardViewProps());
exports.AppMobDEDashboardViewComponent = AppMobDEDashboardViewComponent;