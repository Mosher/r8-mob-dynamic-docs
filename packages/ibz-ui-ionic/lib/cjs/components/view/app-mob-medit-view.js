"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppMobMEditViewComponent = exports.AppMobMEditView = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _deMultiDataViewComponentBase = require("./de-multi-data-view-component-base");

/**
 * 移动端多表单编辑视图
 *
 * @class AppMobMEditView
 * @extends ViewComponentBase
 */
class AppMobMEditView extends _deMultiDataViewComponentBase.DEMultiDataViewComponentBase {
  /**
   * @description 设置响应式
   * @memberof AppMobMEditView
   */
  setup() {
    this.c = (0, _vue.shallowReactive)(this.getViewControllerByType('DEMOBMEDITVIEW9'));
    super.setup();
  }

} //  移动端多表单编辑视图组件


exports.AppMobMEditView = AppMobMEditView;
const AppMobMEditViewComponent = (0, _componentBase.GenerateComponent)(AppMobMEditView, new _ibzCore.AppMobMEditViewProps());
exports.AppMobMEditViewComponent = AppMobMEditViewComponent;