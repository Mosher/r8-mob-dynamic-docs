"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppIndexViewComponent = exports.AppIndexView = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _viewComponentBase = require("./view-component-base");

/**
 * 应用首页视图交联组件
 *
 * @export
 * @class AppIndexView
 */
class AppIndexView extends _viewComponentBase.ViewComponentBase {
  /**
   * @description 设置响应式
   * @memberof AppIndexView
   */
  setup() {
    this.c = (0, _vue.shallowReactive)(this.getViewControllerByType('APPINDEXVIEW'));
    super.setup();
  }

} // 应用首页组件


exports.AppIndexView = AppIndexView;
const AppIndexViewComponent = (0, _componentBase.GenerateComponent)(AppIndexView, new _ibzCore.AppIndexViewProps());
exports.AppIndexViewComponent = AppIndexViewComponent;