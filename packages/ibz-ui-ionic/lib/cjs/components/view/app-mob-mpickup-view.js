"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppMobMPickUpViewComponent = exports.AppMobMPickUpView = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _deViewComponentBase = require("./de-view-component-base");

/**
 * 移动端多数据选择视图
 *
 * @class AppMobMPickUpView
 * @extends ViewComponentBase
 */
class AppMobMPickUpView extends _deViewComponentBase.DEViewComponentBase {
  /**
   * @description 设置响应式
   * @memberof AppMobMPickUpView
   */
  setup() {
    this.c = (0, _vue.shallowReactive)(this.getViewControllerByType('DEMOBMPICKUPVIEW'));
    super.setup();
  }
  /**
   * @description 绘制顶部按钮
   * @return {*}
   * @memberof AppMobMPickUpView
   */


  renderHeaderButtons() {
    return (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-toolbar"), null, {
      default: () => [(0, _vue.createVNode)((0, _vue.resolveComponent)("ion-buttons"), {
        "slot": 'start'
      }, {
        default: () => [(0, _vue.createVNode)((0, _vue.resolveComponent)("ion-button"), {
          "size": 'small',
          "onClick": () => this.c.cancel()
        }, {
          default: () => [`${this.$tl('share.cancel', '取消')}`]
        })]
      }), (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-buttons"), {
        "slot": 'primary'
      }, {
        default: () => [(0, _vue.createVNode)((0, _vue.resolveComponent)("ion-button"), {
          "size": 'small',
          "onClick": () => this.c.ok()
        }, {
          default: () => [`${this.$tl('share.ok', '确认')}`]
        })]
      }), (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-title"), null, {
        default: () => [this.c.viewInstance.caption]
      })]
    });
  }
  /**
   * @description 绘制视图所有部件
   * @return {*}
   * @memberof AppMobMPickUpView
   */


  renderViewControls() {
    const controlObject = super.renderViewControls();
    Object.assign(controlObject, {
      headerButtons: () => this.renderHeaderButtons()
    });
    return controlObject;
  }
  /**
   * @description 额外部件参数
   * @param {*} ctrlProps 部件参数
   * @param {IPSControl} controlInstance 部件实例
   * @memberof AppMobMPickUpView
   */


  extraCtrlParam(ctrlProps, controlInstance) {
    super.extraCtrlParam(ctrlProps, controlInstance);

    if ((controlInstance === null || controlInstance === void 0 ? void 0 : controlInstance.controlType) == 'PICKUPVIEWPANEL') {
      Object.assign(ctrlProps, {
        isMultiple: true
      });
    }
  }

} //  移动端多数据选择视图组件


exports.AppMobMPickUpView = AppMobMPickUpView;
const AppMobMPickUpViewComponent = (0, _componentBase.GenerateComponent)(AppMobMPickUpView, new _ibzCore.AppMobMPickUpViewProps());
exports.AppMobMPickUpViewComponent = AppMobMPickUpViewComponent;