"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppMobPanelViewComponent = exports.AppMobPanelView = void 0;

var _ibzCore = require("ibz-core");

var _vue = require("vue");

var _componentBase = require("../component-base");

var _deViewComponentBase = require("./de-view-component-base");

class AppMobPanelView extends _deViewComponentBase.DEViewComponentBase {
  /**
   * 设置响应式
   *
   * @public
   * @memberof AppMobMDView
   */
  setup() {
    this.c = (0, _vue.shallowReactive)(this.getViewControllerByType('DEMOBPANELVIEW'));
    super.setup();
  }
  /**
   * 额外部件参数
   *
   * @param ctrlProps 部件参数
   * @param controlInstance 部件
   */


  extraCtrlParam(ctrlProps, controlInstance) {
    super.extraCtrlParam(ctrlProps, controlInstance);

    if ((controlInstance === null || controlInstance === void 0 ? void 0 : controlInstance.controlType) == 'PANEL') {
      Object.assign(ctrlProps, {
        isLoadDefault: this.c.isLoadDefault
      });
    }
  }

}

exports.AppMobPanelView = AppMobPanelView;
const AppMobPanelViewComponent = (0, _componentBase.GenerateComponent)(AppMobPanelView, new _ibzCore.AppMobPanelViewProps());
exports.AppMobPanelViewComponent = AppMobPanelViewComponent;