"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppMobOptViewComponent = exports.AppMobOptView = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _deViewComponentBase = require("./de-view-component-base");

/**
 * @description 移动端选项操作视图
 * @export
 * @class AppMobOptView
 * @extends {DEViewComponentBase<AppMobOptViewProps>}
 */
class AppMobOptView extends _deViewComponentBase.DEViewComponentBase {
  /**
    * 设置响应式
    *
    * @public
    * @memberof AppMobOptView
    */
  setup() {
    this.c = (0, _vue.shallowReactive)(this.getViewControllerByType('DEMOBOPTVIEW'));
    super.setup();
  }
  /**
   * @description 绘制底部按钮
   * @return {*}
   * @memberof AppMobOptView
   */


  renderFooterButtons() {
    return (0, _vue.createVNode)("div", {
      "class": "option-view-btnbox"
    }, [(0, _vue.createVNode)((0, _vue.resolveComponent)("ion-button"), {
      "expand": 'full',
      "size": 'large',
      "class": "option-btn-cancel",
      "onClick": () => this.c.cancel()
    }, {
      default: () => [`${this.$tl('share.cancel', '取消')}`]
    }), (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-button"), {
      "expand": 'full',
      "size": 'large',
      "class": "option-btn-success",
      "onClick": () => this.c.ok()
    }, {
      default: () => [`${this.$tl('share.ok', '确认')}`]
    })]);
  }
  /**
   * @description 绘制视图所有部件
   * @return {*}
   * @memberof AppMobMPickUpView
   */


  renderViewControls() {
    const controlObject = super.renderViewControls();
    Object.assign(controlObject, {
      footerButtons: () => this.renderFooterButtons()
    });
    return controlObject;
  }

} // 移动端选项操作视图组件


exports.AppMobOptView = AppMobOptView;
const AppMobOptViewComponent = (0, _componentBase.GenerateComponent)(AppMobOptView, new _ibzCore.AppMobOptViewProps());
exports.AppMobOptViewComponent = AppMobOptViewComponent;