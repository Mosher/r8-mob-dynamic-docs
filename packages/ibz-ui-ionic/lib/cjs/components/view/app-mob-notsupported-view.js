"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppMobNotSupportedViewComponent = exports.AppMobNotSupportedView = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _viewComponentBase = require("./view-component-base");

class AppMobNotSupportedView extends _viewComponentBase.ViewComponentBase {
  /**
   * 设置响应式
   *
   * @public
   * @memberof AppIndexView
   */
  setup() {
    this.c = (0, _vue.shallowReactive)(new _ibzCore.AppViewControllerBase(this.props));
    super.setup();
  }
  /**
   * 视图渲染
   *
   * @memberof AppDefaultNotSupportedView
   */


  render() {
    var _a, _b;

    if (!this.viewIsLoaded || !this.c.viewInstance) {
      return null;
    }

    const flexStyle = 'width: 100%; height: 100%; overflow: auto; display: flex;justify-content:center;align-items:center;';
    return (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-page"), {
      "class": 'app-view',
      "style": flexStyle
    }, {
      default: () => [`${this.$tl('view.notsupportview.tip', '暂未支持')}${(_b = (_a = this.c) === null || _a === void 0 ? void 0 : _a.viewInstance) === null || _b === void 0 ? void 0 : _b.title}`]
    });
  }

} // 应用首页组件


exports.AppMobNotSupportedView = AppMobNotSupportedView;
const AppMobNotSupportedViewComponent = (0, _componentBase.GenerateComponent)(AppMobNotSupportedView, new _ibzCore.AppViewProps());
exports.AppMobNotSupportedViewComponent = AppMobNotSupportedViewComponent;