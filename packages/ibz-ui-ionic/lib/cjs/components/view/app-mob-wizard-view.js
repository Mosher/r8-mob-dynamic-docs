"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppMobWizardViewComponent = exports.AppMobWizardView = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _deViewComponentBase = require("./de-view-component-base");

/**
 * @description 移动端向导视图
 * @export
 * @class AppMobWizardView
 * @extends {DEViewComponentBase<AppMobWizardViewProps>}
 */
class AppMobWizardView extends _deViewComponentBase.DEViewComponentBase {
  /**
    * 设置响应式
    *
    * @public
    * @memberof AppMobWizardView
    */
  setup() {
    this.c = (0, _vue.shallowReactive)(this.getViewControllerByType('DEMOBWIZARDVIEW'));
    super.setup();
  }

} // 移动端向导视图组件


exports.AppMobWizardView = AppMobWizardView;
const AppMobWizardViewComponent = (0, _componentBase.GenerateComponent)(AppMobWizardView, new _ibzCore.AppMobWizardViewProps());
exports.AppMobWizardViewComponent = AppMobWizardViewComponent;