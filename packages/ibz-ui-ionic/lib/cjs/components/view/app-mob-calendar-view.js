"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppMobCalendarViewComponent = exports.AppMobCalendarView = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _deMultiDataViewComponentBase = require("./de-multi-data-view-component-base");

/**
 * 移动端多数据视图
 *
 * @class AppMobCalendarView
 * @extends DEMultiDataViewComponentBase
 */
class AppMobCalendarView extends _deMultiDataViewComponentBase.DEMultiDataViewComponentBase {
  /**
   * 设置响应式
   *
   * @public
   * @memberof AppMobCalendarView
   */
  setup() {
    this.c = (0, _vue.shallowReactive)(this.getViewControllerByType('DEMOBCALENDARVIEW'));
    super.setup();
  }

} //  移动端日历视图组件


exports.AppMobCalendarView = AppMobCalendarView;
const AppMobCalendarViewComponent = (0, _componentBase.GenerateComponent)(AppMobCalendarView, new _ibzCore.AppMobCalendarViewProps());
exports.AppMobCalendarViewComponent = AppMobCalendarViewComponent;