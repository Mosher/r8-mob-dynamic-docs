"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppMobHtmlViewComponent = exports.AppMobHtmlView = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _deViewComponentBase = require("./de-view-component-base");

/**
 * @description 移动端HTML视图
 * @export
 * @class AppMobHtmlView
 * @extends {DEViewComponentBase<AppMobHtmlViewProps>}
 */
class AppMobHtmlView extends _deViewComponentBase.DEViewComponentBase {
  /**
   * @description 设置响应式
   * @memberof AppMobHtmlView
   */
  setup() {
    this.c = (0, _vue.shallowReactive)(this.getViewControllerByType('DEMOBHTMLVIEW'));
    super.setup();
  }
  /**
   * @description 绘制html内容
   * @return {*}
   * @memberof AppMobHtmlView
   */


  renderHtmlContent() {
    var _a, _b;

    if ((_b = (_a = this.c) === null || _a === void 0 ? void 0 : _a.viewInstance) === null || _b === void 0 ? void 0 : _b.htmlUrl) {
      const iframeUrl = _ibzCore.StringUtil.fillStrData(this.c.viewInstance.htmlUrl, this.c.context, this.c.viewParam);

      return (0, _vue.createVNode)("iframe", {
        "class": 'view-html-container',
        "src": iframeUrl
      }, null);
    } else {
      return (0, _vue.createVNode)("div", {
        "class": 'view-error-container'
      }, [(0, _vue.createVNode)("img", {
        "src": 'assets/images/404.jpg',
        "alt": '404'
      }, null), (0, _vue.createVNode)("div", {
        "class": 'error-text'
      }, [`${this.$tl('view.mobhtmlview.notexist', '抱歉，您访问的页面不存在！')}`])]);
    }
  }
  /**
   * @description 绘制视图所有部件
   * @return {*}
   * @memberof AppMobHtmlView
   */


  renderViewControls() {
    const controlObject = super.renderViewControls();
    Object.assign(controlObject, {
      htmlContent: () => this.renderHtmlContent()
    });
    return controlObject;
  }

} // 移动端html视图组件


exports.AppMobHtmlView = AppMobHtmlView;
const AppMobHtmlViewComponent = (0, _componentBase.GenerateComponent)(AppMobHtmlView, new _ibzCore.AppMobHtmlViewProps());
exports.AppMobHtmlViewComponent = AppMobHtmlViewComponent;