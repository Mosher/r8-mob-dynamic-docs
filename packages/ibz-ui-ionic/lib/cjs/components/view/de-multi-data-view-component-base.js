"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DEMultiDataViewComponentBase = void 0;

var _vue = require("vue");

var _deViewComponentBase = require("./de-view-component-base");

/**
 * @description 多数据视图父类
 * @export
 * @class DEMultiDataViewComponentBase
 * @extends {DEViewComponentBase<AppDEMultiDataViewProps>}
 * @template Props
 */
class DEMultiDataViewComponentBase extends _deViewComponentBase.DEViewComponentBase {
  /**
   * @description 快速搜索值变化
   * @param {*} 快速分组抛出值
   * @memberof DEMultiDataViewComponentBase
   */
  quickGroupValueChange(data) {
    if (this.c.quickGroupValueChange && this.c.quickGroupValueChange instanceof Function) {
      this.c.quickGroupValueChange(data);
    }
  }
  /**
   * @description 快速搜索
   * @param {*} query 快速搜索值
   * @memberof DEMultiDataViewComponentBase
   */


  quickSearch(query) {
    if (this.c.quickSearch && this.c.quickSearch instanceof Function) {
      this.c.quickSearch(query);
    }
  }
  /**
   * @description 渲染快速分组
   * @return {*}
   * @memberof DEMultiDataViewComponentBase
   */


  renderQuickGroup() {
    if (this.c.viewInstance.enableQuickGroup) {
      return (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-header"), {
        "class": 'app-quick-group'
      }, {
        default: () => [(0, _vue.createVNode)((0, _vue.resolveComponent)("ion-toolbar"), null, {
          default: () => [(0, _vue.createVNode)((0, _vue.resolveComponent)("app-quick-group"), {
            "items": this.c.quickGroupModel,
            "onValueChange": event => {
              this.quickGroupValueChange(event);
            }
          }, null)]
        })]
      });
    }
  }
  /**
   * @description 绘制快速搜索
   * @return {*}
   * @memberof DEMultiDataViewComponentBase
   */


  renderQuickSearch() {
    return (0, _vue.createVNode)((0, _vue.resolveComponent)("app-quick-search"), {
      "onQuickSearch": event => this.quickSearch(event)
    }, null);
  }
  /**
   * @description 渲染视图部件
   * @return {*}
   * @memberof DEMultiDataViewComponentBase
   */


  renderViewControls() {
    const viewControls = super.renderViewControls();

    if (this.c.viewInstance.enableQuickGroup) {
      Object.assign(viewControls, {
        quickGroupSearch: () => {
          return this.renderQuickGroup();
        }
      });
    }

    if (this.c.viewInstance.enableQuickSearch) {
      Object.assign(viewControls, {
        quickSearch: () => this.renderQuickSearch()
      });
    }

    return viewControls;
  }
  /**
   * @description 额外部件参数
   * @param {*} ctrlProps 部件参数
   * @param {IPSControl} controlInstance 部件实例
   * @memberof DEMultiDataViewComponentBase
   */


  extraCtrlParam(ctrlProps, controlInstance) {
    super.extraCtrlParam(ctrlProps, controlInstance);

    if ((controlInstance === null || controlInstance === void 0 ? void 0 : controlInstance.controlType) == 'SEARCHFORM') {
      Object.assign(ctrlProps, {
        expandSearchForm: this.c.viewInstance.expandSearchForm
      });
    }
  }

}

exports.DEMultiDataViewComponentBase = DEMultiDataViewComponentBase;