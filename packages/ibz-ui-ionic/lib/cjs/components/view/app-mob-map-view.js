"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppMobMapViewComponent = exports.AppMobMapView = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _deMultiDataViewComponentBase = require("./de-multi-data-view-component-base");

class AppMobMapView extends _deMultiDataViewComponentBase.DEMultiDataViewComponentBase {
  /**
   * 设置响应式
   *
   * @public
   * @memberof AppMobMapView
   */
  setup() {
    this.c = (0, _vue.shallowReactive)(this.getViewControllerByType('DEMOBMAPVIEW'));
    super.setup();
  }

} // 应用首页组件


exports.AppMobMapView = AppMobMapView;
const AppMobMapViewComponent = (0, _componentBase.GenerateComponent)(AppMobMapView, new _ibzCore.AppMobMapViewProps());
exports.AppMobMapViewComponent = AppMobMapViewComponent;