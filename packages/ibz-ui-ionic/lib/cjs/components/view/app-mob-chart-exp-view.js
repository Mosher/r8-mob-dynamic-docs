"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppMobChartExpViewComponent = exports.AppMobChartExpView = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _deExpViewComponentBase = require("./de-exp-view-component-base");

/**
 * @description 移动端图表导航视图
 * @export
 * @class AppMobChartExpView
 * @extends {DEViewComponentBase<AppMobChartExpViewProps>}
 */
class AppMobChartExpView extends _deExpViewComponentBase.DEExpViewComponentBase {
  /**
   * 设置响应式
   *
   * @public
   * @memberof AppMobChartExpView
   */
  setup() {
    this.c = (0, _vue.shallowReactive)(this.getViewControllerByType('DEMOBCHARTEXPVIEW'));
    super.setup();
  }

} // 移动端图表导航视图组件


exports.AppMobChartExpView = AppMobChartExpView;
const AppMobChartExpViewComponent = (0, _componentBase.GenerateComponent)(AppMobChartExpView, new _ibzCore.AppMobChartExpViewProps());
exports.AppMobChartExpViewComponent = AppMobChartExpViewComponent;