"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppMobWFDynaEditViewComponent = exports.AppMobWFDynaEditView = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _deViewComponentBase = require("./de-view-component-base");

/**
 * @description 移动端动态工作流编辑视图
 * @export
 * @class AppMobWFDynaEditView
 * @extends {DEViewComponentBase<AppMobWFDynaEditViewProps>}
 */
function _isSlot(s) {
  return typeof s === 'function' || Object.prototype.toString.call(s) === '[object Object]' && !(0, _vue.isVNode)(s);
}

class AppMobWFDynaEditView extends _deViewComponentBase.DEViewComponentBase {
  /**
    * 设置响应式
    *
    * @public
    * @memberof AppMobWFDynaEditView
    */
  setup() {
    this.c = (0, _vue.shallowReactive)(this.getViewControllerByType('DEMOBWFDYNAEDITVIEW'));
    super.setup();
  }
  /**
   * @description 工具栏按钮点击
   * @param {*} linkItem 工具栏项
   * @param {*} event 源事件对象
   * @memberof AppMobWFDynaEditView
   */


  toolbarClick(linkItem, event) {
    this.c.dynamicToolbarClick(linkItem, event);
  }
  /**
   * @description 渲染工具栏
   * @return {*}
   * @memberof AppMobWFDynaEditView
   */


  renderToolbar() {
    let _slot;

    return (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-toolbar"), {
      "class": ['wf-toolbar'],
      "slot": "fixed"
    }, {
      default: () => [(0, _vue.createVNode)((0, _vue.resolveComponent)("ion-buttons"), {
        "slot": "end"
      }, _isSlot(_slot = this.c.linkModel.map((linkItem, index) => {
        return (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-button"), {
          "key": index,
          "expand": 'block',
          "onClick": event => {
            this.toolbarClick(linkItem, event);
          }
        }, {
          default: () => [(0, _vue.createVNode)("span", {
            "class": "caption"
          }, [linkItem.sequenceFlowName])]
        });
      })) ? _slot : {
        default: () => [_slot]
      })]
    });
  }
  /**
   * @description 渲染表单
   * @return {*}
   * @memberof AppMobWFDynaEditView
   */


  renderForm() {
    if (this.c.editFormInstance) {
      return this.computeTargetCtrlData(this.c.editFormInstance);
    } else {
      return null;
    }
  }
  /**
   * @description 渲染视图部件
   * @return {*}
   * @memberof AppMobWFDynaEditView
   */


  renderViewControls() {
    const controlObject = super.renderViewControls();
    Object.assign(controlObject, {
      righttoolbar: () => this.renderToolbar(),
      form: () => this.renderForm()
    });
    return controlObject;
  }

} // 移动端动态工作流编辑视图组件


exports.AppMobWFDynaEditView = AppMobWFDynaEditView;
const AppMobWFDynaEditViewComponent = (0, _componentBase.GenerateComponent)(AppMobWFDynaEditView, new _ibzCore.AppMobWFDynaEditViewProps());
exports.AppMobWFDynaEditViewComponent = AppMobWFDynaEditViewComponent;