"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppMobCustomViewComponent = exports.AppMobCustomView = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _deViewComponentBase = require("./de-view-component-base");

/**
 * 移动端自定义视图
 *
 * @class AppMobCustomView
 * @extends ViewComponentBase
 */
class AppMobCustomView extends _deViewComponentBase.DEViewComponentBase {
  /**
   * 设置响应式
   *
   * @public
   * @memberof AppMobCustomView
   */
  setup() {
    this.c = (0, _vue.shallowReactive)(this.getViewControllerByType('DEMOBCUSTOMVIEW'));
    super.setup();
  }

} //  移动端自定义视图组件


exports.AppMobCustomView = AppMobCustomView;
const AppMobCustomViewComponent = (0, _componentBase.GenerateComponent)(AppMobCustomView, new _ibzCore.AppMobCustomViewProps());
exports.AppMobCustomViewComponent = AppMobCustomViewComponent;