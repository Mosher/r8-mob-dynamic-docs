"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DEViewComponentBase = void 0;

var _viewComponentBase = require("./view-component-base");

class DEViewComponentBase extends _viewComponentBase.ViewComponentBase {}

exports.DEViewComponentBase = DEViewComponentBase;