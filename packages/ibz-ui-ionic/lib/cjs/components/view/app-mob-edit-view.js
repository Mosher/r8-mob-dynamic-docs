"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppMobEditViewComponent = exports.AppMobEditView = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _deViewComponentBase = require("./de-view-component-base");

class AppMobEditView extends _deViewComponentBase.DEViewComponentBase {
  /**
   * 设置响应式
   *
   * @public
   * @memberof AppMobEditView
   */
  setup() {
    this.c = (0, _vue.shallowReactive)(this.getViewControllerByType('DEMOBEDITVIEW'));
    super.setup();
  }
  /**
   * @description 渲染信息栏
   * @return {*}
   * @memberof AppMobEditView
   */


  renderDataInfoBar() {
    var _a;

    if (this.c.viewInstance.showDataInfoBar) {
      return (0, _vue.createVNode)("span", {
        "class": "view-info-bar"
      }, [(_a = this.c.getData()) === null || _a === void 0 ? void 0 : _a.srfmajortext]);
    }
  }
  /**
   * @description 渲染视图组件
   * @return {*}
   * @memberof AppMobEditView
   */


  renderViewControls() {
    const controlObject = super.renderViewControls();
    Object.assign(controlObject, {
      'dataInfoBar': () => this.renderDataInfoBar()
    });
    return controlObject;
  }

} // 应用首页组件


exports.AppMobEditView = AppMobEditView;
const AppMobEditViewComponent = (0, _componentBase.GenerateComponent)(AppMobEditView, new _ibzCore.AppMobEditViewProps());
exports.AppMobEditViewComponent = AppMobEditViewComponent;