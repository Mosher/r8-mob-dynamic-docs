"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppMobTabExpViewComponent = exports.AppMobTabExpView = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _deViewComponentBase = require("./de-view-component-base");

/**
 * 移动端分页导航视图
 *
 * @class AppMobTabExpView
 * @extends DEViewComponentBase
 */
class AppMobTabExpView extends _deViewComponentBase.DEViewComponentBase {
  /**
   * 设置响应式
   *
   * @public
   * @memberof AppMobTabExpView
   */
  setup() {
    this.c = (0, _vue.shallowReactive)(this.getViewControllerByType('DEMOBTABEXPVIEW'));
    super.setup();
  }
  /**
   * @description 渲染信息栏
   * @return {*}
   * @memberof AppMobTabExpView
   */


  renderDataInfoBar() {
    var _a;

    if (this.c.viewInstance.showDataInfoBar) {
      return (0, _vue.createVNode)("span", {
        "class": "view-info-bar"
      }, [(_a = this.c.getData()) === null || _a === void 0 ? void 0 : _a.srfmajortext]);
    }
  }
  /**
   * @description 渲染视图组件
   * @return {*}
   * @memberof AppMobTabExpView
   */


  renderViewControls() {
    const controlObject = super.renderViewControls();
    Object.assign(controlObject, {
      'dataInfoBar': () => this.renderDataInfoBar()
    });
    return controlObject;
  }

} //  移动端多数据视图组件


exports.AppMobTabExpView = AppMobTabExpView;
const AppMobTabExpViewComponent = (0, _componentBase.GenerateComponent)(AppMobTabExpView, new _ibzCore.AppMobTabExpViewProps());
exports.AppMobTabExpViewComponent = AppMobTabExpViewComponent;