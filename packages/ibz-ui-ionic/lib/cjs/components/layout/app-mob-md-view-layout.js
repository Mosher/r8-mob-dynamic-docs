"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppDefaultMobMDViewLayoutComponent = exports.AppDefaultMobMDViewLayout = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _multiDataViewLayoutComponentBase = require("./multi-data-view-layout-component-base");

/**
 * 移动端多数据视图视图布局面板（DEFAULT样式）
 *
 * @class AppDefaultMobMDViewLayout
 * @extends MultiDataViewLayoutComponentBase
 */
class AppDefaultMobMDViewLayout extends _multiDataViewLayoutComponentBase.MultiDataViewLayoutComponentBase {
  /**
   * @description 视图主容器内容
   * @return {*}  {any}
   * @memberof AppDefaultMobMDViewLayout
   */
  renderViewMainContainerContent() {
    return (0, _vue.createVNode)("div", {
      "class": 'view-main-container-content'
    }, [(0, _vue.renderSlot)(this.ctx.slots, 'mdctrl')]);
  }

} //  移动端多数据视图视图布局面板部件


exports.AppDefaultMobMDViewLayout = AppDefaultMobMDViewLayout;
const AppDefaultMobMDViewLayoutComponent = (0, _componentBase.GenerateComponent)(AppDefaultMobMDViewLayout, new _ibzCore.AppMobMDViewLayoutProps());
exports.AppDefaultMobMDViewLayoutComponent = AppDefaultMobMDViewLayoutComponent;