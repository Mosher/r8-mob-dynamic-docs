"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppDefaultMobListExpViewLayoutComponent = exports.AppDefaultMobListExpViewLayout = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _layoutComponentBase = require("./layout-component-base");

/**
 * @description 移动端列表导航视图视图布局组件
 * @export
 * @class AppDefaultMobListExpViewLayout
 * @extends {AppDefaultDEExpView<AppMobListExpViewLayoutProps>}
 */
class AppDefaultMobListExpViewLayout extends _layoutComponentBase.LayoutComponentBase {
  /**
   * @description 渲染部件
   * @return {*}
   * @memberof AppDefaultMobListExpViewLayout
   */
  renderViewMainContainerContent() {
    return (0, _vue.createVNode)("div", {
      "class": 'view-main-container-content'
    }, [[(0, _vue.renderSlot)(this.ctx.slots, 'listexpbar')]]);
  }

} // 应用首页组件


exports.AppDefaultMobListExpViewLayout = AppDefaultMobListExpViewLayout;
const AppDefaultMobListExpViewLayoutComponent = (0, _componentBase.GenerateComponent)(AppDefaultMobListExpViewLayout, new _ibzCore.AppMobListExpViewLayoutProps());
exports.AppDefaultMobListExpViewLayoutComponent = AppDefaultMobListExpViewLayoutComponent;