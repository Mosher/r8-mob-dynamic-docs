"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppDefaultMobWFDynaEditViewLayoutComponent = exports.AppDefaultMobWFDynaEditViewLayout = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _layoutComponentBase = require("./layout-component-base");

/**
 * @description 移动端动态工作流编辑视图视图布局组件
 * @export
 * @class AppDefaultMobWFDynaEditViewLayout
 * @extends {AppDefaultDeView<AppMobWFDynaEditViewLayoutProps>}
 */
class AppDefaultMobWFDynaEditViewLayout extends _layoutComponentBase.LayoutComponentBase {
  /**
   * @description 渲染部件
   * @return {*}
   * @memberof AppDefaultMobWFDynaEditViewLayout
   */
  renderViewMainContainerContent() {
    return (0, _vue.createVNode)("div", {
      "class": 'view-main-container-content'
    }, [[(0, _vue.renderSlot)(this.ctx.slots, 'form')]]);
  }

} // 应用首页组件


exports.AppDefaultMobWFDynaEditViewLayout = AppDefaultMobWFDynaEditViewLayout;
const AppDefaultMobWFDynaEditViewLayoutComponent = (0, _componentBase.GenerateComponent)(AppDefaultMobWFDynaEditViewLayout, new _ibzCore.AppMobWFDynaEditViewLayoutProps());
exports.AppDefaultMobWFDynaEditViewLayoutComponent = AppDefaultMobWFDynaEditViewLayoutComponent;