"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppDefaultPortalViewLayoutComponent = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _layoutComponentBase = require("./layout-component-base");

/**
 * 移动端应用看板视图视图布局面板
 *
 * @class AppDefaultPortalViewLayout
 * @extends LayoutComponentBase
 */
class AppDefaultPortalViewLayout extends _layoutComponentBase.LayoutComponentBase {
  /**
   * @description 视图主容器内容
   * @return {*}
   * @memberof AppDefaultPortalViewLayout
   */
  renderViewMainContainerContent() {
    return (0, _vue.createVNode)("div", {
      "class": 'view-main-container-content'
    }, [[(0, _vue.renderSlot)(this.ctx.slots, 'dashboard')]]);
  }

} //  移动端应用看板视图视图布局面板组件


const AppDefaultPortalViewLayoutComponent = (0, _componentBase.GenerateComponent)(AppDefaultPortalViewLayout, new _ibzCore.AppPortalViewLayoutProps());
exports.AppDefaultPortalViewLayoutComponent = AppDefaultPortalViewLayoutComponent;