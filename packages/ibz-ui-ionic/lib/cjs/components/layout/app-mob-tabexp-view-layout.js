"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppDefaultMobTabExpViewLayoutComponent = exports.AppDefaultMobTabExpViewLayout = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _layoutComponentBase = require("./layout-component-base");

/**
 * 移动端分页导航视图视图布局面板
 *
 * @class AppDefaultMobTabExpViewLayout
 * @extends ComponentBase
 */
class AppDefaultMobTabExpViewLayout extends _layoutComponentBase.LayoutComponentBase {
  /**
   * @description 视图主容器内容
   * @return {*}
   * @memberof AppDefaultMobTabExpViewLayout
   */
  renderViewMainContainerContent() {
    return (0, _vue.createVNode)("div", {
      "class": 'view-main-container-content'
    }, [[(0, _vue.renderSlot)(this.ctx.slots, 'tabexppanel')]]);
  }

} //  移动端多数据视图视图布局面板部件


exports.AppDefaultMobTabExpViewLayout = AppDefaultMobTabExpViewLayout;
const AppDefaultMobTabExpViewLayoutComponent = (0, _componentBase.GenerateComponent)(AppDefaultMobTabExpViewLayout, new _ibzCore.AppMobTabExpViewLayoutProps());
exports.AppDefaultMobTabExpViewLayoutComponent = AppDefaultMobTabExpViewLayoutComponent;