"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppDefaultMobMapViewLayoutComponent = exports.AppDefaultMobMapViewLayout = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _multiDataViewLayoutComponentBase = require("./multi-data-view-layout-component-base");

/**
 * 移动端地图视图视图布局面板（DEFAULT样式）
 *
 * @class AppDefaultMobMapViewLayout
 * @extends ComponentBase
 */
class AppDefaultMobMapViewLayout extends _multiDataViewLayoutComponentBase.MultiDataViewLayoutComponentBase {
  /**
   * @description 视图主容器内容
   * @return {*}
   * @memberof AppDefaultMobMapViewLayout
   */
  renderViewMainContainerContent() {
    return (0, _vue.createVNode)("div", {
      "class": 'view-main-container-content'
    }, [[(0, _vue.renderSlot)(this.ctx.slots, 'map')]]);
  }

} //  移动端多数据视图视图布局面板部件


exports.AppDefaultMobMapViewLayout = AppDefaultMobMapViewLayout;
const AppDefaultMobMapViewLayoutComponent = (0, _componentBase.GenerateComponent)(AppDefaultMobMapViewLayout, new _ibzCore.AppMobMapViewLayoutProps());
exports.AppDefaultMobMapViewLayoutComponent = AppDefaultMobMapViewLayoutComponent;