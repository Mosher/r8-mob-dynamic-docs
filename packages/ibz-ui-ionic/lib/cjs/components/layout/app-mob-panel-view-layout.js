"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppDefaultMobPanelViewLayoutComponent = exports.AppDefaultMobPanelViewLayout = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _layoutComponentBase = require("./layout-component-base");

class AppDefaultMobPanelViewLayout extends _layoutComponentBase.LayoutComponentBase {
  /**
   * @description 视图主容器内容
   * @return {*}
   * @memberof AppDefaultMobMDViewLayout
   */
  renderViewMainContainerContent() {
    return (0, _vue.createVNode)("div", {
      "class": 'view-main-container-content'
    }, [[(0, _vue.renderSlot)(this.ctx.slots, 'panel')]]);
  }

} //  移动端面板视图视图布局面板部件


exports.AppDefaultMobPanelViewLayout = AppDefaultMobPanelViewLayout;
const AppDefaultMobPanelViewLayoutComponent = (0, _componentBase.GenerateComponent)(AppDefaultMobPanelViewLayout, new _ibzCore.AppMobPanelViewLayoutProps());
exports.AppDefaultMobPanelViewLayoutComponent = AppDefaultMobPanelViewLayoutComponent;