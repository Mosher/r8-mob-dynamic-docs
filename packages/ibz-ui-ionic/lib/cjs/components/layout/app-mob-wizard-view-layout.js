"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppDefaultMobWizardViewLayoutComponent = exports.AppDefaultMobWizardViewLayout = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _layoutComponentBase = require("./layout-component-base");

/**
 * @description 移动端向导视图视图布局组件
 * @export
 * @class AppDefaultMobWizardViewLayout
 * @extends {AppDefaultDEView<AppMobWizardViewLayoutProps>}
 */
class AppDefaultMobWizardViewLayout extends _layoutComponentBase.LayoutComponentBase {
  /**
   * @description 渲染部件
   * @return {*}
   * @memberof AppDefaultMobWizardViewLayout
   */
  renderViewMainContainerContent() {
    return (0, _vue.createVNode)("div", {
      "class": 'view-main-container-content'
    }, [[(0, _vue.renderSlot)(this.ctx.slots, 'wizardpanel')]]);
  }

} // 应用首页组件


exports.AppDefaultMobWizardViewLayout = AppDefaultMobWizardViewLayout;
const AppDefaultMobWizardViewLayoutComponent = (0, _componentBase.GenerateComponent)(AppDefaultMobWizardViewLayout, new _ibzCore.AppMobWizardViewLayoutProps());
exports.AppDefaultMobWizardViewLayoutComponent = AppDefaultMobWizardViewLayoutComponent;