"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppDefaultMobCustomViewLayoutComponent = exports.AppDefaultMobCustomViewLayout = void 0;

var _vue = require("vue");

var _runtimeDom = require("@vue/runtime-dom");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _layoutComponentBase = require("./layout-component-base");

class AppDefaultMobCustomViewLayout extends _layoutComponentBase.LayoutComponentBase {
  /**
   *
   * @description 视图主容器头部
   * @return {*}
   * @memberof MultiDataViewLayoutComponentBase
   */
  renderViewMainContainerHeader() {
    return (0, _vue.createVNode)("div", {
      "class": 'view-main-container-header'
    }, [[(0, _runtimeDom.renderSlot)(this.ctx.slots, 'quickGroupSearch'), (0, _runtimeDom.renderSlot)(this.ctx.slots, 'quickSearch'), (0, _runtimeDom.renderSlot)(this.ctx.slots, 'searchform'), (0, _runtimeDom.renderSlot)(this.ctx.slots, 'quicksearchform')]]);
  }
  /**
   * @description 视图主容器内容
   * @return {*}
   * @memberof AppDefaultMobCustomViewLayout
   */


  renderViewMainContainerContent() {
    const ctrlSlots = [];

    for (const slotName in this.ctx.slots) {
      if (!Object.is('topMessage', slotName) && !Object.is('bottomMessage', slotName) && !Object.is('bodyMessage', slotName) && !Object.is('quickGroupSearch', slotName) && !Object.is('quickSearch', slotName) && !Object.is('searchform', slotName) && !Object.is('quicksearchform', slotName)) {
        ctrlSlots.push((0, _runtimeDom.renderSlot)(this.ctx.slots, slotName));
      }
    }

    return (0, _vue.createVNode)("div", {
      "class": 'view-main-container-content'
    }, [ctrlSlots]);
  }

} // 自定义视图布局面板


exports.AppDefaultMobCustomViewLayout = AppDefaultMobCustomViewLayout;
const AppDefaultMobCustomViewLayoutComponent = (0, _componentBase.GenerateComponent)(AppDefaultMobCustomViewLayout, new _ibzCore.AppMobCustomViewLayoutProps());
exports.AppDefaultMobCustomViewLayoutComponent = AppDefaultMobCustomViewLayoutComponent;