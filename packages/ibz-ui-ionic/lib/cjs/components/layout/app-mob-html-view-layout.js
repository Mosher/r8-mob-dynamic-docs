"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppDefaultMobHtmlViewLayoutComponent = exports.AppDefaultMobHtmlViewLayout = void 0;

var _vue = require("vue");

var _runtimeDom = require("@vue/runtime-dom");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _layoutComponentBase = require("./layout-component-base");

class AppDefaultMobHtmlViewLayout extends _layoutComponentBase.LayoutComponentBase {
  /**
   * @description 视图主容器内容
   * @return {*}
   * @memberof AppDefaultMobHtmlViewLayout
   */
  renderViewMainContainerContent() {
    return (0, _vue.createVNode)("div", {
      "class": 'view-main-container-content'
    }, [(0, _runtimeDom.renderSlot)(this.ctx.slots, 'htmlContent')]);
  }

} // 应用首页组件


exports.AppDefaultMobHtmlViewLayout = AppDefaultMobHtmlViewLayout;
const AppDefaultMobHtmlViewLayoutComponent = (0, _componentBase.GenerateComponent)(AppDefaultMobHtmlViewLayout, new _ibzCore.AppMobHtmlViewLayoutProps());
exports.AppDefaultMobHtmlViewLayoutComponent = AppDefaultMobHtmlViewLayoutComponent;