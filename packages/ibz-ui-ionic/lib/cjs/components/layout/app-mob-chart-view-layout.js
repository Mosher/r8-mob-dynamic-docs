"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppDefaultMobChartViewLayoutComponent = exports.AppDefaultMobChartViewLayout = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _multiDataViewLayoutComponentBase = require("./multi-data-view-layout-component-base");

/**
 * 移动端图表视图视图布局面板
 *
 * @class AppDefaultMobChartViewLayout
 * @extends MultiDataViewLayoutComponentBase
 */
class AppDefaultMobChartViewLayout extends _multiDataViewLayoutComponentBase.MultiDataViewLayoutComponentBase {
  /**
   * @description 视图主容器内容
   * @return {*}  {*}
   * @memberof AppDefaultMobChartViewLayout
   */
  renderViewMainContainerContent() {
    return (0, _vue.createVNode)("div", {
      "class": 'view-main-container-content'
    }, [[(0, _vue.renderSlot)(this.ctx.slots, 'chart')]]);
  }

} //  移动端图表视图视图布局面板部件


exports.AppDefaultMobChartViewLayout = AppDefaultMobChartViewLayout;
const AppDefaultMobChartViewLayoutComponent = (0, _componentBase.GenerateComponent)(AppDefaultMobChartViewLayout, new _ibzCore.AppMobChartViewLayoutProps());
exports.AppDefaultMobChartViewLayoutComponent = AppDefaultMobChartViewLayoutComponent;