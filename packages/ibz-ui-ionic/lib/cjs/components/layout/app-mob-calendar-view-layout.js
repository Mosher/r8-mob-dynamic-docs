"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppDefaultMobCalendarViewLayoutComponent = exports.AppDefaultMobCalendarViewLayout = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _multiDataViewLayoutComponentBase = require("./multi-data-view-layout-component-base");

/**
 * 移动端日历视图视图布局面板（DEFAULT样式）
 *
 * @class AppDefaultMobCalendarViewLayout
 * @extends MultiDataViewLayoutComponentBase
 */
class AppDefaultMobCalendarViewLayout extends _multiDataViewLayoutComponentBase.MultiDataViewLayoutComponentBase {
  /**
   *
   * @description 视图主容器内容
   * @return {*}  {*}
   * @memberof AppDefaultMobCalendarViewLayout
   */
  renderViewMainContainerContent() {
    return (0, _vue.createVNode)("div", {
      "class": 'view-main-container-content'
    }, [[(0, _vue.renderSlot)(this.ctx.slots, 'calendar')]]);
  }

} //  移动端日历视图视图布局面板部件


exports.AppDefaultMobCalendarViewLayout = AppDefaultMobCalendarViewLayout;
const AppDefaultMobCalendarViewLayoutComponent = (0, _componentBase.GenerateComponent)(AppDefaultMobCalendarViewLayout, new _ibzCore.AppMobCalendarViewLayoutProps());
exports.AppDefaultMobCalendarViewLayoutComponent = AppDefaultMobCalendarViewLayoutComponent;