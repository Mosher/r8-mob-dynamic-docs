"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppDefaultMobEditViewLayoutComponent = exports.AppDefaultMobEditViewLayout = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _layoutComponentBase = require("./layout-component-base");

class AppDefaultMobEditViewLayout extends _layoutComponentBase.LayoutComponentBase {
  /**
   * @description 视图主容器内容
   * @return {*}
   * @memberof AppDefaultMobEditViewLayout
   */
  renderViewMainContainerContent() {
    return (0, _vue.createVNode)("div", {
      "class": 'view-main-container-content'
    }, [[this.renderSlot('form')]]);
  }

} // 移动端实体编辑视图组件


exports.AppDefaultMobEditViewLayout = AppDefaultMobEditViewLayout;
const AppDefaultMobEditViewLayoutComponent = (0, _componentBase.GenerateComponent)(AppDefaultMobEditViewLayout, new _ibzCore.AppMobEditViewLayoutProps());
exports.AppDefaultMobEditViewLayoutComponent = AppDefaultMobEditViewLayoutComponent;