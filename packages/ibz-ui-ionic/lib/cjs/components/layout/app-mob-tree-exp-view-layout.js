"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppDefaultMobTreeExpViewLayoutComponent = exports.AppDefaultMobTreeExpViewLayout = void 0;

var _vue = require("vue");

var _runtimeDom = require("@vue/runtime-dom");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _layoutComponentBase = require("./layout-component-base");

/**
 * @description 移动端树导航视图视图布局组件
 * @export
 * @class AppDefaultMobTreeExpViewLayout
 * @extends {AppDefaultDEExpView<AppMobTreeExpViewLayoutProps>}
 */
class AppDefaultMobTreeExpViewLayout extends _layoutComponentBase.LayoutComponentBase {
  /**
  * @description 视图主容器内容
  * @return {*}
  * @memberof AppDefaultMobHtmlViewLayout
  */
  renderViewMainContainerContent() {
    return (0, _vue.createVNode)("div", {
      "class": 'view-main-container-content'
    }, [(0, _runtimeDom.renderSlot)(this.ctx.slots, 'treeexpbar')]);
  }

} // 应用首页组件


exports.AppDefaultMobTreeExpViewLayout = AppDefaultMobTreeExpViewLayout;
const AppDefaultMobTreeExpViewLayoutComponent = (0, _componentBase.GenerateComponent)(AppDefaultMobTreeExpViewLayout, new _ibzCore.AppMobTreeExpViewLayoutProps());
exports.AppDefaultMobTreeExpViewLayoutComponent = AppDefaultMobTreeExpViewLayoutComponent;