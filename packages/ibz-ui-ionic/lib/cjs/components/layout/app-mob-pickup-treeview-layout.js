"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppDefaultMobPickupTreeViewLayoutComponent = exports.AppDefaultMobPickupTreeViewLayout = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _layoutComponentBase = require("./layout-component-base");

/**
 * 移动端选择树视图视图布局面板（DEFAULT样式）
 *
 * @class AppDefaultMobPickupTreeViewLayout
 * @extends LayoutComponentBase
 */
class AppDefaultMobPickupTreeViewLayout extends _layoutComponentBase.LayoutComponentBase {
  /**
   *
   * @description 视图主容器头部
   * @return {*}
   * @memberof AppDefaultMobPickupTreeViewLayout
   */
  renderViewMainContainerHeader() {
    return (0, _vue.createVNode)("div", {
      "class": 'view-main-container-header'
    }, [[(0, _vue.renderSlot)(this.ctx.slots, 'quickGroupSearch'), (0, _vue.renderSlot)(this.ctx.slots, 'quickSearch')]]);
  }
  /**
   * @description 视图主容器内容
   * @return {*}  {any[]}
   * @memberof AppDefaultMobPickupTreeViewLayout
   */


  renderViewMainContainerContent() {
    return (0, _vue.createVNode)("div", {
      "class": 'view-main-container-content'
    }, [[(0, _vue.renderSlot)(this.ctx.slots, 'tree')]]);
  }

} //  移动端选择多数据视图视图布局面板部件


exports.AppDefaultMobPickupTreeViewLayout = AppDefaultMobPickupTreeViewLayout;
const AppDefaultMobPickupTreeViewLayoutComponent = (0, _componentBase.GenerateComponent)(AppDefaultMobPickupTreeViewLayout, new _ibzCore.AppMobPickupTreeViewLayoutProps());
exports.AppDefaultMobPickupTreeViewLayoutComponent = AppDefaultMobPickupTreeViewLayoutComponent;