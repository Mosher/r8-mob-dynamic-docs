"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppDefaultMobMapExpViewLayoutComponent = exports.AppDefaultMobMapExpViewLayout = void 0;

var _vue = require("vue");

var _runtimeDom = require("@vue/runtime-dom");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _layoutComponentBase = require("./layout-component-base");

/**
 * @description 移动端地图导航视图视图布局组件
 * @export
 * @class AppDefaultMobMapExpViewLayout
 * @extends {AppDefaultDEExpView<AppMobMapExpViewLayoutProps>}
 */
class AppDefaultMobMapExpViewLayout extends _layoutComponentBase.LayoutComponentBase {
  /**
   * @description 视图主容器内容
   * @return {*}
   * @memberof AppDefaultMobHtmlViewLayout
   */
  renderViewMainContainerContent() {
    return (0, _vue.createVNode)("div", {
      "class": 'view-main-container-content'
    }, [(0, _runtimeDom.renderSlot)(this.ctx.slots, 'mapexpbar')]);
  }

} // 应用首页组件


exports.AppDefaultMobMapExpViewLayout = AppDefaultMobMapExpViewLayout;
const AppDefaultMobMapExpViewLayoutComponent = (0, _componentBase.GenerateComponent)(AppDefaultMobMapExpViewLayout, new _ibzCore.AppMobMapExpViewLayoutProps());
exports.AppDefaultMobMapExpViewLayoutComponent = AppDefaultMobMapExpViewLayoutComponent;