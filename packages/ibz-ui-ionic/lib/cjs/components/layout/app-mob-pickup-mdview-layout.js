"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppDefaultMobPickUpMDViewLayoutComponent = exports.AppDefaultMobPickUpMDViewLayout = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _layoutComponentBase = require("./layout-component-base");

/**
 * 移动端选择多数据视图视图布局面板（DEFAULT样式）
 *
 * @class AppDefaultMobPickUpMDViewLayout
 * @extends ComponentBase
 */
class AppDefaultMobPickUpMDViewLayout extends _layoutComponentBase.LayoutComponentBase {
  /**
   *
   * @description 视图主容器头部
   * @return {*}
   * @memberof AppDefaultMobPickupTreeViewLayout
   */
  renderViewMainContainerHeader() {
    return (0, _vue.createVNode)("div", {
      "class": 'view-main-container-header'
    }, [[(0, _vue.renderSlot)(this.ctx.slots, 'quickGroupSearch'), (0, _vue.renderSlot)(this.ctx.slots, 'quickSearch')]]);
  }
  /**
   * @description 视图主容器内容
   * @return {*}  {any}
   * @memberof AppDefaultMobPickUpMDViewLayout
   */


  renderViewMainContainerContent() {
    return (0, _vue.createVNode)("div", {
      "class": 'view-main-container-content'
    }, [[(0, _vue.renderSlot)(this.ctx.slots, 'mdctrl')]]);
  }

} //  移动端选择多数据视图视图布局面板部件


exports.AppDefaultMobPickUpMDViewLayout = AppDefaultMobPickUpMDViewLayout;
const AppDefaultMobPickUpMDViewLayoutComponent = (0, _componentBase.GenerateComponent)(AppDefaultMobPickUpMDViewLayout, new _ibzCore.AppMobPickUpMDViewLayoutProps());
exports.AppDefaultMobPickUpMDViewLayoutComponent = AppDefaultMobPickUpMDViewLayoutComponent;