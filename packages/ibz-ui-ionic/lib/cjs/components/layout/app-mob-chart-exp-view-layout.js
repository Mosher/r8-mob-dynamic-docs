"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppDefaultMobChartExpViewLayoutComponent = exports.AppDefaultMobChartExpViewLayout = void 0;

var _vue = require("vue");

var _runtimeDom = require("@vue/runtime-dom");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _layoutComponentBase = require("./layout-component-base");

/**
 * @description 移动端图表导航视图视图布局组件
 * @export
 * @class AppDefaultMobChartExpViewLayout
 * @extends {AppDefaultExpView<AppMobChartExpViewLayoutProps>}
 */
class AppDefaultMobChartExpViewLayout extends _layoutComponentBase.LayoutComponentBase {
  /**
   * @description 视图主容器内容
   * @return {*}
   * @memberof AppDefaultMobHtmlViewLayout
   */
  renderViewMainContainerContent() {
    return (0, _vue.createVNode)("div", {
      "class": 'view-main-container-content'
    }, [(0, _runtimeDom.renderSlot)(this.ctx.slots, 'chartexpbar')]);
  }

} // 图表导航栏组件


exports.AppDefaultMobChartExpViewLayout = AppDefaultMobChartExpViewLayout;
const AppDefaultMobChartExpViewLayoutComponent = (0, _componentBase.GenerateComponent)(AppDefaultMobChartExpViewLayout, new _ibzCore.AppMobChartExpViewLayoutProps());
exports.AppDefaultMobChartExpViewLayoutComponent = AppDefaultMobChartExpViewLayoutComponent;