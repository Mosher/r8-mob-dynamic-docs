"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MultiDataViewLayoutComponentBase = void 0;

var _vue = require("vue");

var _layoutComponentBase = require("./layout-component-base");

/**
 * 多数据视图布局基类
 *
 * @export
 * @class MultiDataViewLayoutComponentBase
 * @extends {LayoutComponentBase<AppMultiDataViewLayoutComponentProps>}
 * @template Props
 */
class MultiDataViewLayoutComponentBase extends _layoutComponentBase.LayoutComponentBase {
  /**
   *
   * @description 视图主容器头部
   * @return {*}
   * @memberof MultiDataViewLayoutComponentBase
   */
  renderViewMainContainerHeader() {
    return (0, _vue.createVNode)("div", {
      "class": 'view-main-container-header'
    }, [[(0, _vue.renderSlot)(this.ctx.slots, 'quickGroupSearch'), (0, _vue.renderSlot)(this.ctx.slots, 'quickSearch'), (0, _vue.renderSlot)(this.ctx.slots, 'searchform'), (0, _vue.renderSlot)(this.ctx.slots, 'quicksearchform')]]);
  }

}

exports.MultiDataViewLayoutComponentBase = MultiDataViewLayoutComponentBase;