"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppDefaultMobMEditViewLayoutComponent = exports.AppDefaultMobMEditViewLayout = void 0;

var _vue = require("vue");

var _runtimeDom = require("@vue/runtime-dom");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _layoutComponentBase = require("./layout-component-base");

class AppDefaultMobMEditViewLayout extends _layoutComponentBase.LayoutComponentBase {
  /**
   * @description 视图主容器内容
   * @return {*}  {any}
   * @memberof AppDefaultMobMEditViewLayout
   */
  renderViewMainContainerContent() {
    return (0, _vue.createVNode)("div", {
      "class": 'view-main-container-content'
    }, [[(0, _runtimeDom.renderSlot)(this.ctx.slots, 'meditviewpanel')]]);
  }

} // 移动端多表单编辑视图布局组件


exports.AppDefaultMobMEditViewLayout = AppDefaultMobMEditViewLayout;
const AppDefaultMobMEditViewLayoutComponent = (0, _componentBase.GenerateComponent)(AppDefaultMobMEditViewLayout, new _ibzCore.AppMobMEditViewLayoutProps());
exports.AppDefaultMobMEditViewLayoutComponent = AppDefaultMobMEditViewLayoutComponent;