"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppDefaultMobWFDynaActionViewLayoutComponent = exports.AppDefaultMobWFDynaActionViewLayout = void 0;

var _vue = require("vue");

var _runtimeDom = require("@vue/runtime-dom");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _layoutComponentBase = require("./layout-component-base");

/**
 * @description 移动端动态工作流操作视图视图布局组件
 * @export
 * @class AppDefaultMobWFDynaActionViewLayout
 * @extends {AppDefaultDEView<AppMobWFDynaActionViewLayoutProps>}
 */
class AppDefaultMobWFDynaActionViewLayout extends _layoutComponentBase.LayoutComponentBase {
  /**
   * @description 绘制组件
   * @return {*}
   * @memberof AppDefaultMobWFDynaActionViewLayout
   */
  renderViewMainContainerContent() {
    return (0, _vue.createVNode)("div", {
      "class": 'view-main-container-content'
    }, [[(0, _runtimeDom.renderSlot)(this.ctx.slots, 'form')]]);
  }
  /**
   * @description 绘制视图底部
   * @return {*}
   * @memberof AppDefaultMobWFDynaActionViewLayout
   */


  renderViewFooter() {
    return (0, _runtimeDom.renderSlot)(this.ctx.slots, 'viewFooter');
  }

} // 移动端动态工作流操作视图视图布局组件


exports.AppDefaultMobWFDynaActionViewLayout = AppDefaultMobWFDynaActionViewLayout;
const AppDefaultMobWFDynaActionViewLayoutComponent = (0, _componentBase.GenerateComponent)(AppDefaultMobWFDynaActionViewLayout, new _ibzCore.AppMobWFDynaActionViewLayoutProps());
exports.AppDefaultMobWFDynaActionViewLayoutComponent = AppDefaultMobWFDynaActionViewLayoutComponent;