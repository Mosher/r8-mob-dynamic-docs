"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppDefaultMobPickupViewLayoutComponent = exports.AppDefaultMobPickupViewLayout = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _layoutComponentBase = require("./layout-component-base");

function _isSlot(s) {
  return typeof s === 'function' || Object.prototype.toString.call(s) === '[object Object]' && !(0, _vue.isVNode)(s);
}

class AppDefaultMobPickupViewLayout extends _layoutComponentBase.LayoutComponentBase {
  /**
   * @description 渲染视图头
   * @return {*}  {*}
   * @memberof AppDefaultMobPickupViewLayout
   */
  renderViewHeader() {
    let _slot;

    return (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-header"), null, _isSlot(_slot = (0, _vue.renderSlot)(this.ctx.slots, 'headerButtons')) ? _slot : {
      default: () => [_slot]
    });
  }
  /**
   * @description 视图主容器内容
   * @return {*}  {any}
   * @memberof AppDefaultMobPickupViewLayout
   */


  renderViewMainContainerContent() {
    return (0, _vue.createVNode)("div", {
      "class": 'view-main-container-content'
    }, [[(0, _vue.renderSlot)(this.ctx.slots, 'pickupviewpanel')]]);
  }

} //  移动端数据选择视图布局面板


exports.AppDefaultMobPickupViewLayout = AppDefaultMobPickupViewLayout;
const AppDefaultMobPickupViewLayoutComponent = (0, _componentBase.GenerateComponent)(AppDefaultMobPickupViewLayout, new _ibzCore.AppMobPickUpViewLayoutProps());
exports.AppDefaultMobPickupViewLayoutComponent = AppDefaultMobPickupViewLayoutComponent;