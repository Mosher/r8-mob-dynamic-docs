"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppDefaultMobOptViewLayoutComponent = exports.AppDefaultMobOptViewLayout = void 0;

var _vue = require("vue");

var _runtimeDom = require("@vue/runtime-dom");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _layoutComponentBase = require("./layout-component-base");

/**
 * @description 移动端选项操作视图视图布局组件
 * @export
 * @class AppDefaultMobOptViewLayout
 * @extends {LayoutComponentBase<AppMobOptViewLayoutProps>}
 */
class AppDefaultMobOptViewLayout extends _layoutComponentBase.LayoutComponentBase {
  /**
   * @description 视图主容器内容
   * @return {*}
   * @memberof AppDefaultMobOptViewLayout
   */
  renderViewMainContainerContent() {
    return (0, _vue.createVNode)("div", {
      "class": 'view-main-container-content'
    }, [[(0, _runtimeDom.renderSlot)(this.ctx.slots, 'form')]]);
  }
  /**
   * @description 渲染视图底部
   * @return {*}  {*}
   * @memberof AppDefaultMobOptViewLayout
   */


  renderViewFooter() {
    return (0, _vue.createVNode)((0, _vue.resolveComponent)("ion-footer"), {
      "class": 'view-footer'
    }, {
      default: () => [(0, _runtimeDom.renderSlot)(this.ctx.slots, 'footerButtons'), this.ctx.slots.bottomMessage ? (0, _runtimeDom.renderSlot)(this.ctx.slots, 'bottomMessage') : null]
    });
  }

} // 应用首页组件


exports.AppDefaultMobOptViewLayout = AppDefaultMobOptViewLayout;
const AppDefaultMobOptViewLayoutComponent = (0, _componentBase.GenerateComponent)(AppDefaultMobOptViewLayout, new _ibzCore.AppMobOptViewLayoutProps());
exports.AppDefaultMobOptViewLayoutComponent = AppDefaultMobOptViewLayoutComponent;