"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppDefaultIndexViewLayoutComponent = exports.AppDefaultIndexViewLayout = void 0;

var _vue = require("vue");

var _runtimeDom = require("@vue/runtime-dom");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _layoutComponentBase = require("./layout-component-base");

/**
 * 应用首页视图布局面板
 *
 * @class AppDefaultIndexViewLayout
 */
class AppDefaultIndexViewLayout extends _layoutComponentBase.LayoutComponentBase {
  /**
   * @description 渲染视图头
   * @return {*}
   * @memberof AppDefaultIndexViewLayout
   */
  renderViewHeader() {
    return null;
  }
  /**
   * @description 渲染视图底部
   * @return {*}
   * @memberof AppDefaultIndexViewLayout
   */


  renderViewFooter() {
    return null;
  }
  /**
   * @description 视图主容器内容
   * @return {*}  {any[]}
   * @memberof AppDefaultIndexViewLayout
   */


  renderViewMainContainerContent() {
    return (0, _vue.createVNode)("div", {
      "class": 'view-main-container-content'
    }, [[(0, _runtimeDom.renderSlot)(this.ctx.slots, 'appmenu')]]);
  }

} // 应用首页组件


exports.AppDefaultIndexViewLayout = AppDefaultIndexViewLayout;
const AppDefaultIndexViewLayoutComponent = (0, _componentBase.GenerateComponent)(AppDefaultIndexViewLayout, new _ibzCore.AppIndexViewLayoutProps());
exports.AppDefaultIndexViewLayoutComponent = AppDefaultIndexViewLayoutComponent;