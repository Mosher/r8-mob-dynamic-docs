"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppDefaultMobTreeViewLayoutComponent = exports.AppDefaultMobTreeViewLayout = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _multiDataViewLayoutComponentBase = require("./multi-data-view-layout-component-base");

/**
 * 移动端树视图视图布局面板
 *
 * @class AppMobTreeViewLayout
 * @extends LayoutComponentBase
 */
class AppDefaultMobTreeViewLayout extends _multiDataViewLayoutComponentBase.MultiDataViewLayoutComponentBase {
  /**
   * @description 渲染部件
   * @return {*}  {any}
   * @memberof AppDefaultMobTreeViewLayout
   */
  renderViewMainContainerContent() {
    return (0, _vue.createVNode)("div", {
      "class": 'view-main-container-content'
    }, [[(0, _vue.renderSlot)(this.ctx.slots, 'tree')]]);
  }

} //  移动端树视图视图布局面板组件


exports.AppDefaultMobTreeViewLayout = AppDefaultMobTreeViewLayout;
const AppDefaultMobTreeViewLayoutComponent = (0, _componentBase.GenerateComponent)(AppDefaultMobTreeViewLayout, new _ibzCore.AppMobTreeViewLayoutProps());
exports.AppDefaultMobTreeViewLayoutComponent = AppDefaultMobTreeViewLayoutComponent;