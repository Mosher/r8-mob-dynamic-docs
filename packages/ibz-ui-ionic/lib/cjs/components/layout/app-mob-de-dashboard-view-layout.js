"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AppDefaultMobDEDashboardViewLayoutComponent = exports.AppDefaultMobDEDashboardViewLayout = void 0;

var _vue = require("vue");

var _ibzCore = require("ibz-core");

var _componentBase = require("../component-base");

var _layoutComponentBase = require("./layout-component-base");

/**
 * 移动端实体数据看板视图视图布局面板
 *
 * @exports
 * @class AppDefaultMobDEDashboardViewLayout
 * @extends LayoutComponentBase
 */
class AppDefaultMobDEDashboardViewLayout extends _layoutComponentBase.LayoutComponentBase {
  /**
   *
   * @description 视图主容器头部
   * @return {*}
   * @memberof AppMobDEDashboardViewLayout
   */
  renderViewMainContainerHeader() {
    return (0, _vue.createVNode)("div", {
      "class": 'view-main-container-header'
    }, [[(0, _vue.renderSlot)(this.ctx.slots, 'searchform'), (0, _vue.renderSlot)(this.ctx.slots, 'quicksearchform')]]);
  }
  /**
   * @description 视图主容器内容
   * @return {*}
   * @memberof AppMobDEDashboardViewLayout
   */


  renderViewMainContainerContent() {
    return (0, _vue.createVNode)("div", {
      "class": 'view-main-container-content'
    }, [[(0, _vue.renderSlot)(this.ctx.slots, 'dashboard')]]);
  }

} //  移动端实体数据看板视图视图布局面板组件


exports.AppDefaultMobDEDashboardViewLayout = AppDefaultMobDEDashboardViewLayout;
const AppDefaultMobDEDashboardViewLayoutComponent = (0, _componentBase.GenerateComponent)(AppDefaultMobDEDashboardViewLayout, new _ibzCore.AppMobDEDashboardViewLayoutProps());
exports.AppDefaultMobDEDashboardViewLayoutComponent = AppDefaultMobDEDashboardViewLayoutComponent;