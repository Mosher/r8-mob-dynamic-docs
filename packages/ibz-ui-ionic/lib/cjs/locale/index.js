"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "en_US", {
  enumerable: true,
  get: function () {
    return _en_US.en_US;
  }
});
Object.defineProperty(exports, "zh_CN", {
  enumerable: true,
  get: function () {
    return _zh_CN.zh_CN;
  }
});
exports.handleLocaleMap = exports.translate = void 0;

var _en_US = require("./lang/en_US");

var _zh_CN = require("./lang/zh_CN");

// 英文语言资源
// 中文语言资源
// 多语言翻译
const translate = (key, context, value) => {
  if (key) {
    if (context.$te(key)) {
      return context.$t(key);
    } else {
      if (context.modelService) {
        const lanResource = context.modelService.getPSLang(key);
        return lanResource ? lanResource : value ? value : key;
      } else {
        return value ? value : key;
      }
    }
  } else {
    return value;
  }
}; // 处理语言路径映射


exports.translate = translate;

const handleLocaleMap = key => {
  switch (key) {
    case 'zh-CN':
      return 'ZH_CN';

    case 'en-US':
      return 'EN';

    default:
      return 'ZH_CN';
  }
};

exports.handleLocaleMap = handleLocaleMap;