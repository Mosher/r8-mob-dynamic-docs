"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

// 部件组件国际化（中文）
function getLocaleResource() {
  const widget_zh_CN = {
    mobformdruipart: {
      tooltip: '请先保存主数据'
    },
    mobformgroup: {
      hidden: '隐藏',
      more: '显示更多'
    },
    mobdashboard: {
      title: '定制仪表盘'
    },
    mobmeditviewpanel: {
      noembeddedview: '无嵌入视图'
    },
    mobpanel: {
      nomarkdown: 'MARKDOWN直接内容暂未支持'
    },
    mobpickupviewpanel: {
      nopickerview: '无选择视图'
    },
    mobportlet: {
      nomarkdown: 'MARKDOWN直接内容暂未支持',
      toolbar: '工具栏',
      rendercustom: '绘制自定义'
    },
    mobsearchform: {
      detailtype: '类型表单成员',
      filter: '过滤',
      search: '搜索',
      reset: '重置'
    },
    mobtabviewpanel: {
      noembeddedview: '嵌入视图不存在'
    },
    mobtoolbar: {
      exportmaxrow: '导出最大行',
      exportcurpage: '导出当前页',
      notsupport: '工具栏样式暂未支持！！！'
    }
  };
  return widget_zh_CN;
}

var _default = getLocaleResource;
exports.default = _default;