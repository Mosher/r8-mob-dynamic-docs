"use strict";

var _interopRequireDefault = require("D:/IbizWork/r8-mob-dynamic-docs/packages/ibz-ui-ionic/node_modules/@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.zh_CN = void 0;

var _common_zh_CN = _interopRequireDefault(require("../components/common/common_zh_CN"));

var _view_zh_CN = _interopRequireDefault(require("../components/view/view_zh_CN"));

var _widget_zh_CN = _interopRequireDefault(require("../components/widget/widget_zh_CN"));

// 组件国际化（中文）
const zh_CN = () => {
  const app_zh_CN = {
    // 共享
    share: {
      ok: '确认',
      cancel: '取消',
      notsupported: '暂未支持',
      widget: '部件',
      previous: '上一步',
      next: '下一步',
      finish: '完成',
      emptytext: '暂无数据',
      year: '年',
      month: '月',
      day: '日',
      loading: '加载中···'
    },
    // 通用组件
    common: (0, _common_zh_CN.default)(),
    // 视图组件
    view: (0, _view_zh_CN.default)(),
    // 部件组件
    widget: (0, _widget_zh_CN.default)()
  };
  return app_zh_CN;
};

exports.zh_CN = zh_CN;