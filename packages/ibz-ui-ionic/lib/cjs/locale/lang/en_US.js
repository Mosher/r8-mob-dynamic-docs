"use strict";

var _interopRequireDefault = require("D:/IbizWork/r8-mob-dynamic-docs/packages/ibz-ui-ionic/node_modules/@babel/runtime/helpers/interopRequireDefault").default;

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.en_US = void 0;

var _common_en_US = _interopRequireDefault(require("../components/common/common_en_US"));

var _view_en_US = _interopRequireDefault(require("../components/view/view_en_US"));

var _widget_en_US = _interopRequireDefault(require("../components/widget/widget_en_US"));

// 组件国际化（英文）
const en_US = () => {
  const app_en_US = {
    // 共享
    share: {
      ok: 'Ok',
      cancel: 'Cancel',
      notsupported: 'Temporary does not support',
      widget: 'Widget',
      previous: 'Previous',
      next: 'Next',
      finish: 'Finish',
      emptytext: 'Temporarily no data',
      year: 'Year',
      month: 'Month',
      day: 'Day',
      loading: 'Loading···'
    },
    // 通用组件
    common: (0, _common_en_US.default)(),
    // 视图组件
    view: (0, _view_en_US.default)(),
    // 部件组件
    widget: (0, _widget_en_US.default)()
  };
  return app_en_US;
};

exports.en_US = en_US;