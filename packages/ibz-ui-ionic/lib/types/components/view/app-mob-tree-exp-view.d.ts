import { AppMobTreeExpViewProps, IMobTreeExpViewController } from 'ibz-core';
import { DEExpViewComponentBase } from './de-exp-view-component-base';
/**
 * @description 移动端树导航视图
 * @export
 * @class AppMobTreeExpView
 * @extends {DEViewComponentBase<AppMobTreeExpViewProps>}
 */
export declare class AppMobTreeExpView extends DEExpViewComponentBase<AppMobTreeExpViewProps> {
    /**
      * 视图控制器
      *
      * @protected
      * @memberof AppMobTreeExpView
      */
    protected c: IMobTreeExpViewController;
    /**
      * 设置响应式
      *
      * @public
      * @memberof AppMobTreeExpView
      */
    setup(): void;
}
export declare const AppMobTreeExpViewComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
