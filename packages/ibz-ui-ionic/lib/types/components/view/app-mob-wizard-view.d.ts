import { AppMobWizardViewProps, IMobWizardViewController } from 'ibz-core';
import { DEViewComponentBase } from './de-view-component-base';
/**
 * @description 移动端向导视图
 * @export
 * @class AppMobWizardView
 * @extends {DEViewComponentBase<AppMobWizardViewProps>}
 */
export declare class AppMobWizardView extends DEViewComponentBase<AppMobWizardViewProps> {
    /**
      * 视图控制器
      *
      * @protected
      * @memberof AppMobWizardView
      */
    protected c: IMobWizardViewController;
    /**
      * 设置响应式
      *
      * @public
      * @memberof AppMobWizardView
      */
    setup(): void;
}
export declare const AppMobWizardViewComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
