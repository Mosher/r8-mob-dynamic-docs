import { AppMobTabExpViewProps, IMobTabExpViewController } from 'ibz-core';
import { DEViewComponentBase } from './de-view-component-base';
/**
 * 移动端分页导航视图
 *
 * @class AppMobTabExpView
 * @extends DEViewComponentBase
 */
export declare class AppMobTabExpView extends DEViewComponentBase<AppMobTabExpViewProps> {
    /**
     * 视图控制器
     *
     * @protected
     * @memberof AppMobTabExpView
     */
    protected c: IMobTabExpViewController;
    /**
     * 设置响应式
     *
     * @public
     * @memberof AppMobTabExpView
     */
    setup(): void;
    /**
     * @description 渲染信息栏
     * @return {*}
     * @memberof AppMobTabExpView
     */
    renderDataInfoBar(): JSX.Element | undefined;
    /**
     * @description 渲染视图组件
     * @return {*}
     * @memberof AppMobTabExpView
     */
    renderViewControls(): any;
}
export declare const AppMobTabExpViewComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
