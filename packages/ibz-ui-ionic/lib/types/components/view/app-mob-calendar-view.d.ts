import { AppMobCalendarViewProps, IMobCalendarViewController } from 'ibz-core';
import { DEMultiDataViewComponentBase } from './de-multi-data-view-component-base';
/**
 * 移动端多数据视图
 *
 * @class AppMobCalendarView
 * @extends DEMultiDataViewComponentBase
 */
export declare class AppMobCalendarView extends DEMultiDataViewComponentBase<AppMobCalendarViewProps> {
    /**
     * 视图控制器
     *
     * @protected
     * @memberof AppMobCalendarView
     */
    protected c: IMobCalendarViewController;
    /**
     * 设置响应式
     *
     * @public
     * @memberof AppMobCalendarView
     */
    setup(): void;
}
export declare const AppMobCalendarViewComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
