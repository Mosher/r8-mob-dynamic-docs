import { AppMobDEDashboardViewProps } from 'ibz-core';
import { IMobDEDashboardViewController } from 'ibz-core';
import { ViewComponentBase } from './view-component-base';
/**
 * 移动端实体数据看板视图
 *
 * @class AppMobDEDashboardView
 * @extends ViewComponentBase
 */
export declare class AppMobDEDashboardView extends ViewComponentBase<AppMobDEDashboardViewProps> {
    /**
     * 移动端实体数据看板视图控制器实例对象
     *
     * @type {IMobDEDashboardViewController}
     * @memberof AppMobDEDashboardView
     */
    protected c: IMobDEDashboardViewController;
    /**
     * 设置响应式
     *
     * @memberof AppMobDEDashboardView
     */
    setup(): void;
    /**
     * @description 渲染信息栏
     * @return {*}
     * @memberof AppMobDEDashboardView
     */
    renderDataInfoBar(): JSX.Element | undefined;
    /**
     * @description 渲染视图组件
     * @return {*}
     * @memberof AppMobDEDashboardView
     */
    renderViewControls(): any;
}
export declare const AppMobDEDashboardViewComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
