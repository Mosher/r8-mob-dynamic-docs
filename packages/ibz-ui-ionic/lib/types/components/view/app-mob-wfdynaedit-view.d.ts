import { AppMobWFDynaEditViewProps, IMobWFDynaEditViewController } from 'ibz-core';
import { DEViewComponentBase } from './de-view-component-base';
/**
 * @description 移动端动态工作流编辑视图
 * @export
 * @class AppMobWFDynaEditView
 * @extends {DEViewComponentBase<AppMobWFDynaEditViewProps>}
 */
export declare class AppMobWFDynaEditView extends DEViewComponentBase<AppMobWFDynaEditViewProps> {
    /**
      * 视图控制器
      *
      * @protected
      * @memberof AppMobWFDynaEditView
      */
    protected c: IMobWFDynaEditViewController;
    /**
      * 设置响应式
      *
      * @public
      * @memberof AppMobWFDynaEditView
      */
    setup(): void;
    /**
     * @description 工具栏按钮点击
     * @param {*} linkItem 工具栏项
     * @param {*} event 源事件对象
     * @memberof AppMobWFDynaEditView
     */
    toolbarClick(linkItem: any, event: any): void;
    /**
     * @description 渲染工具栏
     * @return {*}
     * @memberof AppMobWFDynaEditView
     */
    renderToolbar(): JSX.Element;
    /**
     * @description 渲染表单
     * @return {*}
     * @memberof AppMobWFDynaEditView
     */
    renderForm(): import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }> | null;
    /**
     * @description 渲染视图部件
     * @return {*}
     * @memberof AppMobWFDynaEditView
     */
    renderViewControls(): any;
}
export declare const AppMobWFDynaEditViewComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
