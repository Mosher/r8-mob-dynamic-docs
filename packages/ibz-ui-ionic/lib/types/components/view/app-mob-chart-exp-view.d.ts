import { AppMobChartExpViewProps, IMobChartExpViewController } from 'ibz-core';
import { DEExpViewComponentBase } from './de-exp-view-component-base';
/**
 * @description 移动端图表导航视图
 * @export
 * @class AppMobChartExpView
 * @extends {DEViewComponentBase<AppMobChartExpViewProps>}
 */
export declare class AppMobChartExpView extends DEExpViewComponentBase<AppMobChartExpViewProps> {
    /**
     * 视图控制器
     *
     * @protected
     * @memberof AppMobChartExpView
     */
    protected c: IMobChartExpViewController;
    /**
     * 设置响应式
     *
     * @public
     * @memberof AppMobChartExpView
     */
    setup(): void;
}
export declare const AppMobChartExpViewComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
