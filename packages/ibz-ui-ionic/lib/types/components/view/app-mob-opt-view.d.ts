import { AppMobOptViewProps, IMobOptViewController } from 'ibz-core';
import { DEViewComponentBase } from './de-view-component-base';
/**
 * @description 移动端选项操作视图
 * @export
 * @class AppMobOptView
 * @extends {DEViewComponentBase<AppMobOptViewProps>}
 */
export declare class AppMobOptView extends DEViewComponentBase<AppMobOptViewProps> {
    /**
      * 视图控制器
      *
      * @protected
      * @memberof AppMobOptView
      */
    protected c: IMobOptViewController;
    /**
      * 设置响应式
      *
      * @public
      * @memberof AppMobOptView
      */
    setup(): void;
    /**
     * @description 绘制底部按钮
     * @return {*}
     * @memberof AppMobOptView
     */
    renderFooterButtons(): JSX.Element;
    /**
     * @description 绘制视图所有部件
     * @return {*}
     * @memberof AppMobMPickUpView
     */
    renderViewControls(): any;
}
export declare const AppMobOptViewComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
