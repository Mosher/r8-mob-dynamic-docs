import { AppViewProps } from 'ibz-core';
import { ViewComponentBase } from './view-component-base';
export declare class AppMobNotSupportedView extends ViewComponentBase<AppViewProps> {
    /**
     * 设置响应式
     *
     * @public
     * @memberof AppIndexView
     */
    setup(): void;
    /**
     * 视图渲染
     *
     * @memberof AppDefaultNotSupportedView
     */
    render(): JSX.Element | null;
}
export declare const AppMobNotSupportedViewComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
