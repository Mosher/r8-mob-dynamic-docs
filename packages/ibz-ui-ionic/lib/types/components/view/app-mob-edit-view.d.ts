import { AppMobEditViewProps, IMobEditViewController } from 'ibz-core';
import { DEViewComponentBase } from './de-view-component-base';
export declare class AppMobEditView extends DEViewComponentBase<AppMobEditViewProps> {
    /**
     * 视图控制器
     *
     * @protected
     * @memberof AppMobEditView
     */
    protected c: IMobEditViewController;
    /**
     * 设置响应式
     *
     * @public
     * @memberof AppMobEditView
     */
    setup(): void;
    /**
     * @description 渲染信息栏
     * @return {*}
     * @memberof AppMobEditView
     */
    renderDataInfoBar(): JSX.Element | undefined;
    /**
     * @description 渲染视图组件
     * @return {*}
     * @memberof AppMobEditView
     */
    renderViewControls(): any;
}
export declare const AppMobEditViewComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
