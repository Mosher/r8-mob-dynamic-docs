import { AppMobTreeViewProps, IMobTreeViewController } from 'ibz-core';
import { DEMultiDataViewComponentBase } from './de-multi-data-view-component-base';
/**
 * 移动端树视图
 *
 * @class AppMobTreeView
 * @extends DEMultiDataViewComponentBase
 */
export declare class AppMobTreeView extends DEMultiDataViewComponentBase<AppMobTreeViewProps> {
    /**
     * @description 树视图控制器
     * @protected
     * @type {IMobTreeViewController}
     * @memberof AppMobTreeView
     */
    protected c: IMobTreeViewController;
    /**
     * @description 设置响应式
     * @memberof AppMobTreeView
     */
    setup(): void;
}
export declare const AppMobTreeViewComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
