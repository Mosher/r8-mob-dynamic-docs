import { IPSControl } from '@ibiz/dynamic-model-api';
import { AppDEMultiDataViewProps, IAppDEMultiDataViewController } from 'ibz-core';
import { DEViewComponentBase } from './de-view-component-base';
/**
 * @description 多数据视图父类
 * @export
 * @class DEMultiDataViewComponentBase
 * @extends {DEViewComponentBase<AppDEMultiDataViewProps>}
 * @template Props
 */
export declare class DEMultiDataViewComponentBase<Props extends AppDEMultiDataViewProps> extends DEViewComponentBase<AppDEMultiDataViewProps> {
    /**
     * @description 视图控制器
     * @protected
     * @type {IAppDEMultiDataViewController}
     * @memberof DEMultiDataViewComponentBase
     */
    protected c: IAppDEMultiDataViewController;
    /**
     * @description 快速搜索值变化
     * @param {*} 快速分组抛出值
     * @memberof DEMultiDataViewComponentBase
     */
    quickGroupValueChange(data: any): void;
    /**
     * @description 快速搜索
     * @param {*} query 快速搜索值
     * @memberof DEMultiDataViewComponentBase
     */
    quickSearch(query: string): void;
    /**
     * @description 渲染快速分组
     * @return {*}
     * @memberof DEMultiDataViewComponentBase
     */
    renderQuickGroup(): JSX.Element | undefined;
    /**
     * @description 绘制快速搜索
     * @return {*}
     * @memberof DEMultiDataViewComponentBase
     */
    renderQuickSearch(): any;
    /**
     * @description 渲染视图部件
     * @return {*}
     * @memberof DEMultiDataViewComponentBase
     */
    renderViewControls(): any;
    /**
     * @description 额外部件参数
     * @param {*} ctrlProps 部件参数
     * @param {IPSControl} controlInstance 部件实例
     * @memberof DEMultiDataViewComponentBase
     */
    extraCtrlParam(ctrlProps: any, controlInstance: IPSControl): void;
}
