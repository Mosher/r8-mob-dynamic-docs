import { AppMobMEditViewProps, IMobMEditViewController } from 'ibz-core';
import { DEMultiDataViewComponentBase } from './de-multi-data-view-component-base';
/**
 * 移动端多表单编辑视图
 *
 * @class AppMobMEditView
 * @extends ViewComponentBase
 */
export declare class AppMobMEditView extends DEMultiDataViewComponentBase<AppMobMEditViewProps> {
    /**
     * @description 多表单编辑视图控制器
     * @protected
     * @type {IMobMEditViewController}
     * @memberof AppMobMEditView
     */
    protected c: IMobMEditViewController;
    /**
     * @description 设置响应式
     * @memberof AppMobMEditView
     */
    setup(): void;
}
export declare const AppMobMEditViewComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
