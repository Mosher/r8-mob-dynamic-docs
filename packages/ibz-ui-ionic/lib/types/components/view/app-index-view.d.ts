import { AppIndexViewProps, IMobIndexViewController } from 'ibz-core';
import { ViewComponentBase } from './view-component-base';
/**
 * 应用首页视图交联组件
 *
 * @export
 * @class AppIndexView
 */
export declare class AppIndexView extends ViewComponentBase<AppIndexViewProps> {
    /**
     * @description 应用首页控制器
     * @protected
     * @type {IMobIndexViewController}
     * @memberof AppIndexView
     */
    protected c: IMobIndexViewController;
    /**
     * @description 设置响应式
     * @memberof AppIndexView
     */
    setup(): void;
}
export declare const AppIndexViewComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
