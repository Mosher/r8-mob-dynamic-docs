import { AppMobCustomViewProps, IMobCustomViewController } from 'ibz-core';
import { DEViewComponentBase } from './de-view-component-base';
/**
 * 移动端自定义视图
 *
 * @class AppMobCustomView
 * @extends ViewComponentBase
 */
export declare class AppMobCustomView extends DEViewComponentBase<AppMobCustomViewProps> {
    /**
     * 视图控制器
     *
     * @protected
     * @memberof AppIndexView
     */
    protected c: IMobCustomViewController;
    /**
     * 设置响应式
     *
     * @public
     * @memberof AppMobCustomView
     */
    setup(): void;
}
export declare const AppMobCustomViewComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
