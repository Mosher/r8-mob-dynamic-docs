import { AppMobWFDynaActionViewProps, IMobWFDynaActionViewController } from 'ibz-core';
import { DEViewComponentBase } from './de-view-component-base';
/**
 * @description 移动端动态工作流操作视图
 * @export
 * @class AppMobWFDynaActionView
 * @extends {DEViewComponentBase<AppMobWFDynaActionViewProps>}
 */
export declare class AppMobWFDynaActionView extends DEViewComponentBase<AppMobWFDynaActionViewProps> {
    /**
      * 视图控制器
      *
      * @protected
      * @memberof AppMobWFDynaActionView
      */
    protected c: IMobWFDynaActionViewController;
    /**
      * 设置响应式
      *
      * @public
      * @memberof AppMobWFDynaActionView
      */
    setup(): void;
    /**
     * @description 提交
     * @param {*} event
     * @memberof AppMobWFDynaActionView
     */
    submit(event: any): void;
    /**
     * @description 取消
     * @param {*} event
     * @memberof AppMobWFDynaActionView
     */
    cancel(event: any): void;
    /**
     * @description 渲染视图底部
     * @return {*}
     * @memberof AppMobWFDynaActionView
     */
    renderViewFooter(): JSX.Element;
    /**
     * @description 渲染表单
     * @return {*}
     * @memberof AppMobWFDynaActionView
     */
    renderForm(): import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }> | null;
    /**
     * @description 渲染视图组件
     * @return {*}
     * @memberof AppMobWFDynaActionView
     */
    renderViewControls(): any;
}
export declare const AppMobWFDynaActionViewComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
