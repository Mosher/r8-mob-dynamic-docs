import { AppMobMapExpViewProps, IMobMapExpViewController } from 'ibz-core';
import { DEExpViewComponentBase } from './de-exp-view-component-base';
/**
 * @description 移动端地图导航视图
 * @export
 * @class AppMobMapExpView
 * @extends {DEViewComponentBase<AppMobMapExpViewProps>}
 */
export declare class AppMobMapExpView extends DEExpViewComponentBase<AppMobMapExpViewProps> {
    /**
      * 视图控制器
      *
      * @protected
      * @memberof AppMobMapExpView
      */
    protected c: IMobMapExpViewController;
    /**
      * 设置响应式
      *
      * @public
      * @memberof AppMobMapExpView
      */
    setup(): void;
}
export declare const AppMobMapExpViewComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
