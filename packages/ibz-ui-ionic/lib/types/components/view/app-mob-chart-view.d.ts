import { AppMobChartViewProps, IMobChartViewController } from 'ibz-core';
import { DEMultiDataViewComponentBase } from './de-multi-data-view-component-base';
/**
 * 移动端图表视图
 *
 * @class AppMobChartView
 * @extends DEMultiDataViewComponentBase
 */
export declare class AppMobChartView extends DEMultiDataViewComponentBase<AppMobChartViewProps> {
    /**
     * @description 图表视图控制器
     * @protected
     * @type {IMobChartViewController}
     * @memberof AppMobChartView
     */
    protected c: IMobChartViewController;
    /**
     * @description 设置响应式
     * @memberof AppMobChartView
     */
    setup(): void;
}
export declare const AppMobChartViewComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
