import { AppMobMapViewProps, IMobMapViewController } from 'ibz-core';
import { DEMultiDataViewComponentBase } from './de-multi-data-view-component-base';
export declare class AppMobMapView extends DEMultiDataViewComponentBase<AppMobMapViewProps> {
    /**
     * 视图控制器
     *
     * @protected
     * @memberof AppMobMapView
     */
    protected c: IMobMapViewController;
    /**
     * 设置响应式
     *
     * @public
     * @memberof AppMobMapView
     */
    setup(): void;
}
export declare const AppMobMapViewComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
