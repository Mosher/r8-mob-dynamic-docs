import { IPSControl } from '@ibiz/dynamic-model-api';
import { IAppViewControllerBase, IViewEventResult, MobChartExpViewController, MobMapExpViewController, MobMapViewController, MobTabExpViewController, MobTreeExpViewController } from 'ibz-core';
import { AppViewProps, MobIndexViewController, MobCustomViewController, MobEditViewController, MobMDViewController, MobPanelViewController, MobPickUpViewController, MobCalendarViewController, MobTreeViewController, MobHtmlViewController, MobMEditViewController, MobChartViewController, MobPortalViewController, MobDEDashboardViewController, MobWFDynaEditViewController, MobWFDynaActionViewController, MobListExpViewController, MobOptViewController, MobWizardViewController } from 'ibz-core';
import { ComponentBase } from '../component-base';
/**
 * 视图组件基类
 *
 * @export
 * @class ViewComponentBase
 */
export declare class ViewComponentBase<Props extends AppViewProps> extends ComponentBase<Props> {
    /**
     * @description 视图是否加载完成
     * @protected
     * @type {*}
     * @memberof ViewComponentBase
     */
    protected viewIsLoaded: any;
    /**
     * @description 视图控制器
     * @protected
     * @type {IAppViewControllerBase}
     * @memberof ViewComponentBase
     */
    protected c: IAppViewControllerBase;
    /**
     * @description 设置响应式
     * @memberof ViewComponentBase
     */
    setup(): void;
    /**
     * @description 初始化响应式属性
     * @memberof ViewComponentBase
     */
    initReactive(): void;
    /**
     * @description 视图初始化
     * @memberof ViewComponentBase
     */
    init(): void;
    /**
     * @description 视图销毁
     * @memberof ViewComponentBase
     */
    unmounted(): void;
    /**
     * @description 执行视图事件
     * @param {IViewEventResult} args 事件参数
     * @memberof ViewComponentBase
     */
    emitViewEvent(args: IViewEventResult): void;
    /**
     * @description 视图关闭
     * @param {*} [args] 事件参数
     * @memberof ViewComponentBase
     */
    viewClose(args?: any): void;
    /**
     * @description 视图挂载完成
     * @param {*} [args] 事件参数
     * @memberof ViewComponentBase
     */
    viewMounted(args?: any): void;
    /**
     * @description 根据视图类型获取视图控制器
     * @param {string} type 视图类型
     * @return {*}
     * @memberof ViewComponentBase
     */
    getViewControllerByType(type: string): MobIndexViewController | MobCustomViewController | MobEditViewController | MobPanelViewController | MobMDViewController | MobPickUpViewController | MobCalendarViewController | MobTreeViewController | MobChartViewController | MobMapViewController | MobTabExpViewController | MobHtmlViewController | MobMEditViewController | MobPortalViewController | MobDEDashboardViewController | MobWFDynaEditViewController | MobWFDynaActionViewController | MobListExpViewController | MobOptViewController | MobWizardViewController | MobChartExpViewController | MobTreeExpViewController | MobMapExpViewController | null;
    /**
     * @description 初始化视图导航参数
     * @param {*} [inputvalue=null] 输入值
     * @return {*}
     * @memberof ViewComponentBase
     */
    initViewNavParam(inputvalue?: any): void;
    /**
     * @description 绘制视图所有部件
     * @return {*}  {*}
     * @memberof ViewComponentBase
     */
    renderViewControls(): any;
    /**
     * @description 计算目标部件所需参数
     * @param {IPSControl} controlInstance 部件模型实例
     * @return {*}
     * @memberof ViewComponentBase
     */
    computeTargetCtrlData(controlInstance: IPSControl): import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }>;
    /**
     * @description 额外部件参数
     * @param {*} ctrlProps 部件参数
     * @param {IPSControl} controlInstance 部件模型实例
     * @memberof ViewComponentBase
     */
    extraCtrlParam(ctrlProps: any, controlInstance: IPSControl): void;
    /**
     * @description 绘制视图顶部视图消息
     * @return {*}  {*}
     * @memberof ViewComponentBase
     */
    renderTopMessage(): any;
    /**
     * @description 绘制视图内容区视图消息
     * @return {*}  {*}
     * @memberof ViewComponentBase
     */
    renderBodyMessage(): any;
    /**
     * @description 绘制视图下方视图消息
     * @return {*}  {*}
     * @memberof ViewComponentBase
     */
    renderBottomMessage(): any;
    /**
     * @description 绘制视图
     * @return {*}
     * @memberof ViewComponentBase
     */
    render(): import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }> | null;
}
