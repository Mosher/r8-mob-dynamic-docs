import { AppMobPickUpMDViewProps, IMobPickUpMDViewController } from 'ibz-core';
import { DEMultiDataViewComponentBase } from './de-multi-data-view-component-base';
import { IPSControl } from '@ibiz/dynamic-model-api';
/**
 * 移动端多数据视图
 *
 * @class AppMobMDView
 * @extends ViewComponentBase
 */
export declare class AppMobPickUpMDView extends DEMultiDataViewComponentBase<AppMobPickUpMDViewProps> {
    /**
     * @description 选择数据视图控制器
     * @protected
     * @type {IMobPickUpMDViewController}
     * @memberof AppMobPickUpMDView
     */
    protected c: IMobPickUpMDViewController;
    /**
     * @description 设置响应式
     * @memberof AppMobPickUpMDView
     */
    setup(): void;
    /**
     * 额外部件参数
     *
     * @param ctrlProps 部件参数
     * @param controlInstance 部件
     */
    extraCtrlParam(ctrlProps: any, controlInstance: IPSControl): void;
}
export declare const AppMobPickUpMDViewComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
