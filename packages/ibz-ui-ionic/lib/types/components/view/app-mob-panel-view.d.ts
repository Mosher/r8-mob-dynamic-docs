import { IPSControl } from '@ibiz/dynamic-model-api';
import { AppMobPanelViewProps, IMobPanelViewController } from 'ibz-core';
import { DEViewComponentBase } from './de-view-component-base';
export declare class AppMobPanelView extends DEViewComponentBase<AppMobPanelViewProps> {
    /**
     * 视图控制器
     *
     * @protected
     * @memberof AppMobPanelView
     */
    protected c: IMobPanelViewController;
    /**
     * 设置响应式
     *
     * @public
     * @memberof AppMobMDView
     */
    setup(): void;
    /**
     * 额外部件参数
     *
     * @param ctrlProps 部件参数
     * @param controlInstance 部件
     */
    extraCtrlParam(ctrlProps: any, controlInstance: IPSControl): void;
}
export declare const AppMobPanelViewComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
