import { AppMobHtmlViewProps, IMobHtmlViewController } from 'ibz-core';
import { DEViewComponentBase } from './de-view-component-base';
/**
 * @description 移动端HTML视图
 * @export
 * @class AppMobHtmlView
 * @extends {DEViewComponentBase<AppMobHtmlViewProps>}
 */
export declare class AppMobHtmlView extends DEViewComponentBase<AppMobHtmlViewProps> {
    /**
     * @description 视图控制器
     * @protected
     * @type {IMobHtmlViewController}
     * @memberof AppMobHtmlView
     */
    protected c: IMobHtmlViewController;
    /**
     * @description 设置响应式
     * @memberof AppMobHtmlView
     */
    setup(): void;
    /**
     * @description 绘制html内容
     * @return {*}
     * @memberof AppMobHtmlView
     */
    renderHtmlContent(): JSX.Element;
    /**
     * @description 绘制视图所有部件
     * @return {*}
     * @memberof AppMobHtmlView
     */
    renderViewControls(): any;
}
export declare const AppMobHtmlViewComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
