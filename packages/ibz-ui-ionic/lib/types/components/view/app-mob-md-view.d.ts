import { AppMobMDViewProps, IMobMDViewController } from 'ibz-core';
import { DEMultiDataViewComponentBase } from './de-multi-data-view-component-base';
/**
 * 移动端多数据视图
 *
 * @class AppMobMDView
 * @extends ViewComponentBase
 */
export declare class AppMobMDView extends DEMultiDataViewComponentBase<AppMobMDViewProps> {
    /**
     * @description 多数据视图控制器
     * @protected
     * @type {IMobMDViewController}
     * @memberof AppMobMDView
     */
    protected c: IMobMDViewController;
    /**
     * @description 设置响应式
     * @memberof AppMobMDView
     */
    setup(): void;
}
export declare const AppMobMDViewComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
