import { AppMobMPickUpViewProps, IMobMPickUpViewController } from 'ibz-core';
import { DEViewComponentBase } from './de-view-component-base';
import { IPSControl } from '@ibiz/dynamic-model-api';
/**
 * 移动端多数据选择视图
 *
 * @class AppMobMPickUpView
 * @extends ViewComponentBase
 */
export declare class AppMobMPickUpView extends DEViewComponentBase<AppMobMPickUpViewProps> {
    /**
     * @description 多数据选择视图控制器
     * @protected
     * @type {IMobMPickUpViewController}
     * @memberof AppMobMPickUpView
     */
    protected c: IMobMPickUpViewController;
    /**
     * @description 设置响应式
     * @memberof AppMobMPickUpView
     */
    setup(): void;
    /**
     * @description 绘制顶部按钮
     * @return {*}
     * @memberof AppMobMPickUpView
     */
    renderHeaderButtons(): JSX.Element;
    /**
     * @description 绘制视图所有部件
     * @return {*}
     * @memberof AppMobMPickUpView
     */
    renderViewControls(): any;
    /**
     * @description 额外部件参数
     * @param {*} ctrlProps 部件参数
     * @param {IPSControl} controlInstance 部件实例
     * @memberof AppMobMPickUpView
     */
    extraCtrlParam(ctrlProps: any, controlInstance: IPSControl): void;
}
export declare const AppMobMPickUpViewComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
