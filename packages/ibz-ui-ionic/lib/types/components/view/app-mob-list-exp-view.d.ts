import { AppMobListExpViewProps, IMobListExpViewController } from 'ibz-core';
import { DEExpViewComponentBase } from './de-exp-view-component-base';
/**
 * @description 移动端列表导航视图
 * @export
 * @class AppMobListExpView
 * @extends {DEViewComponentBase<AppMobListExpViewProps>}
 */
export declare class AppMobListExpView extends DEExpViewComponentBase<AppMobListExpViewProps> {
    /**
      * 视图控制器
      *
      * @protected
      * @memberof AppMobListExpView
      */
    protected c: IMobListExpViewController;
    /**
      * 设置响应式
      *
      * @public
      * @memberof AppMobListExpView
      */
    setup(): void;
}
export declare const AppMobListExpViewComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
