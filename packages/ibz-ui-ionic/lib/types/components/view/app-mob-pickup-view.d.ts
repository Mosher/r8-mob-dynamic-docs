import { AppMobPickUpViewProps, IMobPickUpViewController } from 'ibz-core';
import { DEViewComponentBase } from './de-view-component-base';
import { IPSControl } from '@ibiz/dynamic-model-api';
/**
 * 移动端数据选择视图
 *
 * @class AppMobPickUpView
 * @extends DEViewComponentBase
 */
export declare class AppMobPickUpView extends DEViewComponentBase<AppMobPickUpViewProps> {
    /**
     * @description 数据选择视图控制器
     * @protected
     * @type {IMobPickUpViewController}
     * @memberof AppMobPickUpView
     */
    protected c: IMobPickUpViewController;
    /**
     * @description 设置响应式
     * @memberof AppMobPickUpView
     */
    setup(): void;
    /**
     * @description 绘制顶部按钮
     * @return {*}
     * @memberof AppMobPickUpView
     */
    renderHeaderButtons(): JSX.Element;
    /**
     * @description 绘制视图所有部件
     * @return {*}
     * @memberof AppMobPickUpView
     */
    renderViewControls(): any;
    /**
     * @description 额外部件参数
     * @param {*} ctrlProps 部件参数
     * @param {IPSControl} controlInstance 部件实例
     * @memberof AppMobPickUpView
     */
    extraCtrlParam(ctrlProps: any, controlInstance: IPSControl): void;
}
export declare const AppMobPickUpViewComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
