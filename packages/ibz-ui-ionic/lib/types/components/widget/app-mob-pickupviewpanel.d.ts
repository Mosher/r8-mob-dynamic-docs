import { AppMobPickUpViewPanelProps, IMobPickUpViewPanelCtrlController } from 'ibz-core';
import { CtrlComponentBase } from './ctrl-component-base';
export declare class AppMobPickUpViewPanel extends CtrlComponentBase<AppMobPickUpViewPanelProps> {
    /**
     * @description 选择视图面板部件控制器
     * @protected
     * @type {IMobPickUpViewPanelCtrlController}
     * @memberof AppMobPickUpViewPanel
     */
    protected c: IMobPickUpViewPanelCtrlController;
    /**
     * @description 设置响应式
     * @memberof AppMobPickUpViewPanel
     */
    setup(): void;
    /**
     * @description 绘制选择视图面板
     * @return {*}
     * @memberof AppMobPickUpViewPanel
     */
    renderPickUpViewPanel(): import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }>;
    /**
     * @description
     * @return {*}
     * @memberof AppMobPickUpViewPanel
     */
    render(): JSX.Element | null;
}
export declare const AppMobPickUpViewPanelComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
