import { Ref } from 'vue';
import { Subject, Unsubscribable } from 'rxjs';
import { FormGroupPanelController, IParam, IViewStateParam } from 'ibz-core';
import { ComponentBase } from '../../component-base';
declare class AppFormGroupProps {
    /**
     * @description 表单分组控制器实例对象
     * @type {IParam}
     * @memberof AppFormGroupProps
     */
    c: IParam;
    /**
     * @description 部件name
     * @type {string}
     * @memberof AppFormGroupProps
     */
    name: string;
    /**
     * @description 表单数据
     * @type {IParam}
     * @memberof AppFormGroupProps
     */
    data: IParam;
    /**
     * @description 表单状态
     * @type {(Subject<IViewStateParam> | null)}
     * @memberof AppFormGroupProps
     */
    formState: Subject<IViewStateParam> | null;
    /**
     * 模型服务
     *
     * @type {IParam}
     * @memberof AppFormGroupProps
     */
    modelService: IParam;
}
export declare class AppFormGroup extends ComponentBase<AppFormGroupProps> {
    /**
     * @description 表单按钮控制器实例对象
     * @type {FormGroupPanelController}
     * @memberof AppFormGroup
     */
    c: FormGroupPanelController;
    /**
     * @description 部件name
     * @type {string}
     * @memberof AppFormGroup
     */
    name: string;
    /**
     * @description 表单状态
     * @type {Subject<IViewStateParam>}
     * @memberof AppFormGroup
     */
    formState: Subject<IViewStateParam>;
    /**
     * @description 表单事件
     * @type {(Unsubscribable | undefined)}
     * @memberof AppFormGroup
     */
    formStateEvent: Unsubscribable | undefined;
    /**
     * @description 表单旧数据
     * @type {IParam}
     * @memberof AppFormGroup
     */
    oldFormData: IParam;
    /**
     * @description 表单数据
     * @type {IParam}
     * @memberof AppFormGroup
     */
    data: IParam;
    /**
     * @description 收缩内容
     * @type {Ref<boolean>}
     * @memberof AppFormGroup
     */
    collapseContent: Ref<boolean>;
    /**
     * @description 是否显示弹窗
     * @type {Ref<boolean>}
     * @memberof AppFormGroup
     */
    isShowPopover: Ref<boolean>;
    /**
     * @description 弹窗位置
     * @type {Ref<any>}
     * @memberof AppFormGroup
     */
    event: Ref<any>;
    /**
     * @description 设置响应式
     * @memberof AppFormGroup
     */
    setup(): void;
    /**
     * @description 初始化
     * @memberof AppFormGroup
     */
    init(): void;
    /**
     * @description 打开弹窗
     * @param state 状态
     * @memberof AppFormGroup
     */
    setOpenState(state: boolean, $event?: any): void;
    /**
     * @description 点击展开
     * @memberof AppFormGroup
     */
    clickCollapse(): void;
    /**
     * @description 显示更多按钮点击
     * @memberof AppFormGroup
     */
    onShowMoreButtonClick(event: any): void;
    /**
     * @description 执行分组行为
     * @param $event 事件源
     * @param item 数据
     * @memberof AppFormGroup
     */
    handleGroupUIActionClick($event: any, item: any): void;
    /**
     * @description  选中
     * @param $event 事件源
     * @param item 数据
     */
    onSelect($event: any, item: any): void;
    /**
     * @description 绘制项展开
     * @param uiActionGroup 界面行为组
     * @memberof AppFormGroup
     */
    renderItemsExpend(uiActionGroup: any): any;
    /**
     * @description 绘制分组展开
     * @param uiActionGroup 界面行为组
     * @memberof AppFormGroup
     */
    renderGroupExpend(uiActionGroup: any): JSX.Element;
    /**
     * @description 渲染表单分组界面行为组
     * @memberof AppFormGroup
     */
    renderUIActionGroup(): JSX.Element | undefined;
    /**
     * @description 绘制底部
     * @memberof AppFormGroup
     */
    renderFooter(): JSX.Element | undefined;
    /**
     * @description 渲染表单分组内容区
     * @memberof AppFormGroup
     */
    renderContent(): JSX.Element;
    /**
     * @description 渲染表单分组标题
     * @memberof AppFormGroup
     */
    renderLabel(): JSX.Element | undefined;
    /**
     * @description 渲染表单分组
     * @memberof AppFormGroup
     */
    render(): JSX.Element;
}
export declare const AppFormGroupComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
export {};
