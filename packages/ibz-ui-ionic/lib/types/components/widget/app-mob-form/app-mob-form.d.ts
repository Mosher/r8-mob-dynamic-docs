import { Ref } from 'vue';
import { IPSDEFormButton, IPSDEFormDetail, IPSDEFormDRUIPart, IPSDEFormGroupPanel, IPSDEFormItem, IPSDEFormPage, IPSEditor } from '@ibiz/dynamic-model-api';
import { AppMobFormProps, IMobFormCtrlController, AppCtrlProps } from 'ibz-core';
import { CtrlComponentBase } from '../ctrl-component-base';
/**
 * 移动端表单
 *
 * @author chitanda
 * @date 2021-08-06 10:08:39
 * @export
 * @class AppMobForm
 * @extends {ComponentBase}
 */
export declare class AppMobForm<Props extends AppCtrlProps> extends CtrlComponentBase<AppMobFormProps> {
    /**
     * @description 部件控制器
     * @protected
     * @type {IMobFormCtrlController}
     * @memberof AppMobForm
     */
    protected c: IMobFormCtrlController;
    /**
     * @description 当前页
     * @type {Ref<string>}
     * @memberof AppMobForm
     */
    currentPage: Ref<string>;
    /**
     * @description 设置响应式
     * @memberof AppMobForm
     */
    setup(): void;
    /**
     * @description 初始化响应式属性
     * @memberof AppMobForm
     */
    initReactive(): void;
    /**
     * @description 表单分页点击事件
     * @returns
     * @memberof AppMobForm
     */
    segmentChanged(e: any): void;
    /**
     * @description 获取栅格布局
     * @param parent 容器
     * @param child 子成员
     * @memberof AppMobForm
     */
    getGridLayoutProps(parent: any, child: any): {
        'size-lg': any;
        'size-md': any;
        'size-sm': any;
        'size-xs': any;
        'offset-lg': any;
        'offset-md': any;
        'offset-sm': any;
        'offset-xs': any;
    };
    /**
     * @description 获取栅格布局样式
     * @param item 表单成员
     * @memberof AppMobForm
     */
    getGridLayoutStyle(item: IPSDEFormDetail): any;
    /**
     * @description 获取flex布局样式
     * @param item 表单成员
     * @memberof AppMobForm
     */
    getFlexLayoutStyle(item: IPSDEFormDetail): any;
    /**
     * @description 渲染锚点
     * @return {*}
     * @memberof AppMobForm
     */
    renderAnchor(): import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }>;
    /**
     * @description 根据detailType绘制对应detail
     * @param {*} modelJson 模型数据
     * @param {number} index 下标
     * @memberof AppMobForm
     */
    renderByDetailType(modelJson: any, index: number): any;
    /**
     * @description 绘制子表单成员,布局控制
     * @param {*} modelJson 表单成员模型
     * @returns {*}
     * @memberof AppMobForm
     */
    renderDetails(modelJson: any): any;
    /**
     * @description 绘制编辑器
     * @returns {*}
     * @memberof AppMobForm
     */
    renderEditor(editor: IPSEditor, parentItem: any): any;
    /**
     * @description 绘制表单项
     * @returns {*}
     * @memberof AppMobForm
     */
    renderFormItem(modelJson: IPSDEFormItem, index: number): any;
    /**
     * @description 绘制关系界面
     * @returns {*}
     * @memberof AppFormBase
     */
    renderDruipart(modelJson: IPSDEFormDRUIPart, index: number): any;
    /**
     * @description 绘制按钮
     * @returns {*}
     * @memberof AppMobForm
     */
    renderButton(modelJson: IPSDEFormButton, index: number): any;
    /**
     * @description 绘制分组面板
     * @returns {*}
     * @memberof AppMobForm
     */
    renderGroupPanel(modelJson: IPSDEFormGroupPanel, index: number): any;
    /**
     * @description 绘制表单分页
     * @returns {*}
     * @memberof AppMobForm
     */
    renderFormPage(modelJson: IPSDEFormPage, index: number): any;
    /**
     * @description 绘制表单内容
     * @returns {*}
     * @memberof AppMobForm
     */
    renderFormContent(): any[] | JSX.Element | undefined;
    /**
     * @description 部件样式名
     * @readonly
     * @type {any}
     * @memberof AppMobForm
     */
    get classNames(): any;
    /**
     * @description 绘制移动端表单
     * @returns {JSX.Element | JSX.Element[] | null}
     * @memberof AppMobForm
     */
    render(): JSX.Element | JSX.Element[] | null;
}
export declare const AppMobFormComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
