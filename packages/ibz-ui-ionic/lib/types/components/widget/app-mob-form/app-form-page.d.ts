import { Ref } from 'vue';
import { FormPageController, IParam, IViewStateParam } from 'ibz-core';
import { Subject, Unsubscribable } from 'rxjs';
import { ComponentBase } from '../../component-base';
/**
 * 表单分页组件输入参数
 *
 * @class AppFormPageProps
 */
declare class AppFormPageProps {
    /**
     * 表单分页控制器
     *
     * @type {FormButtonController}
     * @memberof AppFormPageProps
     */
    c: IParam;
    /**
     * @description 部件name
     * @type {string}
     * @memberof AppFormPageProps
     */
    name: string;
    /**
     * @description 表单数据
     * @type {IParam}
     * @memberof AppFormPageProps
     */
    data: IParam;
    /**
     * 表单状态
     *
     * @type {Subject<IViewStateParam>}
     * @memberof AppFormPageProps
     */
    formState: Subject<IViewStateParam> | null;
    /**
     * 当前分页
     *
     * @type {string}
     * @memberof AppFormPageProps
     */
    currentPage: string;
    /**
     * 模型服务
     *
     * @type {IParam}
     * @memberof AppFormPageProps
     */
    modelService: IParam;
}
/**
 * 表单分页
 *
 * @class AppFormPage
 */
export default class AppFormPage extends ComponentBase<AppFormPageProps> {
    /**
     * @description 表单分页控制器实例对象
     * @type {FormPageController}
     * @memberof AppFormPage
     */
    c: FormPageController;
    /**
     * @description 部件name
     * @type {string}
     * @memberof AppFormPage
     */
    name: string;
    /**
     * @description 表单状态
     * @type {Subject<IViewStateParam>}
     * @memberof AppFormPage
     */
    formState: Subject<IViewStateParam>;
    /**
     * @description 表单事件
     * @type {(Unsubscribable | undefined)}
     * @memberof AppFormPage
     */
    formStateEvent: Unsubscribable | undefined;
    /**
     * @description 表单旧数据
     * @type {IParam}
     * @memberof AppFormPage
     */
    oldFormData: IParam;
    /**
     * @description 表单数据
     * @type {IParam}
     * @memberof AppFormPage
     */
    data: IParam;
    /**
     * @description 当前页
     * @type {IParam}
     * @memberof AppFormPage
     */
    currentPage: Ref<string>;
    /**
     * 设置响应式
     *
     * @memberof AppFormPage
     */
    setup(): void;
    /**
     * 初始化
     *
     * @memberof AppFormPage
     */
    init(): void;
    renderContent(): JSX.Element;
    render(): any;
}
export declare const AppFormPageComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
export {};
