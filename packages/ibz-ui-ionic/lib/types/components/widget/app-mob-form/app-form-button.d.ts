import { FormButtonController, IParam, IViewStateParam } from 'ibz-core';
import { Subject, Unsubscribable } from 'rxjs';
import { ComponentBase } from '../../component-base';
declare class AppFormButtonProps {
    /**
     * @description 表单按钮控制器实例对象
     * @type {IParam}
     * @memberof AppFormButtonProps
     */
    c: IParam;
    /**
     * @description 名称
     * @type {string}
     * @memberof AppFormButtonProps
     */
    name: string;
    /**
     * @description 表单数据
     * @type {IParam}
     * @memberof AppFormButtonProps
     */
    data: IParam;
    /**
     * @description 表单状态
     * @type {(Subject<IViewStateParam> | null)}
     * @memberof AppFormButtonProps
     */
    formState: Subject<IViewStateParam> | null;
    /**
     * 模型服务
     *
     * @type {IParam}
     * @memberof AppFormButtonProps
     */
    modelService: IParam;
}
/**
 * 表单按钮
 */
export declare class AppFormButton extends ComponentBase<AppFormButtonProps> {
    /**
     * @description 表单按钮控制器实例对象
     * @type {FormButtonController}
     * @memberof AppFormButton
     */
    c: FormButtonController;
    /**
     * @description 名称
     * @type {string}
     * @memberof AppFormButton
     */
    name: string;
    /**
     * @description 表单状态
     * @type {Subject<IViewStateParam>}
     * @memberof AppFormButton
     */
    formState: Subject<IViewStateParam>;
    /**
     * @description 表单事件
     * @type {(Unsubscribable | undefined)}
     * @memberof AppFormButton
     */
    formStateEvent: Unsubscribable | undefined;
    /**
     * @description 表单旧数据
     * @type {IParam}
     * @memberof AppFormButton
     */
    oldFormData: IParam;
    /**
     * @description 表单数据
     * @type {IParam}
     * @memberof AppFormButton
     */
    data: IParam;
    /**
     * @description 设置响应式
     * @memberof AppFormButton
     */
    setup(): void;
    /**
     * @description 初始化
     * @memberof AppFormButton
     */
    init(): void;
    /**
     * @description 表单按钮点击
     * @param event 源事件
     */
    buttonClick(event: any): void;
    /**
     * @description 渲染表单按钮
     * @memberof AppFormButton
     */
    render(): JSX.Element;
}
export declare const AppFormButtonComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
export {};
