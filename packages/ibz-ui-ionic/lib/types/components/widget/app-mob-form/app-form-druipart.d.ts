import { Subject, Unsubscribable } from 'rxjs';
import { FormDruipartController, IParam, IViewStateParam } from 'ibz-core';
import { ComponentBase } from '../../component-base';
export declare class AppFormDruipartProps {
    /**
     * @description 表单数据
     * @type {string}
     * @memberof AppFormDruipartProps
     */
    data: string;
    /**
     * @description 名称
     * @type {string}
     * @memberof AppFormDruipartProps
     */
    name: string;
    /**
     * @description 关系界面控制器
     * @type {IParam}
     * @memberof AppFormDruipartProps
     */
    c: IParam;
    /**
     * @description 应用实体参数名称
     * @type {string}
     * @memberof AppFormDruipartProps
     */
    parameterName: string;
    /**
     * @description 表单状态
     * @type {(Subject<IViewStateParam> | null)}
     * @memberof AppFormDruipartProps
     */
    formState: Subject<IViewStateParam> | null;
    /**
     * @description 应用上下文
     * @type {IParam}
     * @memberof AppFormDruipartProps
     */
    navContext: IParam;
    /**
     * @description 视图参数
     * @type {IParam}
     * @memberof AppFormDruipartProps
     */
    navParam: IParam;
    /**
     * @description 视图操作参数集合
     * @type {IParam}
     * @memberof AppFormDruipartProps
     */
    viewCtx: IParam;
    /**
     * @description 忽略表单值变更
     * @type {boolean}
     * @memberof AppFormDruipartProps
     */
    ignorefieldvaluechange: boolean;
    /**
     * 模型服务
     *
     * @type {IParam}
     * @memberof AppFormDruipartProps
     */
    modelService: IParam;
}
export declare class AppFormDruipart extends ComponentBase<AppFormDruipartProps> {
    /**
     * @description 关系视图组件
     * @type {string}
     * @memberof AppFormDruipart
     */
    viewComponent: string;
    /**
     * @description 刷新节点
     * @public
     * @type {string[]}
     * @memberof AppFormDRUIPart
     */
    hookItems: string[];
    /**
     * @description 关系视图控制器
     * @type {FormDruipartController}
     * @memberof AppFormDruipart
     */
    c: FormDruipartController;
    /**
     * @description 关系界面向视图下发指令对象
     * @public
     * @type {Subject<IViewStateParam>}
     * @memberof AppFormDruipart
     */
    formState: Subject<IViewStateParam>;
    /**
     * @description 应用实体参数名称
     * @type {string}
     * @memberof AppFormDRUIPart
     */
    parameterName: string;
    /**
     * @description 部件name
     * @type {string}
     * @memberof AppFormDRUIPart
     */
    name: string;
    /**
     * @description 表单状态事件
     * @public
     * @type {(Unsubscribable | undefined)}
     * @memberof AppFormDRUIPart
     */
    formStateEvent: Unsubscribable | undefined;
    /**
     * @description 表单老数据
     * @public
     * @type {(Unsubscribable | undefined)}
     * @memberof AppFormDRUIPart
     */
    oldFormData: IParam;
    /**
     * @description 唯一标识
     * @type {[string]}
     * @memberof AppFormDRUIPart
     */
    uuid: string;
    /**
     * @description 是否启用遮罩
     * @type {boolean}
     * @memberof AppFormDRUIPart
     */
    blockUI: boolean;
    /**
     * @description 是否刷新关系数据
     * @public
     * @type {boolean}
     * @memberof AppFormDRUIPart
     */
    isRelationalData: boolean;
    /**
     * @description 存储应用上下文
     * @public
     * @type {IParam}
     * @memberof AppFormDRUIPart
     */
    embedViewContext: IParam;
    /**
     * @description 存储应用视图参数
     * @public
     * @type {IParam}
     * @memberof AppFormDRUIPart
     */
    embedViewParam: IParam;
    /**
     * @description 初始化
     * @memberof AppFormDRUIPart
     */
    setup(): void;
    /**
     * @description 挂载
     * @memberof AppFormDRUIPart
     */
    init(): void;
    /**
     * @description 初始化关系界面
     * @memberof AppFormDRUIPart
     */
    initFormDrUIPart(): void;
    /**
     * @description 监听数据变化
     * @memberof AppFormDRUIPart
     */
    watchEffect(): void;
    /**
     * @description 初始化视图组件名称
     * @memberof AppFormDRUIPart
     */
    initViewComponent(): void;
    /**
     * @description 刷新关系页面
     * @public
     * @returns {void}
     * @memberof AppFormDRUIPart
     */
    refreshDRUIPart(formData: IParam | undefined): void;
    /**
     * @description 开启遮罩
     * @public
     * @memberof AppFormDRUIPart
     */
    blockUIStart(): void;
    /**
     * @description 关闭遮罩
     * @public
     * @memberof AppFormDRUIPart
     */
    blockUIStop(): void;
    /**
     * @description 处理视图事件
     * @public
     * @memberof AppFormDRUIPart
     */
    handleViewEvent(viewname: string, action: string, data: any): void;
    /**
     * @description 组件销毁
     * @public
     * @memberof AppFormDRUIPart
     */
    unmounted(): void;
    /**
     * @description 绘制关系视图
     * @public
     * @memberof AppFormDRUIPart
     */
    renderDruipartView(): import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }>;
    /**
     * @description 绘制内容
     * @memberof AppFromItem
     */
    render(): JSX.Element;
}
export declare const AppFormDruipartComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
