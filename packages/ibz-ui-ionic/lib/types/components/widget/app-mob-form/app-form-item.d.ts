import { Subject, Unsubscribable } from 'rxjs';
import { FormItemController, IParam, IViewStateParam } from 'ibz-core';
import { ComponentBase } from '../../component-base';
/**
 * 表单项输入参数
 *
 * @class AppFormItemProps
 */
declare class AppFormItemProps {
    /**
     * 表单数据项控制器
     *
     * @type {FormButtonController}
     * @memberof AppFormItemProps
     */
    c: IParam;
    /**
     * @description 部件name
     * @type {string}
     * @memberof AppFormItemProps
     */
    name: string;
    /**
     * @description 表单数据
     * @type {IParam}
     * @memberof AppFormItemProps
     */
    data: IParam;
    /**
     * @description 表单状态
     * @type {(Subject<IViewStateParam> | null)}
     * @memberof AppFormItemProps
     */
    formState: Subject<IViewStateParam> | null;
    /**
     * 模型服务
     *
     * @type {IParam}
     * @memberof AppFormItemProps
     */
    modelService: IParam;
}
/**
 * 表单项
 *
 * @class AppFormItem
 */
export default class AppFormItem extends ComponentBase<AppFormItemProps> {
    /**
     * @description 表单按钮控制器实例对象
     * @type {FormButtonController}
     * @memberof AppFormItem
     */
    c: FormItemController;
    /**
     * @description 部件name
     * @type {string}
     * @memberof AppFormItem
     */
    name: string;
    /**
     * @description 表单状态
     * @type {Subject<IViewStateParam>}
     * @memberof AppFormItem
     */
    formState: Subject<IViewStateParam>;
    /**
     * @description 表单事件
     * @type {(Unsubscribable | undefined)}
     * @memberof AppFormItem
     */
    formStateEvent: Unsubscribable | undefined;
    /**
     * @description 表单旧数据
     * @type {IParam}
     * @memberof AppFormItem
     */
    oldFormData: IParam;
    /**
     * @description 表单数据
     * @type {IParam}
     * @memberof AppFormItem
     */
    data: IParam;
    /**
     * @description 设置响应式
     * @memberof AppFormItem
     */
    setup(): void;
    /**
     * @description 初始化
     * @memberof AppFormItem
     */
    init(): void;
    /**
     * @description 无标签
     * @memberof AppFormItem
     */
    renderNoneLabel(): JSX.Element[];
    /**
     * @description 标签位于下方
     * @memberof AppFormItem
     */
    renderBottomLabel(): (JSX.Element | null)[];
    /**
     * @description 标签位于上方
     * @memberof AppFormItem
     */
    renderTopLabel(): (JSX.Element | null)[];
    /**
     * @description 标签位于右侧
     * @memberof AppFormItem
     */
    renderRightLabel(): (JSX.Element | null)[];
    /**
     * @description 标签位于左侧
     * @memberof AppFormItem
     */
    renderLeftLabel(): (JSX.Element | null)[];
    /**
     * @description 根据标签位置渲染
     * @memberof AppFormItem
     */
    renderContentByLabelPos(): (JSX.Element | null)[] | undefined;
    /**
     * @description 渲染表单项
     * @returns {JSX.Element | JSX.Element[] | null}
     * @memberof AppFormItem
     */
    render(): JSX.Element | JSX.Element[] | null;
}
export declare const AppFormItemComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
export {};
