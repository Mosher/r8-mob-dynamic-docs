import { AppMobListExpBarProps, IMobListExpBarCtrlController } from 'ibz-core';
import { ExpBarCtrlComponentBase } from './exp-bar-ctrl-component-base';
/**
 * @description 移动端列表导航部件
 * @export
 * @class AppMobListExpBar
 * @extends {DEViewComponentBase<AppMobListExpBarProps>}
 */
export declare class AppMobListExpBar extends ExpBarCtrlComponentBase<AppMobListExpBarProps> {
    /**
     * 部件控制器
     *
     * @protected
     * @memberof AppMobListExpBar
     */
    protected c: IMobListExpBarCtrlController;
    /**
     * 设置响应式
     *
     * @public
     * @memberof AppMobListExpBar
     */
    setup(): void;
}
export declare const AppMobListExpBarComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
