import { AppMobTabExpPanelProps, IMobTabExpPanelCtrlController } from 'ibz-core';
import { CtrlComponentBase } from './ctrl-component-base';
import { IPSDETabViewPanel } from '@ibiz/dynamic-model-api';
/**
 * 移动端分页导航面板部件
 *
 * @export
 * @class AppMobTabExpPanel
 * @extends CtrlComponentBase
 */
export declare class AppMobTabExpPanel extends CtrlComponentBase<AppMobTabExpPanelProps> {
    /**
     * @description 部件控制器
     * @protected
     * @type {IMobTabExpPanelCtrlController}
     * @memberof AppMobTabExpPanel
     */
    protected c: IMobTabExpPanelCtrlController;
    /**
     * @description 设置响应式
     * @memberof AppMobTabExpPanel
     */
    setup(): void;
    /**
     * @description 初始化响应式属性
     * @memberof AppMobTabExpPanel
     */
    initReactive(): void;
    /**
     * @description 分页节点切换
     * @private
     * @param {*} $event 节点数据
     * @return {*}
     * @memberof AppMobTabExpPanel
     */
    private ionChange;
    /**
     * @description 输出分页头部
     * @param {IPSDETabViewPanel[]} allControls 所有分页视图面板部件
     * @return {*}
     * @memberof AppMobTabExpPanel
     */
    renderSegment(allControls: IPSDETabViewPanel[]): JSX.Element;
    /**
     * @description 分页视图面板部件渲染
     * @param {IPSDETabViewPanel[]} allControls
     * @return {*}
     * @memberof AppMobTabExpPanel
     */
    renderTabViewPanel(allControls: IPSDETabViewPanel[]): (import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }> | undefined)[];
    /**
     * @description 分页导航面板部件渲染
     * @return {*}
     * @memberof AppMobTabExpPanel
     */
    render(): JSX.Element | null;
}
export declare const AppMobTabExpPanelComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
