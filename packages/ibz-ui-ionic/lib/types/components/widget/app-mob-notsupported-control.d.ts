import { AppCtrlProps } from 'ibz-core';
import { CtrlComponentBase } from './ctrl-component-base';
/**
 * 未支持部件
 *
 * @export
 * @class AppMobNotSupportedControl
 * @extends {ComponentBase}
 */
export declare class AppMobNotSupportedControl extends CtrlComponentBase<AppCtrlProps> {
    /**
     * 设置响应式
     *
     * @public
     * @memberof AppMobNotSupportedControl
     */
    setup(): void;
    /**
     * 绘制内容
     *
     * @public
     * @memberof AppMobNotSupportedControl
     */
    render(): JSX.Element | null;
}
export declare const AppMobNotSupportedControlComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
