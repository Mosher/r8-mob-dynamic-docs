import { AppMobStateWizardPanelProps, IMobStateWizardPanelController } from 'ibz-core';
import { CtrlComponentBase } from './ctrl-component-base';
import { IPSDEEditForm } from '@ibiz/dynamic-model-api';
/**
 * @description 移动端状态向导面板部件
 * @export
 * @class AppMobStateWizardPanel
 * @extends {DEViewComponentBase<AppMobStateWizardPanelProps>}
 */
export declare class AppMobStateWizardPanel extends CtrlComponentBase<AppMobStateWizardPanelProps> {
    /**
     * @description 移动端状态向导面板控制器实例对象
     * @protected
     * @type {IMobStateWizardPanelController}
     * @memberof AppMobStateWizardPanel
     */
    protected c: IMobStateWizardPanelController;
    /**
     * @description 设置响应式
     * @memberof AppMobStateWizardPanel
     */
    setup(): void;
    protected getActiveFormInstance(name: string): IPSDEEditForm | undefined;
    /**
     * @description 获取激活步骤索引
     * @protected
     * @return {*}  {number}
     * @memberof AppMobStateWizardPanel
     */
    protected getActiveStep(): number;
    /**
     * @description 按钮显示状态
     * @protected
     * @param {string} type 按钮步骤类型
     * @return {*}  {boolean}
     * @memberof AppMobWizardPanel
     */
    protected buttonStatus(type: string): boolean;
    /**
     * @description 上一步
     * @protected
     * @param {*} event 源事件对象
     * @memberof AppMobWizardPanel
     */
    protected onClickPrev(event: any): void;
    /**
     * @description 下一步
     * @protected
     * @param {*} event 源事件对象
     * @memberof AppMobWizardPanel
     */
    protected onClickNext(event: any): void;
    /**
     * @description 完成
     * @protected
     * @param {*} event 源事件对象
     * @memberof AppMobWizardPanel
     */
    protected onClickFinish(event: any): void;
    /**
     * @description 渲染向导步骤表单
     * @return {*}
     * @memberof AppMobStateWizardPanel
     */
    renderWizardStepForm(): import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }> | undefined;
    /**
     * @description 渲染状态向导面板
     * @return {*}
     * @memberof AppMobStateWizardPanel
     */
    render(): JSX.Element | undefined;
}
export declare const AppMobStateWizardPanelComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
