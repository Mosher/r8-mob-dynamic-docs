import { IPSDEToolbarItem } from '@ibiz/dynamic-model-api';
import { IParam, IMobToolbarCtrlController } from 'ibz-core';
import { AppMobToolbarProps } from 'ibz-core';
import { CtrlComponentBase } from './ctrl-component-base';
/**
 * @description 视图工具栏
 * @export
 * @class AppMobToolbar
 * @extends {CtrlComponentBase<AppMobToolbarProps>}
 */
export declare class AppMobToolbar extends CtrlComponentBase<AppMobToolbarProps> {
    /**
     * @description 部件控制器
     * @protected
     * @type {IMobToolbarCtrlController}
     * @memberof AppMobToolbar
     */
    protected c: IMobToolbarCtrlController;
    /**
     * @description 工具栏样式
     * @type {string}
     * @memberof AppMobToolbar
     */
    toolbarStyle: string;
    /**
     * @description 工具栏弹出方向
     * @type {string}
     * @memberof AppMobToolbar
     */
    toolbarSide: string;
    /**
     * @description 工具栏弹出菜单引用
     * @type {IParam}
     * @memberof AppMobToolbar
     */
    toolbarRef: IParam;
    /**
     * @description 工具栏菜单uuid
     * @type {string}
     * @memberof AppMobToolbar
     */
    toolbarID: string;
    /**
     * @description 工具栏内容uuid
     * @type {string}
     * @memberof AppMobToolbar
     */
    contentId: string;
    /**
     * @description 设置响应式
     * @memberof AppMobToolbar
     */
    setup(): void;
    /**
     * @description 部件初始化
     * @memberof AppMobToolbar
     */
    init(): void;
    /**
     * @description 打开工具栏
     * @memberof AppMobToolbar
     */
    openToolbar(): Promise<void>;
    /**
     * @description 关闭工具栏
     * @memberof AppMobToolbar
     */
    closeToolbar(): void;
    /**
     * @description 工具栏项点击
     * @param {string} name
     * @param {MouseEvent} e
     * @memberof AppMobToolbar
     */
    itemClick(item: any, e: MouseEvent): void;
    /**
     * @description 获取图标
     * @param {IPSDEToolbarItem} item
     * @return {*}  {*}
     * @memberof AppMobToolbar
     */
    getIcon(item: IPSDEToolbarItem): any;
    /**
     * @description 计算工具栏样式参数
     * @memberof AppMobToolbar
     */
    calcToolbarStyleParams(): void;
    /**
     * @description 绘制菜单工具项
     * @protected
     * @param {*} item
     * @return {*}  {*}
     * @memberof AppMobToolbar
     */
    protected renderToolbarItem(item: any): any;
    /**
     * @description 绘制侧边工具栏
     * @protected
     * @return {*}
     * @memberof AppMobToolbar
     */
    protected renderSideToolbar(): JSX.Element | null;
    /**
     * @description 绘制工具栏按钮
     * @protected
     * @return {*}  {*}
     * @memberof AppMobToolbar
     */
    protected renderToolbarButton(): any;
    /**
     * @description 绘制侧边工具栏
     * @param {string} side
     * @return {*}
     * @memberof AppMobToolbar
     */
    renderToolbar(side: string): JSX.Element | null;
    /**
     * @description 打开底部菜单
     * @memberof AppMobToolbar
     */
    openBottomToolbar(): Promise<void>;
    /**
     * @description 绘制平铺工具栏
     * @protected
     * @return {*}
     * @memberof AppMobToolbar
     */
    protected renderDefaultToolbar(): any;
    /**
     * @description 绘制工具栏
     * @return {*}  {*}
     * @memberof AppMobToolbar
     */
    render(): any;
}
export declare const AppMobToolbarComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
