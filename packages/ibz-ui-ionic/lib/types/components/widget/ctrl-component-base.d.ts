import { IAppCtrlControllerBase, MobMapCtrlController, MobTabExpPanelCtrlController, MobTabViewPanelCtrlController, ICtrlEventParam, IParam, MobTreeExpBarController, MobMapExpBarController } from 'ibz-core';
import { AppCtrlProps, MobMenuCtrlController, MobFormCtrlController, MobMDCtrlController, MobPanelController, MobToolbarCtrlController, MobCalendarCtrlController, MobPickUpViewPanelCtrlController, MobTreeCtrlController, MobChartCtrlController, MobMEditViewPanelCtrlController, MobContextMenuCtrlController, MobDashboardController, MobPortletController, MobListExpBarController, MobWizardPanelController, MobStateWizardPanelController, MobChartExpBarController } from 'ibz-core';
import { ComponentBase } from '../component-base';
import { IPSControl } from '@ibiz/dynamic-model-api';
/**
 * 部件组件基类
 *
 * @export
 * @class CtrlComponentBase
 */
export declare class CtrlComponentBase<Props extends AppCtrlProps> extends ComponentBase<AppCtrlProps> {
    /**
     * @description 部件是否加载完成
     * @type {*}
     * @memberof CtrlComponentBase
     */
    controlIsLoaded: any;
    /**
     * @description 部件控制器
     * @protected
     * @type {IAppCtrlControllerBase}
     * @memberof CtrlComponentBase
     */
    protected c: IAppCtrlControllerBase;
    /**
     * @description vue ref引用集合
     * @type {Map<string, any>}
     * @memberof CtrlComponentBase
     */
    refsMap: Map<string, any>;
    /**
     * @description 部件样式名
     * @readonly
     * @type {any}
     * @memberof CtrlComponentBase
     */
    get classNames(): any;
    /**
     * @description 设置响应式
     * @memberof CtrlComponentBase
     */
    setup(): void;
    /**
     * @description 初始化响应式属性
     * @memberof CtrlComponentBase
     */
    initReactive(): void;
    /**
     * @description 部件初始化
     * @memberof CtrlComponentBase
     */
    init(): void;
    /**
     * @description 部件输入属性值变更
     * @memberof CtrlComponentBase
     */
    watchEffect(): void;
    /**
     * @description 部件销毁
     * @memberof CtrlComponentBase
     */
    unmounted(): void;
    /**
     * @description 执行部件事件
     * @param {ICtrlEventParam} arg 事件参数
     * @memberof CtrlComponentBase
     */
    emitCtrlEvent(arg: ICtrlEventParam): void;
    /**
     * @description 抛出关闭视图事件
     * @param {IParam} arg 事件参数
     * @memberof CtrlComponentBase
     */
    emitCloseView(arg: IParam): void;
    /**
     * @description 部件挂载完成
     * @param {IParam} arg 事件参数
     * @memberof CtrlComponentBase
     */
    ctrlMounted(args?: any): void;
    /**
     * @description 根据部件类型获取部件控制器
     * @param {string} type 部件类型
     * @return {*}
     * @memberof CtrlComponentBase
     */
    getCtrlControllerByType(type: string): MobMenuCtrlController | MobToolbarCtrlController | MobFormCtrlController | MobMDCtrlController | MobPanelController | MobPickUpViewPanelCtrlController | MobCalendarCtrlController | MobTreeCtrlController | MobChartCtrlController | MobMapCtrlController | MobContextMenuCtrlController | MobTabExpPanelCtrlController | MobTabViewPanelCtrlController | MobMEditViewPanelCtrlController | MobDashboardController | MobPortletController | MobListExpBarController | MobWizardPanelController | MobChartExpBarController | MobStateWizardPanelController | MobTreeExpBarController | MobMapExpBarController | null;
    /**
     * @description 计算目标部件所需参数
     * @param {IPSControl} controlInstance 部件模型实例
     * @return {*}
     * @memberof CtrlComponentBase
     */
    computeTargetCtrlData(controlInstance: IPSControl, otherParams?: IParam): import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }>;
    /**
     * @description 下拉刷新
     * @param {*} event 事件源
     * @memberof CtrlComponentBase
     */
    pullDownRefresh(event: any): void;
    /**
     * @description 绘制下拉刷新
     * @return {*}
     * @memberof CtrlComponentBase
     */
    renderPullDownRefresh(): JSX.Element | undefined;
    /**
     * @description 绘制无数据
     * @return {*}
     * @memberof CtrlComponentBase
     */
    renderNoData(): JSX.Element;
}
