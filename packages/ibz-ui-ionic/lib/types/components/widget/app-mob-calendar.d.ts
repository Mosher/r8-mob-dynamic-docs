import { AppMobCalendarProps, IMobCalendarCtrlController, IParam } from 'ibz-core';
import { MDCtrlComponentBase } from './md-ctrl-component-base';
import { Ref } from 'vue';
import { IPSSysCalendarItem } from '@ibiz/dynamic-model-api';
export declare class AppMobCalendar extends MDCtrlComponentBase<AppMobCalendarProps> {
    /**
     * @description  日历部件控制器
     * @protected
     * @type {IMobCalendarCtrlController}
     * @memberof AppMobCalendar
     */
    protected c: IMobCalendarCtrlController;
    /**
     * @description 当前年份
     * @private
     * @type {Ref<number>}
     * @memberof AppMobCalendar
     */
    private year;
    /**
     * @description 当前月份(0~11)
     * @private
     * @type {Ref<number>}
     * @memberof AppMobCalendar
     */
    private month;
    /**
     * @description 当前日期
     * @private
     * @type {Ref<number>}
     * @memberof AppMobCalendar
     */
    private day;
    /**
     * @description 当前时间
     * @private
     * @type {Ref<Date>}
     * @memberof AppMobCalendar
     */
    private currentDate;
    /**
     * @description 当前激活数据
     * @type {*}
     * @memberof AppMobCalendar
     */
    activeData: any;
    /**
     * @description 默认选中值
     * @private
     * @type {Ref<any[]>}
     * @memberof AppMobCalendar
     */
    private value;
    /**
     * @description 事程内容
     * @private
     * @type {IParam[]}
     * @memberof AppMobCalendar
     */
    private tileContent;
    /**
     * @description 时间轴加载条数
     * @private
     * @type {IParam[]}
     * @memberof AppMobCalendar
     */
    private count;
    /**
     * @description 选中数组
     * @private
     * @type {IParam[]}
     * @memberof AppMobCalendar
     */
    private selectedArray;
    /**
     * @description 开始拖动位置
     * @private
     * @type {number}
     * @memberof AppMobCalendar
     */
    private StarttouchLength;
    /**
     * @description 事程数据
     * @private
     * @type {*}
     * @memberof AppMobCalendar
     */
    private eventsDate;
    /**
     * @description 日历组件引用
     * @private
     * @type {Ref<any>}
     * @memberof AppMobCalendar
     */
    private calendar;
    /**
     * @description 长按定时器
     * @type {Ref<any>}
     * @memberof AppMobCalendar
     */
    touchTimer: Ref<any>;
    /**
     * @description 是否打开上下文菜单
     * @type {*}
     * @memberof AppMobCalendar
     */
    isShowContextMenu: boolean;
    /**
     * @description 设置响应式
     * @memberof AppMobCalendar
     */
    setup(): void;
    /**
     * @description 初始化响应式属性
     * @memberof AppMobCalendar
     */
    initReactive(): void;
    /**
     * @description 初始化当前时间
     * @private
     * @param {*} [curTime=new Date()] 当前时间
     * @memberof AppMobCalendar
     */
    private initCurrentTime;
    /**
     * @description 清除定时器
     * @memberof AppMobCalendar
     */
    clearTimer(): void;
    /**
     * @description 长按开始
     * @param {*} item 节点数据
     * @param {*} event 事件源
     * @memberof AppMobCalendar
     */
    onTouchStart(item: any, event: MouseEvent): void;
    /**
     * @description 初始化时间轴加载条数
     * @private
     * @memberof AppMobCalendar
     */
    private initTimeLineCount;
    /**
     * @description 分页节点切换
     * @private
     * @param {*} $event 数据源
     * @return {*}
     * @memberof AppMobCalendar
     */
    private ionChange;
    /**
     * @description 查询天数
     * @private
     * @param {number} year 年
     * @param {number} month 月
     * @param {number} weekIndex 周索引
     * @memberof AppMobCalendar
     */
    private selectday;
    /**
     * @description 上个
     * @private
     * @param {number} year 年
     * @param {number} month 月
     * @param {number} weekIndex 周索引
     * @memberof AppMobCalendar
     */
    private prev;
    /**
     * @description 下个
     * @private
     * @param {number} year 年
     * @param {number} month 月
     * @param {number} weekIndex 周索引
     * @memberof AppMobCalendar
     */
    private next;
    /**
     * @description 根据周下标计算事件
     * @private
     * @param {number} year 年
     * @param {number} month 月
     * @param {number} week 周
     * @memberof AppMobCalendar
     */
    private countWeeks;
    /**
     * @description 选择年份事件的回调方法
     * @private
     * @param {number} year 年
     * @memberof AppMobCalendar
     */
    private selectYear;
    /**
     * @description 选择月份事件的回调方法
     * @private
     * @param {number} month 月
     * @param {number} year 年
     * @memberof AppMobCalendar
     */
    private selectMonth;
    /**
     * @description 点击前一天
     * @private
     * @memberof AppMobCalendar
     */
    private prevDate;
    /**
     * @description 点击后一天
     * @private
     * @memberof AppMobCalendar
     */
    private nextDate;
    /**
     * @description 日历部件数据选择日期回调
     * @private
     * @param {*} data 日期数据
     * @memberof AppMobCalendar
     */
    private clickDay;
    /**
     * @description 选中或取消事件
     * @private
     * @param {*} item 事件项
     * @memberof AppMobCalendar
     */
    private checkboxSelect;
    /**
     * @description 开始滑动
     * @private
     * @param {*} e 事件源
     * @memberof AppMobCalendar
     */
    private gotouchstart;
    /**
     * @description 触摸移动
     * @private
     * @param {*} e 事件源
     * @memberof AppMobCalendar
     */
    private gotouchmove;
    /**
     * @description 日程点击
     * @private
     * @param {IParam} $event 事件信息
     * @memberof AppMobCalendar
     */
    private onEventClick;
    /**
     * @description 删除事程
     * @private
     * @param {any[]} data 事程数据集
     * @memberof AppMobCalendar
     */
    private remove;
    /**
     * @description 绘制日历样式----月
     * @return {*}
     * @memberof AppMobCalendar
     */
    renderMonthCalendar(): JSX.Element;
    /**
     * @description 绘制日历样式----天
     * @return {*}
     * @memberof AppMobCalendar
     */
    renderDayCalendar(): JSX.Element;
    /**
     * @description 绘制日历样式----周
     * @return {*}
     * @memberof AppMobCalendar
     */
    renderWeekCalendar(): JSX.Element;
    /**
     * @description 绘制分页头
     * @param {IPSSysCalendarItem[]} calendarItems 日历项
     * @return {*}
     * @memberof AppMobCalendar
     */
    renderSegment(calendarItems: IPSSysCalendarItem[]): JSX.Element;
    /**
     * @description 绘制日历样式----时间轴
     * @param {*} calendarStyle 日历样式
     * @return {*}
     * @memberof AppMobCalendar
     */
    renderTimelineCalendar(calendarStyle: any): any;
    /**
     * @description 绘制时间轴
     * @return {*}
     * @memberof AppMobCalendar
     */
    renderTimeline(): JSX.Element;
    /**
     * @description 绘制月-周时间轴
     * @return {*}
     * @memberof AppMobCalendar
     */
    renderWMTimeline(): JSX.Element;
    /**
     * @description 绘制事件内容
     * @param {*} item 日程事件
     * @return {*}
     * @memberof AppMobCalendar
     */
    renderEventContent(item: any): (JSX.Element | undefined)[];
    /**
     * @description 绘制事程列表
     * @return {*}
     * @memberof AppMobCalendar
     */
    renderEventList(): any;
    /**
     * @description 绘制分组事程
     * @memberof AppMobCalendar
     */
    renderGroupEvent(): JSX.Element | (import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }> | undefined)[];
    /**
     * @description 绘制无分组事程
     * @memberof AppMobCalendar
     */
    renderNoGroupEvent(): JSX.Element | (import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }> | undefined)[];
    /**
     * @description 绘制日历项默认样式
     * @param {IPSSysCalendarItem} calendarItem 日历项
     * @param {IParam} item 日历项数据
     * @param {string} itemStyle 日历项样式
     * @memberof AppMobCalendar
     */
    renderCalendarItemDefault(calendarItem: any, item: IParam, itemStyle: IParam): JSX.Element;
    /**
     * @description 绘制日历项布局面板
     * @param {IPSSysCalendarItem} calendarItem 日历项
     * @param {IParam} item 日历项数据
     * @param {string} itemStyle 日历项样式
     * @return {*}
     * @memberof AppMobCalendar
     */
    renderCalendarItemLayoutPanel(calendarItem: IPSSysCalendarItem, item: IParam, itemStyle: IParam): JSX.Element;
    /**
     * @description 绘制上下文菜单
     * @return {*}
     * @memberof AppMobCalendar
     */
    renderContextMenu(): import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }> | undefined;
    /**
     * @description 绘制日历部件
     * @return {*}
     * @memberof AppMobCalendar
     */
    render(): JSX.Element | null;
}
export declare const AppMobCalendarComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
