import { AppMobMapProps, ICtrlEventParam, IMobMapCtrlController } from 'ibz-core';
import { Ref } from 'vue';
import { MDCtrlComponentBase } from './md-ctrl-component-base';
/**
 * 移动端地图部件
 *
 * @export
 * @class AppMobMap
 * @extends MDCtrlComponentBase
 */
export declare class AppMobMap extends MDCtrlComponentBase<AppMobMapProps> {
    /**
     * 部件控制器
     *
     * @protected
     * @memberof AppMobMap
     */
    protected c: IMobMapCtrlController;
    /**
     * 设置响应式
     *
     * @public
     * @memberof AppMobMap
     */
    setup(): void;
    /**
     * @description 部件初始化
     * @memberof AppMobMap
     */
    init(): void;
    /**
     * @description 执行部件事件
     * @param {ICtrlEventParam} arg 事件参数
     * @memberof AppMobMap
     */
    emitCtrlEvent(arg: ICtrlEventParam): void;
    /**
     * 地图div绑定的id
     *
     * @type {}
     * @memberof AppMobMap
     */
    mapID: string;
    /**
     * 地图对象
     *
     * @type {*}
     * @memberof AppMobMap
     */
    map: any;
    /**
     * 地图信息缓存
     *
     * @memberof AppMobMap
     */
    addressCache: Map<string, any>;
    /**
     * 初始化配置
     *
     * @type {}
     * @memberof AppMobMap
     */
    initOptions: any;
    /**
     * 显示的最大值
     *
     * @type {number}
     * @memberof AppMobMap
     */
    valueMax: number;
    /**
     * 显示图例
     *
     * @type {*}
     * @memberof AppMobMap
     */
    showLegends: any[];
    /**
     * 省份区域数据
     *
     * @memberof AppMobMap
     */
    areaData: any;
    /**
     * 区域样式图数据
     *
     * @memberof AppMobMap
     */
    regionData: any;
    /**
     * 地图数据
     *
     * @type {*}
     * @memberof AppMobMap
     */
    items: Array<any>;
    /**
     * @description 是否打开上下文菜单
     * @type {*}
     * @memberof AppMobMap
     */
    isShowContextMenu: boolean;
    /**
     * @description 长按定时器
     * @type {Ref<any>}
     * @memberof AppMobMap
     */
    touchTimer: Ref<any>;
    /**
     * @description 当前激活数据
     * @type {*}
     * @memberof AppMobMap
     */
    activeData: any;
    /**
     * @description 获取当前时间
     * @type {*}
     * @memberof AppMobMap
     */
    getTimeNow(): number;
    /**
     * 地图点击事件
     *
     * @param data 选中数据
     * @memberof AppMobMap
     */
    onClick(data: any[], event?: any): void;
    /**
     * @description 长按开始
     * @param {*} data 节点数据
     * @param {*} event 事件源
     * @memberof AppMobMap
     */
    onMouseDown(data: any, event: MouseEvent): void;
    /**
     * @description 清除定时器
     * @memberof AppMobMap
     */
    clearTimer(): void;
    /**
     * 注册
     *
     * @param name 地图名称
     * @memberof MobMapCtrlController
     */
    registerMap(): void;
    /**
     * @description 部件挂载完成
     * @param {IParam} arg 事件参数
     * @memberof AppMobMap
     */
    ctrlMounted(args?: any): void;
    /**
     * 初始化地图参数
     *
     * @memberof AppMobMap
     */
    initMapModel(): void;
    /**
     * 更新大小
     *
     * @memberof AppMobMap
     */
    updateSize(): void;
    /**
     * 设置地图数据(初始化)
     *
     * @memberof AppMobMap
     */
    setAreaData(): void;
    /**
     * 省份区域值集合
     *
     * @memberof AppMobMap
     */
    getAreaValueList(): {
        name: string;
        value: number;
    }[];
    /**
     * 计算省份区域数据
     *
     * @memberof AppMobMap
     */
    calculateAreaData(): Promise<void>;
    /**
     * 调用服务，根据经纬度获取地址信息
     *
     * @param {*} lng 经度
     * @param {*} lat 纬度
     * @memberof AppMobMap
     */
    getAddress(lng: any, lat: any): Promise<unknown>;
    /**
     * 计算省份区域数据值(项类容属性)
     *
     * @param name 省份区域名
     * @returns 省份区域值
     * @memberof AppMobMap
     */
    calculateAreaValue(name: string): number;
    /**
     * 配置整合
     *
     * @return {*}
     * @memberof AppMobMap
     */
    handleMapOptions(): void;
    /**
     * 整合序列数据
     *
     * @param {*} seriesData 序列数据
     * @param {*} serie 序列
     * @memberof AppMobMap
     */
    handleSeriesOptions(seriesData: Array<any>, serie: any): void;
    /**
     * 设置配置
     *
     * @memberof AppMobMap
     */
    setOptions(): void;
    /**
     * 绘制悬浮提示
     *
     * @param arg
     */
    renderTooltip(arg: any): string;
    /**
     * 绘制区域图
     *
     * @param params 参数
     * @param api 方法集合
     * @returns
     */
    renderRegion(params: any, api: any): {
        type: string;
        children: any[];
    };
    /**
     * @description 绘制上下文菜单
     * @return {*}
     * @memberof AppMobMap
     */
    renderContextMenu(): import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }> | undefined;
    /**
     * 多数据部件渲染
     *
     * @memberof AppMobMap
     */
    render(): JSX.Element | null;
}
export declare const AppMobMapComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
