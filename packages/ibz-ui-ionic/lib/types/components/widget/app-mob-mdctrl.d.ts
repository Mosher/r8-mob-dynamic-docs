import { Ref } from 'vue';
import { IMobMDCtrlController } from 'ibz-core';
import { AppMobMDCtrlProps } from 'ibz-core';
import { MDCtrlComponentBase } from './md-ctrl-component-base';
export declare class AppMobMDCtrl extends MDCtrlComponentBase<AppMobMDCtrlProps> {
    /**
     * @description 多数据部件控制器
     * @protected
     * @type {IMobMDCtrlController}
     * @memberof AppMobMDCtrl
     */
    protected c: IMobMDCtrlController;
    /**
     * @description 加载更多禁用状态
     * @type {boolean}
     * @memberof AppMobMDCtrl
     */
    loadMoreDisabled: Ref<boolean>;
    /**
     * @description 设置响应式
     * @memberof AppMobMDCtrl
     */
    setup(): void;
    /**
     * @description 初始化响应式属性
     * @memberof AppMobMDCtrl
     */
    initReactive(): void;
    /**
     * @description 初始化数据ref引用
     * @param {any[]} items 数据
     * @memberof AppMobMDCtrl
     */
    initItemRef(items: any[]): void;
    /**
     * @description 拖动
     * @memberof AppMobMDCtrl
     */
    ionDrag(): void;
    /**
     * @description 加载更多
     * @param {*} event 事件源
     * @memberof AppMobMDCtrl
     */
    loadMore(event: any): void;
    /**
     * @description 单选值改变
     * @param {*} $event 数据源
     * @return {*}
     * @memberof AppMobMDCtrl
     */
    simpleChange($event: any): void;
    /**
     * @description 多选值改变
     * @param {*} $event 数据源
     * @return {*}
     * @memberof AppMobMDCtrl
     */
    multipleChange($event: any): void;
    /**
     * @description 列表项左滑右滑触发行为
     * @param {*} $event 点击鼠标事件源
     * @param {*} detail 界面行为模型对象
     * @param {*} item 项数据
     * @memberof AppMobMDCtrl
     */
    mdCtrlClick($event: any, detail: any, item: any): void;
    /**
     * @description 关闭列表项左滑右滑
     * @param {*} item 项数据
     * @memberof AppMobMDCtrl
     */
    closeSlidings(item: any): void;
    /**
     * @description 渲染子项（布局面板）
     * @param {*} item 项数据
     * @return {*}
     * @memberof AppMobMDCtrl
     */
    renderItemForLayoutPanel(item: any): JSX.Element;
    /**
     * @description 渲染子项（默认内容）
     * @param {*} item 项数据
     * @param {*} index 索引
     * @return {*}
     * @memberof AppMobMDCtrl
     */
    renderItemForDefault(item: any, index: any): JSX.Element;
    /**
     * @description 渲染多数据部件项
     * @param {*} item 项数据
     * @param {number} index 索引
     * @return {*}
     * @memberof AppMobMDCtrl
     */
    renderMDCtrlItem(item: any, index: number): JSX.Element;
    /**
     * @description 渲染分组
     * @return {*}
     * @memberof AppMobMDCtrl
     */
    renderHaveGroup(): JSX.Element | JSX.Element[];
    /**
     * @description 渲染无分组
     * @return {*}
     * @memberof AppMobMDCtrl
     */
    renderNoGroup(): JSX.Element | JSX.Element[];
    /**
     * @description 绘制界面行为组
     * @param {*} item 项数据
     * @return {*}
     * @memberof AppMobMDCtrl
     */
    renderListItemAction(item: any): (JSX.Element | null | undefined)[];
    /**
     * @description 绘制左滑界面行为组
     * @param {*} item 项数据
     * @return {*}
     * @memberof AppMobMDCtrl
     */
    renderActionGroup(item: any): JSX.Element | undefined;
    /**
     * @description 绘制右滑界面行为组
     * @param {*} item 项数据
     * @return {*}
     * @memberof AppMobMDCtrl
     */
    renderActionGroup2(item: any): JSX.Element | undefined;
    /**
     * @description 渲染列表视图样式
     * @return {*}
     * @memberof AppMobMDCtrl
     */
    renderMdctrlItemsListView(): JSX.Element;
    /**
     * @description 渲染图标视图样式
     * @return {*}
     * @memberof AppMobMDCtrl
     */
    renderMdctrlItemsIconView(): JSX.Element;
    /**
     * @description 渲染多数据部件主体内容
     * @return {*}
     * @memberof AppMobMDCtrl
     */
    renderMDCtrlContent(): JSX.Element | undefined;
    /**
     * @description 绘制单多选
     * @return {*}
     * @memberof AppMobMDCtrl
     */
    renderSelectMDCtrl(): JSX.Element | JSX.Element[];
    /**
     * @description 绘制单选列表
     * @return {*}
     * @memberof AppMobMDCtrl
     */
    renderSimpleList(): JSX.Element;
    /**
     * @description 绘制多选列表
     * @return {*}
     * @memberof AppMobMDCtrl
     */
    renderMultipleList(): JSX.Element[];
    /**
     * @description 渲染触底加载更多
     * @return {*}
     * @memberof AppMobMDCtrl
     */
    renderBottomRefresh(): JSX.Element;
    /**
     * @description 绘制多数据部件
     * @return {*}
     * @memberof AppMobMDCtrl
     */
    render(): JSX.Element | (import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }> | undefined)[] | null;
}
export declare const AppMobMDCtrlComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
