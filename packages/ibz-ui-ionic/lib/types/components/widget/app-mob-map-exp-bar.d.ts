import { AppMobMapExpBarProps, IMobMapExpBarController } from 'ibz-core';
import { ExpBarCtrlComponentBase } from './exp-bar-ctrl-component-base';
/**
 * @description 移动端地图导航部件
 * @export
 * @class AppMobMapExpBar
 * @extends {DEViewComponentBase<AppMobMapExpBarProps>}
 */
export declare class AppMobMapExpBar extends ExpBarCtrlComponentBase<AppMobMapExpBarProps> {
    /**
     * 部件控制器
     *
     * @protected
     * @memberof AppMobMapExpBar
     */
    protected c: IMobMapExpBarController;
    /**
     * 设置响应式
     *
     * @public
     * @memberof AppMobMapExpBar
     */
    setup(): void;
    /**
     * @description 渲染导航栏部件
     * @return {*}
     * @memberof AppMobChartExpBar
     */
    render(): JSX.Element | null;
}
export declare const AppMobMapExpBarComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
