import { IMobSearchFormCtrlController } from 'ibz-core';
import { AppMobSearchFormProps } from 'ibz-core';
import { AppMobForm } from './app-mob-form/app-mob-form';
/**
 * 移动端搜索表单
 *
 * @author chitanda
 * @date 2021-08-06 10:08:39
 * @export
 * @class AppMobSearchForm
 * @extends {ComponentBase}
 */
export declare class AppMobSearchForm extends AppMobForm<AppMobSearchFormProps> {
    /**
     * @description 搜索表单控制器
     * @protected
     * @type {IMobSearchFormCtrlController}
     * @memberof AppMobSearchForm
     */
    protected c: IMobSearchFormCtrlController;
    /**
     * @description 搜索表单uuid
     * @type {string}
     * @memberof AppMobSearchForm
     */
    uuid: string;
    /**
     * @description 设置响应式
     * @memberof AppMobSearchForm
     */
    setup(): void;
    /**
     * @description 初始化响应数据
     * @memberof AppMobSearchForm
     */
    initReactive(): void;
    /**
     * @description 重置
     * @memberof AppMobSearchForm
     */
    onReset(): void;
    /**
     * @description 搜索
     * @memberof AppMobSearchForm
     */
    onSearch(): void;
    /**
     * @description 打开搜索表单
     * @memberof AppMobSearchForm
     */
    openSearchForm(): Promise<void>;
    /**
     * @description 关闭搜索表单
     * @memberof AppMobSearchForm
     */
    closeSearchForm(): void;
    /**
     * @description 根据成员类型绘制
     * @param {*} modelJson 模型数据
     * @param {number} index 索引
     * @return {*}
     * @memberof AppMobSearchForm
     */
    renderByDetailType(modelJson: any, index: number): any;
    /**
     * @description 绘制搜索表单
     * @param {*} className 类名
     * @param {*} controlStyle 样式
     * @return {*}
     * @memberof AppMobSearchForm
     */
    renderSearchForm(className: any, controlStyle: any): JSX.Element[];
    /**
     * @description 绘制快速搜索表单
     * @param {*} className 类名
     * @param {*} controlStyle 样式
     * @memberof AppMobSearchForm
     */
    renderQuickSearchForm(className: any, controlStyle: any): JSX.Element;
    /**
     * @description 绘制搜索表单内容
     * @return {*}
     * @memberof AppMobSearchForm
     */
    renderSearchFormContent(): JSX.Element | JSX.Element[];
    /**
     * @description 绘制
     * @return {*}
     * @memberof AppMobSearchForm
     */
    render(): JSX.Element | JSX.Element[] | null;
}
export declare const AppMobSearchFormComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
