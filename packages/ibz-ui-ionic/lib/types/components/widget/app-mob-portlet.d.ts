import { Ref } from 'vue';
import { AppMobPortletProps, IMobPortletController, IParam } from 'ibz-core';
import { CtrlComponentBase } from './ctrl-component-base';
/**
 * 移动端门户部件
 *
 * @class AppMobPortlet
 */
export declare class AppMobPortlet extends CtrlComponentBase<AppMobPortletProps> {
    /**
     * 移动端门户部件控制器实例对象
     *
     * @type {IMobPortletController}
     * @memberof AppMobPortlet
     */
    protected c: IMobPortletController;
    /**
     * UUID
     *
     * @type {string}
     * @memberof AppMobPortlet
     */
    private uuid;
    /**
     * 操作按钮集合
     *
     * @type {Ref<IParam[]}
     * @memberof AppMobPortlet
     */
    actionSheetButtons: Ref<IParam>;
    /**
     * 行为操作显示状态
     *
     * @type {Ref<IParam[]}
     * @memberof AppMobPortlet
     */
    actionSheetShowStatus: Ref<boolean>;
    /**
     * 设置响应式
     *
     * @memberof AppMobPortlet
     */
    setup(): void;
    /**
     * 获取门户部件大小
     * @memberof AppMobPortlet
     */
    getSize(): IParam;
    /**
     * 打开动作面板
     *
     * @param event 源事件对象
     * @memberof AppMobPortlet
     */
    openActionSheet(event: any): void;
    /**
     * 操作栏按钮点击
     *
     * @param {string} tag 标识
     * @param {*} event 源事件对象
     * @memberof AppMobPortlet
     */
    onActionBarItemClick(tag: string, event?: any): void;
    /**
     * 渲染部件
     *
     * @memberof AppMobPortlet
     */
    renderControl(): import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }>[] | undefined;
    /**
     * 渲染直接内容
     *
     * @memberof AppMobPortlet
     */
    renderRawItem(): any;
    /**
     * 渲染HTML内容
     *
     * @memberof AppMobPortlet
     */
    renderHtml(): JSX.Element;
    /**
     * 渲染工具栏
     *
     * @memberof AppMobPortlet
     */
    renderToolbar(): JSX.Element;
    /**
     * 渲染操作栏
     *
     * @memberof AppMobPortlet
     */
    renderActionBar(): import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }>;
    /**
     * 渲染自定义内容
     *
     * @memberof AppMobPortlet
     */
    renderCustom(): JSX.Element;
    /**
     * 渲染应用菜单
     *
     * @memberof AppMobPortlet
     */
    renderAppMenu(): import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }> | undefined;
    /**
     * 渲染视图
     *
     * @memberof AppMobPortlet
     */
    renderView(): JSX.Element;
    /**
     * 根据门户部件类型渲染内容
     *
     * @memberof AppMobPortlet
     */
    renderByPortletType(): any;
    /**
     * 渲染标题
     *
     * @memberof AppMobPortlet
     */
    renderTitle(): JSX.Element | undefined;
    /**
     * 渲染
     *
     * @memberof AppMobPortlet
     */
    render(): JSX.Element | undefined;
}
export declare const AppMobPortletComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
