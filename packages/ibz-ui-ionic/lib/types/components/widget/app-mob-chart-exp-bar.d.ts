import { AppMobChartExpBarProps, IMobChartExpBarController } from 'ibz-core';
import { ExpBarCtrlComponentBase } from './exp-bar-ctrl-component-base';
/**
 * @description 移动端图表导航栏
 * @export
 * @class AppMobChartExpBar
 * @extends {DEViewComponentBase<AppMobChartExpBarProps>}
 */
export declare class AppMobChartExpBar extends ExpBarCtrlComponentBase<AppMobChartExpBarProps> {
    /**
     * 部件控制器
     *
     * @protected
     * @memberof AppMobChartExpBar
     */
    protected c: IMobChartExpBarController;
    /**
     * 设置响应式
     *
     * @public
     * @memberof AppMobChartExpBar
     */
    setup(): void;
    /**
     * @description 渲染导航栏部件
     * @return {*}
     * @memberof AppMobChartExpBar
     */
    render(): JSX.Element | null;
}
export declare const AppMobChartExpBarComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
