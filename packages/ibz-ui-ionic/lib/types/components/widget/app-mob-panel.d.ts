import { IPSEditor, IPSPanelButton, IPSPanelContainer, IPSPanelField, IPSPanelItem, IPSPanelRawItem, IPSPanelTabPage, IPSPanelTabPanel } from '@ibiz/dynamic-model-api';
import { IMobPanelController, IParam } from 'ibz-core';
import { AppMobPanelProps } from 'ibz-core';
import { CtrlComponentBase } from './ctrl-component-base';
export declare class AppMobPanel extends CtrlComponentBase<AppMobPanelProps> {
    /**
     * @description 部件控制器
     * @protected
     * @type {IMobPanelController}
     * @memberof AppMobPanel
     */
    protected c: IMobPanelController;
    /**
     * @description 当前选中分页
     * @type {*}
     * @memberof AppMobPanel
     */
    currentTab: any;
    /**
     * @description 设置响应式
     * @memberof AppMobPanel
     */
    setup(): void;
    /**
     * @description 初始化响应式属性
     * @memberof AppMobPanel
     */
    initReactive(): void;
    /**
     * @description 获取部件样式名
     * @return {*}
     * @memberof AppMobPanel
     */
    getCtrlClassNames(): {
        [x: string]: boolean;
    };
    /**
     * 计算容器尺寸
     *
     * @param {number} value
     * @return {*}  {string}
     */
    calcBoxSize(value: number, enableRem?: boolean): string;
    /**
     * @description 获取顶级容器样式
     * @return {*}
     * @memberof AppMobPanel
     */
    getRootStyle(): {};
    /**
     * @description 获取面板项样式名
     * @param {IPSPanelItem} item 面板项
     * @return {*}
     * @memberof AppMobPanel
     */
    getDetailClass(item: IPSPanelItem): {
        [x: string]: boolean;
    };
    /**
     * @description 面板分页栏切换
     * @param {string} key 分页栏标识
     * @param {*} event 源事件对象
     * @memberof AppMobPanel
     */
    segmentChanged(key: string, event: any): void;
    /**
     * @description 面板按钮点击
     * @param {IParam} detail 数据
     * @param {*} event 源事件对象
     * @memberof AppMobPanel
     */
    buttonClick(detail: IParam, event: any): void;
    /**
     * @description 渲染面板按钮
     * @param {IPSPanelButton} modelJson 按钮模型数据
     * @param {number} index 序列标识
     * @return {*}
     * @memberof AppMobPanel
     */
    renderButton(modelJson: IPSPanelButton, index: number): any;
    /**
     * @description 渲染编辑器
     * @param {IPSEditor} editor 编辑器实例对象
     * @param {IPSPanelItem} parentItem 面板项对象
     * @return {*}
     * @memberof AppMobPanel
     */
    renderEditor(editor: IPSEditor, parentItem: IPSPanelItem): any;
    /**
     * @description 渲染面板属性
     * @param {IPSPanelField} modelJson 面板属性对象
     * @param {number} index 序列标识
     * @return {*}
     * @memberof AppMobPanel
     */
    renderField(modelJson: IPSPanelField, index: number): any;
    /**
     * @description 渲染直接内容
     * @param {IPSPanelButton} modelJson 直接内容模型数据
     * @param {number} index 序列标识
     * @return {*}
     * @memberof AppMobPanel
     */
    renderRawItem(modelJson: IPSPanelRawItem, index: number): any;
    /**
     * @description 渲染分页部件
     * @param {IPSPanelButton} modelJson 分页部件模型数据
     * @param {number} index 序列标识
     * @return {*}
     * @memberof AppMobPanel
     */
    renderTabPage(modelJson: IPSPanelTabPage, index: number): any;
    /**
     * @description 渲染分页面板
     * @param {IPSPanelButton} modelJson 分页面板模型数据
     * @param {number} index 序列标识
     * @return {*}
     * @memberof AppMobPanel
     */
    renderTabPanel(modelJson: IPSPanelTabPanel, index: number): any;
    /**
     * @description 渲染面板容器
     * @param {IPSPanelButton} modelJson 面板容器模型数据
     * @param {number} index 序列标识
     * @return {*}
     * @memberof AppMobPanel
     */
    renderContainer(modelJson: IPSPanelContainer, index: number): any;
    /**
     * @description 根据类型渲染面板项
     * @param {*} modelJson 面板项对象
     * @param {number} index 序列标识
     * @return {*}
     * @memberof AppMobPanel
     */
    renderByDetailType(modelJson: any, index: number): any;
    /**
     * @description 渲染面板部件根节点
     * @return {*}
     * @memberof AppMobPanel
     */
    renderRootPSPanelItems(): any;
    /**
     * @description 渲染面板部件
     * @return {*}
     * @memberof AppMobPanel
     */
    render(): any;
}
export declare const AppMobPanelComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
