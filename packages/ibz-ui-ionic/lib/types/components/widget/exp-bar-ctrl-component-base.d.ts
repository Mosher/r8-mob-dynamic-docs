import { AppMobListExpBarProps, IAppExpBarCtrlController } from 'ibz-core';
import { CtrlComponentBase } from './ctrl-component-base';
export declare class ExpBarCtrlComponentBase<Props extends AppMobListExpBarProps> extends CtrlComponentBase<AppMobListExpBarProps> {
    /**
     * 部件控制器
     *
     * @protected
     * @memberof ExpBarCtrlComponentBase
     */
    protected c: IAppExpBarCtrlController;
    /**
     * @description 初始化响应式属性
     * @memberof ExpBarCtrlComponentBase
     */
    initReactive(): void;
    /**
     * @description 渲染多数据部件
     * @return {*}
     * @memberof ExpBarCtrlComponentBase
     */
    renderXDataControl(): import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }> | undefined;
    /**
     * @description 渲染导航视图
     * @return {*}
     * @memberof ExpBarCtrlComponentBase
     */
    renderNavView(): import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }> | undefined;
    /**
     * @description 渲染导航栏部件
     * @return {*}
     * @memberof ExpBarCtrlComponentBase
     */
    render(): JSX.Element | null;
}
