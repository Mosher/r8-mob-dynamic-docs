import { Ref } from 'vue';
import { IPSDETreeNode } from '@ibiz/dynamic-model-api';
import { AppMobTreeProps, IParam, IMobTreeCtrlController } from 'ibz-core';
import { MDCtrlComponentBase } from './md-ctrl-component-base';
/**
 * 移动端树部件
 *
 * @export
 * @class AppMobTree
 * @extends MDCtrlComponentBase
 */
export declare class AppMobTree extends MDCtrlComponentBase<AppMobTreeProps> {
    /**
     * @description 树视图部件控制器
     * @protected
     * @type {IMobTreeCtrlController}
     * @memberof AppMobTree
     */
    protected c: IMobTreeCtrlController;
    /**
     * @description 长按定时器
     * @type {Ref<any>}
     * @memberof AppMobTree
     */
    touchTimer: Ref<any>;
    /**
     * @description 当前激活节点
     * @type {*}
     * @memberof AppMobTree
     */
    activeNode: any;
    /**
     * @description 是否打开上下文菜单
     * @type {*}
     * @memberof AppMobTree
     */
    isShowContextMenu: boolean;
    /**
     * @description 初始化响应式
     * @memberof AppMobTree
     */
    setup(): void;
    /**
     * @description 根据类型加载节点模型
     * @param {string} type
     * @return {*}  {(IPSDETreeNode | undefined)}
     * @memberof AppMobTree
     */
    getNodeByType(type: string): IPSDETreeNode | undefined;
    /**
     * @description 清除定时器
     * @memberof AppMobTree
     */
    clearTimer(): void;
    /**
     * @description 长按开始
     * @param {*} item 节点数据
     * @param {*} event 事件源
     * @memberof AppMobTree
     */
    onTouchStart(item: any, event: MouseEvent): void;
    /**
     * @description 处理树节点点击事件
     * @param {IParam} item 树节点数据
     * @param {IPSDETreeNode} node 树节点模型
     * @param {*} event 源事件对象
     * @memberof AppMobTree
     */
    handleTreeNodeClick(item: IParam, node: IPSDETreeNode, event: any): void;
    /**
     * @description 单选选中数据变化
     * @param {IParam} item 节点数据
     * @param {MouseEvent} event 事件源
     * @memberof AppMobTree
     */
    treeNodeSelect(item: IParam, event: MouseEvent): void;
    /**
     * @description 多项选择选中数据
     * @param {*} event 数据源
     * @memberof AppMobTree
     */
    treeNodeChecked(item: IParam, event: any): void;
    /**
     * @description 树节点点击
     * @param {IParam} item 节点数据
     * @param {MouseEvent} event 事件源
     * @memberof AppMobTree
     */
    loadChildNodeData(item: IParam, event: MouseEvent): void;
    /**
     * @description 渲染单选树
     * @return {*}
     * @memberof AppMobTree
     */
    renderSingleTree(): JSX.Element;
    /**
     * @description 渲染多选树
     * @return {*}
     * @memberof AppMobTree
     */
    renderMultipleTree(): JSX.Element;
    /**
     * @description 渲染默认树
     * @return {*}
     * @memberof AppMobTree
     */
    renderDefaultTree(): JSX.Element;
    /**
     * @description 渲染树主体内容
     * @return {*}
     * @memberof AppMobTree
     */
    renderTreeMainContent(): JSX.Element;
    /**
     * @description
     * @param {IParam} node
     * @return {*}
     * @memberof AppMobTree
     */
    /**
     * @description 渲染树节点标题
     * @param {IParam} item 树节点数据
     * @param {boolean} [hasChildren] 是否有子节点
     * @return {*}
     * @memberof AppMobTree
     */
    renderNodeLabel(item: IParam, hasChildren?: boolean): JSX.Element;
    /**
     * @description 渲染树节点
     * @param {IPSDETreeNode} node
     * @param {IParam} item
     * @return {*}
     * @memberof AppMobTree
     */
    renderTreeNode(node: IPSDETreeNode, item: IParam): JSX.Element;
    /**
     * @description 渲染上下文菜单
     * @return {*}
     * @memberof AppMobTree
     */
    renderContextMenu(): import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }> | undefined;
    /**
     * @description 渲染树部件
     * @return {*}
     * @memberof AppMobTree
     */
    render(): JSX.Element | undefined;
}
export declare const AppMobTreeComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
