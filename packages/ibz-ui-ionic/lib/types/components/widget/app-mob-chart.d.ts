import { AppMobChartProps, ICtrlEventParam, IMobChartCtrlController } from 'ibz-core';
import { MDCtrlComponentBase } from './md-ctrl-component-base';
/**
 * 图表部件
 *
 * @export
 * @class AppMobChart
 * @extends {MDCtrlComponentBase}
 */
export declare class AppMobChart extends MDCtrlComponentBase<AppMobChartProps> {
    /**
     * @description 图表部件控制器
     * @protected
     * @type {IMobChartCtrlController}
     * @memberof AppMobChart
     */
    protected c: IMobChartCtrlController;
    /**
     * @description 设置响应式
     * @memberof AppMobChart
     */
    setup(): void;
    /**
     * @description 部件基础数据初始化
     * @memberof MobChartCtrlController
     */
    init(): void;
    /**
     * @description 执行部件事件
     * @param {ICtrlEventParam} arg 事件参数
     * @memberof AppMobMap
     */
    emitCtrlEvent(arg: ICtrlEventParam): void;
    /**
     * @description 初始化序列模型
     * @private
     * @return {*}
     * @memberof MobChartCtrlController
     */
    private initSeriesModel;
    /**
     * @description 填充序列模型数据
     * @private
     * @param {*} opts 序列参数
     * @param {*} series 序列
     * @memberof MobChartCtrlController
     */
    private initChartSeries;
    /**
     * @description 初始化图表参数
     * @private
     * @memberof MobChartCtrlController
     */
    private initChartOption;
    /**
     * @description 初始化图表用户自定义参数
     * @private
     * @memberof MobChartCtrlController
     */
    private initChartUserParams;
    /**
     * @description 获取序列模型参数
     * @private
     * @param {*} series 序列
     * @return {*}  {Promise<IParam>}
     * @memberof MobChartCtrlController
     */
    private getSeriesModelParam;
    /**
     * @description 获取序列dataset属性
     * @private
     * @param {*} series 序列
     * @return {*}
     * @memberof MobChartCtrlController
     */
    private getDataSetFields;
    /**
     * @description 填充标题配置
     * @private
     * @param {*} opts 图表参数
     * @memberof MobChartCtrlController
     */
    private fillTitleOption;
    /**
     * @description 填充图例配置
     * @private
     * @param {*} opts 图表参数
     * @memberof MobChartCtrlController
     */
    private fillLegendOption;
    /**
     * @description 注册地图
     * @private
     * @memberof MobChartCtrlController
     */
    private registerMap;
    /**
     * @description 处理用户自定义参数
     * @private
     * @param {*} param 用户参数
     * @param {*} opts 绘制参数
     * @param {string} tag 标识
     * @return {*}
     * @memberof MobChartCtrlController
     */
    private fillUserParam;
    /**
     * @description 填充序列
     * @private
     * @param {*} series 序列
     * @param {*} [indicator=[]] 雷达图参数
     * @return {*}
     * @memberof MobChartCtrlController
     */
    private fillSeries;
    /**
     * @description 填充坐标
     * @private
     * @param {*} axis 坐标
     * @return {*}  {IParam}
     * @memberof MobChartCtrlController
     */
    private fillAxis;
    /**
     * @description 绘制图表
     * @private
     * @memberof MobChartCtrlController
     */
    private drawCharts;
    /**
     * @description 图标事件
     * @memberof MobChartCtrlController
     */
    onChartEvents(): void;
    /**
     * @description 处理默认选中
     * @private
     * @memberof MobChartCtrlController
     */
    private handleDefaultSelect;
    /**
     * @description 图表单击事件
     * @private
     * @param {*} event 数据源
     * @return {*}
     * @memberof MobChartCtrlController
     */
    private onChartClick;
    /**
     * @description 处理图表参数
     * @private
     * @return {*}  {IParam}
     * @memberof MobChartCtrlController
     */
    private handleChartOPtion;
    /**
     * @description 实体数据集转化为图表数据集
     *  1.获取图表所有代码表值
     *  2.查询集合映射图表数据集
     *  3.补全图表数据集
     *  4.图表数据集分组求和
     *  5.排序图表数据集
     * @private
     * @param {*} data 实体数据集
     * @param {Function} callback 回调
     * @return {*}
     * @memberof MobChartCtrlController
     */
    private transformToBasicChartSetData;
    /**
     * @description 构建图表序列数据集合
     *  1.分组求和
     *  2.排序求和数组
     * @private
     * @param {*} data 传入数据
     * @param {*} item 单个序列
     * @param {Function} callback 回调
     * @param {*} allCodeList 所有代码表
     * @memberof MobChartCtrlController
     */
    private transformToChartSeriesDataSet;
    /**
     * @description 分组和求和
     * @private
     * @param {Array<any>} groupField 分组属性
     * @param {Array<any>} seriesField 序列
     * @param {Array<any>} valueField 值属性
     * @param {*} data 传入数据
     * @param {*} item 项数据
     * @param {*} groupFieldModel 分组属性模型
     * @param {*} allCodeList 所有代码表
     * @return {*} {IParam[]}
     * @memberof MobChartCtrlController
     */
    private groupAndAdd;
    /**
     * @description 排序数组
     * @private
     * @param {Array<any>} arr 传入数组
     * @param {*} groupField 分组属性
     * @param {*} allCodeList 所有代码表
     * @return {*}  {IParam[]}
     * @memberof MobChartCtrlController
     */
    private sortReturnArray;
    /**
     * @description 排序分组模式下的数据
     * @private
     * @param {Array<any>} arr 传入数据
     * @param {*} groupField 分组属性
     * @param {string} label label标签
     * @return {*}  {IParam[]}
     * @memberof MobChartCtrlController
     */
    private handleSortGroupData;
    /**
     * @description 补全数据集
     * @private
     * @param {*} data 传入数据
     * @param {*} item 单个序列
     * @param {*} allCodeList 所有的代码表
     * @return {*}
     * @memberof MobChartCtrlController
     */
    private completeDataSet;
    /**
     * @description 获取最大值最小值
     * @private
     * @param {Array<any>} tempTimeArray 传入数组
     * @return {*} {IParam[]}
     * @memberof MobChartCtrlController
     */
    private getRangeData;
    /**
     * @description 补全时间类型数据集
     * @private
     * @param {*} data 传入数据
     * @param {*} item 单个序列
     * @param {*} allCodeList 所有的代码表
     * @param {*} groupField 分组属性
     * @memberof MobChartCtrlController
     */
    private handleTimeData;
    /**
     * @description 补全代码表
     * @private
     * @param {*} data 传入数据
     * @param {*} item 单个序列
     * @param {*} allCodeList 所有的代码表
     * @return {*}
     * @memberof MobChartCtrlController
     */
    private completeCodeList;
    /**
     * @description 处理单个属性
     * @private
     * @param {*} input 输入值
     * @param {*} field 属性值
     * @param {*} allCodeList 所有代码表
     * @param {*} result 结果值
     * @param {*} groupField 分组属性
     * @memberof MobChartCtrlController
     */
    private handleSingleDataSetField;
    /**
     * @description 获取图表所需代码表
     * @private
     * @return {*}  {Promise<IParam>}
     * @memberof MobChartCtrlController
     */
    private getChartAllCodeList;
    /**
     * @description 获取代码表
     * @private
     * @param {*} codeListObject 代码表对象
     * @return {*}  {Promise<IParam>}
     * @memberof MobChartCtrlController
     */
    private getCodeList;
    /**
     * @description 数组元素小写
     * @private
     * @param {*} arr 数组
     * @return {*} {IParam[]}
     * @memberof MobChartCtrlController
     */
    private arrayToLowerCase;
    /**
     * @description 计算数据集最大数
     * @private
     * @param {any[]} source 传入数据
     * @return {*} {number}
     * @memberof MobChartCtrlController
     */
    private calcSourceMaxValue;
    /**
     * @description 是否为数组字符串
     * @private
     * @param {string} str 字符串
     * @return {*}  {boolean}
     * @memberof MobChartCtrlController
     */
    private isArray;
    /**
     * @description 是否为json字符串
     * @private
     * @param {*} str 字符串
     * @return {*}  {boolean}
     * @memberof MobChartCtrlController
     */
    private isJson;
    /**
     * @description 解析字符串函数
     * @private
     * @param {*} data 传入数据
     * @return {*}
     * @memberof MobChartCtrlController
     */
    private deepJsonParseFun;
    /**
     * @description 整合图表数据集data
     * @private
     * @param {any[]} source 传入数据
     * @param {string} series 序列
     * @param {any[]} [indicator] 雷达图参数
     * @return {*}
     * @memberof MobChartCtrlController
     */
    /**
     *
     * @description 整合图表数据集data
     * @private
     * @param {*} series 序列
     * @param {any[]} source 传入数据
     * @param {string} type 类型
     * @param {any[]} [indicator] 雷达图参数
     * @return {*}  {*}
     * @memberof AppMobChart
     */
    private transformToChartSeriesData;
    /**
     * @description 绘制图表
     * @return {*}
     * @memberof AppMobChart
     */
    render(): JSX.Element | null;
}
export declare const AppMobChartComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
