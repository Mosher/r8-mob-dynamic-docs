import { IPSAppDEView } from '@ibiz/dynamic-model-api';
import { AppMobTabViewPanelProps, IMobTabViewPanelCtrlController } from 'ibz-core';
import { CtrlComponentBase } from './ctrl-component-base';
/**
 * 移动端分页视图面板部件
 *
 * @export
 * @class AppMobTabViewPanel
 * @extends CtrlComponentBase
 */
export declare class AppMobTabViewPanel extends CtrlComponentBase<AppMobTabViewPanelProps> {
    /**
     * @description 部件控制器
     * @protected
     * @type {IMobTabViewPanelCtrlController}
     * @memberof AppMobTabViewPanel
     */
    protected c: IMobTabViewPanelCtrlController;
    /**
     * @description 嵌入视图组件
     * @type {string}
     * @memberof AppMobTabViewPanel
     */
    viewComponent: string;
    /**
     * @description 设置响应式
     * @memberof AppMobTabViewPanel
     */
    setup(): void;
    /**
     * @description 初始化视图组件名称
     * @param {IPSAppDEView} embeddedView
     * @memberof AppMobTabViewPanel
     */
    getViewComponent(embeddedView: IPSAppDEView): void;
    /**
     * @description 绘制关系视图
     * @public
     * @memberof AppMobTabViewPanel
     */
    renderEmbedView(): import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }> | null;
    /**
     * @description 分页视图面板部件渲染
     * @return {*}
     * @memberof AppMobTabViewPanel
     */
    render(): JSX.Element | null;
}
export declare const AppMobTabViewPanelComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
