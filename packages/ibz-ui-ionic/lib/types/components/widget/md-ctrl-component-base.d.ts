import { AppMDCtrlProps, IAppMDCtrlController } from 'ibz-core';
import { CtrlComponentBase } from './ctrl-component-base';
/**
 * 多数据部件组件基类
 *
 * @export
 * @class MDCtrlComponentBase
 */
export declare class MDCtrlComponentBase<Props extends AppMDCtrlProps> extends CtrlComponentBase<AppMDCtrlProps> {
    /**
     * @description 多数据部件控制器
     * @protected
     * @type {IAppMDCtrlController}
     * @memberof MDCtrlComponentBase
     */
    protected c: IAppMDCtrlController;
    /**
     * @description 初始化响应式属性
     * @memberof MDCtrlComponentBase
     */
    initReactive(): void;
    /**
     * @description 绘制快速操作栏
     * @memberof MDCtrlComponentBase
     */
    renderQuickToolbar(): import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }> | undefined;
    /**
     * @description 绘制批操作工具栏
     * @memberof MDCtrlComponentBase
     */
    renderBatchToolbar(): import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }> | undefined;
}
