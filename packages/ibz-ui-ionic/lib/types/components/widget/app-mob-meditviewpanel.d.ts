import { MDCtrlComponentBase } from './md-ctrl-component-base';
import { AppMobMEditViewPanelProps, IMobMEditViewPanelCtrlController } from 'ibz-core';
export declare class AppMobMEditViewPanel extends MDCtrlComponentBase<AppMobMEditViewPanelProps> {
    /**
     * @description 多表单编辑视图面板部件控制器
     * @protected
     * @type {IMobMEditViewPanelCtrlController}
     * @memberof AppMobMEditViewPanel
     */
    protected c: IMobMEditViewPanelCtrlController;
    /**
     * @description 激活项
     * @private
     * @type {Ref<string>}
     * @memberof AppMobMEditViewPanel
     */
    private activeItem;
    /**
     * @description 设置响应式
     * @memberof AppMobMEditViewPanel
     */
    setup(): void;
    /**
     * @description 分页节点切换
     * @private
     * @param {*} $event 数据源
     * @return {*}
     * @memberof AppMobMEditViewPanel
     */
    private ionChange;
    /**
     * @description 处理视图事件
     * @private
     * @param {string} viewname 视图名
     * @param {string} action 行为
     * @param {*} data 数据
     * @memberof AppMobMEditViewPanel
     */
    private handleViewEvent;
    /**
     * @description 绘制嵌入视图
     * @param {*} item 项数据
     * @return {*}
     * @memberof AppMobMEditViewPanel
     */
    renderEmbedView(item: any): import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }>;
    /**
     * @description 绘制行记录样式
     * @return {*}
     * @memberof AppMobMEditViewPanel
     */
    renderRow(): JSX.Element[][];
    /**
     * @description 绘制上分页样式
     * @return {*}
     * @memberof AppMobMEditViewPanel
     */
    renderTabtop(): JSX.Element;
    /**
     * @description 绘制分页头
     * @return {*}
     * @memberof AppMobMEditViewPanel
     */
    renderPageHead(): JSX.Element;
    /**
     * @description 绘制上分页内容区
     * @return {*}
     * @memberof AppMobMEditViewPanel
     */
    renderTabTopContent(): JSX.Element;
    /**
     * @description 绘制内容区
     * @return {*}
     * @memberof AppMobMEditViewPanel
     */
    renderContent(): JSX.Element | JSX.Element[][] | undefined;
    /**
     * @description 绘制多表单编辑面板
     * @return {*}
     * @memberof AppMobMEditViewPanel
     */
    render(): JSX.Element | null;
}
export declare const AppMobMEditViewPanelComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
