import { IPSDEEditForm } from '@ibiz/dynamic-model-api';
import { AppMobWizardPanelProps, IMobWizardPanelController } from 'ibz-core';
import { CtrlComponentBase } from './ctrl-component-base';
/**
 * @description 移动端向导面板部件
 * @export
 * @class AppMobWizardPanel
 * @extends {DEViewComponentBase<AppMobWizardPanelProps>}
 */
export declare class AppMobWizardPanel extends CtrlComponentBase<AppMobWizardPanelProps> {
    /**
     * @description 部件控制器
     * @protected
     * @type {IMobWizardPanelController}
     * @memberof AppMobWizardPanel
     */
    protected c: IMobWizardPanelController;
    /**
     * @description 设置响应式
     * @memberof AppMobWizardPanel
     */
    setup(): void;
    /**
     * @description 获取激活步骤表单实例对象
     * @protected
     * @param {string} name
     * @return {*}  {(IPSDEEditForm | undefined)}
     * @memberof AppMobWizardPanel
     */
    protected getActiveFormInstance(name: string): IPSDEEditForm | undefined;
    /**
     * @description 获取激活步骤
     * @protected
     * @return {*}  {number}
     * @memberof AppMobWizardPanel
     */
    protected getActiveStep(): number;
    /**
     * @description 按钮显示状态
     * @protected
     * @param {string} type 按钮步骤类型
     * @return {*}  {boolean}
     * @memberof AppMobWizardPanel
     */
    protected buttonStatus(type: string): boolean;
    /**
     * @description 上一步
     * @protected
     * @param {*} event 源事件对象
     * @memberof AppMobWizardPanel
     */
    protected onClickPrev(event: any): void;
    /**
     * @description 下一步
     * @protected
     * @param {*} event 源事件对象
     * @memberof AppMobWizardPanel
     */
    protected onClickNext(event: any): void;
    /**
     * @description 完成
     * @protected
     * @param {*} event 源事件对象
     * @memberof AppMobWizardPanel
     */
    protected onClickFinish(event: any): void;
    /**
     * @description 渲染向导步骤表单
     * @memberof AppMobWizardPanel
     */
    renderWizardStepForm(): import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }> | undefined;
    /**
     * @description 渲染向导面板组件
     * @return {*}
     * @memberof AppMobWizardPanel
     */
    render(): JSX.Element | undefined;
}
export declare const AppMobWizardPanelComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
