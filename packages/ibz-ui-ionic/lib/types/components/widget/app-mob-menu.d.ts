import { IPSAppMenuItem } from '@ibiz/dynamic-model-api';
import { IMobMenuCtrlController } from 'ibz-core';
import { AppMobMenuProps } from 'ibz-core';
import { CtrlComponentBase } from './ctrl-component-base';
/**
 * 应用菜单
 *
 * @export
 * @class AppMobMenu
 * @extends {ComponentBase}
 */
export declare class AppMobMenu extends CtrlComponentBase<AppMobMenuProps> {
    /**
     * @description 部件控制器
     * @protected
     * @type {IMobMenuCtrlController}
     * @memberof AppMobMenu
     */
    protected c: IMobMenuCtrlController;
    /**
     * @description 菜单项集合
     * @type {IPSAppMenuItem[]}
     * @memberof AppMobMenu
     */
    menuItems: IPSAppMenuItem[];
    /**
     * @description 设置响应式
     * @memberof AppMobMenu
     */
    setup(): void;
    /**
     * @description 部件初始化
     * @memberof AppMobMenu
     */
    init(): void;
    /**
     * @description 设置默认选中菜单
     * @return {*}
     * @memberof AppMobMenu
     */
    setDefaultSelectMenu(): null | undefined;
    /**
     * @description 获取计数器计数
     * @param {IPSAppMenuItem} item
     * @return {*}
     * @memberof AppMobMenu
     */
    getCounter(item: IPSAppMenuItem): any;
    /**
     * @description 根据菜单项获取菜单权限
     * @param {IPSAppMenuItem} menuItem
     * @return {*}
     * @memberof AppMobMenu
     */
    getMenusPermission(menuItem: IPSAppMenuItem): any;
    /**
     * @description 菜单项点击
     * @param {IPSAppMenuItem} item
     * @memberof AppMobMenu
     */
    menuItemClick(item: IPSAppMenuItem, isGlobal: boolean): Promise<void>;
    /**
     * @description 绘制菜单项
     * @param {IPSAppMenuItem} item
     * @return {*}
     * @memberof AppMobMenu
     */
    renderMenuItem(item: IPSAppMenuItem): JSX.Element;
    /**
     * @description 绘制快速菜单项
     * @return {*}
     * @memberof AppMobMenu
     */
    renderQuickMenu(): JSX.Element;
    /**
     * @description 绘制内容
     * @return {*}
     * @memberof AppMobMenu
     */
    render(): JSX.Element | null | undefined;
}
export declare const AppMobMenuComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
