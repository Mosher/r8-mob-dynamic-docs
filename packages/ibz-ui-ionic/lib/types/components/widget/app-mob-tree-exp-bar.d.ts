import { AppMobTreeExpBarProps, IMobTreeExpBarController } from 'ibz-core';
import { ExpBarCtrlComponentBase } from './exp-bar-ctrl-component-base';
/**
 * @description 移动端树导航部件
 * @export
 * @class AppMobTreeExpBar
 * @extends {DEViewComponentBase<AppMobTreeExpBarProps>}
 */
export declare class AppMobTreeExpBar extends ExpBarCtrlComponentBase<AppMobTreeExpBarProps> {
    /**
     * 部件控制器
     *
     * @protected
     * @memberof AppMobTreeExpBar
     */
    protected c: IMobTreeExpBarController;
    /**
     * 设置响应式
     *
     * @public
     * @memberof AppMobTreeExpBar
     */
    setup(): void;
    /**
     * @description 渲染导航栏部件
     * @return {*}
     * @memberof AppMobChartExpBar
     */
    render(): JSX.Element | null;
}
export declare const AppMobTreeExpBarComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
