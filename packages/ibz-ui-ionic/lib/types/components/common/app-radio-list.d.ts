export declare const AppRadioList: import("vue").DefineComponent<{
    name: {
        type: StringConstructor;
    };
    codeList: {
        type: ObjectConstructor;
    };
    context: {
        type: ObjectConstructor;
        default: {};
    };
    value: {
        type: StringConstructor;
    };
    disabled: {
        type: BooleanConstructor;
        default: boolean;
    };
    readonly: {
        type: BooleanConstructor;
        default: boolean;
    };
    modelService: ObjectConstructor;
}, unknown, {
    codeListService: any;
    options: never[];
}, {}, {
    loadItems(): Promise<any>;
    valueChange(event: any): void;
}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "editorValueChange"[], "editorValueChange", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<{
    name?: unknown;
    codeList?: unknown;
    context?: unknown;
    value?: unknown;
    disabled?: unknown;
    readonly?: unknown;
    modelService?: unknown;
} & {
    disabled: boolean;
    context: Record<string, any>;
    readonly: boolean;
} & {
    modelService?: Record<string, any> | undefined;
    value?: string | undefined;
    name?: string | undefined;
    codeList?: Record<string, any> | undefined;
}> & {
    onEditorValueChange?: ((...args: any[]) => any) | undefined;
}, {
    disabled: boolean;
    context: Record<string, any>;
    readonly: boolean;
}>;
