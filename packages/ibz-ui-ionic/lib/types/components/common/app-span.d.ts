export declare const AppSpan: import("vue").DefineComponent<{
    /**
     * 表单值
     * @type {any}
     * @memberof AppSpanProps
     */
    value: (StringConstructor | NumberConstructor)[];
    /**
     * 上下文data数据
     *
     * @type {Object}
     * @memberof AppSpan
     */
    contextData: ObjectConstructor;
    /**
     * 上下文通讯对象
     *
     * @type {Object}
     * @memberof AppSpan
     */
    contextState: ObjectConstructor;
    /**
     * 上下文
     *
     * @type {Object}
     * @memberof AppSpan
     */
    context: ObjectConstructor;
    /**
     * 视图参数
     *
     * @type {Object}
     * @memberof AppSpan
     */
    viewParam: ObjectConstructor;
    /**
     * 是否禁用
     *
     * @type {boolean}
     * @memberof AppSpan
     */
    disabled: BooleanConstructor;
    /**
     * 代码表标识
     *
     * @type {String}
     * @memberof AppSpan
     */
    tag: StringConstructor;
    /**
     * 代码表类型
     *
     * @type {String}
     * @memberof AppSpan
     */
    codeListType: StringConstructor;
    /**
     * 局部上下文参数
     *
     * @type {Object}
     * @memberof AppSpan
     */
    localContext: ObjectConstructor;
    /**
     * 局部导航参数
     *
     * @type {Object}
     * @memberof AppSpan
     */
    localParam: ObjectConstructor;
    /**
     * 值格式化
     *
     * @type {String}
     * @memberof AppSpan
     */
    valueFormat: StringConstructor;
    /**
     * 浮点精度
     * @type {string}
     * @memberof AppSpan
     */
    precision: StringConstructor;
    /**
     * 模型服务
     * @type {String}
     * @memberof AppSpan
     */
    modelService: ObjectConstructor;
}, {
    codeListService: any;
    items: any[];
    textFormat: string;
}, unknown, {
    curValue(): any;
}, {
    /**
     * 公共参数处理
     *
     * @param {*} arg
     * @returns
     * @memberof AppSpan
     */
    handlePublicParams(arg: any): void;
    /**
     * 加载数据
     *
     * @memberof AppSpan
     */
    load(): Promise<void>;
}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, Record<string, any>, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<{
    value?: unknown;
    contextData?: unknown;
    contextState?: unknown;
    context?: unknown;
    viewParam?: unknown;
    disabled?: unknown;
    tag?: unknown;
    codeListType?: unknown;
    localContext?: unknown;
    localParam?: unknown;
    valueFormat?: unknown;
    precision?: unknown;
    modelService?: unknown;
} & {
    disabled: boolean;
} & {
    modelService?: Record<string, any> | undefined;
    value?: string | number | undefined;
    context?: Record<string, any> | undefined;
    viewParam?: Record<string, any> | undefined;
    contextData?: Record<string, any> | undefined;
    localContext?: Record<string, any> | undefined;
    localParam?: Record<string, any> | undefined;
    tag?: string | undefined;
    codeListType?: string | undefined;
    precision?: string | undefined;
    contextState?: Record<string, any> | undefined;
    valueFormat?: string | undefined;
}>, {
    disabled: boolean;
}>;
