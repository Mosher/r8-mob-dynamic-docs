export declare const AppSwitch: import("vue").DefineComponent<{
    /**
     * 双向绑定值
     * @type {any}
     * @memberof InputBox
     */
    value: {
        type: StringConstructor;
    };
    /**
     * 是否禁用
     * @type {boolean}
     * @memberof InputBox
     */
    disabled: BooleanConstructor;
    /**
     * 只读模式
     *
     * @type {boolean}
     */
    readonly: BooleanConstructor;
}, unknown, unknown, {
    curValue(): boolean;
}, {
    /**
     * 开关值改变事件
     *
     * @param {*} e
     */
    valueChange(e: any): void;
}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "editorValueChange"[], "editorValueChange", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<{
    value?: unknown;
    disabled?: unknown;
    readonly?: unknown;
} & {
    disabled: boolean;
    readonly: boolean;
} & {
    value?: string | undefined;
}> & {
    onEditorValueChange?: ((...args: any[]) => any) | undefined;
}, {
    disabled: boolean;
    readonly: boolean;
}>;
