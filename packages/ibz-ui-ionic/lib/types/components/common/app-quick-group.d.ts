export declare const AppQuickGroup: import("vue").DefineComponent<{
    /**
     * 快速分组项
     *
     * @type {Array}
     */
    items: {
        type: ArrayConstructor;
    };
}, {
    showItems: any[];
    subItems: any[];
    selectedUIItem: any;
    handleClick: (item: any, isFirst?: boolean) => void;
}, unknown, {}, {
    /**
     * 是否为选中项
     *
     * @param item
     * @returns {boolean}
     * @memberof AppQuickGroupTabComponent
     */
    isSelectedItem(item: any): boolean;
    /**
     * 关闭返回框
     *
     * @memberof AppQuickGroupTabComponent
     */
    closeBackdrop(): void;
}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, Record<string, any>, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<{
    items?: unknown;
} & {} & {
    items?: unknown[] | undefined;
}>, {}>;
