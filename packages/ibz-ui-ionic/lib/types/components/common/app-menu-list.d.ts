import { IPSAppMenuItem } from '@ibiz/dynamic-model-api';
import { IParam } from 'ibz-core';
import { ComponentBase } from '../component-base';
export declare class AppMenuListProps {
    /**
     * @description 菜单
     * @type {any[]}
     * @memberof AppMenuListProps
     */
    menu: any[];
    /**
     * @description 应用界面服务对象
     * @type {IParam}
     * @memberof AppMenuIconProps
     */
    appUIService: IParam;
    /**
     * 模型服务
     *
     * @type {IParam}
     * @memberof AppMenuIconProps
     */
    modelService: IParam;
}
export declare class AppMenuList extends ComponentBase<AppMenuListProps> {
    /**
     * 菜单项集合
     *
     * @type IPSAppMenuItem[]
     * @memberof AppMenuList
     */
    menuItems: IPSAppMenuItem[];
    /**
     * vue 生命周期
     *
     * @memberof AppMenuList
     */
    setup(): void;
    /**
     * 打开视图
     *
     * @public
     * @memberof AppMenuList
     */
    openView(item: IPSAppMenuItem): void;
    /**
     * @description 根据菜单项获取菜单权限
     * @param {IPSAppMenuItem} menuItem
     * @return {*}
     * @memberof AppMenuList
     */
    getMenusPermission(menuItem: IPSAppMenuItem): any;
    /**
     * 绘制快速菜单图标项
     *
     * @public
     * @param {IPSAppMenuItem} item
     * @memberof AppMenuList
     */
    renderQuickMenuListItem(item: IPSAppMenuItem): JSX.Element | null;
    /**
     * 绘制内容
     *
     * @memberof AppMenuList
     */
    render(): JSX.Element | undefined;
}
export declare const AppMenuListComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
