/**
 * 操作栏部件
 *
 * @class AppActionBar
 */
export declare const AppActionBar: import("vue").DefineComponent<{
    /**
     * 操作栏模型数据
     *
     * @type {Array}
     */
    items: {
        type: ArrayConstructor;
        default: never[];
    };
    /**
     * 模型服务
     *
     * @type {IParam}
     * @memberof AppFormPageProps
     */
    modelService: ObjectConstructor;
}, {
    handleClick: (tag: string, event: any) => void;
}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, Record<string, any>, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<{
    items?: unknown;
    modelService?: unknown;
} & {
    items: unknown[];
} & {
    modelService?: Record<string, any> | undefined;
}>, {
    items: unknown[];
}>;
