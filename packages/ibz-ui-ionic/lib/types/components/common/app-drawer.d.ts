declare const _default: import("vue").DefineComponent<{
    /**
     * 导航上下文
     *
     * @type {any}
     * @memberof ModelProps
     */
    navContext: {
        type: ObjectConstructor;
        default: {};
    };
    /**
     * 视图上下文参数
     *
     * @type {any}
     * @memberof ModelProps
     */
    navParam: {
        type: ObjectConstructor;
        default: {};
    };
    /**
     * 导航数据
     *
     * @type {*}
     * @memberof ModelProps
     */
    navDatas: {
        type: ArrayConstructor;
        default: never[];
    };
    view: {
        type: ObjectConstructor;
    };
    model: {
        type: ObjectConstructor;
    };
    subject: {
        type: ObjectConstructor;
    };
}, {
    tempResult: any;
    viewPath: string;
    viewComponent: string;
    zIndex: any;
    menu: any;
    side: string;
    customClass: any;
    uuid: string;
    customStyle: any;
}, unknown, {}, {
    /**
     * 视图事件
     *
     * @param viewName 视图名
     * @param action 视图行为
     * @param data 行为数据
     */
    handleViewEvent(viewName: string, action: string, data: any): void;
    /**
     * 视图关闭
     *
     * @memberof AppDrawer
     */
    close(result: any): void;
    /**
     * 视图数据变化
     *
     * @memberof AppDrawer
     */
    dataChange(result: any): void;
    /**
     * 处理数据，向外抛值
     *
     * @memberof AppDrawer
     */
    handleCloseDrawer(): void;
}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, Record<string, any>, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<{
    navContext?: unknown;
    navParam?: unknown;
    navDatas?: unknown;
    view?: unknown;
    model?: unknown;
    subject?: unknown;
} & {
    navContext: Record<string, any>;
    navParam: Record<string, any>;
    navDatas: unknown[];
} & {
    view?: Record<string, any> | undefined;
    model?: Record<string, any> | undefined;
    subject?: Record<string, any> | undefined;
}>, {
    navContext: Record<string, any>;
    navParam: Record<string, any>;
    navDatas: unknown[];
}>;
export default _default;
