export declare const AppLayoutItem: import("vue").DefineComponent<{
    /**
     * @description 样式类名对象
     * @type {*}
     * @memberof AppLayoutItemProps
     */
    class: {
        type: ObjectConstructor;
    };
    /**
     * @description 样式对象
     * @type {*}
     * @memberof AppLayoutItemProps
     */
    style: {
        type: StringConstructor;
        default: string;
    };
    /**
     * @description 布局模式
     * @type {*}
     * @memberof AppLayoutItemProps
     */
    layoutMode: {
        type: StringConstructor;
        default: string;
    };
    /**
     * @description 布局位置模型对象
     * @type {*}
     * @memberof AppLayoutItemProps
     */
    layoutPos: {
        type: ObjectConstructor;
        default: {};
    };
}, unknown, unknown, {}, {
    /**
     * @description 合并样式类名
     * @param {*} arg1 类名对象
     * @param {(any | string)} arg2 类名字符串或对象
     */
    mergeClassNames(arg1: any, arg2: any | string): void;
    /**
     * @description 获取栅格布局
     * @param {*} parent 父容器
     * @param {*} child 子容器
     * @return {*}
     * @memberof AppMobPanel
     */
    getGridLayoutProps(layoutMode: string, layoutPos: any): {
        'size-lg': any;
        'size-md': any;
        'size-sm': any;
        'size-xs': any;
        'offset-lg': any;
        'offset-md': any;
        'offset-sm': any;
        'offset-xs': any;
    };
    /**
     * 计算容器尺寸
     *
     * @param {number} value
     * @return {*}  {string}
     */
    calcBoxSize(value: number, enableRem?: boolean): string;
}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, Record<string, any>, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<{
    class?: unknown;
    style?: unknown;
    layoutMode?: unknown;
    layoutPos?: unknown;
} & {
    style: string;
    layoutMode: string;
    layoutPos: Record<string, any>;
} & {
    class?: Record<string, any> | undefined;
}>, {
    style: string;
    layoutMode: string;
    layoutPos: Record<string, any>;
}>;
