/**
 * @description 步骤条组件
 */
export declare const AppStepsComponent: import("vue").DefineComponent<{
    steps: {
        type: ArrayConstructor;
    };
    active: {
        type: StringConstructor;
        default: string;
    };
    panelInstance: ObjectConstructor;
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, Record<string, any>, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<{
    steps?: unknown;
    active?: unknown;
    panelInstance?: unknown;
} & {
    active: string;
} & {
    steps?: unknown[] | undefined;
    panelInstance?: Record<string, any> | undefined;
}>, {
    active: string;
}>;
