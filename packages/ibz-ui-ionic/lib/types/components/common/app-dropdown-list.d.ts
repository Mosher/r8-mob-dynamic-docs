import { ICodeListService } from 'ibz-core';
/**
 * 下拉列表编辑器
 *
 * @class AppDropdownList
 */
export declare const AppDropdownList: import("vue").DefineComponent<{
    /**
     * 双向绑定值
     *
     * @type {String}
     * @memberof AppDropdownList
     */
    value: {
        type: StringConstructor;
    };
    /**
     * 上下文data数据（表单数据）
     *
     * @type {Object}
     * @memberof AppDropdownList
     */
    contextData: ObjectConstructor;
    /**
     * 上下文
     *
     * @type {Object}
     * @memberof AppDropdownList
     */
    context: ObjectConstructor;
    /**
     * 视图参数
     *
     * @type {Object}
     * @memberof AppDropdownList
     */
    viewParam: ObjectConstructor;
    /**
     * placeholder值
     *
     * @type {String}
     * @memberof AppDropdownList
     */
    placeholder: StringConstructor;
    /**
     * 是否禁用
     *
     * @type {boolean}
     * @memberof AppDropdownList
     */
    disabled: BooleanConstructor;
    /**
     * 只读状态
     *
     * @type {boolean}
     * @memberof AppDropdownList
     */
    readonly: BooleanConstructor;
    /**
     * 代码表标识
     *
     * @type {String}
     * @memberof AppDropdownList
     */
    tag: StringConstructor;
    /**
     * 代码表类型
     *
     * @type {String}
     * @memberof AppDropdownList
     */
    codeListType: StringConstructor;
    /**
     * 局部上下文参数
     *
     * @type {Object}
     * @memberof AppDropdownList
     */
    localContext: ObjectConstructor;
    /**
     * 局部导航参数
     *
     * @type {Object}
     * @memberof AppDropdownList
     */
    localParam: ObjectConstructor;
    /**
     * 值分割符
     *
     * @type {String}
     * @memberof AppDropdownList
     */
    valueSeparator: {
        type: StringConstructor;
        default: string;
    };
    /**
     * 是否多选
     *
     * @type {boolean}
     * @memberof AppDropdownList
     */
    multiple: {
        type: BooleanConstructor;
        default: boolean;
    };
    /**
     * 打开模式(action-sheet和popover模式不支持多选)
     *
     * @type {String}
     * @memberof AppDropdownList
     */
    openMode: {
        type: StringConstructor;
        default: string;
    };
}, unknown, {
    codeListService: ICodeListService;
    items: any[];
}, {}, {
    /**
     * 公共参数处理
     *
     * @param {*} arg
     * @returns
     * @memberof AppDropdownList
     */
    handlePublicParams(arg: any): void;
    /**
     * 加载数据
     *
     * @memberof AppDropdownList
     */
    load(): void;
    /**
     * 选中改变事件
     *
     * @param event
     * @memberof AppDropdownList
     */
    valueChange(event: any): void;
}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "editorValueChange"[], "editorValueChange", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<{
    value?: unknown;
    contextData?: unknown;
    context?: unknown;
    viewParam?: unknown;
    placeholder?: unknown;
    disabled?: unknown;
    readonly?: unknown;
    tag?: unknown;
    codeListType?: unknown;
    localContext?: unknown;
    localParam?: unknown;
    valueSeparator?: unknown;
    multiple?: unknown;
    openMode?: unknown;
} & {
    disabled: boolean;
    readonly: boolean;
    valueSeparator: string;
    multiple: boolean;
    openMode: string;
} & {
    value?: string | undefined;
    context?: Record<string, any> | undefined;
    viewParam?: Record<string, any> | undefined;
    contextData?: Record<string, any> | undefined;
    localContext?: Record<string, any> | undefined;
    localParam?: Record<string, any> | undefined;
    placeholder?: string | undefined;
    tag?: string | undefined;
    codeListType?: string | undefined;
}> & {
    onEditorValueChange?: ((...args: any[]) => any) | undefined;
}, {
    disabled: boolean;
    readonly: boolean;
    valueSeparator: string;
    multiple: boolean;
    openMode: string;
}>;
