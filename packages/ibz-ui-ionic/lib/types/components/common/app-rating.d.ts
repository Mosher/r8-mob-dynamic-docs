export declare const AppRating: import("vue").DefineComponent<{
    /**
     * 双向绑定值
     * @type {String}
     * @memberof AppRatingProps
     */
    value: {
        type: StringConstructor;
    };
    /**
     * 表单项名称
     * @type {String}
     * @memberof AppRatingProps
     */
    name: StringConstructor;
    /**
     * 图标大小
     * @type {String}
     * @memberof AppRatingProps
     */
    size: {
        type: NumberConstructor;
        default: number;
    };
    /**
     * 是否禁用
     * @type {boolean}
     * @memberof AppRatingProps
     */
    disabled: BooleanConstructor;
    /**
     * 只读模式
     * @type {boolean}
     * @memberof AppSlider
     */
    readonly: BooleanConstructor;
    /**
     * 最大值
     * @type {string}
     * @memberof AppSlider
     */
    maxValue: {
        type: StringConstructor;
        default: string;
    };
    /**
     * 最小值
     * @type {string}
     * @memberof AppSlider
     */
    minValue: {
        type: StringConstructor;
        default: string;
    };
    /**
     * 步进值
     * @type {string}
     * @memberof AppSlider
     */
    stepValue: StringConstructor;
    /**
     * 浮点精度
     * @type {string}
     * @memberof AppSlider
     */
    precision: StringConstructor;
}, unknown, {
    starList: any[];
}, {}, {
    /**
     * 值改变事件
     *
     * @param {*} e
     */
    valueChange(value: number, e: MouseEvent): void;
    formatValue(value: number): string;
}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "editorValueChange"[], "editorValueChange", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<{
    value?: unknown;
    name?: unknown;
    size?: unknown;
    disabled?: unknown;
    readonly?: unknown;
    maxValue?: unknown;
    minValue?: unknown;
    stepValue?: unknown;
    precision?: unknown;
} & {
    disabled: boolean;
    readonly: boolean;
    maxValue: string;
    minValue: string;
    size: number;
} & {
    value?: string | undefined;
    name?: string | undefined;
    precision?: string | undefined;
    stepValue?: string | undefined;
}> & {
    onEditorValueChange?: ((...args: any[]) => any) | undefined;
}, {
    disabled: boolean;
    readonly: boolean;
    maxValue: string;
    minValue: string;
    size: number;
}>;
