/**
 * 数据选择编辑器
 *
 * @class AppDataPicker
 */
export declare const AppDataPicker: import("vue").DefineComponent<{
    /**
     * 双向绑定值
     * @type {string}
     * @memberof AppDataPickerProps
     */
    value: {
        type: StringConstructor;
    };
    /**
     * 是否禁用
     * @type {boolean}
     * @memberof AppDataPickerProps
     */
    disabled: BooleanConstructor;
    /**
     * 上下文
     *
     * @type {Object}
     * @memberof AppDataPickerProps
     */
    context: ObjectConstructor;
    /**
     * 视图参数
     *
     * @type {Object}
     * @memberof AppDataPickerProps
     */
    viewParam: ObjectConstructor;
    /**
     * 应用实体主信息属性名称
     *
     * @type {string}
     * @memberof AppDataPickerProps
     */
    deMajorField: {
        type: StringConstructor;
        default: string;
    };
    /**
     * 应用实体主键属性名称
     *
     * @type {string}
     * @memberof AppDataPickerProps
     */
    deKeyField: {
        type: StringConstructor;
        default: string;
    };
    /**
     * 上下文data数据（表单数据）
     *
     * @type {*}
     * @memberof AppDataPickerProps
     */
    contextData: {
        type: ObjectConstructor;
        default: {};
    };
    /**
     * 属性项名称
     *
     * @type {string}
     * @memberof AppDataPickerProps
     */
    name: {
        type: StringConstructor;
    };
    /**
     * 选择视图
     *
     * @type {*}
     * @memberof AppDataPickerProps
     */
    pickUpView: {
        type: ObjectConstructor;
    };
    /**
     * 数据链接参数
     *
     * @type {*}
     * @memberof AppDataPickerProps
     */
    linkView: {
        type: ObjectConstructor;
    };
    /**
     * 值项名称
     *
     * @type {string}
     * @memberof AppDataPickerProps
     */
    valueItem: {
        type: StringConstructor;
    };
    /**
     * 局部上下文参数
     *
     * @type {Object}
     * @memberof AppDataPickerProps
     */
    localContext: ObjectConstructor;
    /**
     * 局部导航参数
     *
     * @type {Object}
     * @memberof AppDataPickerProps
     */
    localParam: ObjectConstructor;
    /**
     * 下拉视图宽度
     *
     * @type {String}
     * @memberof AppDataPickerProps
     */
    dropdownViewWidght: StringConstructor;
    /**
     * 下拉视图高度
     *
     * @type {String}
     * @memberof AppDataPickerProps
     */
    dropdownViewHeight: StringConstructor;
    /**
     * 只读模式
     * @type {boolean}
     * @memberof AppDataPickerProps
     */
    readonly: BooleanConstructor;
    /**
     * placeholder值
     * @type {String}
     * @memberof AppDataPickerProps
     */
    placeholder: StringConstructor;
    /**
     * 模型服务
     * @type {Object}
     * @memberof AppDataPickerProps
     */
    modelService: ObjectConstructor;
}, unknown, unknown, {
    /**
     * 当前选中数据集
     *
     * @returns any[]
     * @memberof AppDataPicker
     */
    curValue(): any[];
}, {
    /**
     * 公共参数处理
     *
     * @param {*} arg
     * @memberof AppDataPicker
     */
    handlePublicParams(arg: any): void;
    /**
     * 打开选择视图
     *
     * @memberof AppDataPicker
     */
    openPickUpView(): void;
    /**
     * 打开链接视图
     *
     * @memberof AppPicker
     */
    openLinkView(): void;
    /**
     * 模态打开
     *
     * @param view 视图模型
     * @param context 上下文
     * @param viewParam 视图参数
     * @memberof AppDataPicker
     */
    openPopupModal(view: any, context: any, viewParam: any): void;
    /**
     * 抽屉打开
     *
     * @param view 视图模型
     * @param context 上下文
     * @param viewParam 视图参数
     * @memberof AppDataPicker
     */
    openDrawer(view: any, context: any, viewParam: any): void;
    /**
     * 视图关闭
     *
     * @param selects 选中数据集
     * @memberof AppDataPicker
     */
    openViewClose(selects: any[]): void;
    /**
     * 清空
     *
     * @memberof AppDataPicker
     */
    onClear(): void;
    renderICons(): JSX.Element | (JSX.Element | null)[] | null;
}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "editorValueChange"[], "editorValueChange", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<{
    value?: unknown;
    disabled?: unknown;
    context?: unknown;
    viewParam?: unknown;
    deMajorField?: unknown;
    deKeyField?: unknown;
    contextData?: unknown;
    name?: unknown;
    pickUpView?: unknown;
    linkView?: unknown;
    valueItem?: unknown;
    localContext?: unknown;
    localParam?: unknown;
    dropdownViewWidght?: unknown;
    dropdownViewHeight?: unknown;
    readonly?: unknown;
    placeholder?: unknown;
    modelService?: unknown;
} & {
    disabled: boolean;
    deMajorField: string;
    deKeyField: string;
    contextData: Record<string, any>;
    readonly: boolean;
} & {
    modelService?: Record<string, any> | undefined;
    value?: string | undefined;
    context?: Record<string, any> | undefined;
    viewParam?: Record<string, any> | undefined;
    name?: string | undefined;
    pickUpView?: Record<string, any> | undefined;
    linkView?: Record<string, any> | undefined;
    valueItem?: string | undefined;
    localContext?: Record<string, any> | undefined;
    localParam?: Record<string, any> | undefined;
    dropdownViewWidght?: string | undefined;
    dropdownViewHeight?: string | undefined;
    placeholder?: string | undefined;
}> & {
    onEditorValueChange?: ((...args: any[]) => any) | undefined;
}, {
    disabled: boolean;
    deMajorField: string;
    deKeyField: string;
    contextData: Record<string, any>;
    readonly: boolean;
}>;
