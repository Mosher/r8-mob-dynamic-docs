import { ComponentBase } from '../component-base';
import { IParam } from 'ibz-core';
export declare class AppIconItemProps {
    /**
     * @description 项数据
     * @type {IParam}
     * @memberof AppIconItemProps
     */
    item: IParam;
    /**
     * @description 索引
     * @type {number}
     * @memberof AppIconItemProps
     */
    index: number;
    /**
     * @description 值格式化
     * @type {string}
     * @memberof AppIconItemProps
     */
    valueFormat: string;
}
export declare class AppIconItem extends ComponentBase<AppIconItemProps> {
    /**
     * @description 项数据
     * @type {IParam}
     * @memberof AppIconItem
     */
    item: IParam;
    /**
     * @description 索引
     * @type {number}
     * @memberof AppIconItem
     */
    index: number;
    /**
     * @description 值格式化
     * @type {string}
     * @memberof AppIconItem
     */
    valueFormat: string;
    /**
     * @description 设置响应式
     * @memberof AppIconItem
     */
    setup(): void;
    /**
     * @description 初始化输入属性
     * @param {AppIconItemProps} opts 输入对象
     * @memberof AppIconItem
     */
    initInputData(opts: AppIconItemProps): void;
    /**
     * @description 输入属性值变更
     * @memberof AppIconItem
     */
    watchEffect(): void;
    /**
     * @description 获取索引样式
     * @memberof AppIconItem
     */
    getIndexText(): void;
    /**
     * @description 点击事件
     * @memberof AppIconItem
     */
    onClick(): void;
    /**
     * @description 绘制
     * @return {*}
     * @memberof AppIconItem
     */
    render(): JSX.Element;
}
export declare const AppIconItemComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
