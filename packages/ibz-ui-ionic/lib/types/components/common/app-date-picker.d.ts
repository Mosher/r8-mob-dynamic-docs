export declare const AppDatePicker: import("vue").DefineComponent<{
    /**
     * 双向绑定值
     * @type {any}
     * @memberof AppDatePickerProps
     */
    value: (StringConstructor | NumberConstructor)[];
    /**
     * placeholder值
     * @type {String}
     * @memberof AppDatePickerProps
     */
    placeholder: StringConstructor;
    /**
     * 是否禁用
     * @type {boolean}
     * @memberof AppDatePickerProps
     */
    disabled: BooleanConstructor;
    /**
     * 只读模式
     *
     * @type {boolean}
     * @memberof AppDatePickerProps
     */
    readonly: BooleanConstructor;
    /**
     * 日期格式
     *
     * @type {string}
     * @memberof AppDatePickerProps
     */
    displayFormat: {
        type: StringConstructor;
        default: string;
    };
}, {
    min: any;
    max: any;
}, unknown, {}, {
    /**
     * 输入框值改变事件
     *
     * @param {*} e
     */
    valueChange(event: any): void;
}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "editorValueChange"[], "editorValueChange", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<{
    value?: unknown;
    placeholder?: unknown;
    disabled?: unknown;
    readonly?: unknown;
    displayFormat?: unknown;
} & {
    disabled: boolean;
    readonly: boolean;
    displayFormat: string;
} & {
    value?: string | number | undefined;
    placeholder?: string | undefined;
}> & {
    onEditorValueChange?: ((...args: any[]) => any) | undefined;
}, {
    disabled: boolean;
    readonly: boolean;
    displayFormat: string;
}>;
