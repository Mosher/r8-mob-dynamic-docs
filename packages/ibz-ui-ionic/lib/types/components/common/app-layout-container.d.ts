export declare const AppLayoutContainer: import("vue").DefineComponent<{
    /**
     * @description 样式类名对象
     * @type {*}
     * @memberof AppLayoutContainerProps
     */
    class: {
        type: ObjectConstructor;
    };
    /**
     * @description 样式对象
     * @type {*}
     * @memberof AppLayoutContainerProps
     */
    style: {
        type: StringConstructor;
        default: string;
    };
    /**
     * @description 样式对象
     * @type {*}
     * @memberof AppLayoutContainerProps
     */
    layout: {
        type: ObjectConstructor;
        default: {};
    };
}, unknown, unknown, {}, {
    /**
     * @description 合并样式类名
     * @param {*} arg1 类名对象
     * @param {(any | string)} arg2 类名字符串或对象
     */
    mergeClassNames(arg1: any, arg2: any | string): void;
}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, Record<string, any>, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<{
    class?: unknown;
    style?: unknown;
    layout?: unknown;
} & {
    style: string;
    layout: Record<string, any>;
} & {
    class?: Record<string, any> | undefined;
}>, {
    style: string;
    layout: Record<string, any>;
}>;
