import { ComponentBase } from '../component-base';
export declare class AppQuickSearchProps {
}
export declare class AppQuickSearch extends ComponentBase<AppQuickSearchProps> {
    /**
     * @description 快速搜索
     * @param {*} $event
     * @memberof AppQuickSearch
     */
    quickSearch($event: any): void;
    /**
     * @description 绘制快速搜索栏
     * @return {*}
     * @memberof AppQuickSearch
     */
    render(): JSX.Element;
}
export declare const AppQuickSearchComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
