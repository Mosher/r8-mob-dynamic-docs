export declare const AppSignature: import("vue").DefineComponent<{
    /**
     *  值
     * @type {String} value
     * @memberof AppSignatureProps
     */
    value: {
        type: StringConstructor;
    };
    /**
     *  线宽
     * @type {String} lineWidth
     * @memberof AppSignatureProps
     */
    lineWidth: {
        type: StringConstructor;
        default: string;
    };
    /**
     *  线色
     * @type {String} lineColor
     * @memberof AppSignatureProps
     */
    lineColor: {
        type: StringConstructor;
        default: string;
    };
    /**
     *  背景色
     * @type {String} backgroundColor
     * @memberof AppSignatureProps
     */
    backgroundColor: {
        type: StringConstructor;
        default: string;
    };
    /**
     *  提示信息
     * @type {String} placeholder
     * @memberof AppSignatureProps
     */
    placeholder: {
        type: StringConstructor;
        default: string;
    };
    /**
     *  图片名称
     * @type {String} imageName
     * @memberof AppSignatureProps
     */
    imageName: {
        type: StringConstructor;
        default: string;
    };
    /**
     *  图片尺寸
     * @type {String} imageSize
     * @memberof AppSignatureProps
     */
    imageSize: {
        type: ObjectConstructor;
        default: {
            width: string;
            height: string;
        };
    };
    /**
     * 只读状态
     *
     * @type {boolean}
     * @memberof AppUploadProps
     */
    readonly: {
        type: BooleanConstructor;
        default: boolean;
    };
    /**
     * 上下文data数据（表单数据）
     *
     * @type {Object}
     * @memberof AppSignatureProps
     */
    contextData: ObjectConstructor;
    /**
     * 上下文
     *
     * @type {Object}
     * @memberof AppSignatureProps
     */
    context: ObjectConstructor;
    /**
     * 视图参数
     *
     * @type {Object}
     * @memberof AppSignatureProps
     */
    viewParam: ObjectConstructor;
    /**
     * 上传参数
     *
     * @type {Object}
     * @memberof AppSignatureProps
     */
    uploadParam: {
        type: ObjectConstructor;
        default: () => void;
    };
    /**
     * 下载参数
     *
     * @type {Object}
     * @memberof AppSignatureProps
     */
    exportParam: {
        type: ObjectConstructor;
        default: () => void;
    };
}, {
    uploadUrl: string;
    downloadUrl: string;
    files: any[];
    data: {
        canvasTxt: any;
        startX: number;
        startY: number;
        moveY: number;
        moveX: number;
        isDraw: boolean;
    };
    position: any;
    currentModal: any;
    canvas: any;
}, unknown, {}, {
    /**
     * @description 开始绘制
     * @param {*} $event
     */
    touchStart($event: any): void;
    /**
     * @description 绘制路径
     * @param {*} $event
     */
    touchMove($event: any): void;
    /**
     * @description 绘制结束
     * @param {*} $event
     */
    touchEnd($event: any): void;
    /**
     * @description 确认绘制
     */
    uploadImage(): Promise<void>;
    /**
     * @description 绘制模态内容
     * @return {*}
     */
    renderModal(): import("vue").DefineComponent<{}, {}, {}, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, import("vue").EmitsOptions, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<{} & {} & {}>, {}>;
    /**
     * @description 打开模态
     */
    openModal(): Promise<void>;
    /**
     * @description 关闭模态
     * @param {boolean} $event
     */
    dismissModal($event: boolean): void;
}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "editorValueChange"[], "editorValueChange", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<{
    value?: unknown;
    lineWidth?: unknown;
    lineColor?: unknown;
    backgroundColor?: unknown;
    placeholder?: unknown;
    imageName?: unknown;
    imageSize?: unknown;
    readonly?: unknown;
    contextData?: unknown;
    context?: unknown;
    viewParam?: unknown;
    uploadParam?: unknown;
    exportParam?: unknown;
} & {
    readonly: boolean;
    placeholder: string;
    uploadParam: Record<string, any>;
    exportParam: Record<string, any>;
    lineWidth: string;
    lineColor: string;
    backgroundColor: string;
    imageName: string;
    imageSize: Record<string, any>;
} & {
    value?: string | undefined;
    context?: Record<string, any> | undefined;
    viewParam?: Record<string, any> | undefined;
    contextData?: Record<string, any> | undefined;
}> & {
    onEditorValueChange?: ((...args: any[]) => any) | undefined;
}, {
    readonly: boolean;
    placeholder: string;
    uploadParam: Record<string, any>;
    exportParam: Record<string, any>;
    lineWidth: string;
    lineColor: string;
    backgroundColor: string;
    imageName: string;
    imageSize: Record<string, any>;
}>;
