export declare const AppSlider: import("vue").DefineComponent<{
    /**
     * 双向绑定值
     * @type {any}
     * @memberof AppSlider
     */
    value: {
        type: StringConstructor;
    };
    /**
     * 名称
     * @type {any}
     * @memberof AppSlider
     */
    name: StringConstructor;
    /**
     * 是否禁用
     * @type {boolean}
     * @memberof AppSlider
     */
    disabled: BooleanConstructor;
    /**
     * 只读模式
     * @type {boolean}
     * @memberof AppSlider
     */
    readonly: BooleanConstructor;
    /**
     * 最大值
     * @type {string}
     * @memberof AppSlider
     */
    maxValue: StringConstructor;
    /**
     * 最小值
     * @type {string}
     * @memberof AppSlider
     */
    minValue: StringConstructor;
    /**
     * 步进值
     * @type {string}
     * @memberof AppSlider
     */
    stepValue: StringConstructor;
    /**
     * 浮点精度
     * @type {string}
     * @memberof AppSlider
     */
    precision: StringConstructor;
}, unknown, unknown, {}, {
    /**
     * 滑动条值改变事件
     *
     * @param {*} e
     */
    valueChange(e: any): void;
}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, Record<string, any>, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<{
    value?: unknown;
    name?: unknown;
    disabled?: unknown;
    readonly?: unknown;
    maxValue?: unknown;
    minValue?: unknown;
    stepValue?: unknown;
    precision?: unknown;
} & {
    disabled: boolean;
    readonly: boolean;
} & {
    value?: string | undefined;
    name?: string | undefined;
    maxValue?: string | undefined;
    minValue?: string | undefined;
    precision?: string | undefined;
    stepValue?: string | undefined;
}>, {
    disabled: boolean;
    readonly: boolean;
}>;
