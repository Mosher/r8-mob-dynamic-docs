import 'quill/dist/quill.snow.css';
export declare const AppRichText: import("vue").DefineComponent<{
    /**
     *  绑定值
     * @type {any}
     * @memberof AppRichTextProps
     */
    value: (StringConstructor | NumberConstructor)[];
    /**
     * 是否禁用
     *
     * @type {boolean}
     * @memberof AppRichTextProps
     */
    disabled: BooleanConstructor;
    /**
     * 只读模式
     * @type {boolean}
     *  @memberof AppRichTextProps
     */
    readonly: BooleanConstructor;
    /**
     * placeholder值
     * @type {String}
     * @memberof AppRichTextProps
     */
    placeholder: StringConstructor;
    /**
     * 模型服务
     * @type {String}
     * @memberof AppRichTextProps
     */
    modelService: ObjectConstructor;
}, {
    toolbarOptions: (string[] | {
        header: (number | boolean)[];
    }[])[];
    quills: any;
    content: string;
    uploadUrl: string;
    delta: null;
}, unknown, {}, {
    /**
     * @description 文件上传
     * @param {*} file 文件
     */
    uploadFile(base64: string): Promise<void>;
    /**
     * @description 成功
     * @param {*} response 响应
     * @param {*} file 上传文件
     */
    onSuccess(response: any): void;
    /**
     * @description 上传失败
     * @param {*} error 错误
     * @param {*} file 文件
     */
    onError(error: any): void;
}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, Record<string, any>, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<{
    value?: unknown;
    disabled?: unknown;
    readonly?: unknown;
    placeholder?: unknown;
    modelService?: unknown;
} & {
    disabled: boolean;
    readonly: boolean;
} & {
    modelService?: Record<string, any> | undefined;
    value?: string | number | undefined;
    placeholder?: string | undefined;
}>, {
    disabled: boolean;
    readonly: boolean;
}>;
