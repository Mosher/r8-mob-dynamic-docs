/**
 * 文件上传组件
 *
 * @class AppUpload
 */
export declare const AppUpload: import("vue").DefineComponent<{
    /**
     * 双向绑定值
     *
     * @type {String}
     * @memberof AppUploadProps
     */
    value: {
        type: StringConstructor;
    };
    /**
     * 上下文data数据（表单数据）
     *
     * @type {Object}
     * @memberof AppUploadProps
     */
    contextData: ObjectConstructor;
    /**
     * 上下文
     *
     * @type {Object}
     * @memberof AppUploadProps
     */
    context: ObjectConstructor;
    /**
     * 视图参数
     *
     * @type {Object}
     * @memberof AppUploadProps
     */
    viewParam: ObjectConstructor;
    /**
     * 是否禁用
     *
     * @type {boolean}
     * @memberof AppUploadProps
     */
    disabled: {
        type: BooleanConstructor;
        default: boolean;
    };
    /**
     * 只读状态
     *
     * @type {boolean}
     * @memberof AppUploadProps
     */
    readonly: {
        type: BooleanConstructor;
        default: boolean;
    };
    /**
     * 是否多选
     *
     * @type {boolean}
     * @memberof AppUploadProps
     */
    multiple: {
        type: BooleanConstructor;
        default: boolean;
    };
    /**
     * 文件后缀
     *
     * @type {String}
     * @memberof AppUploadProps
     */
    fileSuffix: StringConstructor;
    /**
     * 最小文件数量
     *
     * @type {String}
     * @memberof AppUploadProps
     */
    minCount: NumberConstructor;
    /**
     * 最大文件大小
     *
     * @type {String}
     * @memberof AppUploadProps
     */
    maxSize: NumberConstructor;
    /**
     * 最大允许上传个数
     *
     * @type {String}
     * @memberof AppUploadProps
     */
    limit: {
        type: NumberConstructor;
        default: number;
    };
    /**
     * 允许上传的文件类型
     *
     * @type {String}
     * @memberof AppUploadProps
     */
    accept: {
        type: StringConstructor;
        default: string;
    };
    /**
     * 图片选取模式
     *
     * @type {String}
     * @memberof AppUploadProps
     */
    capture: StringConstructor;
    /**
     * 上传类型
     *
     * @type {String}
     * @memberof AppUploadProps
     */
    uploadType: {
        type: StringConstructor;
        default: string;
    };
    /**
     * 上传参数
     *
     * @type {Object}
     * @memberof AppUploadProps
     */
    uploadParam: {
        type: ObjectConstructor;
        default: () => void;
    };
    /**
     * 下载参数
     *
     * @type {Object}
     * @memberof AppUploadProps
     */
    exportParam: {
        type: ObjectConstructor;
        default: () => void;
    };
}, {
    uploadUrl: string;
    downloadUrl: string;
    files: any[];
    devFiles: any[];
}, unknown, {}, {
    /**
     * @description 数据处理
     */
    dataProcess(): void;
    /**
     * @description 文件上传之前
     * @param {*} event 事件源
     */
    beforeFileUpload(event: any): Promise<void>;
    /**
     * @description 文件上传
     * @param {*} file 文件
     */
    onFileUpload(file: any): Promise<void>;
    /**
     * @description 删除文件
     * @param {*} file 文件
     */
    onDelete(file: any): void;
    /**
     * @description 下载
     * @param {*} file 文件
     */
    onDownload(file: any): void;
    /**
     * @description 成功
     * @param {*} response 响应
     * @param {*} file 上传文件
     */
    onSuccess(response: any, file: any): void;
    /**
     * @description 上传失败
     * @param {*} error 错误
     * @param {*} file 文件
     */
    onError(error: any, file: any): void;
    /**
     * @description 绘制文件上传
     * @return {*}
     */
    renderFileUpload(): JSX.Element;
    /**
     * @description 绘制图片上传
     * @return {*}
     */
    renderPictureUpload(): JSX.Element;
}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "editorValueChange"[], "editorValueChange", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<{
    value?: unknown;
    contextData?: unknown;
    context?: unknown;
    viewParam?: unknown;
    disabled?: unknown;
    readonly?: unknown;
    multiple?: unknown;
    fileSuffix?: unknown;
    minCount?: unknown;
    maxSize?: unknown;
    limit?: unknown;
    accept?: unknown;
    capture?: unknown;
    uploadType?: unknown;
    uploadParam?: unknown;
    exportParam?: unknown;
} & {
    disabled: boolean;
    readonly: boolean;
    multiple: boolean;
    limit: number;
    accept: string;
    uploadType: string;
    uploadParam: Record<string, any>;
    exportParam: Record<string, any>;
} & {
    value?: string | undefined;
    context?: Record<string, any> | undefined;
    viewParam?: Record<string, any> | undefined;
    contextData?: Record<string, any> | undefined;
    fileSuffix?: string | undefined;
    minCount?: number | undefined;
    maxSize?: number | undefined;
    capture?: string | undefined;
}> & {
    onEditorValueChange?: ((...args: any[]) => any) | undefined;
}, {
    disabled: boolean;
    readonly: boolean;
    multiple: boolean;
    limit: number;
    accept: string;
    uploadType: string;
    uploadParam: Record<string, any>;
    exportParam: Record<string, any>;
}>;
