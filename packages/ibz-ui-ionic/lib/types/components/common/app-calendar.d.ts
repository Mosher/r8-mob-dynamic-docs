import { ComponentBase } from '../component-base';
import { IParam } from 'ibz-core';
export declare class AppCalendarProps {
    /**
     * @description 多选
     * @type {boolean}
     * @memberof AppCalendarProps
     */
    multi: boolean;
    /**
     * @description 图标
     * @type {IParam[]}
     * @memberof AppCalendarProps
     */
    illustration: IParam[];
    /**
     * @description 长度
     * @type {number}
     * @memberof AppCalendarProps
     */
    touchLength: number;
    /**
     * @description 左箭头
     * @type {string}
     * @memberof AppCalendarProps
     */
    arrowLeft: string;
    /**
     * @description 右箭头
     * @type {string}
     * @memberof AppCalendarProps
     */
    arrowRight: string;
    /**
     * @description 清除
     * @type {boolean}
     * @memberof AppCalendarProps
     */
    clean: boolean;
    /**
     * @description 非现在
     * @type {boolean}
     * @memberof AppCalendarProps
     */
    noNow: boolean;
    /**
     * @description 是否有范围
     * @type {boolean}
     * @memberof AppCalendarProps
     */
    range: boolean;
    /**
     * @description 是否完成
     * @type {boolean}
     * @memberof AppCalendarProps
     */
    completion: boolean;
    /**
     * @description 当前值
     * @type {any[]}
     * @memberof AppCalendarProps
     */
    value: any[];
    /**
     * @description 开始
     * @type {string[]}
     * @memberof AppCalendarProps
     */
    begin: string[];
    /**
     * @description 结束
     * @type {string[]}
     * @memberof AppCalendarProps
     */
    end: string[];
    /**
     * @description 是否补零
     * @type {boolean}
     * @memberof AppCalendarProps
     */
    zero: boolean;
    /**
     * @description 禁用
     * @type {IParam[]}
     * @memberof AppCalendarProps
     */
    disabled: IParam[];
    /**
     * @description 本年历
     * @type {*}
     * @memberof AppCalendarProps
     */
    almanacs: IParam;
    /**
     * @description 内容
     * @type {IParam[]}
     * @memberof AppCalendarProps
     */
    tileContent: IParam[];
    /**
     * @description 标记
     * @type {IParam[]}
     * @memberof AppCalendarProps
     */
    sign: IParam[];
    /**
     * @description 是否为阴历
     * @type {boolean}
     * @memberof AppCalendarProps
     */
    lunar: boolean;
    /**
     * @description 是否周一开始
     * @type {boolean}
     * @memberof AppCalendarProps
     */
    monFirst: boolean;
    /**
     * @description 周显示
     * @type {string[]}
     * @memberof AppCalendarProps
     */
    weeks: string[];
    /**
     * @description 月显示
     * @type {string[]}
     * @memberof AppCalendarProps
     */
    months: string[];
    /**
     * @description 事程
     * @type {IParam}
     * @memberof AppCalendarProps
     */
    events: IParam;
    /**
     * @description 是否改变样式
     * @type {boolean}
     * @memberof AppCalendarProps
     */
    isChangeStyle: boolean;
    /**
     * @description 是否开启周开关
     * @type {boolean}
     * @memberof AppCalendarProps
     */
    weekSwitch: boolean;
    /**
     * @description 月范围
     * @type {any[]}
     * @memberof AppCalendarProps
     */
    monthRange: any[];
    /**
     * @description 是否自适应
     * @type {boolean}
     * @memberof AppCalendarProps
     */
    responsive: boolean;
    /**
     * @description 月范围格式
     * @type {string}
     * @memberof AppCalendarProps
     */
    rangeMonthFormat: string;
}
export declare class AppCalendar extends ComponentBase<AppCalendarProps> {
    /**
     * @description 多选
     * @type {boolean}
     * @memberof AppCalendar
     */
    multi: boolean;
    /**
     * @description 图标
     * @type {IParam[]}
     * @memberof AppCalendar
     */
    illustration: IParam[];
    /**
     * @description 长度
     * @type {number}
     * @memberof AppCalendar
     */
    touchLength: number;
    /**
     * @description 左箭头
     * @type {string}
     * @memberof AppCalendar
     */
    arrowLeft: string;
    /**
     * @description 右箭头
     * @type {string}
     * @memberof AppCalendar
     */
    arrowRight: string;
    /**
     * @description 清除
     * @type {boolean}
     * @memberof AppCalendar
     */
    clean: boolean;
    /**
     * @description 现在
     * @type {boolean}
     * @memberof AppCalendar
     */
    now: boolean;
    /**
     * @description 是否有范围
     * @type {boolean}
     * @memberof AppCalendar
     */
    range: boolean;
    /**
     * @description 是否完成
     * @type {boolean}
     * @memberof AppCalendar
     */
    completion: boolean;
    /**
     * @description 当前值
     * @type {any[]}
     * @memberof AppCalendar
     */
    value: any[];
    /**
     * @description 开始
     * @type {string[]}
     * @memberof AppCalendar
     */
    begin: string[];
    /**
     * @description 结束
     * @type {string[]}
     * @memberof AppCalendar
     */
    end: string[];
    /**
     * @description 是否补零
     * @type {boolean}
     * @memberof AppCalendar
     */
    zero: boolean;
    /**
     * @description 禁用
     * @type {any[]}
     * @memberof AppCalendar
     */
    disabled: IParam[];
    /**
     * @description 本年历
     * @type {*}
     * @memberof AppCalendar
     */
    almanacs: IParam;
    /**
     * @description 内容
     * @type {any[]}
     * @memberof AppCalendar
     */
    tileContent: IParam[];
    /**
     * @description 标记
     * @type {IParam[]}
     * @memberof AppCalendar
     */
    sign: IParam[];
    /**
     * @description 是否为阴历
     * @type {boolean}
     * @memberof AppCalendar
     */
    lunar: boolean;
    /**
     * @description 是否周一开始
     * @type {boolean}
     * @memberof AppCalendar
     */
    monFirst: boolean;
    /**
     * @description 周显示
     * @type {string[]}
     * @memberof AppCalendar
     */
    weeks: string[];
    /**
     * @description 月显示
     * @type {string[]}
     * @memberof AppCalendar
     */
    months: string[];
    /**
     * @description 事程
     * @type {*}
     * @memberof AppCalendar
     */
    events: IParam;
    /**
     * @description 是否改变样式
     * @type {boolean}
     * @memberof AppCalendar
     */
    isChangeStyle: boolean;
    /**
     * @description 是否开启周开关
     * @type {boolean}
     * @memberof AppCalendar
     */
    weekSwitch: boolean;
    /**
     * @description 月范围
     * @type {any[]}
     * @memberof AppCalendar
     */
    monthRange: any[];
    /**
     * @description 是否自适应
     * @type {boolean}
     * @memberof AppCalendar
     */
    responsive: boolean;
    /**
     * @description 月范围格式
     * @type {string}
     * @memberof AppCalendar
     */
    rangeMonthFormat: string;
    /**
     * @description 改变按钮
     * @type {boolean}
     * @memberof AppCalendar
     */
    changebtntop: boolean;
    /**
     * @description 年集合
     * @type {any[]}
     * @memberof AppCalendar
     */
    years: number[];
    /**
     * @description 是否显示年集合
     * @type {boolean}
     * @memberof AppCalendar
     */
    yearsShow: boolean;
    /**
     * @description 年份
     * @type {number}
     * @memberof AppCalendar
     */
    year: number;
    /**
     * @description 月份
     * @type {number}
     * @memberof AppCalendar
     */
    month: number;
    /**
     * @description 月位置
     * @type {number}
     * @memberof AppCalendar
     */
    monthPosition: number;
    /**
     * @description 天
     * @type {number}
     * @memberof AppCalendar
     */
    day: number;
    /**
     * @description 天集合
     * @type {any[]}
     * @memberof AppCalendar
     */
    days: IParam[];
    /**
     * @description 多选天
     * @type {any[]}
     * @memberof AppCalendar
     */
    multiDays: IParam[];
    /**
     * @description 今天集合
     * @type {any[]}
     * @memberof AppCalendar
     */
    today: number[];
    /**
     * @description 处理多选天数据集合
     * @type {any[]}
     * @memberof AppCalendar
     */
    handleMultiDay: IParam[];
    /**
     * @description 是否第一次绘制
     * @type {boolean}
     * @memberof AppCalendar
     */
    firstRender: boolean;
    /**
     * @description 是否为Ios
     * @type {boolean}
     * @memberof AppCalendar
     */
    isIos: boolean;
    /**
     * @description 显示今天
     * @type {*}
     * @memberof AppCalendar
     */
    showToday: IParam;
    /**
     * @description 月文本
     * @type {string}
     * @memberof AppCalendar
     */
    monthText: string;
    /**
     * @description 节日
     * @type {*}
     * @memberof AppCalendar
     */
    festival: IParam;
    /**
     * @description 开始范围
     * @type {any[]}
     * @memberof AppCalendar
     */
    rangeBegin: number[];
    /**
     * @description 结束范围
     * @type {any[]}
     * @memberof AppCalendar
     */
    rangeEnd: number[];
    /**
     * @description 多选天数据
     * @type {any[]}
     * @memberof AppCalendar
     */
    multiDaysData: IParam[];
    /**
     * @description 月循环
     * @type {any[]}
     * @memberof AppCalendar
     */
    monthsLoop: string[];
    /**
     * @description 项宽
     * @type {number}
     * @memberof AppCalendar
     */
    itemWidth: number;
    /**
     * @description 单位
     * @type {string}
     * @memberof AppCalendar
     */
    unit: string;
    /**
     * @description 位置高度
     * @type {number}
     * @memberof AppCalendar
     */
    positionH: number;
    /**
     * @description 月索引
     * @type {number}
     * @memberof AppCalendar
     */
    monthIndex: number;
    /**
     * @description 是否越过
     * @type {boolean}
     * @memberof AppCalendar
     */
    oversliding: boolean;
    /**
     * @description 是否高低差
     * @type {boolean}
     * @memberof AppCalendar
     */
    rangeBgHide: boolean;
    /**
     * @description 月范围天数
     * @type {any[]}
     * @memberof AppCalendar
     */
    monthRangeDays: any[];
    /**
     * @description 月范围
     * @type {any[]}
     * @memberof AppCalendar
     */
    rangeOfMonths: any[];
    /**
     * @description 月天数
     * @type {any[]}
     * @memberof AppCalendar
     */
    monthDays: IParam[];
    /**
     * @description 周索引
     * @type {number}
     * @memberof AppCalendar
     */
    weekIndex: number;
    /**
     * @description 开始周索引
     * @type {number}
     * @memberof AppCalendar
     */
    startWeekIndex: number;
    /**
     * @description 周位置
     * @type {boolean}
     * @memberof AppCalendar
     */
    positionWeek: boolean;
    /**
     * @description 是否在月范围
     * @type {boolean}
     * @memberof AppCalendar
     */
    isMonthRange: boolean;
    /**
     * @description 是否选中
     * @type {boolean}
     * @memberof AppCalendar
     */
    isseletd: boolean;
    /**
     * @description 月循环副本
     * @type {string[]}
     * @memberof AppCalendar
     */
    monthsLoopCopy: string[];
    /**
     * @description 是否初始化绘制
     * @type {boolean}
     * @memberof AppCalendar
     */
    initRender: boolean;
    /**
     * @description 当前时间选择
     * @type {*}
     * @memberof AppCalendar
     */
    thisTimeSelect: number[];
    /**
     * @description 是否为用户选择
     * @type {boolean}
     * @memberof AppCalendar
     */
    isUserSelect: boolean;
    /**
     * @description 临时开始时间范围
     * @type {number[]}
     * @memberof AppCalendar
     */
    rangeBeginTemp: number[];
    /**
     * @description 临时结束时间范围
     * @type {number}
     * @memberof AppCalendar
     */
    rangeEndTemp: number;
    /**
     * @description 设置响应式
     * @memberof AppCalendar
     */
    setup(): void;
    /**
     * @description 值属性变更
     * @memberof AppCalendar
     */
    watchEffect(): void;
    /**
     * @description 初始化输入属性
     * @param {IParam} opts 输入参数
     * @memberof AppCalendar
     */
    initInputData(opts: AppCalendarProps): void;
    /**
     * @description 日历组件基础数据初始化
     * @memberof AppCalendar
     */
    calendarBasicInit(): void;
    /**
     * @description 设置日历监听
     * @memberof AppCalendar
     */
    setCalendarWatch(): void;
    /**
     * @description 周开关变化
     * @return {*}
     * @memberof AppCalendar
     */
    weekSwitchChange(): void;
    /**
     * @description 自适应变化
     * @memberof AppCalendar
     */
    responsiveChange(): void;
    /**
     * @description 月范围变化
     * @return {*}
     * @memberof AppCalendar
     */
    monthRangeChange(): void;
    /**
     * @description 年历变化
     * @return {*}
     * @memberof AppCalendar
     */
    almanacsChange(): void;
    /**
     * @description 内容变化
     * @return {*}
     * @memberof AppCalendar
     */
    tileContentChange(): void;
    /**
     * @description 当前值变化
     * @return {*}
     * @memberof AppCalendar
     */
    valueChange(): void | IParam[];
    /**
     * @description 禁用变化
     * @return {*}
     * @memberof AppCalendar
     */
    disabledChange(): void;
    /**
     * @description 事程变化
     * @return {*}
     * @memberof AppCalendar
     */
    eventsChange(): void;
    /**
     * @description 组件挂载
     * @memberof AppCalendar
     */
    mounted(): void;
    /**
     * @description 初始化日历显示
     * @memberof AppCalendar
     */
    initCalendarShow(): void;
    /**
     * @description 挂载完成之后
     * @return {*}
     * @memberof AppCalendar
     */
    afterMounted(): void;
    /**
     * @description 组件销毁
     * @memberof AppCalendar
     */
    unmounted(): void;
    /**
     * @description 创建初始化
     * @memberof AppCalendar
     */
    createdinit(): void;
    /**
     * @description 改变样式
     * @param {*} item 是否改变按钮
     * @memberof AppCalendar
     */
    changeStyle2(item: any): void;
    /**
     * @description 绘制范围模型
     * @param {*} [renderer] 绘制参数
     * @return {*}
     * @memberof AppCalendar
     */
    isRendeRangeMode(renderer?: any): true | undefined;
    /**
     * @description 初始化绘制范围
     * @param {*} [renderer] 绘制参数
     * @memberof AppCalendar
     */
    initRendeRange(renderer?: any): void;
    /**
     * @description 初始化绘制参数
     * @param {*} y 年
     * @param {*} m 月
     * @param {*} [renderer] 绘制参数
     * @param {*} [payload] 绘制参数类型
     * @return {*}
     * @memberof AppCalendar
     */
    initRenderParam(y: any, m: any, renderer?: any, payload?: any): void | IParam[];
    /**
     * @description 处理绘制参数
     * @param {*} year 年
     * @param {*} month 月
     * @param {*} i 绘制参数
     * @param {*} [playload] 绘制参数类型
     * @return {*}
     * @memberof AppCalendar
     */
    renderOption(year: any, month: any, i: any, playload?: any): any;
    /**
     * @description 是否为本月当天
     * @param {IParam} options 参数
     * @return {*}
     * @memberof AppCalendar
     */
    isCurrentMonthToday(options: IParam): boolean;
    /**
     * @description 监听绘制变化
     * @param {string} type 绘制参数类型
     * @memberof AppCalendar
     */
    watchRender(type: string): void;
    /**
     * @description 项样式
     * @return {*}
     * @memberof AppCalendar
     */
    itemStyle(): {
        width: string;
        height: string;
        fontSize: string;
        lineHeight: string;
    };
    /**
     * @description 行样式
     * @param {*} day 天
     * @param {*} days 天集合
     * @return {*}
     * @memberof AppCalendar
     */
    itemtrStyle(day: any, days: any[]): {
        height: string;
    } | {
        height: number;
    };
    /**
     * @description 主体内容区样式
     * @return {*}
     * @memberof AppCalendar
     */
    mcbodyStyle(): {
        height: string;
    };
    /**
     * @description 临时接触长度
     * @return {*}
     * @memberof AppCalendar
     */
    tempTouchLength(): number | "";
    /**
     * @description 改变样式
     * @memberof AppCalendar
     */
    changeStyle(): void;
    /**
     * @description 处理绘制参数
     * @param {*} y 年
     * @param {*} m 月
     * @param {*} w 绘制参数类型
     * @memberof AppCalendar
     */
    renderer(y: any, m: any, w: any): void;
    /**
     * @description 计算上一年
     * @param {number} year 年
     * @param {number} month 月
     * @return {*}
     * @memberof AppCalendar
     */
    computedPrevYear(year: number, month: number): number;
    /**
     * @description 计算上一月
     * @param {*} isString 是否+1
     * @param {number} month 月份
     * @return {*}
     * @memberof AppCalendar
     */
    computedPrevMonth(isString: boolean, month: number): number;
    /**
     * @description 计算下一年
     * @param {number} year 年
     * @param {number} month 月
     * @return {*}
     * @memberof AppCalendar
     */
    computedNextYear(year: number, month: number): number;
    /**
     * @description 计算下一月
     * @param {*} isString 是否+1
     * @param {number} month 月
     * @return {*}
     * @memberof AppCalendar
     */
    computedNextMonth(isString: any, month: number): number;
    /**
     * @description 获取阴历信息
     * @param {number} y 年
     * @param {number} m 月
     * @param {number} d 日
     * @return {*}
     * @memberof AppCalendar
     */
    getLunarInfo(y: number, m: number, d: number): {
        date: string;
        lunar: any;
        isLunarFestival: boolean;
        isGregorianFestival: boolean;
        isTerm: any;
    };
    /**
     * @description 获取日程
     * @param {*} y 年
     * @param {*} m 月
     * @param {*} d 日
     * @return {*}
     * @memberof AppCalendar
     */
    getEvents(y: number, m: number, d: number): any;
    /**
     * @description 上一个
     * @param {MouseEvent} e 事件源
     * @return {*}
     * @memberof AppCalendar
     */
    prev(e: MouseEvent): void;
    /**
     * @description 下一个
     * @param {MouseEvent} e 事件源
     * @return {*}
     * @memberof AppCalendar
     */
    next(e: MouseEvent): void;
    /**
     * @description 选中日期
     * @param {number} k1 下标1
     * @param {number} k2 下标2
     * @param {IParam} data 数据
     * @param {MouseEvent} e 事件源
     * @param {number} monthIndex 周索引
     * @return {*}
     * @memberof AppCalendar
     */
    select(k1: number, k2: number, data: IParam, e: MouseEvent, monthIndex: number): void | 0;
    /**
     * @description 重置选中
     * @memberof AppCalendar
     */
    resetSelected(): void;
    /**
     * @description 改变年
     * @return {*}
     * @memberof AppCalendar
     */
    changeYear(): false | undefined;
    /**
     * @description 改变月
     * @param {number} value 月
     * @memberof AppCalendar
     */
    changeMonth(value: number): void;
    /**
     * @description 选择年
     * @param {number} value 年
     * @memberof AppCalendar
     */
    selectYear(value: number): void;
    /**
     * @description 设置今天
     * @memberof AppCalendar
     */
    setToday(): void;
    /**
     * @description 设置月范围和周开关
     * @memberof AppCalendar
     */
    setMonthRangeofWeekSwitch(): void;
    /**
     * @description 数据信息
     * @param {number} y 年
     * @param {number} m 月
     * @param {number} d 日
     * @return {*}
     * @memberof AppCalendar
     */
    dateInfo(y: number, m: number, d: number): -1 | {
        lYear: number;
        lMonth: number;
        lDay: number;
        Animal: any;
        IMonthCn: string;
        IDayCn: any;
        cYear: any;
        cMonth: any;
        cDay: any;
        gzYear: any;
        gzMonth: any;
        gzDay: any;
        isToday: boolean;
        isLeap: boolean;
        nWeek: number;
        ncWeek: string;
        isTerm: boolean;
        Term: any;
        astro: string;
    };
    /**
     * @description 补零
     * @param {number} n 需补零的数
     * @return {*}
     * @memberof AppCalendar
     */
    zeroPad(n: number): string;
    /**
     * @description 更新头部显示月
     * @param {*} [type] 类型
     * @memberof AppCalendar
     */
    updateHeadMonth(type?: string): void;
    /**
     * @description 添加监听事件
     * @memberof AppCalendar
     */
    addResponsiveListener(): void;
    /**
     * @description 改变大小
     * @memberof AppCalendar
     */
    resize(): void;
    /**
     * @description 绘制日历操作栏
     * @return {*}
     * @memberof AppCalendar
     */
    renderCalendarTool(): JSX.Element | undefined;
    /**
     * @description 绘制日历内容区
     * @return {*}
     * @memberof AppCalendar
     */
    renderCalendarContent(): JSX.Element;
    /**
     * @description 绘制日历改变
     * @return {*}
     * @memberof AppCalendar
     */
    renderCalendarChange(): JSX.Element;
    /**
     * @description 绘制用户定义
     * @return {*}
     * @memberof AppCalendar
     */
    renderCalendarUser(): any[];
    /**
     * @description 绘制日历
     * @return {*}
     * @memberof AppCalendar
     */
    render(): JSX.Element;
}
export declare const AppCalendarComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
