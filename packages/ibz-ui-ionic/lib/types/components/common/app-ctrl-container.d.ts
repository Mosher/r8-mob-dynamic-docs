/**
 * @description 部件容器组件
 */
export declare const AppCtrlContainer: import("vue").DefineComponent<{
    ctrlName: {
        type: StringConstructor;
        default: string;
    };
}, () => import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
    [key: string]: any;
}>, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, Record<string, any>, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<{
    ctrlName?: unknown;
} & {
    ctrlName: string;
} & {}>, {
    ctrlName: string;
}>;
