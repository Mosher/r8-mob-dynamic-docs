declare const _default: import("vue").DefineComponent<{
    /**
     * 导航上下文
     *
     * @type {any}
     * @memberof ModelProps
     */
    navContext: {
        type: ObjectConstructor;
        default: {};
    };
    /**
     * 视图上下文参数
     *
     * @type {any}
     * @memberof ModelProps
     */
    navParam: {
        type: ObjectConstructor;
        default: {};
    };
    /**
     * 导航数据
     *
     * @type {*}
     * @memberof ModelProps
     */
    navDatas: {
        type: ArrayConstructor;
        default: never[];
    };
    view: {
        type: ObjectConstructor;
    };
    model: {
        type: ObjectConstructor;
    };
    subject: {
        type: ObjectConstructor;
    };
}, {
    tempResult: any;
    viewPath: string;
    viewComponent: string;
    customClass: any;
    zIndex: any;
    customStyle: any;
}, unknown, {}, {
    /**
     * 视图事件
     *
     * @param viewName 视图名
     * @param action 视图行为
     * @param data 抛出数据
     */
    handleViewEvent(viewName: any, action: string, data: any[]): void;
    /**
     * 视图关闭
     *
     * @memberof AppModal
     */
    close(result: any): void;
    /**
     * 视图数据变化
     *
     * @memberof AppModal
     */
    dataChange(result: any): void;
    /**
     * 处理数据，向外抛值
     *
     * @memberof AppModal
     */
    handleCloseModel(): void;
}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, Record<string, any>, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<{
    navContext?: unknown;
    navParam?: unknown;
    navDatas?: unknown;
    view?: unknown;
    model?: unknown;
    subject?: unknown;
} & {
    navContext: Record<string, any>;
    navParam: Record<string, any>;
    navDatas: unknown[];
} & {
    view?: Record<string, any> | undefined;
    model?: Record<string, any> | undefined;
    subject?: Record<string, any> | undefined;
}>, {
    navContext: Record<string, any>;
    navParam: Record<string, any>;
    navDatas: unknown[];
}>;
export default _default;
