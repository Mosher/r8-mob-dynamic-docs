export declare const AppInput: import("vue").DefineComponent<{
    /**
     * 双向绑定值
     * @type {any}
     * @memberof InputBox
     */
    value: (StringConstructor | NumberConstructor)[];
    /**
     * placeholder值
     * @type {String}
     * @memberof InputBox
     */
    placeholder: StringConstructor;
    /**
     * 是否禁用
     * @type {boolean}
     * @memberof InputBox
     */
    disabled: BooleanConstructor;
    /**
     * 只读模式
     *
     * @type {boolean}
     */
    readonly: BooleanConstructor;
    /**
     * 属性类型
     *
     * @type {string}
     * @memberof InputBox
     */
    type: StringConstructor;
    /**
     * 最大值
     * @type {string}
     * @memberof InputBox
     */
    maxValue: StringConstructor;
    /**
     * 最小值
     * @type {string}
     * @memberof InputBox
     */
    minValue: StringConstructor;
    /**
     * 最大长度
     * @type {string}
     * @memberof InputBox
     */
    maxLength: StringConstructor;
    /**
     * 最小长度
     * @type {string}
     * @memberof InputBox
     */
    minLength: StringConstructor;
    /**
     * 是否显示最大长度
     * @type {string}
     * @memberof InputBox
     */
    showMaxLength: BooleanConstructor;
    /**
     * 浮点精度
     * @type {string}
     * @memberof InputBox
     */
    precision: StringConstructor;
    /**
     * 模型服务
     * @type {Object}
     * @memberof InputBox
     */
    modelService: ObjectConstructor;
}, unknown, unknown, {}, {
    /**
     * @description 输入框blur事件
     */
    IonBlur(e: any): void;
}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "editorValueChange"[], "editorValueChange", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<{
    value?: unknown;
    placeholder?: unknown;
    disabled?: unknown;
    readonly?: unknown;
    type?: unknown;
    maxValue?: unknown;
    minValue?: unknown;
    maxLength?: unknown;
    minLength?: unknown;
    showMaxLength?: unknown;
    precision?: unknown;
    modelService?: unknown;
} & {
    disabled: boolean;
    readonly: boolean;
    showMaxLength: boolean;
} & {
    modelService?: Record<string, any> | undefined;
    value?: string | number | undefined;
    placeholder?: string | undefined;
    type?: string | undefined;
    maxValue?: string | undefined;
    minValue?: string | undefined;
    maxLength?: string | undefined;
    minLength?: string | undefined;
    precision?: string | undefined;
}> & {
    onEditorValueChange?: ((...args: any[]) => any) | undefined;
}, {
    disabled: boolean;
    readonly: boolean;
    showMaxLength: boolean;
}>;
