import { IPSAppMenuItem } from '@ibiz/dynamic-model-api';
import { IParam } from 'ibz-core';
import { ComponentBase } from '../component-base';
export declare class AppMenuIconProps {
    /**
     * @description 菜单
     * @type {any[]}
     * @memberof AppMenuIconProps
     */
    menu: any[];
    /**
     * @description 应用界面服务对象
     * @type {IParam}
     * @memberof AppMenuIconProps
     */
    appUIService: IParam;
    /**
     * 模型服务
     *
     * @type {IParam}
     * @memberof AppMenuIconProps
     */
    modelService: IParam;
}
export declare class AppMenuIcon extends ComponentBase<AppMenuIconProps> {
    /**
     * 菜单项集合
     *
     * @type IPSAppMenuItem[]
     * @memberof AppMenuIcon
     */
    menuItems: IPSAppMenuItem[];
    /**
     * vue 生命周期
     *
     * @memberof AppMenuIcon
     */
    setup(): void;
    /**
     * 打开视图
     *
     * @public
     * @memberof AppMenuIcon
     */
    openView(item: IPSAppMenuItem): void;
    /**
     * @description 根据菜单项获取菜单权限
     * @param {IPSAppMenuItem} menuItem
     * @return {*}
     * @memberof AppMenuIcon
     */
    getMenusPermission(menuItem: IPSAppMenuItem): any;
    /**
     * 绘制快速菜单图标项
     *
     * @public
     * @param {IPSAppMenuItem} item
     * @memberof AppMenuIcon
     */
    renderQuickMenuIconItem(item: IPSAppMenuItem): JSX.Element | null;
    /**
     * 绘制内容
     *
     * @memberof AppMenuIcon
     */
    render(): JSX.Element | undefined;
}
export declare const AppMenuIconComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
