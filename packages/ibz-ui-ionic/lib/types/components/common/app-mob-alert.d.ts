export declare const AppMobAlert: import("vue").DefineComponent<{
    /**
     * 视图消息标示
     *
     * @type {string}
     */
    tag: {
        type: StringConstructor;
    };
    /**
     * 显示位置
     *
     * @type {string}
     */
    position: {
        type: StringConstructor;
    };
    /**
     * 应用上下文
     *
     * @type {*}
     */
    context: {
        type: ObjectConstructor;
    };
    /**
     * 视图参数
     *
     * @type {*}
     */
    viewParam: {
        type: ObjectConstructor;
    };
    /**
     * 视图消息集合
     *
     * @type {Array<any>}
     */
    messageDetails: {
        type: ArrayConstructor;
    };
    /**
     * 视图消息组标识
     *
     * @type {string}
     */
    infoGroup: {
        type: StringConstructor;
    };
    /**
     * 视图名称
     *
     * @type {string}
     */
    viewName: {
        type: StringConstructor;
    };
}, {
    items: any[];
}, unknown, {}, {
    handleItems(): void;
    handleItemOption(detail: any): void;
    /**
     * 处理数据关闭模式
     *
     * @memberof AppMobAlert
     */
    handleItemCloseMode(data: any): boolean;
    /**
     * 处理数据显示位置
     *
     * @memberof AppMobAlert
     */
    handleItemPosition(data: any, flag: boolean): void;
    /**
     * 视图消息关闭
     *
     * @memberof AppMobAlert
     */
    alertClose(data: any): void;
}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, Record<string, any>, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<{
    tag?: unknown;
    position?: unknown;
    context?: unknown;
    viewParam?: unknown;
    messageDetails?: unknown;
    infoGroup?: unknown;
    viewName?: unknown;
} & {} & {
    context?: Record<string, any> | undefined;
    viewParam?: Record<string, any> | undefined;
    tag?: string | undefined;
    position?: string | undefined;
    messageDetails?: unknown[] | undefined;
    infoGroup?: string | undefined;
    viewName?: string | undefined;
}>, {}>;
