export declare const AppStepper: import("vue").DefineComponent<{
    /**
     * 双向绑定值
     * @type {any}
     * @memberof AppStepperProps
     */
    value: {
        type: StringConstructor;
    };
    /**
     * 表单项名称
     * @type {any}
     * @memberof AppStepperProps
     */
    name: StringConstructor;
    /**
     * 是否禁用
     * @type {boolean}
     * @memberof AppStepperProps
     */
    disabled: BooleanConstructor;
    /**
     * 只读模式
     * @type {boolean}
     * @memberof AppStepperProps
     */
    readonly: BooleanConstructor;
    /**
     * 最大值
     * @type {string}
     * @memberof AppStepperProps
     */
    maxValue: StringConstructor;
    /**
     * 最小值
     * @type {string}
     * @memberof AppStepperProps
     */
    minValue: StringConstructor;
    /**
     * 步进值
     * @type {string}
     * @memberof AppStepperProps
     */
    stepValue: {
        type: StringConstructor;
        default: string;
    };
    /**
     * 浮点精度
     * @type {string}
     * @memberof AppStepperProps
     */
    precision: StringConstructor;
}, unknown, unknown, {}, {
    /**
     * 输入框change事件
     *
     * @param {any} $event
     */
    onChange($event: any): void;
    /**
     * 按钮点击
     *
     * @param {number} value
     */
    ButtonClick(value: number): void;
    /**
     * @description 值改变
     * @param {number} value
     */
    valueChange(value: number): void;
}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, Record<string, any>, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<{
    value?: unknown;
    name?: unknown;
    disabled?: unknown;
    readonly?: unknown;
    maxValue?: unknown;
    minValue?: unknown;
    stepValue?: unknown;
    precision?: unknown;
} & {
    disabled: boolean;
    readonly: boolean;
    stepValue: string;
} & {
    value?: string | undefined;
    name?: string | undefined;
    maxValue?: string | undefined;
    minValue?: string | undefined;
    precision?: string | undefined;
}>, {
    disabled: boolean;
    readonly: boolean;
    stepValue: string;
}>;
