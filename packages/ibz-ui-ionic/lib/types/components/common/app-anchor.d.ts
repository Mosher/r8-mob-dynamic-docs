/**
 * @description 锚点组件
 */
export declare const AppAnchorComponent: import("vue").DefineComponent<{
    /**
     * @description 传入锚点集合
     * @type {Array}
     */
    anchors: {
        type: ArrayConstructor;
    };
    /**
     * @description 滚动容器标识
     * @type {String}
     */
    scrollContainerTag: {
        type: StringConstructor;
        default: string;
    };
}, () => JSX.Element, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, Record<string, any>, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<{
    anchors?: unknown;
    scrollContainerTag?: unknown;
} & {
    scrollContainerTag: string;
} & {
    anchors?: unknown[] | undefined;
}>, {
    scrollContainerTag: string;
}>;
