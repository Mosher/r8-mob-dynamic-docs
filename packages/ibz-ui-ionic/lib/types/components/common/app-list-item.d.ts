import { ComponentBase } from '../component-base';
import { IParam } from 'ibz-core';
export declare class AppListItemProps {
    /**
     * @description 项数据
     * @type {IParam}
     * @memberof AppListItemProps
     */
    item: IParam;
    /**
     * @description 索引
     * @type {number}
     * @memberof AppListItemProps
     */
    index: number;
    /**
     * @description 值格式化
     * @type {string}
     * @memberof AppListItemProps
     */
    valueFormat: string;
}
export declare class AppListItem extends ComponentBase<AppListItemProps> {
    /**
     * @description 项数据
     * @type {IParam}
     * @memberof AppListItem
     */
    item: IParam;
    /**
     * @description 索引
     * @type {number}
     * @memberof AppListItem
     */
    index: number;
    /**
     * @description 值格式化
     * @type {string}
     * @memberof AppListItem
     */
    valueFormat: string;
    /**
     * @description 设置响应式
     * @memberof AppListItem
     */
    setup(): void;
    /**
     * @description 初始化输入属性
     * @param {AppListItemProps} opts 输入对象
     * @memberof AppListItem
     */
    initInputData(opts: AppListItemProps): void;
    /**
     * @description 输入属性值变更
     * @memberof AppListItem
     */
    watchEffect(): void;
    /**
     * @description 获取索引样式
     * @memberof AppListItem
     */
    getIndexText(): void;
    /**
     * @description 点击事件
     * @memberof AppListItem
     */
    onClick(): void;
    /**
     * @description 绘制
     * @return {*}
     * @memberof AppListItem
     */
    render(): JSX.Element;
}
export declare const AppListItemComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
