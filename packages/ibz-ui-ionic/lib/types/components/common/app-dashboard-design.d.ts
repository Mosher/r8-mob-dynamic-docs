import { IParam } from 'ibz-core';
export declare const AppDashboardDesign: import("vue").DefineComponent<{
    /**
     * @description 应用上下文
     * @type {*}
     * @memberof AppDashboardDesignProps
     */
    navContext: {
        type: ObjectConstructor;
    };
    /**
     * @description 视图参数
     * @type {*}
     * @memberof AppDashboardDesignProps
     */
    navParam: {
        type: ObjectConstructor;
    };
    /**
     * 模型服务
     * @type {Object}
     * @memberof InputBox
     */
    modelService: ObjectConstructor;
}, {
    closeView: (event: any) => void;
    selections: IParam[];
    dragEnd: () => void;
    deleteItem: (id: number) => void;
    notSelections: IParam[];
    titleStatus: import("vue").Ref<boolean>;
    addItem: (id: number) => void;
}, unknown, {}, {
    renderHeader(): JSX.Element;
    renderContent(): JSX.Element;
}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, Record<string, any>, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<{
    navContext?: unknown;
    navParam?: unknown;
    modelService?: unknown;
} & {} & {
    modelService?: Record<string, any> | undefined;
    navContext?: Record<string, any> | undefined;
    navParam?: Record<string, any> | undefined;
}>, {}>;
