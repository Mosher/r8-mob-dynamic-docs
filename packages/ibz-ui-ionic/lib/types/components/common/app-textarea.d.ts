export declare const AppTextArea: import("vue").DefineComponent<{
    /**
     * 双向绑定值
     * @type {any}
     * @memberof AppTextArea
     */
    value: (StringConstructor | NumberConstructor)[];
    /**
     * placeholder值
     * @type {String}
     * @memberof AppTextArea
     */
    placeholder: StringConstructor;
    /**
     * 是否禁用
     * @type {boolean}
     * @memberof AppTextArea
     */
    disabled: BooleanConstructor;
    /**
     * 最大长度
     * @type {string}
     * @memberof AppTextArea
     */
    maxLength: StringConstructor;
    /**
     * 最小长度
     * @type {string}
     * @memberof AppTextArea
     */
    minLength: StringConstructor;
    /**
     * 只读模式
     * @type {boolean}
     * @memberof AppTextArea
     */
    readonly: BooleanConstructor;
    /**
     * 是否显示最大长度
     * @type {string}
     * @memberof AppTextArea
     */
    showMaxLength: BooleanConstructor;
    /**
     * 模型服务
     * @type {Object}
     * @memberof AppTextArea
     */
    modelService: ObjectConstructor;
}, unknown, unknown, {}, {
    /**
     * 输入框值改变事件
     *
     * @param {*} e
     */
    valueChange(e: any): void;
}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, "editorValueChange"[], "editorValueChange", import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<{
    value?: unknown;
    placeholder?: unknown;
    disabled?: unknown;
    maxLength?: unknown;
    minLength?: unknown;
    readonly?: unknown;
    showMaxLength?: unknown;
    modelService?: unknown;
} & {
    disabled: boolean;
    readonly: boolean;
    showMaxLength: boolean;
} & {
    modelService?: Record<string, any> | undefined;
    value?: string | number | undefined;
    placeholder?: string | undefined;
    maxLength?: string | undefined;
    minLength?: string | undefined;
}> & {
    onEditorValueChange?: ((...args: any[]) => any) | undefined;
}, {
    disabled: boolean;
    readonly: boolean;
    showMaxLength: boolean;
}>;
