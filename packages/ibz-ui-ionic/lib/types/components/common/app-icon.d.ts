export declare const AppIcon: import("vue").DefineComponent<{
    /**
     * 图标名称
     *
     * @type {string}
     * @memberof AppIconProps
     */
    name: StringConstructor;
    /**
     * 图标对象
     *
     * @type {string}
     * @memberof AppIconProps
     */
    icon: ObjectConstructor;
    /**
     * 图标路径
     *
     * @type {string}
     * @memberof AppIconProps
     */
    iconSrc: StringConstructor;
}, unknown, unknown, {
    /**
     * iconName
     *
     * @returns string
     */
    iconName(): any;
    /**
     * imagePath
     *
     * @returns string
     */
    imagePath(): string;
}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, Record<string, any>, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<{
    name?: unknown;
    icon?: unknown;
    iconSrc?: unknown;
} & {} & {
    name?: string | undefined;
    icon?: Record<string, any> | undefined;
    iconSrc?: string | undefined;
}>, {}>;
