import { IPSAppDEListExplorerView } from '@ibiz/dynamic-model-api';
import { AppMobListExpViewLayoutProps } from 'ibz-core';
import { LayoutComponentBase } from './layout-component-base';
/**
 * @description 移动端列表导航视图视图布局组件
 * @export
 * @class AppDefaultMobListExpViewLayout
 * @extends {AppDefaultDEExpView<AppMobListExpViewLayoutProps>}
 */
export declare class AppDefaultMobListExpViewLayout extends LayoutComponentBase<AppMobListExpViewLayoutProps> {
    /**
     * 移动端列表导航视图实例对象
     *
     * @type {IPSAppDEListExplorerView}
     * @memberof AppDefaultMobListExpViewLayout
     */
    viewInstance: IPSAppDEListExplorerView;
    /**
     * @description 渲染部件
     * @return {*}
     * @memberof AppDefaultMobListExpViewLayout
     */
    renderViewMainContainerContent(): any;
}
export declare const AppDefaultMobListExpViewLayoutComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
