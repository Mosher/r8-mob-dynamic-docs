import { IPSAppDEMobCalendarView } from '@ibiz/dynamic-model-api';
import { AppMobCalendarViewLayoutProps } from 'ibz-core';
import { MultiDataViewLayoutComponentBase } from './multi-data-view-layout-component-base';
/**
 * 移动端日历视图视图布局面板（DEFAULT样式）
 *
 * @class AppDefaultMobCalendarViewLayout
 * @extends MultiDataViewLayoutComponentBase
 */
export declare class AppDefaultMobCalendarViewLayout extends MultiDataViewLayoutComponentBase<AppMobCalendarViewLayoutProps> {
    /**
     * 移动端日历视图实例对象
     *
     * @public
     * @type {IPSAppDEMobCalendarView}
     * @memberof AppDefaultMobCalendarViewLayout
     */
    viewInstance: IPSAppDEMobCalendarView;
    /**
     *
     * @description 视图主容器内容
     * @return {*}  {*}
     * @memberof AppDefaultMobCalendarViewLayout
     */
    renderViewMainContainerContent(): any;
}
export declare const AppDefaultMobCalendarViewLayoutComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
