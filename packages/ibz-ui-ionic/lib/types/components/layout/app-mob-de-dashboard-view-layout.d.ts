import { IPSAppDEMobDashboardView } from '@ibiz/dynamic-model-api';
import { AppMobDEDashboardViewLayoutProps } from 'ibz-core';
import { LayoutComponentBase } from './layout-component-base';
/**
 * 移动端实体数据看板视图视图布局面板
 *
 * @exports
 * @class AppDefaultMobDEDashboardViewLayout
 * @extends LayoutComponentBase
 */
export declare class AppDefaultMobDEDashboardViewLayout extends LayoutComponentBase<AppMobDEDashboardViewLayoutProps> {
    /**
     * 移动端实体数据看板视图实例对象
     *
     * @type {IPSAppDEMobDashboardView}
     * @memberof AppMobDEDashboardViewLayout
     */
    viewInstance: IPSAppDEMobDashboardView;
    /**
     *
     * @description 视图主容器头部
     * @return {*}
     * @memberof AppMobDEDashboardViewLayout
     */
    renderViewMainContainerHeader(): any;
    /**
     * @description 视图主容器内容
     * @return {*}
     * @memberof AppMobDEDashboardViewLayout
     */
    renderViewMainContainerContent(): any;
}
export declare const AppDefaultMobDEDashboardViewLayoutComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
