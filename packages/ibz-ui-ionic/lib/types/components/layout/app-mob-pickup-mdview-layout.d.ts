import { IPSAppDEMobMDView } from '@ibiz/dynamic-model-api';
import { AppMobPickUpMDViewLayoutProps } from 'ibz-core';
import { LayoutComponentBase } from './layout-component-base';
/**
 * 移动端选择多数据视图视图布局面板（DEFAULT样式）
 *
 * @class AppDefaultMobPickUpMDViewLayout
 * @extends ComponentBase
 */
export declare class AppDefaultMobPickUpMDViewLayout extends LayoutComponentBase<AppMobPickUpMDViewLayoutProps> {
    /**
     * @description 选择多数据视图实例
     * @type {IPSAppDEMobMDView}
     * @memberof AppDefaultMobPickUpMDViewLayout
     */
    viewInstance: IPSAppDEMobMDView;
    /**
     *
     * @description 视图主容器头部
     * @return {*}
     * @memberof AppDefaultMobPickupTreeViewLayout
     */
    renderViewMainContainerHeader(): any;
    /**
     * @description 视图主容器内容
     * @return {*}  {any}
     * @memberof AppDefaultMobPickUpMDViewLayout
     */
    renderViewMainContainerContent(): any;
}
export declare const AppDefaultMobPickUpMDViewLayoutComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
