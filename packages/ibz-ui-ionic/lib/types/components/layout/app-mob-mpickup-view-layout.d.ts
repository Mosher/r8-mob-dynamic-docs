import { IPSAppDEPickupView } from '@ibiz/dynamic-model-api';
import { AppMobMPickUpViewLayoutProps } from 'ibz-core';
import { LayoutComponentBase } from './layout-component-base';
export declare class AppDefaultMobMPickupViewLayout extends LayoutComponentBase<AppMobMPickUpViewLayoutProps> {
    /**
     * @description 多数据选择视图实例对象
     * @type {IPSAppDEPickupView}
     * @memberof AppDefaultMobMPickupViewLayout
     */
    viewInstance: IPSAppDEPickupView;
    /**
     * @description 渲染视图头
     * @return {*}  {*}
     * @memberof AppDefaultMobMPickupViewLayout
     */
    renderViewHeader(): any;
    /**
     * @description 视图主容器内容
     * @return {*}  {any}
     * @memberof AppDefaultMobMPickupViewLayout
     */
    renderViewMainContainerContent(): any;
}
export declare const AppDefaultMobMPickupViewLayoutComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
