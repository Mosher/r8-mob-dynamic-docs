import { IPSAppView, IPSEditor, IPSPanelContainer, IPSPanelCtrlPos, IPSPanelField, IPSPanelItem, IPSPanelRawItem, IPSPanelTabPage, IPSPanelTabPanel, IPSViewLayoutPanel } from '@ibiz/dynamic-model-api';
import { IEditorEventParam, IParam, AppLayoutProps } from 'ibz-core';
import { ComponentBase } from '../component-base';
export declare class LayoutComponentBase<Props extends AppLayoutProps> extends ComponentBase<AppLayoutProps> {
    /**
     * @description 视图实例对象
     * @type {IPSAppView}
     * @memberof LayoutComponentBase
     */
    viewInstance: IPSAppView;
    /**
     * @description 应用上下文
     * @type {IParam}
     * @memberof LayoutComponentBase
     */
    context: IParam;
    /**
     * @description 视图导航参数
     * @type {IParam}
     * @memberof LayoutComponentBase
     */
    viewParam: IParam;
    /**
     * @description 视图导航数据
     * @type {IParam}
     * @memberof LayoutComponentBase
     */
    navDatas: IParam;
    /**
     * @description 是否显示视图标题栏
     * @type {boolean}
     * @memberof LayoutComponentBase
     */
    isShowCaptionBar: boolean;
    /**
     * @description 视图布局面板
     * @type {(IPSViewLayoutPanel | null)}
     * @memberof LayoutComponentBase
     */
    viewLayoutPanel?: IPSViewLayoutPanel | null;
    /**
     * @description 当前选中分页
     * @type {*}
     * @memberof LayoutComponentBase
     */
    currentTab: any;
    renderSlot(name: string): any;
    /**
     * @description 视图容器样式名
     * @readonly
     * @memberof LayoutComponentBase
     */
    get viewClass(): {
        [x: string]: boolean;
        'container-margin': boolean;
        'container-padding': boolean;
        'app-view': boolean;
    };
    /**
     * @description 初始化
     * @memberof LayoutComponentBase
     */
    init(): void;
    /**
     * @description 获取栅格布局
     * @param {*} parent 父容器
     * @param {*} child 子容器
     * @return {*}
     * @memberof LayoutComponentBase
     */
    getGridLayoutProps(parent: any, child: any): {
        'size-lg': any;
        'size-md': any;
        'size-sm': any;
        'size-xs': any;
        'offset-lg': any;
        'offset-md': any;
        'offset-sm': any;
        'offset-xs': any;
    };
    /**
     * @description 渲染标题栏
     * @return {*}
     * @memberof LayoutComponentBase
     */
    renderCaptionBar(): JSX.Element | null;
    /**
     * @description 渲染视图头
     * @return {*}  {*}
     * @memberof LayoutComponentBase
     */
    renderViewHeader(): any;
    /**
     *
     * @description 视图主容器头部
     * @return {*}
     * @memberof LayoutComponentBase
     */
    renderViewMainContainerHeader(): any;
    /**
     *
     * @description 视图主容器内容
     * @return {*}
     * @memberof LayoutComponentBase
     */
    renderViewMainContainerContent(): any;
    /**
     * @description 视图主容器底部
     * @return {*}
     * @memberof LayoutComponentBase
     */
    renderViewMainContainerFooter(): any;
    /**
     * @description 视图顶部插槽
     *
     * @return {*}  {any[]}
     * @memberof LayoutComponentBase
     */
    renderViewTopSlot(): any[];
    /**
     * @description 渲染视图内容
     * @return {*}  {*}
     * @memberof LayoutComponentBase
     */
    renderViewContent(): any;
    /**
     * @description 渲染视图底部
     * @return {*}  {*}
     * @memberof LayoutComponentBase
     */
    renderViewFooter(): any;
    /**
     * @description 获取面板项样式名
     * @param {IPSPanelItem} item 面板项
     * @return {*}
     * @memberof LayoutComponentBase
     */
    getDetailClass(item: IPSPanelItem): {
        [x: string]: boolean;
    };
    /**
     * @description 计算面板项样式及类名
     * @param {IPSPanelItem} item 面板项
     * @return {*}
     * @memberof LayoutComponentBase
     */
    computePanelItemStyle(item: IPSPanelItem): {
        detailStyle: any;
        detailClassName: any;
    };
    /**
     * @description 渲染无父面板项
     * @param {IPSPanelItem} item 面板项
     * @param {*} content 渲染内容
     * @return {*}
     * @memberof LayoutComponentBase
     */
    renderNoParentPanelItem(item: IPSPanelItem, content: any): JSX.Element;
    /**
     * @description 渲染面板子项
     * @param {*} item 面板项实例对象
     * @return {*}
     * @memberof LayoutComponentBase
     */
    renderPanelItems(item: any): JSX.Element | null;
    /**
     * @description 渲染面板容器
     * @param {IPSPanelContainer} item 面板容器实例对象
     * @param {boolean} [hasParent=false] 是否具有父容器
     * @return {*}
     * @memberof LayoutComponentBase
     */
    renderContainer(item: IPSPanelContainer, hasParent?: boolean): JSX.Element | null;
    /**
     * @description 处理编辑器事件
     * @param {IEditorEventParam} event
     * @memberof LayoutComponentBase
     */
    handleEditorEvent(event: IEditorEventParam): void;
    /**
     * @description 渲染编辑器
     * @param {IPSEditor} editor 编辑器实例对象
     * @param {IPSPanelField} parentItem 父容器
     * @return {*}
     * @memberof LayoutComponentBase
     */
    renderEditor(editor: IPSEditor, parentItem: IPSPanelField): import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }>;
    /**
     * @description 渲染面板属性
     * @param {IPSPanelField} item 面板属性实例对象
     * @param {boolean} [hasParent=false] 是否具有父容器
     * @return {*}
     * @memberof LayoutComponentBase
     */
    renderField(item: IPSPanelField, hasParent?: boolean): JSX.Element;
    /**
     * @description 渲染面板直接内容
     * @param {IPSPanelRawItem} item 面板直接内容对象实例
     * @param {boolean} [hasParent=false] 是否具有父容器
     * @return {*}
     * @memberof LayoutComponentBase
     */
    renderRawItem(item: IPSPanelRawItem, hasParent?: boolean): any;
    /**
     * @description 分页面板切换
     * @param {string} key 分页面板标识
     * @param {*} event 源事件对象
     * @memberof LayoutComponentBase
     */
    segmentChanged(key: string, event: any): void;
    /**
     * @description 渲染面板分页面板
     * @param {IPSPanelTabPanel} item 面板分页面板实例对象
     * @param {boolean} [hasParent=false] 是否具有父容器
     * @return {*}
     * @memberof LayoutComponentBase
     */
    renderTabPanel(item: IPSPanelTabPanel, hasParent?: boolean): JSX.Element | JSX.Element[] | undefined;
    /**
     * @description 渲染面板分页
     * @param {IPSPanelTabPage} item 面板分页实例对象
     * @param {boolean} [hasParent=false] 是否具有父容器
     * @return {*}
     * @memberof LayoutComponentBase
     */
    renderTabPage(item: IPSPanelTabPage, hasParent?: boolean): JSX.Element;
    /**
     * @description 渲染面板控件
     * @param {IPSPanelCtrlPos} item 面板组件实例对象
     * @param {boolean} [hasParent=false] 是否具有父容器
     * @return {*}
     * @memberof LayoutComponentBase
     */
    renderCtrlPos(item: IPSPanelCtrlPos, hasParent?: boolean): any;
    /**
     * @description 根据面板项类型绘制
     * @param {*} item 面板项
     * @param {boolean} [hasParent=false] 是否具有父容器
     * @return {*}
     * @memberof LayoutComponentBase
     */
    renderByDetailType(item: any, hasParent?: boolean): any;
    /**
     * @description 渲染视图布局面板
     * @return {*}
     * @memberof LayoutComponentBase
     */
    renderViewLayoutPanel(): any[];
    /**
     * @description 布局面板渲染
     * @return {*}
     * @memberof LayoutComponentBase
     */
    render(): JSX.Element;
}
