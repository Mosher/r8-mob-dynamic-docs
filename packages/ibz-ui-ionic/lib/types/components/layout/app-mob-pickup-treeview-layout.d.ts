import { IPSAppDEMobTreeView } from '@ibiz/dynamic-model-api';
import { AppMobPickupTreeViewLayoutProps } from 'ibz-core';
import { LayoutComponentBase } from './layout-component-base';
/**
 * 移动端选择树视图视图布局面板（DEFAULT样式）
 *
 * @class AppDefaultMobPickupTreeViewLayout
 * @extends LayoutComponentBase
 */
export declare class AppDefaultMobPickupTreeViewLayout extends LayoutComponentBase<AppMobPickupTreeViewLayoutProps> {
    /**
     * @description 选择树视图实例
     * @type {IPSAppDEMobTreeView}
     * @memberof AppDefaultMobPickupTreeViewLayout
     */
    viewInstance: IPSAppDEMobTreeView;
    /**
     *
     * @description 视图主容器头部
     * @return {*}
     * @memberof AppDefaultMobPickupTreeViewLayout
     */
    renderViewMainContainerHeader(): any;
    /**
     * @description 视图主容器内容
     * @return {*}  {any[]}
     * @memberof AppDefaultMobPickupTreeViewLayout
     */
    renderViewMainContainerContent(): any;
}
export declare const AppDefaultMobPickupTreeViewLayoutComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
