import { IPSAppDEMobMapExplorerView } from '@ibiz/dynamic-model-api';
import { AppMobMapExpViewLayoutProps } from 'ibz-core';
import { LayoutComponentBase } from './layout-component-base';
/**
 * @description 移动端地图导航视图视图布局组件
 * @export
 * @class AppDefaultMobMapExpViewLayout
 * @extends {AppDefaultDEExpView<AppMobMapExpViewLayoutProps>}
 */
export declare class AppDefaultMobMapExpViewLayout extends LayoutComponentBase<AppMobMapExpViewLayoutProps> {
    /**
     * 移动端地图导航视图实例对象
     *
     * @type {IPSAppDEMobMapExpView}
     * @memberof AppDefaultMobMapExpViewLayout
     */
    viewInstance: IPSAppDEMobMapExplorerView;
    /**
     * @description 视图主容器内容
     * @return {*}
     * @memberof AppDefaultMobHtmlViewLayout
     */
    renderViewMainContainerContent(): any;
}
export declare const AppDefaultMobMapExpViewLayoutComponent: import("@vue/runtime-core").DefineComponent<any, {}, {}, import("@vue/runtime-core").ComputedOptions, import("@vue/runtime-core").MethodOptions, import("@vue/runtime-core").ComponentOptionsMixin, import("@vue/runtime-core").ComponentOptionsMixin, {}, string, import("@vue/runtime-core").VNodeProps & import("@vue/runtime-core").AllowedComponentProps & import("@vue/runtime-core").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
