import { AppIndexViewLayoutProps } from 'ibz-core';
import { LayoutComponentBase } from './layout-component-base';
/**
 * 应用首页视图布局面板
 *
 * @class AppDefaultIndexViewLayout
 */
export declare class AppDefaultIndexViewLayout extends LayoutComponentBase<AppIndexViewLayoutProps> {
    /**
     * @description 渲染视图头
     * @return {*}
     * @memberof AppDefaultIndexViewLayout
     */
    renderViewHeader(): null;
    /**
     * @description 渲染视图底部
     * @return {*}
     * @memberof AppDefaultIndexViewLayout
     */
    renderViewFooter(): null;
    /**
     * @description 视图主容器内容
     * @return {*}  {any[]}
     * @memberof AppDefaultIndexViewLayout
     */
    renderViewMainContainerContent(): any;
}
export declare const AppDefaultIndexViewLayoutComponent: import("@vue/runtime-core").DefineComponent<any, {}, {}, import("@vue/runtime-core").ComputedOptions, import("@vue/runtime-core").MethodOptions, import("@vue/runtime-core").ComponentOptionsMixin, import("@vue/runtime-core").ComponentOptionsMixin, {}, string, import("@vue/runtime-core").VNodeProps & import("@vue/runtime-core").AllowedComponentProps & import("@vue/runtime-core").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
