import { IPSAppDEMobEditView } from '@ibiz/dynamic-model-api';
import { AppMobEditViewLayoutProps } from 'ibz-core';
import { LayoutComponentBase } from './layout-component-base';
export declare class AppDefaultMobEditViewLayout extends LayoutComponentBase<AppMobEditViewLayoutProps> {
    /**
     * 移动端编辑视图实例对象
     *
     * @type {IPSAppDEMobEditView}
     * @memberof AppDefaultMobEditViewLayout
     */
    viewInstance: IPSAppDEMobEditView;
    /**
     * @description 视图主容器内容
     * @return {*}
     * @memberof AppDefaultMobEditViewLayout
     */
    renderViewMainContainerContent(): any;
}
export declare const AppDefaultMobEditViewLayoutComponent: import("@vue/runtime-core").DefineComponent<any, {}, {}, import("@vue/runtime-core").ComputedOptions, import("@vue/runtime-core").MethodOptions, import("@vue/runtime-core").ComponentOptionsMixin, import("@vue/runtime-core").ComponentOptionsMixin, {}, string, import("@vue/runtime-core").VNodeProps & import("@vue/runtime-core").AllowedComponentProps & import("@vue/runtime-core").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
