import { IPSAppDEMobTabExplorerView } from '@ibiz/dynamic-model-api';
import { AppMobTabExpViewLayoutProps } from 'ibz-core';
import { LayoutComponentBase } from './layout-component-base';
/**
 * 移动端分页导航视图视图布局面板
 *
 * @class AppDefaultMobTabExpViewLayout
 * @extends ComponentBase
 */
export declare class AppDefaultMobTabExpViewLayout extends LayoutComponentBase<AppMobTabExpViewLayoutProps> {
    /**
     * 移动端地图视图实例对象
     *
     * @public
     * @type {IPSAppDEMobTabExplorerView}
     * @memberof AppDefaultMobTabExpViewLayout
     */
    viewInstance: IPSAppDEMobTabExplorerView;
    /**
     * @description 视图主容器内容
     * @return {*}
     * @memberof AppDefaultMobTabExpViewLayout
     */
    renderViewMainContainerContent(): any;
}
export declare const AppDefaultMobTabExpViewLayoutComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
