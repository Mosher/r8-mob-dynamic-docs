import { AppMobTreeViewLayoutProps } from 'ibz-core';
import { MultiDataViewLayoutComponentBase } from './multi-data-view-layout-component-base';
/**
 * 移动端树视图视图布局面板
 *
 * @class AppMobTreeViewLayout
 * @extends LayoutComponentBase
 */
export declare class AppDefaultMobTreeViewLayout extends MultiDataViewLayoutComponentBase<AppMobTreeViewLayoutProps> {
    /**
     * @description 渲染部件
     * @return {*}  {any}
     * @memberof AppDefaultMobTreeViewLayout
     */
    renderViewMainContainerContent(): any;
}
export declare const AppDefaultMobTreeViewLayoutComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
