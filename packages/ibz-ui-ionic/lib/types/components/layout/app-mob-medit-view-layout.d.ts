import { IPSAppDEMobMEditView } from '@ibiz/dynamic-model-api';
import { AppMobMEditViewLayoutProps } from 'ibz-core';
import { LayoutComponentBase } from './layout-component-base';
export declare class AppDefaultMobMEditViewLayout extends LayoutComponentBase<AppMobMEditViewLayoutProps> {
    /**
     * @description 移动端多表单编辑视图实例对象
     * @type {IPSAppDEMobMEditView}
     * @memberof AppDefaultMobMEditViewLayout
     */
    viewInstance: IPSAppDEMobMEditView;
    /**
     * @description 视图主容器内容
     * @return {*}  {any}
     * @memberof AppDefaultMobMEditViewLayout
     */
    renderViewMainContainerContent(): any;
}
export declare const AppDefaultMobMEditViewLayoutComponent: import("@vue/runtime-core").DefineComponent<any, {}, {}, import("@vue/runtime-core").ComputedOptions, import("@vue/runtime-core").MethodOptions, import("@vue/runtime-core").ComponentOptionsMixin, import("@vue/runtime-core").ComponentOptionsMixin, {}, string, import("@vue/runtime-core").VNodeProps & import("@vue/runtime-core").AllowedComponentProps & import("@vue/runtime-core").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
