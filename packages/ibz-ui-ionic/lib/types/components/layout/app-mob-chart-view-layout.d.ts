import { IPSAppDEMobChartView } from '@ibiz/dynamic-model-api';
import { AppMobChartViewLayoutProps } from 'ibz-core';
import { MultiDataViewLayoutComponentBase } from './multi-data-view-layout-component-base';
/**
 * 移动端图表视图视图布局面板
 *
 * @class AppDefaultMobChartViewLayout
 * @extends MultiDataViewLayoutComponentBase
 */
export declare class AppDefaultMobChartViewLayout extends MultiDataViewLayoutComponentBase<AppMobChartViewLayoutProps> {
    /**
     * @description 移动端图表视图实例对象
     * @type {IPSAppDEMobChartView}
     * @memberof AppDefaultMobChartViewLayout
     */
    viewInstance: IPSAppDEMobChartView;
    /**
     * @description 视图主容器内容
     * @return {*}  {*}
     * @memberof AppDefaultMobChartViewLayout
     */
    renderViewMainContainerContent(): any;
}
export declare const AppDefaultMobChartViewLayoutComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
