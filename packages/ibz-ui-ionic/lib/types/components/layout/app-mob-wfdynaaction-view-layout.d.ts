import { IPSAppDEMobWFDynaActionView } from '@ibiz/dynamic-model-api';
import { AppMobWFDynaActionViewLayoutProps } from 'ibz-core';
import { LayoutComponentBase } from './layout-component-base';
/**
 * @description 移动端动态工作流操作视图视图布局组件
 * @export
 * @class AppDefaultMobWFDynaActionViewLayout
 * @extends {AppDefaultDEView<AppMobWFDynaActionViewLayoutProps>}
 */
export declare class AppDefaultMobWFDynaActionViewLayout extends LayoutComponentBase<AppMobWFDynaActionViewLayoutProps> {
    /**
     * @description 移动端动态工作流操作视图实例对象
     * @type {IPSAppDEMobWFDynaActionView}
     * @memberof AppDefaultMobWFDynaActionViewLayout
     */
    viewInstance: IPSAppDEMobWFDynaActionView;
    /**
     * @description 绘制组件
     * @return {*}
     * @memberof AppDefaultMobWFDynaActionViewLayout
     */
    renderViewMainContainerContent(): any;
    /**
     * @description 绘制视图底部
     * @return {*}
     * @memberof AppDefaultMobWFDynaActionViewLayout
     */
    renderViewFooter(): import("@vue/runtime-core").VNode<import("@vue/runtime-core").RendererNode, import("@vue/runtime-core").RendererElement, {
        [key: string]: any;
    }>;
}
export declare const AppDefaultMobWFDynaActionViewLayoutComponent: import("@vue/runtime-core").DefineComponent<any, {}, {}, import("@vue/runtime-core").ComputedOptions, import("@vue/runtime-core").MethodOptions, import("@vue/runtime-core").ComponentOptionsMixin, import("@vue/runtime-core").ComponentOptionsMixin, {}, string, import("@vue/runtime-core").VNodeProps & import("@vue/runtime-core").AllowedComponentProps & import("@vue/runtime-core").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
