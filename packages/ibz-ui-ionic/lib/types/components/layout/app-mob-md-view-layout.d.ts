import { IPSAppDEMobMDView } from '@ibiz/dynamic-model-api';
import { AppMobMDViewLayoutProps } from 'ibz-core';
import { MultiDataViewLayoutComponentBase } from './multi-data-view-layout-component-base';
/**
 * 移动端多数据视图视图布局面板（DEFAULT样式）
 *
 * @class AppDefaultMobMDViewLayout
 * @extends MultiDataViewLayoutComponentBase
 */
export declare class AppDefaultMobMDViewLayout extends MultiDataViewLayoutComponentBase<AppMobMDViewLayoutProps> {
    /**
     * @description 移动端多数据视图实例对象
     * @type {IPSAppDEMobMDView}
     * @memberof AppDefaultMobMDViewLayout
     */
    viewInstance: IPSAppDEMobMDView;
    /**
     * @description 视图主容器内容
     * @return {*}  {any}
     * @memberof AppDefaultMobMDViewLayout
     */
    renderViewMainContainerContent(): any;
}
export declare const AppDefaultMobMDViewLayoutComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
