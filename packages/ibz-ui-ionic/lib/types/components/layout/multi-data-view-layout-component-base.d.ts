import { AppMultiDataViewLayoutComponentProps } from 'ibz-core';
import { LayoutComponentBase } from './layout-component-base';
/**
 * 多数据视图布局基类
 *
 * @export
 * @class MultiDataViewLayoutComponentBase
 * @extends {LayoutComponentBase<AppMultiDataViewLayoutComponentProps>}
 * @template Props
 */
export declare class MultiDataViewLayoutComponentBase<Props extends AppMultiDataViewLayoutComponentProps> extends LayoutComponentBase<AppMultiDataViewLayoutComponentProps> {
    /**
     *
     * @description 视图主容器头部
     * @return {*}
     * @memberof MultiDataViewLayoutComponentBase
     */
    renderViewMainContainerHeader(): any;
}
