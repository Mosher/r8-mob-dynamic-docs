import { IPSAppDEMobTreeExplorerView } from '@ibiz/dynamic-model-api';
import { AppMobTreeExpViewLayoutProps } from 'ibz-core';
import { LayoutComponentBase } from './layout-component-base';
/**
 * @description 移动端树导航视图视图布局组件
 * @export
 * @class AppDefaultMobTreeExpViewLayout
 * @extends {AppDefaultDEExpView<AppMobTreeExpViewLayoutProps>}
 */
export declare class AppDefaultMobTreeExpViewLayout extends LayoutComponentBase<AppMobTreeExpViewLayoutProps> {
    /**
     * 移动端树导航视图实例对象
     *
     * @type {IPSAppDEMobTreeExplorerView}
     * @memberof AppDefaultMobTreeExpViewLayout
     */
    viewInstance: IPSAppDEMobTreeExplorerView;
    /**
   * @description 视图主容器内容
   * @return {*}
   * @memberof AppDefaultMobHtmlViewLayout
   */
    renderViewMainContainerContent(): any;
}
export declare const AppDefaultMobTreeExpViewLayoutComponent: import("@vue/runtime-core").DefineComponent<any, {}, {}, import("@vue/runtime-core").ComputedOptions, import("@vue/runtime-core").MethodOptions, import("@vue/runtime-core").ComponentOptionsMixin, import("@vue/runtime-core").ComponentOptionsMixin, {}, string, import("@vue/runtime-core").VNodeProps & import("@vue/runtime-core").AllowedComponentProps & import("@vue/runtime-core").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
