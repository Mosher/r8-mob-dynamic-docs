import { IPSAppDEChartExplorerView } from '@ibiz/dynamic-model-api';
import { AppMobChartExpViewLayoutProps } from 'ibz-core';
import { LayoutComponentBase } from './layout-component-base';
/**
 * @description 移动端图表导航视图视图布局组件
 * @export
 * @class AppDefaultMobChartExpViewLayout
 * @extends {AppDefaultExpView<AppMobChartExpViewLayoutProps>}
 */
export declare class AppDefaultMobChartExpViewLayout extends LayoutComponentBase<AppMobChartExpViewLayoutProps> {
    /**
     * 移动端图表导航视图实例对象
     *
     * @type {IPSAppDEMobChartExpView}
     * @memberof AppDefaultMobChartExpViewLayout
     */
    viewInstance: IPSAppDEChartExplorerView;
    /**
     * @description 视图主容器内容
     * @return {*}
     * @memberof AppDefaultMobHtmlViewLayout
     */
    renderViewMainContainerContent(): any;
}
export declare const AppDefaultMobChartExpViewLayoutComponent: import("@vue/runtime-core").DefineComponent<any, {}, {}, import("@vue/runtime-core").ComputedOptions, import("@vue/runtime-core").MethodOptions, import("@vue/runtime-core").ComponentOptionsMixin, import("@vue/runtime-core").ComponentOptionsMixin, {}, string, import("@vue/runtime-core").VNodeProps & import("@vue/runtime-core").AllowedComponentProps & import("@vue/runtime-core").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
