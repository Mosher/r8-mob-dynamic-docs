import { IPSAppDEMobWizardView } from '@ibiz/dynamic-model-api';
import { AppMobWizardViewLayoutProps } from 'ibz-core';
import { LayoutComponentBase } from './layout-component-base';
/**
 * @description 移动端向导视图视图布局组件
 * @export
 * @class AppDefaultMobWizardViewLayout
 * @extends {AppDefaultDEView<AppMobWizardViewLayoutProps>}
 */
export declare class AppDefaultMobWizardViewLayout extends LayoutComponentBase<AppMobWizardViewLayoutProps> {
    /**
     * @description 移动端向导视图实例对象
     * @type {IPSAppDEMobWizardView}
     * @memberof AppDefaultMobWizardViewLayout
     */
    viewInstance: IPSAppDEMobWizardView;
    /**
     * @description 渲染部件
     * @return {*}
     * @memberof AppDefaultMobWizardViewLayout
     */
    renderViewMainContainerContent(): any;
}
export declare const AppDefaultMobWizardViewLayoutComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
