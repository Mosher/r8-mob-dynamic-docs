import { IPSAppDEPickupView } from '@ibiz/dynamic-model-api';
import { AppMobPickUpViewLayoutProps } from 'ibz-core';
import { LayoutComponentBase } from './layout-component-base';
export declare class AppDefaultMobPickupViewLayout extends LayoutComponentBase<AppMobPickUpViewLayoutProps> {
    /**
     * @description 数据选择视图实例
     * @type {IPSAppDEPickupView}
     * @memberof AppDefaultMobPickupViewLayout
     */
    viewInstance: IPSAppDEPickupView;
    /**
     * @description 渲染视图头
     * @return {*}  {*}
     * @memberof AppDefaultMobPickupViewLayout
     */
    renderViewHeader(): any;
    /**
     * @description 视图主容器内容
     * @return {*}  {any}
     * @memberof AppDefaultMobPickupViewLayout
     */
    renderViewMainContainerContent(): any;
}
export declare const AppDefaultMobPickupViewLayoutComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
