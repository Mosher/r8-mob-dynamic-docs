import { IPSAppDEMobEditView } from '@ibiz/dynamic-model-api';
import { AppMobOptViewLayoutProps } from 'ibz-core';
import { LayoutComponentBase } from './layout-component-base';
/**
 * @description 移动端选项操作视图视图布局组件
 * @export
 * @class AppDefaultMobOptViewLayout
 * @extends {LayoutComponentBase<AppMobOptViewLayoutProps>}
 */
export declare class AppDefaultMobOptViewLayout extends LayoutComponentBase<AppMobOptViewLayoutProps> {
    /**
     * 移动端选项操作视图实例对象
     *
     * @type {IPSAppDEMobOptView}
     * @memberof AppDefaultMobOptViewLayout
     */
    viewInstance: IPSAppDEMobEditView;
    /**
     * @description 视图主容器内容
     * @return {*}
     * @memberof AppDefaultMobOptViewLayout
     */
    renderViewMainContainerContent(): any;
    /**
     * @description 渲染视图底部
     * @return {*}  {*}
     * @memberof AppDefaultMobOptViewLayout
     */
    renderViewFooter(): any;
}
export declare const AppDefaultMobOptViewLayoutComponent: import("@vue/runtime-core").DefineComponent<any, {}, {}, import("@vue/runtime-core").ComputedOptions, import("@vue/runtime-core").MethodOptions, import("@vue/runtime-core").ComponentOptionsMixin, import("@vue/runtime-core").ComponentOptionsMixin, {}, string, import("@vue/runtime-core").VNodeProps & import("@vue/runtime-core").AllowedComponentProps & import("@vue/runtime-core").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
