import { IPSAppDECustomView } from '@ibiz/dynamic-model-api';
import { AppMobCustomViewLayoutProps } from 'ibz-core';
import { LayoutComponentBase } from './layout-component-base';
export declare class AppDefaultMobCustomViewLayout extends LayoutComponentBase<AppMobCustomViewLayoutProps> {
    /**
     * 移动端自定义视图实例对象
     *
     * @type {IPSAppDECustomView}
     * @memberof AppDefaultMobCustomViewLayout
     */
    viewInstance: IPSAppDECustomView;
    /**
     *
     * @description 视图主容器头部
     * @return {*}
     * @memberof MultiDataViewLayoutComponentBase
     */
    renderViewMainContainerHeader(): any;
    /**
     * @description 视图主容器内容
     * @return {*}
     * @memberof AppDefaultMobCustomViewLayout
     */
    renderViewMainContainerContent(): any;
}
export declare const AppDefaultMobCustomViewLayoutComponent: import("@vue/runtime-core").DefineComponent<any, {}, {}, import("@vue/runtime-core").ComputedOptions, import("@vue/runtime-core").MethodOptions, import("@vue/runtime-core").ComponentOptionsMixin, import("@vue/runtime-core").ComponentOptionsMixin, {}, string, import("@vue/runtime-core").VNodeProps & import("@vue/runtime-core").AllowedComponentProps & import("@vue/runtime-core").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
