import { IPSAppDEMobMapView } from '@ibiz/dynamic-model-api';
import { AppMobMapViewLayoutProps } from 'ibz-core';
import { MultiDataViewLayoutComponentBase } from './multi-data-view-layout-component-base';
/**
 * 移动端地图视图视图布局面板（DEFAULT样式）
 *
 * @class AppDefaultMobMapViewLayout
 * @extends ComponentBase
 */
export declare class AppDefaultMobMapViewLayout extends MultiDataViewLayoutComponentBase<AppMobMapViewLayoutProps> {
    /**
     * 移动端地图视图实例对象
     *
     * @public
     * @type {IPSAppDEMobMapView}
     * @memberof AppDefaultMobMapViewLayout
     */
    viewInstance: IPSAppDEMobMapView;
    /**
     * @description 视图主容器内容
     * @return {*}
     * @memberof AppDefaultMobMapViewLayout
     */
    renderViewMainContainerContent(): any;
}
export declare const AppDefaultMobMapViewLayoutComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
