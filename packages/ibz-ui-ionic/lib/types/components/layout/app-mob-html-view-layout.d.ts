import { IPSAppDEMobHtmlView } from '@ibiz/dynamic-model-api';
import { AppMobHtmlViewLayoutProps } from 'ibz-core';
import { LayoutComponentBase } from './layout-component-base';
export declare class AppDefaultMobHtmlViewLayout extends LayoutComponentBase<AppMobHtmlViewLayoutProps> {
    /**
     * 移动端编辑视图实例对象
     *
     * @type {IPSAppDEMobHtmlView}
     * @memberof AppDefaultMobHtmlViewLayout
     */
    viewInstance: IPSAppDEMobHtmlView;
    /**
     * @description 视图主容器内容
     * @return {*}
     * @memberof AppDefaultMobHtmlViewLayout
     */
    renderViewMainContainerContent(): any;
}
export declare const AppDefaultMobHtmlViewLayoutComponent: import("@vue/runtime-core").DefineComponent<any, {}, {}, import("@vue/runtime-core").ComputedOptions, import("@vue/runtime-core").MethodOptions, import("@vue/runtime-core").ComponentOptionsMixin, import("@vue/runtime-core").ComponentOptionsMixin, {}, string, import("@vue/runtime-core").VNodeProps & import("@vue/runtime-core").AllowedComponentProps & import("@vue/runtime-core").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
