import { IPSAppDEMobWFDynaEditView } from '@ibiz/dynamic-model-api';
import { AppMobWFDynaEditViewLayoutProps } from 'ibz-core';
import { LayoutComponentBase } from './layout-component-base';
/**
 * @description 移动端动态工作流编辑视图视图布局组件
 * @export
 * @class AppDefaultMobWFDynaEditViewLayout
 * @extends {AppDefaultDeView<AppMobWFDynaEditViewLayoutProps>}
 */
export declare class AppDefaultMobWFDynaEditViewLayout extends LayoutComponentBase<AppMobWFDynaEditViewLayoutProps> {
    /**
     * @description 移动端动态工作流编辑视图实例对象
     * @type {IPSAppDEMobWFDynaEditView}
     * @memberof AppDefaultMobWFDynaEditViewLayout
     */
    viewInstance: IPSAppDEMobWFDynaEditView;
    /**
     * @description 渲染部件
     * @return {*}
     * @memberof AppDefaultMobWFDynaEditViewLayout
     */
    renderViewMainContainerContent(): any;
}
export declare const AppDefaultMobWFDynaEditViewLayoutComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
