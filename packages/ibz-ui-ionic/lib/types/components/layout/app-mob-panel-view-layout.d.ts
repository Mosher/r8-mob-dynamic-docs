import { IPSAppDEMobPanelView } from '@ibiz/dynamic-model-api';
import { AppMobPanelViewLayoutProps } from 'ibz-core';
import { LayoutComponentBase } from './layout-component-base';
export declare class AppDefaultMobPanelViewLayout extends LayoutComponentBase<AppMobPanelViewLayoutProps> {
    /**
     * 移动端面板视图实例对象
     *
     * @public
     * @type {IPSAppDEMobPanelView}
     * @memberof AppDefaultMobMDViewLayout
     */
    viewInstance: IPSAppDEMobPanelView;
    /**
     * @description 视图主容器内容
     * @return {*}
     * @memberof AppDefaultMobMDViewLayout
     */
    renderViewMainContainerContent(): any;
}
export declare const AppDefaultMobPanelViewLayoutComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
