import { EmitsOptions, SetupContext } from 'vue';
import { RouteLocationNormalizedLoaded, Router } from 'vue-router';
/**
 * 组件基类
 *
 * @export
 * @class ComponentBase
 * @template Props 输入属性接口
 */
export declare class ComponentBase<Props = any> {
    /**
     * 输入属性
     *
     * @protected
     * @type {Props}
     */
    protected props: Props;
    /**
     * vue运行上下文
     *
     * @protected
     * @type {SetupContext<EmitsOptions>}
     */
    protected ctx: SetupContext<EmitsOptions>;
    /**
     * 翻译
     *
     * @protected
     * @type {*}
     */
    protected $tl: any;
    /**
     * vue路由
     *
     * @protected
     * @type {RouteLocationNormalizedLoaded}
     */
    protected route: RouteLocationNormalizedLoaded;
    /**
     * vue路由器
     *
     * @protected
     * @type {Router}
     */
    protected router: Router;
    /**
     * 路由解析工具类
     *
     * @protected
     * @type {*}
     */
    pathToRegExp: any;
    /**
     * Creates an instance of ComponentBase.
     *
     * @param {Props} props
     * @param {SetupContext<EmitsOptions>} ctx
     */
    constructor(props: Props, ctx: SetupContext<EmitsOptions>);
    /**
     * 构建组件
     *
     * @memberof ComponentBase
     */
    setup(): void;
    /**
     * 初始化
     *
     * @memberof ComponentBase
     */
    init(): void;
    /**
     * 组件挂载完成
     *
     * @memberof ComponentBase
     */
    mounted(): void;
    /**
     * 绘制内容
     *
     * @return {*}  {*}
     * @memberof ComponentBase
     */
    render(): any;
    /**
     * 强制更新
     *
     * @memberof ComponentBase
     */
    forceUpdate(): void;
    /**
     * 输入属性变更执行
     *
     * @memberof ComponentBase
     */
    watchEffect(): void;
    /**
     * 组件销毁
     *
     * @memberof ComponentBase
     */
    unmounted(): void;
    renderSlot(name: string): any;
}
