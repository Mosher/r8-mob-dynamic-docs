import { Component, DefineComponent } from 'vue';
/**
 * 生成组件
 *
 * @author chitanda
 * @date 2021-05-18 11:05:46
 * @export
 * @param {*} ComponentClass 组件类
 * @param {*} [props] 传入的输入参数
 * @return {*}
 */
export declare function GenerateComponent(ComponentClass: any, props?: any, components?: Record<string, Component>): DefineComponent<any>;
