import { AppStepperProps, IMobStepperEditorController } from 'ibz-core';
import { EditorComponentBase } from './editor-component-base';
/**
 * 评分编辑器
 *
 * @export
 * @class AppStepperEditor
 * @extends {ComponentBase}
 */
export declare class AppStepperEditor extends EditorComponentBase<AppStepperProps> {
    /**
     * @description 编辑器控制器
     * @protected
     * @type {IMobStepperEditorController}
     * @memberof AppStepperEditor
     */
    protected c: IMobStepperEditorController;
    /**
     * @description 设置响应式
     * @memberof AppStepperEditor
     */
    setup(): void;
    /**
     * @description 设置编辑器组件
     * @memberof AppStepperEditor
     */
    setEditorComponent(): void;
    /**
     * @description 绘制内容
     * @return {*}
     * @memberof AppStepperEditor
     */
    render(): import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }> | null;
}
export declare const AppStepperEditorComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
