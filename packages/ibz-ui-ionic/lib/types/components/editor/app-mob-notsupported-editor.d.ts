import { AppEditorProps } from 'ibz-core';
import { EditorComponentBase } from './editor-component-base';
/**
 * 开关编辑器
 *
 * @export
 * @class AppMobNotSupportedEditor
 * @extends {EditorComponentBase}
 */
export declare class AppMobNotSupportedEditor extends EditorComponentBase<AppEditorProps> {
    /**
     * 设置响应式
     *
     * @public
     * @memberof AppMobNotSupportedEditor
     */
    setup(): void;
    /**
     * 绘制内容
     *
     * @public
     * @memberof AppMobNotSupportedEditor
     */
    render(): JSX.Element | null;
}
export declare const AppMobNotSupportedEditorComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
