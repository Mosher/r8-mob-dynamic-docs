import { AppDataPickerProps, IMobDataPickerEditorController } from 'ibz-core';
import { EditorComponentBase } from './editor-component-base';
/**
 * 时间选择编辑器
 *
 * @export
 * @class AppDataPickerEditor
 * @extends {ComponentBase}
 */
export declare class AppDataPickerEditor extends EditorComponentBase<AppDataPickerProps> {
    /**
     * @description 编辑器控制器
     * @protected
     * @type {IMobDataPickerEditorController}
     * @memberof AppDataPickerEditor
     */
    protected c: IMobDataPickerEditorController;
    /**
     * @description 设置响应式
     * @memberof AppDataPickerEditor
     */
    setup(): void;
    /**
     * @description 设置编辑器组件
     * @memberof AppDataPickerEditor
     */
    setEditorComponent(): void;
    /**
     * @description 绘制内容
     * @return {*}
     * @memberof AppDataPickerEditor
     */
    render(): import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }> | null;
}
export declare const AppDataPickerEditorComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
