import { AppRichTextProps, IMobRichTextEditorController } from 'ibz-core';
import { EditorComponentBase } from './editor-component-base';
/**
 * @description 富文本编辑器
 * @export
 * @class AppRichTextEditor
 * @extends {EditorComponentBase<AppRichTextProps>}
 */
export declare class AppRichTextEditor extends EditorComponentBase<AppRichTextProps> {
    /**
     * @description 编辑器控制器
     * @protected
     * @type {IMobRichTextEditorController}
     * @memberof AppRichTextEditor
     */
    protected c: IMobRichTextEditorController;
    /**
     * @description 设置响应式
     * @memberof AppRichTextEditor
     */
    setup(): void;
    /**
     * @description 设置编辑器组件
     * @memberof AppRichTextEditor
     */
    setEditorComponent(): void;
    /**
     * @description 绘制内容
     * @return {*}
     * @memberof AppRichTextEditor
     */
    render(): import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }> | null;
}
export declare const AppRichTextEditorComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
