import { AppDropdownListProps, IMobDropdownListController } from 'ibz-core';
import { EditorComponentBase } from './editor-component-base';
/**
 * 时间选择编辑器
 *
 * @export
 * @class AppDropdownListEditor
 * @extends {ComponentBase}
 */
export declare class AppDropdownListEditor extends EditorComponentBase<AppDropdownListProps> {
    /**
     * @description 编辑器控制器
     * @protected
     * @type {IMobDropdownListController}
     * @memberof AppDropdownListEditor
     */
    protected c: IMobDropdownListController;
    /**
     * @description 设置响应式
     * @memberof AppDropdownListEditor
     */
    setup(): void;
    /**
     * @description 设置编辑器组件
     * @memberof AppDropdownListEditor
     */
    setEditorComponent(): void;
    /**
     * @description 绘制内容
     * @return {*}
     * @memberof AppDropdownListEditor
     */
    render(): import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }> | null;
}
export declare const AppDropdownListEditorComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
