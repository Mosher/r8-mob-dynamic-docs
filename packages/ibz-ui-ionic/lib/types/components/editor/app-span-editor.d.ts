import { AppSpanProps, IMobSpanEditorController } from 'ibz-core';
import { EditorComponentBase } from './editor-component-base';
/**
 * 标签编辑器
 *
 * @export
 * @class AppSpanEditor
 * @extends {ComponentBase}
 */
export declare class AppSpanEditor extends EditorComponentBase<AppSpanProps> {
    /**
     * @description 编辑器控制器
     * @protected
     * @type {IMobSpanEditorController}
     * @memberof AppSpanEditor
     */
    protected c: IMobSpanEditorController;
    /**
     * @description 设置响应式
     * @memberof AppSpanEditor
     */
    setup(): void;
    /**
     * @description 设置编辑器组件
     * @memberof AppSpanEditor
     */
    setEditorComponent(): void;
    /**
     * @description 绘制内容
     * @return {*}
     * @memberof AppSpanEditor
     */
    render(): import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }> | null;
}
export declare const AppSpanEditorComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
