import { AppRatingProps, IMobRatingEditorController } from 'ibz-core';
import { EditorComponentBase } from './editor-component-base';
/**
 * 评分编辑器
 *
 * @export
 * @class AppRatingEditor
 * @extends {ComponentBase}
 */
export declare class AppRatingEditor extends EditorComponentBase<AppRatingProps> {
    /**
     * @description 编辑器控制器
     * @protected
     * @type {IMobRatingEditorController}
     * @memberof AppRatingEditor
     */
    protected c: IMobRatingEditorController;
    /**
     * @description 设置响应式
     * @memberof AppRatingEditor
     */
    setup(): void;
    /**
     * @description 设置编辑器组件
     * @memberof AppRatingEditor
     */
    setEditorComponent(): void;
    /**
     * @description 绘制内容
     * @return {*}
     * @memberof AppRatingEditor
     */
    render(): import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }> | null;
}
export declare const AppRatingEditorComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
