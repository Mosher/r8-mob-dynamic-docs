import { IAppEditorHooks, IEditorControllerBase, IEditorEventParam, MobRichTextEditorController, MobUploadEditorController } from 'ibz-core';
import { MobCheckBoxEditorController, MobDatePickerEditorController, MobDropdownListController, AppEditorProps, MobRatingEditorController, MobSliderEditorController, MobSpanEditorController, MobStepperEditorController, MobSwitchEditorController, MobTextboxEditorController, MobDataPickerEditorController } from 'ibz-core';
import { ComponentBase } from '../component-base/component-base';
/**
 * 编辑器组件基类
 *
 * @export
 * @class EditorComponentBase
 * @template Props 输入属性接口
 */
export declare class EditorComponentBase<Props extends AppEditorProps> extends ComponentBase<AppEditorProps> {
    /**
     * @description 编辑器控制器
     * @protected
     * @type {IEditorControllerBase}
     * @memberof EditorComponentBase
     */
    protected c: IEditorControllerBase;
    /**
     * @description 编辑器组件
     * @protected
     * @type {*}
     * @memberof EditorComponentBase
     */
    protected editorComponent: any;
    /**
     * @description 编辑器输入属性值变更
     * @memberof EditorComponentBase
     */
    watchEffect(): void;
    /**
     * @description 编辑器钩子
     * @readonly
     * @type {IAppEditorHooks}
     * @memberof EditorComponentBase
     */
    get hooks(): IAppEditorHooks;
    /**
     * @description 编辑器实例
     * @readonly
     * @memberof EditorComponentBase
     */
    get editorInstance(): import("@ibiz/dynamic-model-api").IPSEditor;
    /**
     * @description 根据编辑器类型获取编辑器控制器
     * @param {string} type 编辑器类型
     * @return {*}
     * @memberof EditorComponentBase
     */
    getEditorControllerByType(type: string): MobTextboxEditorController | MobSwitchEditorController | MobSliderEditorController | MobCheckBoxEditorController | MobDropdownListController | MobSpanEditorController | MobDatePickerEditorController | MobRatingEditorController | MobStepperEditorController | MobDataPickerEditorController | MobUploadEditorController | MobRichTextEditorController | null;
    /**
     * @description 构建组件
     * @memberof EditorComponentBase
     */
    setup(): void;
    /**
     * @description 初始化编辑器
     * @memberof EditorComponentBase
     */
    init(): void;
    /**
     * @description 部件销毁
     * @memberof EditorComponentBase
     */
    unmounted(): void;
    /**
     * @description 设置编辑器组件
     * @memberof EditorComponentBase
     */
    setEditorComponent(): void;
    /**
     * @description 抛出编辑器值change事件
     * @param {*} args 参数
     * @memberof EditorComponentBase
     */
    emitValueChange(args: any): void;
    /**
     * @description 执行编辑器事件
     * @param {IEditorEventParam} args
     * @memberof EditorComponentBase
     */
    emitEditorEvent(args: IEditorEventParam): void;
}
