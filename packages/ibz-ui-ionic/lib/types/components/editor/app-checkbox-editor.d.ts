import { AppCheckBoxProps, IMobCheckBoxEditorController } from 'ibz-core';
import { EditorComponentBase } from './editor-component-base';
export declare class AppCheckBoxEditor extends EditorComponentBase<AppCheckBoxProps> {
    /**
     * @description 编辑器控制器
     * @protected
     * @type {IMobCheckBoxEditorController}
     * @memberof AppCheckBoxEditor
     */
    protected c: IMobCheckBoxEditorController;
    /**
     * @description 设置响应式
     * @memberof AppCheckBoxEditor
     */
    setup(): void;
    /**
     * @description 设置编辑器组件
     * @memberof AppCheckBoxEditor
     */
    setEditorComponent(): void;
    /**
     * @description 绘制内容
     * @return {*}
     * @memberof AppCheckBoxEditor
     */
    render(): import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }> | null;
}
export declare const AppCheckBoxEditorComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
