import { AppSwitchProps, IMobSwitchEditorController } from 'ibz-core';
import { EditorComponentBase } from './editor-component-base';
/**
 * 开关编辑器
 *
 * @export
 * @class AppSwitchEditor
 * @extends {ComponentBase}
 */
export declare class AppSwitchEditor extends EditorComponentBase<AppSwitchProps> {
    /**
     * @description 编辑器控制器
     * @protected
     * @type {IMobSwitchEditorController}
     * @memberof AppSwitchEditor
     */
    protected c: IMobSwitchEditorController;
    /**
     * @description 设置响应式
     * @memberof AppSwitchEditor
     */
    setup(): void;
    /**
     * @description 设置编辑器组件
     * @memberof AppTextboxEditor
     */
    setEditorComponent(): void;
    /**
     * @description 绘制内容
     * @return {*}
     * @memberof AppSwitchEditor
     */
    render(): import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }> | null;
}
export declare const AppSwitchEditorComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
