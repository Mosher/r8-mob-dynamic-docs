import { AppUploadProps, IMobUploadEditorController } from 'ibz-core';
import { EditorComponentBase } from './editor-component-base';
/**
 * 上传组件编辑器
 *
 * @export
 * @class AppUploadEditor
 * @extends {ComponentBase}
 */
export declare class AppUploadEditor extends EditorComponentBase<AppUploadProps> {
    /**
     * @description 编辑器控制器
     * @protected
     * @type {IMobUploadEditorController}
     * @memberof AppUploadEditor
     */
    protected c: IMobUploadEditorController;
    /**
     * @description 设置响应式
     * @memberof AppUploadEditor
     */
    setup(): void;
    /**
     * @description 设置编辑器组件
     * @memberof AppUploadEditor
     */
    setEditorComponent(): void;
    /**
     * @description 绘制内容
     * @return {*}
     * @memberof AppUploadEditor
     */
    render(): import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }> | null;
}
export declare const AppUploadEditorComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
