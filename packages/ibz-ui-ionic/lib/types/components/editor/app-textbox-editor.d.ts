import { AppTextboxProps, IMobTextboxEditorController } from 'ibz-core';
import { EditorComponentBase } from './editor-component-base';
/**
 * 文本框编辑器
 *
 * @export
 * @class AppTextboxEditor
 * @extends {ComponentBase}
 */
export declare class AppTextboxEditor extends EditorComponentBase<AppTextboxProps> {
    /**
     * @description 编辑器控制器
     * @protected
     * @type {IMobTextboxEditorController}
     * @memberof AppTextboxEditor
     */
    protected c: IMobTextboxEditorController;
    /**
     * @description 设置响应式
     * @memberof AppTextboxEditor
     */
    setup(): void;
    /**
     * @description 设置编辑器组件
     * @memberof AppTextboxEditor
     */
    setEditorComponent(): void;
    /**
     * @description 绘制内容
     * @return {*}
     * @memberof AppTextboxEditor
     */
    render(): import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }> | null;
}
export declare const AppTextboxEditorComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
