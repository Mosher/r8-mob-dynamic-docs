import { AppDatePickerProps, IMobDatePickerEditorController } from 'ibz-core';
import { EditorComponentBase } from './editor-component-base';
/**
 * 时间选择编辑器
 *
 * @export
 * @class AppDatePickerEditor
 * @extends {ComponentBase}
 */
export declare class AppDatePickerEditor extends EditorComponentBase<AppDatePickerProps> {
    /**
     * @description 编辑器控制器
     * @protected
     * @type {IMobDatePickerEditorController}
     * @memberof AppDatePickerEditor
     */
    protected c: IMobDatePickerEditorController;
    /**
     * @description 设置响应式
     * @memberof AppDatePickerEditor
     */
    setup(): void;
    /**
     * @description 设置编辑器组件
     * @memberof AppDatePickerEditor
     */
    setEditorComponent(): void;
    /**
     * @description 绘制内容
     * @return {*}
     * @memberof AppDatePickerEditor
     */
    render(): import("vue").VNode<import("vue").RendererNode, import("vue").RendererElement, {
        [key: string]: any;
    }> | null;
}
export declare const AppDatePickerEditorComponent: import("vue").DefineComponent<any, {}, {}, import("vue").ComputedOptions, import("vue").MethodOptions, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, {}, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<any>, {} | {
    [x: string]: any;
}>;
