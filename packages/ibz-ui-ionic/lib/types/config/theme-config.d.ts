/**
 * @description 预置主题
 */
export declare const PresetTheme: {
    name: string;
    title: string;
    lanResTag: string;
    color: string;
}[];
/**
 * @description 主题配置项
 */
export declare const ThemeOptions: ({
    name: string;
    title: string;
    lanResTag: string;
    items: {
        name: string;
        title: string;
        lanResTag: string;
        default: string;
        shade: string;
        tint: string;
    }[];
} | {
    name: string;
    title: string;
    lanResTag: string;
    items: {
        name: string;
        title: string;
        lanResTag: string;
        default: string;
    }[];
})[];
