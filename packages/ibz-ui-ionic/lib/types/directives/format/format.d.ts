/**
 * 格式化指令
 *
 */
export declare const Format: any;
/**
 * @description 格式化控制器
 * @export
 * @class FormatController
 */
export declare class FormatController {
    /**
     * @description 唯一实例
     * @private
     * @static
     * @memberof FormatController
     */
    private static readonly instance;
    /**
     * @description 容器
     * @protected
     * @type {HTMLElement}
     * @memberof FormatController
     */
    protected el: HTMLElement;
    /**
     * Creates an instance of FormatController.
     * @memberof FormatController
     */
    private constructor();
    /**
     * @description 获取唯一实例
     * @static
     * @return {*}  {FormatController}
     * @memberof FormatController
     */
    static getInstance(): FormatController;
    /**
     * @description 初始化
     * @param {HTMLDivElement} el 元素
     * @param {*} binding 绑定值
     * @param {*} vnode 节点
     * @memberof FormatController
     */
    init(el: HTMLDivElement, binding: any, vNode: any): void;
    /**
     * @description 格式化时间
     * @private
     * @param {*} newValue 新值
     * @param {*} oldValue 原始值
     * @param {string} dateFormat 日期格式化
     * @return {*}
     * @memberof FormatController
     */
    private formatDate;
    /**
     * @description 格式化数据
     * @private
     * @param {string} value 值
     * @param {string} formatRule 格式化规则
     * @return {*}
     * @memberof FormatController
     */
    private formatData;
    /**
     * @description 处理格式化规则
     * 根据格式化规则对 正 负 零 区分显示规则处理
     * @private
     * @param {*} value 值
     * @param {string} formatRule 格式化规则
     * @return {*}  {string}
     * @memberof FormatController
     */
    private handleFormatRules;
    /**
     * @description 解析数字
     * @param {number} num 值
     * @param {number} placeholder 占位数
     * @return {*}
     * @memberof FormatController
     */
    private parseNumber;
    /**
     * @description Html
     * @param {*} [value0, value1, value2, style]
     * @return {*}
     * @memberof FormatController
     */
    private encodeHtml;
}
export declare const format: FormatController;
