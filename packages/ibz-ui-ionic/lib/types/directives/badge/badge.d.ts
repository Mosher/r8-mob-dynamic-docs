/**
 * 微标指令
 *
 * @export
 * @class Badge
 */
export declare const Badge: any;
/**
 * 微标控制器
 *
 * @export
 * @class BadgeController
 */
export declare class BadgeController {
    /**
     * 唯一实例
     *
     * @private
     * @static
     * @memberof BadgeControllerController
     */
    private static readonly instance;
    /**
     * 容器
     *
     * @protected
     * @type {HTMLDivElement}
     * @memberof NotificationSignalController
     */
    protected el: HTMLElement;
    /**
     * Creates an instance of BadgeControllerController.
     * @memberof BadgeControllerController
     */
    private constructor();
    /**
     * 初始化
     *
     * @param {HTMLDivElement}
     * @param {any}
     * @memberof BadgeController
     */
    init(el: HTMLDivElement, binding: any): void;
    /**
     * 更新
     *
     * @param {HTMLDivElement}
     * @param {any}
     * @memberof BadgeController
     */
    update(el: HTMLDivElement, binding: any): void;
    /**
     * 设置徽标类型样式
     *
     * @param {string}
     * @memberof BadgeController
     */
    setBadgeclass(type: string): void;
    /**
     * 设置徽标偏移量
     *
     * @param {string}
     * @memberof BadgeController
     */
    setBadgeOffset(offset: Array<number>): void;
    /**
     * 获取唯一实例
     *
     * @static
     * @returns {BadgeController}
     * @memberof BadgeController
     */
    static getInstance(): BadgeController;
}
export declare const bc: BadgeController;
