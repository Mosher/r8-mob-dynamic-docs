declare function getLocaleResource(): {
    input: {
        maxlength: string;
    };
    datapicker: {
        nopickupview: string;
        nolinkview: string;
    };
    radiolist: {
        notfount: string;
    };
    richtext: {
        uploadfailed: string;
    };
    span: {
        nocodelist: string;
    };
    textarea: {
        maxlength: string;
    };
    upload: {
        file: string;
        nofile: string;
        notpicture: string;
        nopicture: string;
        uploadfile: string;
        uploadfailure: string;
        sizeover: string;
        uploadlimitamount: string;
    };
    dashboard: {
        close: string;
        customdatakanban: string;
        existcard: string;
        noexistcard: string;
        nodescription: string;
    };
    calendar: {
        today: string;
        Monday: string;
        Tuesday: string;
        Wednesday: string;
        Thursday: string;
        Friday: string;
        Saturday: string;
        Sunday: string;
        January: string;
        February: string;
        March: string;
        April: string;
        May: string;
        June: string;
        July: string;
        August: string;
        September: string;
        October: string;
        November: string;
        December: string;
    };
};
export default getLocaleResource;
