declare function getLocaleResource(): {
    mobhtmlview: {
        notexist: string;
    };
    notsupportview: {
        tip: string;
    };
    wfdynaactionview: {
        submit: string;
    };
};
export default getLocaleResource;
