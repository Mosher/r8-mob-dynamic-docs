declare function getLocaleResource(): {
    mobformdruipart: {
        tooltip: string;
    };
    mobformgroup: {
        hidden: string;
        more: string;
    };
    mobdashboard: {
        title: string;
    };
    mobmeditviewpanel: {
        noembeddedview: string;
    };
    mobpanel: {
        nomarkdown: string;
    };
    mobpickupviewpanel: {
        nopickerview: string;
    };
    mobportlet: {
        nomarkdown: string;
        toolbar: string;
        rendercustom: string;
    };
    mobsearchform: {
        detailtype: string;
        filter: string;
        search: string;
        reset: string;
    };
    mobtabviewpanel: {
        noembeddedview: string;
    };
    mobtoolbar: {
        exportmaxrow: string;
        exportcurpage: string;
        notsupport: string;
    };
};
export default getLocaleResource;
