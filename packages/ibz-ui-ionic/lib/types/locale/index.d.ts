export { en_US } from './lang/en_US';
export { zh_CN } from './lang/zh_CN';
export declare const translate: Function;
export declare const handleLocaleMap: Function;
