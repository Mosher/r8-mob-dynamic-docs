export declare const en_US: () => {
    share: {
        ok: string;
        cancel: string;
        notsupported: string;
        widget: string;
        previous: string;
        next: string;
        finish: string;
        emptytext: string;
        year: string;
        month: string;
        day: string;
        loading: string;
    };
    common: {
        input: {
            maxlength: string;
        };
        datapicker: {
            nopickupview: string;
            nolinkview: string;
        };
        radiolist: {
            notfount: string;
        };
        richtext: {
            uploadfailed: string;
        };
        span: {
            nocodelist: string;
        };
        textarea: {
            maxlength: string;
        };
        upload: {
            file: string;
            nofile: string;
            notpicture: string;
            nopicture: string;
            uploadfile: string;
            uploadfailure: string;
            sizeover: string;
            uploadlimitamount: string;
        };
        dashboard: {
            close: string;
            customdatakanban: string;
            existcard: string;
            noexistcard: string;
            nodescription: string;
        };
        calendar: {
            today: string;
            Monday: string;
            Tuesday: string;
            Wednesday: string;
            Thursday: string;
            Friday: string;
            Saturday: string;
            Sunday: string;
            January: string;
            February: string;
            March: string;
            April: string;
            May: string;
            June: string;
            July: string;
            August: string;
            September: string;
            October: string;
            November: string;
            December: string;
        };
    };
    view: {
        mobhtmlview: {
            notexist: string;
        };
        notsupportview: {
            tip: string;
        };
        wfdynaactionview: {
            submit: string;
        };
    };
    widget: {
        mobformdruipart: {
            tooltip: string;
        };
        mobformgroup: {
            hidden: string;
            more: string;
        };
        mobdashboard: {
            title: string;
        };
        mobmeditviewpanel: {
            noembeddedview: string;
        };
        mobpanel: {
            nomarkdown: string;
        };
        mobpickupviewpanel: {
            nopickerview: string;
        };
        mobportlet: {
            nomarkdown: string;
            toolbar: string;
            rendercustom: string;
        };
        mobsearchform: {
            detailtype: string;
            filter: string;
            search: string;
            reset: string;
        };
        mobtabviewpanel: {
            noembeddedview: string;
        };
        mobtoolbar: {
            exportmaxrow: string;
            exportcurpage: string;
            notsupport: string;
        };
    };
};
