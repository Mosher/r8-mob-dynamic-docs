import { IAppThemeService, IParam } from 'ibz-core';
/**
 * @description 应用主题服务
 * @export
 * @class AppThemeService
 * @implements {IAppThemeService}
 */
export declare class AppThemeService implements IAppThemeService {
    /**
     * @description 应用主题服务实例对象
     * @private
     * @static
     * @type {AppThemeService}
     * @memberof AppThemeService
     */
    private static instance;
    /**
     * @description 预置主题
     * @private
     * @type {IParam[]}
     * @memberof AppThemeService
     */
    private presetTheme;
    /**
     * @description 主题配置
     * @private
     * @type {IParam[]}
     * @memberof AppThemeService
     */
    private themeOptions;
    /**
     * @description <html> dom对象
     * @private
     * @type {HTMLElement}
     * @memberof AppThemeService
     */
    private htmlElement;
    /**
     * Creates an instance of AppThemeService.
     * @memberof AppThemeService
     */
    constructor();
    /**
     * @description 获取应用主题服务(单例模式)
     * @static
     * @return {AppThemeService}
     * @memberof AppThemeService
     */
    static getInstance(): AppThemeService;
    /**
     * @description 初始化应用主题
     * @return {*}  {{ themeOptions: IParam[], activeTheme: { isCustom: boolean, theme: string } }}
     * @memberof AppThemeService
     */
    initAppTheme(): {
        themeOptions: IParam[];
        activeTheme: {
            isCustom: boolean;
            theme: string;
        };
    };
    /**
     * @description 获取当前激活主题
     * @return {*}  {{ isCustom: boolean, theme: string }}
     * @memberof AppThemeService
     */
    getActiveUITheme(): {
        isCustom: boolean;
        theme: string;
    };
    /**
     * @description 设置当前激活主题
     * @param {string} theme 主题
     * @param {boolean} [isCustom=false] 是否自定义
     * @memberof AppThemeService
     */
    setActiveUITheme(theme: string, isCustom?: boolean): void;
    /**
     * @description 获取预置主题
     * @return {*}  {IParam[]}
     * @memberof AppThemeService
     */
    getPresetTheme(): IParam[];
    /**
     * @description 设置自定义配置
     * @param {IParam} item 自定义配置
     * @memberof AppThemeService
     */
    setCustomOptions(item: IParam): void;
    /**
     * @description 删除自定义配置
     * @param {IParam} item 自定义配置
     * @memberof AppThemeService
     */
    removeCustomOptions(item: IParam): void;
    /**
     * @description 根据标识获取css变量值
     * @param {string} tag 变量标识
     * @return {*}  {string}
     * @memberof AppThemeService
     */
    getCssVariablet(tag: string): string;
    /**
     * @description 获取主题配置
     * @return {*}  {IParam[]}
     * @memberof AppThemeService
     */
    getThemeOptions(): IParam[];
    /**
     * @description 应用自定义主题
     * @param {string} theme 主题基调
     * @param {IParam[]} options 配置
     * @memberof AppThemeService
     */
    applyCustomTheme(theme: string, options: IParam[]): void;
    /**
     * @description 查找更改的自定义配置
     * @private
     * @param {IParam[]} options
     * @return {*}  {IParam[]}
     * @memberof AppThemeService
     */
    private findChangeOptions;
    /**
     * @description 切换预置主题
     * @param {string} oldVal 旧主题标识
     * @param {string} newVal 新主题标识
     * @param {IParam[]} options 主题配置
     * @return {*}
     * @memberof AppThemeService
     */
    changeInternalTheme(oldVal: string, newVal: string, options: IParam[]): void;
    /**
     * @description 重置主题
     * @param {*} options 主题配置
     * @memberof AppThemeService
     */
    resetAppTheme(options: any): void;
    /**
     * @description 根据key删除本地存储
     * @private
     * @param {string} key
     * @memberof AppThemeService
     */
    private removeLocalStorage;
}
