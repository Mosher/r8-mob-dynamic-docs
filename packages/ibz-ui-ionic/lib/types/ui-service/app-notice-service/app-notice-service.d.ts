import { IAppNoticeService } from 'ibz-core';
/**
 * 消息提示
 *
 * @export
 * @class AppNoticeService
 */
export declare class AppNoticeService implements IAppNoticeService {
    /**
     * 唯一实例
     *
     * @private
     * @static
     * @type {AppNoticeService}
     * @memberof AppNoticeService
     */
    private static readonly instance;
    /**
     * Creates an instance of AppNoticeService.
     * @memberof AppNoticeService
     */
    constructor();
    /**
     * 消息提示
     *
     * @param {string} message
     * @param {number} [time]
     * @memberof AppNoticeService
     */
    info(message: string, time?: number): void;
    /**
     * 成功提示
     *
     * @param {string} message
     * @param {number} [time]
     * @memberof AppNoticeService
     */
    success(message: string, time?: number): void;
    /**
     * 警告提示
     *
     * @param {string} message
     * @param {number} [time]
     * @memberof AppNoticeService
     */
    warning(message: string, time?: number): void;
    /**
     * 错误提示
     *
     * @param {string} message
     * @param {number} [time]
     * @memberof AppNoticeService
     */
    error(message: string, time?: number): void;
    /**
     * 创建对象
     *
     * @private
     * @param {string} type
     * @param {string} message
     * @param {number} [time]
     * @memberof AppNoticeService
     */
    private createToast;
    /**
     * 获取实例
     *
     * @static
     * @returns {AppNoticeService}
     * @memberof AppNoticeService
     */
    static getInstance(): AppNoticeService;
}
