import { IAppActionSheetService, ICtrlActionResult, IAppActionSheetOptions } from 'ibz-core';
/**
 * 动作面板服务
 *
 * @class AppActionSheetService
 * @description 底部弹出的模态面板
 */
export declare class AppActionSheetService implements IAppActionSheetService {
    /**
     * @description 动态面板服务实例对象（单例）
     * @protected
     * @static
     * @type {AppActionSheetService}
     * @memberof AppActionSheetService
     */
    protected static instance: AppActionSheetService;
    /**
     * @description 获取动态面板服务实例对象
     * @static
     * @return {*}  {IAppActionSheetService}
     * @memberof AppActionSheetService
     */
    static getInstance(): IAppActionSheetService;
    /**
     * @description 创建动态面板
     * @param {IAppActionSheetOptions} options 动态面板配置
     * @return {*}  {Promise<ICtrlActionResult>}
     * @memberof AppActionSheetService
     */
    create(options: IAppActionSheetOptions): Promise<ICtrlActionResult>;
    /**
     * @description 配置转换
     * @private
     * @param {IAppActionSheetOptions} options 动态面板配置
     * @return {*}  {IParam}
     * @memberof AppActionSheetService
     */
    private configConvert;
}
