import { IComponentService } from 'ibz-core';
/**
 * 应用组件服务
 *
 * @memberof AppComponentService
 */
export declare class AppComponentService implements IComponentService {
    /**
     * @description 单例变量声明
     *
     * @private
     * @static
     * @type {AppComponentService}
     * @memberof AppComponentService
     */
    private static AppComponentService;
    /**
     * @description 视图类型组件Map
     * @protected
     * @type {Map<string, any>}
     * @memberof AppComponentService
     */
    protected viewTypeMap: Map<string, any>;
    /**
     * @description 视图布局组件Map
     * @protected
     * @type {Map<string, any>}
     * @memberof AppComponentService
     */
    protected layoutMap: Map<string, any>;
    /**
     * @description 部件组件Map
     * @protected
     * @type {Map<string, any>}
     * @memberof AppComponentService
     */
    protected controlMap: Map<string, any>;
    /**
     * 控件组件Map
     *
     * @memberof AppComponentService
     */
    protected editorMap: Map<string, any>;
    /**
     * Creates an instance of AppComponentService.
     * @memberof AppComponentService
     */
    constructor();
    /**
     * 获取 AppComponentService 单例对象
     *
     * @static
     * @returns {AppComponentService}
     * @memberof AppComponentService
     */
    static getInstance(): AppComponentService;
    /**
     * @description 注册应用组件
     * @memberof AppComponentService
     */
    registerAppComponents(): void;
    /**
     * @description 注册布局组件
     * @memberof AppComponentService
     */
    registerLayoutComponent(): void;
    /**
     * @description 获取布局组件
     * @param {string} viewType 布局类型
     * @param {string} viewStyle 布局样式
     * @param {string} [pluginCode] 插件代码
     * @return {string}
     * @memberof AppComponentService
     */
    getLayoutComponent(viewType: string, viewStyle: string, pluginCode?: string): any;
    /**
     * @description 注册视图类型组件
     * @return {*}
     * @memberof AppComponentService
     */
    protected registerViewTypeComponents(): void;
    /**
     * @description 获取视图类型组件
     * @param {string} viewType 视图类型
     * @param {string} viewStyle 视图样式
     * @param {string} [pluginCode] 插件代码
     * @return {string}
     * @memberof AppComponentService
     */
    getViewTypeComponent(viewType: string, viewStyle?: string, pluginCode?: string): any;
    /**
     * @description 注册部件组件
     * @return {*}
     * @memberof AppComponentService
     */
    protected registerControlComponents(): void;
    /**
     * @description 获取部件组件
     * @param {string} ctrlType 部件类型
     * @param {string} ctrlStyle 部件样式
     * @param {string} [pluginCode] 插件标识
     * @return {string}
     * @memberof AppComponentService
     */
    getControlComponent(ctrlType: string, ctrlStyle?: string, pluginCode?: string): any;
    /**
     * @description 注册编辑器组件
     * @protected
     * @memberof AppComponentService
     */
    protected registerEditorComponents(): void;
    /**
     * @description 获取编辑器组件
     * @param {string} editorType 编辑器类型
     * @param {string} editorStyle 编辑器样式
     * @param {string} [pluginCode] 编辑器插件标识
     * @return {string}
     * @memberof AppComponentService
     */
    getEditorComponent(editorType: string, editorStyle?: string, pluginCode?: string, infoMode?: boolean): any;
    /**
     * @description 计算信息模式编辑器
     * @param {string} editorType 编辑器类型
     * @param {string} editorStyle 编辑器样式
     * @return {*}  {string}
     * @memberof AppComponentService
     */
    computeInfoModeEditor(editorType: string, editorStyle: string): any;
}
