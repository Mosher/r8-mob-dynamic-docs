import { IAppThirdPartyService } from 'ibz-core';
import { DingTalkService } from './ding-talk-service';
import { WeChatService } from './we-chat-service';
/**
 * 第三方服务
 *
 * @export
 * @class AppThirdPartyService
 */
export declare class AppThirdPartyService implements IAppThirdPartyService {
    /**
     * 唯一实例
     *
     * @private
     * @static
     * @type {AppThirdPartyService}
     * @memberof AppThirdPartyService
     */
    private static instance;
    /**
     * 当前搭载平台服务类
     *
     * @type {(DingTalkService|WeChatService)}
     * @memberof AppThirdPartyService
     */
    private platformService;
    /**
     * 搭载平台
     *
     * @private
     * @type {string}
     * @memberof AppThirdPartyService
     */
    private platform;
    /**
     * Creates an instance of AppThirdPartyService.
     * @memberof AppThirdPartyService
     */
    private constructor();
    /**
     *  第三方初始化
     *
     * @memberof AppThirdPartyService
     */
    thirdPartyInit(): void;
    /**
     * 获取实例
     *
     * @static
     * @return {*}  {AppThirdPartyService}
     * @memberof AppThirdPartyService
     */
    static getInstance(): AppThirdPartyService;
    /**
     * 初始化搭载平台
     *
     * @memberof AppThirdPartyService
     */
    private initPlatformService;
    /**
     * 初始化搭载平台
     *
     * @memberof AppThirdPartyService
     */
    initAppPlatForm(): void;
    /**
     * 获取APP搭载平台
     *
     * @memberof AppThirdPartyService
     */
    getAppPlatform(): Promise<'ios' | 'android' | 'dd' | 'wx-mini' | 'wx-pn' | 'desktop' | 'web'>;
    /**
     * 获取当前搭载平台服务类
     *
     * @return {*}
     * @memberof AppThirdPartyService
     */
    getPlatformService(): DingTalkService | WeChatService;
    /**
     * 清楚登录用户信息
     *
     * @memberof AppThirdPartyService
     */
    clearUserInfo(): void;
    /**
     * 获取用户信息
     *
     * @returns {*}
     * @memberof AppThirdPartyService
     */
    getUserInfo(): Promise<any>;
    /**
     * 关闭应用
     *
     * @memberof AppThirdPartyService
     */
    close(): void;
    /**
     * 第三方事件
     *
     * @param {string} tag
     * @param {*} [arg={}]
     * @return {*}  {Promise<any>}
     * @memberof AppThirdPartyService
     */
    thirdPartyEvent(tag: string, arg?: any): Promise<any>;
}
