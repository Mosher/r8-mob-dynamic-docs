/**
 * 微信服务
 *
 * @export
 * @class WeChatService
 */
export declare class WeChatService {
    /**
     * 唯一实例
     *
     * @private
     * @static
     * @type {WeChatService}
     * @memberof WeChatService
     */
    private static instance;
    /**
     * 用户信息缓存key
     *
     * @private
     * @type {string}
     * @memberof WeChatService
     */
    private readonly infoName;
    /**
     * 企业corpId
     *
     * @private
     * @type {string}
     * @memberof WeChatService
     */
    private readonly appId;
    /**
     * 微信SDK
     *
     * @protected
     * @type {*}
     * @memberof WeChatService
     */
    protected wx: any;
    /**
     * http请求服务
     *
     * @protected
     * @memberof ThirdPartyService
     */
    protected http: import("../../../../ibz-core/types").IHttp;
    /**
     * 是否初始化
     *
     * @protected
     * @type {boolean}
     * @memberof WeChatService
     */
    protected $isInit: boolean;
    get isInit(): boolean;
    /**
     * Creates an instance of WeChatService.
     * @memberof WeChatService
     */
    private constructor();
    /**
     * 获取实例
     *
     * @static
     * @returns {WeChatService}
     * @memberof WeChatService
     */
    static getInstance(): WeChatService;
    /**
     * 是否是小程序
     *
     * @memberof WeChatService
     */
    isMini(): Promise<unknown>;
    /**
     * 初始化
     *
     * @protected
     * @returns {Promise<void>}
     * @memberof WeChatService
     */
    protected init(): Promise<void>;
    /**
     * 登录
     *
     * @returns {Promise<any>}
     * @memberof WeChatService
     */
    login(): Promise<any>;
    /**
     * 清楚登录用户信息
     *
     * @memberof WeChatService
     */
    clearUserInfo(): void;
    /**
     * 获取用户信息
     *
     * @returns {*}
     * @memberof WeChatService
     */
    getUserInfo(): any;
    /**
     * 获取url参数
     *
     * @private
     * @param {string} queryName
     * @returns
     * @memberof WeChatService
     */
    private getQueryValue1;
    /**
     * 关闭微信应用
     *
     * @static
     * @memberof WeChatService
     */
    close(): void;
    /**
     * 设置微信标题
     *
     * @static
     * @memberof WeChatService
     */
    setTitle(title: string): void;
    /**
     * 微信导航栏返回事件
     *
     * @static
     * @memberof WeChatService
     */
    private backEvent;
    /**
     * 设置微信导航栏返回事件
     *
     * @static
     * @memberof WeChatService
     */
    setBackEvent(event: Array<Function>): void;
    /**
     *
     * @static
     * @memberof WeChatService
     */
    getNetworkType(): void;
    /**
     * wx事件
     *
     * @param {string} tag
     * @param {*} [arg={}]
     * @memberof WeChatService
     */
    event(tag: string, arg?: any): void;
}
