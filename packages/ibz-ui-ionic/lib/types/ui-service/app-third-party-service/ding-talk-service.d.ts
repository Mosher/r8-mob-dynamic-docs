/**
 * 钉钉服务
 *
 * @export
 * @class DingTalkService
 */
export declare class DingTalkService {
    /**
     * 唯一实例
     *
     * @private
     * @static
     * @memberof DingTalkService
     */
    private static instance;
    /**
     * 用户信息缓存key
     *
     * @private
     * @type {string}
     * @memberof DingTalkService
     */
    private readonly infoName;
    /**
     * 钉钉sdk
     *
     * @memberof DingTalkService
     */
    readonly dd: typeof import("dingtalk-jsapi/lib/otherApi") & import("dingtalk-jsapi/lib/sdk").IUNCore & {
        biz: {
            ATMBle: {
                beaconPicker: typeof import("dingtalk-jsapi/api/biz/ATMBle/beaconPicker").beaconPicker$;
                detectFace: typeof import("dingtalk-jsapi/api/biz/ATMBle/detectFace").detectFace$;
                detectFaceFullScreen: typeof import("dingtalk-jsapi/api/biz/ATMBle/detectFaceFullScreen").detectFaceFullScreen$;
                faceManager: typeof import("dingtalk-jsapi/api/biz/ATMBle/faceManager").faceManager$;
                punchModePicker: typeof import("dingtalk-jsapi/api/biz/ATMBle/punchModePicker").punchModePicker$;
            };
            alipay: {
                openAuth: typeof import("dingtalk-jsapi/api/biz/alipay/openAuth").openAuth$;
                pay: typeof import("dingtalk-jsapi/api/biz/alipay/pay").pay$;
            };
            auth: {
                openAccountPwdLoginPage: typeof import("dingtalk-jsapi/api/biz/auth/openAccountPwdLoginPage").openAccountPwdLoginPage$;
                requestAuthInfo: typeof import("dingtalk-jsapi/api/biz/auth/requestAuthInfo").requestAuthInfo$;
            };
            calendar: {
                chooseDateTime: typeof import("dingtalk-jsapi/api/biz/calendar/chooseDateTime").chooseDateTime$;
                chooseHalfDay: typeof import("dingtalk-jsapi/api/biz/calendar/chooseHalfDay").chooseHalfDay$;
                chooseInterval: typeof import("dingtalk-jsapi/api/biz/calendar/chooseInterval").chooseInterval$;
                chooseOneDay: typeof import("dingtalk-jsapi/api/biz/calendar/chooseOneDay").chooseOneDay$;
            };
            chat: {
                chooseConversationByCorpId: typeof import("dingtalk-jsapi/api/biz/chat/chooseConversationByCorpId").chooseConversationByCorpId$;
                collectSticker: typeof import("dingtalk-jsapi/api/biz/chat/collectSticker").collectSticker$;
                createSceneGroup: typeof import("dingtalk-jsapi/api/biz/chat/createSceneGroup").createSceneGroup$;
                getRealmCid: typeof import("dingtalk-jsapi/api/biz/chat/getRealmCid").getRealmCid$;
                locationChatMessage: typeof import("dingtalk-jsapi/api/biz/chat/locationChatMessage").locationChatMessage$;
                openSingleChat: typeof import("dingtalk-jsapi/api/biz/chat/openSingleChat").openSingleChat$;
                pickConversation: typeof import("dingtalk-jsapi/api/biz/chat/pickConversation").pickConversation$;
                sendEmotion: typeof import("dingtalk-jsapi/api/biz/chat/sendEmotion").sendEmotion$;
                toConversation: typeof import("dingtalk-jsapi/api/biz/chat/toConversation").toConversation$;
                toConversationByOpenConversationId: typeof import("dingtalk-jsapi/api/biz/chat/toConversationByOpenConversationId").toConversationByOpenConversationId$;
            };
            clipboardData: {
                setData: typeof import("dingtalk-jsapi/api/biz/clipboardData/setData").setData$;
            };
            conference: {
                createCloudCall: typeof import("dingtalk-jsapi/api/biz/conference/createCloudCall").createCloudCall$;
                getCloudCallInfo: typeof import("dingtalk-jsapi/api/biz/conference/getCloudCallInfo").getCloudCallInfo$;
                getCloudCallList: typeof import("dingtalk-jsapi/api/biz/conference/getCloudCallList").getCloudCallList$;
                videoConfCall: typeof import("dingtalk-jsapi/api/biz/conference/videoConfCall").videoConfCall$;
            };
            contact: {
                choose: typeof import("dingtalk-jsapi/api/biz/contact/choose").choose$;
                chooseMobileContacts: typeof import("dingtalk-jsapi/api/biz/contact/chooseMobileContacts").chooseMobileContacts$;
                complexPicker: typeof import("dingtalk-jsapi/api/biz/contact/complexPicker").complexPicker$;
                createGroup: typeof import("dingtalk-jsapi/api/biz/contact/createGroup").createGroup$;
                departmentsPicker: typeof import("dingtalk-jsapi/api/biz/contact/departmentsPicker").departmentsPicker$;
                externalComplexPicker: typeof import("dingtalk-jsapi/api/biz/contact/externalComplexPicker").externalComplexPicker$;
                externalEditForm: typeof import("dingtalk-jsapi/api/biz/contact/externalEditForm").externalEditForm$;
                setRule: typeof import("dingtalk-jsapi/api/biz/contact/setRule").setRule$;
            };
            cspace: {
                chooseSpaceDir: typeof import("dingtalk-jsapi/api/biz/cspace/chooseSpaceDir").chooseSpaceDir$;
                delete: typeof import("dingtalk-jsapi/api/biz/cspace/delete").delete$;
                preview: typeof import("dingtalk-jsapi/api/biz/cspace/preview").preview$;
                saveFile: typeof import("dingtalk-jsapi/api/biz/cspace/saveFile").saveFile$;
            };
            customContact: {
                choose: typeof import("dingtalk-jsapi/api/biz/customContact/choose").choose$;
                multipleChoose: typeof import("dingtalk-jsapi/api/biz/customContact/multipleChoose").multipleChoose$;
            };
            ding: {
                create: typeof import("dingtalk-jsapi/api/biz/ding/create").create$;
                post: typeof import("dingtalk-jsapi/api/biz/ding/post").post$;
            };
            edu: {
                finishMiniCourseByRecordId: typeof import("dingtalk-jsapi/api/biz/edu/finishMiniCourseByRecordId").finishMiniCourseByRecordId$;
                getMiniCourseDraftList: typeof import("dingtalk-jsapi/api/biz/edu/getMiniCourseDraftList").getMiniCourseDraftList$;
                joinClassroom: typeof import("dingtalk-jsapi/api/biz/edu/joinClassroom").joinClassroom$;
                makeMiniCourse: typeof import("dingtalk-jsapi/api/biz/edu/makeMiniCourse").makeMiniCourse$;
            };
            event: {
                notifyWeex: typeof import("dingtalk-jsapi/api/biz/event/notifyWeex").notifyWeex$;
            };
            intent: {
                fetchData: typeof import("dingtalk-jsapi/api/biz/intent/fetchData").fetchData$;
            };
            iot: {
                bind: typeof import("dingtalk-jsapi/api/biz/iot/bind").bind$;
                bindMeetingRoom: typeof import("dingtalk-jsapi/api/biz/iot/bindMeetingRoom").bindMeetingRoom$;
                getDeviceProperties: typeof import("dingtalk-jsapi/api/biz/iot/getDeviceProperties").getDeviceProperties$;
                invokeThingService: typeof import("dingtalk-jsapi/api/biz/iot/invokeThingService").invokeThingService$;
                queryMeetingRoomList: typeof import("dingtalk-jsapi/api/biz/iot/queryMeetingRoomList").queryMeetingRoomList$;
                setDeviceProperties: typeof import("dingtalk-jsapi/api/biz/iot/setDeviceProperties").setDeviceProperties$;
                unbind: typeof import("dingtalk-jsapi/api/biz/iot/unbind").unbind$;
            };
            live: {
                startClassRoom: typeof import("dingtalk-jsapi/api/biz/live/startClassRoom").startClassRoom$;
                startUnifiedLive: typeof import("dingtalk-jsapi/api/biz/live/startUnifiedLive").startUnifiedLive$;
            };
            map: {
                locate: typeof import("dingtalk-jsapi/api/biz/map/locate").locate$;
                search: typeof import("dingtalk-jsapi/api/biz/map/search").search$;
                view: typeof import("dingtalk-jsapi/api/biz/map/view").view$;
            };
            media: {
                compressVideo: typeof import("dingtalk-jsapi/api/biz/media/compressVideo").compressVideo$;
            };
            microApp: {
                openApp: typeof import("dingtalk-jsapi/api/biz/microApp/openApp").openApp$;
            };
            navigation: {
                close: typeof import("dingtalk-jsapi/api/biz/navigation/close").close$;
                goBack: typeof import("dingtalk-jsapi/api/biz/navigation/goBack").goBack$;
                hideBar: typeof import("dingtalk-jsapi/api/biz/navigation/hideBar").hideBar$; /**
                 * 获取用户信息
                 *
                 * @return {*}  {Promise<any>}
                 * @memberof DingTalkService
                 */
                navigateToMiniProgram: typeof import("dingtalk-jsapi/api/biz/navigation/navigateToMiniProgram").navigateToMiniProgram$;
                quit: typeof import("dingtalk-jsapi/api/biz/navigation/quit").quit$;
                replace: typeof import("dingtalk-jsapi/api/biz/navigation/replace").replace$;
                setIcon: typeof import("dingtalk-jsapi/api/biz/navigation/setIcon").setIcon$;
                setLeft: typeof import("dingtalk-jsapi/api/biz/navigation/setLeft").setLeft$;
                setMenu: typeof import("dingtalk-jsapi/api/biz/navigation/setMenu").setMenu$;
                setRight: typeof import("dingtalk-jsapi/api/biz/navigation/setRight").setRight$;
                setTitle: typeof import("dingtalk-jsapi/api/biz/navigation/setTitle").setTitle$;
            };
            pbp: {
                componentPunchFromPartner: typeof import("dingtalk-jsapi/api/biz/pbp/componentPunchFromPartner").componentPunchFromPartner$;
                startMatchRuleFromPartner: typeof import("dingtalk-jsapi/api/biz/pbp/startMatchRuleFromPartner").startMatchRuleFromPartner$;
                stopMatchRuleFromPartner: typeof import("dingtalk-jsapi/api/biz/pbp/stopMatchRuleFromPartner").stopMatchRuleFromPartner$;
            };
            realm: {
                getRealtimeTracingStatus: typeof import("dingtalk-jsapi/api/biz/realm/getRealtimeTracingStatus").getRealtimeTracingStatus$;
                getUserExclusiveInfo: typeof import("dingtalk-jsapi/api/biz/realm/getUserExclusiveInfo").getUserExclusiveInfo$;
                startRealtimeTracing: typeof import("dingtalk-jsapi/api/biz/realm/startRealtimeTracing").startRealtimeTracing$;
                stopRealtimeTracing: typeof import("dingtalk-jsapi/api/biz/realm/stopRealtimeTracing").stopRealtimeTracing$;
                subscribe: typeof import("dingtalk-jsapi/api/biz/realm/subscribe").subscribe$;
                unsubscribe: typeof import("dingtalk-jsapi/api/biz/realm/unsubscribe").unsubscribe$;
            };
            shortCut: {
                addShortCut: typeof import("dingtalk-jsapi/api/biz/shortCut/addShortCut").addShortCut$;
            };
            store: {
                closeUnpayOrder: typeof import("dingtalk-jsapi/api/biz/store/closeUnpayOrder").closeUnpayOrder$;
                createOrder: typeof import("dingtalk-jsapi/api/biz/store/createOrder").createOrder$;
                getPayUrl: typeof import("dingtalk-jsapi/api/biz/store/getPayUrl").getPayUrl$;
                inquiry: typeof import("dingtalk-jsapi/api/biz/store/inquiry").inquiry$;
            };
            telephone: {
                call: typeof import("dingtalk-jsapi/api/biz/telephone/call").call$;
                checkBizCall: typeof import("dingtalk-jsapi/api/biz/telephone/checkBizCall").checkBizCall$;
                quickCallList: typeof import("dingtalk-jsapi/api/biz/telephone/quickCallList").quickCallList$;
                showCallMenu: typeof import("dingtalk-jsapi/api/biz/telephone/showCallMenu").showCallMenu$;
            };
            user: {
                checkPassword: typeof import("dingtalk-jsapi/api/biz/user/checkPassword").checkPassword$;
                get: typeof import("dingtalk-jsapi/api/biz/user/get").get$;
            };
            util: {
                chooseImage: typeof import("dingtalk-jsapi/api/biz/util/chooseImage").chooseImage$;
                chosen: typeof import("dingtalk-jsapi/api/biz/util/chosen").chosen$;
                clearWebStoreCache: typeof import("dingtalk-jsapi/api/biz/util/clearWebStoreCache").clearWebStoreCache$;
                closePreviewImage: typeof import("dingtalk-jsapi/api/biz/util/closePreviewImage").closePreviewImage$;
                datepicker: typeof import("dingtalk-jsapi/api/biz/util/datepicker").datepicker$;
                datetimepicker: typeof import("dingtalk-jsapi/api/biz/util/datetimepicker").datetimepicker$;
                decrypt: typeof import("dingtalk-jsapi/api/biz/util/decrypt").decrypt$;
                downloadFile: typeof import("dingtalk-jsapi/api/biz/util/downloadFile").downloadFile$;
                encrypt: typeof import("dingtalk-jsapi/api/biz/util/encrypt").encrypt$;
                getPerfInfo: typeof import("dingtalk-jsapi/api/biz/util/getPerfInfo").getPerfInfo$;
                isEnableGPUAcceleration: typeof import("dingtalk-jsapi/api/biz/util/isEnableGPUAcceleration").isEnableGPUAcceleration$;
                isLocalFileExist: typeof import("dingtalk-jsapi/api/biz/util/isLocalFileExist").isLocalFileExist$;
                multiSelect: typeof import("dingtalk-jsapi/api/biz/util/multiSelect").multiSelect$;
                open: typeof import("dingtalk-jsapi/api/biz/util/open").open$;
                openLink: typeof import("dingtalk-jsapi/api/biz/util/openLink").openLink$;
                openLocalFile: typeof import("dingtalk-jsapi/api/biz/util/openLocalFile").openLocalFile$;
                openModal: typeof import("dingtalk-jsapi/api/biz/util/openModal").openModal$;
                openSlidePanel: typeof import("dingtalk-jsapi/api/biz/util/openSlidePanel").openSlidePanel$;
                presentWindow: typeof import("dingtalk-jsapi/api/biz/util/presentWindow").presentWindow$;
                previewImage: typeof import("dingtalk-jsapi/api/biz/util/previewImage").previewImage$;
                previewVideo: typeof import("dingtalk-jsapi/api/biz/util/previewVideo").previewVideo$;
                saveImage: typeof import("dingtalk-jsapi/api/biz/util/saveImage").saveImage$;
                scan: typeof import("dingtalk-jsapi/api/biz/util/scan").scan$;
                scanCard: typeof import("dingtalk-jsapi/api/biz/util/scanCard").scanCard$;
                setGPUAcceleration: typeof import("dingtalk-jsapi/api/biz/util/setGPUAcceleration").setGPUAcceleration$;
                setScreenBrightnessAndKeepOn: typeof import("dingtalk-jsapi/api/biz/util/setScreenBrightnessAndKeepOn").setScreenBrightnessAndKeepOn$;
                setScreenKeepOn: typeof import("dingtalk-jsapi/api/biz/util/setScreenKeepOn").setScreenKeepOn$;
                share: typeof import("dingtalk-jsapi/api/biz/util/share").share$;
                shareImage: typeof import("dingtalk-jsapi/api/biz/util/shareImage").shareImage$;
                startDocSign: typeof import("dingtalk-jsapi/api/biz/util/startDocSign").startDocSign$;
                systemShare: typeof import("dingtalk-jsapi/api/biz/util/systemShare").systemShare$;
                timepicker: typeof import("dingtalk-jsapi/api/biz/util/timepicker").timepicker$;
                uploadAttachment: typeof import("dingtalk-jsapi/api/biz/util/uploadAttachment").uploadAttachment$;
                uploadImage: typeof import("dingtalk-jsapi/api/biz/util/uploadImage").uploadImage$;
                uploadImageFromCamera: typeof import("dingtalk-jsapi/api/biz/util/uploadImageFromCamera").uploadImageFromCamera$;
                ut: typeof import("dingtalk-jsapi/api/biz/util/ut").ut$;
            };
            verify: {
                openBindIDCard: typeof import("dingtalk-jsapi/api/biz/verify/openBindIDCard").openBindIDCard$;
                startAuth: typeof import("dingtalk-jsapi/api/biz/verify/startAuth").startAuth$;
            };
        };
        channel: {
            permission: {
                requestAuthCode: typeof import("dingtalk-jsapi/api/channel/permission/requestAuthCode").requestAuthCode$;
            };
        };
        device: {
            accelerometer: {
                clearShake: typeof import("dingtalk-jsapi/api/device/accelerometer/clearShake").clearShake$;
                watchShake: typeof import("dingtalk-jsapi/api/device/accelerometer/watchShake").watchShake$;
            };
            audio: {
                download: typeof import("dingtalk-jsapi/api/device/audio/download").download$;
                onPlayEnd: typeof import("dingtalk-jsapi/api/device/audio/onPlayEnd").onPlayEnd$;
                onRecordEnd: typeof import("dingtalk-jsapi/api/device/audio/onRecordEnd").onRecordEnd$;
                pause: typeof import("dingtalk-jsapi/api/device/audio/pause").pause$;
                play: typeof import("dingtalk-jsapi/api/device/audio/play").play$;
                resume: typeof import("dingtalk-jsapi/api/device/audio/resume").resume$;
                startRecord: typeof import("dingtalk-jsapi/api/device/audio/startRecord").startRecord$;
                stop: typeof import("dingtalk-jsapi/api/device/audio/stop").stop$;
                stopRecord: typeof import("dingtalk-jsapi/api/device/audio/stopRecord").stopRecord$;
                translateVoice: typeof import("dingtalk-jsapi/api/device/audio/translateVoice").translateVoice$;
            };
            base: {
                getInterface: typeof import("dingtalk-jsapi/api/device/base/getInterface").getInterface$;
                getPhoneInfo: typeof import("dingtalk-jsapi/api/device/base/getPhoneInfo").getPhoneInfo$;
                getUUID: typeof import("dingtalk-jsapi/api/device/base/getUUID").getUUID$;
                getWifiStatus: typeof import("dingtalk-jsapi/api/device/base/getWifiStatus").getWifiStatus$;
            };
            connection: {
                getNetworkType: typeof import("dingtalk-jsapi/api/device/connection/getNetworkType").getNetworkType$;
            };
            geolocation: {
                checkPermission: typeof import("dingtalk-jsapi/api/device/geolocation/checkPermission").checkPermission$;
                get: typeof import("dingtalk-jsapi/api/device/geolocation/get").get$;
                start: typeof import("dingtalk-jsapi/api/device/geolocation/start").start$;
                status: typeof import("dingtalk-jsapi/api/device/geolocation/status").status$;
                stop: typeof import("dingtalk-jsapi/api/device/geolocation/stop").stop$;
            };
            launcher: {
                checkInstalledApps: typeof import("dingtalk-jsapi/api/device/launcher/checkInstalledApps").checkInstalledApps$;
                launchApp: typeof import("dingtalk-jsapi/api/device/launcher/launchApp").launchApp$;
            };
            nfc: {
                nfcRead: typeof import("dingtalk-jsapi/api/device/nfc/nfcRead").nfcRead$;
                nfcStop: typeof import("dingtalk-jsapi/api/device/nfc/nfcStop").nfcStop$;
                nfcWrite: typeof import("dingtalk-jsapi/api/device/nfc/nfcWrite").nfcWrite$;
            };
            notification: {
                actionSheet: typeof import("dingtalk-jsapi/api/device/notification/actionSheet").actionSheet$;
                alert: typeof import("dingtalk-jsapi/api/device/notification/alert").alert$;
                confirm: typeof import("dingtalk-jsapi/api/device/notification/confirm").confirm$;
                extendModal: typeof import("dingtalk-jsapi/api/device/notification/extendModal").extendModal$;
                hidePreloader: typeof import("dingtalk-jsapi/api/device/notification/hidePreloader").hidePreloader$;
                modal: typeof import("dingtalk-jsapi/api/device/notification/modal").modal$;
                prompt: typeof import("dingtalk-jsapi/api/device/notification/prompt").prompt$;
                showPreloader: typeof import("dingtalk-jsapi/api/device/notification/showPreloader").showPreloader$;
                toast: typeof import("dingtalk-jsapi/api/device/notification/toast").toast$;
                vibrate: typeof import("dingtalk-jsapi/api/device/notification/vibrate").vibrate$;
            };
            screen: {
                insetAdjust: typeof import("dingtalk-jsapi/api/device/screen/insetAdjust").insetAdjust$;
                resetView: typeof import("dingtalk-jsapi/api/device/screen/resetView").resetView$;
                rotateView: typeof import("dingtalk-jsapi/api/device/screen/rotateView").rotateView$;
            };
        };
        media: {
            voiceRecorder: {
                keepAlive: typeof import("dingtalk-jsapi/api/media/voiceRecorder/keepAlive").keepAlive$;
                pause: typeof import("dingtalk-jsapi/api/media/voiceRecorder/pause").pause$;
                resume: typeof import("dingtalk-jsapi/api/media/voiceRecorder/resume").resume$;
                start: typeof import("dingtalk-jsapi/api/media/voiceRecorder/start").start$;
                stop: typeof import("dingtalk-jsapi/api/media/voiceRecorder/stop").stop$;
            };
        };
        net: {
            bjGovApn: {
                loginGovNet: typeof import("dingtalk-jsapi/api/net/bjGovApn/loginGovNet").loginGovNet$;
            };
        };
        runtime: {
            message: {
                fetch: typeof import("dingtalk-jsapi/api/runtime/message/fetch").fetch$;
                post: typeof import("dingtalk-jsapi/api/runtime/message/post").post$;
            };
            monitor: {
                getLoadTime: typeof import("dingtalk-jsapi/api/runtime/monitor/getLoadTime").getLoadTime$;
            };
            permission: {
                requestAuthCode: typeof import("dingtalk-jsapi/api/runtime/permission/requestAuthCode").requestAuthCode$;
                requestOperateAuthCode: typeof import("dingtalk-jsapi/api/runtime/permission/requestOperateAuthCode").requestOperateAuthCode$;
            };
        };
        ui: {
            input: {
                plain: typeof import("dingtalk-jsapi/api/ui/input/plain").plain$;
            };
            nav: {
                close: typeof import("dingtalk-jsapi/api/ui/nav/close").close$;
                getCurrentId: typeof import("dingtalk-jsapi/api/ui/nav/getCurrentId").getCurrentId$;
                go: typeof import("dingtalk-jsapi/api/ui/nav/go").go$;
                preload: typeof import("dingtalk-jsapi/api/ui/nav/preload").preload$;
                recycle: typeof import("dingtalk-jsapi/api/ui/nav/recycle").recycle$;
            };
            progressBar: {
                setColors: typeof import("dingtalk-jsapi/api/ui/progressBar/setColors").setColors$;
            };
            pullToRefresh: {
                disable: typeof import("dingtalk-jsapi/api/ui/pullToRefresh/disable").disable$;
                enable: typeof import("dingtalk-jsapi/api/ui/pullToRefresh/enable").enable$;
                stop: typeof import("dingtalk-jsapi/api/ui/pullToRefresh/stop").stop$;
            };
            webViewBounce: {
                disable: typeof import("dingtalk-jsapi/api/ui/webViewBounce/disable").disable$;
                enable: typeof import("dingtalk-jsapi/api/ui/webViewBounce/enable").enable$;
            };
        };
        util: {
            domainStorage: {
                getItem: typeof import("dingtalk-jsapi/api/util/domainStorage/getItem").getItem$;
                removeItem: typeof import("dingtalk-jsapi/api/util/domainStorage/removeItem").removeItem$;
                setItem: typeof import("dingtalk-jsapi/api/util/domainStorage/setItem").setItem$;
            };
        };
    };
    /**
     * 钉钉是否已经初始化
     *
     * @private
     * @type {boolean}
     * @memberof DingTalkService
     */
    private $isInit;
    /**
     * 是否已经初始化
     *
     * @type {boolean}
     * @memberof DingTalkService
     */
    get isInit(): boolean;
    /**
     * http请求服务
     *
     * @protected
     * @memberof DingTalkService
     */
    protected http: import("../../../../ibz-core/types").IHttp;
    /**
     * Creates an instance of DingTalkService.
     * @memberof DingTalkService
     */
    private constructor();
    /**
     * 钉钉初始化  鉴权
     * @memberof DingTalkService
     */
    private init;
    /**
     * 钉钉初始化回调方法
     *
     * @private
     * @memberof DingTalkService
     */
    private dd_ready;
    /**
     * 获取当前钉钉组织信息
     *
     * @return {*}  {Promise<any>}
     * @memberof DingTalkService
     */
    getAccess_token(): Promise<any>;
    /**
     * 根据企业id获取权限码
     *
     * @param {string} corpId
     * @return {*}  {Promise<any>}
     * @memberof DingTalkService
     */
    requestAuthCode(corpId: string): Promise<any>;
    /**
     * 钉钉登录
     *
     * @return {*}  {Promise<any>}
     * @memberof DingTalkService
     */
    login(): Promise<any>;
    /**
     * 开始录音
     *
     * @memberof DingTalkService
     */
    private startRecord;
    /**
     * 停止录音
     *
     * @memberof DingTalkService
     */
    private stopRecord;
    /**
     * 语音转文字
     *
     * @private
     * @param {*} arg
     * @return {*}  {Promise<any>}
     * @memberof DingTalkService
     */
    private translateVoice;
    /**
     * 震动
     *
     * @param {*} arg 震动时间，android可配置 iOS忽略
     * @return {*}  {Promise<any>}
     * @memberof DingTalkService
     */
    vibrate(arg: any): Promise<any>;
    /**
     * 清楚登录用户信息
     *
     * @memberof DingTalkService
     */
    clearUserInfo(): void;
    /**
     * 获取用户信息
     *
     * @return {*}  {Promise<any>}
     * @memberof DingTalkService
     */
    getUserInfo(): Promise<any>;
    /**
     * 获取实例
     *
     * @static
     * @return {*}  {DingTalkService}
     * @memberof DingTalkService
     */
    static getInstance(): DingTalkService;
    /**
     * 关闭钉钉应用
     *
     * @memberof DingTalkService
     */
    close(): void;
    /**
     *
     *
     * @memberof DingTalkService
     */
    private setTitle;
    /**
     * 设置钉钉导航栏返回按钮
     *
     * @memberof DingTalkService
     */
    private setNavBack;
    /**
     * 钉钉导航栏返回事件
     *
     * @memberof DingTalkService
     */
    initRightBtn({ model, fun }: any): void;
    /**
     * 钉钉导航栏返回事件
     *
     * @memberof DingTalkService
     */
    private backEvent;
    /**
     * 设置钉钉导航栏返回事件
     *
     * @memberof DingTalkService
     */
    private setBackEvent;
    /**
     * 是否调用导航栏返回事件
     *
     * @memberof DingTalkService
     */
    private controlBackEvent;
    /**
     * 钉钉开放事件
     *
     *  @memberof DingTalkService
     */
    event(tag: string, arg: any): Promise<any>;
}
