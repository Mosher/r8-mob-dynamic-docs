import { IContext, IParam, IViewOpenService } from 'ibz-core';
import { Subject } from 'rxjs';
export declare class AppViewOpenService implements IViewOpenService {
    /**
     * 单例对象
     *
     * @private
     * @static
     * @type {AppViewOpenService}
     * @memberof AppViewOpenService
     */
    private static instance;
    /**
     * 路由
     *
     * @private
     * @type {(Router | any)}
     * @memberof AppViewOpenService
     */
    private router;
    /**
     * 单例构造器
     *
     * @static
     * @return {*}
     * @memberof AppViewOpenService
     */
    static getInstance(router?: any): AppViewOpenService;
    /**
     * @description 打开模态
     * @param {({ viewComponent?: any | undefined, viewModel?: IParam, customClass?: string | undefined, customStyle?: IParam })} view
     * @param {IContext} [context] 应用上下文
     * @param {IParam} [navParam] 导航参数
     * @param {*} [navDatas] 数据
     * @param {IParam} [otherParam] 额外参数
     * @return {*}  {Subject<{ret: boolean, datas?: IParam[]}>}
     * @memberof AppViewOpenService
     */
    openModal(view: {
        viewComponent?: any | undefined;
        viewModel?: IParam;
        customClass?: string | undefined;
        customStyle?: IParam;
    }, context?: IContext, navParam?: IParam, navDatas?: any, otherParam?: IParam): Subject<{
        ret: boolean;
        datas?: IParam[];
    }>;
    /**
     * 路由打开
     *
     * @param {*} opts
     * @returns {Promise<any>}
     * @memberof AppViewOpenService
     */
    openView(path: string): void;
    /**
     * 浏览器新标签页打开
     *
     * @param {string} url
     * @returns {Promise<any>}
     * @memberof AppViewOpenService
     */
    openPopupApp(url: string): void;
    /**
     * @description 打开抽屉
     * @param {({ viewComponent?: any | undefined, viewModel?: IParam, customClass?: string | undefined, customStyle?: IParam })} view
     * @param {IContext} [context] 应用上下文
     * @param {IParam} [navParam] 导航参数
     * @param {*} [navDatas] 数据
     * @param {IParam} [otherParam] 额外参数
     * @return {*}  {Subject<{ret: boolean, datas?: IParam[]}>}
     * @memberof AppViewOpenService
     */
    openDrawer(view: {
        viewComponent?: any | undefined;
        viewModel?: IParam;
        customClass?: string | undefined;
        customStyle?: IParam;
    }, context?: IContext, navParam?: IParam, navDatas?: any, otherParam?: IParam): Subject<{
        ret: boolean;
        datas?: IParam[];
    }>;
}
