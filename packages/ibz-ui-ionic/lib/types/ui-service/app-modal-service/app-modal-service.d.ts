import { IParam } from 'ibz-core';
import { Subject } from 'rxjs';
/**
 * 模态框工具
 *
 * @export
 * @class AppModalService
 */
export declare class AppModalService {
    /**
     * 实例对象
     *
     * @private
     * @static
     * @memberof AppModalService
     */
    private static modal;
    /**
     * Creates an instance of AppModalService.
     *
     * @memberof AppModalService
     */
    constructor();
    /**
     * 获取单例对象
     *
     * @static
     * @returns {AppModalService}
     * @memberof AppModalService
     */
    static getInstance(): AppModalService;
    /**
     * 创建 Vue 实例对象
     *
     * @private
     * @param {{ viewComponent?: any,viewPath:string, viewModel: IParam, customClass?: string, customStyle?: IParam, }} view
     * @param {*} [navContext={}]
     * @param {*} [navParam={}]
     * @param {Array<any>} [navDatas=[]]
     * @param {*} viewCtx
     * @param {string} uuid
     * @return {*}  {Promise<any>}
     * @memberof AppModalService
     */
    private createVueExample;
    /**
     * 打开 ionic 模式模态框
     *
     * @private
     * @param {Element} ele
     * @returns {Promise<any>}
     * @memberof AppModalService
     */
    private createModal;
    /**
     * 打开模态视图
     *
     * @param {{ viewComponent?: any, viewPath: string, viewModel: IParam, customClass?: string, customStyle?: IParam }} view
     * @param {*} [navContext={}]
     * @param {*} [navParam={}]
     * @param {Array<any>} [navDatas=[]]
     * @param {*} [otherParam={}]
     * @return {*}  {Subject<any>}
     * @memberof AppModalService
     */
    openModal(view: {
        viewComponent?: any;
        viewPath?: string;
        viewModel?: IParam;
        customClass?: string;
        customStyle?: IParam;
    }, navContext?: any, navParam?: any, navDatas?: Array<any>, otherParam?: any): Subject<any>;
    /**
     *  初始化视图名称
     *
     * @memberof AppModalService
     */
    fillView(view: any): void;
}
