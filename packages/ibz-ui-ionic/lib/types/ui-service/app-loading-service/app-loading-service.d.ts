import { ILoadingService } from 'ibz-core';
/**
 * @description 应用加载服务
 * @export
 * @class AppLoadingService
 * @implements {ILoadingService}
 */
export declare class AppLoadingService implements ILoadingService {
    /**
     * @description 应用加载服务实例
     * @private
     * @static
     * @type {AppLoadingService}
     * @memberof AppLoadingService
     */
    private static instance;
    /**
     * @description 获取应用加载服务实例
     * @static
     * @return {*}
     * @memberof AppLoadingService
     */
    static getInstance(): AppLoadingService;
    /**
     * @description loading 对象
     * @type {*}
     * @memberof AppLoadingService
     */
    private elLoadingComponent;
    /**
     * @description 是否加载
     * @type {boolean}
     * @memberof AppLoadingServiceBase
     */
    private isLoading;
    /**
     * @description 统计加载
     * @type {number}
     * @memberof AppAppLoadingService
     */
    private loadingCount;
    /**
     * @description 加载结束
     * @memberof AppLoadingService
     */
    endLoading(): void;
    /**
     * @description 应用开始加载
     * @memberof AppLoadingService
     */
    beginLoading(): void;
}
