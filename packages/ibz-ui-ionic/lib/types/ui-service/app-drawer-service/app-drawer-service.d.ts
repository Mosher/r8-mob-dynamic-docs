import { Subject } from 'rxjs';
import { IParam } from 'ibz-core';
/**
 * 抽屉工具
 *
 * @export
 * @class AppDrawerService
 */
export declare class AppDrawerService {
    /**
     * 实例对象
     *
     * @private
     * @static
     * @memberof AppDrawerService
     */
    private static readonly $drawer;
    /**
     * 构造方法
     *
     * @memberof AppDrawerService
     */
    constructor();
    /**
     * 获取实例对象
     *
     * @static
     * @returns
     * @memberof AppDrawerService
     */
    static getInstance(): AppDrawerService;
    /**
     * 打开 ionic 模式模态框
     *
     * @private
     * @param {*} ele
     * @param {string} uuid
     * @return {*}  {Promise<any>}
     * @memberof AppDrawerService
     */
    private createDrawer;
    /**
     *  创建 Vue 实例对象
     *
     * @private
     * @param {({ viewComponentName?: string,viewModel: IParam, viewPath: string, customClass?: string, customStyle?: IParam, placement?: 'DRAWER_LEFT' | 'DRAWER_RIGHT' })} view
     * @param {*} [navContext={}]
     * @param {*} [navParam={}]
     * @param {Array<any>} [navDatas=[]]
     * @param {*} otherParam
     * @param {string} uuid
     * @return {*}  {Promise<void>}
     * @memberof AppDrawerService
     */
    private createVueExample;
    /**
     * 打开抽屉
     *
     * @param {({ viewComponent?: any, viewModel: IParam,viewPath: string, customClass?: string, customStyle?: IParam, placement?: 'DRAWER_LEFT' | 'DRAWER_RIGHT' })} view
     * @param {*} [navContext={}]
     * @param {*} [navParam={}]
     * @param {Array<any>} [navDatas=[]]
     * @param {*} [otherParam={}]
     * @return {*}  {Subject<any>}
     * @memberof AppDrawer
     */
    openDrawer(view: {
        viewComponent?: any;
        viewModel?: IParam;
        viewPath?: string;
        customClass?: string;
        customStyle?: IParam;
        placement?: 'DRAWER_LEFT' | 'DRAWER_RIGHT';
    }, navContext?: any, navParam?: any, navDatas?: Array<any>, otherParam?: any): Subject<any>;
    /**
     *  初始化视图名称
     *
     * @memberof AppModal
     */
    fillView(view: any): void;
}
