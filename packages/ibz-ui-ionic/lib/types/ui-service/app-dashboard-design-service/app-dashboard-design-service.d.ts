import { IAppDashboardDesignService, IParam } from 'ibz-core';
/**
 * @description 数据看板设计服务
 * @class AppDashboardDesignService
 */
export declare class AppDashboardDesignService implements IAppDashboardDesignService {
    /**
     * @description 数据看板设计服务对象
     * @private
     * @static
     * @type {AppDashboardDesignService}
     * @memberof AppDashboardDesignService
     */
    private static intance;
    /**
     * @description 获取数据看板设计服务（单例对象）
     * @memberof AppDashboardDesignService
     */
    static getInstance(): AppDashboardDesignService;
    /**
     * @description 保存模型数据
     * @param {string} serviceKey 工具服务标识
     * @param {IParam} context 应用上下文
     * @param {IParam} viewParam 保存视图参数
     * @memberof AppDashboardDesignService
     */
    saveModelData(serviceKey: string, context: IParam, viewParam: IParam): Promise<IParam>;
    /**
     * @description 加载门户部件集合
     * @param {IParam} context 应用上下文
     * @param {IParam} viewParam 视图参数
     */
    loadPortletList(context: IParam, viewParam: IParam): Promise<IParam>;
    /**
     * @description 过滤数据
     * @param {any[]} datas
     * @memberof AppDashboardDesignService
     */
    private filterData;
    /**
     * @description 分组集合
     * @param {any[]} [datas=[]]
     * @returns {any[]}
     * @memberof AppDashboardDesignService
     */
    private prepareGroup;
    /**
     * @description 准备list集合
     * @memberof AppDashboardDesignService
     */
    private prepareList;
    /**
     * @description 准备list项集合
     * @param {any[]} [children=[]]
     * @param {*} [data={}]
     * @memberof AppDashboardDesignService
     */
    private prepareList2;
}
