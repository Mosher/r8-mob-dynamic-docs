import { Subject } from 'rxjs';
import { IMsgboxService, IMsgboxOptions } from 'ibz-core';
/**
 * @description 消息弹框服务
 * @export
 * @class AppMsgboxService
 * @implements {IMsgboxService}
 */
export declare class AppMsgboxService implements IMsgboxService {
    /**
     * @description 消息弹框服务实例对象
     * @private
     * @static
     * @type {AppMsgboxService}
     * @memberof AppMsgboxService
     */
    private static instance;
    /**
     * @description 消息订阅
     * @private
     * @type {Subject<string>}
     * @memberof AppMsgboxService
     */
    private subject;
    /**
     * @description 弹框按钮
     * @private
     * @type {any[]}
     * @memberof AppMsgboxService
     */
    private buttons;
    /**
     * @description 弹框按钮模型
     * @private
     * @type {any[]}
     * @memberof AppMsgboxService
     */
    private buttonModel;
    /**
     * @description 消息弹框样式数组
     * @private
     * @type {string[]}
     * @memberof AppMsgboxService
     */
    private cssClass;
    /**
     * @description 获取唯一实列
     * @static
     * @return {*}  {AppMsgboxService}
     * @memberof AppMsgboxService
     */
    static getInstance(): AppMsgboxService;
    /**
     * @description 打开消息弹框
     * @param {IMsgboxOptions} options 消息弹框配置
     * @return {*}  {(Subject<string>)}
     * @memberof AppMsgboxService
     */
    open(options: IMsgboxOptions): Subject<string>;
    /**
     * @description 消息弹框按钮点击
     * @private
     * @param {string} value
     * @memberof AppMsgboxService
     */
    private itemClick;
    /**
     * @description 打开消息弹框
     * @private
     * @param {IParam} options 消息弹框配置
     * @return {*}  {Promise<any>}
     * @memberof AppMsgboxService
     */
    private openMsgbox;
    /**
     * @description 计算消息弹框样式
     * @private
     * @param {IParam} options
     * @memberof AppMsgboxService
     */
    private clacMsgboxClass;
    /**
     * @description
     * @private
     * @param {string} buttonType 按钮类型
     * @memberof AppMsgboxService
     */
    private initButtonModel;
}
