import { IAppAuthService, IHttp, IParam } from 'ibz-core';
/**
 * 应用权限服务类
 *
 * @export
 * @class AppAuthService
 */
export declare class AppAuthService implements IAppAuthService {
    /**
     * 网络请求对象
     *
     * @private
     * @type {Http}
     * @memberof AppAuthService
     */
    readonly http: IHttp;
    /**
     * 唯一实例
     *
     * @private
     * @static
     * @memberof AppAuthService
     */
    private static readonly instance;
    /**
     * 获取唯一实例
     *
     * @static
     * @return {*}  {AppAuthService}
     * @memberof AppAuthService
     */
    static getInstance(): AppAuthService;
    /**
     * @description 登录(后续需考虑运行平台)
     * @param {{ loginname: string, password: string }} _data 登录名，密码
     * @return {*}  {Promise<IParam>}
     * @memberof AppAuthService
     */
    login(_data: {
        loginname: string;
        password: string;
    }): Promise<IParam>;
    /**
     * @description 登出
     * @return {*}  {Promise<IParam>}
     * @memberof AppAuthService
     */
    logout(): Promise<IParam>;
    /**
     * @description 刷新TOKEN
     * @param {IParam} data 请求相关数据
     * @return {*}  {Promise<boolean>}
     * @memberof IAppAuthService
     */
    refreshToken(data: IParam): Promise<boolean>;
    /**
     * @description 判断TOKEN是否过期
     * @param {Date} date
     * @return {*}  {boolean}
     * @memberof AppAuthService
     */
    isTokenExpired(date: Date): boolean;
    /**
     * @description 获取TOKEN过期时间
     * @return {*}  {Date}
     * @memberof AppAuthService
     */
    getExpiredDate(): Date;
    /**
     * @description 设置TOKEN过期时间
     * @param {Date} date 时间
     * @memberof AppAuthService
     */
    setExpiredDate(date: Date): void;
    /**
     * @description 获取用户访问权限
     * @param {IParam} viewModel 视图模型
     * @return {*}  {boolean}
     * @memberof AppAuthService
     */
    getUserAccessAuth(viewModel: IParam): Promise<boolean>;
}
