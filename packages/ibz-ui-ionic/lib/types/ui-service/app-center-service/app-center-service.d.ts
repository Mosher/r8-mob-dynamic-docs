import { Subject } from 'rxjs';
import { IAppCenterService, IParam } from 'ibz-core';
/**
 * 应用中心服务类
 *
 * @export
 * @class AppCenterService
 */
export declare class AppCenterService implements IAppCenterService {
    /**
     * 应用数据状态管理对象
     *
     * @private
     * @type {Subject<any>}
     * @memberof AppCenterService
     */
    private subject;
    /**
     * 单例变量声明
     *
     * @private
     * @static
     * @type {AppCenterService}
     * @memberof AppCenterService
     */
    private static appCenterService;
    /**
     * 获取 AppCenterService 单例对象
     *
     * @static
     * @returns {AppCenterService}
     * @memberof AppCenterService
     */
    static getInstance(): AppCenterService;
    /**
     * 通知消息
     *
     * @param {*} name 名称(通常是应用实体名称)
     * @param {*} action 行为（操作数据行为）
     * @param {*} data 数据（操作数据）
     * @memberof AppCenterService
     */
    notifyMessage(name: string, action: string, data: IParam): void;
    /**
     * 获取消息中心
     *
     * @memberof AppCenterService
     */
    getMessageCenter(): Subject<{
        name: string;
        action: string;
        data: IParam;
    }>;
}
