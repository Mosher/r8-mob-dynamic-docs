import { shallowReactive } from 'vue';
import { AppPortalViewProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { ViewComponentBase } from './view-component-base';
/**
 * 移动端应用看板视图
 *
 * @class AppPortalView
 * @extends ViewComponentBase
 */

class AppPortalView extends ViewComponentBase {
  /**
   * 设置响应式
   *
   * @memberof AppPortalView
   */
  setup() {
    this.c = shallowReactive(this.getViewControllerByType('APPPORTALVIEW'));
    super.setup();
  }

} //  移动端应用看板视图组件


export const AppPortalViewComponent = GenerateComponent(AppPortalView, new AppPortalViewProps());