import { createVNode as _createVNode, resolveComponent as _resolveComponent } from "vue";
import { shallowReactive } from 'vue';
import { AppViewProps, AppViewControllerBase } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { ViewComponentBase } from './view-component-base';
export class AppMobNotSupportedView extends ViewComponentBase {
  /**
   * 设置响应式
   *
   * @public
   * @memberof AppIndexView
   */
  setup() {
    this.c = shallowReactive(new AppViewControllerBase(this.props));
    super.setup();
  }
  /**
   * 视图渲染
   *
   * @memberof AppDefaultNotSupportedView
   */


  render() {
    var _a, _b;

    if (!this.viewIsLoaded || !this.c.viewInstance) {
      return null;
    }

    const flexStyle = 'width: 100%; height: 100%; overflow: auto; display: flex;justify-content:center;align-items:center;';
    return _createVNode(_resolveComponent("ion-page"), {
      "class": 'app-view',
      "style": flexStyle
    }, {
      default: () => [`${this.$tl('view.notsupportview.tip', '暂未支持')}${(_b = (_a = this.c) === null || _a === void 0 ? void 0 : _a.viewInstance) === null || _b === void 0 ? void 0 : _b.title}`]
    });
  }

} // 应用首页组件

export const AppMobNotSupportedViewComponent = GenerateComponent(AppMobNotSupportedView, new AppViewProps());