import { shallowReactive } from 'vue';
import { AppMobMapViewProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { DEMultiDataViewComponentBase } from './de-multi-data-view-component-base';
export class AppMobMapView extends DEMultiDataViewComponentBase {
  /**
   * 设置响应式
   *
   * @public
   * @memberof AppMobMapView
   */
  setup() {
    this.c = shallowReactive(this.getViewControllerByType('DEMOBMAPVIEW'));
    super.setup();
  }

} // 应用首页组件

export const AppMobMapViewComponent = GenerateComponent(AppMobMapView, new AppMobMapViewProps());