import { shallowReactive } from 'vue';
import { AppMobPickUpMDViewProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { DEMultiDataViewComponentBase } from './de-multi-data-view-component-base';
/**
 * 移动端多数据视图
 *
 * @class AppMobMDView
 * @extends ViewComponentBase
 */

export class AppMobPickUpMDView extends DEMultiDataViewComponentBase {
  /**
   * @description 设置响应式
   * @memberof AppMobPickUpMDView
   */
  setup() {
    this.c = shallowReactive(this.getViewControllerByType('DEMOBPICKUPMDVIEW'));
    super.setup();
  }
  /**
   * 额外部件参数
   *
   * @param ctrlProps 部件参数
   * @param controlInstance 部件
   */


  extraCtrlParam(ctrlProps, controlInstance) {
    super.extraCtrlParam(ctrlProps, controlInstance);

    if ((controlInstance === null || controlInstance === void 0 ? void 0 : controlInstance.controlType) == 'MOBMDCTRL') {
      Object.assign(ctrlProps, {
        isMultiple: this.c.isMultiple,
        ctrlShowMode: 'SELECT'
      });
    }
  }

} //  移动端选择多数据视图组件

export const AppMobPickUpMDViewComponent = GenerateComponent(AppMobPickUpMDView, new AppMobPickUpMDViewProps());