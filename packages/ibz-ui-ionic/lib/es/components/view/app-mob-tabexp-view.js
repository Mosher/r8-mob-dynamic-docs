import { createVNode as _createVNode } from "vue";
import { shallowReactive } from 'vue';
import { AppMobTabExpViewProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { DEViewComponentBase } from './de-view-component-base';
/**
 * 移动端分页导航视图
 *
 * @class AppMobTabExpView
 * @extends DEViewComponentBase
 */

export class AppMobTabExpView extends DEViewComponentBase {
  /**
   * 设置响应式
   *
   * @public
   * @memberof AppMobTabExpView
   */
  setup() {
    this.c = shallowReactive(this.getViewControllerByType('DEMOBTABEXPVIEW'));
    super.setup();
  }
  /**
   * @description 渲染信息栏
   * @return {*}
   * @memberof AppMobTabExpView
   */


  renderDataInfoBar() {
    var _a;

    if (this.c.viewInstance.showDataInfoBar) {
      return _createVNode("span", {
        "class": "view-info-bar"
      }, [(_a = this.c.getData()) === null || _a === void 0 ? void 0 : _a.srfmajortext]);
    }
  }
  /**
   * @description 渲染视图组件
   * @return {*}
   * @memberof AppMobTabExpView
   */


  renderViewControls() {
    const controlObject = super.renderViewControls();
    Object.assign(controlObject, {
      'dataInfoBar': () => this.renderDataInfoBar()
    });
    return controlObject;
  }

} //  移动端多数据视图组件

export const AppMobTabExpViewComponent = GenerateComponent(AppMobTabExpView, new AppMobTabExpViewProps());