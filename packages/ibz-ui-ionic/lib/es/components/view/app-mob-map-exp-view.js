import { shallowReactive } from 'vue';
import { AppMobMapExpViewProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { DEExpViewComponentBase } from './de-exp-view-component-base';
/**
 * @description 移动端地图导航视图
 * @export
 * @class AppMobMapExpView
 * @extends {DEViewComponentBase<AppMobMapExpViewProps>}
 */

export class AppMobMapExpView extends DEExpViewComponentBase {
  /**
    * 设置响应式
    *
    * @public
    * @memberof AppMobMapExpView
    */
  setup() {
    this.c = shallowReactive(this.getViewControllerByType('DEMOBMAPEXPVIEW'));
    super.setup();
  }

} // 移动端地图导航视图组件

export const AppMobMapExpViewComponent = GenerateComponent(AppMobMapExpView, new AppMobMapExpViewProps());