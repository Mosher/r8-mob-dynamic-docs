import { shallowReactive } from 'vue';
import { AppMobChartExpViewProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { DEExpViewComponentBase } from './de-exp-view-component-base';
/**
 * @description 移动端图表导航视图
 * @export
 * @class AppMobChartExpView
 * @extends {DEViewComponentBase<AppMobChartExpViewProps>}
 */

export class AppMobChartExpView extends DEExpViewComponentBase {
  /**
   * 设置响应式
   *
   * @public
   * @memberof AppMobChartExpView
   */
  setup() {
    this.c = shallowReactive(this.getViewControllerByType('DEMOBCHARTEXPVIEW'));
    super.setup();
  }

} // 移动端图表导航视图组件

export const AppMobChartExpViewComponent = GenerateComponent(AppMobChartExpView, new AppMobChartExpViewProps());