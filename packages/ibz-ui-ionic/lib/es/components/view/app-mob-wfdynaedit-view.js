import { isVNode as _isVNode, resolveComponent as _resolveComponent, createVNode as _createVNode } from "vue";
import { shallowReactive } from 'vue';
import { AppMobWFDynaEditViewProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { DEViewComponentBase } from './de-view-component-base';
/**
 * @description 移动端动态工作流编辑视图
 * @export
 * @class AppMobWFDynaEditView
 * @extends {DEViewComponentBase<AppMobWFDynaEditViewProps>}
 */

function _isSlot(s) {
  return typeof s === 'function' || Object.prototype.toString.call(s) === '[object Object]' && !_isVNode(s);
}

export class AppMobWFDynaEditView extends DEViewComponentBase {
  /**
    * 设置响应式
    *
    * @public
    * @memberof AppMobWFDynaEditView
    */
  setup() {
    this.c = shallowReactive(this.getViewControllerByType('DEMOBWFDYNAEDITVIEW'));
    super.setup();
  }
  /**
   * @description 工具栏按钮点击
   * @param {*} linkItem 工具栏项
   * @param {*} event 源事件对象
   * @memberof AppMobWFDynaEditView
   */


  toolbarClick(linkItem, event) {
    this.c.dynamicToolbarClick(linkItem, event);
  }
  /**
   * @description 渲染工具栏
   * @return {*}
   * @memberof AppMobWFDynaEditView
   */


  renderToolbar() {
    let _slot;

    return _createVNode(_resolveComponent("ion-toolbar"), {
      "class": ['wf-toolbar'],
      "slot": "fixed"
    }, {
      default: () => [_createVNode(_resolveComponent("ion-buttons"), {
        "slot": "end"
      }, _isSlot(_slot = this.c.linkModel.map((linkItem, index) => {
        return _createVNode(_resolveComponent("ion-button"), {
          "key": index,
          "expand": 'block',
          "onClick": event => {
            this.toolbarClick(linkItem, event);
          }
        }, {
          default: () => [_createVNode("span", {
            "class": "caption"
          }, [linkItem.sequenceFlowName])]
        });
      })) ? _slot : {
        default: () => [_slot]
      })]
    });
  }
  /**
   * @description 渲染表单
   * @return {*}
   * @memberof AppMobWFDynaEditView
   */


  renderForm() {
    if (this.c.editFormInstance) {
      return this.computeTargetCtrlData(this.c.editFormInstance);
    } else {
      return null;
    }
  }
  /**
   * @description 渲染视图部件
   * @return {*}
   * @memberof AppMobWFDynaEditView
   */


  renderViewControls() {
    const controlObject = super.renderViewControls();
    Object.assign(controlObject, {
      righttoolbar: () => this.renderToolbar(),
      form: () => this.renderForm()
    });
    return controlObject;
  }

} // 移动端动态工作流编辑视图组件

export const AppMobWFDynaEditViewComponent = GenerateComponent(AppMobWFDynaEditView, new AppMobWFDynaEditViewProps());