import { shallowReactive } from 'vue';
import { AppMobChartViewProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { DEMultiDataViewComponentBase } from './de-multi-data-view-component-base';
/**
 * 移动端图表视图
 *
 * @class AppMobChartView
 * @extends DEMultiDataViewComponentBase
 */

export class AppMobChartView extends DEMultiDataViewComponentBase {
  /**
   * @description 设置响应式
   * @memberof AppMobChartView
   */
  setup() {
    this.c = shallowReactive(this.getViewControllerByType('DEMOBCHARTVIEW'));
    super.setup();
  }

} //  移动端日历视图组件

export const AppMobChartViewComponent = GenerateComponent(AppMobChartView, new AppMobChartViewProps());