import { createVNode as _createVNode, resolveComponent as _resolveComponent } from "vue";
import { shallowReactive } from 'vue';
import { AppMobWFDynaActionViewProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { DEViewComponentBase } from './de-view-component-base';
/**
 * @description 移动端动态工作流操作视图
 * @export
 * @class AppMobWFDynaActionView
 * @extends {DEViewComponentBase<AppMobWFDynaActionViewProps>}
 */

export class AppMobWFDynaActionView extends DEViewComponentBase {
  /**
    * 设置响应式
    *
    * @public
    * @memberof AppMobWFDynaActionView
    */
  setup() {
    this.c = shallowReactive(this.getViewControllerByType('DEMOBWFDYNAACTIONVIEW'));
    super.setup();
  }
  /**
   * @description 提交
   * @param {*} event
   * @memberof AppMobWFDynaActionView
   */


  submit(event) {
    this.c.handleOk();
  }
  /**
   * @description 取消
   * @param {*} event
   * @memberof AppMobWFDynaActionView
   */


  cancel(event) {
    this.c.handleCancel();
  }
  /**
   * @description 渲染视图底部
   * @return {*}
   * @memberof AppMobWFDynaActionView
   */


  renderViewFooter() {
    return _createVNode(_resolveComponent("ion-footer"), {
      "class": ['view-footer', 'wf-action-view-footer']
    }, {
      default: () => [_createVNode(_resolveComponent("ion-buttons"), null, {
        default: () => [_createVNode(_resolveComponent("ion-button"), {
          "expand": 'full',
          "size": 'large',
          "onClick": event => {
            this.submit(event);
          }
        }, {
          default: () => [`${this.$tl('view.wfdynaactionview.submit', '提交')}`]
        }), _createVNode(_resolveComponent("ion-button"), {
          "expand": 'full',
          "size": 'large',
          "onClick": event => {
            this.cancel(event);
          }
        }, {
          default: () => [`${this.$tl('share.cancel', '取消')}`]
        })]
      })]
    });
  }
  /**
   * @description 渲染表单
   * @return {*}
   * @memberof AppMobWFDynaActionView
   */


  renderForm() {
    if (this.c.editFormInstance) {
      return this.computeTargetCtrlData(this.c.editFormInstance);
    } else {
      return null;
    }
  }
  /**
   * @description 渲染视图组件
   * @return {*}
   * @memberof AppMobWFDynaActionView
   */


  renderViewControls() {
    const controlObject = super.renderViewControls();
    Object.assign(controlObject, {
      viewFooter: () => this.renderViewFooter(),
      form: () => this.renderForm()
    });
    return controlObject;
  }

} // 移动端动态工作流操作视图组件

export const AppMobWFDynaActionViewComponent = GenerateComponent(AppMobWFDynaActionView, new AppMobWFDynaActionViewProps());