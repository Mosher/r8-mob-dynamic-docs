import { createVNode as _createVNode } from "vue";
import { shallowReactive } from 'vue';
import { AppMobEditViewProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { DEViewComponentBase } from './de-view-component-base';
export class AppMobEditView extends DEViewComponentBase {
  /**
   * 设置响应式
   *
   * @public
   * @memberof AppMobEditView
   */
  setup() {
    this.c = shallowReactive(this.getViewControllerByType('DEMOBEDITVIEW'));
    super.setup();
  }
  /**
   * @description 渲染信息栏
   * @return {*}
   * @memberof AppMobEditView
   */


  renderDataInfoBar() {
    var _a;

    if (this.c.viewInstance.showDataInfoBar) {
      return _createVNode("span", {
        "class": "view-info-bar"
      }, [(_a = this.c.getData()) === null || _a === void 0 ? void 0 : _a.srfmajortext]);
    }
  }
  /**
   * @description 渲染视图组件
   * @return {*}
   * @memberof AppMobEditView
   */


  renderViewControls() {
    const controlObject = super.renderViewControls();
    Object.assign(controlObject, {
      'dataInfoBar': () => this.renderDataInfoBar()
    });
    return controlObject;
  }

} // 应用首页组件

export const AppMobEditViewComponent = GenerateComponent(AppMobEditView, new AppMobEditViewProps());