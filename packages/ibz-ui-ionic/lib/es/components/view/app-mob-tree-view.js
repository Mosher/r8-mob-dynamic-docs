import { shallowReactive } from 'vue';
import { AppMobTreeViewProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { DEMultiDataViewComponentBase } from './de-multi-data-view-component-base';
/**
 * 移动端树视图
 *
 * @class AppMobTreeView
 * @extends DEMultiDataViewComponentBase
 */

export class AppMobTreeView extends DEMultiDataViewComponentBase {
  /**
   * @description 设置响应式
   * @memberof AppMobTreeView
   */
  setup() {
    this.c = shallowReactive(this.getViewControllerByType('DEMOBTREEVIEW'));
    super.setup();
  }

} //  移动端树视图组件

export const AppMobTreeViewComponent = GenerateComponent(AppMobTreeView, new AppMobTreeViewProps());