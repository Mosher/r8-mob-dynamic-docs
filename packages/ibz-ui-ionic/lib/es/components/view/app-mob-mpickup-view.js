import { createVNode as _createVNode, resolveComponent as _resolveComponent } from "vue";
import { shallowReactive } from 'vue';
import { AppMobMPickUpViewProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { DEViewComponentBase } from './de-view-component-base';
/**
 * 移动端多数据选择视图
 *
 * @class AppMobMPickUpView
 * @extends ViewComponentBase
 */

export class AppMobMPickUpView extends DEViewComponentBase {
  /**
   * @description 设置响应式
   * @memberof AppMobMPickUpView
   */
  setup() {
    this.c = shallowReactive(this.getViewControllerByType('DEMOBMPICKUPVIEW'));
    super.setup();
  }
  /**
   * @description 绘制顶部按钮
   * @return {*}
   * @memberof AppMobMPickUpView
   */


  renderHeaderButtons() {
    return _createVNode(_resolveComponent("ion-toolbar"), null, {
      default: () => [_createVNode(_resolveComponent("ion-buttons"), {
        "slot": 'start'
      }, {
        default: () => [_createVNode(_resolveComponent("ion-button"), {
          "size": 'small',
          "onClick": () => this.c.cancel()
        }, {
          default: () => [`${this.$tl('share.cancel', '取消')}`]
        })]
      }), _createVNode(_resolveComponent("ion-buttons"), {
        "slot": 'primary'
      }, {
        default: () => [_createVNode(_resolveComponent("ion-button"), {
          "size": 'small',
          "onClick": () => this.c.ok()
        }, {
          default: () => [`${this.$tl('share.ok', '确认')}`]
        })]
      }), _createVNode(_resolveComponent("ion-title"), null, {
        default: () => [this.c.viewInstance.caption]
      })]
    });
  }
  /**
   * @description 绘制视图所有部件
   * @return {*}
   * @memberof AppMobMPickUpView
   */


  renderViewControls() {
    const controlObject = super.renderViewControls();
    Object.assign(controlObject, {
      headerButtons: () => this.renderHeaderButtons()
    });
    return controlObject;
  }
  /**
   * @description 额外部件参数
   * @param {*} ctrlProps 部件参数
   * @param {IPSControl} controlInstance 部件实例
   * @memberof AppMobMPickUpView
   */


  extraCtrlParam(ctrlProps, controlInstance) {
    super.extraCtrlParam(ctrlProps, controlInstance);

    if ((controlInstance === null || controlInstance === void 0 ? void 0 : controlInstance.controlType) == 'PICKUPVIEWPANEL') {
      Object.assign(ctrlProps, {
        isMultiple: true
      });
    }
  }

} //  移动端多数据选择视图组件

export const AppMobMPickUpViewComponent = GenerateComponent(AppMobMPickUpView, new AppMobMPickUpViewProps());