import { createVNode as _createVNode } from "vue";
import { AppMobDEDashboardViewProps } from 'ibz-core';
import { shallowReactive } from 'vue';
import { GenerateComponent } from '../component-base';
import { ViewComponentBase } from './view-component-base';
/**
 * 移动端实体数据看板视图
 *
 * @class AppMobDEDashboardView
 * @extends ViewComponentBase
 */

export class AppMobDEDashboardView extends ViewComponentBase {
  /**
   * 设置响应式
   *
   * @memberof AppMobDEDashboardView
   */
  setup() {
    this.c = shallowReactive(this.getViewControllerByType('DEMOBPORTALVIEW'));
    super.setup();
  }
  /**
   * @description 渲染信息栏
   * @return {*}
   * @memberof AppMobDEDashboardView
   */


  renderDataInfoBar() {
    var _a;

    if (this.c.viewInstance.showDataInfoBar) {
      return _createVNode("span", {
        "class": "view-info-bar"
      }, [(_a = this.c.getData()) === null || _a === void 0 ? void 0 : _a.srfmajortext]);
    }
  }
  /**
   * @description 渲染视图组件
   * @return {*}
   * @memberof AppMobDEDashboardView
   */


  renderViewControls() {
    const controlObject = super.renderViewControls();
    Object.assign(controlObject, {
      'dataInfoBar': () => this.renderDataInfoBar()
    });
    return controlObject;
  }

} //  移动端实体数据看板视图组件

export const AppMobDEDashboardViewComponent = GenerateComponent(AppMobDEDashboardView, new AppMobDEDashboardViewProps());