import { AppMobPanelViewProps } from 'ibz-core';
import { shallowReactive } from 'vue';
import { GenerateComponent } from '../component-base';
import { DEViewComponentBase } from './de-view-component-base';
export class AppMobPanelView extends DEViewComponentBase {
  /**
   * 设置响应式
   *
   * @public
   * @memberof AppMobMDView
   */
  setup() {
    this.c = shallowReactive(this.getViewControllerByType('DEMOBPANELVIEW'));
    super.setup();
  }
  /**
   * 额外部件参数
   *
   * @param ctrlProps 部件参数
   * @param controlInstance 部件
   */


  extraCtrlParam(ctrlProps, controlInstance) {
    super.extraCtrlParam(ctrlProps, controlInstance);

    if ((controlInstance === null || controlInstance === void 0 ? void 0 : controlInstance.controlType) == 'PANEL') {
      Object.assign(ctrlProps, {
        isLoadDefault: this.c.isLoadDefault
      });
    }
  }

}
export const AppMobPanelViewComponent = GenerateComponent(AppMobPanelView, new AppMobPanelViewProps());