import { shallowReactive } from 'vue';
import { AppMobCustomViewProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { DEViewComponentBase } from './de-view-component-base';
/**
 * 移动端自定义视图
 *
 * @class AppMobCustomView
 * @extends ViewComponentBase
 */

export class AppMobCustomView extends DEViewComponentBase {
  /**
   * 设置响应式
   *
   * @public
   * @memberof AppMobCustomView
   */
  setup() {
    this.c = shallowReactive(this.getViewControllerByType('DEMOBCUSTOMVIEW'));
    super.setup();
  }

} //  移动端自定义视图组件

export const AppMobCustomViewComponent = GenerateComponent(AppMobCustomView, new AppMobCustomViewProps());