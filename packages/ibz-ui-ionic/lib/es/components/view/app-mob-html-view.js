import { createVNode as _createVNode } from "vue";
import { shallowReactive } from 'vue';
import { AppMobHtmlViewProps, StringUtil } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { DEViewComponentBase } from './de-view-component-base';
/**
 * @description 移动端HTML视图
 * @export
 * @class AppMobHtmlView
 * @extends {DEViewComponentBase<AppMobHtmlViewProps>}
 */

export class AppMobHtmlView extends DEViewComponentBase {
  /**
   * @description 设置响应式
   * @memberof AppMobHtmlView
   */
  setup() {
    this.c = shallowReactive(this.getViewControllerByType('DEMOBHTMLVIEW'));
    super.setup();
  }
  /**
   * @description 绘制html内容
   * @return {*}
   * @memberof AppMobHtmlView
   */


  renderHtmlContent() {
    var _a, _b;

    if ((_b = (_a = this.c) === null || _a === void 0 ? void 0 : _a.viewInstance) === null || _b === void 0 ? void 0 : _b.htmlUrl) {
      const iframeUrl = StringUtil.fillStrData(this.c.viewInstance.htmlUrl, this.c.context, this.c.viewParam);
      return _createVNode("iframe", {
        "class": 'view-html-container',
        "src": iframeUrl
      }, null);
    } else {
      return _createVNode("div", {
        "class": 'view-error-container'
      }, [_createVNode("img", {
        "src": 'assets/images/404.jpg',
        "alt": '404'
      }, null), _createVNode("div", {
        "class": 'error-text'
      }, [`${this.$tl('view.mobhtmlview.notexist', '抱歉，您访问的页面不存在！')}`])]);
    }
  }
  /**
   * @description 绘制视图所有部件
   * @return {*}
   * @memberof AppMobHtmlView
   */


  renderViewControls() {
    const controlObject = super.renderViewControls();
    Object.assign(controlObject, {
      htmlContent: () => this.renderHtmlContent()
    });
    return controlObject;
  }

} // 移动端html视图组件

export const AppMobHtmlViewComponent = GenerateComponent(AppMobHtmlView, new AppMobHtmlViewProps());