import { shallowReactive } from 'vue';
import { AppIndexViewProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { ViewComponentBase } from './view-component-base';
/**
 * 应用首页视图交联组件
 *
 * @export
 * @class AppIndexView
 */

export class AppIndexView extends ViewComponentBase {
  /**
   * @description 设置响应式
   * @memberof AppIndexView
   */
  setup() {
    this.c = shallowReactive(this.getViewControllerByType('APPINDEXVIEW'));
    super.setup();
  }

} // 应用首页组件

export const AppIndexViewComponent = GenerateComponent(AppIndexView, new AppIndexViewProps());