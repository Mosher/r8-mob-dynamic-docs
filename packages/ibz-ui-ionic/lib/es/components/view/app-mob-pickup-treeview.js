import { shallowReactive } from 'vue';
import { AppMobPickupTreeViewProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { DEMultiDataViewComponentBase } from './de-multi-data-view-component-base';
/**
 * 移动端选择树视图
 *
 * @class AppMobPickupTreeView
 */

class AppMobPickupTreeView extends DEMultiDataViewComponentBase {
  /**
   * @description 设置响应式
   * @memberof AppMobPickupTreeView
   */
  setup() {
    this.c = shallowReactive(this.getViewControllerByType('DEMOBPICKUPTREEVIEW'));
    super.setup();
  }
  /**
   * @description 额外部件参数
   * @param {*} ctrlProps 部件参数
   * @param {IPSControl} controlInstance 部件实例
   * @memberof AppMobPickupTreeView
   */


  extraCtrlParam(ctrlProps, controlInstance) {
    super.extraCtrlParam(ctrlProps, controlInstance);

    if ((controlInstance === null || controlInstance === void 0 ? void 0 : controlInstance.controlType) == 'TREEVIEW') {
      Object.assign(ctrlProps, {
        isMultiple: this.c.isMultiple,
        ctrlShowMode: 'SELECT'
      });
    }
  }

} //  移动端选择树视图组件


export const AppMobPickupTreeViewComponent = GenerateComponent(AppMobPickupTreeView, new AppMobPickupTreeViewProps());