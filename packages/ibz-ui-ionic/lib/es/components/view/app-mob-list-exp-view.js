import { shallowReactive } from 'vue';
import { AppMobListExpViewProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { DEExpViewComponentBase } from './de-exp-view-component-base';
/**
 * @description 移动端列表导航视图
 * @export
 * @class AppMobListExpView
 * @extends {DEViewComponentBase<AppMobListExpViewProps>}
 */

export class AppMobListExpView extends DEExpViewComponentBase {
  /**
    * 设置响应式
    *
    * @public
    * @memberof AppMobListExpView
    */
  setup() {
    this.c = shallowReactive(this.getViewControllerByType('DEMOBLISTEXPVIEW'));
    super.setup();
  }

} // 移动端列表导航视图组件

export const AppMobListExpViewComponent = GenerateComponent(AppMobListExpView, new AppMobListExpViewProps());