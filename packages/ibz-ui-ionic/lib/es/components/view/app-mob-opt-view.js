import { createVNode as _createVNode, resolveComponent as _resolveComponent } from "vue";
import { shallowReactive } from 'vue';
import { AppMobOptViewProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { DEViewComponentBase } from './de-view-component-base';
/**
 * @description 移动端选项操作视图
 * @export
 * @class AppMobOptView
 * @extends {DEViewComponentBase<AppMobOptViewProps>}
 */

export class AppMobOptView extends DEViewComponentBase {
  /**
    * 设置响应式
    *
    * @public
    * @memberof AppMobOptView
    */
  setup() {
    this.c = shallowReactive(this.getViewControllerByType('DEMOBOPTVIEW'));
    super.setup();
  }
  /**
   * @description 绘制底部按钮
   * @return {*}
   * @memberof AppMobOptView
   */


  renderFooterButtons() {
    return _createVNode("div", {
      "class": "option-view-btnbox"
    }, [_createVNode(_resolveComponent("ion-button"), {
      "expand": 'full',
      "size": 'large',
      "class": "option-btn-cancel",
      "onClick": () => this.c.cancel()
    }, {
      default: () => [`${this.$tl('share.cancel', '取消')}`]
    }), _createVNode(_resolveComponent("ion-button"), {
      "expand": 'full',
      "size": 'large',
      "class": "option-btn-success",
      "onClick": () => this.c.ok()
    }, {
      default: () => [`${this.$tl('share.ok', '确认')}`]
    })]);
  }
  /**
   * @description 绘制视图所有部件
   * @return {*}
   * @memberof AppMobMPickUpView
   */


  renderViewControls() {
    const controlObject = super.renderViewControls();
    Object.assign(controlObject, {
      footerButtons: () => this.renderFooterButtons()
    });
    return controlObject;
  }

} // 移动端选项操作视图组件

export const AppMobOptViewComponent = GenerateComponent(AppMobOptView, new AppMobOptViewProps());