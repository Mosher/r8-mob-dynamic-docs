import { shallowReactive } from 'vue';
import { AppMobCalendarViewProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { DEMultiDataViewComponentBase } from './de-multi-data-view-component-base';
/**
 * 移动端多数据视图
 *
 * @class AppMobCalendarView
 * @extends DEMultiDataViewComponentBase
 */

export class AppMobCalendarView extends DEMultiDataViewComponentBase {
  /**
   * 设置响应式
   *
   * @public
   * @memberof AppMobCalendarView
   */
  setup() {
    this.c = shallowReactive(this.getViewControllerByType('DEMOBCALENDARVIEW'));
    super.setup();
  }

} //  移动端日历视图组件

export const AppMobCalendarViewComponent = GenerateComponent(AppMobCalendarView, new AppMobCalendarViewProps());