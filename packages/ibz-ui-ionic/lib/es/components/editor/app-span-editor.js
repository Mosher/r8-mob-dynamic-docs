import { h, shallowReactive } from 'vue';
import { AppSpanProps } from 'ibz-core';
import { AppSpan } from '../common';
import { GenerateComponent } from '../component-base';
import { EditorComponentBase } from './editor-component-base';
/**
 * 标签编辑器
 *
 * @export
 * @class AppSpanEditor
 * @extends {ComponentBase}
 */

export class AppSpanEditor extends EditorComponentBase {
  /**
   * @description 设置响应式
   * @memberof AppSpanEditor
   */
  setup() {
    this.c = shallowReactive(this.getEditorControllerByType('SPAN'));
    super.setup();
  }
  /**
   * @description 设置编辑器组件
   * @memberof AppSpanEditor
   */


  setEditorComponent() {
    this.editorComponent = AppSpan;
  }
  /**
   * @description 绘制内容
   * @return {*}
   * @memberof AppSpanEditor
   */


  render() {
    if (!this.c.editorIsLoaded) {
      return null;
    }

    return h(this.editorComponent, Object.assign(Object.assign({
      value: this.c.value,
      disabled: this.c.disabled
    }, this.c.customProps), {
      onEditorValueChange: $event => {
        this.c.changeValue($event);
      }
    }));
  }

} // 标签组件

export const AppSpanEditorComponent = GenerateComponent(AppSpanEditor, new AppSpanProps());