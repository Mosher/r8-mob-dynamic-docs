import { h, shallowReactive } from 'vue';
import { AppSliderProps } from 'ibz-core';
import { AppSlider } from '../common';
import { GenerateComponent } from '../component-base';
import { EditorComponentBase } from './editor-component-base';
/**
 * 开关编辑器
 *
 * @export
 * @class AppSliderEditor
 * @extends {ComponentBase}
 */

export class AppSliderEditor extends EditorComponentBase {
  /**
   * @description 设置响应式
   * @memberof AppSliderEditor
   */
  setup() {
    this.c = shallowReactive(this.getEditorControllerByType('MOBSLIDER'));
    super.setup();
  }
  /**
   * @description 设置编辑器组件
   * @memberof AppSliderEditor
   */


  setEditorComponent() {
    this.editorComponent = AppSlider;
  }
  /**
   * @description 绘制内容
   * @return {*}
   * @memberof AppSliderEditor
   */


  render() {
    if (!this.c.editorIsLoaded) {
      return null;
    }

    return h(this.editorComponent, Object.assign(Object.assign({
      value: this.c.value,
      disabled: this.c.disabled
    }, this.c.customProps), {
      onEditorValueChange: $event => {
        this.c.changeValue($event);
      }
    }));
  }

} // Slider组件

export const AppSliderEditorComponent = GenerateComponent(AppSliderEditor, new AppSliderProps());