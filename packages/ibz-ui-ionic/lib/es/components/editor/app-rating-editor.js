import { h, shallowReactive } from 'vue';
import { AppRatingProps } from 'ibz-core';
import { AppRating } from '../common';
import { GenerateComponent } from '../component-base';
import { EditorComponentBase } from './editor-component-base';
/**
 * 评分编辑器
 *
 * @export
 * @class AppRatingEditor
 * @extends {ComponentBase}
 */

export class AppRatingEditor extends EditorComponentBase {
  /**
   * @description 设置响应式
   * @memberof AppRatingEditor
   */
  setup() {
    this.c = shallowReactive(this.getEditorControllerByType('MOBRATING'));
    super.setup();
  }
  /**
   * @description 设置编辑器组件
   * @memberof AppRatingEditor
   */


  setEditorComponent() {
    this.editorComponent = AppRating;
  }
  /**
   * @description 绘制内容
   * @return {*}
   * @memberof AppRatingEditor
   */


  render() {
    if (!this.c.editorIsLoaded) {
      return null;
    }

    return h(this.editorComponent, Object.assign(Object.assign({
      value: this.c.value,
      disabled: this.c.disabled
    }, this.c.customProps), {
      onEditorValueChange: $event => {
        this.c.changeValue($event);
      }
    }));
  }

} // Rate组件

export const AppRatingEditorComponent = GenerateComponent(AppRatingEditor, new AppRatingProps());