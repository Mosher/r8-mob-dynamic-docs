import { createVNode as _createVNode, createTextVNode as _createTextVNode } from "vue";
import { shallowReactive } from 'vue';
import { AppEditorProps, EditorControllerBase } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { EditorComponentBase } from './editor-component-base';
/**
 * 开关编辑器
 *
 * @export
 * @class AppMobNotSupportedEditor
 * @extends {EditorComponentBase}
 */

export class AppMobNotSupportedEditor extends EditorComponentBase {
  /**
   * 设置响应式
   *
   * @public
   * @memberof AppMobNotSupportedEditor
   */
  setup() {
    this.c = shallowReactive(new EditorControllerBase(this.props));
    super.setup();
  }
  /**
   * 绘制内容
   *
   * @public
   * @memberof AppMobNotSupportedEditor
   */


  render() {
    if (!this.c.editorIsLoaded) {
      return null;
    }

    const flexStyle = 'width: 100%; height: 100%; overflow: hidden; display: flex;justify-content:center;align-items:center;';
    return _createVNode("div", {
      "class": 'control-container',
      "style": flexStyle
    }, [_createTextVNode("\u6682\u672A\u652F\u6301\u7F16\u8F91\u5668")]);
  }

} // Slider组件

export const AppMobNotSupportedEditorComponent = GenerateComponent(AppMobNotSupportedEditor, new AppEditorProps());