import { h, shallowReactive } from 'vue';
import { AppRichTextProps } from 'ibz-core';
import { AppRichText } from '../common';
import { GenerateComponent } from '../component-base';
import { EditorComponentBase } from './editor-component-base';
/**
 * @description 富文本编辑器
 * @export
 * @class AppRichTextEditor
 * @extends {EditorComponentBase<AppRichTextProps>}
 */

export class AppRichTextEditor extends EditorComponentBase {
  /**
   * @description 设置响应式
   * @memberof AppRichTextEditor
   */
  setup() {
    this.c = shallowReactive(this.getEditorControllerByType('MOBHTMLTEXT'));
    super.setup();
  }
  /**
   * @description 设置编辑器组件
   * @memberof AppRichTextEditor
   */


  setEditorComponent() {
    this.editorComponent = AppRichText;
  }
  /**
   * @description 绘制内容
   * @return {*}
   * @memberof AppRichTextEditor
   */


  render() {
    if (!this.c.editorIsLoaded) {
      return null;
    }

    return h(this.editorComponent, Object.assign(Object.assign({
      value: this.c.value,
      disabled: this.c.disabled
    }, this.c.customProps), {
      onEditorValueChange: $event => {
        this.c.changeValue($event);
      }
    }));
  }

} // AppRichText组件

export const AppRichTextEditorComponent = GenerateComponent(AppRichTextEditor, new AppRichTextProps());