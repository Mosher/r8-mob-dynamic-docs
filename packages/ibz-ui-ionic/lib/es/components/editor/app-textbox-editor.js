import { h, shallowReactive } from 'vue';
import { AppTextboxProps } from 'ibz-core';
import { AppInput, AppTextArea } from '../common';
import { GenerateComponent } from '../component-base';
import { EditorComponentBase } from './editor-component-base';
/**
 * 文本框编辑器
 *
 * @export
 * @class AppTextboxEditor
 * @extends {ComponentBase}
 */

export class AppTextboxEditor extends EditorComponentBase {
  /**
   * @description 设置响应式
   * @memberof AppTextboxEditor
   */
  setup() {
    this.c = shallowReactive(this.getEditorControllerByType('MOBTEXT'));
    super.setup();
  }
  /**
   * @description 设置编辑器组件
   * @memberof AppTextboxEditor
   */


  setEditorComponent() {
    const type = this.editorInstance.editorType;

    switch (type) {
      case 'MOBTEXT':
      case 'MOBNUMBER':
      case 'MOBPASSWORD':
        this.editorComponent = AppInput;
        break;

      case 'MOBTEXTAREA':
        this.editorComponent = AppTextArea;
        break;

      default:
        this.editorComponent = AppInput;
        break;
    }
  }
  /**
   * @description 绘制内容
   * @return {*}
   * @memberof AppTextboxEditor
   */


  render() {
    if (!this.c.editorIsLoaded) {
      return null;
    }

    return h(this.editorComponent, Object.assign(Object.assign({
      value: this.c.value,
      disabled: this.c.disabled
    }, this.c.customProps), {
      onEditorValueChange: $event => {
        this.c.changeValue($event);
      }
    }));
  }

} // Textbox组件

export const AppTextboxEditorComponent = GenerateComponent(AppTextboxEditor, new AppTextboxProps());