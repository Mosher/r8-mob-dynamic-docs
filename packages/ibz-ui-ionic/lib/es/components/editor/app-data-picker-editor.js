import { h, shallowReactive } from 'vue';
import { AppDataPickerProps } from 'ibz-core';
import { AppDataPicker } from '../common';
import { GenerateComponent } from '../component-base';
import { EditorComponentBase } from './editor-component-base';
/**
 * 时间选择编辑器
 *
 * @export
 * @class AppDataPickerEditor
 * @extends {ComponentBase}
 */

export class AppDataPickerEditor extends EditorComponentBase {
  /**
   * @description 设置响应式
   * @memberof AppDataPickerEditor
   */
  setup() {
    this.c = shallowReactive(this.getEditorControllerByType('PICKER'));
    super.setup();
  }
  /**
   * @description 设置编辑器组件
   * @memberof AppDataPickerEditor
   */


  setEditorComponent() {
    this.editorComponent = AppDataPicker;
  }
  /**
   * @description 绘制内容
   * @return {*}
   * @memberof AppDataPickerEditor
   */


  render() {
    if (!this.c.editorIsLoaded) {
      return null;
    }

    return h(this.editorComponent, Object.assign(Object.assign({
      value: this.c.value,
      disabled: this.c.disabled
    }, this.c.customProps), {
      onEditorValueChange: $event => {
        this.c.changeValue($event);
      }
    }));
  }

} // 数据选择组件

export const AppDataPickerEditorComponent = GenerateComponent(AppDataPickerEditor, new AppDataPickerProps());