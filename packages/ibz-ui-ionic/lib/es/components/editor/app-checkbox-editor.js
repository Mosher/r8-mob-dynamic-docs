import { h, shallowReactive } from 'vue';
import { AppCheckBoxProps } from 'ibz-core';
import { AppRadioList } from '../common';
import { GenerateComponent } from '../component-base';
import { EditorComponentBase } from './editor-component-base';
export class AppCheckBoxEditor extends EditorComponentBase {
  /**
   * @description 设置响应式
   * @memberof AppCheckBoxEditor
   */
  setup() {
    this.c = shallowReactive(this.getEditorControllerByType('MOBRADIOLIST'));
    super.setup();
  }
  /**
   * @description 设置编辑器组件
   * @memberof AppCheckBoxEditor
   */


  setEditorComponent() {
    this.editorComponent = AppRadioList;
  }
  /**
   * @description 绘制内容
   * @return {*}
   * @memberof AppCheckBoxEditor
   */


  render() {
    if (!this.c.editorIsLoaded) {
      return null;
    }

    return h(this.editorComponent, Object.assign(Object.assign({
      value: this.c.value,
      disabled: this.c.disabled
    }, this.c.customProps), {
      onEditorValueChange: $event => {
        this.c.changeValue($event);
      }
    }));
  }

} // CheckBox组件

export const AppCheckBoxEditorComponent = GenerateComponent(AppCheckBoxEditor, new AppCheckBoxProps());