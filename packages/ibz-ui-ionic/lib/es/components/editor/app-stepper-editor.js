import { h, shallowReactive } from 'vue';
import { AppStepperProps } from 'ibz-core';
import { AppStepper } from '../common';
import { GenerateComponent } from '../component-base';
import { EditorComponentBase } from './editor-component-base';
/**
 * 评分编辑器
 *
 * @export
 * @class AppStepperEditor
 * @extends {ComponentBase}
 */

export class AppStepperEditor extends EditorComponentBase {
  /**
   * @description 设置响应式
   * @memberof AppStepperEditor
   */
  setup() {
    this.c = shallowReactive(this.getEditorControllerByType('MOBSTEPPER'));
    super.setup();
  }
  /**
   * @description 设置编辑器组件
   * @memberof AppStepperEditor
   */


  setEditorComponent() {
    this.editorComponent = AppStepper;
  }
  /**
   * @description 绘制内容
   * @return {*}
   * @memberof AppStepperEditor
   */


  render() {
    if (!this.c.editorIsLoaded) {
      return null;
    }

    return h(this.editorComponent, Object.assign(Object.assign({
      value: this.c.value,
      disabled: this.c.disabled
    }, this.c.customProps), {
      onEditorValueChange: $event => {
        this.c.changeValue($event);
      }
    }));
  }

} // Stepper组件

export const AppStepperEditorComponent = GenerateComponent(AppStepperEditor, new AppStepperProps());