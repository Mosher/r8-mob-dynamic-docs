import { isVNode as _isVNode, createVNode as _createVNode, resolveComponent as _resolveComponent } from "vue";
import { defineComponent } from 'vue';
/**
 * 操作栏部件输入参数
 */

function _isSlot(s) {
  return typeof s === 'function' || Object.prototype.toString.call(s) === '[object Object]' && !_isVNode(s);
}

const AppActionBarProps = {
  /**
   * 操作栏模型数据
   *
   * @type {Array}
   */
  items: {
    type: Array,
    default: []
  },

  /**
   * 模型服务
   *
   * @type {IParam}
   * @memberof AppFormPageProps
   */
  modelService: Object
};
/**
 * 操作栏部件
 *
 * @class AppActionBar
 */

export const AppActionBar = defineComponent({
  name: 'AppActionBar',
  props: AppActionBarProps,

  setup(props, ctx) {
    /**
     * 触发界面行为
     *
     * @param event
     */
    const handleClick = (tag, event) => {
      ctx.emit('itemClick', tag, event);
    };

    return {
      handleClick
    };
  },

  render() {
    return _createVNode("div", {
      "class": 'app-actionbar'
    }, [this.items && this.items.length > 0 ? this.items.map((item, index) => {
      let _slot;

      return _createVNode("div", {
        "class": 'actionbar-item',
        "key": index
      }, [item.counterService && item.counterService.counterData ? _createVNode(_resolveComponent("ion-badge"), {
        "color": 'success'
      }, {
        default: () => [item.counterService.counterData[item.counterId]]
      }) : null, _createVNode(_resolveComponent("ion-button"), {
        "color": 'light',
        "onClick": event => {
          this.handleClick(item.viewLogicName, event);
        }
      }, _isSlot(_slot = this.$tl(item.language, item.name)) ? _slot : {
        default: () => [_slot]
      })]);
    }) : null, _createVNode("div", {
      "class": 'actionbar-item'
    }, null)]);
  }

});