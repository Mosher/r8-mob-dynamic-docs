import { isVNode as _isVNode, createVNode as _createVNode, resolveComponent as _resolveComponent } from "vue";
import { IonButton, modalController } from '@ionic/vue';
import axios from 'axios';
import { Util } from 'ibz-core';
import { defineComponent, reactive } from 'vue';
/**
 * 电子签名编辑器输入属性
 *
 * @memberof AppSignatureProps
 */

function _isSlot(s) {
  return typeof s === 'function' || Object.prototype.toString.call(s) === '[object Object]' && !_isVNode(s);
}

const AppSignatureProps = {
  /**
   *  值
   * @type {String} value
   * @memberof AppSignatureProps
   */
  value: {
    type: String
  },

  /**
   *  线宽
   * @type {String} lineWidth
   * @memberof AppSignatureProps
   */
  lineWidth: {
    type: String,
    default: '3'
  },

  /**
   *  线色
   * @type {String} lineColor
   * @memberof AppSignatureProps
   */
  lineColor: {
    type: String,
    default: '#000'
  },

  /**
   *  背景色
   * @type {String} backgroundColor
   * @memberof AppSignatureProps
   */
  backgroundColor: {
    type: String,
    default: '#fff'
  },

  /**
   *  提示信息
   * @type {String} placeholder
   * @memberof AppSignatureProps
   */
  placeholder: {
    type: String,
    default: '开始签名'
  },

  /**
   *  图片名称
   * @type {String} imageName
   * @memberof AppSignatureProps
   */
  imageName: {
    type: String,
    default: '个性签名'
  },

  /**
   *  图片尺寸
   * @type {String} imageSize
   * @memberof AppSignatureProps
   */
  imageSize: {
    type: Object,
    default: {
      width: '100%',
      height: '100%'
    }
  },

  /**
   * 只读状态
   *
   * @type {boolean}
   * @memberof AppUploadProps
   */
  readonly: {
    type: Boolean,
    default: false
  },

  /**
   * 上下文data数据（表单数据）
   *
   * @type {Object}
   * @memberof AppSignatureProps
   */
  contextData: Object,

  /**
   * 上下文
   *
   * @type {Object}
   * @memberof AppSignatureProps
   */
  context: Object,

  /**
   * 视图参数
   *
   * @type {Object}
   * @memberof AppSignatureProps
   */
  viewParam: Object,

  /**
   * 上传参数
   *
   * @type {Object}
   * @memberof AppSignatureProps
   */
  uploadParam: {
    type: Object,
    default: () => {}
  },

  /**
   * 下载参数
   *
   * @type {Object}
   * @memberof AppSignatureProps
   */
  exportParam: {
    type: Object,
    default: () => {}
  }
};
export const AppSignature = defineComponent({
  name: 'AppSignature',
  props: AppSignatureProps,
  emits: ['editorValueChange'],
  methods: {
    /**
     * @description 开始绘制
     * @param {*} $event
     */
    touchStart($event) {
      var _a;

      $event.preventDefault();

      if ($event.touches.length == 1) {
        this.data.isDraw = true; //签名标记

        let obj = {
          x: $event.targetTouches[0].clientX - this.position.offsetLeft,
          y: $event.targetTouches[0].clientY - this.position.offsetTop
        };
        this.data.startX = obj.x;
        this.data.startY = obj.y;
        (_a = this.data.canvasTxt) === null || _a === void 0 ? void 0 : _a.beginPath(); //开始作画
      }
    },

    /**
     * @description 绘制路径
     * @param {*} $event
     */
    touchMove($event) {
      $event.preventDefault();

      if ($event.touches.length == 1) {
        let obj = {
          x: $event.targetTouches[0].clientX - this.position.offsetLeft,
          y: $event.targetTouches[0].clientY - this.position.offsetTop
        };
        this.data.moveY = obj.y;
        this.data.moveX = obj.x;
        this.data.canvasTxt.moveTo(this.data.startX, this.data.startY);
        this.data.canvasTxt.lineTo(obj.x, obj.y);
        this.data.canvasTxt.stroke();
        this.data.startX = obj.x;
        this.data.startY = obj.y;
      }
    },

    /**
     * @description 绘制结束
     * @param {*} $event
     */
    touchEnd($event) {
      var _a;

      if ($event.touches.length == 0) {
        (_a = this.data.canvasTxt) === null || _a === void 0 ? void 0 : _a.closePath(); //收笔
      }
    },

    /**
     * @description 确认绘制
     */
    async uploadImage() {
      const imgBase64 = this.canvas.toDataURL();
      const image = Util.dataURLtoFile(imgBase64, this.imageName);
      const params = new FormData();
      params.append('file', image, image.name);
      const config = {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      };
      const response = await axios.post(this.uploadUrl, params, config);

      if (response && response.data && response.status === 200) {
        this.$emit('editorValueChange', [response.data]);
      } else {
        App.getNoticeService().error(`${image.name} ${this.$tl('common.upload.uploadfailure', '上传失败')}`);
      }
    },

    /**
     * @description 绘制模态内容
     * @return {*}
     */
    renderModal() {
      return defineComponent({
        render: () => {
          let _slot, _slot2;

          return [_createVNode(_resolveComponent("ion-header"), {
            "translucent": true
          }, {
            default: () => [_createVNode(_resolveComponent("ion-toolbar"), null, {
              default: () => [_createVNode(_resolveComponent("ion-title"), null, {
                default: () => [this.imageName]
              }), _createVNode(_resolveComponent("ion-buttons"), {
                "slot": 'start'
              }, {
                default: () => [_createVNode(_resolveComponent("ion-button"), {
                  "onclick": () => this.dismissModal(false)
                }, _isSlot(_slot = this.$tl('share.cancel', '取消')) ? _slot : {
                  default: () => [_slot]
                })]
              }), _createVNode(_resolveComponent("ion-buttons"), {
                "slot": 'end'
              }, {
                default: () => [_createVNode(_resolveComponent("ion-button"), {
                  "onclick": () => this.dismissModal(true)
                }, _isSlot(_slot2 = this.$tl('share.ok', '确认')) ? _slot2 : {
                  default: () => [_slot2]
                })]
              })]
            })]
          }), _createVNode(_resolveComponent("ion-content"), {
            "fullscreen": true
          }, {
            default: () => [_createVNode("div", {
              "class": 'signature-canvas-box'
            }, [_createVNode("canvas", {
              "class": 'signature-canvas',
              "style": {
                backgroundColor: this.backgroundColor,
                width: this.imageSize.width,
                height: this.imageSize.height
              },
              "onTouchstart": $event => {
                this.touchStart($event);
              },
              "onTouchmove": $event => {
                this.touchMove($event);
              },
              "onTouchend": $event => {
                this.touchEnd($event);
              }
            }, null)])]
          })];
        }
      });
    },

    /**
     * @description 打开模态
     */
    async openModal() {
      const modal = await modalController.create({
        component: this.renderModal(),
        showBackdrop: true,
        cssClass: 'app-signature-modal'
      });
      await modal.present();
      this.currentModal = modal;
      this.canvas = this.currentModal.querySelector('.signature-canvas');
      this.canvas.height = (Object.is(this.imageSize.height, '100%') ? this.canvas.clientHeight : Number.parseInt(this.imageSize.height)) - 1;
      this.canvas.width = (Object.is(this.imageSize.width, '100%') ? this.canvas.clientWidth : Number.parseInt(this.imageSize.width)) - 1;
      this.position.offsetTop = this.canvas.offsetTop;
      this.position.offsetLeft = this.canvas.offsetLeft;
      this.data.canvasTxt = this.canvas.getContext('2d');
      this.data.canvasTxt.strokeStyle = this.lineColor;
      this.data.canvasTxt.lineWidth = Number(this.lineWidth);
    },

    /**
     * @description 关闭模态
     * @param {boolean} $event
     */
    dismissModal($event) {
      if ($event) {
        this.uploadImage();
      }

      if (this.currentModal) {
        this.currentModal.dismiss().then(() => {
          this.currentModal = null;
        });
      }
    }

  },

  setup(props) {
    let uploadUrl = `${App.getEnvironment().UploadFile}`;
    let downloadUrl = `${App.getEnvironment().ExportFile}`;
    let files = props.value ? JSON.parse(props.value) : [];
    let position = {};
    let canvasTxt = null;
    let startX = 0;
    let startY = 0;
    let moveY = 0;
    let moveX = 0;
    let isDraw = false;
    let currentModal = null;
    let canvas = null;
    const data = reactive({
      canvasTxt,
      startX,
      startY,
      moveY,
      moveX,
      isDraw
    });
    return {
      uploadUrl,
      downloadUrl,
      files,
      data,
      position,
      currentModal,
      canvas
    };
  },

  render() {
    var _a;

    return _createVNode("div", {
      "class": 'app-signature'
    }, [((_a = this.files) === null || _a === void 0 ? void 0 : _a.length) > 0 ? this.files.map(file => {
      return _createVNode("div", {
        "class": "signature-image"
      }, [_createVNode("img", {
        "src": file.url
      }, null)]);
    }) : null, _createVNode(IonButton, {
      "onClick": () => {
        this.openModal();
      }
    }, {
      default: () => [this.placeholder]
    })]);
  }

});