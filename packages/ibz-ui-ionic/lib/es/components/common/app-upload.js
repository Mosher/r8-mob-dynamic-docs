import { isVNode as _isVNode, resolveComponent as _resolveComponent, createVNode as _createVNode } from "vue";
import { defineComponent } from 'vue';
import axios from 'axios';
import { Util } from 'ibz-core';
import qs from 'qs';
/**
 * 上传组件编辑器输入属性
 *
 * @memberof AppUploadProps
 */

function _isSlot(s) {
  return typeof s === 'function' || Object.prototype.toString.call(s) === '[object Object]' && !_isVNode(s);
}

const AppUploadProps = {
  /**
   * 双向绑定值
   *
   * @type {String}
   * @memberof AppUploadProps
   */
  value: {
    type: String
  },

  /**
   * 上下文data数据（表单数据）
   *
   * @type {Object}
   * @memberof AppUploadProps
   */
  contextData: Object,

  /**
   * 上下文
   *
   * @type {Object}
   * @memberof AppUploadProps
   */
  context: Object,

  /**
   * 视图参数
   *
   * @type {Object}
   * @memberof AppUploadProps
   */
  viewParam: Object,

  /**
   * 是否禁用
   *
   * @type {boolean}
   * @memberof AppUploadProps
   */
  disabled: {
    type: Boolean,
    default: false
  },

  /**
   * 只读状态
   *
   * @type {boolean}
   * @memberof AppUploadProps
   */
  readonly: {
    type: Boolean,
    default: false
  },

  /**
   * 是否多选
   *
   * @type {boolean}
   * @memberof AppUploadProps
   */
  multiple: {
    type: Boolean,
    default: false
  },

  /**
   * 文件后缀
   *
   * @type {String}
   * @memberof AppUploadProps
   */
  fileSuffix: String,

  /**
   * 最小文件数量
   *
   * @type {String}
   * @memberof AppUploadProps
   */
  minCount: Number,

  /**
   * 最大文件大小
   *
   * @type {String}
   * @memberof AppUploadProps
   */
  maxSize: Number,

  /**
   * 最大允许上传个数
   *
   * @type {String}
   * @memberof AppUploadProps
   */
  limit: {
    type: Number,
    default: 9999
  },

  /**
   * 允许上传的文件类型
   *
   * @type {String}
   * @memberof AppUploadProps
   */
  accept: {
    type: String,
    default: '*'
  },

  /**
   * 图片选取模式
   *
   * @type {String}
   * @memberof AppUploadProps
   */
  capture: String,

  /**
   * 上传类型
   *
   * @type {String}
   * @memberof AppUploadProps
   */
  uploadType: {
    type: String,
    default: 'file'
  },

  /**
   * 上传参数
   *
   * @type {Object}
   * @memberof AppUploadProps
   */
  uploadParam: {
    type: Object,
    default: () => {}
  },

  /**
   * 下载参数
   *
   * @type {Object}
   * @memberof AppUploadProps
   */
  exportParam: {
    type: Object,
    default: () => {}
  }
};
/**
 * 文件上传组件
 *
 * @class AppUpload
 */

export const AppUpload = defineComponent({
  name: 'AppUpload',
  props: AppUploadProps,
  emits: ['editorValueChange'],
  methods: {
    /**
     * @description 数据处理
     */
    dataProcess() {
      const {
        context: uploadContext,
        param: uploadParam
      } = Util.computedNavData(this.contextData, this.context, this.viewParam, this.uploadParam);
      const {
        context: exportContext,
        param: exportParam
      } = Util.computedNavData(this.contextData, this.context, this.viewParam, this.exportParam);
      const uploadContextStr = qs.stringify(uploadContext, {
        delimiter: '&'
      });
      const uploadParamStr = qs.stringify(uploadParam, {
        delimiter: '&'
      });

      if (!Object.is(uploadContextStr, '') || !Object.is(uploadParamStr, '')) {
        const uploadUrl = `${this.uploadUrl}?${uploadContextStr}&${uploadParamStr}`;
        this.uploadUrl = uploadUrl;
      }

      this.files.forEach(file => {
        if (process.env.NODE_ENV === 'development') {
          let index = this.devFiles.findIndex(devFile => Object.is(devFile.id, file.id));

          if (index !== -1) {
            file.url = this.devFiles[index].url;
            file.isImage = true;
          }
        }

        let downloadUrl = `${this.downloadUrl}/${file.id}`;
        const exportContextStr = qs.stringify(exportContext, {
          delimiter: '&'
        });
        const exportParamStr = qs.stringify(exportParam, {
          delimiter: '&'
        });

        if (!Object.is(exportContextStr, '') || !Object.is(exportParamStr, '')) {
          downloadUrl = `${downloadUrl}?${exportContextStr}&${exportParamStr}`;
        }

        file.url = downloadUrl;
      });
    },

    /**
     * @description 文件上传之前
     * @param {*} event 事件源
     */
    async beforeFileUpload(event) {
      const {
        files
      } = event.target;

      if (files && files.length > 0) {
        if (files.length + this.files.length > this.limit) {
          App.getNoticeService().warning(`${this.$tl('common.upload.uploadlimitamount', '上传限制数量为 ')} ${this.limit}`);
          return;
        }

        for (let i = 0; i < files.length; i++) {
          if (!Object.is('file', this.uploadType) && files[i].type.indexOf('image/') == -1) {
            App.getNoticeService().error(`${files[i].name} ${this.$tl('common.upload.notpicture', '不是图片')}`);
            return;
          }

          if (this.maxSize) {
            if (files[i].size <= this.maxSize) {
              await this.onFileUpload(files[i]);
            } else {
              App.getNoticeService().error(`${files[i].name} ${this.$tl('common.upload.sizeover', '大小超出')} ${this.maxSize}`);
            }
          } else {
            await this.onFileUpload(files[i]);
          }
        }
      }
    },

    /**
     * @description 文件上传
     * @param {*} file 文件
     */
    async onFileUpload(file) {
      const params = new FormData();
      params.append('file', file, file.name);
      const config = {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      };
      const response = await axios.post(this.uploadUrl, params, config);

      if (response && response.data && response.status === 200) {
        this.onSuccess(response, file);
      } else {
        this.onError(response, file);
      }
    },

    /**
     * @description 删除文件
     * @param {*} file 文件
     */
    onDelete(file) {
      let arr = [];
      this.files.forEach(_file => {
        if (_file.id != file.id) {
          arr.push({
            name: _file.name,
            id: _file.id
          });
        }
      });
      let value = arr.length > 0 ? JSON.stringify(arr) : null;
      this.$emit('editorValueChange', value);
    },

    /**
     * @description 下载
     * @param {*} file 文件
     */
    onDownload(file) {
      window.open(file.url);
    },

    /**
     * @description 成功
     * @param {*} response 响应
     * @param {*} file 上传文件
     */
    onSuccess(response, file) {
      const data = {
        name: response.filename,
        id: response.fileid
      };

      if (process.env.NODE_ENV === 'development') {
        this.devFiles.push(Object.assign({}, data, {
          url: file.content
        }));
      }

      let arr = [];
      this.files.forEach(_file => {
        arr.push({
          name: _file.name,
          id: _file.id
        });
      });
      arr.push(data);
      let value = arr.length > 0 ? JSON.stringify(arr) : null;
      this.$emit('editorValueChange', value);
    },

    /**
     * @description 上传失败
     * @param {*} error 错误
     * @param {*} file 文件
     */
    onError(error, file) {
      App.getNoticeService().error(`${file.name} ${this.$tl('common.upload.uploadfailure', '上传失败')}`);
    },

    /**
     * @description 绘制文件上传
     * @return {*}
     */
    renderFileUpload() {
      let _slot;

      var _a;

      return _createVNode("div", {
        "class": "app-file-upload"
      }, [((_a = this.files) === null || _a === void 0 ? void 0 : _a.length) > 0 ? _createVNode(_resolveComponent("ion-item-group"), {
        "class": "app-file-items"
      }, _isSlot(_slot = this.files.map(file => {
        return _createVNode(_resolveComponent("ion-item"), {
          "class": "app-file-item"
        }, {
          default: () => [_createVNode(_resolveComponent("ion-label"), null, {
            default: () => [_createVNode("a", {
              "class": "file",
              "onClick": () => this.onDownload(file)
            }, [file.name])]
          }), _createVNode(_resolveComponent("app-icon"), {
            "name": "close-outline",
            "onClick": () => this.onDelete(file)
          }, null)]
        });
      })) ? _slot : {
        default: () => [_slot]
      }) : this.readonly ? `${this.$tl('common.upload.nofile', '没有文件')}` : null, _createVNode(_resolveComponent("ion-row"), null, {
        default: () => [!this.readonly ? _createVNode(_resolveComponent("ion-button"), {
          "disabled": this.disabled
        }, {
          default: () => [_createVNode(_resolveComponent("app-icon"), {
            "name": 'add'
          }, null), `${this.$tl('common.upload.uploadfile', '上传文件')}`, _createVNode("input", {
            "type": "file",
            "class": "file-upload",
            "accept": this.accept,
            "multiple": this.multiple,
            "disabled": this.disabled,
            "onChange": $event => {
              this.beforeFileUpload($event);
            }
          }, null)]
        }) : null]
      })]);
    },

    /**
     * @description 绘制图片上传
     * @return {*}
     */
    renderPictureUpload() {
      var _a;

      return _createVNode("div", {
        "class": "app-picture-upload"
      }, [((_a = this.files) === null || _a === void 0 ? void 0 : _a.length) > 0 ? this.files.map(file => {
        return _createVNode("div", {
          "class": "app-picture-container"
        }, [_createVNode("img", {
          "class": "picture-preview",
          "src": file.url
        }, null)]);
      }) : this.readonly ? `${this.$tl('common.upload.nopicture', '没有图片')}` : null, !this.readonly ? _createVNode("div", {
        "class": "app-picture-container"
      }, [_createVNode(_resolveComponent("app-icon"), {
        "name": 'add'
      }, null), _createVNode("input", {
        "type": "file",
        "class": "file-upload",
        "accept": this.accept,
        "multiple": this.multiple,
        "disabled": this.disabled,
        "onChange": $event => {
          this.beforeFileUpload($event);
        }
      }, null)]) : null]);
    }

  },

  setup(props) {
    const environment = App.getEnvironment();
    let uploadUrl = `${environment === null || environment === void 0 ? void 0 : environment.UploadFile}`;
    let downloadUrl = `${environment === null || environment === void 0 ? void 0 : environment.ExportFile}`;
    let devFiles = [];
    let files = props.value ? JSON.parse(props.value) : [];
    return {
      uploadUrl,
      downloadUrl,
      files,
      devFiles
    };
  },

  mounted() {
    this.dataProcess();
  },

  render() {
    console.log(22, this);
    return _createVNode("div", {
      "class": "app-upload"
    }, [Object.is('file', this.uploadType) ? this.renderFileUpload() : this.renderPictureUpload()]);
  }

});