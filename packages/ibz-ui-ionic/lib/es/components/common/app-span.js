import { withDirectives as _withDirectives, createVNode as _createVNode, resolveDirective as _resolveDirective } from "vue";
import { defineComponent } from 'vue';
import { LogUtil, Util } from 'ibz-core';
const AppSpanProps = {
  /**
   * 表单值
   * @type {any}
   * @memberof AppSpanProps
   */
  value: [String, Number],

  /**
   * 上下文data数据
   *
   * @type {Object}
   * @memberof AppSpan
   */
  contextData: Object,

  /**
   * 上下文通讯对象
   *
   * @type {Object}
   * @memberof AppSpan
   */
  contextState: Object,

  /**
   * 上下文
   *
   * @type {Object}
   * @memberof AppSpan
   */
  context: Object,

  /**
   * 视图参数
   *
   * @type {Object}
   * @memberof AppSpan
   */
  viewParam: Object,

  /**
   * 是否禁用
   *
   * @type {boolean}
   * @memberof AppSpan
   */
  disabled: Boolean,

  /**
   * 代码表标识
   *
   * @type {String}
   * @memberof AppSpan
   */
  tag: String,

  /**
   * 代码表类型
   *
   * @type {String}
   * @memberof AppSpan
   */
  codeListType: String,

  /**
   * 局部上下文参数
   *
   * @type {Object}
   * @memberof AppSpan
   */
  localContext: Object,

  /**
   * 局部导航参数
   *
   * @type {Object}
   * @memberof AppSpan
   */
  localParam: Object,

  /**
   * 值格式化
   *
   * @type {String}
   * @memberof AppSpan
   */
  valueFormat: String,

  /**
   * 浮点精度
   * @type {string}
   * @memberof AppSpan
   */
  precision: String,

  /**
   * 模型服务
   * @type {String}
   * @memberof AppSpan
   */
  modelService: Object
};
export const AppSpan = defineComponent({
  name: 'AppSpan',
  props: AppSpanProps,
  computed: {
    curValue() {
      let curValue;

      if (this.disabled) {
        curValue = '——';
      } else if (this.items.length > 0) {
        const tempValue = this.items.find(item => {
          return item.value == this.value;
        });
        curValue = (tempValue === null || tempValue === void 0 ? void 0 : tempValue.text) ? tempValue.text : this.value;
      } else if (this.value) {
        curValue = this.value;
      }

      return curValue;
    }

  },
  methods: {
    /**
     * 公共参数处理
     *
     * @param {*} arg
     * @returns
     * @memberof AppSpan
     */
    handlePublicParams(arg) {
      // 合并表单参数
      arg.viewParam = this.viewParam ? Util.deepCopy(this.viewParam) : {};
      arg.context = this.context ? Util.deepCopy(this.context) : {}; // 附加参数处理

      if (this.localContext && Object.keys(this.localContext).length > 0) {
        const _context = Util.computedNavData(this.contextData, arg.context, arg.viewParam, this.localContext);

        Object.assign(arg.context, _context);
      }

      if (this.localParam && Object.keys(this.localParam).length > 0) {
        const _param = Util.computedNavData(this.contextData, arg.context, arg.viewParam, this.localParam);

        Object.assign(arg.viewParam, _param);
      }
    },

    /**
     * 加载数据
     *
     * @memberof AppSpan
     */
    async load() {
      this.items = [];
      const data = {};
      this.handlePublicParams(data);

      if (this.tag && this.codeListType) {
        const param = {
          tag: this.tag,
          type: this.codeListType,
          navContext: data.context,
          navViewParam: data.viewParam
        };

        try {
          this.items = await this.codeListService.getDataItems(param);
        } catch (error) {
          LogUtil.log(`----${this.tag}----${this.$tl('common.span.nocodelist', '代码表不存在')}`);
        }
      }
    }

  },
  setup: prop => {
    const codeListService = App.getCodeListService();
    const items = [];
    const textFormat = prop.valueFormat ? prop.valueFormat : prop.precision ? `#.${'0'.repeat(prop.precision)}` : '';
    return {
      codeListService,
      items,
      textFormat
    };
  },

  beforeMount() {
    if (this.tag && this.codeListType) {
      this.load();
    }
  },

  render() {
    return _createVNode("div", {
      "class": 'app-span'
    }, [this.textFormat ? _withDirectives(_createVNode("span", null, [this.curValue]), [[_resolveDirective("format"), this.textFormat]]) : _createVNode("span", null, [this.curValue])]);
  }

});