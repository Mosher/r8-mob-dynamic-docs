import { resolveDirective as _resolveDirective, createVNode as _createVNode, resolveComponent as _resolveComponent } from "vue";
import { defineComponent, ref, toRefs, reactive, onBeforeMount, watchEffect } from 'vue';
import draggable from 'vuedraggable';
import { AppDashboardDesignService } from '../../ui-service';
const AppDashboardDesignProps = {
  /**
   * @description 应用上下文
   * @type {*}
   * @memberof AppDashboardDesignProps
   */
  navContext: {
    type: Object
  },

  /**
   * @description 视图参数
   * @type {*}
   * @memberof AppDashboardDesignProps
   */
  navParam: {
    type: Object
  },

  /**
   * 模型服务
   * @type {Object}
   * @memberof InputBox
   */
  modelService: Object
}; //  数据看板自定义组件

export const AppDashboardDesign = defineComponent({
  name: 'AppDashboardDesign',
  props: AppDashboardDesignProps,
  components: {
    draggable
  },

  setup(props, ctx) {
    /**
     * @description 选中门户集合
     * @type {IParam[]}
     */
    const selections = reactive([]);
    /**
     * @description 未选中门户集合
     * @type {IParam[]}
     */

    const notSelections = reactive([]);
    /**
     * @description 显示状态
     * @type {boolean}
     */

    const titleStatus = ref(true);
    /**
     * @description 面板设计服务
     * @type {AppDashboardDesignService}
     */

    const designService = AppDashboardDesignService.getInstance();
    /**
     * @description 部件挂载之前
     */

    onBeforeMount(() => {
      const {
        navContext,
        navParam
      } = toRefs(props);
      designService.loadPortletList(navContext.value, navParam.value).then(result => {
        handleSelections(result.data);
      });
    });
    /**
     * @description 监听
     */

    watchEffect(() => {
      if (selections.length > 0) {
        selections.forEach((selection, index) => {
          selection.id = index + 1;
        });
      }

      if (notSelections.length > 0) {
        notSelections.forEach((notSelection, index) => {
          notSelection.id = index + 1;
        });
      }
    });
    /**
     * @description 处理选中数据
     */

    const handleSelections = items => {
      var _a;

      const {
        navParam
      } = toRefs(props);
      const customModel = ((_a = navParam.value) === null || _a === void 0 ? void 0 : _a.customModel) || [];
      items.forEach(item => {
        const custom = customModel.find(_custom => {
          return _custom.codeName == item.portletCodeName;
        });

        if (custom) {
          selections.push(item);
        } else {
          notSelections.push(item);
        }
      });
    };
    /**
     * @description 保存模型数据
     */


    const saveModel = (isReset = false) => {
      var _a;

      const {
        navParam,
        navContext
      } = toRefs(props);
      const tempViewParam = {};
      Object.assign(tempViewParam, Object.assign(Object.assign({}, navParam.value), {
        model: isReset ? [] : selections
      }));
      designService.saveModelData((_a = navParam.value) === null || _a === void 0 ? void 0 : _a.utilServiceName, navContext.value, tempViewParam);
    };
    /**
     * @description 删除选中项
     */


    const deleteItem = id => {
      const index = selections.findIndex(item => item.id === id);

      if (index !== -1) {
        notSelections.push(selections[index]);
        selections.splice(index, 1);
        saveModel();
      }
    };
    /**
     * @description 添加选中项
     */


    const addItem = id => {
      const index = notSelections.findIndex(item => item.id === id);

      if (index !== -1) {
        selections.push(notSelections[index]);
        notSelections.splice(index, 1);
        saveModel();
      }
    };
    /**
     * @description 拖拽结束
     */


    const dragEnd = () => {
      saveModel();
    };
    /**
     * @description 视图关闭
     */


    const closeView = event => {
      ctx.emit('viewEvent', {
        viewName: 'app-dashboard-design',
        action: 'onClose',
        data: []
      });
    };

    return {
      closeView,
      selections,
      dragEnd,
      deleteItem,
      notSelections,
      titleStatus,
      addItem
    };
  },

  methods: {
    renderHeader() {
      return _createVNode(_resolveComponent("ion-header"), null, {
        default: () => [_createVNode(_resolveComponent("ion-toolbar"), {
          "class": 'view-header'
        }, {
          default: () => [_createVNode(_resolveComponent("ion-buttons"), {
            "slot": 'start'
          }, {
            default: () => [_createVNode(_resolveComponent("ion-button"), {
              "onClick": event => {
                this.closeView(event);
              }
            }, {
              default: () => [_createVNode(_resolveComponent("app-icon"), {
                "name": 'chevron-back'
              }, null), `${this.$tl('common.dashboard.close', '关闭')}`]
            })]
          }), _createVNode(_resolveComponent("ion-title"), null, {
            default: () => [_createVNode("label", {
              "class": 'title-label'
            }, [`${this.$tl('common.dashboard.customdatakanban', '自定义数据看板')}`])]
          })]
        })]
      });
    },

    renderContent() {
      return _createVNode(_resolveComponent("ion-content"), null, {
        default: () => [_createVNode("div", {
          "class": 'dashboard-list'
        }, [_createVNode("div", {
          "class": 'dashboard-list-item list-item--added'
        }, [_createVNode("div", {
          "class": 'list-item-header'
        }, [`${this.$tl('common.dashboard.existcard', '已经添加的卡片')}`]), _createVNode(draggable, {
          "list": this.selections,
          "handle": '.list-item-content-end',
          "itemKey": 'id',
          "animation": 200,
          "onEnd": this.dragEnd
        }, {
          item: ({
            element,
            index
          }) => {
            return _createVNode("div", {
              "class": 'list-item-content',
              "key": element.componentName
            }, [_createVNode("div", {
              "class": 'list-item-content-start'
            }, [_createVNode(_resolveComponent("app-icon"), {
              "onClick": () => {
                this.deleteItem(element.id);
              },
              "name": 'remove-circle-outline'
            }, null)]), _createVNode("div", {
              "class": 'list-item-pic'
            }, [_createVNode("img", {
              "src": element.portletImage ? element.portletImage : 'assets/images/portlet.jpg',
              "alt": ''
            }, null)]), _createVNode("div", {
              "class": 'list-item-text'
            }, [_createVNode("div", null, [_createVNode("span", null, [element.portletName]), _createVNode("div", null, [element.detailText ? element.detailText : `${this.$tl('common.dashboard.nodescription', '暂无描述')}`])])]), _createVNode("div", {
              "class": 'list-item-content-end'
            }, [_createVNode(_resolveComponent("app-icon"), {
              "name": 'swap-vertical-outline'
            }, null)])]);
          }
        })]), _createVNode("div", {
          "class": 'dashboard-list-item dashboard-list-item--add'
        }, [_createVNode("div", {
          "class": 'list-item-header'
        }, [`${this.$tl('common.dashboard.noexistcard', '可添加的卡片')}`]), this.notSelections.map((item, index) => {
          return _createVNode("div", {
            "class": 'list-item-content',
            "key": index
          }, [_createVNode("div", {
            "class": 'list-item-content-start'
          }, [_createVNode(_resolveComponent("app-icon"), {
            "onClick": () => {
              this.addItem(item.id);
            },
            "name": 'add-circle-outline'
          }, null)]), _createVNode("div", {
            "class": 'list-item-pic'
          }, [_createVNode("img", {
            "src": item.portletImage ? item.portletImage : 'assets/images/portlet.jpg',
            "alt": ''
          }, null)]), _createVNode("div", {
            "class": 'list-item-text'
          }, [_createVNode("div", null, [_createVNode("span", null, [item.portletName]), _createVNode("div", null, [item.detailText ? item.detailText : `${this.$tl('common.dashboard.nodescription', '暂无描述')}`])])]), _createVNode("div", {
            "class": 'list-item-content-end'
          }, null)]);
        })])])]
      });
    }

  },

  render() {
    return _createVNode("div", {
      "class": 'app-dashboard-design'
    }, [this.titleStatus ? this.renderHeader() : null, this.renderContent()]);
  }

});