import { withDirectives as _withDirectives, resolveDirective as _resolveDirective, createVNode as _createVNode, resolveComponent as _resolveComponent } from "vue";
import { ComponentBase, GenerateComponent } from '../component-base';
export class AppIconItemProps {
  constructor() {
    /**
     * @description 项数据
     * @type {IParam}
     * @memberof AppIconItemProps
     */
    this.item = {};
    /**
     * @description 索引
     * @type {number}
     * @memberof AppIconItemProps
     */

    this.index = 0;
    /**
     * @description 值格式化
     * @type {string}
     * @memberof AppIconItemProps
     */

    this.valueFormat = '';
  }

}
export class AppIconItem extends ComponentBase {
  constructor() {
    super(...arguments);
    /**
     * @description 项数据
     * @type {IParam}
     * @memberof AppIconItem
     */

    this.item = {};
    /**
     * @description 索引
     * @type {number}
     * @memberof AppIconItem
     */

    this.index = 0;
    /**
     * @description 值格式化
     * @type {string}
     * @memberof AppIconItem
     */

    this.valueFormat = '';
  }
  /**
   * @description 设置响应式
   * @memberof AppIconItem
   */


  setup() {
    this.initInputData(this.props);
  }
  /**
   * @description 初始化输入属性
   * @param {AppIconItemProps} opts 输入对象
   * @memberof AppIconItem
   */


  initInputData(opts) {
    this.item = opts.item;
    this.index = opts.index;
    this.valueFormat = opts.valueFormat;
  }
  /**
   * @description 输入属性值变更
   * @memberof AppIconItem
   */


  watchEffect() {
    this.initInputData(this.props);
    this.getIndexText();
  }
  /**
   * @description 获取索引样式
   * @memberof AppIconItem
   */


  getIndexText() {
    var _a;

    const colorArray = ['#ffa600', '#498cf2', '#f76e9a', '#f56ef7', '#a56ef7'];

    if ((_a = this.item) === null || _a === void 0 ? void 0 : _a.srfmajortext) {
      this.item.indexText = this.item.srfmajortext[0];
    }

    this.item.indexColor = {
      'background-color': colorArray[this.index % colorArray.length]
    };
  }
  /**
   * @description 点击事件
   * @memberof AppIconItem
   */


  onClick() {
    this.ctx.emit('itemClick', this.item);
  }
  /**
   * @description 绘制
   * @return {*}
   * @memberof AppIconItem
   */


  render() {
    var _a, _b, _c, _d;

    return _createVNode("div", {
      "class": 'app-icon-item',
      "onClick": () => this.onClick()
    }, [_createVNode("div", {
      "class": 'icon-item-container'
    }, [((_a = this.item) === null || _a === void 0 ? void 0 : _a.iconsrc) ? _createVNode(_resolveComponent("app-icon"), {
      "iconSrc": this.item.iconsrc
    }, null) : _createVNode("div", {
      "class": 'icon-item-text',
      "style": (_b = this.item) === null || _b === void 0 ? void 0 : _b.indexColor
    }, [(_c = this.item) === null || _c === void 0 ? void 0 : _c.indexText, ' '])]), _createVNode("div", {
      "class": 'icon-item-title'
    }, [_withDirectives(_createVNode("span", null, [(_d = this.item) === null || _d === void 0 ? void 0 : _d.srfmajortext]), [[_resolveDirective("format"), this.valueFormat]])])]);
  }

}
export const AppIconItemComponent = GenerateComponent(AppIconItem, Object.keys(new AppIconItemProps()));