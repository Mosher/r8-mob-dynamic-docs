import { createVNode as _createVNode, resolveComponent as _resolveComponent } from "vue";
import { defineComponent } from 'vue';
const AppSwitchProps = {
  /**
   * 双向绑定值
   * @type {any}
   * @memberof InputBox
   */
  value: {
    type: String
  },

  /**
   * 是否禁用
   * @type {boolean}
   * @memberof InputBox
   */
  disabled: Boolean,

  /**
   * 只读模式
   *
   * @type {boolean}
   */
  readonly: Boolean
};
export const AppSwitch = defineComponent({
  name: 'AppSwitch',
  props: AppSwitchProps,
  emits: ['editorValueChange'],
  methods: {
    /**
     * 开关值改变事件
     *
     * @param {*} e
     */
    valueChange(e) {
      this.$emit('editorValueChange', e.detail.checked == true ? '1' : '0');
    }

  },
  computed: {
    curValue() {
      return this.value == '1' ? true : false;
    }

  },

  render() {
    return _createVNode("div", {
      "class": 'app-switch'
    }, [_createVNode(_resolveComponent("ion-toggle"), {
      "style": {
        'pointer-events': this.readonly ? 'none' : 'auto'
      },
      "disabled": this.disabled,
      "checked": this.curValue,
      "onIonChange": e => {
        this.valueChange(e);
      }
    }, null)]);
  }

});