import { resolveComponent as _resolveComponent, withDirectives as _withDirectives, resolveDirective as _resolveDirective, createVNode as _createVNode } from "vue";
import { ComponentBase, GenerateComponent } from '../component-base';
export class AppListItemProps {
  constructor() {
    /**
     * @description 项数据
     * @type {IParam}
     * @memberof AppListItemProps
     */
    this.item = {};
    /**
     * @description 索引
     * @type {number}
     * @memberof AppListItemProps
     */

    this.index = 0;
    /**
     * @description 值格式化
     * @type {string}
     * @memberof AppListItemProps
     */

    this.valueFormat = '';
  }

}
export class AppListItem extends ComponentBase {
  constructor() {
    super(...arguments);
    /**
     * @description 项数据
     * @type {IParam}
     * @memberof AppListItem
     */

    this.item = {};
    /**
     * @description 索引
     * @type {number}
     * @memberof AppListItem
     */

    this.index = 0;
    /**
     * @description 值格式化
     * @type {string}
     * @memberof AppListItem
     */

    this.valueFormat = '';
  }
  /**
   * @description 设置响应式
   * @memberof AppListItem
   */


  setup() {
    this.initInputData(this.props);
  }
  /**
   * @description 初始化输入属性
   * @param {AppListItemProps} opts 输入对象
   * @memberof AppListItem
   */


  initInputData(opts) {
    this.item = opts.item;
    this.index = opts.index;
    this.valueFormat = opts.valueFormat;
  }
  /**
   * @description 输入属性值变更
   * @memberof AppListItem
   */


  watchEffect() {
    this.initInputData(this.props);
    this.getIndexText();
  }
  /**
   * @description 获取索引样式
   * @memberof AppListItem
   */


  getIndexText() {
    var _a;

    const colorArray = ['#ffa600', '#498cf2', '#f76e9a', '#f56ef7', '#a56ef7'];

    if ((_a = this.item) === null || _a === void 0 ? void 0 : _a.srfmajortext) {
      this.item.indexText = this.item.srfmajortext[0];
    }

    this.item.indexColor = {
      'background-color': colorArray[this.index % colorArray.length]
    };
  }
  /**
   * @description 点击事件
   * @memberof AppListItem
   */


  onClick() {
    this.ctx.emit('itemClick', this.item);
  }
  /**
   * @description 绘制
   * @return {*}
   * @memberof AppListItem
   */


  render() {
    var _a, _b, _c;

    return _createVNode(_resolveComponent("ion-item"), null, {
      default: () => [_createVNode("div", {
        "class": 'app-list-item',
        "onClick": () => this.onClick()
      }, [_createVNode("div", {
        "class": 'list-item-container',
        "style": (_a = this.item) === null || _a === void 0 ? void 0 : _a.indexColor
      }, [(_b = this.item) === null || _b === void 0 ? void 0 : _b.indexText]), _createVNode("div", {
        "class": 'list-item-title'
      }, [_withDirectives(_createVNode("span", null, [(_c = this.item) === null || _c === void 0 ? void 0 : _c.srfmajortext]), [[_resolveDirective("format"), this.valueFormat]])])])]
    });
  }

}
export const AppListItemComponent = GenerateComponent(AppListItem, Object.keys(new AppListItemProps()));