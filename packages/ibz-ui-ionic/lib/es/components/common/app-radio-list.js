import { isVNode as _isVNode, createVNode as _createVNode, resolveComponent as _resolveComponent } from "vue";
import { defineComponent } from 'vue';
import { LogUtil } from 'ibz-core';

function _isSlot(s) {
  return typeof s === 'function' || Object.prototype.toString.call(s) === '[object Object]' && !_isVNode(s);
}

const AppRadioListProps = {
  name: {
    type: String
  },
  codeList: {
    type: Object
  },
  context: {
    type: Object,
    default: {}
  },
  value: {
    type: String
  },
  disabled: {
    type: Boolean,
    default: false
  },
  readonly: {
    type: Boolean,
    default: false
  },
  modelService: Object
};
export const AppRadioList = defineComponent({
  name: 'AppRadioList',
  props: AppRadioListProps,
  emits: ['editorValueChange'],

  data() {
    const codeListService = App.getCodeListService();
    return {
      codeListService: codeListService,
      options: []
    };
  },

  created() {
    if (this.codeList) {
      this.loadItems();
    }
  },

  methods: {
    async loadItems() {
      var _a;

      const tag = this.codeList.codeName;
      const type = this.codeList.codeListType;
      (_a = this.codeListService) === null || _a === void 0 ? void 0 : _a.getDataItems({
        tag: tag,
        type: type,
        navContext: this.context
      }).then(codeListItems => {
        this.options = codeListItems;
      }).catch(error => {
        LogUtil.log(`----${tag}----${this.$tl('common.radiolist.notfount', '未找到')}`);
      });
    },

    valueChange(event) {
      if (event && event.detail && event.detail.value) {
        this.$emit('editorValueChange', event.detail.value.toString());
      }
    }

  },

  render() {
    let _slot;

    return _createVNode(_resolveComponent("ion-radio-group"), {
      "class": 'app-radio-list',
      "disabled": this.disabled || this.readonly,
      "value": this.value,
      "onIonChange": this.valueChange.bind(this)
    }, _isSlot(_slot = this.options.map((option, index) => {
      return _createVNode(_resolveComponent("ion-item"), {
        "class": 'radio-list-item',
        "key": index
      }, {
        default: () => [_createVNode(_resolveComponent("ion-label"), null, {
          default: () => [option.label]
        }), _createVNode(_resolveComponent("ion-radio"), {
          "name": option.value,
          "value": option.value
        }, null)]
      });
    })) ? _slot : {
      default: () => [_slot]
    });
  }

});