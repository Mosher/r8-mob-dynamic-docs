import { createVNode as _createVNode, resolveComponent as _resolveComponent } from "vue";
import { defineComponent } from 'vue';
const AppTextAreaProps = {
  /**
   * 双向绑定值
   * @type {any}
   * @memberof AppTextArea
   */
  value: [String, Number],

  /**
   * placeholder值
   * @type {String}
   * @memberof AppTextArea
   */
  placeholder: String,

  /**
   * 是否禁用
   * @type {boolean}
   * @memberof AppTextArea
   */
  disabled: Boolean,

  /**
   * 最大长度
   * @type {string}
   * @memberof AppTextArea
   */
  maxLength: String,

  /**
   * 最小长度
   * @type {string}
   * @memberof AppTextArea
   */
  minLength: String,

  /**
   * 只读模式
   * @type {boolean}
   * @memberof AppTextArea
   */
  readonly: Boolean,

  /**
   * 是否显示最大长度
   * @type {string}
   * @memberof AppTextArea
   */
  showMaxLength: Boolean,

  /**
   * 模型服务
   * @type {Object}
   * @memberof AppTextArea
   */
  modelService: Object
};
export const AppTextArea = defineComponent({
  name: 'AppTextArea',
  props: AppTextAreaProps,
  emits: ['editorValueChange'],
  methods: {
    /**
     * 输入框值改变事件
     *
     * @param {*} e
     */
    valueChange(e) {
      this.$emit('editorValueChange', e.detail.value ? e.detail.value : null);
    }

  },

  render() {
    return _createVNode(_resolveComponent("ion-textarea"), {
      "class": 'app-textarea',
      "value": this.value,
      "disabled": this.disabled,
      "maxLength": this.maxLength,
      "minLength": this.minLength,
      "readonly": this.readonly,
      "placeholder": this.showMaxLength ? `${this.$tl('common.textarea.maxlength', '最大内容长度为 ')}${this.maxLength}` : this.placeholder,
      "onIonChange": e => {
        this.valueChange(e);
      }
    }, null);
  }

});