import { createVNode as _createVNode, resolveComponent as _resolveComponent } from "vue";
import { defineComponent, toRefs, ref } from 'vue';
import { ViewTool } from 'ibz-core';
const AppQuickGroupTabProps = {
  /**
   * 快速分组项
   *
   * @type {Array}
   */
  items: {
    type: Array
  }
};
export const AppQuickGroup = defineComponent({
  name: 'app-quick-group',
  props: AppQuickGroupTabProps,

  setup(prop, ctx) {
    //  渲染列表
    let showItems = []; //  子项列表

    const subItems = []; //  当前选中项

    const selectedUIItem = ref({}); //  解构items

    const {
      items
    } = toRefs(prop);
    /**
     * 处理数据集
     *
     * @memberof AppQuickGroupTabComponent
     */

    const handleDataSet = result => {
      const list = [];

      if (result.length === 0) {
        return list;
      }

      result.forEach(codeItem => {
        if (!codeItem.pvalue) {
          const valueField = codeItem.value;
          setChildCodeItems(valueField, result, codeItem);
          list.push(codeItem);
        }
      });
      return list;
    };
    /**
     * 设置子项数据
     *
     * @memberof AppQuickGroupTabComponent
     */


    const setChildCodeItems = (pValue, result, codeItem) => {
      result.forEach(item => {
        if (item.pvalue == pValue) {
          const valueField = item.value;
          setChildCodeItems(valueField, result, item);

          if (!codeItem.children) {
            codeItem.children = [];
          }

          codeItem.children.push(item);
        }
      });
    };
    /**
     * 处理点击事件
     *
     * @memberof AppQuickGroupTabComponent
     */


    const handleClick = (item, isFirst = false) => {
      selectedUIItem.value = item;

      if (item.children) {
        if (subItems.length > 0) {
          subItems.length = 0;
        } else {
          if (!isFirst) {
            subItems.push(...item.children);
          }
        }
      } else {
        subItems.length = 0;
        items.value.forEach(item => {
          item.selected = false;
          item.childSelected = false;
          item.selectChildLabel = '';
        });
        item.selected = true;

        if (item.pvalue) {
          items.value.forEach(item => {
            if (item.value === item.pvalue) {
              item.childSelected = true;
              item.selectChildLabel = item.label;
            }
          });
        }
      }

      ctx.emit('valueChange', item);
    };

    if (items && items.value && items.value.length > 0) {
      const select = items.value.find(item => {
        return item.default;
      });
      showItems = handleDataSet(items.value);
      handleClick(select ? select : items.value[0]);
    }

    return {
      showItems,
      subItems,
      selectedUIItem,
      handleClick
    };
  },

  methods: {
    /**
     * 是否为选中项
     *
     * @param item
     * @returns {boolean}
     * @memberof AppQuickGroupTabComponent
     */
    isSelectedItem(item) {
      if (this.selectedUIItem && this.selectedUIItem.id === item.id) {
        return true;
      } else {
        return false;
      }
    },

    /**
     * 关闭返回框
     *
     * @memberof AppQuickGroupTabComponent
     */
    closeBackdrop() {
      this.subItems.length = 0;
      this.$forceUpdate();
    }

  },

  /**
   * 组件渲染
   *
   * @memberof AppQuickGroupTabComponent
   */
  render() {
    return [_createVNode("div", {
      "class": 'app-quick-group'
    }, [this.items && this.items.map((item, index) => {
      return _createVNode("div", {
        "key": index,
        "class": {
          'group-item': true,
          'group-item--active': this.isSelectedItem(item) || item.childSelected
        },
        "onClick": () => {
          this.handleClick(item);
        }
      }, [_createVNode("div", {
        "style": {
          color: item.color
        }
      }, [item.ioncls ? _createVNode(_resolveComponent("ion-icon"), {
        "name": ViewTool.setIcon(item.iconcls)
      }, null) : item.icon ? _createVNode("img", {
        "src": item.icon
      }, null) : null, _createVNode("span", {
        "class": 'group-item-label'
      }, [item.selectChildLabel ? item.selectChildLabel : item.label]), item.children ? _createVNode(_resolveComponent("ion-icon"), {
        "name": 'caret-down-outline',
        "style": 'margin-left: 4px;'
      }, null) : null])]);
    })]), _createVNode("div", {
      "ref": 'child-list',
      "class": {
        'app-quick-group-popup': true,
        'app-quick-group-popup--open': this.subItems.length > 0
      }
    }, [this.subItems.map((item, index) => {
      return _createVNode("div", {
        "key": index,
        "class": {
          'group-popup-item': true,
          'group-popup-item--active': item.selected
        },
        "onClick": () => {
          this.handleClick(item);
        }
      }, [_createVNode("span", null, [item.ioncls ? _createVNode(_resolveComponent("ion-icon"), {
        "name": ViewTool.setIcon(item.iconcls)
      }, null) : item.icon ? _createVNode("img", {
        "src": item.icon
      }, null) : null, _createVNode("span", null, [item.label])]), item.selected ? _createVNode(_resolveComponent("ion-icon"), {
        "size": 'small',
        "style": 'margin-left:auto; color:green;',
        "name": 'checkmark-outline'
      }, null) : null]);
    })]), this.subItems.length > 0 ? _createVNode(_resolveComponent("ion-backdrop"), {
      "style": 'height: 100vh; z-index: -1;',
      "visible": 'true',
      "tappable": 'true',
      "onIonBackdropTap": this.closeBackdrop.bind(this)
    }, null) : null];
  }

});