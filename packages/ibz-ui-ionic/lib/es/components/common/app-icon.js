import { createVNode as _createVNode, resolveComponent as _resolveComponent } from "vue";
import { defineComponent } from 'vue';
import * as ionicons from 'ionicons/icons';
import { Util, ViewTool } from 'ibz-core';
const AppIconProps = {
  /**
   * 图标名称
   *
   * @type {string}
   * @memberof AppIconProps
   */
  name: String,

  /**
   * 图标对象
   *
   * @type {string}
   * @memberof AppIconProps
   */
  icon: Object,

  /**
   * 图标路径
   *
   * @type {string}
   * @memberof AppIconProps
   */
  iconSrc: String
};
export const AppIcon = defineComponent({
  name: 'AppIcon',
  props: AppIconProps,
  computed: {
    /**
     * iconName
     *
     * @returns string
     */
    iconName() {
      var _a;

      let name = '';

      if (this.name) {
        name = Util.formatCamelCase(this.name);
      } else if ((_a = this.icon) === null || _a === void 0 ? void 0 : _a.cssClass) {
        name = Util.formatCamelCase(ViewTool.setIcon(this.icon.cssClass));
      }

      return name ? ionicons[name] : null;
    },

    /**
     * imagePath
     *
     * @returns string
     */
    imagePath() {
      var _a;

      let src = '';

      if (this.iconSrc) {
        src = this.iconSrc;
      } else {
        src = (_a = this.icon) === null || _a === void 0 ? void 0 : _a.imagePath;
      }

      return src;
    }

  },

  render() {
    if (this.imagePath) {
      return _createVNode(_resolveComponent("ion-icon"), {
        "class": 'app-icon',
        "src": this.imagePath
      }, null);
    } else if (this.iconName) {
      return _createVNode(_resolveComponent("ion-icon"), {
        "class": 'app-icon',
        "icon": this.iconName
      }, null);
    } else {
      return null;
    }
  }

});