import { createVNode as _createVNode, resolveComponent as _resolveComponent } from "vue";
import { defineComponent } from 'vue';
const AppStepsProps = {
  steps: {
    type: Array
  },
  active: {
    type: String || Number,
    default: ''
  },
  panelInstance: Object
};
/**
 * @description 步骤条组件
 */

export const AppStepsComponent = defineComponent({
  name: 'AppSteps',
  props: AppStepsProps,

  setup(props, {
    emit
  }) {
    const stepItemClick = (step, event) => {
      event.stopPropagation();
      emit('stepClick', step, event);
    };

    const renderStep = (step, index) => {
      const {
        active
      } = props;
      let _index = 0;

      if (typeof active == 'number') {
        _index = active >= 0 ? active : 0;
      } else {
        _index = isNaN(Number(active)) ? 0 : Number(active);
      }

      return _createVNode("div", {
        "class": ['step-item', _index >= index ? _index == index ? 'step-item--active' : 'step-item--finish' : ''],
        "onClick": event => {
          stepItemClick(step, event);
        }
      }, [_createVNode("div", {
        "class": ["step-item__title", step.className]
      }, [step.icon ? _createVNode(_resolveComponent("app-icon"), {
        "name": step.icon
      }, null) : null, props.panelInstance.$tl(step.titleLanResTag, step.title), step.subTitle ? [_createVNode("br", null, null), _createVNode("span", {
        "class": "step-item__subtitle"
      }, [props.panelInstance.$tl(step.subTitleLanResTag, step.subTitle)])] : null]), _createVNode("div", {
        "class": "step-item__container"
      }, [_createVNode("i", {
        "class": _index == index ? "step-item__circle--active" : 'step-item__circle'
      }, null)]), _createVNode("div", {
        "class": "step-item__line"
      }, null)]);
    };

    return () => {
      const {
        steps
      } = props;
      return _createVNode("div", {
        "class": "app-steps"
      }, [_createVNode("div", {
        "class": ["steps-items", steps.find(step => step.subTitle) ? 'has-subtitle' : '']
      }, [steps ? steps.map((step, index) => {
        return renderStep(step, index);
      }) : null])]);
    };
  }

});