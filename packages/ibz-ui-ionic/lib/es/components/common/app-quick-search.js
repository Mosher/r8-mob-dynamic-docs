import { createVNode as _createVNode, resolveComponent as _resolveComponent } from "vue";
import { throttle } from 'ibz-core';
import { ComponentBase, GenerateComponent } from '../component-base';
export class AppQuickSearchProps {}
export class AppQuickSearch extends ComponentBase {
  /**
   * @description 快速搜索
   * @param {*} $event
   * @memberof AppQuickSearch
   */
  quickSearch($event) {
    this.ctx.emit('quickSearch', $event.detail.value);
  }
  /**
   * @description 绘制快速搜索栏
   * @return {*}
   * @memberof AppQuickSearch
   */


  render() {
    return _createVNode(_resolveComponent("ion-header"), {
      "class": 'app-quick-search'
    }, {
      default: () => [_createVNode(_resolveComponent("ion-toolbar"), null, {
        default: () => [_createVNode(_resolveComponent("ion-searchbar"), {
          "showClearButton": true,
          "debounce": '500',
          "onIonChange": $event => throttle(this.quickSearch, [$event], this)
        }, null)]
      })]
    });
  }

}
export const AppQuickSearchComponent = GenerateComponent(AppQuickSearch);