import { onMounted } from 'vue';
import { useRoute, useRouter } from 'vue-router';
/**
 * 组件基类
 *
 * @export
 * @class ComponentBase
 * @template Props 输入属性接口
 */

export class ComponentBase {
  /**
   * Creates an instance of ComponentBase.
   *
   * @param {Props} props
   * @param {SetupContext<EmitsOptions>} ctx
   */
  constructor(props, ctx) {
    /**
     * 路由解析工具类
     *
     * @protected
     * @type {*}
     */
    this.pathToRegExp = require('path-to-regexp');
    this.props = props;
    this.ctx = ctx;
    this.route = useRoute();
    this.router = useRouter();
    onMounted(this.mounted.bind(this));
  }
  /**
   * 构建组件
   *
   * @memberof ComponentBase
   */


  setup() {}
  /**
   * 初始化
   *
   * @memberof ComponentBase
   */


  init() {}
  /**
   * 组件挂载完成
   *
   * @memberof ComponentBase
   */


  mounted() {}
  /**
   * 绘制内容
   *
   * @return {*}  {*}
   * @memberof ComponentBase
   */


  render() {}
  /**
   * 强制更新
   *
   * @memberof ComponentBase
   */


  forceUpdate() {}
  /**
   * 输入属性变更执行
   *
   * @memberof ComponentBase
   */


  watchEffect() {}
  /**
   * 组件销毁
   *
   * @memberof ComponentBase
   */


  unmounted() {}

  renderSlot(name) {
    var _a;

    if (name && ((_a = this.ctx.slots) === null || _a === void 0 ? void 0 : _a[name])) {
      return this.ctx.slots[name]();
    }
  }

}