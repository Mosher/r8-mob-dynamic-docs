import { createVNode as _createVNode, isVNode as _isVNode, resolveComponent as _resolveComponent } from "vue";
import { renderSlot } from 'vue';
import { AppMobMPickUpViewLayoutProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { LayoutComponentBase } from './layout-component-base';

function _isSlot(s) {
  return typeof s === 'function' || Object.prototype.toString.call(s) === '[object Object]' && !_isVNode(s);
}

export class AppDefaultMobMPickupViewLayout extends LayoutComponentBase {
  /**
   * @description 渲染视图头
   * @return {*}  {*}
   * @memberof AppDefaultMobMPickupViewLayout
   */
  renderViewHeader() {
    let _slot;

    return _createVNode(_resolveComponent("ion-header"), null, _isSlot(_slot = renderSlot(this.ctx.slots, 'headerButtons')) ? _slot : {
      default: () => [_slot]
    });
  }
  /**
   * @description 视图主容器内容
   * @return {*}  {any}
   * @memberof AppDefaultMobMPickupViewLayout
   */


  renderViewMainContainerContent() {
    return _createVNode("div", {
      "class": 'view-main-container-content'
    }, [[renderSlot(this.ctx.slots, 'pickupviewpanel')]]);
  }

} //  移动端数据选择视图布局面板

export const AppDefaultMobMPickupViewLayoutComponent = GenerateComponent(AppDefaultMobMPickupViewLayout, new AppMobMPickUpViewLayoutProps());