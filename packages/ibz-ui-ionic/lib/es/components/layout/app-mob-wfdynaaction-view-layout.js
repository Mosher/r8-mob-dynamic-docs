import { createVNode as _createVNode } from "vue";
import { renderSlot } from '@vue/runtime-dom';
import { AppMobWFDynaActionViewLayoutProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { LayoutComponentBase } from './layout-component-base';
/**
 * @description 移动端动态工作流操作视图视图布局组件
 * @export
 * @class AppDefaultMobWFDynaActionViewLayout
 * @extends {AppDefaultDEView<AppMobWFDynaActionViewLayoutProps>}
 */

export class AppDefaultMobWFDynaActionViewLayout extends LayoutComponentBase {
  /**
   * @description 绘制组件
   * @return {*}
   * @memberof AppDefaultMobWFDynaActionViewLayout
   */
  renderViewMainContainerContent() {
    return _createVNode("div", {
      "class": 'view-main-container-content'
    }, [[renderSlot(this.ctx.slots, 'form')]]);
  }
  /**
   * @description 绘制视图底部
   * @return {*}
   * @memberof AppDefaultMobWFDynaActionViewLayout
   */


  renderViewFooter() {
    return renderSlot(this.ctx.slots, 'viewFooter');
  }

} // 移动端动态工作流操作视图视图布局组件

export const AppDefaultMobWFDynaActionViewLayoutComponent = GenerateComponent(AppDefaultMobWFDynaActionViewLayout, new AppMobWFDynaActionViewLayoutProps());