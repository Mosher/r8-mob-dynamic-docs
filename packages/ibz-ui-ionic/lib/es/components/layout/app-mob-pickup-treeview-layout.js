import { createVNode as _createVNode } from "vue";
import { renderSlot } from 'vue';
import { AppMobPickupTreeViewLayoutProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { LayoutComponentBase } from './layout-component-base';
/**
 * 移动端选择树视图视图布局面板（DEFAULT样式）
 *
 * @class AppDefaultMobPickupTreeViewLayout
 * @extends LayoutComponentBase
 */

export class AppDefaultMobPickupTreeViewLayout extends LayoutComponentBase {
  /**
   *
   * @description 视图主容器头部
   * @return {*}
   * @memberof AppDefaultMobPickupTreeViewLayout
   */
  renderViewMainContainerHeader() {
    return _createVNode("div", {
      "class": 'view-main-container-header'
    }, [[renderSlot(this.ctx.slots, 'quickGroupSearch'), renderSlot(this.ctx.slots, 'quickSearch')]]);
  }
  /**
   * @description 视图主容器内容
   * @return {*}  {any[]}
   * @memberof AppDefaultMobPickupTreeViewLayout
   */


  renderViewMainContainerContent() {
    return _createVNode("div", {
      "class": 'view-main-container-content'
    }, [[renderSlot(this.ctx.slots, 'tree')]]);
  }

} //  移动端选择多数据视图视图布局面板部件

export const AppDefaultMobPickupTreeViewLayoutComponent = GenerateComponent(AppDefaultMobPickupTreeViewLayout, new AppMobPickupTreeViewLayoutProps());