import { createVNode as _createVNode } from "vue";
import { renderSlot } from 'vue';
import { AppMobWizardViewLayoutProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { LayoutComponentBase } from './layout-component-base';
/**
 * @description 移动端向导视图视图布局组件
 * @export
 * @class AppDefaultMobWizardViewLayout
 * @extends {AppDefaultDEView<AppMobWizardViewLayoutProps>}
 */

export class AppDefaultMobWizardViewLayout extends LayoutComponentBase {
  /**
   * @description 渲染部件
   * @return {*}
   * @memberof AppDefaultMobWizardViewLayout
   */
  renderViewMainContainerContent() {
    return _createVNode("div", {
      "class": 'view-main-container-content'
    }, [[renderSlot(this.ctx.slots, 'wizardpanel')]]);
  }

} // 应用首页组件

export const AppDefaultMobWizardViewLayoutComponent = GenerateComponent(AppDefaultMobWizardViewLayout, new AppMobWizardViewLayoutProps());