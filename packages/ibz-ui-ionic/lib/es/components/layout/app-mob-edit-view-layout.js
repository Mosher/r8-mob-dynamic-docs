import { createVNode as _createVNode } from "vue";
import { AppMobEditViewLayoutProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { LayoutComponentBase } from './layout-component-base';
export class AppDefaultMobEditViewLayout extends LayoutComponentBase {
  /**
   * @description 视图主容器内容
   * @return {*}
   * @memberof AppDefaultMobEditViewLayout
   */
  renderViewMainContainerContent() {
    return _createVNode("div", {
      "class": 'view-main-container-content'
    }, [[this.renderSlot('form')]]);
  }

} // 移动端实体编辑视图组件

export const AppDefaultMobEditViewLayoutComponent = GenerateComponent(AppDefaultMobEditViewLayout, new AppMobEditViewLayoutProps());