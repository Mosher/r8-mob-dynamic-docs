import { createVNode as _createVNode } from "vue";
import { renderSlot } from 'vue';
import { AppMobPanelViewLayoutProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { LayoutComponentBase } from './layout-component-base';
export class AppDefaultMobPanelViewLayout extends LayoutComponentBase {
  /**
   * @description 视图主容器内容
   * @return {*}
   * @memberof AppDefaultMobMDViewLayout
   */
  renderViewMainContainerContent() {
    return _createVNode("div", {
      "class": 'view-main-container-content'
    }, [[renderSlot(this.ctx.slots, 'panel')]]);
  }

} //  移动端面板视图视图布局面板部件

export const AppDefaultMobPanelViewLayoutComponent = GenerateComponent(AppDefaultMobPanelViewLayout, new AppMobPanelViewLayoutProps());