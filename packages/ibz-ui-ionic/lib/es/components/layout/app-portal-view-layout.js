import { createVNode as _createVNode } from "vue";
import { renderSlot } from 'vue';
import { AppPortalViewLayoutProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { LayoutComponentBase } from './layout-component-base';
/**
 * 移动端应用看板视图视图布局面板
 *
 * @class AppDefaultPortalViewLayout
 * @extends LayoutComponentBase
 */

class AppDefaultPortalViewLayout extends LayoutComponentBase {
  /**
   * @description 视图主容器内容
   * @return {*}
   * @memberof AppDefaultPortalViewLayout
   */
  renderViewMainContainerContent() {
    return _createVNode("div", {
      "class": 'view-main-container-content'
    }, [[renderSlot(this.ctx.slots, 'dashboard')]]);
  }

} //  移动端应用看板视图视图布局面板组件


export const AppDefaultPortalViewLayoutComponent = GenerateComponent(AppDefaultPortalViewLayout, new AppPortalViewLayoutProps());