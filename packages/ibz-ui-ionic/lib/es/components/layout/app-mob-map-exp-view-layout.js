import { createVNode as _createVNode } from "vue";
import { renderSlot } from '@vue/runtime-dom';
import { AppMobMapExpViewLayoutProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { LayoutComponentBase } from './layout-component-base';
/**
 * @description 移动端地图导航视图视图布局组件
 * @export
 * @class AppDefaultMobMapExpViewLayout
 * @extends {AppDefaultDEExpView<AppMobMapExpViewLayoutProps>}
 */

export class AppDefaultMobMapExpViewLayout extends LayoutComponentBase {
  /**
   * @description 视图主容器内容
   * @return {*}
   * @memberof AppDefaultMobHtmlViewLayout
   */
  renderViewMainContainerContent() {
    return _createVNode("div", {
      "class": 'view-main-container-content'
    }, [renderSlot(this.ctx.slots, 'mapexpbar')]);
  }

} // 应用首页组件

export const AppDefaultMobMapExpViewLayoutComponent = GenerateComponent(AppDefaultMobMapExpViewLayout, new AppMobMapExpViewLayoutProps());