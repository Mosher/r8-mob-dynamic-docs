import { createVNode as _createVNode } from "vue";
import { renderSlot } from '@vue/runtime-dom';
import { AppMobChartExpViewLayoutProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { LayoutComponentBase } from './layout-component-base';
/**
 * @description 移动端图表导航视图视图布局组件
 * @export
 * @class AppDefaultMobChartExpViewLayout
 * @extends {AppDefaultExpView<AppMobChartExpViewLayoutProps>}
 */

export class AppDefaultMobChartExpViewLayout extends LayoutComponentBase {
  /**
   * @description 视图主容器内容
   * @return {*}
   * @memberof AppDefaultMobHtmlViewLayout
   */
  renderViewMainContainerContent() {
    return _createVNode("div", {
      "class": 'view-main-container-content'
    }, [renderSlot(this.ctx.slots, 'chartexpbar')]);
  }

} // 图表导航栏组件

export const AppDefaultMobChartExpViewLayoutComponent = GenerateComponent(AppDefaultMobChartExpViewLayout, new AppMobChartExpViewLayoutProps());