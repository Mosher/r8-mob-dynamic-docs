import { createVNode as _createVNode } from "vue";
import { renderSlot } from '@vue/runtime-dom';
import { AppIndexViewLayoutProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { LayoutComponentBase } from './layout-component-base';
/**
 * 应用首页视图布局面板
 *
 * @class AppDefaultIndexViewLayout
 */

export class AppDefaultIndexViewLayout extends LayoutComponentBase {
  /**
   * @description 渲染视图头
   * @return {*}
   * @memberof AppDefaultIndexViewLayout
   */
  renderViewHeader() {
    return null;
  }
  /**
   * @description 渲染视图底部
   * @return {*}
   * @memberof AppDefaultIndexViewLayout
   */


  renderViewFooter() {
    return null;
  }
  /**
   * @description 视图主容器内容
   * @return {*}  {any[]}
   * @memberof AppDefaultIndexViewLayout
   */


  renderViewMainContainerContent() {
    return _createVNode("div", {
      "class": 'view-main-container-content'
    }, [[renderSlot(this.ctx.slots, 'appmenu')]]);
  }

} // 应用首页组件

export const AppDefaultIndexViewLayoutComponent = GenerateComponent(AppDefaultIndexViewLayout, new AppIndexViewLayoutProps());