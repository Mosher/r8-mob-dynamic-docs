import { mergeProps as _mergeProps, isVNode as _isVNode, createTextVNode as _createTextVNode, createVNode as _createVNode, resolveComponent as _resolveComponent } from "vue";
import { toRaw, ref, h } from 'vue';
import { Util } from 'ibz-core';
import { ComponentBase } from '../component-base';

function _isSlot(s) {
  return typeof s === 'function' || Object.prototype.toString.call(s) === '[object Object]' && !_isVNode(s);
}

export class LayoutComponentBase extends ComponentBase {
  constructor() {
    super(...arguments);
    /**
     * @description 当前选中分页
     * @type {*}
     * @memberof LayoutComponentBase
     */

    this.currentTab = ref({});
  }

  renderSlot(name) {
    if (name && this.ctx.slots && this.ctx.slots[name]) {
      return this.ctx.slots[name]();
    }
  }
  /**
   * @description 视图容器样式名
   * @readonly
   * @memberof LayoutComponentBase
   */


  get viewClass() {
    var _a;

    const viewClass = {
      'container-margin': true,
      'container-padding': true,
      'app-view': true,
      [(_a = this.viewInstance.viewType) === null || _a === void 0 ? void 0 : _a.toLowerCase()]: true,
      [Util.srfFilePath2(this.viewInstance.codeName)]: true
    };
    const sysCss = this.viewInstance.getPSSysCss();

    if (sysCss) {
      Object.assign(viewClass, {
        [sysCss.cssName]: true
      });
    }

    return viewClass;
  }
  /**
   * @description 初始化
   * @memberof LayoutComponentBase
   */


  init() {
    this.viewInstance = toRaw(this.props.viewInstance);
    this.context = toRaw(this.props.navContext);
    this.viewParam = toRaw(this.props.navParam);
    this.navDatas = toRaw(this.props.navDatas);
    this.isShowCaptionBar = this.props.isShowCaptionBar;
    this.viewLayoutPanel = this.viewInstance.getPSViewLayoutPanel();
  }
  /**
   * @description 获取栅格布局
   * @param {*} parent 父容器
   * @param {*} child 子容器
   * @return {*}
   * @memberof LayoutComponentBase
   */


  getGridLayoutProps(parent, child) {
    var _a, _b, _c;

    const layout = (_b = (_a = parent === null || parent === void 0 ? void 0 : parent.getPSLayout) === null || _a === void 0 ? void 0 : _a.call(parent)) === null || _b === void 0 ? void 0 : _b.layout;
    let {
      colXS,
      colSM,
      colMD,
      colLG,
      colXSOffset,
      colSMOffset,
      colMDOffset,
      colLGOffset
    } = (_c = child === null || child === void 0 ? void 0 : child.getPSLayoutPos) === null || _c === void 0 ? void 0 : _c.call(child); // 设置初始值

    colXS = !colXS || colXS == -1 ? 12 : colXS / 2;
    colSM = !colSM || colSM == -1 ? 12 : colSM / 2;
    colMD = !colMD || colMD == -1 ? 12 : colMD / 2;
    colLG = !colLG || colLG == -1 ? 12 : colLG / 2;
    colXSOffset = !colXSOffset || colXSOffset == -1 ? 0 : colXSOffset / 2;
    colSMOffset = !colSMOffset || colSMOffset == -1 ? 0 : colSMOffset / 2;
    colMDOffset = !colMDOffset || colMDOffset == -1 ? 0 : colMDOffset / 2;
    colLGOffset = !colLGOffset || colLGOffset == -1 ? 0 : colLGOffset / 2;

    if (layout == 'TABLE_12COL') {
      // 重新计算12列的栅格数值
      colXS = Math.min(colXS * 2, 12);
      colSM = Math.min(colSM * 2, 12);
      colMD = Math.min(colMD * 2, 12);
      colLG = Math.min(colXS * 2, 12); // 重新计算12列的栅格偏移

      const sign = num => num == 0 ? 0 : num / Math.abs(num);

      colXSOffset = sign(colXSOffset) * Math.min(colXSOffset * 2, 12);
      colSMOffset = sign(colSMOffset) * Math.min(colSMOffset * 2, 12);
      colMDOffset = sign(colMDOffset) * Math.min(colMDOffset * 2, 12);
      colLGOffset = sign(colLGOffset) * Math.min(colLGOffset * 2, 12);
    }

    return {
      'size-lg': colLG,
      'size-md': colMD,
      'size-sm': colSM,
      'size-xs': colXS,
      'offset-lg': colLGOffset,
      'offset-md': colMDOffset,
      'offset-sm': colSMOffset,
      'offset-xs': colXSOffset
    };
  }
  /**
   * @description 渲染标题栏
   * @return {*}
   * @memberof LayoutComponentBase
   */


  renderCaptionBar() {
    var _a, _b, _c, _d;

    const {
      showCaptionBar,
      showDataInfoBar
    } = this.viewInstance;

    if (showCaptionBar) {
      return _createVNode(_resolveComponent("ion-title"), {
        "class": 'view-caption-bar'
      }, {
        default: () => [this.viewInstance.getPSSysImage() && _createVNode(_resolveComponent("app-icon"), {
          "class": 'view-icon',
          "icon": this.viewInstance.getPSSysImage()
        }, null), _createVNode("span", {
          "class": 'view-caption'
        }, [this.$tl((_b = (_a = this.viewInstance) === null || _a === void 0 ? void 0 : _a.getCapPSLanguageRes()) === null || _b === void 0 ? void 0 : _b.lanResTag, this.viewInstance.caption), showDataInfoBar ? [_createVNode("span", null, [_createTextVNode("-")]), this.renderSlot('dataInfoBar')] : null]), _createVNode("br", null, null), _createVNode("span", {
          "class": 'view-sub-caption'
        }, [this.$tl((_d = (_c = this.viewInstance) === null || _c === void 0 ? void 0 : _c.getSubCapPSLanguageRes()) === null || _d === void 0 ? void 0 : _d.lanResTag, this.viewInstance.subCaption)])]
      });
    } else {
      if (showDataInfoBar) {
        let _slot;

        return _createVNode(_resolveComponent("ion-title"), {
          "class": "view-info-bar"
        }, _isSlot(_slot = this.renderSlot('dataInfoBar')) ? _slot : {
          default: () => [_slot]
        });
      } else {
        return null;
      }
    }
  }
  /**
   * @description 渲染视图头
   * @return {*}  {*}
   * @memberof LayoutComponentBase
   */


  renderViewHeader() {
    return this.isShowCaptionBar ? _createVNode(_resolveComponent("ion-header"), {
      "className": 'view-header'
    }, {
      default: () => [_createVNode(_resolveComponent("ion-toolbar"), null, {
        default: () => [this.ctx.slots.topMessage ? this.renderSlot('topMessage') : null, this.renderCaptionBar()]
      })]
    }) : null;
  }
  /**
   *
   * @description 视图主容器头部
   * @return {*}
   * @memberof LayoutComponentBase
   */


  renderViewMainContainerHeader() {
    return null;
  }
  /**
   *
   * @description 视图主容器内容
   * @return {*}
   * @memberof LayoutComponentBase
   */


  renderViewMainContainerContent() {
    return null;
  }
  /**
   * @description 视图主容器底部
   * @return {*}
   * @memberof LayoutComponentBase
   */


  renderViewMainContainerFooter() {
    return null;
  }
  /**
   * @description 视图顶部插槽
   *
   * @return {*}  {any[]}
   * @memberof LayoutComponentBase
   */


  renderViewTopSlot() {
    return [this.renderSlot('lefttoolbar'), this.renderSlot('righttoolbar')];
  }
  /**
   * @description 渲染视图内容
   * @return {*}  {*}
   * @memberof LayoutComponentBase
   */


  renderViewContent() {
    return _createVNode(_resolveComponent("ion-content"), {
      "class": 'view-body'
    }, {
      default: () => [this.renderViewTopSlot(), _createVNode("div", {
        "class": 'view-main-container'
      }, [this.renderSlot('bodyMessage'), this.renderViewMainContainerHeader(), this.renderViewMainContainerContent(), this.renderViewMainContainerFooter()])]
    });
  }
  /**
   * @description 渲染视图底部
   * @return {*}  {*}
   * @memberof LayoutComponentBase
   */


  renderViewFooter() {
    return _createVNode(_resolveComponent("ion-footer"), {
      "class": 'view-footer'
    }, {
      default: () => [this.ctx.slots.bottomMessage ? this.renderSlot('bottomMessage') : null]
    });
  }
  /**
   * @description 获取面板项样式名
   * @param {IPSPanelItem} item 面板项
   * @return {*}
   * @memberof LayoutComponentBase
   */


  getDetailClass(item) {
    var _a;

    const {
      itemType,
      name
    } = item;
    const detailClass = {
      [`app-view-layout-panel-${itemType.toLowerCase()}`]: true,
      [`panel-${itemType.toLowerCase()}-${name.toLowerCase()}`]: true
    };
    const css = (_a = item.getPSSysCss) === null || _a === void 0 ? void 0 : _a.call(item);

    if (css) {
      Object.assign(detailClass, {
        [`${css.cssName}`]: true
      });
    }

    return detailClass;
  }
  /**
   * @description 计算面板项样式及类名
   * @param {IPSPanelItem} item 面板项
   * @return {*}
   * @memberof LayoutComponentBase
   */


  computePanelItemStyle(item) {
    var _a;

    const layoutPos = item.getPSLayoutPos(); //  项样式

    const detailStyle = {}; //  宽高

    const {
      width,
      height
    } = layoutPos;

    if (width) {
      Object.assign(detailStyle, {
        width: width + 'px'
      });
    }

    if (height) {
      Object.assign(detailStyle, {
        height: height + 'px'
      });
    } //  class名称


    const detailClassName = this.getDetailClass(item);

    if (item.itemType == 'TABPANEL') {
      Object.assign(detailClassName, {
        'panel-tabs': true
      });
    }

    const layoutMode = (_a = item.getPSLayout()) === null || _a === void 0 ? void 0 : _a.layout;

    if (layoutMode == 'FLEX') {
      const grow = layoutPos.grow;
      Object.assign(detailStyle, {
        grow: grow != -1 ? grow : 0
      });
    }

    return {
      detailStyle: detailStyle,
      detailClassName: detailClassName
    };
  }
  /**
   * @description 渲染无父面板项
   * @param {IPSPanelItem} item 面板项
   * @param {*} content 渲染内容
   * @return {*}
   * @memberof LayoutComponentBase
   */


  renderNoParentPanelItem(item, content) {
    var _a, _b;

    const layout = item.getPSLayout();
    const layoutMode = layout === null || layout === void 0 ? void 0 : layout.layout;
    const {
      detailStyle,
      detailClassName
    } = this.computePanelItemStyle(item);
    const parent = (_a = item.getParentPSModelObject) === null || _a === void 0 ? void 0 : _a.call(item); //  是否为根

    const rootFlag = ((_b = parent) === null || _b === void 0 ? void 0 : _b.controlType) == 'VIEWLAYOUTPANEL' ? true : false;

    if (layout && layoutMode == 'FLEX') {
      let cssStyle = 'width: 100%; height: 100%; overflow: auto; display: flex;';
      cssStyle += layout.dir ? `flex-direction: ${layout.dir};` : '';
      cssStyle += layout.align ? `justify-content: ${layout.align};` : '';
      cssStyle += layout.vAlign ? `align-items: ${layout.vAlign};` : '';

      if (rootFlag) {
        return _createVNode("div", {
          "style": detailStyle,
          "class": detailClassName
        }, [content]);
      } else {
        return _createVNode("div", {
          "style": cssStyle
        }, [_createVNode("div", {
          "style": detailStyle,
          "class": detailClassName
        }, [content])]);
      }
    } else {
      const attrs = this.getGridLayoutProps({}, item);

      if (rootFlag) {
        return _createVNode(_resolveComponent("ion-col"), _mergeProps({
          "class": detailClassName,
          "style": detailStyle
        }, attrs), _isSlot(content) ? content : {
          default: () => [content]
        });
      } else {
        return _createVNode(_resolveComponent("ion-row"), {
          "style": 'height: 100%'
        }, {
          default: () => [_createVNode(_resolveComponent("ion-col"), _mergeProps({
            "class": detailClassName,
            "style": detailStyle
          }, attrs), _isSlot(content) ? content : {
            default: () => [content]
          })]
        });
      }
    }
  }
  /**
   * @description 渲染面板子项
   * @param {*} item 面板项实例对象
   * @return {*}
   * @memberof LayoutComponentBase
   */


  renderPanelItems(item) {
    var _a, _b, _c;

    const panelItems = ((_b = (_a = item).getPSPanelItems) === null || _b === void 0 ? void 0 : _b.call(_a)) || [];

    if (!panelItems.length) {
      return null;
    }

    const layout = item.getPSLayout();
    const layoutMode = (_c = item.getPSLayout()) === null || _c === void 0 ? void 0 : _c.layout; //  FLEX布局

    if (layout && layoutMode == 'FLEX') {
      let cssStyle = 'width: 100%; height: 100%; overflow: auto; display: flex;';
      cssStyle += layout.dir ? `flex-direction: ${layout.dir};` : '';
      cssStyle += layout.align ? `justify-content: ${layout.align};` : '';
      cssStyle += layout.vAlign ? `align-items: ${layout.vAlign};` : '';
      return _createVNode("div", {
        "style": cssStyle
      }, [panelItems.map((item, index) => {
        const {
          detailStyle,
          detailClassName
        } = this.computePanelItemStyle(item);
        return _createVNode("div", {
          "style": detailStyle,
          "class": detailClassName
        }, [this.renderByDetailType(item, true)]);
      })]);
    } else {
      let _slot3;

      //  栅格布局
      return _createVNode(_resolveComponent("ion-row"), {
        "style": 'height: 100%'
      }, _isSlot(_slot3 = panelItems.map((item, index) => {
        let _slot2;

        const {
          detailStyle,
          detailClassName
        } = this.computePanelItemStyle(item);
        const attrs = this.getGridLayoutProps(item, item);
        return _createVNode(_resolveComponent("ion-col"), _mergeProps({
          "style": detailStyle,
          "class": detailClassName
        }, attrs), _isSlot(_slot2 = this.renderByDetailType(item, true)) ? _slot2 : {
          default: () => [_slot2]
        });
      })) ? _slot3 : {
        default: () => [_slot3]
      });
    }
  }
  /**
   * @description 渲染面板容器
   * @param {IPSPanelContainer} item 面板容器实例对象
   * @param {boolean} [hasParent=false] 是否具有父容器
   * @return {*}
   * @memberof LayoutComponentBase
   */


  renderContainer(item, hasParent = false) {
    var _a, _b;

    const layoutMode = (_a = item.getPSLayout()) === null || _a === void 0 ? void 0 : _a.layout;
    let containerClass = 'app-view-layout-panel-container';

    if (item.getPSSysCss()) {
      containerClass += ` ${(_b = item.getPSSysCss()) === null || _b === void 0 ? void 0 : _b.cssName}`;
    } //  父为容器时直接绘制子内容


    if (hasParent) {
      return this.renderPanelItems(item);
    }

    if (layoutMode == 'FLEX') {
      return _createVNode("div", {
        "class": containerClass
      }, [this.renderPanelItems(item)]);
    } else {
      let _slot4;

      const attrs = this.getGridLayoutProps({}, item);
      return _createVNode(_resolveComponent("ion-col"), attrs, _isSlot(_slot4 = this.renderPanelItems(item)) ? _slot4 : {
        default: () => [_slot4]
      });
    }
  }
  /**
   * @description 处理编辑器事件
   * @param {IEditorEventParam} event
   * @memberof LayoutComponentBase
   */


  handleEditorEvent(event) {}
  /**
   * @description 渲染编辑器
   * @param {IPSEditor} editor 编辑器实例对象
   * @param {IPSPanelField} parentItem 父容器
   * @return {*}
   * @memberof LayoutComponentBase
   */


  renderEditor(editor, parentItem) {
    const targetCtrlComponent = App.getComponentService().getEditorComponent(editor === null || editor === void 0 ? void 0 : editor.editorType, (editor === null || editor === void 0 ? void 0 : editor.editorStyle) ? editor === null || editor === void 0 ? void 0 : editor.editorStyle : 'DEFAULT', editor.getPSSysPFPlugin() ? `${editor === null || editor === void 0 ? void 0 : editor.editorType}_${editor === null || editor === void 0 ? void 0 : editor.editorStyle}` : undefined);
    return h(targetCtrlComponent, {
      editorInstance: editor,
      containerCtrl: this.viewInstance,
      contextData: this.context,
      parentItem: parentItem,
      navContext: Util.deepCopy(this.context),
      navParam: Util.deepCopy(this.viewParam),
      value: this.context[(editor === null || editor === void 0 ? void 0 : editor.name) || ''],
      onEditorEvent: event => {
        this.handleEditorEvent(event);
      }
    });
  }
  /**
   * @description 渲染面板属性
   * @param {IPSPanelField} item 面板属性实例对象
   * @param {boolean} [hasParent=false] 是否具有父容器
   * @return {*}
   * @memberof LayoutComponentBase
   */


  renderField(item, hasParent = false) {
    const {
      caption
    } = item;
    const editor = item.getPSEditor();

    const content = _createVNode("div", {
      "class": 'panel-item-field'
    }, [caption && _createVNode(_resolveComponent("ion-label"), {
      "class": 'panel-item-field-label'
    }, _isSlot(caption) ? caption : {
      default: () => [caption]
    }), editor && this.renderEditor(editor, item)]);

    if (hasParent) {
      return content;
    } else {
      return this.renderNoParentPanelItem(item, content);
    }
  }
  /**
   * @description 渲染面板直接内容
   * @param {IPSPanelRawItem} item 面板直接内容对象实例
   * @param {boolean} [hasParent=false] 是否具有父容器
   * @return {*}
   * @memberof LayoutComponentBase
   */


  renderRawItem(item, hasParent = false) {
    var _a;

    const {
      contentType,
      htmlContent,
      getPSSysImage,
      rawContent
    } = item;
    let element = undefined;

    if (contentType == 'HTML' && htmlContent) {
      element = h('div', {
        innerHTML: htmlContent
      });
    }

    let content;

    switch (contentType) {
      case 'HTML':
        content = element ? element : htmlContent;
        break;

      case 'RAW':
        content = rawContent;
        break;

      case 'IMAGE':
        content = _createVNode("img", {
          "src": (_a = getPSSysImage === null || getPSSysImage === void 0 ? void 0 : getPSSysImage()) === null || _a === void 0 ? void 0 : _a.imagePath
        }, null);
        break;

      case 'MARKDOWN':
        content = _createVNode("div", null, [_createTextVNode("MARKDOWN\u76F4\u63A5\u5185\u5BB9\u6682\u672A\u652F\u6301")]);
        break;
    }

    if (hasParent) {
      return content;
    } else {
      return this.renderNoParentPanelItem(item, content);
    }
  }
  /**
   * @description 分页面板切换
   * @param {string} key 分页面板标识
   * @param {*} event 源事件对象
   * @memberof LayoutComponentBase
   */


  segmentChanged(key, event) {
    if (key) {
      this.currentTab.value[key] = event.detail.value;
    }
  }
  /**
   * @description 渲染面板分页面板
   * @param {IPSPanelTabPanel} item 面板分页面板实例对象
   * @param {boolean} [hasParent=false] 是否具有父容器
   * @return {*}
   * @memberof LayoutComponentBase
   */


  renderTabPanel(item, hasParent = false) {
    const tabPages = item.getPSPanelTabPages() || [];

    if (tabPages.length) {
      let _slot5;

      //  默认选中第一个分页
      if (!this.currentTab.value[item.name]) {
        this.currentTab.value[item.name] = tabPages[0].name;
      }

      const content = [_createVNode("div", {
        "class": 'header'
      }, [_createVNode(_resolveComponent("ion-segment"), {
        "value": this.currentTab.value[item.name],
        "onIonChange": $event => this.segmentChanged(item.name, $event)
      }, _isSlot(_slot5 = tabPages.map((page, index) => {
        return _createVNode(_resolveComponent("ion-segment-button"), {
          "value": page.name
        }, {
          default: () => [_createVNode(_resolveComponent("ion-label"), null, {
            default: () => [page.caption]
          })]
        });
      })) ? _slot5 : {
        default: () => [_slot5]
      })]), _createVNode("div", {
        "class": 'content'
      }, [tabPages.map((page, index) => {
        return this.renderByDetailType(page);
      })])];

      if (hasParent) {
        return content;
      } else {
        return this.renderNoParentPanelItem(item, content);
      }
    }
  }
  /**
   * @description 渲染面板分页
   * @param {IPSPanelTabPage} item 面板分页实例对象
   * @param {boolean} [hasParent=false] 是否具有父容器
   * @return {*}
   * @memberof LayoutComponentBase
   */


  renderTabPage(item, hasParent = false) {
    var _a;

    const parent = (_a = item.getParentPSModelObject) === null || _a === void 0 ? void 0 : _a.call(item);
    const currentKey = this.currentTab.value[parent === null || parent === void 0 ? void 0 : parent.name];
    return _createVNode("div", {
      "class": this.getDetailClass(item),
      "style": currentKey !== item.name ? 'display: none;' : ''
    }, [this.renderPanelItems(item)]);
  }
  /**
   * @description 渲染面板控件
   * @param {IPSPanelCtrlPos} item 面板组件实例对象
   * @param {boolean} [hasParent=false] 是否具有父容器
   * @return {*}
   * @memberof LayoutComponentBase
   */


  renderCtrlPos(item, hasParent = false) {
    var _a;

    const content = this.renderSlot((_a = item.name) === null || _a === void 0 ? void 0 : _a.toLowerCase());

    if (hasParent) {
      return content;
    } else {
      return this.renderNoParentPanelItem(item, content);
    }
  }
  /**
   * @description 根据面板项类型绘制
   * @param {*} item 面板项
   * @param {boolean} [hasParent=false] 是否具有父容器
   * @return {*}
   * @memberof LayoutComponentBase
   */


  renderByDetailType(item, hasParent = false) {
    switch (item.itemType) {
      case 'CONTAINER':
        return this.renderContainer(item, hasParent);

      case 'FIELD':
        return this.renderField(item, hasParent);

      case 'RAWITEM':
        return this.renderRawItem(item, hasParent);

      case 'TABPANEL':
        return this.renderTabPanel(item, hasParent);

      case 'TABPAGE':
        return this.renderTabPage(item, hasParent);

      case 'CTRLPOS':
        return this.renderCtrlPos(item, hasParent);
    }
  }
  /**
   * @description 渲染视图布局面板
   * @return {*}
   * @memberof LayoutComponentBase
   */


  renderViewLayoutPanel() {
    var _a, _b, _c, _d;

    const bodyOnly = (_a = this.viewLayoutPanel) === null || _a === void 0 ? void 0 : _a.layoutBodyOnly;
    return [bodyOnly ? this.renderViewHeader() : null, _createVNode(_resolveComponent("ion-content"), {
      "class": 'view-body'
    }, {
      default: () => [bodyOnly && ((_b = this.ctx.slots) === null || _b === void 0 ? void 0 : _b.bodyMessage) ? this.renderSlot('bodyMessage') : null, _createVNode(_resolveComponent("ion-row"), {
        "class": 'app-view-layout-panel'
      }, {
        default: () => [(_d = (_c = this.viewLayoutPanel) === null || _c === void 0 ? void 0 : _c.getRootPSPanelItems()) === null || _d === void 0 ? void 0 : _d.map(item => {
          return this.renderByDetailType(item);
        })]
      })]
    }), bodyOnly ? this.renderViewFooter() : null];
  }
  /**
   * @description 布局面板渲染
   * @return {*}
   * @memberof LayoutComponentBase
   */


  render() {
    return _createVNode(_resolveComponent("ion-page"), {
      "class": Object.assign({
        'ion-page': true
      }, this.viewClass)
    }, {
      default: () => [this.viewLayoutPanel && this.viewLayoutPanel.useDefaultLayout ? [this.renderViewHeader(), this.renderViewContent(), this.renderViewFooter()] : this.renderViewLayoutPanel()]
    });
  }

}