import { createVNode as _createVNode } from "vue";
import { renderSlot } from 'vue';
import { AppMobListExpViewLayoutProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { LayoutComponentBase } from './layout-component-base';
/**
 * @description 移动端列表导航视图视图布局组件
 * @export
 * @class AppDefaultMobListExpViewLayout
 * @extends {AppDefaultDEExpView<AppMobListExpViewLayoutProps>}
 */

export class AppDefaultMobListExpViewLayout extends LayoutComponentBase {
  /**
   * @description 渲染部件
   * @return {*}
   * @memberof AppDefaultMobListExpViewLayout
   */
  renderViewMainContainerContent() {
    return _createVNode("div", {
      "class": 'view-main-container-content'
    }, [[renderSlot(this.ctx.slots, 'listexpbar')]]);
  }

} // 应用首页组件

export const AppDefaultMobListExpViewLayoutComponent = GenerateComponent(AppDefaultMobListExpViewLayout, new AppMobListExpViewLayoutProps());