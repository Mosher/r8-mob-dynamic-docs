import { createVNode as _createVNode } from "vue";
import { renderSlot } from 'vue';
import { AppMobDEDashboardViewLayoutProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { LayoutComponentBase } from './layout-component-base';
/**
 * 移动端实体数据看板视图视图布局面板
 *
 * @exports
 * @class AppDefaultMobDEDashboardViewLayout
 * @extends LayoutComponentBase
 */

export class AppDefaultMobDEDashboardViewLayout extends LayoutComponentBase {
  /**
   *
   * @description 视图主容器头部
   * @return {*}
   * @memberof AppMobDEDashboardViewLayout
   */
  renderViewMainContainerHeader() {
    return _createVNode("div", {
      "class": 'view-main-container-header'
    }, [[renderSlot(this.ctx.slots, 'searchform'), renderSlot(this.ctx.slots, 'quicksearchform')]]);
  }
  /**
   * @description 视图主容器内容
   * @return {*}
   * @memberof AppMobDEDashboardViewLayout
   */


  renderViewMainContainerContent() {
    return _createVNode("div", {
      "class": 'view-main-container-content'
    }, [[renderSlot(this.ctx.slots, 'dashboard')]]);
  }

} //  移动端实体数据看板视图视图布局面板组件

export const AppDefaultMobDEDashboardViewLayoutComponent = GenerateComponent(AppDefaultMobDEDashboardViewLayout, new AppMobDEDashboardViewLayoutProps());