import { createVNode as _createVNode } from "vue";
import { renderSlot } from '@vue/runtime-dom';
import { AppMobCustomViewLayoutProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { LayoutComponentBase } from './layout-component-base';
export class AppDefaultMobCustomViewLayout extends LayoutComponentBase {
  /**
   *
   * @description 视图主容器头部
   * @return {*}
   * @memberof MultiDataViewLayoutComponentBase
   */
  renderViewMainContainerHeader() {
    return _createVNode("div", {
      "class": 'view-main-container-header'
    }, [[renderSlot(this.ctx.slots, 'quickGroupSearch'), renderSlot(this.ctx.slots, 'quickSearch'), renderSlot(this.ctx.slots, 'searchform'), renderSlot(this.ctx.slots, 'quicksearchform')]]);
  }
  /**
   * @description 视图主容器内容
   * @return {*}
   * @memberof AppDefaultMobCustomViewLayout
   */


  renderViewMainContainerContent() {
    const ctrlSlots = [];

    for (const slotName in this.ctx.slots) {
      if (!Object.is('topMessage', slotName) && !Object.is('bottomMessage', slotName) && !Object.is('bodyMessage', slotName) && !Object.is('quickGroupSearch', slotName) && !Object.is('quickSearch', slotName) && !Object.is('searchform', slotName) && !Object.is('quicksearchform', slotName)) {
        ctrlSlots.push(renderSlot(this.ctx.slots, slotName));
      }
    }

    return _createVNode("div", {
      "class": 'view-main-container-content'
    }, [ctrlSlots]);
  }

} // 自定义视图布局面板

export const AppDefaultMobCustomViewLayoutComponent = GenerateComponent(AppDefaultMobCustomViewLayout, new AppMobCustomViewLayoutProps());