import { createVNode as _createVNode } from "vue";
import { renderSlot } from 'vue';
import { AppMobChartViewLayoutProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { MultiDataViewLayoutComponentBase } from './multi-data-view-layout-component-base';
/**
 * 移动端图表视图视图布局面板
 *
 * @class AppDefaultMobChartViewLayout
 * @extends MultiDataViewLayoutComponentBase
 */

export class AppDefaultMobChartViewLayout extends MultiDataViewLayoutComponentBase {
  /**
   * @description 视图主容器内容
   * @return {*}  {*}
   * @memberof AppDefaultMobChartViewLayout
   */
  renderViewMainContainerContent() {
    return _createVNode("div", {
      "class": 'view-main-container-content'
    }, [[renderSlot(this.ctx.slots, 'chart')]]);
  }

} //  移动端图表视图视图布局面板部件

export const AppDefaultMobChartViewLayoutComponent = GenerateComponent(AppDefaultMobChartViewLayout, new AppMobChartViewLayoutProps());