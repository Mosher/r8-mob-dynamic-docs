import { createVNode as _createVNode } from "vue";
import { renderSlot } from 'vue';
import { AppMobWFDynaEditViewLayoutProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { LayoutComponentBase } from './layout-component-base';
/**
 * @description 移动端动态工作流编辑视图视图布局组件
 * @export
 * @class AppDefaultMobWFDynaEditViewLayout
 * @extends {AppDefaultDeView<AppMobWFDynaEditViewLayoutProps>}
 */

export class AppDefaultMobWFDynaEditViewLayout extends LayoutComponentBase {
  /**
   * @description 渲染部件
   * @return {*}
   * @memberof AppDefaultMobWFDynaEditViewLayout
   */
  renderViewMainContainerContent() {
    return _createVNode("div", {
      "class": 'view-main-container-content'
    }, [[renderSlot(this.ctx.slots, 'form')]]);
  }

} // 应用首页组件

export const AppDefaultMobWFDynaEditViewLayoutComponent = GenerateComponent(AppDefaultMobWFDynaEditViewLayout, new AppMobWFDynaEditViewLayoutProps());