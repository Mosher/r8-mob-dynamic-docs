import { createVNode as _createVNode } from "vue";
import { renderSlot } from '@vue/runtime-dom';
import { AppMobTreeExpViewLayoutProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { LayoutComponentBase } from './layout-component-base';
/**
 * @description 移动端树导航视图视图布局组件
 * @export
 * @class AppDefaultMobTreeExpViewLayout
 * @extends {AppDefaultDEExpView<AppMobTreeExpViewLayoutProps>}
 */

export class AppDefaultMobTreeExpViewLayout extends LayoutComponentBase {
  /**
  * @description 视图主容器内容
  * @return {*}
  * @memberof AppDefaultMobHtmlViewLayout
  */
  renderViewMainContainerContent() {
    return _createVNode("div", {
      "class": 'view-main-container-content'
    }, [renderSlot(this.ctx.slots, 'treeexpbar')]);
  }

} // 应用首页组件

export const AppDefaultMobTreeExpViewLayoutComponent = GenerateComponent(AppDefaultMobTreeExpViewLayout, new AppMobTreeExpViewLayoutProps());