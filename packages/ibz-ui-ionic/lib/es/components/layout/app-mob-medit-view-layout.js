import { createVNode as _createVNode } from "vue";
import { renderSlot } from '@vue/runtime-dom';
import { AppMobMEditViewLayoutProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { LayoutComponentBase } from './layout-component-base';
export class AppDefaultMobMEditViewLayout extends LayoutComponentBase {
  /**
   * @description 视图主容器内容
   * @return {*}  {any}
   * @memberof AppDefaultMobMEditViewLayout
   */
  renderViewMainContainerContent() {
    return _createVNode("div", {
      "class": 'view-main-container-content'
    }, [[renderSlot(this.ctx.slots, 'meditviewpanel')]]);
  }

} // 移动端多表单编辑视图布局组件

export const AppDefaultMobMEditViewLayoutComponent = GenerateComponent(AppDefaultMobMEditViewLayout, new AppMobMEditViewLayoutProps());