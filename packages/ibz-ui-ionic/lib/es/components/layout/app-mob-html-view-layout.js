import { createVNode as _createVNode } from "vue";
import { renderSlot } from '@vue/runtime-dom';
import { AppMobHtmlViewLayoutProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { LayoutComponentBase } from './layout-component-base';
export class AppDefaultMobHtmlViewLayout extends LayoutComponentBase {
  /**
   * @description 视图主容器内容
   * @return {*}
   * @memberof AppDefaultMobHtmlViewLayout
   */
  renderViewMainContainerContent() {
    return _createVNode("div", {
      "class": 'view-main-container-content'
    }, [renderSlot(this.ctx.slots, 'htmlContent')]);
  }

} // 应用首页组件

export const AppDefaultMobHtmlViewLayoutComponent = GenerateComponent(AppDefaultMobHtmlViewLayout, new AppMobHtmlViewLayoutProps());