import { createVNode as _createVNode } from "vue";
import { renderSlot } from 'vue';
import { AppMobMDViewLayoutProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { MultiDataViewLayoutComponentBase } from './multi-data-view-layout-component-base';
/**
 * 移动端多数据视图视图布局面板（DEFAULT样式）
 *
 * @class AppDefaultMobMDViewLayout
 * @extends MultiDataViewLayoutComponentBase
 */

export class AppDefaultMobMDViewLayout extends MultiDataViewLayoutComponentBase {
  /**
   * @description 视图主容器内容
   * @return {*}  {any}
   * @memberof AppDefaultMobMDViewLayout
   */
  renderViewMainContainerContent() {
    return _createVNode("div", {
      "class": 'view-main-container-content'
    }, [renderSlot(this.ctx.slots, 'mdctrl')]);
  }

} //  移动端多数据视图视图布局面板部件

export const AppDefaultMobMDViewLayoutComponent = GenerateComponent(AppDefaultMobMDViewLayout, new AppMobMDViewLayoutProps());