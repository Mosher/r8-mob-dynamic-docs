import { createVNode as _createVNode } from "vue";
import { h, shallowReactive } from 'vue';
import { AppMobPickUpViewPanelProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { CtrlComponentBase } from './ctrl-component-base';
export class AppMobPickUpViewPanel extends CtrlComponentBase {
  /**
   * @description 设置响应式
   * @memberof AppMobPickUpViewPanel
   */
  setup() {
    this.c = shallowReactive(this.getCtrlControllerByType('PICKUPVIEWPANEL'));
    super.setup();
  }
  /**
   * @description 绘制选择视图面板
   * @return {*}
   * @memberof AppMobPickUpViewPanel
   */


  renderPickUpViewPanel() {
    var _a;

    const embeddedView = this.c.controlInstance.getEmbeddedPSAppDEView();
    const viewComponent = App.getComponentService().getViewTypeComponent(embeddedView.viewType, embeddedView.viewStyle, (_a = embeddedView.getPSSysPFPlugin()) === null || _a === void 0 ? void 0 : _a.pluginCode);

    if (viewComponent) {
      return h(viewComponent, {
        viewPath: embeddedView.modelPath,
        navContext: this.c.context,
        navParam: this.c.viewParam,
        navDatas: this.c.navDatas,
        isMultiple: this.c.isMultiple,
        viewState: this.c.viewState,
        isShowCaptionBar: false,
        viewShowMode: 'EMBEDDED',
        onViewEvent: ({
          viewname,
          action,
          data
        }) => {
          this.c.handlePickUPViewEvent(action, data);
        }
      });
    } else {
      return _createVNode("div", {
        "class": 'no-embedded-view'
      }, [this.$tl('widget.mobpickupviewpanel.nopickerview', '无选择视图')]);
    }
  }
  /**
   * @description
   * @return {*}
   * @memberof AppMobPickUpViewPanel
   */


  render() {
    if (!this.controlIsLoaded.value) {
      return null;
    }

    const {
      width,
      height
    } = this.c.controlInstance;
    const controlstyle = {
      width: width ? `${width}px` : '100%',
      height: height ? `${height}px` : '100%'
    };
    return _createVNode("div", {
      "class": Object.assign({}, this.classNames),
      "style": controlstyle
    }, [this.renderPickUpViewPanel()]);
  }

} // 移动端选择视图部件

export const AppMobPickUpViewPanelComponent = GenerateComponent(AppMobPickUpViewPanel, Object.keys(new AppMobPickUpViewPanelProps()));