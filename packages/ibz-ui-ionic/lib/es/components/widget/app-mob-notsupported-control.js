import { createVNode as _createVNode } from "vue";
import { shallowReactive } from 'vue';
import { AppCtrlProps, AppCtrlControllerBase } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { CtrlComponentBase } from './ctrl-component-base';
/**
 * 未支持部件
 *
 * @export
 * @class AppMobNotSupportedControl
 * @extends {ComponentBase}
 */

export class AppMobNotSupportedControl extends CtrlComponentBase {
  /**
   * 设置响应式
   *
   * @public
   * @memberof AppMobNotSupportedControl
   */
  setup() {
    this.c = shallowReactive(new AppCtrlControllerBase(this.props));
    super.setup();
  }
  /**
   * 绘制内容
   *
   * @public
   * @memberof AppMobNotSupportedControl
   */


  render() {
    var _a, _b;

    if (!this.controlIsLoaded.value) {
      return null;
    }

    const flexStyle = 'width: 100%; height: 100%; overflow: auto; display: flex;justify-content:center;align-items:center;';
    return _createVNode("div", {
      "class": 'app-ctrl',
      "style": flexStyle
    }, [`${this.$tl('share.notsupported', '暂未支持')} ${(_b = (_a = this.c) === null || _a === void 0 ? void 0 : _a.controlInstance) === null || _b === void 0 ? void 0 : _b.controlType} ${this.$tl('share.widget', '部件')}`]);
  }

} // 应用菜单组件

export const AppMobNotSupportedControlComponent = GenerateComponent(AppMobNotSupportedControl, Object.keys(new AppCtrlProps()));