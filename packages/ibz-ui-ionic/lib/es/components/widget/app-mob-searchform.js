import { createVNode as _createVNode, isVNode as _isVNode, resolveComponent as _resolveComponent } from "vue";
import { shallowReactive, reactive, watch, toRef } from 'vue';
import { menuController } from '@ionic/core';
import { LogUtil, Util } from 'ibz-core';
import { AppMobSearchFormProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { AppMobForm } from './app-mob-form/app-mob-form';
/**
 * 移动端搜索表单
 *
 * @author chitanda
 * @date 2021-08-06 10:08:39
 * @export
 * @class AppMobSearchForm
 * @extends {ComponentBase}
 */

function _isSlot(s) {
  return typeof s === 'function' || Object.prototype.toString.call(s) === '[object Object]' && !_isVNode(s);
}

export class AppMobSearchForm extends AppMobForm {
  constructor() {
    super(...arguments);
    /**
     * @description 搜索表单uuid
     * @type {string}
     * @memberof AppMobSearchForm
     */

    this.uuid = Util.createUUID();
  }
  /**
   * @description 设置响应式
   * @memberof AppMobSearchForm
   */


  setup() {
    this.c = shallowReactive(this.getCtrlControllerByType('SEARCHFORM'));
    watch(() => this.c.isExpandSearchForm, () => {
      if (this.c.isExpandSearchForm) {
        this.openSearchForm();
      }
    }, {
      immediate: true
    }); // TODO 不能使用super.setup 这会导致c指向问题

    this.controlIsLoaded = toRef(this.c, 'controlIsLoaded');
    this.initReactive();
    this.emitCtrlEvent = this.emitCtrlEvent.bind(this);
    this.c.hooks.event.tap(this.emitCtrlEvent);
    this.emitCloseView = this.emitCloseView.bind(this);
    this.c.hooks.closeView.tap(this.emitCloseView);
  }
  /**
   * @description 初始化响应数据
   * @memberof AppMobSearchForm
   */


  initReactive() {
    super.initReactive();
    this.c.isExpandSearchForm = reactive(this.c.isExpandSearchForm);
  }
  /**
   * @description 重置
   * @memberof AppMobSearchForm
   */


  onReset() {
    this.c.onReset();
  }
  /**
   * @description 搜索
   * @memberof AppMobSearchForm
   */


  onSearch() {
    this.c.onSearch();
    this.closeSearchForm();
  }
  /**
   * @description 打开搜索表单
   * @memberof AppMobSearchForm
   */


  async openSearchForm() {
    await menuController.enable(true, this.uuid);
    menuController.open(this.uuid);
  }
  /**
   * @description 关闭搜索表单
   * @memberof AppMobSearchForm
   */


  closeSearchForm() {
    menuController.close(this.uuid);
  }
  /**
   * @description 根据成员类型绘制
   * @param {*} modelJson 模型数据
   * @param {number} index 索引
   * @return {*}
   * @memberof AppMobSearchForm
   */


  renderByDetailType(modelJson, index) {
    var _a, _b;

    if ((_a = modelJson.getPSSysPFPlugin()) === null || _a === void 0 ? void 0 : _a.pluginCode) {
      const ctrlItemPluginInstance = App.getPluginService().getCtrlItemPluginByTag(((_b = modelJson.getPSSysPFPlugin()) === null || _b === void 0 ? void 0 : _b.pluginCode) || '');

      if (ctrlItemPluginInstance) {
        return ctrlItemPluginInstance.renderItem(this.c.data[modelJson.name], this.c, this);
      }
    } else {
      switch (modelJson.detailType) {
        case 'FORMPAGE':
          return this.renderFormPage(modelJson, index);

        case 'GROUPPANEL':
          return this.renderGroupPanel(modelJson, index);

        case 'FORMITEM':
          return this.renderFormItem(modelJson, index);

        default:
          LogUtil.log(`${this.$tl('share.notsupported', '暂未实现')} ${modelJson.detailType} ${this.$tl('widget.mobsearchform.detailtype', '类型表单成员')}`);
      }
    }
  }
  /**
   * @description 绘制搜索表单
   * @param {*} className 类名
   * @param {*} controlStyle 样式
   * @return {*}
   * @memberof AppMobSearchForm
   */


  renderSearchForm(className, controlStyle) {
    let _slot, _slot2, _slot3;

    return [_createVNode(_resolveComponent("ion-header"), {
      "class": 'searchForm-header'
    }, {
      default: () => [_createVNode(_resolveComponent("ion-toolbar"), null, {
        default: () => [_createVNode(_resolveComponent("ion-buttons"), {
          "slot": 'end'
        }, {
          default: () => [_createVNode(_resolveComponent("ion-button"), {
            "onClick": () => this.openSearchForm(),
            "id": 'searchForm'
          }, _isSlot(_slot = this.$tl('widget.mobsearchform.filter', '过滤')) ? _slot : {
            default: () => [_slot]
          })]
        })]
      })]
    }), _createVNode(_resolveComponent("ion-menu"), {
      "side": 'end',
      "menuId": this.uuid,
      "contentId": 'searchForm'
    }, {
      default: () => [_createVNode(_resolveComponent("ion-content"), null, {
        default: () => [_createVNode("div", {
          "class": className,
          "style": controlStyle
        }, [this.renderFormContent()])]
      }), _createVNode(_resolveComponent("ion-footer"), {
        "class": 'searchForm-footer'
      }, {
        default: () => [_createVNode(_resolveComponent("ion-toolbar"), null, {
          default: () => [_createVNode(_resolveComponent("ion-button"), {
            "size": 'small',
            "onClick": () => this.onSearch()
          }, _isSlot(_slot2 = this.$tl('widget.mobsearchform.search', '搜索')) ? _slot2 : {
            default: () => [_slot2]
          }), _createVNode(_resolveComponent("ion-button"), {
            "size": 'small',
            "onClick": () => this.onReset()
          }, _isSlot(_slot3 = this.$tl('widget.mobsearchform.reset', '重置')) ? _slot3 : {
            default: () => [_slot3]
          })]
        })]
      })]
    })];
  }
  /**
   * @description 绘制快速搜索表单
   * @param {*} className 类名
   * @param {*} controlStyle 样式
   * @memberof AppMobSearchForm
   */


  renderQuickSearchForm(className, controlStyle) {
    return _createVNode(_resolveComponent("ion-header"), null, {
      default: () => [_createVNode("div", {
        "class": className,
        "style": controlStyle
      }, [this.renderFormContent()])]
    });
  }
  /**
   * @description 绘制搜索表单内容
   * @return {*}
   * @memberof AppMobSearchForm
   */


  renderSearchFormContent() {
    const controlStyle = {
      width: this.c.controlInstance.width ? `${this.c.controlInstance.width}px` : this.c.controlInstance.formWidth ? `${this.c.controlInstance.formWidth}` : '',
      height: this.c.controlInstance.height ? `${this.c.controlInstance.height}px` : ''
    };
    const className = Object.assign({}, this.classNames);

    if (Object.is(this.c.controlInstance.name, 'quicksearchform') || this.c.expandSearchForm) {
      return this.renderQuickSearchForm(className, controlStyle);
    } else {
      return this.renderSearchForm(className, controlStyle);
    }
  }
  /**
   * @description 绘制
   * @return {*}
   * @memberof AppMobSearchForm
   */


  render() {
    if (!this.controlIsLoaded.value) {
      return null;
    }

    return this.renderSearchFormContent();
  }

} // 搜索表单部件

export const AppMobSearchFormComponent = GenerateComponent(AppMobSearchForm, Object.keys(new AppMobSearchFormProps()));