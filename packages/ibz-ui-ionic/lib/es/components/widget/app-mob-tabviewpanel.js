import { createVNode as _createVNode } from "vue";
import { h, shallowReactive } from 'vue';
import { AppMobTabViewPanelProps } from 'ibz-core';
import { CtrlComponentBase } from './ctrl-component-base';
import { GenerateComponent } from '../component-base';
/**
 * 移动端分页视图面板部件
 *
 * @export
 * @class AppMobTabViewPanel
 * @extends CtrlComponentBase
 */

export class AppMobTabViewPanel extends CtrlComponentBase {
  constructor() {
    super(...arguments);
    /**
     * @description 嵌入视图组件
     * @type {string}
     * @memberof AppMobTabViewPanel
     */

    this.viewComponent = '';
  }
  /**
   * @description 设置响应式
   * @memberof AppMobTabViewPanel
   */


  setup() {
    this.c = shallowReactive(this.getCtrlControllerByType('TABVIEWPANEL'));
    super.setup();
  }
  /**
   * @description 初始化视图组件名称
   * @param {IPSAppDEView} embeddedView
   * @memberof AppMobTabViewPanel
   */


  getViewComponent(embeddedView) {
    var _a;

    this.viewComponent = App.getComponentService().getViewTypeComponent(embeddedView === null || embeddedView === void 0 ? void 0 : embeddedView.viewType, embeddedView === null || embeddedView === void 0 ? void 0 : embeddedView.viewStyle, (_a = embeddedView === null || embeddedView === void 0 ? void 0 : embeddedView.getPSSysPFPlugin()) === null || _a === void 0 ? void 0 : _a.pluginCode);
  }
  /**
   * @description 绘制关系视图
   * @public
   * @memberof AppMobTabViewPanel
   */


  renderEmbedView() {
    const embeddedView = this.c.controlInstance.getEmbeddedPSAppDEView();

    if (!embeddedView) {
      App.getNoticeService().warning(this.$tl('widget.mobtabviewpanel.noembeddedview', '嵌入视图不存在'));
      return null;
    }

    this.getViewComponent(embeddedView);
    const {
      context,
      viewParam
    } = this.c.getNavParams();
    return h(this.viewComponent, {
      viewPath: embeddedView === null || embeddedView === void 0 ? void 0 : embeddedView.modelPath,
      navContext: context,
      navParam: viewParam,
      viewState: this.c.viewState,
      viewShowMode: 'EMBEDDED',
      isShowCaptionBar: false
    });
  }
  /**
   * @description 分页视图面板部件渲染
   * @return {*}
   * @memberof AppMobTabViewPanel
   */


  render() {
    if (!this.controlIsLoaded.value) {
      return null;
    }

    return _createVNode("div", {
      "class": Object.assign({}, this.classNames)
    }, [this.renderEmbedView()]);
  }

}
export const AppMobTabViewPanelComponent = GenerateComponent(AppMobTabViewPanel, Object.keys(new AppMobTabViewPanelProps()));