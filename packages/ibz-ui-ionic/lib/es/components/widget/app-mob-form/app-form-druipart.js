import { createVNode as _createVNode } from "vue";
import { h } from 'vue';
import { Subject } from 'rxjs';
import { MobFormEvents, Util, ViewTool } from 'ibz-core';
import { ComponentBase, GenerateComponent } from '../../component-base';
export class AppFormDruipartProps {
  constructor() {
    /**
     * @description 表单数据
     * @type {string}
     * @memberof AppFormDruipartProps
     */
    this.data = '';
    /**
     * @description 名称
     * @type {string}
     * @memberof AppFormDruipartProps
     */

    this.name = '';
    /**
     * @description 关系界面控制器
     * @type {IParam}
     * @memberof AppFormDruipartProps
     */

    this.c = {};
    /**
     * @description 应用实体参数名称
     * @type {string}
     * @memberof AppFormDruipartProps
     */

    this.parameterName = '';
    /**
     * @description 表单状态
     * @type {(Subject<IViewStateParam> | null)}
     * @memberof AppFormDruipartProps
     */

    this.formState = null;
    /**
     * @description 应用上下文
     * @type {IParam}
     * @memberof AppFormDruipartProps
     */

    this.navContext = {};
    /**
     * @description 视图参数
     * @type {IParam}
     * @memberof AppFormDruipartProps
     */

    this.navParam = {};
    /**
     * @description 视图操作参数集合
     * @type {IParam}
     * @memberof AppFormDruipartProps
     */

    this.viewCtx = {};
    /**
     * @description 忽略表单值变更
     * @type {boolean}
     * @memberof AppFormDruipartProps
     */

    this.ignorefieldvaluechange = false;
    /**
     * 模型服务
     *
     * @type {IParam}
     * @memberof AppFormDruipartProps
     */

    this.modelService = {};
  }

} //  表单关系界面

export class AppFormDruipart extends ComponentBase {
  constructor() {
    super(...arguments);
    /**
     * @description 关系视图组件
     * @type {string}
     * @memberof AppFormDruipart
     */

    this.viewComponent = '';
    /**
     * @description 刷新节点
     * @public
     * @type {string[]}
     * @memberof AppFormDRUIPart
     */

    this.hookItems = [];
    /**
     * @description 关系界面向视图下发指令对象
     * @public
     * @type {Subject<IViewStateParam>}
     * @memberof AppFormDruipart
     */

    this.formState = new Subject();
    /**
     * @description 应用实体参数名称
     * @type {string}
     * @memberof AppFormDRUIPart
     */

    this.parameterName = '';
    /**
     * @description 部件name
     * @type {string}
     * @memberof AppFormDRUIPart
     */

    this.name = '';
    /**
     * @description 表单老数据
     * @public
     * @type {(Unsubscribable | undefined)}
     * @memberof AppFormDRUIPart
     */

    this.oldFormData = {};
    /**
     * @description 唯一标识
     * @type {[string]}
     * @memberof AppFormDRUIPart
     */

    this.uuid = Util.createUUID();
    /**
     * @description 是否启用遮罩
     * @type {boolean}
     * @memberof AppFormDRUIPart
     */

    this.blockUI = false;
    /**
     * @description 是否刷新关系数据
     * @public
     * @type {boolean}
     * @memberof AppFormDRUIPart
     */

    this.isRelationalData = true;
    /**
     * @description 存储应用上下文
     * @public
     * @type {IParam}
     * @memberof AppFormDRUIPart
     */

    this.embedViewContext = {};
    /**
     * @description 存储应用视图参数
     * @public
     * @type {IParam}
     * @memberof AppFormDRUIPart
     */

    this.embedViewParam = {};
  }
  /**
   * @description 初始化
   * @memberof AppFormDRUIPart
   */


  setup() {
    this.c = this.props.c;
  }
  /**
   * @description 挂载
   * @memberof AppFormDRUIPart
   */


  init() {
    this.c.initInputData(this.props);
    this.initFormDrUIPart();
    this.initViewComponent();
  }
  /**
   * @description 初始化关系界面
   * @memberof AppFormDRUIPart
   */


  initFormDrUIPart() {
    var _a;

    if (this.c.refreshitems) {
      this.hookItems = [...this.c.refreshitems.split(';')];
    }

    this.parameterName = this.props.parameterName;
    this.name = this.props.name;
    this.embedViewContext = this.props.navContext;
    this.embedViewParam = {};
    const formDRState = this.props.formState;
    const embedViewName = (_a = this.c.embeddedView) === null || _a === void 0 ? void 0 : _a.codeName;

    if (!Object.is(this.c.paramItem, this.parameterName.toLowerCase())) {
      this.hookItems.push(this.c.paramItem);
    }

    if (formDRState) {
      this.formStateEvent = formDRState.subscribe(({
        tag,
        action,
        data
      }) => {
        if (!Object.is(this.name, tag)) {
          return;
        } // 表单加载完成


        if (Object.is(action, MobFormEvents.LOAD_SUCCESS) || Object.is(action, MobFormEvents.LOAD_DRAFT_SUCCESS)) {
          this.refreshDRUIPart(data);
        } // 表单保存之前


        if (Object.is(action, MobFormEvents.BEFORE_SAVE)) {
          if (this.c.tempMode && this.c.tempMode == 2) {
            this.formState.next({
              tag: embedViewName,
              action: 'save',
              data: data
            });
          } else {
            if (data && !Object.is(data.srfuf, '0')) {
              this.formState.next({
                tag: embedViewName,
                action: 'save',
                data: data
              });
            } else {
              this.c.embedViewDataSaved();
            }
          }
        } // 表单保存完成


        if (Object.is(action, MobFormEvents.SAVE_SUCCESS)) {
          this.refreshDRUIPart(data);
        } // 表单项更新


        if (Object.is(action, 'updateFormItem')) {
          if (!data) return;
          let refreshRefview = false;
          Object.keys(data).some(name => {
            const index = this.hookItems.findIndex(_name => Object.is(_name, name));
            refreshRefview = index !== -1 ? true : false;
            return refreshRefview;
          });

          if (refreshRefview) {
            this.refreshDRUIPart(data);
          }
        }
      });
    }
  }
  /**
   * @description 监听数据变化
   * @memberof AppFormDRUIPart
   */


  watchEffect() {
    this.c.initInputData(this.props);

    if (this.hookItems.length > 0) {
      let refreshRefview = false;
      this.hookItems.forEach(item => {
        if (!Object.is(this.oldFormData[item], this.c.data[item])) {
          refreshRefview = true;
        }
      });
      this.oldFormData = Util.deepCopy(this.c.data);

      if (refreshRefview) {
        this.refreshDRUIPart(this.c.data);
      }
    }
  }
  /**
   * @description 初始化视图组件名称
   * @memberof AppFormDRUIPart
   */


  initViewComponent() {
    var _a, _b, _c, _d;

    this.viewComponent = App.getComponentService().getViewTypeComponent((_a = this.c.embeddedView) === null || _a === void 0 ? void 0 : _a.viewType, (_b = this.c.embeddedView) === null || _b === void 0 ? void 0 : _b.viewStyle, (_d = (_c = this.c.embeddedView) === null || _c === void 0 ? void 0 : _c.getPSSysPFPlugin()) === null || _d === void 0 ? void 0 : _d.pluginCode);
  }
  /**
   * @description 刷新关系页面
   * @public
   * @returns {void}
   * @memberof AppFormDRUIPart
   */


  refreshDRUIPart(formData) {
    var _a, _b;

    if (Object.is((_a = this.c.parentdata) === null || _a === void 0 ? void 0 : _a.SRFPARENTTYPE, 'CUSTOM')) {
      this.isRelationalData = false;
    }

    const _paramitem = formData[this.c.paramItem];
    const tempContext = {};
    const tempParam = {};
    const _parameters = [...ViewTool.getIndexParameters(), ...this.c.parameters];

    _parameters.forEach(parameter => {
      const {
        pathName,
        parameterName
      } = parameter;

      if (formData[parameterName] && !Object.is(formData[parameterName], '')) {
        Object.assign(tempContext, {
          [parameterName]: formData[parameterName]
        });
      }
    });

    Object.assign(tempContext, {
      [this.c.paramItem]: _paramitem
    }); // //设置顶层视图唯一标识

    Object.assign(tempContext, this.c.context);
    Object.assign(tempContext, {
      srfparentdename: this.parameterName,
      srfparentkey: _paramitem
    });
    Object.assign(tempParam, {
      srfparentdename: this.parameterName,
      srfparentkey: _paramitem
    }); // 设置局部上下文

    if (this.c.localContext && Object.keys(this.c.localContext).length > 0) {
      const _context = Util.computedNavData(formData, tempContext, this.c.viewParam, this.c.localContext);

      Object.assign(tempContext, _context);
    }

    this.embedViewContext = tempContext; // // 设置局部参数

    if (this.c.localParam && Object.keys(this.c.localParam).length > 0) {
      const _param = Util.computedNavData(formData, tempContext, this.c.viewParam, this.c.localParam);

      Object.assign(tempParam, _param);
    }

    if ((_b = this.c.viewParam) === null || _b === void 0 ? void 0 : _b.hasOwnProperty('copymode')) {
      Object.assign(tempParam, {
        copymode: this.props.c.copymode
      });
    }

    this.embedViewParam = tempParam;

    if (this.isRelationalData) {
      if (this.c.tempMode && Object.is(this.c.tempMode, 2)) {
        this.blockUIStop();
      } else {
        if (!_paramitem || _paramitem == null || Object.is(_paramitem, '')) {
          this.blockUIStart();
          return;
        } else {
          this.blockUIStop();
        }
      }
    } //根据key值刷新


    this.uuid = Util.createUUID(); // 重置状态对象

    this.formState = new Subject();
    this.forceUpdate();
  }
  /**
   * @description 开启遮罩
   * @public
   * @memberof AppFormDRUIPart
   */


  blockUIStart() {
    this.blockUI = true;
  }
  /**
   * @description 关闭遮罩
   * @public
   * @memberof AppFormDRUIPart
   */


  blockUIStop() {
    this.blockUI = false;
  }
  /**
   * @description 处理视图事件
   * @public
   * @memberof AppFormDRUIPart
   */


  handleViewEvent(viewname, action, data) {
    switch (action) {
      case 'save':
        this.c.embedViewDataSaved();
        break;
    }
  }
  /**
   * @description 组件销毁
   * @public
   * @memberof AppFormDRUIPart
   */


  unmounted() {
    if (this.formStateEvent) {
      this.formStateEvent.unsubscribe();
    }
  }
  /**
   * @description 绘制关系视图
   * @public
   * @memberof AppFormDRUIPart
   */


  renderDruipartView() {
    var _a, _b, _c;

    return h(this.viewComponent, {
      key: this.uuid,
      class: `form-druipart-view ${((_a = this.c) === null || _a === void 0 ? void 0 : _a.customClass) ? (_b = this.c) === null || _b === void 0 ? void 0 : _b.customClass : ''}`,
      style: this.c.customStyle,
      viewPath: (_c = this.c.embeddedView) === null || _c === void 0 ? void 0 : _c.modelPath,
      navContext: this.embedViewContext,
      navParam: this.embedViewParam,
      navDatas: [this.c.data],
      viewShowMode: 'EMBEDDED',
      viewState: this.formState,
      onViewEvent: ({
        viewname,
        action,
        data
      }) => {
        this.handleViewEvent(viewname, action, data);
      }
    });
  }
  /**
   * @description 绘制内容
   * @memberof AppFromItem
   */


  render() {
    return _createVNode("div", {
      "class": 'form-druipart'
    }, [this.blockUI && _createVNode("div", {
      "class": 'form-druipart-spin'
    }, [_createVNode("span", null, [this.$tl('widget.mobformdruipart.tooltip', '请先保存主数据')])]), this.renderDruipartView()]);
  }

} //  表单关系界面组件

export const AppFormDruipartComponent = GenerateComponent(AppFormDruipart, Object.keys(new AppFormDruipartProps()));