import { isVNode as _isVNode, resolveComponent as _resolveComponent, createTextVNode as _createTextVNode, createVNode as _createVNode } from "vue";
import { renderSlot } from 'vue';
import { Subject } from 'rxjs';
import { ComponentBase, GenerateComponent } from '../../component-base';
/**
 * 表单项输入参数
 *
 * @class AppFormItemProps
 */

function _isSlot(s) {
  return typeof s === 'function' || Object.prototype.toString.call(s) === '[object Object]' && !_isVNode(s);
}

class AppFormItemProps {
  constructor() {
    /**
     * 表单数据项控制器
     *
     * @type {FormButtonController}
     * @memberof AppFormItemProps
     */
    this.c = {};
    /**
     * @description 部件name
     * @type {string}
     * @memberof AppFormItemProps
     */

    this.name = '';
    /**
     * @description 表单数据
     * @type {IParam}
     * @memberof AppFormItemProps
     */

    this.data = {};
    /**
     * @description 表单状态
     * @type {(Subject<IViewStateParam> | null)}
     * @memberof AppFormItemProps
     */

    this.formState = null;
    /**
     * 模型服务
     *
     * @type {IParam}
     * @memberof AppFormItemProps
     */

    this.modelService = {};
  }

}
/**
 * 表单项
 *
 * @class AppFormItem
 */


export default class AppFormItem extends ComponentBase {
  constructor() {
    super(...arguments);
    /**
     * @description 部件name
     * @type {string}
     * @memberof AppFormItem
     */

    this.name = '';
    /**
     * @description 表单状态
     * @type {Subject<IViewStateParam>}
     * @memberof AppFormItem
     */

    this.formState = new Subject();
    /**
     * @description 表单旧数据
     * @type {IParam}
     * @memberof AppFormItem
     */

    this.oldFormData = {};
    /**
     * @description 表单数据
     * @type {IParam}
     * @memberof AppFormItem
     */

    this.data = {};
  }
  /**
   * @description 设置响应式
   * @memberof AppFormItem
   */


  setup() {
    this.c = this.props.c;
    this.name = this.props.name;
  }
  /**
   * @description 初始化
   * @memberof AppFormItem
   */


  init() {
    this.c.initInputData(this.props);
  }
  /**
   * @description 无标签
   * @memberof AppFormItem
   */


  renderNoneLabel() {
    const editor = this.c.model.getPSEditor();
    const editorStyle = {
      width: (editor === null || editor === void 0 ? void 0 : editor.editorWidth) ? `${editor.editorWidth}px` : '',
      height: (editor === null || editor === void 0 ? void 0 : editor.editorHeight) ? `${editor.editorHeight}px` : ''
    };
    return [_createVNode("div", {
      "class": `${this.name}-item-content`
    }, [_createVNode("div", {
      "class": `${this.name}-item-editor`,
      "style": editorStyle
    }, [renderSlot(this.ctx.slots, 'default')]), _createVNode("div", {
      "class": `${this.name}-item-error`
    }, [_createTextVNode(" "), this.c.error, _createTextVNode(" ")])])];
  }
  /**
   * @description 标签位于下方
   * @memberof AppFormItem
   */


  renderBottomLabel() {
    return this.renderLeftLabel();
  }
  /**
   * @description 标签位于上方
   * @memberof AppFormItem
   */


  renderTopLabel() {
    var _a, _b, _c;

    const {
      showCaption,
      labelWidth,
      emptyCaption,
      caption
    } = this.c.model;
    const labelClass = (_a = this.c.model.getLabelPSSysCss()) === null || _a === void 0 ? void 0 : _a.cssName;
    const editor = this.c.model.getPSEditor();
    const editorStyle = {
      width: (editor === null || editor === void 0 ? void 0 : editor.editorWidth) ? `${editor.editorWidth}px` : '',
      height: (editor === null || editor === void 0 ? void 0 : editor.editorHeight) ? `${editor.editorHeight}px` : ''
    };
    return [showCaption ? _createVNode(_resolveComponent("ion-label"), {
      "position": 'stacked',
      "style": {
        width: `${labelWidth}px`
      },
      "class": ['form-item-label', labelClass]
    }, {
      default: () => [this.c.required ? _createVNode("span", {
        "class": 'required'
      }, [_createTextVNode("* ")]) : null, emptyCaption ? '' : this.$tl((_c = (_b = this.c.model) === null || _b === void 0 ? void 0 : _b.getCapPSLanguageRes()) === null || _c === void 0 ? void 0 : _c.lanResTag, caption)]
    }) : null, _createVNode("div", {
      "class": `${this.name}-item-content`
    }, [_createVNode("div", {
      "class": `${this.name}-item-editor`,
      "style": editorStyle
    }, [renderSlot(this.ctx.slots, 'default')]), _createVNode("div", {
      "class": `${this.name}-item-error`
    }, [_createTextVNode(" "), this.c.error, _createTextVNode(" ")])])];
  }
  /**
   * @description 标签位于右侧
   * @memberof AppFormItem
   */


  renderRightLabel() {
    return this.renderLeftLabel();
  }
  /**
   * @description 标签位于左侧
   * @memberof AppFormItem
   */


  renderLeftLabel() {
    var _a, _b, _c;

    const {
      showCaption,
      labelWidth,
      emptyCaption,
      caption
    } = this.c.model;
    const labelClass = (_a = this.c.model.getLabelPSSysCss()) === null || _a === void 0 ? void 0 : _a.cssName;
    const editor = this.c.model.getPSEditor();
    const editorStyle = {
      width: (editor === null || editor === void 0 ? void 0 : editor.editorWidth) ? `${editor.editorWidth}px` : '',
      height: (editor === null || editor === void 0 ? void 0 : editor.editorHeight) ? `${editor.editorHeight}px` : ''
    };
    return [showCaption ? _createVNode(_resolveComponent("ion-label"), {
      "position": 'fixed',
      "style": {
        width: `${labelWidth}px`
      },
      "class": ['form-item-label', labelClass]
    }, {
      default: () => [this.c.required ? _createVNode("span", {
        "class": 'required'
      }, [_createTextVNode("* ")]) : null, emptyCaption ? '' : this.$tl((_c = (_b = this.c.model) === null || _b === void 0 ? void 0 : _b.getCapPSLanguageRes()) === null || _c === void 0 ? void 0 : _c.lanResTag, caption)]
    }) : null, _createVNode("div", {
      "class": `${this.name}-item-content`
    }, [_createVNode("div", {
      "class": `${this.name}-item-editor`,
      "style": editorStyle
    }, [renderSlot(this.ctx.slots, 'default')]), _createVNode("div", {
      "class": `${this.name}-item-error`
    }, [_createTextVNode(" "), this.c.error, _createTextVNode(" ")])])];
  }
  /**
   * @description 根据标签位置渲染
   * @memberof AppFormItem
   */


  renderContentByLabelPos() {
    const labelPos = this.c.model.labelPos;

    switch (labelPos) {
      case 'BOTTOM':
        return this.renderBottomLabel();

      case 'LEFT':
        return this.renderLeftLabel();

      case 'NONE':
        return this.renderNoneLabel();

      case 'RIGHT':
        return this.renderRightLabel();

      case 'TOP':
        return this.renderTopLabel();
    }
  }
  /**
   * @description 渲染表单项
   * @returns {JSX.Element | JSX.Element[] | null}
   * @memberof AppFormItem
   */


  render() {
    let _slot;

    return _createVNode(_resolveComponent("ion-item"), {
      "class": `${this.name}-item`
    }, _isSlot(_slot = this.renderContentByLabelPos()) ? _slot : {
      default: () => [_slot]
    });
  }

} //  表单项组件

export const AppFormItemComponent = GenerateComponent(AppFormItem, Object.keys(new AppFormItemProps()));