import { isVNode as _isVNode, createVNode as _createVNode, resolveComponent as _resolveComponent } from "vue";
import { renderSlot, ref } from 'vue';
import { Subject } from 'rxjs';
import { ComponentBase, GenerateComponent } from '../../component-base';

function _isSlot(s) {
  return typeof s === 'function' || Object.prototype.toString.call(s) === '[object Object]' && !_isVNode(s);
}

class AppFormGroupProps {
  constructor() {
    /**
     * @description 表单分组控制器实例对象
     * @type {IParam}
     * @memberof AppFormGroupProps
     */
    this.c = {};
    /**
     * @description 部件name
     * @type {string}
     * @memberof AppFormGroupProps
     */

    this.name = '';
    /**
     * @description 表单数据
     * @type {IParam}
     * @memberof AppFormGroupProps
     */

    this.data = {};
    /**
     * @description 表单状态
     * @type {(Subject<IViewStateParam> | null)}
     * @memberof AppFormGroupProps
     */

    this.formState = null;
    /**
     * 模型服务
     *
     * @type {IParam}
     * @memberof AppFormGroupProps
     */

    this.modelService = {};
  }

}

export class AppFormGroup extends ComponentBase {
  constructor() {
    super(...arguments);
    /**
     * @description 部件name
     * @type {string}
     * @memberof AppFormGroup
     */

    this.name = '';
    /**
     * @description 表单状态
     * @type {Subject<IViewStateParam>}
     * @memberof AppFormGroup
     */

    this.formState = new Subject();
    /**
     * @description 表单旧数据
     * @type {IParam}
     * @memberof AppFormGroup
     */

    this.oldFormData = {};
    /**
     * @description 表单数据
     * @type {IParam}
     * @memberof AppFormGroup
     */

    this.data = {};
    /**
     * @description 收缩内容
     * @type {Ref<boolean>}
     * @memberof AppFormGroup
     */

    this.collapseContent = ref(false);
    /**
     * @description 是否显示弹窗
     * @type {Ref<boolean>}
     * @memberof AppFormGroup
     */

    this.isShowPopover = ref(false);
    /**
     * @description 弹窗位置
     * @type {Ref<any>}
     * @memberof AppFormGroup
     */

    this.event = ref();
  }
  /**
   * @description 设置响应式
   * @memberof AppFormGroup
   */


  setup() {
    this.c = this.props.c;
    this.name = this.props.name;
  }
  /**
   * @description 初始化
   * @memberof AppFormGroup
   */


  init() {
    this.c.initInputData(this.props);
  }
  /**
   * @description 打开弹窗
   * @param state 状态
   * @memberof AppFormGroup
   */


  setOpenState(state, $event) {
    this.event.value = $event;
    this.isShowPopover.value = state;
  }
  /**
   * @description 点击展开
   * @memberof AppFormGroup
   */


  clickCollapse() {
    const titleBarCloseMode = Number(this.c.model.titleBarCloseMode) || 0;

    if (titleBarCloseMode === 0) {
      return;
    }

    this.collapseContent.value = !this.collapseContent.value;
  }
  /**
   * @description 显示更多按钮点击
   * @memberof AppFormGroup
   */


  onShowMoreButtonClick(event) {
    this.c.setManageContainerStatus(!this.c.manageContainerStatus);
  }
  /**
   * @description 执行分组行为
   * @param $event 事件源
   * @param item 数据
   * @memberof AppFormGroup
   */


  handleGroupUIActionClick($event, item) {
    this.c.onGroupUIActionClick($event, item);
  }
  /**
   * @description  选中
   * @param $event 事件源
   * @param item 数据
   */


  onSelect($event, item) {
    this.handleGroupUIActionClick($event, item);
    this.setOpenState(false);
  }
  /**
   * @description 绘制项展开
   * @param uiActionGroup 界面行为组
   * @memberof AppFormGroup
   */


  renderItemsExpend(uiActionGroup) {
    return uiActionGroup.details.map(item => {
      if (item.visabled) {
        return _createVNode("div", {
          "class": 'action-item'
        }, [_createVNode(_resolveComponent("ion-buttons"), {
          "onClick": $event => this.handleGroupUIActionClick($event, item),
          "disabled": item.disabled
        }, {
          default: () => [item.isShowIcon && item.icon && _createVNode(_resolveComponent("app-icon"), {
            "icon": item.icon
          }, null), item.isShowCaption && _createVNode("span", null, [item.caption])]
        })]);
      }
    });
  }
  /**
   * @description 绘制分组展开
   * @param uiActionGroup 界面行为组
   * @memberof AppFormGroup
   */


  renderGroupExpend(uiActionGroup) {
    let _slot;

    return _createVNode(_resolveComponent("ion-buttons"), {
      "onClick": $event => this.setOpenState(true, $event)
    }, {
      default: () => [uiActionGroup.caption, _createVNode(_resolveComponent("ion-popover"), {
        "cssClass": 'group-actions',
        "isOpen": this.isShowPopover.value,
        "event": this.event.value,
        "onDidDismiss": () => this.setOpenState(false)
      }, {
        default: () => [_createVNode(_resolveComponent("ion-list"), null, _isSlot(_slot = uiActionGroup.details.map(item => {
          if (item.visabled) {
            return _createVNode(_resolveComponent("ion-item"), {
              "button": true,
              "detail": false,
              "disabled": item.disabled,
              "onClick": $event => this.onSelect($event, item)
            }, {
              default: () => [item.isShowIcon && item.icon && _createVNode(_resolveComponent("app-icon"), {
                "icon": item.icon
              }, null), item.isShowCaption && _createVNode("span", null, [item.caption])]
            });
          }
        })) ? _slot : {
          default: () => [_slot]
        })]
      })]
    });
  }
  /**
   * @description 渲染表单分组界面行为组
   * @memberof AppFormGroup
   */


  renderUIActionGroup() {
    var _a;

    const uiActionGroup = this.c.uiActionGroup;

    if (((_a = uiActionGroup === null || uiActionGroup === void 0 ? void 0 : uiActionGroup.details) === null || _a === void 0 ? void 0 : _a.length) > 0) {
      return _createVNode("div", {
        "class": `${this.name}-group-action`
      }, [Object.is(uiActionGroup.extractMode, 'ITEM') ? this.renderItemsExpend(uiActionGroup) : this.renderGroupExpend(uiActionGroup)]);
    }
  }
  /**
   * @description 绘制底部
   * @memberof AppFormGroup
   */


  renderFooter() {
    if (this.c.isManageContainer) {
      return _createVNode(_resolveComponent("ion-button"), {
        "class": 'show-more',
        "onClick": event => {
          this.onShowMoreButtonClick(event);
        }
      }, {
        default: () => [this.c.manageContainerStatus ? this.$tl('widget.mobformgroup.hidden', '隐藏') : this.$tl('widget.mobformgroup.more', '显示更多')]
      });
    }
  }
  /**
   * @description 渲染表单分组内容区
   * @memberof AppFormGroup
   */


  renderContent() {
    return _createVNode(_resolveComponent("ion-row"), {
      "style": {
        display: this.collapseContent.value ? 'none' : 'block'
      },
      "class": `${this.name}-group-content`
    }, {
      default: () => [this.ctx.slots.default ? renderSlot(this.ctx.slots, 'default') : null]
    });
  }
  /**
   * @description 渲染表单分组标题
   * @memberof AppFormGroup
   */


  renderLabel() {
    var _a, _b, _c, _d, _e, _f, _g;

    const {
      showCaption,
      caption,
      captionItemName
    } = this.c.model;
    const cssName = (_c = (_b = (_a = this.c.model).getLabelPSSysCss) === null || _b === void 0 ? void 0 : _b.call(_a)) === null || _c === void 0 ? void 0 : _c.cssName;
    const icon = (_e = (_d = this.c.model).getPSSysImage) === null || _e === void 0 ? void 0 : _e.call(_d);

    if (showCaption) {
      return _createVNode(_resolveComponent("ion-item-divider"), {
        "class": cssName || ''
      }, {
        default: () => [_createVNode(_resolveComponent("ion-label"), {
          "onClick": () => this.clickCollapse()
        }, {
          default: () => [_createVNode("span", {
            "class": `${this.name}-group-title`
          }, [icon ? _createVNode(_resolveComponent("app-icon"), {
            "icon": icon
          }, null) : null, captionItemName ? this.c.data[captionItemName.toLowerCase()] : this.$tl((_g = (_f = this.c.model) === null || _f === void 0 ? void 0 : _f.getCapPSLanguageRes()) === null || _g === void 0 ? void 0 : _g.lanResTag, caption)])]
        }), this.renderUIActionGroup()]
      });
    }
  }
  /**
   * @description 渲染表单分组
   * @memberof AppFormGroup
   */


  render() {
    return _createVNode(_resolveComponent("ion-row"), {
      "class": ['form-group', `form-group-${this.c.name}`]
    }, {
      default: () => [this.renderLabel(), this.renderContent(), this.renderFooter()]
    });
  }

} //  表单分组组件

export const AppFormGroupComponent = GenerateComponent(AppFormGroup, Object.keys(new AppFormGroupProps()));