import { isVNode as _isVNode, mergeProps as _mergeProps, resolveComponent as _resolveComponent, createVNode as _createVNode } from "vue";
import { h, reactive, ref, shallowReactive } from 'vue';
import { LogUtil, Util, AppMobFormProps } from 'ibz-core';
import { GenerateComponent } from '../../component-base';
import { CtrlComponentBase } from '../ctrl-component-base';
import { AppAnchorComponent, AppFormItemComponent } from '../../../components';
import { AppFormButtonComponent, AppFormDruipartComponent, AppFormGroupComponent, AppFormPageComponent } from '../app-mob-form';
/**
 * 移动端表单
 *
 * @author chitanda
 * @date 2021-08-06 10:08:39
 * @export
 * @class AppMobForm
 * @extends {ComponentBase}
 */

function _isSlot(s) {
  return typeof s === 'function' || Object.prototype.toString.call(s) === '[object Object]' && !_isVNode(s);
}

export class AppMobForm extends CtrlComponentBase {
  constructor() {
    super(...arguments);
    /**
     * @description 当前页
     * @type {Ref<string>}
     * @memberof AppMobForm
     */

    this.currentPage = ref('');
  }
  /**
   * @description 设置响应式
   * @memberof AppMobForm
   */


  setup() {
    this.c = shallowReactive(this.getCtrlControllerByType('FORM'));
    super.setup();
  }
  /**
   * @description 初始化响应式属性
   * @memberof AppMobForm
   */


  initReactive() {
    super.initReactive();
    this.c.data = reactive(this.c.data);
    this.c.detailsModel = reactive(this.c.detailsModel);
    this.c.errorMessages = reactive(this.c.errorMessages);
  }
  /**
   * @description 表单分页点击事件
   * @returns
   * @memberof AppMobForm
   */


  segmentChanged(e) {
    if (e && e.detail && e.detail.value) {
      this.currentPage.value = e.detail.value;
      console.log(111, e);
    }
  }
  /**
   * @description 获取栅格布局
   * @param parent 容器
   * @param child 子成员
   * @memberof AppMobForm
   */


  getGridLayoutProps(parent, child) {
    var _a, _b, _c;

    const layout = (_b = (_a = parent === null || parent === void 0 ? void 0 : parent.getPSLayout) === null || _a === void 0 ? void 0 : _a.call(parent)) === null || _b === void 0 ? void 0 : _b.layout;
    let {
      colXS,
      colSM,
      colMD,
      colLG,
      colXSOffset,
      colSMOffset,
      colMDOffset,
      colLGOffset
    } = (_c = child === null || child === void 0 ? void 0 : child.getPSLayoutPos) === null || _c === void 0 ? void 0 : _c.call(child); // 设置初始值

    colXS = !colXS || colXS == -1 ? 12 : colXS / 2;
    colSM = !colSM || colSM == -1 ? 12 : colSM / 2;
    colMD = !colMD || colMD == -1 ? 12 : colMD / 2;
    colLG = !colLG || colLG == -1 ? 12 : colLG / 2;
    colXSOffset = !colXSOffset || colXSOffset == -1 ? 0 : colXSOffset / 2;
    colSMOffset = !colSMOffset || colSMOffset == -1 ? 0 : colSMOffset / 2;
    colMDOffset = !colMDOffset || colMDOffset == -1 ? 0 : colMDOffset / 2;
    colLGOffset = !colLGOffset || colLGOffset == -1 ? 0 : colLGOffset / 2;

    if (layout == 'TABLE_12COL') {
      // 重新计算12列的栅格数值
      colXS = Math.min(colXS * 2, 12);
      colSM = Math.min(colSM * 2, 12);
      colMD = Math.min(colMD * 2, 12);
      colLG = Math.min(colXS * 2, 12); // 重新计算12列的栅格偏移

      const sign = num => num == 0 ? 0 : num / Math.abs(num);

      colXSOffset = sign(colXSOffset) * Math.min(colXSOffset * 2, 12);
      colSMOffset = sign(colSMOffset) * Math.min(colSMOffset * 2, 12);
      colMDOffset = sign(colMDOffset) * Math.min(colMDOffset * 2, 12);
      colLGOffset = sign(colLGOffset) * Math.min(colLGOffset * 2, 12);
    }

    return {
      'size-lg': colLG,
      'size-md': colMD,
      'size-sm': colSM,
      'size-xs': colXS,
      'offset-lg': colLGOffset,
      'offset-md': colMDOffset,
      'offset-sm': colSMOffset,
      'offset-xs': colXSOffset
    };
  }
  /**
   * @description 获取栅格布局样式
   * @param item 表单成员
   * @memberof AppMobForm
   */


  getGridLayoutStyle(item) {
    var _a;

    const {
      name,
      width,
      height
    } = item;
    const style = {};

    if (!((_a = this.c.detailsModel[name]) === null || _a === void 0 ? void 0 : _a.visible)) {
      Object.assign(style, {
        display: 'none'
      });
    }

    if (width && width > 0) {
      Object.assign(style, {
        width: `${width}px`
      });
    }

    if (height && height > 0) {
      Object.assign(style, {
        height: `${height}px`
      });
    }

    return style;
  }
  /**
   * @description 获取flex布局样式
   * @param item 表单成员
   * @memberof AppMobForm
   */


  getFlexLayoutStyle(item) {
    var _a;

    const layoutPos = item.getPSLayoutPos();
    const style = {};

    if (!((_a = this.c.detailsModel[item.name]) === null || _a === void 0 ? void 0 : _a.visible)) {
      Object.assign(style, {
        display: 'none'
      });
    }

    if (layoutPos) {
      const {
        height,
        width
      } = layoutPos; //  宽高

      if (width) {
        Object.assign(style, {
          width: `${width}px`
        });
      }

      if (height) {
        Object.assign(style, {
          height: `${height}px`
        });
      }
    }

    return style;
  }
  /**
   * @description 渲染锚点
   * @return {*}
   * @memberof AppMobForm
   */


  renderAnchor() {
    var _a, _b, _c;

    const scrollContainerTag = `${Util.srfFilePath2((_c = (_b = (_a = this.c.controlInstance) === null || _a === void 0 ? void 0 : _a.getParentPSModelObject) === null || _b === void 0 ? void 0 : _b.call(_a)) === null || _c === void 0 ? void 0 : _c.codeName)} .view-body`;
    return h(AppAnchorComponent, {
      anchors: this.c.groupAnchors,
      scrollContainerTag: scrollContainerTag
    });
  }
  /**
   * @description 根据detailType绘制对应detail
   * @param {*} modelJson 模型数据
   * @param {number} index 下标
   * @memberof AppMobForm
   */


  renderByDetailType(modelJson, index) {
    var _a, _b;

    if ((_a = modelJson.getPSSysPFPlugin()) === null || _a === void 0 ? void 0 : _a.pluginCode) {
      const ctrlItemPluginInstance = App.getPluginService().getCtrlItemPluginByTag((_b = modelJson.getPSSysPFPlugin()) === null || _b === void 0 ? void 0 : _b.pluginCode);

      if (ctrlItemPluginInstance) {
        return ctrlItemPluginInstance.renderItem(this.c.data[modelJson.name], this.c, this);
      }
    } else {
      switch (modelJson.detailType) {
        case 'FORMPAGE':
          return this.renderFormPage(modelJson, index);

        case 'GROUPPANEL':
          return this.renderGroupPanel(modelJson, index);

        case 'FORMITEM':
          return this.renderFormItem(modelJson, index);

        case 'DRUIPART':
          return this.renderDruipart(modelJson, index);

        case 'BUTTON':
          return this.renderButton(modelJson, index);

        default:
          LogUtil.log(`暂未实现 ${modelJson.detailType} 类型表单成员`);
      }
    }
  }
  /**
   * @description 绘制子表单成员,布局控制
   * @param {*} modelJson 表单成员模型
   * @returns {*}
   * @memberof AppMobForm
   */


  renderDetails(modelJson) {
    const formDetails = modelJson.getPSDEFormDetails();
    const layout = modelJson.getPSLayout(); // 没有子表单成员

    if (!formDetails || formDetails.length == 0) {
      return null;
    } // flex布局


    if (layout && layout.layout == 'FLEX') {
      let cssStyle = 'width: 100%; height: 100%; overflow: auto; display: flex;';
      cssStyle += layout.dir ? `flex-direction: ${layout.dir};` : '';
      cssStyle += layout.align ? `justify-content: ${layout.align};` : '';
      cssStyle += layout.vAlign ? `align-items: ${layout.vAlign};` : '';
      return _createVNode("div", {
        "style": cssStyle
      }, [formDetails.map((item, index) => {
        const detailStyle = this.getFlexLayoutStyle(item);
        return _createVNode("div", {
          "style": detailStyle
        }, [this.renderByDetailType(item, index)]);
      })]);
    } else {
      let _slot2;

      // 栅格布局
      return _createVNode(_resolveComponent("ion-row"), {
        "style": 'height: 100%;'
      }, _isSlot(_slot2 = formDetails.map((item, index) => {
        if (!item.hidden) {
          let _slot;

          const attrs = this.getGridLayoutProps(modelJson, item);
          const detailStyle = this.getGridLayoutStyle(item);
          return _createVNode(_resolveComponent("ion-col"), _mergeProps({
            "class": `${this.c.controlInstance.name}-layout-container`,
            "style": detailStyle
          }, attrs), _isSlot(_slot = this.renderByDetailType(item, index)) ? _slot : {
            default: () => [_slot]
          });
        }
      })) ? _slot2 : {
        default: () => [_slot2]
      });
    }
  }
  /**
   * @description 绘制编辑器
   * @returns {*}
   * @memberof AppMobForm
   */


  renderEditor(editor, parentItem) {
    var _a, _b;

    const targetCtrlComponent = App.getComponentService().getEditorComponent(editor === null || editor === void 0 ? void 0 : editor.editorType, (editor === null || editor === void 0 ? void 0 : editor.editorStyle) ? editor === null || editor === void 0 ? void 0 : editor.editorStyle : 'DEFAULT', editor.getPSSysPFPlugin() ? `${editor === null || editor === void 0 ? void 0 : editor.editorType}_${editor === null || editor === void 0 ? void 0 : editor.editorStyle}` : undefined, (_a = this.c.detailsModel[editor.name]) === null || _a === void 0 ? void 0 : _a.infoMode);
    return h(targetCtrlComponent, {
      editorInstance: editor,
      containerCtrl: this.c.controlInstance,
      parentItem: parentItem,
      contextData: this.c.data,
      contextState: this.c.formState,
      ctrlService: this.c.ctrlService,
      navContext: Util.deepCopy(this.c.context),
      navParam: Util.deepCopy(this.c.viewParam),
      disabled: (_b = this.c.detailsModel[editor.name]) === null || _b === void 0 ? void 0 : _b.disabled,
      value: this.c.data[(editor === null || editor === void 0 ? void 0 : editor.name) || ''],
      modelService: this.c.modelService,
      onEditorEvent: event => {
        this.c.handleEditorEvent(event);
      }
    });
  }
  /**
   * @description 绘制表单项
   * @returns {*}
   * @memberof AppMobForm
   */


  renderFormItem(modelJson, index) {
    const editor = modelJson.getPSEditor();
    return h(AppFormItemComponent, {
      formState: this.c.formState,
      parameterName: this.c.appDeCodeName,
      navContext: Util.deepCopy(this.c.context),
      navParam: Util.deepCopy(this.c.viewParam),
      viewCtx: this.c.viewCtx,
      data: this.c.data,
      name: this.c.controlInstance.name,
      modelService: this.c.modelService,
      ignorefieldvaluechange: this.c.ignorefieldvaluechange,
      c: this.c.detailsModel[modelJson.name]
    }, () => {
      return editor ? this.renderEditor(editor, modelJson) : null;
    });
  }
  /**
   * @description 绘制关系界面
   * @returns {*}
   * @memberof AppFormBase
   */


  renderDruipart(modelJson, index) {
    const appDataEntity = this.c.controlInstance.getPSAppDataEntity();
    return h(AppFormDruipartComponent, {
      formState: this.c.formState,
      parameterName: appDataEntity === null || appDataEntity === void 0 ? void 0 : appDataEntity.codeName,
      navContext: Util.deepCopy(this.c.context),
      navParam: Util.deepCopy(this.c.viewParam),
      viewCtx: this.c.viewCtx,
      data: this.c.data,
      name: this.c.controlInstance.name,
      modelService: this.c.modelService,
      ignorefieldvaluechange: this.c.ignorefieldvaluechange,
      c: this.c.detailsModel[modelJson.name]
    });
  }
  /**
   * @description 绘制按钮
   * @returns {*}
   * @memberof AppMobForm
   */


  renderButton(modelJson, index) {
    return h(AppFormButtonComponent, {
      formState: this.c.formState,
      parameterName: this.c.appDeCodeName,
      navContext: Util.deepCopy(this.c.context),
      navParam: Util.deepCopy(this.c.viewParam),
      viewCtx: this.c.viewCtx,
      data: this.c.data,
      name: this.c.controlInstance.name,
      modelService: this.c.modelService,
      ignorefieldvaluechange: this.c.ignorefieldvaluechange,
      c: this.c.detailsModel[modelJson.name]
    });
  }
  /**
   * @description 绘制分组面板
   * @returns {*}
   * @memberof AppMobForm
   */


  renderGroupPanel(modelJson, index) {
    return h(AppFormGroupComponent, {
      formState: this.c.formState,
      parameterName: this.c.appDeCodeName,
      navContext: Util.deepCopy(this.c.context),
      navParam: Util.deepCopy(this.c.viewParam),
      viewCtx: this.c.viewCtx,
      data: this.c.data,
      name: this.c.controlInstance.name,
      modelService: this.c.modelService,
      ignorefieldvaluechange: this.c.ignorefieldvaluechange,
      c: this.c.detailsModel[modelJson.name]
    }, () => {
      return this.renderDetails(modelJson);
    });
  }
  /**
   * @description 绘制表单分页
   * @returns {*}
   * @memberof AppMobForm
   */


  renderFormPage(modelJson, index) {
    return h(AppFormPageComponent, {
      formState: this.c.formState,
      parameterName: this.c.appDeCodeName,
      navContext: Util.deepCopy(this.c.context),
      navParam: Util.deepCopy(this.c.viewParam),
      viewCtx: this.c.viewCtx,
      data: this.c.data,
      name: this.c.controlInstance.name,
      currentPage: this.currentPage.value,
      ignorefieldvaluechange: this.c.ignorefieldvaluechange,
      c: this.c.detailsModel[modelJson.name]
    }, () => {
      return this.renderDetails(modelJson);
    });
  }
  /**
   * @description 绘制表单内容
   * @returns {*}
   * @memberof AppMobForm
   */


  renderFormContent() {
    const {
      noTabHeader
    } = this.c.controlInstance;
    const formPages = this.c.controlInstance.getPSDEFormPages();

    if (!this.currentPage.value && formPages) {
      this.currentPage.value = formPages[0].codeName;
    }

    if (formPages && formPages.length > 0) {
      if (noTabHeader) {
        return formPages.map((item, index) => {
          return this.renderFormPage(item, index);
        });
      } else {
        let _slot3;

        const controlName = this.c.controlInstance.name;
        return _createVNode("div", {
          "class": `${controlName}-container`
        }, [_createVNode("div", {
          "class": `${controlName}-header`
        }, [_createVNode(_resolveComponent("ion-segment"), {
          "value": this.currentPage.value,
          "onIonChange": $event => this.segmentChanged($event)
        }, _isSlot(_slot3 = formPages.map((item, index) => {
          var _a, _b; // 表单分页标题样式


          const pageClass = (_a = item.getLabelPSSysCss()) === null || _a === void 0 ? void 0 : _a.cssName;
          return _createVNode(_resolveComponent("ion-segment-button"), {
            "value": item.codeName,
            "class": pageClass
          }, {
            default: () => [_createVNode(_resolveComponent("ion-label"), null, {
              default: () => [item.getPSSysImage() && _createVNode(_resolveComponent("app-icon"), {
                "icon": item.getPSSysImage()
              }, null), this.$tl((_b = item === null || item === void 0 ? void 0 : item.getCapPSLanguageRes()) === null || _b === void 0 ? void 0 : _b.lanResTag, item.caption)]
            })]
          });
        })) ? _slot3 : {
          default: () => [_slot3]
        })]), _createVNode("div", {
          "class": `${controlName}-body`
        }, [formPages.map((item, index) => {
          return this.renderFormPage(item, index);
        })])]);
      }
    }
  }
  /**
   * @description 部件样式名
   * @readonly
   * @type {any}
   * @memberof AppMobForm
   */


  get classNames() {
    var _a, _b;

    const {
      name,
      codeName
    } = this.c.controlInstance;
    const classNames = {
      'container-margin': true,
      'container-padding': true,
      'app-ctrl': true,
      [name]: true,
      [Util.srfFilePath2(codeName)]: true
    };
    const sysCss = (_b = (_a = this.c.controlInstance).getPSSysCss) === null || _b === void 0 ? void 0 : _b.call(_a);

    if (sysCss) {
      Object.assign(classNames, {
        [sysCss.cssName]: true
      });
    }

    return classNames;
  }
  /**
   * @description 绘制移动端表单
   * @returns {JSX.Element | JSX.Element[] | null}
   * @memberof AppMobForm
   */


  render() {
    if (!this.controlIsLoaded.value) {
      return null;
    }

    const controlStyle = {
      width: this.c.controlInstance.width ? `${this.c.controlInstance.width}px` : this.c.controlInstance.formWidth ? `${this.c.controlInstance.formWidth}` : '',
      height: this.c.controlInstance.height ? `${this.c.controlInstance.height}px` : ''
    };
    return _createVNode("div", {
      "class": Object.assign({}, this.classNames),
      "style": controlStyle
    }, [this.c.groupAnchors.length ? this.renderAnchor() : null, this.renderPullDownRefresh(), this.renderFormContent()]);
  }

} // 移动端表单部件

export const AppMobFormComponent = GenerateComponent(AppMobForm, Object.keys(new AppMobFormProps()));