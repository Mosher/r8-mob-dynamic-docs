import { createVNode as _createVNode, resolveComponent as _resolveComponent } from "vue";
import { shallowReactive } from 'vue';
import { AppMobDashBoardProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { CtrlComponentBase } from './ctrl-component-base';
/**
 * 移动端数据看板部件
 *
 * @class AppMobDashboard
 */

class AppMobDashboard extends CtrlComponentBase {
  /**
   * @description 设置响应式
   * @public
   * @memberof AppMobDashboard
   */
  setup() {
    this.c = shallowReactive(this.getCtrlControllerByType('DASHBOARD'));
    super.setup();
  }
  /**
   * @description 处理自定义面板
   * @memberof AppMobDashboard
   */


  handleCustom(event) {
    if (this.c.handleCustom && this.c.handleCustom instanceof Function) {
      const view = {
        viewComponentName: 'app-dashboard-design'
      };
      this.c.handleCustom(view, event).then(ret => {});
    }
  }
  /**
   * @description 渲染门户部件
   * @memberof AppMobDashboard
   */


  renderPortlet(item, isCustom = false) {
    return this.computeTargetCtrlData(item, isCustom ? {
      isAdaptiveSize: true,
      class: 'dashboard-item user-customize'
    } : undefined);
  }
  /**
   * @description 渲染自定义数据看板部件
   * @memberof AppMobDashboard
   */


  renderCustomizedDashboard() {
    if (this.c.customDashboardModelData.length > 0) {
      return this.c.customDashboardModelData.map(item => {
        return this.renderPortlet(item.modelData, true);
      });
    }
  }
  /**
   * @description 渲染静态数据看板内容
   * @memberof AppMobDashboard
   */


  renderStaticDashboard() {
    const controls = this.c.controlInstance.getPSControls() || [];
    return controls.map((control, index) => {
      return this.renderPortlet(control);
    });
  }
  /**
   * @description 渲染
   * @memberof AppMobDashboard
   */


  render() {
    if (!this.controlIsLoaded) {
      return;
    }

    return _createVNode(_resolveComponent("ion-grid"), {
      "class": Object.assign({}, this.classNames)
    }, {
      default: () => [this.c.enableCustomized ? _createVNode("div", {
        "class": 'dashboard-customized',
        "onClick": event => {
          this.handleCustom(event);
        }
      }, [this.$tl('widget.mobdashboard.title', '定制仪表盘'), _createVNode(_resolveComponent("app-icon"), {
        "name": 'settings-outline'
      }, null)]) : null, this.c.hasCustomized ? this.renderCustomizedDashboard() : this.renderStaticDashboard()]
    });
  }

} //  移动端数据看板部件组件


export const AppMobDashboardComponent = GenerateComponent(AppMobDashboard, Object.keys(new AppMobDashBoardProps()));