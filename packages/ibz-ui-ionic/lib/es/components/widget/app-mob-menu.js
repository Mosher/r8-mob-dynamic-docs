import { withDirectives as _withDirectives, resolveDirective as _resolveDirective, isVNode as _isVNode, resolveComponent as _resolveComponent, createVNode as _createVNode, createTextVNode as _createTextVNode } from "vue";
import { shallowReactive } from 'vue';
import { AppMobMenuProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { CtrlComponentBase } from './ctrl-component-base';
/**
 * 应用菜单
 *
 * @export
 * @class AppMobMenu
 * @extends {ComponentBase}
 */

function _isSlot(s) {
  return typeof s === 'function' || Object.prototype.toString.call(s) === '[object Object]' && !_isVNode(s);
}

export class AppMobMenu extends CtrlComponentBase {
  /**
   * @description 设置响应式
   * @memberof AppMobMenu
   */
  setup() {
    this.c = shallowReactive(this.getCtrlControllerByType('APPMENU'));
    super.setup();
  }
  /**
   * @description 部件初始化
   * @memberof AppMobMenu
   */


  init() {
    super.init();
    this.menuItems = this.c.controlInstance.getPSAppMenuItems() || [];
    this.setDefaultSelectMenu();
  }
  /**
   * @description 设置默认选中菜单
   * @return {*}
   * @memberof AppMobMenu
   */


  setDefaultSelectMenu() {
    if (Object.is(this.c.ctrlShowMode, 'QUICKMENU')) {
      return null;
    }

    let defaultSelectMenu = null;
    this.menuItems.map(menu => {
      if (menu.openDefault) {
        defaultSelectMenu = menu;
      }
    });

    if (!defaultSelectMenu) {
      defaultSelectMenu = this.menuItems[0];
    }

    if (defaultSelectMenu) {
      this.menuItemClick(defaultSelectMenu, true);
    }
  }
  /**
   * @description 获取计数器计数
   * @param {IPSAppMenuItem} item
   * @return {*}
   * @memberof AppMobMenu
   */


  getCounter(item) {
    var _a;

    const counterService = this.c.getCounterService();

    if (item.counterId && counterService) {
      return (_a = counterService === null || counterService === void 0 ? void 0 : counterService.counterData) === null || _a === void 0 ? void 0 : _a[item.counterId.toLowerCase()];
    } else {
      return null;
    }
  }
  /**
   * @description 根据菜单项获取菜单权限
   * @param {IPSAppMenuItem} menuItem
   * @return {*}
   * @memberof AppMobMenu
   */


  getMenusPermission(menuItem) {
    if (!App.isPreviewMode() && menuItem.accessKey) {
      return this.c.appUIService.getResourceOPPrivs(menuItem.accessKey);
    } else {
      return true;
    }
  }
  /**
   * @description 菜单项点击
   * @param {IPSAppMenuItem} item
   * @memberof AppMobMenu
   */


  async menuItemClick(item, isGlobal) {
    const func = item.getPSAppFunc();

    if (App.isPreviewMode()) {
      return;
    }

    if (func && func.id) {
      const path = await App.getFuncService().executeAppFunction(func.id, this.c.context, this.c.viewParam, this, isGlobal);

      if (path) {
        App.getOpenViewService().openView(path);
      }
    }
  }
  /**
   * @description 绘制菜单项
   * @param {IPSAppMenuItem} item
   * @return {*}
   * @memberof AppMobMenu
   */


  renderMenuItem(item) {
    let _slot;

    var _a, _b; // if (item.hidden || !this.getMenusPermission(item)) {
    //   return null;
    // }


    return _createVNode("div", null, [_createTextVNode("\u83DC\u5355\u9879")]);
    const cssName = ((_a = item.getPSSysCss()) === null || _a === void 0 ? void 0 : _a.cssName) || '';
    const icon = item.getPSSysImage();
    return _withDirectives(_createVNode(_resolveComponent("ion-tab-button"), {
      "onClick": () => this.menuItemClick(item, true),
      "tab": item.name,
      "class": cssName
    }, {
      default: () => [_createVNode(_resolveComponent("ion-label"), null, _isSlot(_slot = this.$tl((_b = item.getCapPSLanguageRes()) === null || _b === void 0 ? void 0 : _b.lanResTag, item.caption)) ? _slot : {
        default: () => [_slot]
      }), icon ? _createVNode(_resolveComponent("app-icon"), {
        "icon": icon
      }, null) : _createVNode(_resolveComponent("app-icon"), {
        "name": 'home'
      }, null)]
    }), [[_resolveDirective("badge"), {
      count: this.getCounter(item),
      offset: [15, 10]
    }]]);
  }
  /**
   * @description 绘制快速菜单项
   * @return {*}
   * @memberof AppMobMenu
   */


  renderQuickMenu() {
    var _a;

    switch ((_a = this.c.controlInstance.controlStyle) === null || _a === void 0 ? void 0 : _a.toLocaleLowerCase()) {
      case 'listview':
        return _createVNode(_resolveComponent("app-menu-list"), {
          "menu": this.menuItems,
          "appUIService": this.c.appUIService,
          "modelService": this.c.modelService,
          "onMenuClick": item => this.menuItemClick(item, false)
        }, null);

      default:
        return _createVNode(_resolveComponent("app-menu-icon"), {
          "menu": this.menuItems,
          "appUIService": this.c.appUIService,
          "modelService": this.c.modelService,
          "onMenuClick": item => this.menuItemClick(item, false)
        }, null);
    }
  }
  /**
   * @description 绘制内容
   * @return {*}
   * @memberof AppMobMenu
   */


  render() {
    if (!this.controlIsLoaded.value) {
      return null;
    }

    console.log(222, this.c.ctrlShowMode);

    if (Object.is(this.c.ctrlShowMode, 'QUICKMENU')) {// return <div class={{ ...this.classNames }}>{this.renderQuickMenu()}</div>;
    } else {
      return _createVNode(_resolveComponent("ion-tabs"), {
        "class": Object.assign({}, this.classNames)
      }, {
        default: () => [_createVNode("div", null, [_createTextVNode("123")])]
      });
    }
  }

} // 应用菜单组件

export const AppMobMenuComponent = GenerateComponent(AppMobMenu, Object.keys(new AppMobMenuProps()));