import { createVNode as _createVNode, isVNode as _isVNode, resolveComponent as _resolveComponent } from "vue";
import { shallowReactive, ref } from 'vue';
import { Util, ViewTool } from 'ibz-core';
import { AppMobMDCtrlProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { MDCtrlComponentBase } from './md-ctrl-component-base';

function _isSlot(s) {
  return typeof s === 'function' || Object.prototype.toString.call(s) === '[object Object]' && !_isVNode(s);
}

export class AppMobMDCtrl extends MDCtrlComponentBase {
  constructor() {
    super(...arguments);
    /**
     * @description 加载更多禁用状态
     * @type {boolean}
     * @memberof AppMobMDCtrl
     */

    this.loadMoreDisabled = ref(false);
  }
  /**
   * @description 设置响应式
   * @memberof AppMobMDCtrl
   */


  setup() {
    this.c = shallowReactive(this.getCtrlControllerByType('MOBMDCTRL'));
    super.setup();
  }
  /**
   * @description 初始化响应式属性
   * @memberof AppMobMDCtrl
   */


  initReactive() {
    super.initReactive();
  }
  /**
   * @description 初始化数据ref引用
   * @param {any[]} items 数据
   * @memberof AppMobMDCtrl
   */


  initItemRef(items) {
    items.forEach(item => {
      this.refsMap.set(item.srfkey, ref(null));
    });
  }
  /**
   * @description 拖动
   * @memberof AppMobMDCtrl
   */


  ionDrag() {// this.$store.commit('setPopupStatus', false)
  }
  /**
   * @description 加载更多
   * @param {*} event 事件源
   * @memberof AppMobMDCtrl
   */


  loadMore(event) {
    if (this.c.needLoadMore) {
      if (this.c.loadBottom && this.c.loadBottom instanceof Function) {
        this.c.loadBottom().then(response => {
          event.target.complete();
        }).catch(error => {
          event.target.complete();
        });
      }
    } else {
      this.loadMoreDisabled.value = true;
    }
  }
  /**
   * @description 单选值改变
   * @param {*} $event 数据源
   * @return {*}
   * @memberof AppMobMDCtrl
   */


  simpleChange($event) {
    const {
      detail
    } = $event;

    if (!detail) {
      return;
    }

    const {
      value
    } = detail;
    const selections = [];
    const selectItem = this.c.items.find(item => {
      return Object.is(item.srfkey, value);
    });
    selections.push(selectItem);
    this.c.selectionChange(selections);
  }
  /**
   * @description 多选值改变
   * @param {*} $event 数据源
   * @return {*}
   * @memberof AppMobMDCtrl
   */


  multipleChange($event) {
    const {
      detail
    } = $event;

    if (!detail) {
      return;
    }

    const {
      value
    } = detail;
    const selections = [...this.c.selections];
    const selectItem = this.c.items.find(item => {
      return Object.is(item.srfkey, value);
    });

    if (selectItem) {
      if (detail.checked) {
        selections.push(selectItem);
      } else {
        const index = selections.findIndex(i => {
          return i.srfkey === selectItem.srfkey;
        });

        if (index != -1) {
          selections.splice(index, 1);
        }
      }
    }

    this.c.selectionChange(selections);
  }
  /**
   * @description 列表项左滑右滑触发行为
   * @param {*} $event 点击鼠标事件源
   * @param {*} detail 界面行为模型对象
   * @param {*} item 项数据
   * @memberof AppMobMDCtrl
   */


  mdCtrlClick($event, detail, item) {
    $event.stopPropagation();

    if (this.c.handleActionClick && this.c.handleActionClick instanceof Function) {
      this.c.handleActionClick(item, $event, detail);
    }

    this.closeSlidings(item);
  }
  /**
   * @description 关闭列表项左滑右滑
   * @param {*} item 项数据
   * @memberof AppMobMDCtrl
   */


  closeSlidings(item) {
    var _a;

    const element = (_a = this.refsMap.get(item.srfkey)) === null || _a === void 0 ? void 0 : _a.value;

    if (element && element.closeOpened && element.closeOpened instanceof Function) {
      element.closeOpened();
    }
  }
  /**
   * @description 渲染子项（布局面板）
   * @param {*} item 项数据
   * @return {*}
   * @memberof AppMobMDCtrl
   */


  renderItemForLayoutPanel(item) {
    let _slot;

    const _context = Util.deepCopy(this.c.context);

    Object.assign(_context, {
      [this.c.appDeCodeName.toLowerCase()]: item[this.c.appDeCodeName.toLowerCase()]
    });
    const otherParams = {
      navDatas: [item],
      navContext: _context,
      dataMap: this.c.dataMap
    };
    return _createVNode("div", {
      "style": 'width:100%;'
    }, [_createVNode(_resolveComponent("ion-item"), {
      "class": 'item-layout'
    }, _isSlot(_slot = this.computeTargetCtrlData(this.c.controlInstance.getItemPSLayoutPanel(), otherParams)) ? _slot : {
      default: () => [_slot]
    })]);
  }
  /**
   * @description 渲染子项（默认内容）
   * @param {*} item 项数据
   * @param {*} index 索引
   * @return {*}
   * @memberof AppMobMDCtrl
   */


  renderItemForDefault(item, index) {
    var _a;

    let valueFormat = '';
    const srfmajortext = (_a = this.c.controlInstance.getPSDEListDataItems()) === null || _a === void 0 ? void 0 : _a.find(dataItem => Object.is('srfmajortext', dataItem.name));

    if (srfmajortext) {
      valueFormat = srfmajortext.format;
    }

    if (this.c.controlInstance.controlStyle == 'ICONVIEW') {
      return _createVNode(_resolveComponent("app-icon-item"), {
        "item": item,
        "index": index,
        "valueFormat": valueFormat,
        "onClick": () => this.c.onRowSelect(item)
      }, null);
    } else {
      return _createVNode(_resolveComponent("app-list-item"), {
        "item": item,
        "index": index,
        "valueFormat": valueFormat,
        "onClick": () => this.c.onRowSelect(item)
      }, null);
    }
  }
  /**
   * @description 渲染多数据部件项
   * @param {*} item 项数据
   * @param {number} index 索引
   * @return {*}
   * @memberof AppMobMDCtrl
   */


  renderMDCtrlItem(item, index) {
    if (this.c.controlInstance.controlStyle == 'ICONVIEW') {
      return _createVNode("div", {
        "ref": this.refsMap.get(item.srfkey),
        "class": 'icon-item'
      }, [this.c.controlInstance.getItemPSLayoutPanel() ? this.renderItemForLayoutPanel(item) : this.renderItemForDefault(item, index)]);
    } else {
      return _createVNode(_resolveComponent("ion-item-sliding"), {
        "ref": this.refsMap.get(item.srfkey),
        "class": 'list-item',
        "onIonDrag": this.ionDrag.bind(this)
      }, {
        default: () => [this.renderListItemAction(item), this.c.controlInstance.getItemPSLayoutPanel() ? this.renderItemForLayoutPanel(item) : this.renderItemForDefault(item, index)]
      });
    }
  }
  /**
   * @description 渲染分组
   * @return {*}
   * @memberof AppMobMDCtrl
   */


  renderHaveGroup() {
    if (this.c.controlInstance.controlStyle == 'ICONVIEW') {
      return _createVNode("div", {
        "class": 'iconview-group'
      }, [this.c.groupData.map(data => {
        return _createVNode("div", {
          "class": 'iconview-group-item'
        }, [_createVNode("div", {
          "class": 'group-title'
        }, [data.text]), _createVNode("div", {
          "class": 'group-content'
        }, [data.items && data.items.length > 0 && data.items.map((item, index) => {
          return this.renderMDCtrlItem(item, index);
        })])]);
      })]);
    } else {
      return this.c.groupData.map(data => {
        return _createVNode("div", {
          "class": 'listview-group'
        }, [_createVNode(_resolveComponent("ion-item-group"), null, {
          default: () => [_createVNode(_resolveComponent("ion-item-divider"), null, {
            default: () => [_createVNode(_resolveComponent("ion-label"), null, {
              default: () => [data.text]
            })]
          }), data.items && data.items.length > 0 && data.items.map((item, index) => {
            return this.renderMDCtrlItem(item, index);
          })]
        })]);
      });
    }
  }
  /**
   * @description 渲染无分组
   * @return {*}
   * @memberof AppMobMDCtrl
   */


  renderNoGroup() {
    if (this.c.controlInstance.controlStyle == 'ICONVIEW') {
      return _createVNode("div", {
        "class": 'iconview-nogroup'
      }, [this.c.items.map((item, index) => {
        return this.renderMDCtrlItem(item, index);
      })]);
    } else {
      return this.c.items.map((item, index) => {
        return this.renderMDCtrlItem(item, index);
      });
    }
  }
  /**
   * @description 绘制界面行为组
   * @param {*} item 项数据
   * @return {*}
   * @memberof AppMobMDCtrl
   */


  renderListItemAction(item) {
    var _a, _b, _c, _d;

    return [((_b = (_a = this.c.controlInstance) === null || _a === void 0 ? void 0 : _a.getPSDEUIActionGroup) === null || _b === void 0 ? void 0 : _b.call(_a)) ? this.renderActionGroup(item) : null, ((_d = (_c = this.c.controlInstance) === null || _c === void 0 ? void 0 : _c.getPSDEUIActionGroup2) === null || _d === void 0 ? void 0 : _d.call(_c)) ? this.renderActionGroup2(item) : null];
  }
  /**
   * @description 绘制左滑界面行为组
   * @param {*} item 项数据
   * @return {*}
   * @memberof AppMobMDCtrl
   */


  renderActionGroup(item) {
    var _a, _b, _c; //TODO 待补充国际化


    const details = (_c = (_b = (_a = this.c.controlInstance) === null || _a === void 0 ? void 0 : _a.getPSDEUIActionGroup()) === null || _b === void 0 ? void 0 : _b.getPSUIActionGroupDetails) === null || _c === void 0 ? void 0 : _c.call(_b);

    if (this.c.controlInstance.controlStyle != 'LISTVIEW3') {
      return _createVNode(_resolveComponent("ion-item-options"), {
        "side": 'start'
      }, {
        default: () => [details && (details === null || details === void 0 ? void 0 : details.map(detail => {
          var _a, _b, _c, _d, _e, _f;

          const uiaction = detail.getPSUIAction();

          if (uiaction && ((_a = item[uiaction.codeName]) === null || _a === void 0 ? void 0 : _a.visabled)) {
            const iconName = item[uiaction.codeName].icon ? ViewTool.setIcon((_b = item[uiaction.codeName]) === null || _b === void 0 ? void 0 : _b.icon) : '';
            return _createVNode(_resolveComponent("ion-item-option"), {
              "disabled": (_c = item[uiaction.codeName]) === null || _c === void 0 ? void 0 : _c.disabled,
              "color": uiaction.uIActionTag == 'Remove' ? 'danger' : 'primary',
              "onClick": $event => this.mdCtrlClick($event, detail, item)
            }, {
              default: () => [((_d = item[uiaction.codeName]) === null || _d === void 0 ? void 0 : _d.icon) && ((_e = item[uiaction.codeName]) === null || _e === void 0 ? void 0 : _e.isShowIcon) && _createVNode(_resolveComponent("app-icon"), {
                "name": iconName
              }, null), ((_f = item[uiaction.codeName]) === null || _f === void 0 ? void 0 : _f.isShowCaption) && _createVNode(_resolveComponent("ion-label"), null, {
                default: () => [uiaction.caption]
              })]
            });
          }
        }))]
      });
    }
  }
  /**
   * @description 绘制右滑界面行为组
   * @param {*} item 项数据
   * @return {*}
   * @memberof AppMobMDCtrl
   */


  renderActionGroup2(item) {
    var _a, _b, _c; //TODO 待补充国际化


    const details = (_c = (_b = (_a = this.c.controlInstance) === null || _a === void 0 ? void 0 : _a.getPSDEUIActionGroup2()) === null || _b === void 0 ? void 0 : _b.getPSUIActionGroupDetails) === null || _c === void 0 ? void 0 : _c.call(_b);

    if (this.c.controlInstance.controlStyle != 'LISTVIEW3') {
      return _createVNode(_resolveComponent("ion-item-options"), {
        "side": 'end'
      }, {
        default: () => [details === null || details === void 0 ? void 0 : details.map(detail => {
          var _a, _b, _c, _d, _e, _f;

          const uiaction = detail.getPSUIAction();

          if (uiaction && ((_a = item[uiaction.codeName]) === null || _a === void 0 ? void 0 : _a.visabled)) {
            const iconName = item[uiaction.codeName].icon ? ViewTool.setIcon((_b = item[uiaction.codeName]) === null || _b === void 0 ? void 0 : _b.icon) : '';
            return _createVNode(_resolveComponent("ion-item-option"), {
              "disabled": (_c = item[uiaction.codeName]) === null || _c === void 0 ? void 0 : _c.disabled,
              "color": uiaction.uIActionTag == 'Remove' ? 'danger' : 'primary',
              "onClick": $event => this.mdCtrlClick($event, detail, item)
            }, {
              default: () => [((_d = item[uiaction.codeName]) === null || _d === void 0 ? void 0 : _d.icon) && ((_e = item[uiaction.codeName]) === null || _e === void 0 ? void 0 : _e.isShowIcon) && _createVNode(_resolveComponent("app-icon"), {
                "name": iconName
              }, null), ((_f = item[uiaction.codeName]) === null || _f === void 0 ? void 0 : _f.isShowCaption) && _createVNode(_resolveComponent("ion-label"), null, {
                default: () => [uiaction.caption]
              })]
            });
          }
        })]
      });
    }
  }
  /**
   * @description 渲染列表视图样式
   * @return {*}
   * @memberof AppMobMDCtrl
   */


  renderMdctrlItemsListView() {
    return _createVNode(_resolveComponent("ion-list"), {
      "class": 'mdctrl-listview'
    }, {
      default: () => [Object.is('SELECT', this.c.ctrlShowMode) ? this.renderSelectMDCtrl() : this.c.isEnableGroup ? this.renderHaveGroup() : this.renderNoGroup()]
    });
  }
  /**
   * @description 渲染图标视图样式
   * @return {*}
   * @memberof AppMobMDCtrl
   */


  renderMdctrlItemsIconView() {
    return _createVNode("div", {
      "class": 'mdctrl-iconview'
    }, [this.c.isEnableGroup ? this.renderHaveGroup() : this.renderNoGroup()]);
  }
  /**
   * @description 渲染多数据部件主体内容
   * @return {*}
   * @memberof AppMobMDCtrl
   */


  renderMDCtrlContent() {
    if (this.c.items && this.c.items.length > 0) {
      this.initItemRef(this.c.items);

      if (this.c.controlInstance.controlStyle == 'ICONVIEW') {
        return this.renderMdctrlItemsIconView();
      } else {
        return this.renderMdctrlItemsListView();
      }
    }
  }
  /**
   * @description 绘制单多选
   * @return {*}
   * @memberof AppMobMDCtrl
   */


  renderSelectMDCtrl() {
    if (!this.c.isMultiple) {
      return this.renderSimpleList();
    } else {
      return this.renderMultipleList();
    }
  }
  /**
   * @description 绘制单选列表
   * @return {*}
   * @memberof AppMobMDCtrl
   */


  renderSimpleList() {
    let _slot2;

    var _a;

    const value = (_a = this.c.selections[0]) === null || _a === void 0 ? void 0 : _a.srfkey;
    return _createVNode(_resolveComponent("ion-radio-group"), {
      "value": value,
      "onIonChange": $event => this.simpleChange($event)
    }, _isSlot(_slot2 = this.c.items.map((item, index) => {
      return _createVNode(_resolveComponent("ion-item"), null, {
        default: () => [_createVNode(_resolveComponent("ion-radio"), {
          "value": item.srfkey
        }, null), _createVNode(_resolveComponent("app-list-item"), {
          "item": item,
          "index": index
        }, null)]
      });
    })) ? _slot2 : {
      default: () => [_slot2]
    });
  }
  /**
   * @description 绘制多选列表
   * @return {*}
   * @memberof AppMobMDCtrl
   */


  renderMultipleList() {
    return this.c.items.map((item, index) => {
      return _createVNode(_resolveComponent("ion-item"), null, {
        default: () => [_createVNode(_resolveComponent("ion-checkbox"), {
          "color": 'secondary',
          "checked": item.checked,
          "value": item.srfkey,
          "onIonChange": $event => this.multipleChange($event)
        }, null), _createVNode(_resolveComponent("app-list-item"), {
          "item": item,
          "index": index
        }, null)]
      });
    });
  }
  /**
   * @description 渲染触底加载更多
   * @return {*}
   * @memberof AppMobMDCtrl
   */


  renderBottomRefresh() {
    return _createVNode(_resolveComponent("ion-infinite-scroll"), {
      "onIonInfinite": event => {
        this.loadMore(event);
      },
      "threshold": '100px',
      "disabled": this.loadMoreDisabled.value
    }, {
      default: () => [_createVNode(_resolveComponent("ion-infinite-scroll-content"), {
        "loading-spinner": 'bubbles',
        "loading-text": this.$tl('share.loading', '加载中···')
      }, null)]
    });
  }
  /**
   * @description 绘制多数据部件
   * @return {*}
   * @memberof AppMobMDCtrl
   */


  render() {
    if (!this.controlIsLoaded.value) {
      return null;
    }

    const controlStyle = {
      width: this.c.controlInstance.width ? `${this.c.controlInstance.width}px` : '',
      height: this.c.controlInstance.height ? `${this.c.controlInstance.height}px` : ''
    };
    return this.c.items && this.c.items.length > 0 ? _createVNode(_resolveComponent("ion-content"), {
      "class": Object.assign({}, this.classNames),
      "style": controlStyle
    }, {
      default: () => [[this.renderPullDownRefresh(), this.renderMDCtrlContent(), this.renderBottomRefresh(), this.renderBatchToolbar()]]
    }) : [this.renderQuickToolbar(), this.renderNoData()];
  }

}
export const AppMobMDCtrlComponent = GenerateComponent(AppMobMDCtrl, Object.keys(new AppMobMDCtrlProps()));