import { isVNode as _isVNode, createVNode as _createVNode, resolveComponent as _resolveComponent } from "vue";
import { shallowReactive, ref } from 'vue';
import { AppMobContextMenuProps, MobContextMenuEvents } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { CtrlComponentBase } from './ctrl-component-base';
/**
 * 移动端上下文菜单
 *
 * @exports
 * @class AppMobContextMenu
 * @extends CtrlComponentBase
 */

function _isSlot(s) {
  return typeof s === 'function' || Object.prototype.toString.call(s) === '[object Object]' && !_isVNode(s);
}

class AppMobContextMenu extends CtrlComponentBase {
  constructor() {
    super(...arguments);
    /**
     * @description 是否显示弹窗
     * @type {Ref<boolean>}
     * @memberof AppMobContextMenu
     */

    this.isShowPopover = ref(true);
    /**
     * @description 触发上下文菜单的数据源
     * @type {IParam}
     * @memberof AppMobContextMenu
     */

    this.item = {};
  }
  /**
   * @description 响应式
   * @memberof AppMobContextMenu
   */


  setup() {
    this.c = shallowReactive(this.getCtrlControllerByType('CONTEXTMENU'));
    super.setup();
  }
  /**
   * @description 部件初始化
   * @memberof AppMobContextMenu
   */


  init() {
    super.init();
    this.toolbarItems = this.c.controlInstance.getPSDEToolbarItems() || [];
    this.item = this.c.navDatas[0];
  }
  /**
   * @description 设置弹窗状态
   * @param {boolean} state 状态
   * @memberof AppMobContextMenu
   */


  setOpenState(state) {
    this.isShowPopover.value = state;
  }
  /**
   * @description 上下文菜单项点击
   * @param {IPSDEToolbarItem} detail 界面行为模型
   * @param {MouseEvent} event 点击事件源
   * @memberof AppMobContextMenu
   */


  itemClick(detail, event) {
    var _a;

    const data = {
      name: this.c.controlInstance.name,
      detail: detail,
      event: event,
      data: this.item
    };
    this.setOpenState(false);
    this.emitCtrlEvent({
      controlname: (_a = this.c.controlInstance.controlType) === null || _a === void 0 ? void 0 : _a.toLowerCase(),
      action: MobContextMenuEvents.MENUITEM_CLICK,
      data: data
    });
  }
  /**
   * @description 绘制界面行为
   * @param {IPSDEToolbarItem} item 界面行为模型
   * @return {*}
   * @memberof AppMobContextMenu
   */


  renderUIAction(item) {
    var _a, _b, _c;

    const visible = this.c.contextMenuActionModel[item.name] ? (_a = this.c.contextMenuActionModel[item.name]) === null || _a === void 0 ? void 0 : _a.visabled : true;
    const disabled = this.c.contextMenuActionModel[item.name] ? (_b = this.c.contextMenuActionModel[item.name]) === null || _b === void 0 ? void 0 : _b.disabled : false;

    if (visible) {
      return _createVNode(_resolveComponent("ion-item"), {
        "button": true,
        "class": 'context-menu-item',
        "detail": false,
        "disabled": disabled,
        "onClick": event => this.itemClick(item, event)
      }, {
        default: () => [_createVNode(_resolveComponent("ion-label"), null, {
          default: () => [(item === null || item === void 0 ? void 0 : item.showIcon) && item.getPSSysImage() && _createVNode(_resolveComponent("app-icon"), {
            "icon": item.getPSSysImage()
          }, null), item.showCaption && _createVNode("span", null, [this.$tl((_c = item.getCapPSLanguageRes()) === null || _c === void 0 ? void 0 : _c.lanResTag, item.caption)])]
        })]
      });
    }
  }
  /**
   * @description 根据项类型绘制项
   * @param {IPSDEToolbarItem} item 项模型
   * @return {*}
   * @memberof AppMobContextMenu
   */


  renderToolbarItemsByType(item) {
    if (item.itemType === 'DEUIACTION') {
      return this.renderUIAction(item);
    } else if (item.itemType === 'SEPERATOR') {
      // todo 分割线
      return;
    } else if (item.itemType === 'ITEMS') {
      // todo 分组
      return;
    } else if (item.itemType === 'RAWITEM') {// return <span>{item.rawContent}</span>
    }
  }
  /**
   * @description 绘制上下文菜单内容
   * @return {*}
   * @memberof AppMobContextMenu
   */


  renderContextMenuContent() {
    let _slot;

    return _createVNode(_resolveComponent("ion-list"), null, _isSlot(_slot = this.toolbarItems.map(item => {
      return this.renderToolbarItemsByType(item);
    })) ? _slot : {
      default: () => [_slot]
    });
  }
  /**
   * @description 绘制上下文菜单
   * @return {*}
   * @memberof AppMobContextMenu
   */


  render() {
    let _slot2;

    if (!this.controlIsLoaded.value) {
      return;
    }

    return _createVNode(_resolveComponent("ion-popover"), {
      "cssClass": 'app-mob-contextmenu',
      "isOpen": this.isShowPopover.value,
      "event": this.c.mouseEvent,
      "onDidDismiss": () => this.setOpenState(false)
    }, _isSlot(_slot2 = this.renderContextMenuContent()) ? _slot2 : {
      default: () => [_slot2]
    });
  }

} // 移动端上下文菜单组件


export const AppMobContextMenuComponent = GenerateComponent(AppMobContextMenu, Object.keys(new AppMobContextMenuProps()));