import { shallowReactive } from 'vue';
import { AppMobListExpBarProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { ExpBarCtrlComponentBase } from './exp-bar-ctrl-component-base';
/**
 * @description 移动端列表导航部件
 * @export
 * @class AppMobListExpBar
 * @extends {DEViewComponentBase<AppMobListExpBarProps>}
 */

export class AppMobListExpBar extends ExpBarCtrlComponentBase {
  /**
   * 设置响应式
   *
   * @public
   * @memberof AppMobListExpBar
   */
  setup() {
    this.c = shallowReactive(this.getCtrlControllerByType('LISTEXPBAR'));
    super.setup();
  }

} // 移动端列表导航部件 组件

export const AppMobListExpBarComponent = GenerateComponent(AppMobListExpBar, Object.keys(new AppMobListExpBarProps()));