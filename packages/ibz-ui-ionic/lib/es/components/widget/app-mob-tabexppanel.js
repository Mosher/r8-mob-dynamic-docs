import { isVNode as _isVNode, createVNode as _createVNode, resolveComponent as _resolveComponent } from "vue";
import { AppMobMapProps } from 'ibz-core';
import { reactive, shallowReactive } from 'vue';
import { CtrlComponentBase } from './ctrl-component-base';
import { GenerateComponent } from '../component-base';
/**
 * 移动端分页导航面板部件
 *
 * @export
 * @class AppMobTabExpPanel
 * @extends CtrlComponentBase
 */

function _isSlot(s) {
  return typeof s === 'function' || Object.prototype.toString.call(s) === '[object Object]' && !_isVNode(s);
}

export class AppMobTabExpPanel extends CtrlComponentBase {
  /**
   * @description 设置响应式
   * @memberof AppMobTabExpPanel
   */
  setup() {
    this.c = shallowReactive(this.getCtrlControllerByType('TABEXPPANEL'));
    super.setup();
  }
  /**
   * @description 初始化响应式属性
   * @memberof AppMobTabExpPanel
   */


  initReactive() {
    super.initReactive();
    this.c.activeItem = reactive(this.c.activeItem);
  }
  /**
   * @description 分页节点切换
   * @private
   * @param {*} $event 节点数据
   * @return {*}
   * @memberof AppMobTabExpPanel
   */


  ionChange($event) {
    const {
      detail: _detail
    } = $event;

    if (!_detail) {
      return;
    }

    const {
      value: _value
    } = _detail;

    if (_value) {
      this.c.activeItemChange(_value);
    }
  }
  /**
   * @description 输出分页头部
   * @param {IPSDETabViewPanel[]} allControls 所有分页视图面板部件
   * @return {*}
   * @memberof AppMobTabExpPanel
   */


  renderSegment(allControls) {
    let _slot;

    return _createVNode(_resolveComponent("ion-segment"), {
      "class": 'tabexppanel-header',
      "scrollable": true,
      "value": this.c.activeItem,
      "onIonChange": $event => this.ionChange($event)
    }, _isSlot(_slot = allControls.map(viewPanel => {
      var _a;

      const counterData = this.c.getCounterData(viewPanel);
      return _createVNode(_resolveComponent("ion-segment-button"), {
        "value": viewPanel === null || viewPanel === void 0 ? void 0 : viewPanel.name
      }, {
        default: () => [_createVNode(_resolveComponent("ion-label"), null, {
          default: () => [viewPanel.getPSSysImage() && _createVNode(_resolveComponent("app-icon"), {
            "icon": viewPanel.getPSSysImage()
          }, null), this.$tl((_a = viewPanel.getCapPSLanguageRes()) === null || _a === void 0 ? void 0 : _a.lanResTag, viewPanel.caption), counterData && Object.is(this.c.activeItem, viewPanel.name) ? _createVNode(_resolveComponent("ion-badge"), {
            "class": 'badge'
          }, _isSlot(counterData) ? counterData : {
            default: () => [counterData]
          }) : null]
        })]
      });
    })) ? _slot : {
      default: () => [_slot]
    });
  }
  /**
   * @description 分页视图面板部件渲染
   * @param {IPSDETabViewPanel[]} allControls
   * @return {*}
   * @memberof AppMobTabExpPanel
   */


  renderTabViewPanel(allControls) {
    return allControls.map(item => {
      const otherParams = {
        key: item.name
      };

      if (Object.is(this.c.activeItem, item.name)) {
        return this.computeTargetCtrlData(item, otherParams);
      }
    });
  }
  /**
   * @description 分页导航面板部件渲染
   * @return {*}
   * @memberof AppMobTabExpPanel
   */


  render() {
    if (!this.controlIsLoaded.value) {
      return null;
    }

    const {
      width,
      height
    } = this.c.controlInstance;
    const chartControlstyle = {
      width: width ? `${width}px` : '',
      height: height ? `${height}px` : '100%'
    };
    const allControls = this.c.controlInstance.getPSControls();
    return _createVNode("div", {
      "class": Object.assign({}, this.classNames),
      "style": chartControlstyle
    }, [this.renderPullDownRefresh(), this.renderSegment(allControls), this.renderTabViewPanel(allControls)]);
  }

}
export const AppMobTabExpPanelComponent = GenerateComponent(AppMobTabExpPanel, Object.keys(new AppMobMapProps()));