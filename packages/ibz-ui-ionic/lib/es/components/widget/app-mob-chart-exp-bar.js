import { createVNode as _createVNode } from "vue";
import { shallowReactive } from 'vue';
import { AppMobChartExpBarProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { ExpBarCtrlComponentBase } from './exp-bar-ctrl-component-base';
/**
 * @description 移动端图表导航栏
 * @export
 * @class AppMobChartExpBar
 * @extends {DEViewComponentBase<AppMobChartExpBarProps>}
 */

export class AppMobChartExpBar extends ExpBarCtrlComponentBase {
  /**
   * 设置响应式
   *
   * @public
   * @memberof AppMobChartExpBar
   */
  setup() {
    this.c = shallowReactive(this.getCtrlControllerByType('CHARTEXPBAR'));
    super.setup();
  }
  /**
   * @description 渲染导航栏部件
   * @return {*}
   * @memberof AppMobChartExpBar
   */


  render() {
    if (!this.controlIsLoaded.value) {
      return null;
    }

    const {
      width,
      height
    } = this.c.controlInstance;
    const controlStyle = {
      width: width ? width + 'px' : '',
      height: height ? height + 'px' : ''
    };
    return _createVNode("div", {
      "class": Object.assign({
        'exp-bar': true
      }, this.classNames),
      "style": controlStyle
    }, [_createVNode("div", {
      "class": 'exp-bar-container'
    }, [_createVNode("div", {
      "class": 'container__multi_data_ctrl'
    }, [this.renderXDataControl()])])]);
  }

} // 移动端图表导航栏 组件

export const AppMobChartExpBarComponent = GenerateComponent(AppMobChartExpBar, Object.keys(new AppMobChartExpBarProps()));