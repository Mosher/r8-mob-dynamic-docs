import { isVNode as _isVNode, createVNode as _createVNode, resolveComponent as _resolveComponent } from "vue";
import { shallowReactive, ref } from 'vue';
import { actionSheetController } from '@ionic/vue';
import { menuController } from '@ionic/core';
import * as ionicons from 'ionicons/icons';
import { MobToolbarEvents } from 'ibz-core';
import { Util, ViewTool } from 'ibz-core';
import { AppMobToolbarProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { CtrlComponentBase } from './ctrl-component-base';
/**
 * @description 视图工具栏
 * @export
 * @class AppMobToolbar
 * @extends {CtrlComponentBase<AppMobToolbarProps>}
 */

function _isSlot(s) {
  return typeof s === 'function' || Object.prototype.toString.call(s) === '[object Object]' && !_isVNode(s);
}

export class AppMobToolbar extends CtrlComponentBase {
  constructor() {
    super(...arguments);
    /**
     * @description 工具栏弹出方向
     * @type {string}
     * @memberof AppMobToolbar
     */

    this.toolbarSide = '';
    /**
     * @description 工具栏弹出菜单引用
     * @type {IParam}
     * @memberof AppMobToolbar
     */

    this.toolbarRef = {};
    /**
     * @description 工具栏菜单uuid
     * @type {string}
     * @memberof AppMobToolbar
     */

    this.toolbarID = Util.createUUID();
    /**
     * @description 工具栏内容uuid
     * @type {string}
     * @memberof AppMobToolbar
     */

    this.contentId = Util.createUUID();
  }
  /**
   * @description 设置响应式
   * @memberof AppMobToolbar
   */


  setup() {
    this.toolbarRef = ref(null);
    this.c = shallowReactive(this.getCtrlControllerByType('TOOLBAR'));
    super.setup();
  }
  /**
   * @description 部件初始化
   * @memberof AppMobToolbar
   */


  init() {
    super.init();
    this.toolbarStyle = this.c.controlInstance.toolbarStyle ? this.c.controlInstance.toolbarStyle : 'TOOLBAR';
    this.calcToolbarStyleParams();
  }
  /**
   * @description 打开工具栏
   * @memberof AppMobToolbar
   */


  async openToolbar() {
    if (Object.is(this.toolbarSide, 'bottom')) {
      this.openBottomToolbar();
    } else {
      await menuController.enable(true, this.toolbarID);
      menuController.open(this.toolbarID);
    }
  }
  /**
   * @description 关闭工具栏
   * @memberof AppMobToolbar
   */


  closeToolbar() {
    if (Object.is(this.toolbarStyle, 'MOBNAVRIGHTMENU') || Object.is(this.toolbarStyle, 'MOBNAVLEFTMENU')) {
      menuController.close(this.toolbarID);
    }
  }
  /**
   * @description 工具栏项点击
   * @param {string} name
   * @param {MouseEvent} e
   * @memberof AppMobToolbar
   */


  itemClick(item, e) {
    this.emitCtrlEvent({
      action: MobToolbarEvents.TOOLBAR_CLICK,
      controlname: this.c.controlInstance.name,
      data: {
        tag: `${this.c.name}_${item.name}_click`,
        event: e
      }
    });
    this.closeToolbar();
  }
  /**
   * @description 获取图标
   * @param {IPSDEToolbarItem} item
   * @return {*}  {*}
   * @memberof AppMobToolbar
   */


  getIcon(item) {
    var _a, _b;

    if ((_a = item.getPSSysImage()) === null || _a === void 0 ? void 0 : _a.cssClass) {
      const iconName = Util.formatCamelCase(ViewTool.setIcon((_b = item.getPSSysImage()) === null || _b === void 0 ? void 0 : _b.cssClass));
      return iconName ? ionicons[iconName] : null;
    } else {
      return null;
    }
  }
  /**
   * @description 计算工具栏样式参数
   * @memberof AppMobToolbar
   */


  calcToolbarStyleParams() {
    switch (this.toolbarStyle) {
      case 'MOBNAVRIGHTMENU':
        this.toolbarSide = 'end';
        break;

      case 'MOBNAVLEFTMENU':
        this.toolbarSide = 'start';
        break;

      case 'MOBBOTTOMMENU':
        this.toolbarSide = 'bottom';
        break;
    }
  }
  /**
   * @description 绘制菜单工具项
   * @protected
   * @param {*} item
   * @return {*}  {*}
   * @memberof AppMobToolbar
   */


  renderToolbarItem(item) {
    var _a, _b, _c, _d;

    if (item.hiddenItem || item.noPrivHidden) {
      return null;
    }

    console.log(111, item);

    if ((_a = item.getPSSysPFPlugin()) === null || _a === void 0 ? void 0 : _a.pluginCode) {
      const ctrlItemPluginInstance = App.getPluginService().getCtrlItemPluginByTag((_b = item.getPSSysPFPlugin()) === null || _b === void 0 ? void 0 : _b.pluginCode);

      if (ctrlItemPluginInstance) {
        return ctrlItemPluginInstance.renderItem(item, this.c, this);
      }
    } else {
      let _slot;

      const itemStyle = {
        width: item.width ? item.width + 'px' : 'auto'
      };
      const cssName = ((_c = item.getPSSysCss()) === null || _c === void 0 ? void 0 : _c.cssName) || '';
      return _createVNode(_resolveComponent("ion-item"), {
        "disabled": item.noPrivDisabled,
        "style": itemStyle,
        "class": cssName,
        "onClick": e => this.itemClick(item, e)
      }, {
        default: () => [item.showIcon ? _createVNode(_resolveComponent("app-icon"), {
          "icon": item.getPSSysImage()
        }, null) : null, item.showCaption ? _createVNode(_resolveComponent("ion-label"), null, _isSlot(_slot = this.$tl((_d = item.getCapPSLanguageRes()) === null || _d === void 0 ? void 0 : _d.lanResTag, item.caption)) ? _slot : {
          default: () => [_slot]
        }) : null]
      });
    }
  }
  /**
   * @description 绘制侧边工具栏
   * @protected
   * @return {*}
   * @memberof AppMobToolbar
   */


  renderSideToolbar() {
    if (!this.toolbarSide || Object.is(this.toolbarSide, 'bottom')) {
      return null;
    }

    return this.renderToolbar(this.toolbarSide);
  }
  /**
   * @description 绘制工具栏按钮
   * @protected
   * @return {*}  {*}
   * @memberof AppMobToolbar
   */


  renderToolbarButton() {
    if (!Object.is(this.toolbarStyle, 'MOBNAVRIGHTMENU') && !Object.is(this.toolbarStyle, 'MOBNAVLEFTMENU') && !Object.is(this.toolbarStyle, 'MOBBOTTOMMENU')) {
      App.getNoticeService().warning(this.$tl('widget.mobtoolbar.notsupport', '工具栏样式暂未支持！！！'));
      return null;
    }

    return _createVNode(_resolveComponent("ion-button"), {
      "id": this.contentId,
      "onClick": () => this.openToolbar(),
      "expand": 'block'
    }, {
      default: () => [_createVNode(_resolveComponent("app-icon"), {
        "name": 'ellipsis-horizontal'
      }, null)]
    });
  }
  /**
   * @description 绘制侧边工具栏
   * @param {string} side
   * @return {*}
   * @memberof AppMobToolbar
   */


  renderToolbar(side) {
    let _slot2;

    if (this.c.toolbarModels.length == 0) {
      return null;
    }

    return _createVNode(_resolveComponent("ion-menu"), {
      "slot": 'fixed',
      "class": 'app-toolbar-popup',
      "ref": this.toolbarRef,
      "side": side,
      "contentId": this.contentId,
      "menuId": this.toolbarID
    }, {
      default: () => [_createVNode(_resolveComponent("ion-list"), null, _isSlot(_slot2 = this.c.toolbarModels.map(item => {
        return this.renderToolbarItem(item);
      })) ? _slot2 : {
        default: () => [_slot2]
      })]
    });
  }
  /**
   * @description 打开底部菜单
   * @memberof AppMobToolbar
   */


  async openBottomToolbar() {
    const buttons = [];
    this.c.toolbarModels.forEach(item => {
      var _a, _b;

      const cssName = ((_a = item.getPSSysCss()) === null || _a === void 0 ? void 0 : _a.cssName) || '';
      const icon = this.getIcon(item);
      buttons.push({
        text: this.$tl((_b = item.getCapPSLanguageRes()) === null || _b === void 0 ? void 0 : _b.lanResTag, item.caption),
        icon: icon,
        cssClass: cssName,
        handler: e => {
          this.itemClick(item, e);
        }
      });
    });
    const actionSheet = await actionSheetController.create({
      buttons: buttons
    });
    await actionSheet.present();
  }
  /**
   * @description 绘制平铺工具栏
   * @protected
   * @return {*}
   * @memberof AppMobToolbar
   */


  renderDefaultToolbar() {
    if (this.c.toolbarModels.length == 0) {
      return null;
    }

    if (Object.is(this.toolbarStyle, 'TOOLBAR')) {
      return this.c.toolbarModels.map(item => {
        var _a, _b, _c, _d;

        if (item.hiddenItem || item.noPrivHidden) {
          return null;
        }

        if ((_a = item.getPSSysPFPlugin()) === null || _a === void 0 ? void 0 : _a.pluginCode) {
          const ctrlItemPluginInstance = App.getPluginService().getCtrlItemPluginByTag((_b = item.getPSSysPFPlugin()) === null || _b === void 0 ? void 0 : _b.pluginCode);

          if (ctrlItemPluginInstance) {
            return ctrlItemPluginInstance.renderItem(item, this.c, this);
          }
        } else {
          const buttonStyle = {
            width: item.width ? item.width + 'px' : 'auto'
          };
          const cssName = ((_c = item.getPSSysCss()) === null || _c === void 0 ? void 0 : _c.cssName) || '';
          return _createVNode(_resolveComponent("ion-button"), {
            "disabled": item.noPrivDisabled,
            "style": buttonStyle,
            "class": cssName,
            "onClick": e => this.itemClick(item, e),
            "expand": 'block'
          }, {
            default: () => [_createVNode(_resolveComponent("app-icon"), {
              "icon": item.getPSSysImage()
            }, null), this.$tl((_d = item.getCapPSLanguageRes()) === null || _d === void 0 ? void 0 : _d.lanResTag, item.caption)]
          });
        }
      });
    } else {
      return this.renderToolbarButton();
    }
  }
  /**
   * @description 绘制工具栏
   * @return {*}  {*}
   * @memberof AppMobToolbar
   */


  render() {
    let _slot3;

    if (!this.controlIsLoaded.value) {
      return null;
    }

    const dir = Object.is(this.c.name, 'lefttoolbar') ? 'start' : 'end';
    return [_createVNode(_resolveComponent("ion-toolbar"), {
      "class": Object.assign({}, this.classNames),
      "slot": 'fixed'
    }, {
      default: () => [_createVNode(_resolveComponent("ion-buttons"), {
        "slot": dir
      }, _isSlot(_slot3 = this.renderDefaultToolbar()) ? _slot3 : {
        default: () => [_slot3]
      })]
    }), this.renderSideToolbar()];
  }

} // 应用菜单组件

export const AppMobToolbarComponent = GenerateComponent(AppMobToolbar, Object.keys(new AppMobToolbarProps()));