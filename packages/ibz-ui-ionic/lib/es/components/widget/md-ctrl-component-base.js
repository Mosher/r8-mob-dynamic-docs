import { ModelTool } from 'ibz-core';
import { reactive } from 'vue';
import { CtrlComponentBase } from './ctrl-component-base';
/**
 * 多数据部件组件基类
 *
 * @export
 * @class MDCtrlComponentBase
 */

export class MDCtrlComponentBase extends CtrlComponentBase {
  /**
   * @description 初始化响应式属性
   * @memberof MDCtrlComponentBase
   */
  initReactive() {
    super.initReactive();
    this.c.items = reactive(this.c.items);
    this.c.sort = reactive(this.c.sort);
    this.c.groupDetail = reactive(this.c.groupDetail);
    this.c.groupData = reactive(this.c.groupData);
    this.c.selections = reactive(this.c.selections);
    this.c.dataMap = reactive(this.c.dataMap);
  }
  /**
   * @description 绘制快速操作栏
   * @memberof MDCtrlComponentBase
   */


  renderQuickToolbar() {
    const quickToolbar = ModelTool.findPSControlByName(`${this.c.controlInstance.name}_quicktoolbar`, this.c.controlInstance.getPSControls());

    if (quickToolbar) {
      return this.computeTargetCtrlData(quickToolbar);
    }
  }
  /**
   * @description 绘制批操作工具栏
   * @memberof MDCtrlComponentBase
   */


  renderBatchToolbar() {
    if (this.c.selections.length > 0) {
      const batchToolbar = ModelTool.findPSControlByName(`${this.c.controlInstance.name}_batchtoolbar`, this.c.controlInstance.getPSControls());

      if (batchToolbar) {
        return this.computeTargetCtrlData(batchToolbar);
      }
    }
  }

}