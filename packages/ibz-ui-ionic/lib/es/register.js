import { IonApp, IonRouterOutlet, IonIcon, IonLabel, IonPage, IonTabBar, IonTabButton, IonTabs, IonBadge, IonHeader, IonToolbar, IonTitle, IonContent, IonInput, IonItem, IonRow, IonItemDivider, IonSegment, IonSegmentButton, IonTextarea, IonRadio, IonRadioGroup, IonSelect, IonDatetime, IonToggle, IonRange, IonList, IonListHeader, IonItemGroup, IonButton, IonCol, IonSearchbar, IonBackdrop, IonPopover, IonFooter, IonButtons, IonSelectOption, IonMenu, IonInfiniteScroll, IonInfiniteScrollContent, IonRefresher, IonRefresherContent, IonItemSliding, IonItemOption, IonItemOptions, IonCheckbox, IonActionSheet, IonCard, IonGrid, IonCardTitle, IonCardContent, IonCardHeader } from '@ionic/vue'; //  编辑器组件
// UI绘制组件

import { AppQuickGroup, AppMobAlert, AppIcon, AppActionBar, AppMenuIconComponent, AppMenuListComponent, AppListItemComponent, AppIconItemComponent, AppDashboardDesign, AppQuickSearchComponent, AppCalendarComponent, AppAnchorComponent, AppStepsComponent } from './components/common'; //指令引入

import { Badge, Format } from './directives';
export const ComponentRegister = {
  install(vue) {
    // 通用组件
    vue.component('ion-checkbox', IonCheckbox);
    vue.component('ion-popover', IonPopover);
    vue.component('ion-searchbar', IonSearchbar);
    vue.component('ion-app', IonApp);
    vue.component('ion-router-outlet', IonRouterOutlet);
    vue.component('ion-icon', IonIcon);
    vue.component('ion-label', IonLabel);
    vue.component('ion-item', IonItem);
    vue.component('ion-input', IonInput);
    vue.component('ion-page', IonPage);
    vue.component('ion-tab-bar', IonTabBar);
    vue.component('ion-tab-button', IonTabButton);
    vue.component('ion-tabs', IonTabs);
    vue.component('ion-badge', IonBadge);
    vue.component('ion-header', IonHeader);
    vue.component('ion-toolbar', IonToolbar);
    vue.component('ion-title', IonTitle);
    vue.component('ion-content', IonContent);
    vue.component('ion-row', IonRow);
    vue.component('ion-item-divider', IonItemDivider);
    vue.component('ion-segment', IonSegment);
    vue.component('ion-segment-button', IonSegmentButton);
    vue.component('ion-textarea', IonTextarea);
    vue.component('ion-radio', IonRadio);
    vue.component('ion-radio-group', IonRadioGroup);
    vue.component('ion-select', IonSelect);
    vue.component('ion-datetime', IonDatetime);
    vue.component('ion-toggle', IonToggle);
    vue.component('ion-range', IonRange);
    vue.component('ion-list', IonList);
    vue.component('ion-list-header', IonListHeader);
    vue.component('ion-item-group', IonItemGroup);
    vue.component('ion-button', IonButton);
    vue.component('ion-col', IonCol);
    vue.component('ion-backdrop', IonBackdrop);
    vue.component('ion-footer', IonFooter);
    vue.component('ion-buttons', IonButtons);
    vue.component('ion-select-option', IonSelectOption);
    vue.component('ion-menu', IonMenu);
    vue.component('ion-infinite-scroll', IonInfiniteScroll);
    vue.component('ion-infinite-scroll-content', IonInfiniteScrollContent);
    vue.component('ion-refresher', IonRefresher);
    vue.component('ion-refresher-content', IonRefresherContent);
    vue.component('ion-item-sliding', IonItemSliding);
    vue.component('ion-item-option', IonItemOption);
    vue.component('ion-item-options', IonItemOptions);
    vue.component('ion-grid', IonGrid);
    vue.component('ion-action-sheet', IonActionSheet);
    vue.component('ion-card', IonCard);
    vue.component('ion-card-header', IonCardHeader);
    vue.component('ion-card-title', IonCardTitle);
    vue.component('ion-card-content', IonCardContent); // 视图组件
    // UI绘制组件

    vue.component('app-calendar', AppCalendarComponent);
    vue.component('app-menu-icon', AppMenuIconComponent);
    vue.component('app-menu-list', AppMenuListComponent);
    vue.component('app-quick-search', AppQuickSearchComponent);
    vue.component('app-list-item', AppListItemComponent);
    vue.component('app-icon-item', AppIconItemComponent);
    vue.component('app-mob-alert', AppMobAlert);
    vue.component('app-icon', AppIcon);
    vue.component('app-quick-group', AppQuickGroup);
    vue.component('app-actionbar', AppActionBar);
    vue.component('app-dashboard-design', AppDashboardDesign);
    vue.component('app-anchor', AppAnchorComponent);
    vue.component('app-steps', AppStepsComponent); //指令注册

    vue.directive('badge', Badge);
    vue.directive('format', Format);
  }

};