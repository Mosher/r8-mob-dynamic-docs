// 通用组件国际化（英文）
function getLocaleResource() {
  const common_en_US = {
    input: {
      maxlength: 'Maximum content length is '
    },
    datapicker: {
      nopickupview: 'The selection view is not configured',
      nolinkview: 'The link view is not configured'
    },
    radiolist: {
      notfount: 'Not Fount'
    },
    richtext: {
      uploadfailed: 'Picture upload failed!'
    },
    span: {
      nocodelist: 'The code table does not exist'
    },
    textarea: {
      maxlength: 'Maximum content length is '
    },
    upload: {
      file: 'File',
      nofile: 'No files',
      notpicture: 'Not pictures',
      nopicture: 'No pictures',
      uploadfile: 'Upload files',
      uploadfailure: 'Upload failure',
      sizeover: 'The size of the beyond',
      uploadlimitamount: 'The upload limit is '
    },
    dashboard: {
      close: 'close',
      customdatakanban: 'Custom data Kanban',
      existcard: 'Cards that have been added',
      noexistcard: 'Cards that can be added',
      nodescription: 'No description'
    },
    calendar: {
      today: 'today',
      Monday: 'Mon',
      Tuesday: 'Tue',
      Wednesday: 'Wed',
      Thursday: 'Thu',
      Friday: 'Fri',
      Saturday: 'Sat',
      Sunday: 'Sun',
      January: 'January',
      February: 'February',
      March: 'March',
      April: 'April',
      May: 'May',
      June: 'June',
      July: 'July',
      August: 'August',
      September: 'September',
      October: 'October',
      November: 'November',
      December: 'December'
    }
  };
  return common_en_US;
}

export default getLocaleResource;