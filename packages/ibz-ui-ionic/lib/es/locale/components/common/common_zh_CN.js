// 通用组件国际化（中文）
function getLocaleResource() {
  const common_zh_CN = {
    input: {
      maxlength: '最大内容长度为 '
    },
    datapicker: {
      nopickupview: '没有配置选择视图',
      nolinkview: '没有配置链接视图'
    },
    radiolist: {
      notfount: '未找到'
    },
    richtext: {
      uploadfailed: '图片上传失败！'
    },
    span: {
      nocodelist: '代码表不存在'
    },
    textarea: {
      maxlength: '最大内容长度为 '
    },
    upload: {
      file: '文件',
      nofile: '没有文件',
      notpicture: '不是图片',
      nopicture: '没有图片',
      uploadfile: '上传文件',
      uploadfailure: '上传失败',
      sizeover: '大小超出',
      uploadlimitamount: '上传限制数量为 '
    },
    dashboard: {
      close: '关闭',
      customdatakanban: '自定义数据看板',
      existcard: '已经添加的卡片',
      noexistcard: '可添加的卡片',
      nodescription: '暂无描述'
    },
    calendar: {
      today: '今',
      Monday: '一',
      Tuesday: '二',
      Wednesday: '三',
      Thursday: '四',
      Friday: '五',
      Saturday: '六',
      Sunday: '日',
      January: '一月',
      February: '二月',
      March: '三月',
      April: '四月',
      May: '五月',
      June: '六月',
      July: '七月',
      August: '八月',
      September: '九月',
      October: '十月',
      November: '十一月',
      December: '十二月'
    }
  };
  return common_zh_CN;
}

export default getLocaleResource;