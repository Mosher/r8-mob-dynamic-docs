// 视图组件国际化（中文）
function getLocaleResource() {
  const view_zh_CN = {
    mobhtmlview: {
      notexist: '抱歉，您访问的页面不存在！'
    },
    notsupportview: {
      tip: '暂未支持'
    },
    wfdynaactionview: {
      submit: '提交'
    }
  };
  return view_zh_CN;
}

export default getLocaleResource;