// 视图组件国际化（英文）
function getLocaleResource() {
  const view_en_US = {
    mobhtmlview: {
      notexist: 'Sorry, the page you visited does not exist!'
    },
    notsupportview: {
      tip: 'Temporary does not support'
    },
    wfdynaactionview: {
      submit: 'submit'
    }
  };
  return view_en_US;
}

export default getLocaleResource;