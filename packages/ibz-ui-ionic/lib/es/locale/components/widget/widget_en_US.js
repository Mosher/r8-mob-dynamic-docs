// 部件组件国际化（英文）
function getLocaleResource() {
  const widget_en_US = {
    mobformdruipart: {
      tooltip: 'Please save the master data first'
    },
    mobformgroup: {
      hidden: 'Hidden',
      more: 'Show more'
    },
    mobdashboard: {
      title: 'Custom dashboard'
    },
    mobmeditviewpanel: {
      noembeddedview: 'No embedded view'
    },
    mobpanel: {
      nomarkdown: 'MARKDOWN direct content is not currently supported'
    },
    mobpickupviewpanel: {
      nopickerview: 'Unselected view'
    },
    mobportlet: {
      nomarkdown: 'MARKDOWN direct content is not currently supported',
      toolbar: 'Toolbar',
      rendercustom: 'Draw custom'
    },
    mobsearchform: {
      detailtype: 'Type form member',
      filter: 'Filter',
      search: 'Search',
      reset: 'Reset'
    },
    mobtabviewpanel: {
      noembeddedview: 'The embedded view does not exist'
    },
    mobtoolbar: {
      exportmaxrow: 'Export maximum row',
      exportcurpage: 'Export current page',
      notsupport: 'Toolbar style not supported yet!!'
    }
  };
  return widget_en_US;
}

export default getLocaleResource;