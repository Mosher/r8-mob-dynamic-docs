// 英文语言资源
export { en_US } from './lang/en_US'; // 中文语言资源

export { zh_CN } from './lang/zh_CN'; // 多语言翻译

export const translate = (key, context, value) => {
  if (key) {
    if (context.$te(key)) {
      return context.$t(key);
    } else {
      if (context.modelService) {
        const lanResource = context.modelService.getPSLang(key);
        return lanResource ? lanResource : value ? value : key;
      } else {
        return value ? value : key;
      }
    }
  } else {
    return value;
  }
}; // 处理语言路径映射

export const handleLocaleMap = key => {
  switch (key) {
    case 'zh-CN':
      return 'ZH_CN';

    case 'en-US':
      return 'EN';

    default:
      return 'ZH_CN';
  }
};