import { AppDrawerService } from '../app-drawer-service/app-drawer-service';
import { AppModalService } from '../app-modal-service/app-modal-service';
export class AppViewOpenService {
  /**
   * 单例构造器
   *
   * @static
   * @return {*}
   * @memberof AppViewOpenService
   */
  static getInstance(router) {
    if (!this.instance) {
      this.instance = new AppViewOpenService();
      this.instance.router = router;
    }

    return this.instance;
  }
  /**
   * @description 打开模态
   * @param {({ viewComponent?: any | undefined, viewModel?: IParam, customClass?: string | undefined, customStyle?: IParam })} view
   * @param {IContext} [context] 应用上下文
   * @param {IParam} [navParam] 导航参数
   * @param {*} [navDatas] 数据
   * @param {IParam} [otherParam] 额外参数
   * @return {*}  {Subject<{ret: boolean, datas?: IParam[]}>}
   * @memberof AppViewOpenService
   */


  openModal(view, context, navParam, navDatas, otherParam) {
    return AppModalService.getInstance().openModal(view, context, navParam, navDatas, otherParam);
  }
  /**
   * 路由打开
   *
   * @param {*} opts
   * @returns {Promise<any>}
   * @memberof AppViewOpenService
   */


  openView(path) {
    if (path) {
      this.router.push(path);
    }
  }
  /**
   * 浏览器新标签页打开
   *
   * @param {string} url
   * @returns {Promise<any>}
   * @memberof AppViewOpenService
   */


  openPopupApp(url) {
    window.open(url, '_blank');
  }
  /**
   * @description 打开抽屉
   * @param {({ viewComponent?: any | undefined, viewModel?: IParam, customClass?: string | undefined, customStyle?: IParam })} view
   * @param {IContext} [context] 应用上下文
   * @param {IParam} [navParam] 导航参数
   * @param {*} [navDatas] 数据
   * @param {IParam} [otherParam] 额外参数
   * @return {*}  {Subject<{ret: boolean, datas?: IParam[]}>}
   * @memberof AppViewOpenService
   */


  openDrawer(view, context, navParam, navDatas, otherParam) {
    return AppDrawerService.getInstance().openDrawer(view, context, navParam, navDatas, otherParam);
  }

}