/**
 * @description 应用加载服务
 * @export
 * @class AppLoadingService
 * @implements {ILoadingService}
 */
export class AppLoadingService {
  constructor() {
    /**
     * @description 是否加载
     * @type {boolean}
     * @memberof AppLoadingServiceBase
     */
    this.isLoading = false;
    /**
     * @description 统计加载
     * @type {number}
     * @memberof AppAppLoadingService
     */

    this.loadingCount = 0;
  }
  /**
   * @description 获取应用加载服务实例
   * @static
   * @return {*}
   * @memberof AppLoadingService
   */


  static getInstance() {
    if (!this.instance) {
      this.instance = new AppLoadingService();
    }

    return this.instance;
  }
  /**
   * @description 加载结束
   * @memberof AppLoadingService
   */


  endLoading() {
    const endLoading = selector => {
      if (!this.isLoading) {
        return;
      }

      if (selector) {
        const loadMask = selector.querySelector('.loading-container');

        if (loadMask && selector.contains(loadMask)) {
          selector.removeChild(loadMask);
        }
      }

      this.isLoading = false;
    };

    const body = document.querySelector('body');

    if (this.loadingCount > 0) {
      this.loadingCount--;
    }

    if (this.loadingCount === 0) {
      endLoading(body);
    }
  }
  /**
   * @description 应用开始加载
   * @memberof AppLoadingService
   */


  beginLoading() {
    const beginLoading = selector => {
      this.isLoading = true; // 自定义loading元素

      const userEle = document.createElement('div');
      userEle.classList.add('loading-container');
      const innerDiv = document.createElement('div');
      innerDiv.classList.add('loading');

      for (let i = 0; i < 4; i++) {
        const dot = document.createElement('span');
        innerDiv.appendChild(dot);
      }

      userEle.appendChild(innerDiv); // 挂载

      if (selector) {
        selector.appendChild(userEle);
      }
    };

    const body = document.querySelector('body');

    if (this.loadingCount === 0) {
      beginLoading(body);
    }

    this.loadingCount++;
  }

}