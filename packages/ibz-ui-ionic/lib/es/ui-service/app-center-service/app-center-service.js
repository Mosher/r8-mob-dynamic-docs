import { Subject } from 'rxjs';
/**
 * 应用中心服务类
 *
 * @export
 * @class AppCenterService
 */

export class AppCenterService {
  constructor() {
    /**
     * 应用数据状态管理对象
     *
     * @private
     * @type {Subject<any>}
     * @memberof AppCenterService
     */
    this.subject = new Subject();
  }
  /**
   * 获取 AppCenterService 单例对象
   *
   * @static
   * @returns {AppCenterService}
   * @memberof AppCenterService
   */


  static getInstance() {
    if (!AppCenterService.appCenterService) {
      AppCenterService.appCenterService = new AppCenterService();
    }

    return this.appCenterService;
  }
  /**
   * 通知消息
   *
   * @param {*} name 名称(通常是应用实体名称)
   * @param {*} action 行为（操作数据行为）
   * @param {*} data 数据（操作数据）
   * @memberof AppCenterService
   */


  notifyMessage(name, action, data) {
    this.subject.next({
      name,
      action,
      data
    });
  }
  /**
   * 获取消息中心
   *
   * @memberof AppCenterService
   */


  getMessageCenter() {
    return this.subject;
  }

}