/**
 * @description 数据看板设计服务
 * @class AppDashboardDesignService
 */
export class AppDashboardDesignService {
  /**
   * @description 获取数据看板设计服务（单例对象）
   * @memberof AppDashboardDesignService
   */
  static getInstance() {
    if (!this.intance) {
      this.intance = new AppDashboardDesignService();
    }

    return this.intance;
  }
  /**
   * @description 保存模型数据
   * @param {string} serviceKey 工具服务标识
   * @param {IParam} context 应用上下文
   * @param {IParam} viewParam 保存视图参数
   * @memberof AppDashboardDesignService
   */


  saveModelData(serviceKey, context, viewParam) {
    return new Promise((resolve, reject) => {
      App.getUtilService().getService(context, serviceKey).then(utilService => {
        const saveModel = [];

        if (viewParam.model) {
          for (const model of viewParam.model) {
            const temp = Object.assign({}, model);
            delete temp.modelData;
            saveModel.push(temp);
          }
        }

        viewParam.model = saveModel;
        utilService.saveModelData(context, viewParam).then(result => {
          resolve(result);
        }).catch(error => {
          reject(error);
        });
      });
    });
  }
  /**
   * @description 加载门户部件集合
   * @param {IParam} context 应用上下文
   * @param {IParam} viewParam 视图参数
   */


  async loadPortletList(context, viewParam) {
    const app = App.getModel();
    const list = [];
    const portletCats = app.getAllPSAppPortletCats() || [];
    const portlets = app.getAllPSAppPortlets() || [];
    portlets.forEach(portlet => {
      var _a, _b, _c, _d, _e, _f;

      const appDe = (_a = portlet.getPSAppDataEntity) === null || _a === void 0 ? void 0 : _a.call(portlet);
      const portletCat = portletCats.find(cat => {
        var _a, _b;

        return cat.codeName == ((_b = (_a = portlet.getPSAppPortletCat) === null || _a === void 0 ? void 0 : _a.call(portlet)) === null || _b === void 0 ? void 0 : _b.codeName);
      });
      const temp = {
        type: 'app',
        portletType: portlet.portletType,
        portletCodeName: portlet.codeName,
        portletName: portlet.name,
        portletImage: (_e = (_d = (_c = (_b = portlet.getPSControl) === null || _b === void 0 ? void 0 : _b.call(portlet)) === null || _c === void 0 ? void 0 : _c.getPSSysImage) === null || _d === void 0 ? void 0 : _d.call(_c)) === null || _e === void 0 ? void 0 : _e.cssClass,
        groupCodeName: (portletCat === null || portletCat === void 0 ? void 0 : portletCat.codeName) || '',
        groupName: (portletCat === null || portletCat === void 0 ? void 0 : portletCat.name) || '',
        appCodeName: (appDe === null || appDe === void 0 ? void 0 : appDe.codeName) || app.pKGCodeName,
        appName: (appDe === null || appDe === void 0 ? void 0 : appDe.logicName) || app.name,
        detailText: portlet.userTag,
        modelData: (_f = portlet.getPSControl) === null || _f === void 0 ? void 0 : _f.call(portlet)
      };
      list.push(temp);
    });
    const datas = this.filterData(list, viewParam.appdeNamePath);
    const result = this.prepareList(datas);
    const groups = this.prepareGroup(datas);
    return {
      data: datas,
      result: result.reverse(),
      groups: groups
    };
  }
  /**
   * @description 过滤数据
   * @param {any[]} datas
   * @memberof AppDashboardDesignService
   */


  filterData(datas = [], dataType) {
    const items = [];
    datas.forEach(data => {
      if (Object.is(data.type, 'app')) {
        items.push(data);
      }

      if (Object.is(data.appCodeName, dataType)) {
        items.push(data);
      }
    });
    return items;
  }
  /**
   * @description 分组集合
   * @param {any[]} [datas=[]]
   * @returns {any[]}
   * @memberof AppDashboardDesignService
   */


  prepareGroup(datas = []) {
    const items = [];
    datas.forEach(data => {
      const item = items.find(item => Object.is(item.value, data.groupCodeName));

      if (item) {
        const _item = item.children.find(a => Object.is(a.portletCodeName, data.portletCodeName));

        if (!_item) {
          item.children.push(data);
        }
      } else {
        items.push({
          name: data.groupName,
          value: data.groupCodeName,
          children: [data]
        });
      }
    });
    return items;
  }
  /**
   * @description 准备list集合
   * @memberof AppDashboardDesignService
   */


  prepareList(datas = []) {
    const list = [];
    datas.forEach(data => {
      let item = list.find(item => Object.is(data.type, item.type));

      if (!item) {
        item = {};
        Object.assign(item, {
          type: data.type,
          name: Object.is(data.type, 'app') ? '全局' : data.appName,
          children: []
        });
        list.push(item);
      }

      this.prepareList2(item.children, data);
    });
    return list;
  }
  /**
   * @description 准备list项集合
   * @param {any[]} [children=[]]
   * @param {*} [data={}]
   * @memberof AppDashboardDesignService
   */


  prepareList2(children = [], data = {}) {
    let item = children.find(item => Object.is(data.groupCodeName, item.type));

    if (!item) {
      item = {};
      Object.assign(item, {
        type: data.groupCodeName,
        name: data.groupName,
        children: []
      });
      children.push(item);
    }

    const _item = item.children.find(a => Object.is(a.portletCodeName, data.portletCodeName));

    if (!_item) {
      item.children.push(data);
    }
  }

}