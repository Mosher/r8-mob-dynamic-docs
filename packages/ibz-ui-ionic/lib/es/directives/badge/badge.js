import { Util } from 'ibz-core';
/**
 * 微标指令
 *
 * @export
 * @class Badge
 */

export const Badge = {
  /**
   * 指令初始化
   *
   * @param {HTMLDivElement} el
   * @param {*} binding
   * @param {VNode} vNode
   * @param {VNode} oldVNode
   */
  beforeMount(el, binding, vNode, oldVNode) {
    bc.init(el, binding);
  },

  /**
   * 指令更新
   *
   * @param {HTMLDivElement} el
   * @param {*} binding
   * @param {VNode} vNode
   * @param {VNode} oldVNode
   */
  updated(el, binding, vNode, oldVNode) {
    bc.update(el, binding);
  }

};
/**
 * 微标控制器
 *
 * @export
 * @class BadgeController
 */

export class BadgeController {
  /**
   * Creates an instance of BadgeControllerController.
   * @memberof BadgeControllerController
   */
  constructor() {
    if (BadgeController.instance) {
      return BadgeController.instance;
    }
  }
  /**
   * 初始化
   *
   * @param {HTMLDivElement}
   * @param {any}
   * @memberof BadgeController
   */


  init(el, binding) {
    const item = binding.value;

    if (Object.keys(item).length > 0 && Util.isExistAndNotEmpty(item.count)) {
      if (!item.showZero && item.count == 0) {
        return;
      }

      const badge = document.createElement('sup');
      badge.innerHTML = String(item.count);
      badge.classList.add('ibiz-badge');
      this.el = badge;
      el.append(badge);
      this.setBadgeclass(item.type);
      this.setBadgeclass(item.size);
      this.setBadgeclass(item.className);
      this.setBadgeOffset(item.offset);
    }
  }
  /**
   * 更新
   *
   * @param {HTMLDivElement}
   * @param {any}
   * @memberof BadgeController
   */


  update(el, binding) {
    const item = binding.value;

    if (Object.keys(item).length > 0 && Util.isExistAndNotEmpty(item.count)) {
      if (!item.showZero && item.count == 0) {
        return;
      }

      this.el.innerHTML = String(item.count);
    }
  }
  /**
   * 设置徽标类型样式
   *
   * @param {string}
   * @memberof BadgeController
   */


  setBadgeclass(type) {
    if (!type) {
      return;
    }

    this.el.classList.add(type);
  }
  /**
   * 设置徽标偏移量
   *
   * @param {string}
   * @memberof BadgeController
   */


  setBadgeOffset(offset) {
    if (offset && offset.length == 2) {
      this.el.style.transform = `translate(${offset[0] / 50}rem, ${offset[1] / 50}rem)`;
    }
  }
  /**
   * 获取唯一实例
   *
   * @static
   * @returns {BadgeController}
   * @memberof BadgeController
   */


  static getInstance() {
    return BadgeController.instance;
  }

}
/**
 * 唯一实例
 *
 * @private
 * @static
 * @memberof BadgeControllerController
 */

BadgeController.instance = new BadgeController(); // 导出服务

export const bc = BadgeController.getInstance();