// 导入需要的库
const genCode = require('./gen').genCode;

// 模板参数对象
const tplCtx = {
  templateType: 'view',
  viewType: 'DEMOBWFDYNAEDITVIEW',
  logicName: '移动端动态工作流编辑视图',
  name: 'MobWFDynaEditView',
  name2: 'mob-wfdynaedit-view',
  extendName: 'DeView',
  extendName2: 'de-view',
};

// 输出文件列表
const genList = [
  {
    tplPath: 'build/gen-code/template/components/view/app-template.njk',
    outPath: `src/components/view/app-${tplCtx.name2}.tsx`,
  },
  {
    isAppend: true,
    tplPath: 'build/gen-code/template/components/view/index.njk',
    outPath: `src/components/view/index.ts`,
  },
  {
    tplPath: 'build/gen-code/template/components/layout/app-template.njk',
    outPath: `src/components/layout/app-${tplCtx.name2}-layout.tsx`,
  },
  {
    isAppend: true,
    tplPath: 'build/gen-code/template/components/layout/index.njk',
    outPath: `src/components/layout/index.ts`,
  },
  {
    tplPath: 'build/gen-code/template/styles/layout/app-template.njk',
    outPath: `src/styles/layout/app-${tplCtx.name2}-layout.scss`,
  },
];

try {
  for (const item of genList) {
    genCode(item.tplPath, item.outPath, { tplCtx }, item.isAppend);
  }
} catch (error) {
  console.log(error);
}
