// 导入需要的库
const genCode = require('./gen').genCode;

// 模板参数对象
const tplCtx = {
  templateType: 'editor',
  editorType: 'MOBDATE',
  logicName: '移动端时间选择编辑器',
  name: 'DatePicker',
  name2: 'date-picker',
};

// 输出文件列表
const genList = [
  {
    tplPath: 'build/gen-code/template/components/editor/app-template.njk',
    outPath: `src/components/editor/app-${tplCtx.name2}-editor.tsx`,
  },
  {
    isAppend: true,
    tplPath: 'build/gen-code/template/components/editor/index.njk',
    outPath: `src/components/editor/index.ts`,
  },
];

try {
  for (const item of genList) {
    genCode(item.tplPath, item.outPath, { tplCtx }, item.isAppend);
  }
} catch (error) {
  console.log(error);
}
