// 导入需要的库
const nunjucks = require('nunjucks');
const fs = require('fs');
const path = require('path');

// 工具方法

/**
 * @description 判断路径是否存在，不存在则创建目录，支持多级递归
 * @param {*} dir 目录地址
 */
function mkDirs(dir) {
  var parentDir = path.parse(dir);
  if (!fs.existsSync(parentDir.dir)) {
    mkDirs(parentDir.dir);
    fs.mkdirSync(parentDir.dir);
  }
}

// 配置模板文件
nunjucks.configure({ autoescape: true });

/**
 * @description 生成模板文件方法
 * @param {*} tplPath 模板文件路径
 * @param {*} outPath 成果物输出路径
 * @param {*} ctx 模板上下文对象
 * @param {boolean} [isAppend=false] 是否在文件内追加
 */
function genCode(tplPath, outPath, ctx, isAppend = false) {
  // 判断模板文件是否存在
  if (!fs.existsSync(tplPath)) {
    throw new Error(`${tplPath}不存在`);
  }
  const result = nunjucks.render(tplPath, ctx);

  // 输出目录不存在时新建
  mkDirs(outPath);
  if (!isAppend) {
    fs.writeFileSync(path.resolve(outPath), result);
  } else {
    fs.appendFileSync(path.resolve(outPath), result);
  }
}

module.exports = { genCode };
