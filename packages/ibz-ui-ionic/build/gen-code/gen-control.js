// 导入需要的库
const genCode = require('./gen').genCode;

// 模板参数对象
const tplCtx = {
  templateType: 'control',
  controlType: 'PORTLET',
  logicName: '移动端门户部件',
  name: 'MobPortlet',
  name2: 'mob-portlet',
  extendName: 'Ctrl',
  extendName2: 'ctrl',
};

// 输出文件列表
const genList = [
  {
    tplPath: 'build/gen-code/template/components/widget/app-template.njk',
    outPath: `src/components/widget/app-${tplCtx.name2}.tsx`,
  },
  {
    isAppend: true,
    tplPath: 'build/gen-code/template/components/widget/index.njk',
    outPath: `src/components/widget/index.ts`,
  },
];

try {
  for (const item of genList) {
    genCode(item.tplPath, item.outPath, { tplCtx }, item.isAppend);
  }
} catch (error) {
  console.log(error);
}
