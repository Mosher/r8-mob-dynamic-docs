module.exports = {
  //一行最多有多少字符
  printWidth: 120,
  //语句末尾打印分号
  semi: true,
  //使用单引号代替双引号(JSX忽略此配置)
  singleQuote: true,
  //在多行逗号分隔的句法结构中尽可能打印尾随逗号
  trailingComma: 'all',
  //括号之间打印空格
  bracketSpacing: true,
  //将`>`多行 HTML（HTML、JSX、Vue、Angular）元素的 放在最后一行的末尾，而不是单独放在下一行（不适用于自关闭元素）
  bracketSameLine: false,
  //在 JSX 中使用单引号代替双引号
  jsxSingleQuote: true,
  //引用对象中的属性时更改
  quoteProps: 'as-needed',
  //在唯一的箭头函数参数周围包含括号
  arrowParens: 'avoid',
  //指定每个缩进级别的空格数
  tabWidth: 2,
  //用制表符而不是空格缩进行
  useTabs: false,
  //换行符
  endOfLine: 'auto',
};
