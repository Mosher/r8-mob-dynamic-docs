import { IComponentService } from 'ibz-core';
//  视图布局面板组件
import {
  AppDefaultIndexViewLayoutComponent,
  AppDefaultMobCalendarViewLayoutComponent,
  AppDefaultMobChartExpViewLayoutComponent,
  AppDefaultMobChartViewLayoutComponent,
  AppDefaultMobCustomViewLayoutComponent,
  AppDefaultMobDEDashboardViewLayoutComponent,
  AppDefaultMobEditViewLayoutComponent,
  AppDefaultMobHtmlViewLayoutComponent,
  AppDefaultMobListExpViewLayoutComponent,
  AppDefaultMobMapExpViewLayoutComponent,
  AppDefaultMobMapViewLayoutComponent,
  AppDefaultMobMDViewLayoutComponent,
  AppDefaultMobMEditViewLayoutComponent,
  AppDefaultMobMPickupViewLayoutComponent,
  AppDefaultMobOptViewLayoutComponent,
  AppDefaultMobPanelViewLayoutComponent,
  AppDefaultMobPickUpMDViewLayoutComponent,
  AppDefaultMobPickupTreeViewLayoutComponent,
  AppDefaultMobPickupViewLayoutComponent,
  AppDefaultMobTabExpViewLayoutComponent,
  AppDefaultMobTreeExpViewLayoutComponent,
  AppDefaultMobTreeViewLayoutComponent,
  AppDefaultMobWFDynaActionViewLayoutComponent,
  AppDefaultMobWFDynaEditViewLayoutComponent,
  AppDefaultMobWizardViewLayoutComponent,
  AppDefaultPortalViewLayoutComponent,
  AppDefaultMobCalendarExpViewLayoutComponent,
} from '../../components/layout';

//  视图组件
import {
  AppIndexViewComponent,
  AppMobCalendarViewComponent,
  AppMobChartExpViewComponent,
  AppMobChartViewComponent,
  AppMobCustomViewComponent,
  AppMobDEDashboardViewComponent,
  AppMobEditViewComponent,
  AppMobHtmlViewComponent,
  AppMobListExpViewComponent,
  AppMobMapExpViewComponent,
  AppMobMapViewComponent,
  AppMobMDViewComponent,
  AppMobMEditViewComponent,
  AppMobMPickUpViewComponent,
  AppMobNotSupportedViewComponent,
  AppMobOptViewComponent,
  AppMobPanelViewComponent,
  AppMobPickUpMDViewComponent,
  AppMobPickupTreeViewComponent,
  AppMobPickUpViewComponent,
  AppMobTabExpViewComponent,
  AppMobTreeExpViewComponent,
  AppMobTreeViewComponent,
  AppMobWFDynaActionViewComponent,
  AppMobWFDynaEditViewComponent,
  AppMobWizardViewComponent,
  AppPortalViewComponent,
  AppMobCalendarExpViewComponent,
} from '../../components/view';

//  部件组件
import {
  AppMobCalendarComponent,
  AppMobChartComponent,
  AppMobChartExpBarComponent,
  AppMobContextMenuComponent,
  AppMobDashboardComponent,
  AppMobFormComponent,
  AppMobListExpBarComponent,
  AppMobMapComponent,
  AppMobMapExpBarComponent,
  AppMobMDCtrlComponent,
  AppMobMEditViewPanelComponent,
  AppMobMenuComponent,
  AppMobNotSupportedControlComponent,
  AppMobPanelComponent,
  AppMobPickUpViewPanelComponent,
  AppMobPortletComponent,
  AppMobSearchFormComponent,
  AppMobStateWizardPanelComponent,
  AppMobTabExpPanelComponent,
  AppMobTabViewPanelComponent,
  AppMobToolbarComponent,
  AppMobTreeComponent,
  AppMobTreeExpBarComponent,
  AppMobWizardPanelComponent,
  AppMobCalendarExpBarComponent,
} from '../../components/widget';

//  编辑器组件
import {
  AppCheckBoxEditorComponent,
  AppDataPickerEditorComponent,
  AppDatePickerEditorComponent,
  AppDropdownListEditorComponent,
  AppMobNotSupportedEditorComponent,
  AppRatingEditorComponent,
  AppRichTextEditorComponent,
  AppSliderEditorComponent,
  AppSpanEditorComponent,
  AppStepperEditorComponent,
  AppSwitchEditorComponent,
  AppTextboxEditorComponent,
  AppUploadEditorComponent,
} from '../../components/editor';

/**
 * 应用组件服务
 *
 * @memberof AppComponentService
 */
export class AppComponentService implements IComponentService {
  /**
   * @description 单例变量声明
   *
   * @private
   * @static
   * @type {AppComponentService}
   * @memberof AppComponentService
   */
  private static AppComponentService: AppComponentService;

  /**
   * @description 视图类型组件Map
   * @protected
   * @type {Map<string, any>}
   * @memberof AppComponentService
   */
  protected viewTypeMap: Map<string, any> = new Map();

  /**
   * @description 视图布局组件Map
   * @protected
   * @type {Map<string, any>}
   * @memberof AppComponentService
   */
  protected layoutMap: Map<string, any> = new Map();

  /**
   * @description 部件组件Map
   * @protected
   * @type {Map<string, any>}
   * @memberof AppComponentService
   */
  protected controlMap: Map<string, any> = new Map();

  /**
   * 控件组件Map
   *
   * @memberof AppComponentService
   */
  protected editorMap: Map<string, any> = new Map();

  /**
   * Creates an instance of AppComponentService.
   * @memberof AppComponentService
   */
  public constructor() {
    this.registerAppComponents();
  }

  /**
   * 获取 AppComponentService 单例对象
   *
   * @static
   * @returns {AppComponentService}
   * @memberof AppComponentService
   */
  static getInstance(): AppComponentService {
    if (!AppComponentService.AppComponentService) {
      AppComponentService.AppComponentService = new AppComponentService();
    }
    return this.AppComponentService;
  }

  /**
   * @description 注册应用组件
   * @memberof AppComponentService
   */
  public registerAppComponents() {
    this.registerViewTypeComponents();
    this.registerLayoutComponent();
    this.registerControlComponents();
    this.registerEditorComponents();
  }

  /**
   * @description 注册布局组件
   * @memberof AppComponentService
   */
  public registerLayoutComponent() {
    this.layoutMap.set('APPINDEXVIEW_DEFAULT', AppDefaultIndexViewLayoutComponent);
    this.layoutMap.set('APPPORTALVIEW_DEFAULT', AppDefaultPortalViewLayoutComponent);
    this.layoutMap.set('DEMOBEDITVIEW_DEFAULT', AppDefaultMobEditViewLayoutComponent);
    this.layoutMap.set('DEMOBMDVIEW_DEFAULT', AppDefaultMobMDViewLayoutComponent);
    this.layoutMap.set('DEMOBPANELVIEW_DEFAULT', AppDefaultMobPanelViewLayoutComponent);
    this.layoutMap.set('DEMOBCUSTOMVIEW_DEFAULT', AppDefaultMobCustomViewLayoutComponent);
    this.layoutMap.set('DEMOBPICKUPVIEW_DEFAULT', AppDefaultMobPickupViewLayoutComponent);
    this.layoutMap.set('DEMOBMPICKUPVIEW_DEFAULT', AppDefaultMobMPickupViewLayoutComponent);
    this.layoutMap.set('DEMOBPICKUPMDVIEW_DEFAULT', AppDefaultMobPickUpMDViewLayoutComponent);
    this.layoutMap.set('DEMOBCALENDARVIEW_DEFAULT', AppDefaultMobCalendarViewLayoutComponent);
    this.layoutMap.set('DEMOBTREEVIEW_DEFAULT', AppDefaultMobTreeViewLayoutComponent);
    this.layoutMap.set('DEMOBMAPVIEW_DEFAULT', AppDefaultMobMapViewLayoutComponent);
    this.layoutMap.set('DEMOBCHARTVIEW_DEFAULT', AppDefaultMobChartViewLayoutComponent);
    this.layoutMap.set('DEMOBHTMLVIEW_DEFAULT', AppDefaultMobHtmlViewLayoutComponent);
    this.layoutMap.set('DEMOBPICKUPTREEVIEW_DEFAULT', AppDefaultMobPickupTreeViewLayoutComponent);
    this.layoutMap.set('DEMOBTABEXPVIEW_DEFAULT', AppDefaultMobTabExpViewLayoutComponent);
    this.layoutMap.set('DEMOBMEDITVIEW9_DEFAULT', AppDefaultMobMEditViewLayoutComponent);
    this.layoutMap.set('DEMOBPORTALVIEW_DEFAULT', AppDefaultMobDEDashboardViewLayoutComponent);
    this.layoutMap.set('DEMOBWFDYNAEDITVIEW_DEFAULT', AppDefaultMobWFDynaEditViewLayoutComponent);
    this.layoutMap.set('DEMOBWFDYNAACTIONVIEW_DEFAULT', AppDefaultMobWFDynaActionViewLayoutComponent);
    this.layoutMap.set('DEMOBLISTEXPVIEW_DEFAULT', AppDefaultMobListExpViewLayoutComponent);
    this.layoutMap.set('DEMOBOPTVIEW_DEFAULT', AppDefaultMobOptViewLayoutComponent);
    this.layoutMap.set('DEMOBWIZARDVIEW_DEFAULT', AppDefaultMobWizardViewLayoutComponent);
    this.layoutMap.set('DEMOBCHARTEXPVIEW_DEFAULT', AppDefaultMobChartExpViewLayoutComponent);
    this.layoutMap.set('DEMOBTREEEXPVIEW_DEFAULT', AppDefaultMobTreeExpViewLayoutComponent);
    this.layoutMap.set('DEMOBMAPEXPVIEW_DEFAULT', AppDefaultMobMapExpViewLayoutComponent);
    this.layoutMap.set('DEMOBCALENDAREXPVIEW_DEFAULT', AppDefaultMobCalendarExpViewLayoutComponent);
  }

  /**
   * @description 获取布局组件
   * @param {string} viewType 布局类型
   * @param {string} viewStyle 布局样式
   * @param {string} [pluginCode] 插件代码
   * @return {string}
   * @memberof AppComponentService
   */
  public getLayoutComponent(viewType: string, viewStyle: string, pluginCode?: string): any {
    return this.layoutMap.get(`${viewType}_${viewStyle}`);
  }

  /**
   * @description 注册视图类型组件
   * @return {*}
   * @memberof AppComponentService
   */
  protected registerViewTypeComponents() {
    this.viewTypeMap.set('APPINDEXVIEW_DEFAULT', AppIndexViewComponent);
    this.viewTypeMap.set('APPPORTALVIEW_DEFAULT', AppPortalViewComponent);
    this.viewTypeMap.set('DEMOBEDITVIEW_DEFAULT', AppMobEditViewComponent);
    this.viewTypeMap.set('DEMOBMDVIEW_DEFAULT', AppMobMDViewComponent);
    this.viewTypeMap.set('DEMOBPANELVIEW_DEFAULT', AppMobPanelViewComponent);
    this.viewTypeMap.set('DEMOBCUSTOMVIEW_DEFAULT', AppMobCustomViewComponent);
    this.viewTypeMap.set('DEMOBPICKUPVIEW_DEFAULT', AppMobPickUpViewComponent);
    this.viewTypeMap.set('DEMOBMPICKUPVIEW_DEFAULT', AppMobMPickUpViewComponent);
    this.viewTypeMap.set('DEMOBPICKUPMDVIEW_DEFAULT', AppMobPickUpMDViewComponent);
    this.viewTypeMap.set('DEMOBCALENDARVIEW_DEFAULT', AppMobCalendarViewComponent);
    this.viewTypeMap.set('DEMOBTREEVIEW_DEFAULT', AppMobTreeViewComponent);
    this.viewTypeMap.set('DEMOBMAPVIEW_DEFAULT', AppMobMapViewComponent);
    this.viewTypeMap.set('DEMOBCHARTVIEW_DEFAULT', AppMobChartViewComponent);
    this.viewTypeMap.set('DEMOBHTMLVIEW_DEFAULT', AppMobHtmlViewComponent);
    this.viewTypeMap.set('DEMOBPICKUPTREEVIEW_DEFAULT', AppMobPickupTreeViewComponent);
    this.viewTypeMap.set('DEMOBTABEXPVIEW_DEFAULT', AppMobTabExpViewComponent);
    this.viewTypeMap.set('DEMOBMEDITVIEW9_DEFAULT', AppMobMEditViewComponent);
    this.viewTypeMap.set('DEMOBPORTALVIEW_DEFAULT', AppMobDEDashboardViewComponent);
    this.viewTypeMap.set('DEMOBWFDYNAEDITVIEW_DEFAULT', AppMobWFDynaEditViewComponent);
    this.viewTypeMap.set('DEMOBWFDYNAACTIONVIEW_DEFAULT', AppMobWFDynaActionViewComponent);
    this.viewTypeMap.set('DEMOBLISTEXPVIEW_DEFAULT', AppMobListExpViewComponent);
    this.viewTypeMap.set('DEMOBOPTVIEW_DEFAULT', AppMobOptViewComponent);
    this.viewTypeMap.set('DEMOBWIZARDVIEW_DEFAULT', AppMobWizardViewComponent);
    this.viewTypeMap.set('DEMOBCHARTEXPVIEW_DEFAULT', AppMobChartExpViewComponent);
    this.viewTypeMap.set('DEMOBTREEEXPVIEW_DEFAULT', AppMobTreeExpViewComponent);
    this.viewTypeMap.set('DEMOBMAPEXPVIEW_DEFAULT', AppMobMapExpViewComponent);
    this.viewTypeMap.set('DEMOBCALENDAREXPVIEW_DEFAULT', AppMobCalendarExpViewComponent);
  }

  /**
   * @description 获取视图类型组件
   * @param {string} viewType 视图类型
   * @param {string} viewStyle 视图样式
   * @param {string} [pluginCode] 插件代码
   * @return {string}
   * @memberof AppComponentService
   */
  public getViewTypeComponent(viewType: string, viewStyle: string = 'DEFAULT', pluginCode?: string): any {
    let component = AppMobNotSupportedViewComponent;
    if (pluginCode) {
      component = this.viewTypeMap.get(`${viewType}_${pluginCode}`);
    } else {
      component = this.viewTypeMap.get(`${viewType}_${viewStyle}`);
    }
    return component || AppMobNotSupportedViewComponent;
  }

  /**
   * @description 注册部件组件
   * @return {*}
   * @memberof AppComponentService
   */
  protected registerControlComponents() {
    this.controlMap.set('APPMENU_DEFAULT', AppMobMenuComponent);
    this.controlMap.set('APPMENU_ICONVIEW', AppMobMenuComponent);
    this.controlMap.set('APPMENU_LISTVIEW', AppMobMenuComponent);
    this.controlMap.set('TOOLBAR_DEFAULT', AppMobToolbarComponent);
    this.controlMap.set('FORM_DEFAULT', AppMobFormComponent);
    this.controlMap.set('SEARCHFORM_DEFAULT', AppMobSearchFormComponent);
    this.controlMap.set('MOBMDCTRL_LISTVIEW', AppMobMDCtrlComponent);
    this.controlMap.set('MOBMDCTRL_ICONVIEW', AppMobMDCtrlComponent);
    this.controlMap.set('LIST_DEFAULT', AppMobMDCtrlComponent);
    this.controlMap.set('PANEL_DEFAULT', AppMobPanelComponent);
    this.controlMap.set('CALENDAR_DEFAULT', AppMobCalendarComponent);
    this.controlMap.set('PICKUPVIEWPANEL_DEFAULT', AppMobPickUpViewPanelComponent);
    this.controlMap.set('TREEVIEW_DEFAULT', AppMobTreeComponent);
    this.controlMap.set('MAP_DEFAULT', AppMobMapComponent);
    this.controlMap.set('CHART_DEFAULT', AppMobChartComponent);
    this.controlMap.set('CONTEXTMENU_DEFAULT', AppMobContextMenuComponent);
    this.controlMap.set('TABEXPPANEL_DEFAULT', AppMobTabExpPanelComponent);
    this.controlMap.set('TABVIEWPANEL_DEFAULT', AppMobTabViewPanelComponent);
    this.controlMap.set('MULTIEDITVIEWPANEL_DEFAULT', AppMobMEditViewPanelComponent);
    this.controlMap.set('DASHBOARD_DEFAULT', AppMobDashboardComponent);
    this.controlMap.set('PORTLET_DEFAULT', AppMobPortletComponent);
    this.controlMap.set('LISTEXPBAR_DEFAULT', AppMobListExpBarComponent);
    this.controlMap.set('WIZARDPANEL_DEFAULT', AppMobWizardPanelComponent);
    this.controlMap.set('WIZARDPANEL_STATE', AppMobStateWizardPanelComponent);
    this.controlMap.set('CHARTEXPBAR_DEFAULT', AppMobChartExpBarComponent);
    this.controlMap.set('TREEEXPBAR_DEFAULT', AppMobTreeExpBarComponent);
    this.controlMap.set('MAPEXPBAR_DEFAULT', AppMobMapExpBarComponent);
    this.controlMap.set('CALENDAREXPBAR_DEFAULT', AppMobCalendarExpBarComponent);
  }

  /**
   * @description 获取部件组件
   * @param {string} ctrlType 部件类型
   * @param {string} ctrlStyle 部件样式
   * @param {string} [pluginCode] 插件标识
   * @return {string}
   * @memberof AppComponentService
   */
  public getControlComponent(ctrlType: string, ctrlStyle: string = 'DEFAULT', pluginCode?: string): any {
    let component = AppMobNotSupportedControlComponent;
    if (pluginCode) {
      component = this.controlMap.get(`${ctrlType}_${pluginCode}`);
    } else {
      component = this.controlMap.get(`${ctrlType}_${ctrlStyle}`);
    }
    return component || AppMobNotSupportedControlComponent;
  }

  /**
   * @description 注册编辑器组件
   * @protected
   * @memberof AppComponentService
   */
  protected registerEditorComponents() {
    this.editorMap.set('MOBTEXT_DEFAULT', AppTextboxEditorComponent);
    this.editorMap.set('MOBNUMBER_DEFAULT', AppTextboxEditorComponent);
    this.editorMap.set('MOBTEXTAREA_DEFAULT', AppTextboxEditorComponent);
    this.editorMap.set('MOBPASSWORD_DEFAULT', AppTextboxEditorComponent);
    this.editorMap.set('MOBSWITCH_DEFAULT', AppSwitchEditorComponent);
    this.editorMap.set('MOBSLIDER_DEFAULT', AppSliderEditorComponent);
    this.editorMap.set('MOBRADIOLIST_DEFAULT', AppCheckBoxEditorComponent);
    this.editorMap.set('MOBDROPDOWNLIST_DEFAULT', AppDropdownListEditorComponent);
    this.editorMap.set('MOBCHECKLIST_DEFAULT', AppDropdownListEditorComponent);
    this.editorMap.set('SPAN_DEFAULT', AppSpanEditorComponent);
    this.editorMap.set('MOBPICKER_DEFAULT', AppDataPickerEditorComponent);
    this.editorMap.set('MOBMPICKER_DEFAULT', AppDataPickerEditorComponent);
    this.editorMap.set('MOBPICKER_DROPDOWNVIEW_DEFAULT', AppDataPickerEditorComponent);
    this.editorMap.set('MOBDATE_DEFAULT', AppDatePickerEditorComponent);
    this.editorMap.set('MOBSINGLEFILEUPLOAD_DEFAULT', AppUploadEditorComponent);
    this.editorMap.set('MOBMULTIFILEUPLOAD_DEFAULT', AppUploadEditorComponent);
    this.editorMap.set('MOBPICTURE_DEFAULT', AppUploadEditorComponent);
    this.editorMap.set('MOBPICTURELIST_DEFAULT', AppUploadEditorComponent);
    this.editorMap.set('MOBRATING_DEFAULT', AppRatingEditorComponent);
    this.editorMap.set('MOBSTEPPER_DEFAULT', AppStepperEditorComponent);
    this.editorMap.set('MOBHTMLTEXT_DEFAULT', AppRichTextEditorComponent);
    // 精确分钟
    this.editorMap.set('MOBDATE_Auto1', AppDatePickerEditorComponent);
    // 只有小时分钟
    this.editorMap.set('MOBDATE_Auto3', AppDatePickerEditorComponent);
    // 精确小时
    this.editorMap.set('MOBDATE_Auto8', AppDatePickerEditorComponent);
    // 精确天
    this.editorMap.set('MOBDATE_Auto6', AppDatePickerEditorComponent);
    // 电子签名
    this.editorMap.set('MOBPICTURE_DZQM', AppUploadEditorComponent);
  }

  /**
   * @description 获取编辑器组件
   * @param {string} editorType 编辑器类型
   * @param {string} editorStyle 编辑器样式
   * @param {string} [pluginCode] 编辑器插件标识
   * @return {string}
   * @memberof AppComponentService
   */
  public getEditorComponent(
    editorType: string,
    editorStyle: string = 'DEFAULT',
    pluginCode?: string,
    infoMode: boolean = false,
  ): any {
    let component: any = AppMobNotSupportedEditorComponent;
    if (pluginCode) {
      component = this.editorMap.get(`${pluginCode}`);
    } else {
      if (infoMode) {
        component = this.computeInfoModeEditor(editorType, editorStyle);
      } else {
        component = this.editorMap.get(`${editorType}_${editorStyle}`);
      }
    }
    return component || AppMobNotSupportedEditorComponent;
  }

  /**
   * @description 计算信息模式编辑器
   * @param {string} editorType 编辑器类型
   * @param {string} editorStyle 编辑器样式
   * @return {*}  {string}
   * @memberof AppComponentService
   */
  public computeInfoModeEditor(editorType: string, editorStyle: string): any {
    //  TODO目前返回span，等待后续编辑器补充
    let component = AppSpanEditorComponent;
    return component;
  }
}
