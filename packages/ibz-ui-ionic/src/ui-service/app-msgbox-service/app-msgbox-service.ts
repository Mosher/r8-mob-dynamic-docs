import { Subject } from 'rxjs';
import { alertController } from '@ionic/vue';
import { IParam, IMsgboxService, IMsgboxOptions, Util, LogUtil } from 'ibz-core';

/**
 * @description 消息弹框服务
 * @export
 * @class AppMsgboxService
 * @implements {IMsgboxService}
 */
export class AppMsgboxService implements IMsgboxService {
  /**
   * @description 消息弹框服务实例对象
   * @private
   * @static
   * @type {AppMsgboxService}
   * @memberof AppMsgboxService
   */
  private static instance: AppMsgboxService;

  /**
   * @description 消息订阅
   * @private
   * @type {Subject<string>}
   * @memberof AppMsgboxService
   */
  private subject: Subject<string> = new Subject<string>();

  /**
   * @description 弹框按钮
   * @private
   * @type {any[]}
   * @memberof AppMsgboxService
   */
  private buttons: any[] = [];

  /**
   * @description 弹框按钮模型
   * @private
   * @type {any[]}
   * @memberof AppMsgboxService
   */
  private buttonModel: any[] = [
    { text: '确认', cssClass: 'app-msgbox-ok', handler: () => this.itemClick('ok') },
    { text: '是', cssClass: 'app-msgbox-yes', handler: () => this.itemClick('yes') },
    { text: '否', cssClass: 'app-msgbox-no', handler: () => this.itemClick('no') },
    { text: '取消', cssClass: 'app-msgbox-cancel', handler: () => this.itemClick('cancel') },
  ];

  /**
   * @description 消息弹框样式数组
   * @private
   * @type {string[]}
   * @memberof AppMsgboxService
   */
  private cssClass: string[] = ['message-container'];

  /**
   * @description 获取唯一实列
   * @static
   * @return {*}  {AppMsgboxService}
   * @memberof AppMsgboxService
   */
  public static getInstance(): AppMsgboxService {
    if (!this.instance) {
      this.instance = new AppMsgboxService();
    }
    return this.instance;
  }

  /**
   * @description 打开消息弹框
   * @param {IMsgboxOptions} options 消息弹框配置
   * @return {*}  {(Subject<string>)}
   * @memberof AppMsgboxService
   */
  public open(options: IMsgboxOptions): Subject<string> {
    this.openMsgbox(options);
    return this.subject;
  }

  /**
   * @description 消息弹框按钮点击
   * @private
   * @param {string} value
   * @memberof AppMsgboxService
   */
  private itemClick(value: string) {
    this.subject.next(value);
  }

  /**
   * @description 打开消息弹框
   * @private
   * @param {IParam} options 消息弹框配置
   * @return {*}  {Promise<any>}
   * @memberof AppMsgboxService
   */
  private async openMsgbox(options: IMsgboxOptions): Promise<any> {
    if (options.buttons && options.buttons.length > 0) {
      this.buttons = Util.deepCopy(options.buttons);
    } else {
      this.initButtonModel(options.buttonType);
    }
    this.clacMsgboxClass(options);
    try {
      const alert = await alertController.create({
        header: options.title,
        message: options.content,
        cssClass: this.cssClass,
        buttons: this.buttons,
      });
      await alert.present();
    } catch (error) {
      LogUtil.error(error);
    }
  }

  /**
   * @description 计算消息弹框样式
   * @private
   * @param {IParam} options
   * @memberof AppMsgboxService
   */
  private clacMsgboxClass(options: IParam) {
    if (options.type) {
      this.cssClass.push(`ion-${options.type}`);
    }
    if (options.showMode) {
      this.cssClass.push(options.showMode);
    }
    if (options.cssClass) {
      this.cssClass.push(options.cssClass);
    }
  }

  /**
   * @description
   * @private
   * @param {string} buttonType 按钮类型
   * @memberof AppMsgboxService
   */
  private initButtonModel(buttonType: string = '') {
    this.buttons = [];
    switch (buttonType) {
      case 'okcancel':
        this.buttons.push(this.buttonModel[0], this.buttonModel[3]);
        break;
      case 'yesno':
        this.buttons.push(this.buttonModel[1], this.buttonModel[2]);
        break;
      case 'yesnocancel':
        this.buttons.push(this.buttonModel[1], this.buttonModel[2], this.buttonModel[3]);
        break;
      case 'ok':
        this.buttons.push(this.buttonModel[0]);
        break;
      default:
        this.buttons.push(this.buttonModel[0], this.buttonModel[3]);
        break;
    }
  }
}
