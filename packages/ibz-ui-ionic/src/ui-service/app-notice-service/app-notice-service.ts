import { toastController } from '@ionic/core';
import { IAppNoticeService } from 'ibz-core';

/**
 * 消息提示
 *
 * @export
 * @class AppNoticeService
 */
export class AppNoticeService implements IAppNoticeService {
  /**
   * 唯一实例
   *
   * @private
   * @static
   * @type {AppNoticeService}
   * @memberof AppNoticeService
   */
  private static readonly instance: AppNoticeService = new AppNoticeService();

  /**
   * Creates an instance of AppNoticeService.
   * @memberof AppNoticeService
   */
  constructor() {
    if (AppNoticeService.instance) {
      return AppNoticeService.instance;
    }
  }

  /**
   * 消息提示
   *
   * @param {string} message
   * @param {number} [time]
   * @memberof AppNoticeService
   */
  public info(message: string, time?: number): void {
    const type = 'secondary';
    this.createToast(type, message, time);
  }

  /**
   * 成功提示
   *
   * @param {string} message
   * @param {number} [time]
   * @memberof AppNoticeService
   */
  public success(message: string, time?: number): void {
    const type = 'success';
    this.createToast(type, message, time);
  }

  /**
   * 警告提示
   *
   * @param {string} message
   * @param {number} [time]
   * @memberof AppNoticeService
   */
  public warning(message: string, time?: number): void {
    const type = 'warning';
    this.createToast(type, message, time);
  }

  /**
   * 错误提示
   *
   * @param {string} message
   * @param {number} [time]
   * @memberof AppNoticeService
   */
  public error(message: string, time?: number): void {
    const type = 'danger';
    this.createToast(type, message, time);
  }

  /**
   * 创建对象
   *
   * @private
   * @param {string} type
   * @param {string} message
   * @param {number} [time]
   * @memberof AppNoticeService
   */
  private async createToast(type: string, message: string, time?: number) {
    const toast = await toastController.create({
      position: 'top',
      color: type ? type : 'primary',
      duration: time ? time : 2000,
      message: message,
    });
    await toast.present();
  }

  /**
   * 获取实例
   *
   * @static
   * @returns {AppNoticeService}
   * @memberof AppNoticeService
   */
  public static getInstance(): AppNoticeService {
    return this.instance;
  }
}
