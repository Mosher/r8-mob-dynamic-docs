import { IContext, IParam, IViewOpenService } from 'ibz-core';
import { Subject } from 'rxjs';
import { Router } from 'vue-router';
import { AppDrawerService } from '../app-drawer-service/app-drawer-service';
import { AppModalService } from '../app-modal-service/app-modal-service';

export class AppViewOpenService implements IViewOpenService {
  /**
   * 单例对象
   *
   * @private
   * @static
   * @type {AppViewOpenService}
   * @memberof AppViewOpenService
   */
  private static instance: AppViewOpenService;

  /**
   * 路由
   *
   * @private
   * @type {(Router | any)}
   * @memberof AppViewOpenService
   */
  private router: any | Router;

  /**
   * 单例构造器
   *
   * @static
   * @return {*}
   * @memberof AppViewOpenService
   */
  public static getInstance(router?: any) {
    if (!this.instance) {
      this.instance = new AppViewOpenService();
      this.instance.router = router;
    }
    return this.instance;
  }

  /**
   * @description 打开模态
   * @param {({ viewComponent?: any | undefined, viewModel?: IParam, customClass?: string | undefined, customStyle?: IParam })} view
   * @param {IContext} [context] 应用上下文
   * @param {IParam} [navParam] 导航参数
   * @param {*} [navDatas] 数据
   * @param {IParam} [otherParam] 额外参数
   * @return {*}  {Subject<{ret: boolean, datas?: IParam[]}>}
   * @memberof AppViewOpenService
   */
  openModal(
    view: {
      viewComponent?: any | undefined;
      viewModel?: IParam;
      customClass?: string | undefined;
      customStyle?: IParam;
    },
    context?: IContext,
    navParam?: IParam,
    navDatas?: any,
    otherParam?: IParam,
  ): Subject<{ ret: boolean; datas?: IParam[] }> {
    return AppModalService.getInstance().openModal(view, context, navParam, navDatas, otherParam);
  }

  /**
   * 路由打开
   *
   * @param {*} opts
   * @returns {Promise<any>}
   * @memberof AppViewOpenService
   */
  public openView(path: string): void {
    if (path) {
      this.router.push(path);
    }
  }

  /**
   * 浏览器新标签页打开
   *
   * @param {string} url
   * @returns {Promise<any>}
   * @memberof AppViewOpenService
   */
  public openPopupApp(url: string): void {
    window.open(url, '_blank');
  }

  /**
   * @description 打开抽屉
   * @param {({ viewComponent?: any | undefined, viewModel?: IParam, customClass?: string | undefined, customStyle?: IParam })} view
   * @param {IContext} [context] 应用上下文
   * @param {IParam} [navParam] 导航参数
   * @param {*} [navDatas] 数据
   * @param {IParam} [otherParam] 额外参数
   * @return {*}  {Subject<{ret: boolean, datas?: IParam[]}>}
   * @memberof AppViewOpenService
   */
  openDrawer(
    view: {
      viewComponent?: any | undefined;
      viewModel?: IParam;
      customClass?: string | undefined;
      customStyle?: IParam;
    },
    context?: IContext,
    navParam?: IParam,
    navDatas?: any,
    otherParam?: IParam,
  ): Subject<{ ret: boolean; datas?: IParam[] }> {
    return AppDrawerService.getInstance().openDrawer(view, context, navParam, navDatas, otherParam);
  }
}
