import { Http } from 'ibz-core';
const wx = require('weixin-js-sdk');
/**
 * 微信服务
 *
 * @export
 * @class WeChatService
 */
export class WeChatService {
  /**
   * 唯一实例
   *
   * @private
   * @static
   * @type {WeChatService}
   * @memberof WeChatService
   */
  private static instance: WeChatService;

  /**
   * 用户信息缓存key
   *
   * @private
   * @type {string}
   * @memberof WeChatService
   */
  private readonly infoName: string = '';

  /**
   * 企业corpId
   *
   * @private
   * @type {string}
   * @memberof WeChatService
   */
  private readonly appId: string = '';

  /**
   * 微信SDK
   *
   * @protected
   * @type {*}
   * @memberof WeChatService
   */
  protected wx: any = wx;

  /**
   * http请求服务
   *
   * @protected
   * @memberof ThirdPartyService
   */
  protected http = Http.getInstance();

  /**
   * 是否初始化
   *
   * @protected
   * @type {boolean}
   * @memberof WeChatService
   */
  protected $isInit: boolean = false;
  public get isInit(): boolean {
    return this.$isInit;
  }
  /**
   * Creates an instance of WeChatService.
   * @memberof WeChatService
   */
  private constructor() {
    this.init();
  }

  /**
   * 获取实例
   *
   * @static
   * @returns {WeChatService}
   * @memberof WeChatService
   */
  public static getInstance(): WeChatService {
    if (!this.instance) {
      this.instance = new WeChatService();
    }
    return WeChatService.instance;
  }

  /**
   * 是否是小程序
   *
   * @memberof WeChatService
   */
  public async isMini() {
    return new Promise((resolve: any) => {
      wx.miniProgram.getEnv((res: any) => {
        if (res.miniprogram) {
          resolve(true); //小程序
        } else {
          resolve(false); //在公众号
        }
      });
    });
  }

  /**
   * 初始化
   *
   * @protected
   * @returns {Promise<void>}
   * @memberof WeChatService
   */
  protected async init(): Promise<void> {
    // if (this.wx) {
    //     const data: any = await this.http.get('./wechat/config?url=' + encodeURIComponent(window.location.origin));
    //     if (data) {
    //         const config = {
    //             beta: true,// 必须这么写，否则wx.invoke调用形式的jsapi会有问题
    //             debug: true, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
    //             appId: data.appid, // 必填，企业微信的corpID
    //             timestamp: data.timestamp, // 必填，生成签名的时间戳
    //             nonceStr: data.nonceStr, // 必填，生成签名的随机串
    //             signature: data.signature,// 必填，签名，见 附录-JS-SDK使用权限签名算法
    //             jsApiList: [] // 必填，需要使用的JS接口列表，凡是要调用的接口都需要传进来
    //         }
    //         this.wx.config(config);
    //         this.wx.ready(() => {
    //             this.$isInit = true;
    //             alert('微信初始化成功');
    //         });
    //         this.wx.error((res: any) => {
    //             // alert('微信信息初始化失败!---' + JSON.stringify(res));
    //         });
    //     }
    this.$isInit = true;
  }

  /**
   * 登录
   *
   * @returns {Promise<any>}
   * @memberof WeChatService
   */
  public async login(): Promise<any> {
    const userInfo = this.getUserInfo();
    // if (!userInfo) {
    //     const code = this.getQueryValue1('code');
    //     if (code && !Object.is(code, '')) {
    //         const res = await this.http.get(`./wechat/login?code=${code}`);
    //         if (res) {
    //             window.localStorage.setItem(this.infoName, res);
    //             userInfo = res;
    //         }
    //     } else {
    //         window.location.href = `https://open.weixin.qq.com/connect/oauth2/authorize?appid=${this.appId}&redirect_uri=${encodeURIComponent(window.location.href)}&response_type=code&scope=snsapi_base&state==#wechat_redirect`;
    //     }
    // }
    return userInfo;
  }

  /**
   * 清楚登录用户信息
   *
   * @memberof WeChatService
   */
  public clearUserInfo(): void {
    window.localStorage.removeItem(this.infoName);
  }

  /**
   * 获取用户信息
   *
   * @returns {*}
   * @memberof WeChatService
   */
  public getUserInfo(): any {
    return window.localStorage.getItem(this.infoName);
  }

  /**
   * 获取url参数
   *
   * @private
   * @param {string} queryName
   * @returns
   * @memberof WeChatService
   */
  private getQueryValue1(queryName: string) {
    const reg = new RegExp('(^|&)' + queryName + '=([^&]*)(&|$)', 'i');
    const r = window.location.search.substr(1).match(reg);
    if (r != null) {
      return decodeURI(r[2]);
    } else {
      return null;
    }
  }

  /**
   * 关闭微信应用
   *
   * @static
   * @memberof WeChatService
   */
  public close() {
    wx.closeWindow();
  }

  /**
   * 设置微信标题
   *
   * @static
   * @memberof WeChatService
   */
  public setTitle(title: string) {
    // 设置标题
  }

  /**
   * 微信导航栏返回事件
   *
   * @static
   * @memberof WeChatService
   */
  private backEvent: Function = () => {};

  /**
   * 设置微信导航栏返回事件
   *
   * @static
   * @memberof WeChatService
   */
  public setBackEvent(event: Array<Function>) {}

  /**
   *
   * @static
   * @memberof WeChatService
   */
  public getNetworkType() {
    wx.getNetworkType({
      success: (res: any) => {
        alert(res.networkType);
      },
    });
  }

  /**
   * wx事件
   *
   * @param {string} tag
   * @param {*} [arg={}]
   * @memberof WeChatService
   */
  public event(tag: string, arg: any = {}) {
    if (Object.is(tag, 'close')) {
      return this.close();
    }
    if (Object.is(tag, 'getNetworkType')) {
      return this.getNetworkType();
    }
  }
}
