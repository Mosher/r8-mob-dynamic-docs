import { IAppThirdPartyService } from 'ibz-core';
import { DingTalkService } from './ding-talk-service';
import { WeChatService } from './we-chat-service';

/**
 * 第三方服务
 *
 * @export
 * @class AppThirdPartyService
 */
export class AppThirdPartyService implements IAppThirdPartyService {
  /**
   * 唯一实例
   *
   * @private
   * @static
   * @type {AppThirdPartyService}
   * @memberof AppThirdPartyService
   */
  private static instance: AppThirdPartyService;

  /**
   * 当前搭载平台服务类
   *
   * @type {(DingTalkService|WeChatService)}
   * @memberof AppThirdPartyService
   */
  private platformService!: DingTalkService | WeChatService;

  /**
   * 搭载平台
   *
   * @private
   * @type {string}
   * @memberof AppThirdPartyService
   */
  private platform!: 'ios' | 'android' | 'dd' | 'wx' | 'desktop' | 'web';

  /**
   * Creates an instance of AppThirdPartyService.
   * @memberof AppThirdPartyService
   */
  private constructor() {
    this.thirdPartyInit();
  }

  /**
   *  第三方初始化
   *
   * @memberof AppThirdPartyService
   */
  public thirdPartyInit() {
    this.initAppPlatForm();
    this.initPlatformService();
  }

  /**
   * 获取实例
   *
   * @static
   * @return {*}  {AppThirdPartyService}
   * @memberof AppThirdPartyService
   */
  public static getInstance(): AppThirdPartyService {
    if (!this.instance) {
      this.instance = new AppThirdPartyService();
    }
    return AppThirdPartyService.instance;
  }

  /**
   * 初始化搭载平台
   *
   * @memberof AppThirdPartyService
   */
  private initPlatformService() {
    if (Object.is(this.platform, 'dd')) {
      this.platformService = DingTalkService.getInstance();
    } else if (Object.is(this.platform, 'wx')) {
      this.platformService = WeChatService.getInstance();
    }
  }

  /**
   * 初始化搭载平台
   *
   * @memberof AppThirdPartyService
   */
  public initAppPlatForm() {
    let platform: 'ios' | 'android' | 'dd' | 'wx' | 'desktop' | 'web' = 'web';
    const info: string = window.navigator.userAgent.toUpperCase();
    // 钉钉
    if (info.indexOf('DINGTALK') !== -1) {
      platform = 'dd';
    }
    // 桌面端
    else if (info.indexOf('ELECTRON') !== -1) {
      platform = 'desktop';
    }
    // 微信 (小程序或者公众号)
    else if (info.indexOf('MICROMESSENGER') !== -1) {
      platform = 'wx';
    }
    // 安卓应用
    else if (info.indexOf('ANDROID') !== -1) {
      platform = 'android';
    }
    // 苹果应用 (未测试)
    // else if (info.indexOf('IPHONE') !== -1) {
    //     platform = 'ios';
    // }
    this.platform = platform;
  }

  /**
   * 获取APP搭载平台
   *
   * @memberof AppThirdPartyService
   */
  public async getAppPlatform(): Promise<'ios' | 'android' | 'dd' | 'wx-mini' | 'wx-pn' | 'desktop' | 'web'> {
    let platform: 'ios' | 'android' | 'dd' | 'wx-mini' | 'wx-pn' | 'desktop' | 'web';
    if (this.platform === 'wx') {
      const isMini = await (this.platformService as WeChatService).isMini();
      platform = isMini ? 'wx-mini' : 'wx-pn';
    } else {
      platform = this.platform;
    }
    return platform;
  }

  /**
   * 获取当前搭载平台服务类
   *
   * @return {*}
   * @memberof AppThirdPartyService
   */
  public getPlatformService(): DingTalkService | WeChatService {
    return this.platformService;
  }

  /**
   * 清楚登录用户信息
   *
   * @memberof AppThirdPartyService
   */
  public clearUserInfo(): void {
    return this.platformService.clearUserInfo();
  }

  /**
   * 获取用户信息
   *
   * @returns {*}
   * @memberof AppThirdPartyService
   */
  public async getUserInfo(): Promise<any> {
    return this.platformService.getUserInfo();
  }

  /**
   * 关闭应用
   *
   * @memberof AppThirdPartyService
   */
  public close() {
    this.platformService.close();
  }

  /**
   * 第三方事件
   *
   * @param {string} tag
   * @param {*} [arg={}]
   * @return {*}  {Promise<any>}
   * @memberof AppThirdPartyService
   */
  public async thirdPartyEvent(tag: string, arg: any = {}): Promise<any> {
    return await this.platformService.event(tag, arg);
  }
}
