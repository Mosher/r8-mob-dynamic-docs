import { IUIApp } from 'ibz-core';

declare global {
  interface Window {
    App: IUIApp;
  }
  const App: IUIApp;
}
