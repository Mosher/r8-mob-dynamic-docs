import { renderSlot } from 'vue';
import { IPSAppDEPickupView } from '@ibiz/dynamic-model-api';
import { AppMobMPickUpViewLayoutProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { LayoutComponentBase } from './layout-component-base';

export class AppDefaultMobMPickupViewLayout extends LayoutComponentBase<AppMobMPickUpViewLayoutProps> {
  /**
   * @description 多数据选择视图实例对象
   * @type {IPSAppDEPickupView}
   * @memberof AppDefaultMobMPickupViewLayout
   */
  public viewInstance!: IPSAppDEPickupView;

  /**
   * @description 渲染视图头
   * @return {*}  {*}
   * @memberof AppDefaultMobMPickupViewLayout
   */
  renderViewHeader(): any {
    return <ion-header>{renderSlot(this.ctx.slots, 'headerButtons')}</ion-header>;
  }

  /**
   * @description 视图主容器内容
   * @return {*}  {any}
   * @memberof AppDefaultMobMPickupViewLayout
   */
  renderViewMainContainerContent(): any {
    return <div class='view-main-container-content'>{[renderSlot(this.ctx.slots, 'pickupviewpanel')]}</div>;
  }
}

//  移动端数据选择视图布局面板
export const AppDefaultMobMPickupViewLayoutComponent = GenerateComponent(
  AppDefaultMobMPickupViewLayout,
  new AppMobMPickUpViewLayoutProps(),
);
