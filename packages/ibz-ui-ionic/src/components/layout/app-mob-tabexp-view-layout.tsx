import { renderSlot } from 'vue';
import { IPSAppDEMobTabExplorerView } from '@ibiz/dynamic-model-api';
import { AppMobTabExpViewLayoutProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { LayoutComponentBase } from './layout-component-base';

/**
 * 移动端分页导航视图视图布局面板
 *
 * @class AppDefaultMobTabExpViewLayout
 * @extends ComponentBase
 */
export class AppDefaultMobTabExpViewLayout extends LayoutComponentBase<AppMobTabExpViewLayoutProps> {
  /**
   * 移动端地图视图实例对象
   *
   * @public
   * @type {IPSAppDEMobTabExplorerView}
   * @memberof AppDefaultMobTabExpViewLayout
   */
  public viewInstance!: IPSAppDEMobTabExplorerView;

  /**
   * @description 视图主容器内容
   * @return {*}
   * @memberof AppDefaultMobTabExpViewLayout
   */
  renderViewMainContainerContent(): any {
    return <div class='view-main-container-content'>{[renderSlot(this.ctx.slots, 'tabexppanel')]}</div>;
  }
}

//  移动端多数据视图视图布局面板部件
export const AppDefaultMobTabExpViewLayoutComponent = GenerateComponent(
  AppDefaultMobTabExpViewLayout,
  new AppMobTabExpViewLayoutProps(),
);
