import { IPSAppDEMobEditView } from '@ibiz/dynamic-model-api';
import { renderSlot } from '@vue/runtime-dom';
import { AppMobEditViewLayoutProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { LayoutComponentBase } from './layout-component-base';

export class AppDefaultMobEditViewLayout extends LayoutComponentBase<AppMobEditViewLayoutProps> {
  /**
   * 移动端编辑视图实例对象
   *
   * @type {IPSAppDEMobEditView}
   * @memberof AppDefaultMobEditViewLayout
   */
  public viewInstance!: IPSAppDEMobEditView;

  /**
   * @description 视图主容器内容
   * @return {*}
   * @memberof AppDefaultMobEditViewLayout
   */
  renderViewMainContainerContent(): any {
    return <div class='view-main-container-content'>{[this.renderSlot('form')]}</div>;
  }
}
// 移动端实体编辑视图组件
export const AppDefaultMobEditViewLayoutComponent = GenerateComponent(
  AppDefaultMobEditViewLayout,
  new AppMobEditViewLayoutProps(),
);
