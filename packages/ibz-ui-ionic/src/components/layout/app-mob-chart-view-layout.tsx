import { renderSlot } from 'vue';
import { IPSAppDEMobChartView } from '@ibiz/dynamic-model-api';
import { AppMobChartViewLayoutProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { MultiDataViewLayoutComponentBase } from './multi-data-view-layout-component-base';
/**
 * 移动端图表视图视图布局面板
 *
 * @class AppDefaultMobChartViewLayout
 * @extends MultiDataViewLayoutComponentBase
 */
export class AppDefaultMobChartViewLayout extends MultiDataViewLayoutComponentBase<AppMobChartViewLayoutProps> {
  /**
   * @description 移动端图表视图实例对象
   * @type {IPSAppDEMobChartView}
   * @memberof AppDefaultMobChartViewLayout
   */
  public viewInstance!: IPSAppDEMobChartView;

  /**
   * @description 视图主容器内容
   * @return {*}  {*}
   * @memberof AppDefaultMobChartViewLayout
   */
  renderViewMainContainerContent(): any {
    return <div class='view-main-container-content'>{[renderSlot(this.ctx.slots, 'chart')]}</div>;
  }
}

//  移动端图表视图视图布局面板部件
export const AppDefaultMobChartViewLayoutComponent = GenerateComponent(
  AppDefaultMobChartViewLayout,
  new AppMobChartViewLayoutProps(),
);
