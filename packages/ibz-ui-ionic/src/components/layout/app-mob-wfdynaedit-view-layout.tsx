import { renderSlot } from 'vue';
import { IPSAppDEMobWFDynaEditView } from '@ibiz/dynamic-model-api';
import { AppMobWFDynaEditViewLayoutProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { LayoutComponentBase } from './layout-component-base';

/**
 * @description 移动端动态工作流编辑视图视图布局组件
 * @export
 * @class AppDefaultMobWFDynaEditViewLayout
 * @extends {AppDefaultDeView<AppMobWFDynaEditViewLayoutProps>}
 */
export class AppDefaultMobWFDynaEditViewLayout extends LayoutComponentBase<AppMobWFDynaEditViewLayoutProps> {
  /**
   * @description 移动端动态工作流编辑视图实例对象
   * @type {IPSAppDEMobWFDynaEditView}
   * @memberof AppDefaultMobWFDynaEditViewLayout
   */
  public viewInstance!: IPSAppDEMobWFDynaEditView;

  /**
   * @description 渲染部件
   * @return {*}
   * @memberof AppDefaultMobWFDynaEditViewLayout
   */
  public renderViewMainContainerContent(): any {
    return <div class='view-main-container-content'>{[renderSlot(this.ctx.slots, 'form')]}</div>;
  }
}
// 应用首页组件
export const AppDefaultMobWFDynaEditViewLayoutComponent = GenerateComponent(
  AppDefaultMobWFDynaEditViewLayout,
  new AppMobWFDynaEditViewLayoutProps(),
);
