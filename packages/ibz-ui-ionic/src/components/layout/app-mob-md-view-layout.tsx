import { renderSlot } from 'vue';
import { IPSAppDEMobMDView } from '@ibiz/dynamic-model-api';
import { AppMobMDViewLayoutProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { LayoutComponentBase } from './layout-component-base';
import { MultiDataViewLayoutComponentBase } from './multi-data-view-layout-component-base';

/**
 * 移动端多数据视图视图布局面板（DEFAULT样式）
 *
 * @class AppDefaultMobMDViewLayout
 * @extends MultiDataViewLayoutComponentBase
 */
export class AppDefaultMobMDViewLayout extends MultiDataViewLayoutComponentBase<AppMobMDViewLayoutProps> {
  /**
   * @description 移动端多数据视图实例对象
   * @type {IPSAppDEMobMDView}
   * @memberof AppDefaultMobMDViewLayout
   */
  public viewInstance!: IPSAppDEMobMDView;

  /**
   * @description 视图主容器内容
   * @return {*}  {any}
   * @memberof AppDefaultMobMDViewLayout
   */
  renderViewMainContainerContent(): any {
    return <div class='view-main-container-content'>{renderSlot(this.ctx.slots, 'mdctrl')}</div>;
  }
}

//  移动端多数据视图视图布局面板部件
export const AppDefaultMobMDViewLayoutComponent = GenerateComponent(
  AppDefaultMobMDViewLayout,
  new AppMobMDViewLayoutProps(),
);
