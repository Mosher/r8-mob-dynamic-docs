import { toRaw, renderSlot, ref, h } from 'vue';
import {
  IPSAppView,
  IPSEditor,
  IPSPanelContainer,
  IPSPanelCtrlPos,
  IPSPanelField,
  IPSPanelItem,
  IPSPanelRawItem,
  IPSPanelTabPage,
  IPSPanelTabPanel,
  IPSViewLayoutPanel,
} from '@ibiz/dynamic-model-api';
import { IEditorEventParam, IParam, AppLayoutProps, Util } from 'ibz-core';
import { ComponentBase } from '../component-base';
export class LayoutComponentBase<Props extends AppLayoutProps> extends ComponentBase<AppLayoutProps> {
  /**
   * @description 视图实例对象
   * @type {IPSAppView}
   * @memberof LayoutComponentBase
   */
  public viewInstance!: IPSAppView;

  /**
   * @description 应用上下文
   * @type {IParam}
   * @memberof LayoutComponentBase
   */
  public context!: IParam;

  /**
   * @description 视图导航参数
   * @type {IParam}
   * @memberof LayoutComponentBase
   */
  public viewParam!: IParam;

  /**
   * @description 视图导航数据
   * @type {IParam}
   * @memberof LayoutComponentBase
   */
  public navDatas!: IParam;

  /**
   * @description 是否显示视图标题栏
   * @type {boolean}
   * @memberof LayoutComponentBase
   */
  public isShowCaptionBar!: boolean;

  /**
   * @description 视图布局面板
   * @type {(IPSViewLayoutPanel | null)}
   * @memberof LayoutComponentBase
   */
  public viewLayoutPanel?: IPSViewLayoutPanel | null;

  /**
   * @description 当前选中分页
   * @type {*}
   * @memberof LayoutComponentBase
   */
  public currentTab: any = ref({});

  /**
   * @description 视图容器样式名
   * @readonly
   * @memberof LayoutComponentBase
   */
  get viewClass() {
    const viewClass = {
      'container-margin': true,
      'container-padding': true,
      'app-view': true,
      [this.viewInstance.viewType?.toLowerCase()]: true,
      [Util.srfFilePath2(this.viewInstance.codeName)]: true,
    };
    const sysCss = this.viewInstance.getPSSysCss();
    if (sysCss) {
      Object.assign(viewClass, {
        [sysCss.cssName]: true,
      });
    }
    return viewClass;
  }

  /**
   * @description 初始化
   * @memberof LayoutComponentBase
   */
  init() {
    this.viewInstance = toRaw(this.props.viewInstance) as IPSAppView;
    this.context = toRaw(this.props.navContext);
    this.viewParam = toRaw(this.props.navParam);
    this.navDatas = toRaw(this.props.navDatas);
    this.isShowCaptionBar = this.props.isShowCaptionBar;
    this.viewLayoutPanel = this.viewInstance.getPSViewLayoutPanel();
  }

  /**
   * @description 获取栅格布局
   * @param {*} parent 父容器
   * @param {*} child 子容器
   * @return {*}
   * @memberof LayoutComponentBase
   */
  public getGridLayoutProps(parent: any, child: any) {
    const layout = parent?.getPSLayout?.()?.layout;
    let { colXS, colSM, colMD, colLG, colXSOffset, colSMOffset, colMDOffset, colLGOffset } = child?.getPSLayoutPos?.();
    // 设置初始值
    colXS = !colXS || colXS == -1 ? 12 : colXS / 2;
    colSM = !colSM || colSM == -1 ? 12 : colSM / 2;
    colMD = !colMD || colMD == -1 ? 12 : colMD / 2;
    colLG = !colLG || colLG == -1 ? 12 : colLG / 2;
    colXSOffset = !colXSOffset || colXSOffset == -1 ? 0 : colXSOffset / 2;
    colSMOffset = !colSMOffset || colSMOffset == -1 ? 0 : colSMOffset / 2;
    colMDOffset = !colMDOffset || colMDOffset == -1 ? 0 : colMDOffset / 2;
    colLGOffset = !colLGOffset || colLGOffset == -1 ? 0 : colLGOffset / 2;
    if (layout == 'TABLE_12COL') {
      // 重新计算12列的栅格数值
      colXS = Math.min(colXS * 2, 12);
      colSM = Math.min(colSM * 2, 12);
      colMD = Math.min(colMD * 2, 12);
      colLG = Math.min(colXS * 2, 12);
      // 重新计算12列的栅格偏移
      const sign = (num: number) => (num == 0 ? 0 : num / Math.abs(num));
      colXSOffset = sign(colXSOffset) * Math.min(colXSOffset * 2, 12);
      colSMOffset = sign(colSMOffset) * Math.min(colSMOffset * 2, 12);
      colMDOffset = sign(colMDOffset) * Math.min(colMDOffset * 2, 12);
      colLGOffset = sign(colLGOffset) * Math.min(colLGOffset * 2, 12);
    }
    return {
      'size-lg': colLG,
      'size-md': colMD,
      'size-sm': colSM,
      'size-xs': colXS,
      'offset-lg': colLGOffset,
      'offset-md': colMDOffset,
      'offset-sm': colSMOffset,
      'offset-xs': colXSOffset,
    };
  }

  /**
   * @description 渲染标题栏
   * @return {*} 
   * @memberof LayoutComponentBase
   */
  public renderCaptionBar() {
    const { showCaptionBar, showDataInfoBar } = this.viewInstance as any;
    if (showCaptionBar) {
      return (
        <ion-title class={'view-caption-bar'}>
          {this.viewInstance.getPSSysImage() && (
            <app-icon class={'view-icon'} icon={this.viewInstance.getPSSysImage()}></app-icon>
          )}
          <span class={'view-caption'}>
            {this.$tl(this.viewInstance?.getCapPSLanguageRes()?.lanResTag, this.viewInstance.caption)}
            {showDataInfoBar ?
              [
                <span>-</span>,
                renderSlot(this.ctx.slots, 'dataInfoBar')
              ] : null
            }
          </span>
          <br />
          <span class={'view-sub-caption'}>
            {this.$tl(this.viewInstance?.getSubCapPSLanguageRes()?.lanResTag, this.viewInstance.subCaption)}
          </span>
        </ion-title>
      )
    } else {
      if (showDataInfoBar) {
        return (
          <ion-title class="view-info-bar">
            {renderSlot(this.ctx.slots, 'dataInfoBar')}
          </ion-title>
        )
      } else {
        return null;
      }
    }
  }

  /**
   * @description 渲染视图头
   * @return {*}  {*}
   * @memberof LayoutComponentBase
   */
  renderViewHeader(): any {
    return this.isShowCaptionBar ? (
      <ion-header className='view-header'>
        <ion-toolbar>
          {this.renderCaptionBar()}
        </ion-toolbar>         
      </ion-header>
    ) : null;
  }

  /**
   *
   * @description 视图主容器头部
   * @return {*}
   * @memberof LayoutComponentBase
   */
  renderViewMainContainerHeader(): any {
    return null;
  }

  /**
   *
   * @description 视图主容器内容
   * @return {*}
   * @memberof LayoutComponentBase
   */
  renderViewMainContainerContent(): any {
    return null;
  }

  /**
   * @description 视图主容器底部
   * @return {*}
   * @memberof LayoutComponentBase
   */
  renderViewMainContainerFooter(): any {
    return null;
  }

  /**
   * @description 视图顶部插槽
   *
   * @return {*}  {any[]}
   * @memberof LayoutComponentBase
   */
  public renderViewTopSlot(): any[] {
    return [renderSlot(this.ctx.slots, 'lefttoolbar'), renderSlot(this.ctx.slots, 'righttoolbar')];
  }

  /**
   * @description 渲染视图内容
   * @return {*}  {*}
   * @memberof LayoutComponentBase
   */
  renderViewContent(): any {
    return (
      <ion-content class={'view-body'}>
        {this.ctx.slots.topMessage ? renderSlot(this.ctx.slots, 'topMessage') : null}          
        {this.renderViewTopSlot()}
        <div class='view-main-container'>
          {this.renderViewMainContainerHeader()}
          {renderSlot(this.ctx.slots, 'bodyMessage')}
          {this.renderViewMainContainerContent()}
          {this.renderViewMainContainerFooter()}
        </div>
      </ion-content>
    );
  }

  /**
   * @description 渲染视图底部
   * @return {*}  {*}
   * @memberof LayoutComponentBase
   */
  renderViewFooter(): any {
    return (
      <ion-footer class={'view-footer'}>
        {this.ctx.slots.bottomMessage ? renderSlot(this.ctx.slots, 'bottomMessage') : null}
      </ion-footer>
    );
  }

  /**
   * @description 获取面板项样式名
   * @param {IPSPanelItem} item 面板项
   * @return {*}
   * @memberof LayoutComponentBase
   */
  public getDetailClass(item: IPSPanelItem) {
    const { itemType, name } = item;
    const detailClass = {
      [`app-view-layout-panel-${itemType.toLowerCase()}`]: true,
      [`panel-${itemType.toLowerCase()}-${name.toLowerCase()}`]: true,
    };
    const css = item.getPSSysCss?.();
    if (css) {
      Object.assign(detailClass, {
        [`${css.cssName}`]: true,
      });
    }
    return detailClass;
  }

  /**
   * @description 计算面板项样式及类名
   * @param {IPSPanelItem} item 面板项
   * @return {*}
   * @memberof LayoutComponentBase
   */
  public computePanelItemStyle(item: IPSPanelItem) {
    const layoutPos = item.getPSLayoutPos();
    //  项样式
    const detailStyle: any = {};
    //  宽高
    const { width, height } = layoutPos as any;
    if (width) {
      Object.assign(detailStyle, { width: width + 'px' });
    }
    if (height) {
      Object.assign(detailStyle, { height: height + 'px' });
    }

    //  class名称
    const detailClassName: any = this.getDetailClass(item);
    if (item.itemType == 'TABPANEL') {
      Object.assign(detailClassName, { 'panel-tabs': true });
    }
    const layoutMode = item.getPSLayout()?.layout;
    if (layoutMode == 'FLEX') {
      const grow = (layoutPos as any).grow;
      Object.assign(detailStyle, { grow: grow != -1 ? grow : 0 });
    }
    return { detailStyle: detailStyle, detailClassName: detailClassName };
  }

  /**
   * @description 渲染无父面板项
   * @param {IPSPanelItem} item 面板项
   * @param {*} content 渲染内容
   * @return {*}
   * @memberof LayoutComponentBase
   */
  public renderNoParentPanelItem(item: IPSPanelItem, content: any) {
    const layout = item.getPSLayout() as any;
    const layoutMode = layout?.layout;
    const { detailStyle, detailClassName } = this.computePanelItemStyle(item);
    const parent = item.getParentPSModelObject?.();
    //  是否为根
    const rootFlag = (parent as any)?.controlType == 'VIEWLAYOUTPANEL' ? true : false;
    if (layout && layoutMode == 'FLEX') {
      let cssStyle: string = 'width: 100%; height: 100%; overflow: auto; display: flex;';
      cssStyle += layout.dir ? `flex-direction: ${layout.dir};` : '';
      cssStyle += layout.align ? `justify-content: ${layout.align};` : '';
      cssStyle += layout.vAlign ? `align-items: ${layout.vAlign};` : '';
      if (rootFlag) {
        return (
          <div style={detailStyle} class={detailClassName}>
            {content}
          </div>
        );
      } else {
        return (
          <div style={cssStyle}>
            <div style={detailStyle} class={detailClassName}>
              {content}
            </div>
          </div>
        );
      }
    } else {
      const attrs = this.getGridLayoutProps({}, item);
      if (rootFlag) {
        return (
          <ion-col class={detailClassName} style={detailStyle} {...attrs}>
            {content}
          </ion-col>
        );
      } else {
        return (
          <ion-row style='height: 100%'>
            <ion-col class={detailClassName} style={detailStyle} {...attrs}>
              {content}
            </ion-col>
          </ion-row>
        );
      }
    }
  }

  /**
   * @description 渲染面板子项
   * @param {*} item 面板项实例对象
   * @return {*}
   * @memberof LayoutComponentBase
   */
  public renderPanelItems(item: any) {
    const panelItems = (item as IPSPanelContainer).getPSPanelItems?.() || [];
    if (!panelItems.length) {
      return null;
    }
    const layout: any = item.getPSLayout();
    const layoutMode = item.getPSLayout()?.layout;
    //  FLEX布局
    if (layout && layoutMode == 'FLEX') {
      let cssStyle: string = 'width: 100%; height: 100%; overflow: auto; display: flex;';
      cssStyle += layout.dir ? `flex-direction: ${layout.dir};` : '';
      cssStyle += layout.align ? `justify-content: ${layout.align};` : '';
      cssStyle += layout.vAlign ? `align-items: ${layout.vAlign};` : '';
      return (
        <div style={cssStyle}>
          {panelItems.map((item: IPSPanelItem, index: number) => {
            const { detailStyle, detailClassName } = this.computePanelItemStyle(item);
            return (
              <div style={detailStyle} class={detailClassName}>
                {this.renderByDetailType(item, true)}
              </div>
            );
          })}
        </div>
      );
    } else {
      //  栅格布局
      return (
        <ion-row style='height: 100%'>
          {panelItems.map((item: IPSPanelItem, index: number) => {
            const { detailStyle, detailClassName } = this.computePanelItemStyle(item);
            const attrs = this.getGridLayoutProps(item, item);
            return (
              <ion-col style={detailStyle} class={detailClassName} {...attrs}>
                {this.renderByDetailType(item, true)}
              </ion-col>
            );
          })}
        </ion-row>
      );
    }
  }

  /**
   * @description 渲染面板容器
   * @param {IPSPanelContainer} item 面板容器实例对象
   * @param {boolean} [hasParent=false] 是否具有父容器
   * @return {*}
   * @memberof LayoutComponentBase
   */
  public renderContainer(item: IPSPanelContainer, hasParent: boolean = false) {
    const layoutMode = item.getPSLayout()?.layout;
    let containerClass = 'app-view-layout-panel-container';
    if (item.getPSSysCss()) {
      containerClass += ` ${item.getPSSysCss()?.cssName}`;
    }
    //  父为容器时直接绘制子内容
    if (hasParent) {
      return this.renderPanelItems(item);
    }
    if (layoutMode == 'FLEX') {
      return <div class={containerClass}>{this.renderPanelItems(item)}</div>;
    } else {
      const attrs = this.getGridLayoutProps({}, item);
      return <ion-col {...attrs}>{this.renderPanelItems(item)}</ion-col>;
    }
  }

  /**
   * @description 处理编辑器事件
   * @param {IEditorEventParam} event
   * @memberof LayoutComponentBase
   */
  public handleEditorEvent(event: IEditorEventParam) { }

  /**
   * @description 渲染编辑器
   * @param {IPSEditor} editor 编辑器实例对象
   * @param {IPSPanelField} parentItem 父容器
   * @return {*}
   * @memberof LayoutComponentBase
   */
  public renderEditor(editor: IPSEditor, parentItem: IPSPanelField) {
    const targetCtrlComponent: any = App.getComponentService().getEditorComponent(
      editor?.editorType,
      editor?.editorStyle ? editor?.editorStyle : 'DEFAULT',
      editor.getPSSysPFPlugin()
        ? `${editor?.editorType}_${editor?.editorStyle}`
        : undefined,
    );
    return h(targetCtrlComponent, {
      editorInstance: editor,
      containerCtrl: this.viewInstance,
      contextData: this.context,
      parentItem: parentItem,
      navContext: Util.deepCopy(this.context),
      navParam: Util.deepCopy(this.viewParam),
      value: this.context?.[editor?.name || ''],
      onEditorEvent: (event: IEditorEventParam) => {
        this.handleEditorEvent(event);
      },
    });
  }

  /**
   * @description 渲染面板属性
   * @param {IPSPanelField} item 面板属性实例对象
   * @param {boolean} [hasParent=false] 是否具有父容器
   * @return {*}
   * @memberof LayoutComponentBase
   */
  public renderField(item: IPSPanelField, hasParent: boolean = false) {
    const { caption } = item;
    const editor = item.getPSEditor();
    const content = (
      <div class='panel-item-field'>
        {caption && <ion-label class='panel-item-field-label'>{caption}</ion-label>}
        {editor && this.renderEditor(editor, item)}
      </div>
    );
    if (hasParent) {
      return content;
    } else {
      return this.renderNoParentPanelItem(item, content);
    }
  }

  /**
   * @description 渲染面板直接内容
   * @param {IPSPanelRawItem} item 面板直接内容对象实例
   * @param {boolean} [hasParent=false] 是否具有父容器
   * @return {*}
   * @memberof LayoutComponentBase
   */
  public renderRawItem(item: IPSPanelRawItem, hasParent: boolean = false) {
    const { contentType, htmlContent, getPSSysImage, rawContent } = item;
    let element: any = undefined;
    if (contentType == 'HTML' && htmlContent) {
      element = h('div', {
        innerHTML: htmlContent,
      });
    }
    let content: any;
    switch (contentType) {
      case 'HTML':
        content = element ? element : htmlContent;
        break;
      case 'RAW':
        content = rawContent;
        break;
      case 'IMAGE':
        content = <img src={getPSSysImage?.()?.imagePath} />;
        break;
      case 'MARKDOWN':
        content = <div>MARKDOWN直接内容暂未支持</div>;
        break;
    }
    if (hasParent) {
      return content;
    } else {
      return this.renderNoParentPanelItem(item, content);
    }
  }

  /**
   * @description 分页面板切换
   * @param {string} key 分页面板标识
   * @param {*} event 源事件对象
   * @memberof LayoutComponentBase
   */
  public segmentChanged(key: string, event: any) {
    if (key) {
      this.currentTab.value[key] = event.detail.value;
    }
  }

  /**
   * @description 渲染面板分页面板
   * @param {IPSPanelTabPanel} item 面板分页面板实例对象
   * @param {boolean} [hasParent=false] 是否具有父容器
   * @return {*}
   * @memberof LayoutComponentBase
   */
  public renderTabPanel(item: IPSPanelTabPanel, hasParent: boolean = false) {
    const tabPages = item.getPSPanelTabPages() || [];
    if (tabPages.length) {
      //  默认选中第一个分页
      if (!this.currentTab.value[item.name]) {
        this.currentTab.value[item.name] = tabPages[0].name;
      }
      const content = [
        <div class='header'>
          <ion-segment
            value={this.currentTab.value[item.name]}
            onIonChange={($event: any) => this.segmentChanged(item.name, $event)}
          >
            {tabPages.map((page: IPSPanelTabPage, index: number) => {
              return (
                <ion-segment-button value={page.name}>
                  <ion-label>{page.caption}</ion-label>
                </ion-segment-button>
              );
            })}
          </ion-segment>
        </div>,
        <div class='content'>
          {tabPages.map((page: IPSPanelTabPage, index: number) => {
            return this.renderByDetailType(page);
          })}
        </div>,
      ];
      if (hasParent) {
        return content;
      } else {
        return this.renderNoParentPanelItem(item, content);
      }
    }
  }

  /**
   * @description 渲染面板分页
   * @param {IPSPanelTabPage} item 面板分页实例对象
   * @param {boolean} [hasParent=false] 是否具有父容器
   * @return {*}
   * @memberof LayoutComponentBase
   */
  public renderTabPage(item: IPSPanelTabPage, hasParent: boolean = false) {
    const parent = item.getParentPSModelObject?.() as IPSPanelTabPanel;
    const currentKey = this.currentTab.value[parent?.name];
    return (
      <div class={this.getDetailClass(item)} style={currentKey !== item.name ? 'display: none;' : ''}>
        {this.renderPanelItems(item)}
      </div>
    );
  }

  /**
   * @description 渲染面板控件
   * @param {IPSPanelCtrlPos} item 面板组件实例对象
   * @param {boolean} [hasParent=false] 是否具有父容器
   * @return {*}
   * @memberof LayoutComponentBase
   */
  public renderCtrlPos(item: IPSPanelCtrlPos, hasParent: boolean = false) {
    const content = renderSlot(this.ctx.slots, item.name?.toLowerCase());
    if (hasParent) {
      return content;
    } else {
      return this.renderNoParentPanelItem(item, content);
    }
  }

  /**
   * @description 根据面板项类型绘制
   * @param {*} item 面板项
   * @param {boolean} [hasParent=false] 是否具有父容器
   * @return {*}
   * @memberof LayoutComponentBase
   */
  public renderByDetailType(item: any, hasParent: boolean = false) {
    switch (item.itemType) {
      case 'CONTAINER':
        return this.renderContainer(item, hasParent);
      case 'FIELD':
        return this.renderField(item, hasParent);
      case 'RAWITEM':
        return this.renderRawItem(item, hasParent);
      case 'TABPANEL':
        return this.renderTabPanel(item, hasParent);
      case 'TABPAGE':
        return this.renderTabPage(item, hasParent);
      case 'CTRLPOS':
        return this.renderCtrlPos(item, hasParent);
    }
  }

  /**
   * @description 渲染视图布局面板
   * @return {*}
   * @memberof LayoutComponentBase
   */
  public renderViewLayoutPanel() {
    const bodyOnly = this.viewLayoutPanel?.layoutBodyOnly;
    return [
      bodyOnly ? this.renderViewHeader() : null,
      <ion-content class={'view-body'}>
        {bodyOnly && this.ctx.slots?.bodyMessage ? renderSlot(this.ctx.slots, 'bodyMessage') : null}
        <ion-row class='app-view-layout-panel'>
          {this.viewLayoutPanel?.getRootPSPanelItems()?.map((item: IPSPanelItem) => {
            return this.renderByDetailType(item);
          })}
        </ion-row>
      </ion-content>,
      bodyOnly ? this.renderViewFooter() : null,
    ];
  }

  /**
   * @description 布局面板渲染
   * @return {*}
   * @memberof LayoutComponentBase
   */
  render() {
    return (
      <ion-page class={{ 'ion-page': true, ...this.viewClass }}>
        {this.viewLayoutPanel && this.viewLayoutPanel.useDefaultLayout
          ? [this.renderViewHeader(), this.renderViewContent(), this.renderViewFooter()]
          : this.renderViewLayoutPanel()}
      </ion-page>
    );
  }
}
