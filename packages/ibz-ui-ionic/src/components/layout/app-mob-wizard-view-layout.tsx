import { renderSlot } from 'vue';
import { IPSAppDEMobWizardView } from '@ibiz/dynamic-model-api';
import { AppMobWizardViewLayoutProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { LayoutComponentBase } from './layout-component-base';

/**
 * @description 移动端向导视图视图布局组件
 * @export
 * @class AppDefaultMobWizardViewLayout
 * @extends {AppDefaultDEView<AppMobWizardViewLayoutProps>}
 */
export class AppDefaultMobWizardViewLayout extends LayoutComponentBase<AppMobWizardViewLayoutProps> {

  /**
   * @description 移动端向导视图实例对象
   * @type {IPSAppDEMobWizardView}
   * @memberof AppDefaultMobWizardViewLayout
   */
  public viewInstance !: IPSAppDEMobWizardView;

  /**
   * @description 渲染部件
   * @return {*}
   * @memberof AppDefaultMobWizardViewLayout
   */
  public renderViewMainContainerContent(): any {
    return <div class='view-main-container-content'>{[renderSlot(this.ctx.slots, 'wizardpanel')]}</div>;
  }
  
}
// 应用首页组件
export const AppDefaultMobWizardViewLayoutComponent = GenerateComponent(AppDefaultMobWizardViewLayout, new AppMobWizardViewLayoutProps());