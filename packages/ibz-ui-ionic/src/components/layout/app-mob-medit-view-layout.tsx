import { IPSAppDEMobMEditView } from '@ibiz/dynamic-model-api';
import { renderSlot } from '@vue/runtime-dom';
import { AppMobMEditViewLayoutProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { LayoutComponentBase } from './layout-component-base';

export class AppDefaultMobMEditViewLayout extends LayoutComponentBase<AppMobMEditViewLayoutProps> {
  /**
   * @description 移动端多表单编辑视图实例对象
   * @type {IPSAppDEMobMEditView}
   * @memberof AppDefaultMobMEditViewLayout
   */
  public viewInstance!: IPSAppDEMobMEditView;

  /**
   * @description 视图主容器内容
   * @return {*}  {any}
   * @memberof AppDefaultMobMEditViewLayout
   */
  renderViewMainContainerContent(): any {
    return <div class='view-main-container-content'>{[renderSlot(this.ctx.slots, 'meditviewpanel')]}</div>;
  }
}
// 移动端多表单编辑视图布局组件
export const AppDefaultMobMEditViewLayoutComponent = GenerateComponent(
  AppDefaultMobMEditViewLayout,
  new AppMobMEditViewLayoutProps(),
);
