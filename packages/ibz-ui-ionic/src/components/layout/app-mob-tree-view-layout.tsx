import { renderSlot } from 'vue';
import { AppMobTreeViewLayoutProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { MultiDataViewLayoutComponentBase } from './multi-data-view-layout-component-base';

/**
 * 移动端树视图视图布局面板
 *
 * @class AppMobTreeViewLayout
 * @extends LayoutComponentBase
 */
export class AppDefaultMobTreeViewLayout extends MultiDataViewLayoutComponentBase<AppMobTreeViewLayoutProps> {
  /**
   * @description 渲染部件
   * @return {*}  {any}
   * @memberof AppDefaultMobTreeViewLayout
   */
  public renderViewMainContainerContent(): any {
    return <div class='view-main-container-content'>{[renderSlot(this.ctx.slots, 'tree')]}</div>;
  }
}

//  移动端树视图视图布局面板组件
export const AppDefaultMobTreeViewLayoutComponent = GenerateComponent(
  AppDefaultMobTreeViewLayout,
  new AppMobTreeViewLayoutProps(),
);
