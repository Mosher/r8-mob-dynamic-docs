import { renderSlot } from 'vue';
import { IPSAppDEMobMapView } from '@ibiz/dynamic-model-api';
import { AppMobMapViewLayoutProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { MultiDataViewLayoutComponentBase } from './multi-data-view-layout-component-base';
/**
 * 移动端地图视图视图布局面板（DEFAULT样式）
 *
 * @class AppDefaultMobMapViewLayout
 * @extends ComponentBase
 */
export class AppDefaultMobMapViewLayout extends MultiDataViewLayoutComponentBase<AppMobMapViewLayoutProps> {
  /**
   * 移动端地图视图实例对象
   *
   * @public
   * @type {IPSAppDEMobMapView}
   * @memberof AppDefaultMobMapViewLayout
   */
  public viewInstance!: IPSAppDEMobMapView;

  /**
   * @description 视图主容器内容
   * @return {*}
   * @memberof AppDefaultMobMapViewLayout
   */
  renderViewMainContainerContent(): any {
    return <div class='view-main-container-content'>{[renderSlot(this.ctx.slots, 'map')]}</div>;
  }
}

//  移动端多数据视图视图布局面板部件
export const AppDefaultMobMapViewLayoutComponent = GenerateComponent(
  AppDefaultMobMapViewLayout,
  new AppMobMapViewLayoutProps(),
);
