import { AppMultiDataViewLayoutComponentProps } from 'ibz-core';
import { renderSlot } from 'vue';
import { LayoutComponentBase } from './layout-component-base';

/**
 * 多数据视图布局基类
 *
 * @export
 * @class MultiDataViewLayoutComponentBase
 * @extends {LayoutComponentBase<AppMultiDataViewLayoutComponentProps>}
 * @template Props
 */
export class MultiDataViewLayoutComponentBase<Props extends AppMultiDataViewLayoutComponentProps> extends LayoutComponentBase<AppMultiDataViewLayoutComponentProps> {

  /**
   *
   * @description 视图主容器头部
   * @return {*}
   * @memberof MultiDataViewLayoutComponentBase
   */
  renderViewMainContainerHeader(): any {
    return (
      <div class='view-main-container-header'>
        {[
          renderSlot(this.ctx.slots, 'quickGroupSearch'),
          renderSlot(this.ctx.slots, 'quickSearch'),
          renderSlot(this.ctx.slots, 'searchform'),
          renderSlot(this.ctx.slots, 'quicksearchform'),
        ]}
      </div>
    );
  }
}
