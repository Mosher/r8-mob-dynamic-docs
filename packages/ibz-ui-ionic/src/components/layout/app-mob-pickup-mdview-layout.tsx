import { renderSlot } from 'vue';
import { IPSAppDEMobMDView } from '@ibiz/dynamic-model-api';
import { AppMobPickUpMDViewLayoutProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { LayoutComponentBase } from './layout-component-base';

/**
 * 移动端选择多数据视图视图布局面板（DEFAULT样式）
 *
 * @class AppDefaultMobPickUpMDViewLayout
 * @extends ComponentBase
 */
export class AppDefaultMobPickUpMDViewLayout extends LayoutComponentBase<AppMobPickUpMDViewLayoutProps> {
  /**
   * @description 选择多数据视图实例
   * @type {IPSAppDEMobMDView}
   * @memberof AppDefaultMobPickUpMDViewLayout
   */
  public viewInstance!: IPSAppDEMobMDView;

  /**
   *
   * @description 视图主容器头部
   * @return {*}
   * @memberof AppDefaultMobPickupTreeViewLayout
   */
  renderViewMainContainerHeader(): any {
    return (
      <div class='view-main-container-header'>
        {[renderSlot(this.ctx.slots, 'quickGroupSearch'), renderSlot(this.ctx.slots, 'quickSearch')]}
      </div>
    );
  }

  /**
   * @description 视图主容器内容
   * @return {*}  {any}
   * @memberof AppDefaultMobPickUpMDViewLayout
   */
  renderViewMainContainerContent(): any {
    return <div class='view-main-container-content'>{[renderSlot(this.ctx.slots, 'mdctrl')]}</div>;
  }
}

//  移动端选择多数据视图视图布局面板部件
export const AppDefaultMobPickUpMDViewLayoutComponent = GenerateComponent(
  AppDefaultMobPickUpMDViewLayout,
  new AppMobPickUpMDViewLayoutProps(),
);
