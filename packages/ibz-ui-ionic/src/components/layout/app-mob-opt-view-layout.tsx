import { IPSAppDEMobEditView } from '@ibiz/dynamic-model-api';
import { renderSlot } from '@vue/runtime-dom';
import { AppMobOptViewLayoutProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { LayoutComponentBase } from './layout-component-base';

/**
 * @description 移动端选项操作视图视图布局组件
 * @export
 * @class AppDefaultMobOptViewLayout
 * @extends {LayoutComponentBase<AppMobOptViewLayoutProps>}
 */
export class AppDefaultMobOptViewLayout extends LayoutComponentBase<AppMobOptViewLayoutProps> {
  /**
   * 移动端选项操作视图实例对象
   *
   * @type {IPSAppDEMobOptView}
   * @memberof AppDefaultMobOptViewLayout
   */
  public viewInstance!: IPSAppDEMobEditView;

  /**
   * @description 视图主容器内容
   * @return {*}
   * @memberof AppDefaultMobOptViewLayout
   */
  renderViewMainContainerContent(): any {
    return <div class='view-main-container-content'>{[renderSlot(this.ctx.slots, 'form')]}</div>;
  }

  /**
   * @description 渲染视图底部
   * @return {*}  {*}
   * @memberof AppDefaultMobOptViewLayout
   */
  renderViewFooter(): any {
    return (
      <ion-footer class={'view-footer'}>
        {renderSlot(this.ctx.slots, 'footerButtons')}
        {this.ctx.slots.bottomMessage ? renderSlot(this.ctx.slots, 'bottomMessage') : null}
      </ion-footer>
    );
  }
}
// 应用首页组件
export const AppDefaultMobOptViewLayoutComponent = GenerateComponent(
  AppDefaultMobOptViewLayout,
  new AppMobOptViewLayoutProps(),
);
