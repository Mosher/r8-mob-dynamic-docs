import { IPSAppDEMobMapExplorerView } from '@ibiz/dynamic-model-api';
import { renderSlot } from '@vue/runtime-dom';
import { AppMobMapExpViewLayoutProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { LayoutComponentBase } from './layout-component-base';

/**
 * @description 移动端地图导航视图视图布局组件
 * @export
 * @class AppDefaultMobMapExpViewLayout
 * @extends {AppDefaultDEExpView<AppMobMapExpViewLayoutProps>}
 */
export class AppDefaultMobMapExpViewLayout extends LayoutComponentBase<AppMobMapExpViewLayoutProps> {
  /**
   * 移动端地图导航视图实例对象
   *
   * @type {IPSAppDEMobMapExpView}
   * @memberof AppDefaultMobMapExpViewLayout
   */
  public viewInstance!: IPSAppDEMobMapExplorerView;

  /**
   * @description 视图主容器内容
   * @return {*}
   * @memberof AppDefaultMobHtmlViewLayout
   */
  renderViewMainContainerContent(): any {
    return <div class='view-main-container-content'>{renderSlot(this.ctx.slots, 'mapexpbar')}</div>;
  }
}
// 应用首页组件
export const AppDefaultMobMapExpViewLayoutComponent = GenerateComponent(
  AppDefaultMobMapExpViewLayout,
  new AppMobMapExpViewLayoutProps(),
);
