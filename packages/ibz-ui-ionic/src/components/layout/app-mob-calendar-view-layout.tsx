import { renderSlot } from 'vue';
import { IPSAppDEMobCalendarView } from '@ibiz/dynamic-model-api';
import { AppMobCalendarViewLayoutProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { MultiDataViewLayoutComponentBase } from './multi-data-view-layout-component-base';

/**
 * 移动端日历视图视图布局面板（DEFAULT样式）
 *
 * @class AppDefaultMobCalendarViewLayout
 * @extends MultiDataViewLayoutComponentBase
 */
export class AppDefaultMobCalendarViewLayout extends MultiDataViewLayoutComponentBase<AppMobCalendarViewLayoutProps> {
  /**
   * 移动端日历视图实例对象
   *
   * @public
   * @type {IPSAppDEMobCalendarView}
   * @memberof AppDefaultMobCalendarViewLayout
   */
  public viewInstance!: IPSAppDEMobCalendarView;

  /**
   *
   * @description 视图主容器内容
   * @return {*}  {*}
   * @memberof AppDefaultMobCalendarViewLayout
   */
  renderViewMainContainerContent(): any {
    return <div class='view-main-container-content'>{[renderSlot(this.ctx.slots, 'calendar')]}</div>;
  }
}

//  移动端日历视图视图布局面板部件
export const AppDefaultMobCalendarViewLayoutComponent = GenerateComponent(
  AppDefaultMobCalendarViewLayout,
  new AppMobCalendarViewLayoutProps(),
);
