import { IPSAppDEChartExplorerView } from '@ibiz/dynamic-model-api';
import { renderSlot } from '@vue/runtime-dom';
import { AppMobChartExpViewLayoutProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { LayoutComponentBase } from './layout-component-base';

/**
 * @description 移动端图表导航视图视图布局组件
 * @export
 * @class AppDefaultMobChartExpViewLayout
 * @extends {AppDefaultExpView<AppMobChartExpViewLayoutProps>}
 */
export class AppDefaultMobChartExpViewLayout extends LayoutComponentBase<AppMobChartExpViewLayoutProps> {
  /**
   * 移动端图表导航视图实例对象
   *
   * @type {IPSAppDEMobChartExpView}
   * @memberof AppDefaultMobChartExpViewLayout
   */
  public viewInstance!: IPSAppDEChartExplorerView;

  /**
   * @description 视图主容器内容
   * @return {*}
   * @memberof AppDefaultMobHtmlViewLayout
   */
  renderViewMainContainerContent(): any {
    return <div class='view-main-container-content'>{renderSlot(this.ctx.slots, 'chartexpbar')}</div>;
  }
}
// 图表导航栏组件
export const AppDefaultMobChartExpViewLayoutComponent = GenerateComponent(
  AppDefaultMobChartExpViewLayout,
  new AppMobChartExpViewLayoutProps(),
);
