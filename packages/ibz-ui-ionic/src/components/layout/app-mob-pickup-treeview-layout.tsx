import { renderSlot } from 'vue';
import { IPSAppDEMobTreeView } from '@ibiz/dynamic-model-api';
import { AppMobPickupTreeViewLayoutProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { LayoutComponentBase } from './layout-component-base';

/**
 * 移动端选择树视图视图布局面板（DEFAULT样式）
 *
 * @class AppDefaultMobPickupTreeViewLayout
 * @extends LayoutComponentBase
 */
export class AppDefaultMobPickupTreeViewLayout extends LayoutComponentBase<AppMobPickupTreeViewLayoutProps> {
  /**
   * @description 选择树视图实例
   * @type {IPSAppDEMobTreeView}
   * @memberof AppDefaultMobPickupTreeViewLayout
   */
  public viewInstance!: IPSAppDEMobTreeView;
  /**
   *
   * @description 视图主容器头部
   * @return {*}
   * @memberof AppDefaultMobPickupTreeViewLayout
   */
  renderViewMainContainerHeader(): any {
    return (
      <div class='view-main-container-header'>
        {[renderSlot(this.ctx.slots, 'quickGroupSearch'), renderSlot(this.ctx.slots, 'quickSearch')]}
      </div>
    );
  }

  /**
   * @description 视图主容器内容
   * @return {*}  {any[]}
   * @memberof AppDefaultMobPickupTreeViewLayout
   */
  renderViewMainContainerContent(): any {
    return <div class='view-main-container-content'>{[renderSlot(this.ctx.slots, 'tree')]}</div>;
  }
}

//  移动端选择多数据视图视图布局面板部件
export const AppDefaultMobPickupTreeViewLayoutComponent = GenerateComponent(
  AppDefaultMobPickupTreeViewLayout,
  new AppMobPickupTreeViewLayoutProps(),
);
