import { IPSAppDEMobCalendarExplorerView } from '@ibiz/dynamic-model-api';
import { renderSlot } from '@vue/runtime-dom';
import { AppMobCalendarExpViewLayoutProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { LayoutComponentBase } from './layout-component-base';

/**
 * @description 移动端地图导航视图视图布局组件
 * @export
 * @class AppDefaultMobCalendarExpViewLayout
 * @extends {AppDefaultDEExpView<AppMobCalendarExpViewLayoutProps>}
 */
export class AppDefaultMobCalendarExpViewLayout extends LayoutComponentBase<AppMobCalendarExpViewLayoutProps> {
  /**
   * 移动端地图导航视图实例对象
   *
   * @type {IPSAppDEMobMapExpView}
   * @memberof AppDefaultMobCalendarExpViewLayout
   */
  public viewInstance!: IPSAppDEMobCalendarExplorerView;

  /**
   * @description 视图主容器内容
   * @return {*}
   * @memberof AppDefaultMobHtmlViewLayout
   */
  renderViewMainContainerContent(): any {
    return <div class='view-main-container-content'>{renderSlot(this.ctx.slots, 'calendarexpbar')}</div>;
  }
}
// 应用首页组件
export const AppDefaultMobCalendarExpViewLayoutComponent = GenerateComponent(
  AppDefaultMobCalendarExpViewLayout,
  new AppMobCalendarExpViewLayoutProps(),
);
