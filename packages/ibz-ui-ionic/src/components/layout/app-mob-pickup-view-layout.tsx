import { renderSlot } from 'vue';
import { IPSAppDEPickupView } from '@ibiz/dynamic-model-api';
import { AppMobPickUpViewLayoutProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { LayoutComponentBase } from './layout-component-base';

export class AppDefaultMobPickupViewLayout extends LayoutComponentBase<AppMobPickUpViewLayoutProps> {
  /**
   * @description 数据选择视图实例
   * @type {IPSAppDEPickupView}
   * @memberof AppDefaultMobPickupViewLayout
   */
  public viewInstance!: IPSAppDEPickupView;

  /**
   * @description 渲染视图头
   * @return {*}  {*}
   * @memberof AppDefaultMobPickupViewLayout
   */
  renderViewHeader(): any {
    return <ion-header>{renderSlot(this.ctx.slots, 'headerButtons')}</ion-header>;
  }

  /**
   * @description 视图主容器内容
   * @return {*}  {any}
   * @memberof AppDefaultMobPickupViewLayout
   */
  renderViewMainContainerContent(): any {
    return <div class='view-main-container-content'>{[renderSlot(this.ctx.slots, 'pickupviewpanel')]}</div>;
  }
}

//  移动端数据选择视图布局面板
export const AppDefaultMobPickupViewLayoutComponent = GenerateComponent(
  AppDefaultMobPickupViewLayout,
  new AppMobPickUpViewLayoutProps(),
);
