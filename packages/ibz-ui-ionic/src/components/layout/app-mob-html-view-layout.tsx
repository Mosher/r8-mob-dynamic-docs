import { IPSAppDEMobHtmlView } from '@ibiz/dynamic-model-api';
import { renderSlot } from '@vue/runtime-dom';
import { AppMobHtmlViewLayoutProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { LayoutComponentBase } from './layout-component-base';

export class AppDefaultMobHtmlViewLayout extends LayoutComponentBase<AppMobHtmlViewLayoutProps> {
  /**
   * 移动端编辑视图实例对象
   *
   * @type {IPSAppDEMobHtmlView}
   * @memberof AppDefaultMobHtmlViewLayout
   */
  public viewInstance!: IPSAppDEMobHtmlView;

  /**
   * @description 视图主容器内容
   * @return {*}
   * @memberof AppDefaultMobHtmlViewLayout
   */
  renderViewMainContainerContent(): any {
    return <div class='view-main-container-content'>{renderSlot(this.ctx.slots, 'htmlContent')}</div>;
  }
}
// 应用首页组件
export const AppDefaultMobHtmlViewLayoutComponent = GenerateComponent(
  AppDefaultMobHtmlViewLayout,
  new AppMobHtmlViewLayoutProps(),
);
