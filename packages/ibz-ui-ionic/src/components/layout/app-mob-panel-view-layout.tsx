import { renderSlot } from 'vue';
import { IPSAppDEMobPanelView } from '@ibiz/dynamic-model-api';
import { AppMobPanelViewLayoutProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { LayoutComponentBase } from './layout-component-base';

export class AppDefaultMobPanelViewLayout extends LayoutComponentBase<AppMobPanelViewLayoutProps> {
  /**
   * 移动端面板视图实例对象
   *
   * @public
   * @type {IPSAppDEMobPanelView}
   * @memberof AppDefaultMobMDViewLayout
   */
  public viewInstance!: IPSAppDEMobPanelView;

  /**
   * @description 视图主容器内容
   * @return {*}
   * @memberof AppDefaultMobMDViewLayout
   */
  renderViewMainContainerContent(): any {
    return <div class='view-main-container-content'>{[renderSlot(this.ctx.slots, 'panel')]}</div>;
  }
}

//  移动端面板视图视图布局面板部件
export const AppDefaultMobPanelViewLayoutComponent = GenerateComponent(
  AppDefaultMobPanelViewLayout,
  new AppMobPanelViewLayoutProps(),
);
