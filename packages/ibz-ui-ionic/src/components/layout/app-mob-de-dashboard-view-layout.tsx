import { renderSlot } from 'vue';
import { IPSAppDEMobDashboardView } from '@ibiz/dynamic-model-api';
import { AppMobDEDashboardViewLayoutProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { LayoutComponentBase } from './layout-component-base';

/**
 * 移动端实体数据看板视图视图布局面板
 *
 * @exports
 * @class AppDefaultMobDEDashboardViewLayout
 * @extends LayoutComponentBase
 */
export class AppDefaultMobDEDashboardViewLayout extends LayoutComponentBase<AppMobDEDashboardViewLayoutProps> {
  /**
   * 移动端实体数据看板视图实例对象
   *
   * @type {IPSAppDEMobDashboardView}
   * @memberof AppMobDEDashboardViewLayout
   */
  public viewInstance!: IPSAppDEMobDashboardView;

  /**
   *
   * @description 视图主容器头部
   * @return {*}
   * @memberof AppMobDEDashboardViewLayout
   */
  renderViewMainContainerHeader(): any {
    return (
      <div class='view-main-container-header'>
        {[
          renderSlot(this.ctx.slots, 'searchform'),
          renderSlot(this.ctx.slots, 'quicksearchform'),
        ]}
      </div>
    );
  }

  /**
   * @description 视图主容器内容
   * @return {*}
   * @memberof AppMobDEDashboardViewLayout
   */
  renderViewMainContainerContent(): any {
    return <div class='view-main-container-content'>{[renderSlot(this.ctx.slots, 'dashboard')]}</div>;
  }
}

//  移动端实体数据看板视图视图布局面板组件
export const AppDefaultMobDEDashboardViewLayoutComponent = GenerateComponent(
  AppDefaultMobDEDashboardViewLayout,
  new AppMobDEDashboardViewLayoutProps(),
);
