import { renderSlot } from 'vue';
import { IPSAppDEListExplorerView } from '@ibiz/dynamic-model-api';
import { AppMobListExpViewLayoutProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { LayoutComponentBase } from './layout-component-base';

/**
 * @description 移动端列表导航视图视图布局组件
 * @export
 * @class AppDefaultMobListExpViewLayout
 * @extends {AppDefaultDEExpView<AppMobListExpViewLayoutProps>}
 */
export class AppDefaultMobListExpViewLayout extends LayoutComponentBase<AppMobListExpViewLayoutProps> {
  /**
   * 移动端列表导航视图实例对象
   *
   * @type {IPSAppDEListExplorerView}
   * @memberof AppDefaultMobListExpViewLayout
   */
  public viewInstance!: IPSAppDEListExplorerView;

  /**
   * @description 渲染部件
   * @return {*}
   * @memberof AppDefaultMobListExpViewLayout
   */
  public renderViewMainContainerContent(): any {
    return <div class='view-main-container-content'>{[renderSlot(this.ctx.slots, 'listexpbar')]}</div>;
  }
}
// 应用首页组件
export const AppDefaultMobListExpViewLayoutComponent = GenerateComponent(
  AppDefaultMobListExpViewLayout,
  new AppMobListExpViewLayoutProps(),
);
