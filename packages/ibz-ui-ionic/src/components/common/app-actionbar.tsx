import { defineComponent } from 'vue';
/**
 * 操作栏部件输入参数
 */
const AppActionBarProps = {
  /**
   * 操作栏模型数据
   *
   * @type {Array}
   */
  items: {
    type: Array,
    default: [],
  },

  /**
   * 模型服务
   *
   * @type {IParam}
   * @memberof AppFormPageProps
   */
  modelService: Object,
};

/**
 * 操作栏部件
 *
 * @class AppActionBar
 */
export const AppActionBar = defineComponent({
  name: 'AppActionBar',
  props: AppActionBarProps,
  setup(props: any, ctx: any) {
    /**
     * 触发界面行为
     *
     * @param event
     */
    const handleClick = (tag: string, event: any) => {
      ctx.emit('itemClick', tag, event);
    };
    return {
      handleClick,
    };
  },
  render() {
    return (
      <div class='app-actionbar'>
        {this.items && this.items.length > 0
          ? this.items.map((item: any, index: number) => {
              return (
                <div class='actionbar-item' key={index}>
                  {item.counterService && item.counterService.counterData ? (
                    <ion-badge color='success'>{item.counterService.counterData[item.counterId]}</ion-badge>
                  ) : null}
                  <ion-button
                    color='light'
                    onClick={(event: any) => {
                      this.handleClick(item.viewLogicName, event);
                    }}
                  >
                    {(this as any).$tl(item.language, item.name)}
                  </ion-button>
                </div>
              );
            })
          : null}
        <div class='actionbar-item'></div>
      </div>
    );
  },
});
