import { ComponentBase, GenerateComponent } from '../component-base';
import { IParam } from 'ibz-core';

export class AppListItemProps {
  /**
   * @description 项数据
   * @type {IParam}
   * @memberof AppListItemProps
   */
  item: IParam = {};

  /**
   * @description 索引
   * @type {number}
   * @memberof AppListItemProps
   */
  index: number = 0;

  /**
   * @description 值格式化
   * @type {string}
   * @memberof AppListItemProps
   */
  valueFormat: string = '';
}
export class AppListItem extends ComponentBase<AppListItemProps> {
  /**
   * @description 项数据
   * @type {IParam}
   * @memberof AppListItem
   */
  public item: IParam = {};

  /**
   * @description 索引
   * @type {number}
   * @memberof AppListItem
   */
  public index: number = 0;

  /**
   * @description 值格式化
   * @type {string}
   * @memberof AppListItem
   */
  public valueFormat: string = '';

  /**
   * @description 设置响应式
   * @memberof AppListItem
   */
  setup() {
    this.initInputData(this.props);
  }

  /**
   * @description 初始化输入属性
   * @param {AppListItemProps} opts 输入对象
   * @memberof AppListItem
   */
  public initInputData(opts: AppListItemProps) {
    this.item = opts.item;
    this.index = opts.index;
    this.valueFormat = opts.valueFormat;
  }

  /**
   * @description 输入属性值变更
   * @memberof AppListItem
   */
  watchEffect() {
    this.initInputData(this.props);
    this.getIndexText();
  }

  /**
   * @description 获取索引样式
   * @memberof AppListItem
   */
  getIndexText() {
    const colorArray = ['#ffa600', '#498cf2', '#f76e9a', '#f56ef7', '#a56ef7'];
    if (this.item?.srfmajortext) {
      this.item.indexText = this.item.srfmajortext[0];
    }
    this.item.indexColor = { 'background-color': colorArray[this.index % colorArray.length] };
  }

  /**
   * @description 点击事件
   * @memberof AppListItem
   */
  onClick() {
    this.ctx.emit('itemClick', this.item);
  }

  /**
   * @description 绘制
   * @return {*}
   * @memberof AppListItem
   */
  render() {
    return (
      <ion-item>
        <div class='app-list-item' onClick={() => this.onClick()}>
          <div class='list-item-container' style={this.item?.indexColor}>
            {this.item?.indexText}
          </div>
          <div class='list-item-title'>
            <span v-format={this.valueFormat}>{this.item?.srfmajortext}</span>
          </div>
        </div>
      </ion-item>
    );
  }
}
export const AppListItemComponent = GenerateComponent(AppListItem, Object.keys(new AppListItemProps()));
