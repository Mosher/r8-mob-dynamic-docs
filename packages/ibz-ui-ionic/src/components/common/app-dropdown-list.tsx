import { defineComponent } from 'vue';
import { ICodeListService, LogUtil, Util } from 'ibz-core';

/**
 * 下拉列表编辑器输入属性
 *
 * @memberof AppDropdownList
 */
const AppDropdownListProps = {
  /**
   * 双向绑定值
   *
   * @type {String}
   * @memberof AppDropdownList
   */
  value: {
    type: String,
  },

  /**
   * 上下文data数据（表单数据）
   *
   * @type {Object}
   * @memberof AppDropdownList
   */
  contextData: Object,

  /**
   * 上下文
   *
   * @type {Object}
   * @memberof AppDropdownList
   */
  context: Object,

  /**
   * 视图参数
   *
   * @type {Object}
   * @memberof AppDropdownList
   */
  viewParam: Object,

  /**
   * placeholder值
   *
   * @type {String}
   * @memberof AppDropdownList
   */
  placeholder: String,

  /**
   * 是否禁用
   *
   * @type {boolean}
   * @memberof AppDropdownList
   */
  disabled: Boolean,

  /**
   * 只读状态
   *
   * @type {boolean}
   * @memberof AppDropdownList
   */
  readonly: Boolean,

  /**
   * 代码表标识
   *
   * @type {String}
   * @memberof AppDropdownList
   */
  tag: String,

  /**
   * 代码表类型
   *
   * @type {String}
   * @memberof AppDropdownList
   */
  codeListType: String,

  /**
   * 局部上下文参数
   *
   * @type {Object}
   * @memberof AppDropdownList
   */
  localContext: Object,

  /**
   * 局部导航参数
   *
   * @type {Object}
   * @memberof AppDropdownList
   */
  localParam: Object,

  /**
   * 值分割符
   *
   * @type {String}
   * @memberof AppDropdownList
   */
  valueSeparator: {
    type: String,
    default: ',',
  },

  /**
   * 是否多选
   *
   * @type {boolean}
   * @memberof AppDropdownList
   */
  multiple: {
    type: Boolean,
    default: false,
  },

  /**
   * 打开模式(action-sheet和popover模式不支持多选)
   *
   * @type {String}
   * @memberof AppDropdownList
   */
  openMode: {
    type: String,
    default: 'action-sheet',
  },
};

/**
 * 下拉列表编辑器
 *
 * @class AppDropdownList
 */
export const AppDropdownList = defineComponent({
  name: 'AppDropdownList',
  props: AppDropdownListProps,
  emits: ['editorValueChange'],
  methods: {
    /**
     * 公共参数处理
     *
     * @param {*} arg
     * @returns
     * @memberof AppDropdownList
     */
    handlePublicParams(arg: any) {
      // 合并表单参数
      arg.viewParam = this.viewParam ? Util.deepCopy(this.viewParam) : {};
      arg.context = this.context ? Util.deepCopy(this.context) : {};
      // 附加参数处理
      if (this.localContext && Object.keys(this.localContext).length > 0) {
        const _context = Util.computedNavData(this.contextData, arg.context, arg.viewParam, this.localContext);
        Object.assign(arg.context, _context);
      }
      if (this.localParam && Object.keys(this.localParam).length > 0) {
        const _param = Util.computedNavData(this.contextData, arg.context, arg.viewParam, this.localParam);
        Object.assign(arg.viewParam, _param);
      }
    },

    /**
     * 加载数据
     *
     * @memberof AppDropdownList
     */
    load() {
      this.items = [];
      const data: any = {};
      this.handlePublicParams(data);
      if (this.tag && this.codeListType) {
        const param = {
          tag: this.tag,
          type: this.codeListType,
          navContext: data.context,
          navViewParam: data.viewParam,
        };
        this.codeListService
          .getDataItems(param)
          .then((items: any[]) => {
            this.items = items;
          })
          .catch((error: any) => {
            LogUtil.log(`----${this.tag}----代码表不存在`);
          });
      }
    },

    /**
     * 选中改变事件
     *
     * @param event
     * @memberof AppDropdownList
     */
    valueChange(event: any) {
      const value =
        Util.typeOf(event.detail.value) == 'array' ? event.detail.value.join(this.valueSeparator) : event.detail.value;
      if (Object.is(this.value, value)) {
        return;
      }
      this.$emit('editorValueChange', value);
    },
  },
  data() {
    const codeListService: ICodeListService = App.getCodeListService();
    const items: any[] = [];
    return {
      // 代码表服务
      codeListService: codeListService,
      // 下拉数据数组
      items: items,
    };
  },
  created() {
    this.load();
  },
  render() {
    return (
      <ion-select
        value={this.value?.split(this.valueSeparator)}
        disabled={this.disabled || this.readonly}
        interface={this.multiple ? 'alert' : this.openMode}
        multiple={this.multiple}
        cancelText={(this as any).$tl('share.cancel','取消')}
        okText={(this as any).$tl('share.ok','确认')}
        class='app-dropdown-list'
        onIonChange={(e: any) => {
          this.valueChange(e);
        }}
      >
        {this.items.map((option: any) => {
          return <ion-select-option value={option.value}>{option.text}</ion-select-option>;
        })}
      </ion-select>
    );
  },
});
