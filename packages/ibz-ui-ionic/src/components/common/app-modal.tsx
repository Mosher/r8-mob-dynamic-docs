import { defineComponent, h, toRefs } from 'vue';
import { AppViewEvents, Util } from 'ibz-core';

const ModelProps = {
  /**
   * 导航上下文
   *
   * @type {any}
   * @memberof ModelProps
   */
  navContext: {
    type: Object,
    default: {},
  },
  /**
   * 视图上下文参数
   *
   * @type {any}
   * @memberof ModelProps
   */
  navParam: {
    type: Object,
    default: {},
  },
  /**
   * 导航数据
   *
   * @type {*}
   * @memberof ModelProps
   */
  navDatas: {
    type: Array,
    default: [],
  },
  view: {
    type: Object,
  },
  model: {
    type: Object,
  },
  subject: {
    type: Object,
  },
};
export default defineComponent({
  props: ModelProps,
  /**
   * vue 生命周期
   *
   * @memberof AppFormGroup
   */
  setup(props: any) {
    const { view } = toRefs(props);

    /**
     * 临时结果
     */
    const tempResult: any = { ret: '' };

    /**
     * 视图组件
     *
     * @type {any}
     * @memberof AppModal
     */
    const viewComponent: string = view.value.viewComponent;

    /**
     *  视图动态路径
     *
     * @type {*}
     */
    const viewPath: string = view.value.viewPath;

    /**
     * 自定义类名
     */
    const customClass = view.value.customClass;

    /**
     * 自定义样式
     */
    const customStyle = view.value.customStyle;

    /**
     * 视图层级
     *
     * @type {any}
     * @memberof AppModal
     */
    const zIndex: any = null;


    /**
     * 模型数据
     *
     * @type {any}
     * @memberof AppModal
     */
    const modelData: any = view.value.modelData;

    return {
      tempResult,
      viewPath,
      viewComponent,
      customClass,
      zIndex,
      customStyle,
      modelData
    };
  },
  mounted() {
    if (this.model && this.customStyle) {
      const modal = this.model;
      Object.keys(this.customStyle).forEach((key: string) => {
        modal.style[key] = this.customStyle[key];
      });
    }
  },
  methods: {
    /**
     * 视图事件
     *
     * @param viewName 视图名
     * @param action 视图行为
     * @param data 抛出数据
     */
    handleViewEvent(viewName: any, action: string, data: any[]) {
      switch (action) {
        case AppViewEvents.CLOSE:
          this.close(data);
          break;
        case AppViewEvents.DATA_CHANGE:
          this.dataChange(data);
          break;
      }
    },

    /**
     * 视图关闭
     *
     * @memberof AppModal
     */
    close(result: any) {
      if (result && Array.isArray(result) && result.length > 0) {
        Object.assign(this.tempResult, { ret: 'OK', datas: Util.deepCopy(result) });
      }
      this.handleCloseModel();
    },

    /**
     * 视图数据变化
     *
     * @memberof AppModal
     */
    dataChange(result: any) {
      this.tempResult = { ret: '' };
      if (result && Array.isArray(result) && result.length > 0) {
        Object.assign(this.tempResult, { ret: 'OK', datas: Util.deepCopy(result) });
      }
    },

    /**
     * 处理数据，向外抛值
     *
     * @memberof AppModal
     */
    handleCloseModel() {
      if (this.subject && this.tempResult) {
        this.subject.next(this.tempResult);
      }
      this.model?.dismiss();
    },
  },
  /**
   * 绘制内容
   *
   * @memberof AppFromGroup
   */
  render() {
    return h(this.viewComponent, {
      viewShowMode: 'MODEL',
      navContext: this.navContext,
      navParam: this.navParam,
      navDatas: this.navDatas,
      viewPath: this.viewPath,
      modelData: this.modelData,
      class: this.customClass,
      onViewEvent: ({ viewName, action, data }: { viewName: string; action: string; data: any }) => {
        this.handleViewEvent(viewName, action, data);
      },
    });
  },
});
