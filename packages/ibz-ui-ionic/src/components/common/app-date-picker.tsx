import { defineComponent } from 'vue';
import moment from 'moment';
const AppDatePickerProps = {
  /**
   * 双向绑定值
   * @type {any}
   * @memberof AppDatePickerProps
   */
  value: String,

  /**
   * placeholder值
   * @type {String}
   * @memberof AppDatePickerProps
   */
  placeholder: String,

  /**
   * 是否禁用
   * @type {boolean}
   * @memberof AppDatePickerProps
   */
  disabled: Boolean,

  /**
   * 只读模式
   *
   * @type {boolean}
   * @memberof AppDatePickerProps
   */
  readonly: Boolean,

  /**
   * 日期格式
   *
   * @type {string}
   * @memberof AppDatePickerProps
   */
  displayFormat: {
    type: String,
    default: 'YYYY-MM-DD HH:mm:ss',
  },
};

export const AppDatePicker = defineComponent({
  name: 'AppDatePicker',
  props: AppDatePickerProps,
  emits: ['editorValueChange'],
  setup: () => {
    const currentDate: any = new Date().getFullYear();
    const min: any = currentDate - 100;
    const max: any = currentDate + 100;
    return {
      min,
      max,
    };
  },
  methods: {
    /**
     * 输入框值改变事件
     *
     * @param {*} e
     */
    valueChange(event: any) {
      let tempValue: any = null;
      tempValue = moment(event.detail.value).format(this.displayFormat);
      if (Object.is(tempValue, 'Invalid date')) {
        tempValue = event.detail.value;
      }
      if (Object.is(this.value, tempValue)) {
        return;
      }
      this.$emit('editorValueChange', tempValue);
    },
  },
  render() {
    console.log(this.displayFormat,111);
    
    return (
      <ion-datetime
        class='app-date-picker'
        value={this.value}
        disabled={this.disabled}
        readonly={this.readonly}
        min={this.min}
        max={this.max}
        cancelText={(this as any).$tl('share.cancel','取消')}
        doneText={(this as any).$tl('share.ok','确认')}
        placeholder={this.placeholder}
        displayFormat={this.displayFormat}
        onIonChange={(e: any) => {
          this.valueChange(e);
        }}
      ></ion-datetime>
    );
  },
});
