import { defineComponent } from 'vue';
const AppSwitchProps = {
  /**
   * 双向绑定值
   * @type {any}
   * @memberof InputBox
   */
  value: {
    type: String,
  },

  /**
   * 是否禁用
   * @type {boolean}
   * @memberof InputBox
   */
  disabled: Boolean,

  /**
   * 只读模式
   *
   * @type {boolean}
   */
  readonly: Boolean,
};

export const AppSwitch = defineComponent({
  name: 'AppSwitch',
  props: AppSwitchProps,
  emits: ['editorValueChange'],
  methods: {
    /**
     * 开关值改变事件
     *
     * @param {*} e
     */
    valueChange(e: any) {
      this.$emit('editorValueChange', e.detail.checked == true ? '1' : '0');
    },
  },
  computed: {
    curValue(): boolean {
      return this.value == '1' ? true : false;
    },
  },
  render() {
    return (
      <div class='app-switch'>
        <ion-toggle
          class={{'app-switch--readonly': this.readonly}}
          disabled={this.disabled}
          checked={this.curValue}
          onIonChange={(e: any) => {
            this.valueChange(e);
          }}
        ></ion-toggle>
      </div>
    );
  },
});
