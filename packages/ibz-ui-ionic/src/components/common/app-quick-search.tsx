import { throttle } from 'ibz-core';
import { ComponentBase, GenerateComponent } from '../component-base';
export class AppQuickSearchProps {}

export class AppQuickSearch extends ComponentBase<AppQuickSearchProps> {
  /**
   * @description 快速搜索
   * @param {*} $event
   * @memberof AppQuickSearch
   */
  public quickSearch($event: any) {
    this.ctx.emit('quickSearch', $event.detail.value);
  }

  /**
   * @description 绘制快速搜索栏
   * @return {*}
   * @memberof AppQuickSearch
   */
  render() {
    return (
      <ion-header class='app-quick-search'>
        <ion-toolbar>
          <ion-searchbar
            showClearButton
            debounce='500'
            onIonChange={($event: any) => throttle(this.quickSearch, [$event], this)}
          ></ion-searchbar>
        </ion-toolbar>
      </ion-header>
    );
  }
}
export const AppQuickSearchComponent = GenerateComponent(AppQuickSearch);
