import { defineComponent } from 'vue';
const AppLayoutContainerProps = {
  /**
   * @description 样式类名对象
   * @type {*}
   * @memberof AppLayoutContainerProps
   */
  class: {
    type: Object,
  },
  /**
   * @description 样式对象
   * @type {*}
   * @memberof AppLayoutContainerProps
   */
  style: {
    type: String,
    default: '',
  },
  /**
   * @description 样式对象
   * @type {*}
   * @memberof AppLayoutContainerProps
   */
  layout: {
    type: Object,
    default: {},
  },
};

export const AppLayoutContainer = defineComponent({
  name: 'AppLayoutContainer',
  props: AppLayoutContainerProps,
  methods: {
    /**
     * @description 合并样式类名
     * @param {*} arg1 类名对象
     * @param {(any | string)} arg2 类名字符串或对象
     */
    mergeClassNames(arg1: any, arg2: any | string) {
      if (typeof arg2 == 'string') {
        arg2.split(' ').map((item: string) => {
          Object.assign(arg1, {
            [item]: true,
          });
        });
      } else {
        Object.assign(arg1, arg2);
      }
    },
  },
  render() {
    // 插槽内容
    const defaultContent = (this.$slots.default as any)?.();

    let layoutMode = this.layout?.layout;

    if (this.layout && layoutMode == 'FLEX') {
      //  FLEX布局

      // 样式处理
      let cssStyle: string = 'width: 100%; height: 100%; overflow: auto; display: flex;';
      cssStyle += this.layout.dir ? `flex-direction: ${this.layout.dir};` : '';
      cssStyle += this.layout.align ? `justify-content: ${this.layout.align};` : '';
      cssStyle += this.layout.vAlign ? `align-items: ${this.layout.vAlign};` : '';
      cssStyle += this.style;

      // 类名处理
      let classNames: any = {
        'app-layout-container': true,
        'app-layout-container--flex': true,
      };
      this.mergeClassNames(classNames, this.class);

      // 绘制flex的父容器
      return (
        <div class={classNames} style={cssStyle}>
          {defaultContent}
        </div>
      );
    } else {
      //  栅格布局

      // 样式处理
      let cssStyle = 'height: 100%;';
      cssStyle += this.style;

      // 类名处理
      let classNames: any = {
        'app-layout-container': true,
        'app-layout-container--grid': true,
      };
      this.mergeClassNames(classNames, this.class);

      // 绘制栅格的父容器
      return (
        <ion-row class={classNames} style={cssStyle}>
          {defaultContent}
        </ion-row>
      );
    }
  },
});
