import { defineComponent } from 'vue';
import * as ionicons from 'ionicons/icons';
import { Util, ViewTool } from 'ibz-core';
const AppIconProps = {
  /**
   * 图标名称
   *
   * @type {string}
   * @memberof AppIconProps
   */
  name: String,

  /**
   * 图标对象
   *
   * @type {string}
   * @memberof AppIconProps
   */
  icon: Object,

  /**
   * 图标路径
   *
   * @type {string}
   * @memberof AppIconProps
   */
  iconSrc: String,
};

export const AppIcon = defineComponent({
  name: 'AppIcon',
  props: AppIconProps,
  computed: {
    /**
     * iconName
     *
     * @returns string
     */
    iconName() {
      let name = '';
      if (this.name) {
        name = Util.formatCamelCase(this.name);
      } else if (this.icon?.cssClass) {
        name = Util.formatCamelCase(ViewTool.setIcon(this.icon.cssClass));
      }
      return name ? (ionicons as any)[name] : null;
    },
    /**
     * imagePath
     *
     * @returns string
     */
    imagePath() {
      let src = '';
      if (this.iconSrc) {
        src = this.iconSrc;
      } else {
        src = this.icon?.imagePath;
      }
      return src;
    },
  },
  render() {
    if (this.imagePath) {
      return <ion-icon class='app-icon' src={this.imagePath}></ion-icon>
    } else if (this.iconName) {
      return <ion-icon class='app-icon' icon={this.iconName}></ion-icon>
    } else {
      return null;
    }
  },
});
