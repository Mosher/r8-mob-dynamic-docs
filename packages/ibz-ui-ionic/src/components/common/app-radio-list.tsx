import { defineComponent } from 'vue';
import { IPSAppCodeList } from '@ibiz/dynamic-model-api';
import { LogUtil } from 'ibz-core';
const AppRadioListProps = {
  name: {
    type: String,
  },
  codeList: {
    type: Object,
  },
  context: {
    type: Object,
    default: {},
  },
  value: {
    type: String,
  },
  disabled: {
    type: Boolean,
    default: false,
  },
  readonly: {
    type: Boolean,
    default: false,
  },
  modelService: Object,
};

export const AppRadioList = defineComponent({
  name: 'AppRadioList',
  props: AppRadioListProps,
  emits: ['editorValueChange'],
  data() {
    const codeListService = App.getCodeListService();
    return {
      codeListService: codeListService,
      options: [],
    };
  },
  created() {
    if (this.codeList) {
      this.loadItems();
    }
  },
  methods: {
    async loadItems(): Promise<any> {
      const tag = (this.codeList as IPSAppCodeList).codeName;
      const type = (this.codeList as IPSAppCodeList).codeListType;
      this.codeListService?.getDataItems({ tag: tag, type: type, navContext: this.context })
        .then((codeListItems: any) => {
          this.options = codeListItems;
        })
        .catch((error: any) => {
          LogUtil.log(`----${tag}----${(this as any).$tl('common.radiolist.notfount', '未找到')}`);
        });
    },
    valueChange(event: any) {
      if (event && event.detail && event.detail.value) {
        this.$emit('editorValueChange', event.detail.value.toString());
      }
    },
  },
  render() {
    return (
      <ion-radio-group
        class='app-radio-list'
        value={this.value}
        onIonChange={this.valueChange.bind(this)}
      >
        {this.options.map((option: any, index: number) => {
          return (
            <ion-item class='radio-list-item' key={index}>
              <ion-label>{option.label}</ion-label>
              <ion-radio disabled={this.disabled || this.readonly} name={option.value} value={option.value} />
            </ion-item>
          );
        })}
      </ion-radio-group>
    );
  },
});
