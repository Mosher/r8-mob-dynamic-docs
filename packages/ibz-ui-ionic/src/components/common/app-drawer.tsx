import { defineComponent, h, ref, toRefs } from 'vue';
import { menuController } from '@ionic/core';
import { Util, AppViewEvents } from 'ibz-core';
const ModelProps = {
  /**
   * 导航上下文
   *
   * @type {any}
   * @memberof ModelProps
   */
  navContext: {
    type: Object,
    default: {},
  },
  /**
   * 视图上下文参数
   *
   * @type {any}
   * @memberof ModelProps
   */
  navParam: {
    type: Object,
    default: {},
  },
  /**
   * 导航数据
   *
   * @type {*}
   * @memberof ModelProps
   */
  navDatas: {
    type: Array,
    default: [],
  },
  view: {
    type: Object,
  },
  model: {
    type: Object,
  },
  subject: {
    type: Object,
  },
};
export default defineComponent({
  props: ModelProps,
  /**
   * vue 生命周期
   *
   * @param {*} props
   * @return {*}
   */
  setup(props: any) {
    const { view } = toRefs(props);

    /**
     * 临时结果
     *
     * @type {any}
     * @memberof AppDrawer
     */
    const tempResult: any = { ret: '' };

    /**
     * 视图名称
     *
     * @type {string}
     * @memberof AppDrawer
     */
    const viewComponent: string = view.value.viewComponent;

    /**
     *  视图动态路径
     *
     * @type {*}
     */
    const viewPath: string = view.value.viewPath;

    /**
     *  打开模式
     *
     * @type {*}
     */
    const side: string = view.value.placement == 'DRAWER_RIGHT' ? 'end' : 'start';

    /**
     * 视图层级
     *
     * @type {any}
     * @memberof AppDrawer
     */
    const zIndex: any = null;

    const uuid = Util.createUUID();

    const menu: any = ref('menu');

    /**
     * 自定义类名
     */
    const customClass = view.value.customClass;

    /**
     * 自定义样式
     */
    const customStyle = view.value.customStyle;

    /**
     * 模型数据
     *
     * @type {any}
     * @memberof AppModal
     */
    const modelData: any = view.value.viewModel;

    return {
      tempResult,
      viewPath,
      viewComponent,
      zIndex,
      menu,
      side,
      customClass,
      uuid,
      customStyle,
      modelData,
    };
  },
  methods: {
    /**
     * 视图事件
     *
     * @param viewName 视图名
     * @param action 视图行为
     * @param data 行为数据
     */
    handleViewEvent(viewName: string, action: string, data: any) {
      switch (action) {
        case AppViewEvents.CLOSE:
          this.close(data);
          break;
        case AppViewEvents.DATA_CHANGE:
          this.dataChange(data);
          break;
      }
    },

    /**
     * 视图关闭
     *
     * @memberof AppDrawer
     */
    close(result: any) {
      if (result && Array.isArray(result) && result.length > 0) {
        Object.assign(this.tempResult, { ret: 'OK', datas: Util.deepCopy(result) });
      }
      this.handleCloseDrawer();
    },

    /**
     * 视图数据变化
     *
     * @memberof AppDrawer
     */
    dataChange(result: any) {
      this.tempResult = { ret: '' };
      if (result && Array.isArray(result) && result.length > 0) {
        Object.assign(this.tempResult, { ret: 'OK', datas: Util.deepCopy(result) });
      }
    },

    /**
     * 处理数据，向外抛值
     *
     * @memberof AppDrawer
     */
    handleCloseDrawer() {
      if (this.subject && this.tempResult) {
        this.subject.next(this.tempResult);
      }
      menuController.close(this.uuid).then(() => {
        this.model?.dismiss();
      });
    },
  },
  async mounted() {
    await menuController.enable(true, this.uuid);
    menuController.open(this.uuid);
  },
  /**
   * 绘制内容
   *
   * @memberof AppFromGroup
   */
  render() {
    return (
      <div style={{ zIndex: this.zIndex, height: '100%' }}>
        <div id={this.uuid}></div>
        <ion-content>
          <ion-menu
            ref='menu'
            side={this.side}
            onIonDidClose={($event: any) => {
              setTimeout(() => {
                this.close(null);
              }, 1000);
            }}
            contentId={this.uuid}
            menuId={this.uuid}
            style={this.customStyle}
          >
            {h(this.viewComponent, {
              viewShowMode: 'MODEL',
              navContext: this.navContext,
              navParam: this.navParam,
              navDatas: this.navDatas,
              viewPath: this.viewPath,
              class: this.customClass,
              modelData: this.modelData,
              onViewEvent: ({ viewName, action, data }: { viewName: string; action: string; data: any }) => {
                this.handleViewEvent(viewName, action, data);
              },
            })}
          </ion-menu>
        </ion-content>
      </div>
    );
  },
});
