import { IPSAppMenuItem } from '@ibiz/dynamic-model-api';
import { IParam } from 'ibz-core';
import { ComponentBase, GenerateComponent } from '../component-base';

export class AppMenuListProps {
  /**
   * @description 菜单
   * @type {any[]}
   * @memberof AppMenuListProps
   */
  menu: any[] = [];

  /**
   * @description 应用界面服务对象
   * @type {IParam}
   * @memberof AppMenuIconProps
   */
  appUIService: IParam = {};

  /**
   * 模型服务
   *
   * @type {IParam}
   * @memberof AppMenuIconProps
   */
  modelService: IParam = {};
}

export class AppMenuList extends ComponentBase<AppMenuListProps> {
  /**
   * 菜单项集合
   *
   * @type IPSAppMenuItem[]
   * @memberof AppMenuList
   */
  public menuItems: IPSAppMenuItem[] = [];

  /**
   * vue 生命周期
   *
   * @memberof AppMenuList
   */
  setup() {
    this.menuItems = this.props.menu;
  }

  /**
   * 打开视图
   *
   * @public
   * @memberof AppMenuList
   */
  public openView(item: IPSAppMenuItem) {
    this.ctx.emit('menuClick', item);
  }

  /**
   * @description 根据菜单项获取菜单权限
   * @param {IPSAppMenuItem} menuItem
   * @return {*}
   * @memberof AppMenuList
   */
  public getMenusPermission(menuItem: IPSAppMenuItem) {
    if (!App.isPreviewMode() && menuItem.accessKey) {
      return this.props.appUIService.getResourceOPPrivs(menuItem.accessKey);
    } else {
      return true;
    }
  }

  /**
   * 绘制快速菜单图标项
   *
   * @public
   * @param {IPSAppMenuItem} item
   * @memberof AppMenuList
   */
  public renderQuickMenuListItem(item: IPSAppMenuItem) {
    if (item.hidden || !this.getMenusPermission(item)) {
      return null;
    }
    const cssName: string = item.getPSSysCss()?.cssName || '';
    const icon = item.getPSSysImage();
    return (
      <ion-item class={[cssName, 'menu-list-item']} onClick={() => this.openView(item)}>
        {icon ? <app-icon icon={icon}></app-icon> : <app-icon name='home'></app-icon>}
        <ion-label>
          {this.$tl(item.getCapPSLanguageRes()?.lanResTag, item.caption)}
        </ion-label>
        <app-icon name='chevron-forward-outline'></app-icon>
      </ion-item>
    );
  }

  /**
   * 绘制内容
   *
   * @memberof AppMenuList
   */
  render() {
    if (this.menuItems.length > 0) {
      return (
        <ion-list class='app-menu-list'>
          {this.menuItems.map((item: IPSAppMenuItem) => {
            return this.renderQuickMenuListItem(item);
          })}
        </ion-list>
      );
    }
  }
}

export const AppMenuListComponent = GenerateComponent(AppMenuList, Object.keys(new AppMenuListProps()));
