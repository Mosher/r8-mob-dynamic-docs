import { defineComponent } from 'vue';
const AppLayoutItemProps = {
  /**
   * @description 样式类名对象
   * @type {*}
   * @memberof AppLayoutItemProps
   */
  class: {
    type: Object,
  },
  /**
   * @description 样式对象
   * @type {*}
   * @memberof AppLayoutItemProps
   */
  style: {
    type: String,
    default: '',
  },
  /**
   * @description 布局模式
   * @type {*}
   * @memberof AppLayoutItemProps
   */
  layoutMode: {
    type: String,
    default: 'TABLE_12COL',
  },
  /**
   * @description 布局位置模型对象
   * @type {*}
   * @memberof AppLayoutItemProps
   */
  layoutPos: {
    type: Object,
    default: {},
  },
};

export const AppLayoutItem = defineComponent({
  name: 'AppLayoutItem',
  props: AppLayoutItemProps,
  methods: {
    /**
     * @description 合并样式类名
     * @param {*} arg1 类名对象
     * @param {(any | string)} arg2 类名字符串或对象
     */
    mergeClassNames(arg1: any, arg2: any | string) {
      if (typeof arg2 == 'string') {
        arg2.split(' ').map((item: string) => {
          Object.assign(arg1, {
            [item]: true,
          });
        });
      } else {
        Object.assign(arg1, arg2);
      }
    },
    /**
     * @description 获取栅格布局
     * @param {*} parent 父容器
     * @param {*} child 子容器
     * @return {*}
     * @memberof AppMobPanel
     */
    getGridLayoutProps(layoutMode: string, layoutPos: any) {
      let { colXS, colSM, colMD, colLG, colXSOffset, colSMOffset, colMDOffset, colLGOffset } = layoutPos;
      // 设置初始值
      colXS = !colXS || colXS == -1 ? 12 : colXS / 2;
      colSM = !colSM || colSM == -1 ? 12 : colSM / 2;
      colMD = !colMD || colMD == -1 ? 12 : colMD / 2;
      colLG = !colLG || colLG == -1 ? 12 : colLG / 2;
      colXSOffset = !colXSOffset || colXSOffset == -1 ? 0 : colXSOffset / 2;
      colSMOffset = !colSMOffset || colSMOffset == -1 ? 0 : colSMOffset / 2;
      colMDOffset = !colMDOffset || colMDOffset == -1 ? 0 : colMDOffset / 2;
      colLGOffset = !colLGOffset || colLGOffset == -1 ? 0 : colLGOffset / 2;
      if (layoutMode == 'TABLE_12COL') {
        // 重新计算12列的栅格数值
        colXS = Math.min(colXS * 2, 12);
        colSM = Math.min(colSM * 2, 12);
        colMD = Math.min(colMD * 2, 12);
        colLG = Math.min(colXS * 2, 12);
        // 重新计算12列的栅格偏移
        const sign = (num: number) => (num == 0 ? 0 : num / Math.abs(num));
        colXSOffset = sign(colXSOffset) * Math.min(colXSOffset * 2, 12);
        colSMOffset = sign(colSMOffset) * Math.min(colSMOffset * 2, 12);
        colMDOffset = sign(colMDOffset) * Math.min(colMDOffset * 2, 12);
        colLGOffset = sign(colLGOffset) * Math.min(colLGOffset * 2, 12);
      }
      return {
        'size-lg': colLG,
        'size-md': colMD,
        'size-sm': colSM,
        'size-xs': colXS,
        'offset-lg': colLGOffset,
        'offset-md': colMDOffset,
        'offset-sm': colSMOffset,
        'offset-xs': colXSOffset,
      };
    },
    /**
     * 计算容器尺寸
     *
     * @param {number} value
     * @return {*}  {string}
     */
    calcBoxSize(value: number, enableRem: boolean = true): string {
      if (value > 1) {
        return enableRem ? value / 50 + 'rem' : value + 'px';
      } else if (value > 0) {
        return value * 100 + '%';
      }
      return '100%';
    },
  },
  render() {
    // 插槽内容
    const defaultContent = (this.$slots.default as any)?.();

    // 通用样式处理
    const { width, height, grow } = this.layoutPos;
    let cssStyle: string = '';
    cssStyle += width ? `width: ${this.calcBoxSize(width)};` : '';
    cssStyle += height ? `height: ${this.calcBoxSize(height)};` : '';

    if (this.layoutPos && this.layoutMode == 'FLEX') {
      //  FLEX布局

      // 样式处理
      cssStyle += grow ? `flex-grow: ${grow != -1 ? grow : 0};` : '';
      cssStyle += this.style;

      // 类名处理
      let classNames: any = {
        'app-layout-item': true,
        'app-layout-item--flex': true,
      };
      this.mergeClassNames(classNames, this.class);

      // 绘制flex的父容器
      return (
        <div class={classNames} style={cssStyle}>
          {defaultContent}
        </div>
      );
    } else {
      //  栅格布局

      // 样式处理
      cssStyle += this.style;

      // 类名处理
      let classNames: any = {
        'app-layout-item': true,
        'app-layout-item--grid': true,
      };
      this.mergeClassNames(classNames, this.class);

      const attrs = this.getGridLayoutProps(this.layoutMode, this.layoutPos);

      // 绘制栅格的父容器
      return (
        <ion-col class={classNames} style={cssStyle} {...attrs}>
          {defaultContent}
        </ion-col>
      );
    }
  },
});
