import { defineComponent, toRefs, ref } from 'vue';
import { ViewTool } from 'ibz-core';

const AppQuickGroupTabProps = {
  /**
   * 快速分组项
   *
   * @type {Array}
   */
  items: {
    type: Array,
  },
};

export const AppQuickGroup = defineComponent({
  name: 'app-quick-group',
  props: AppQuickGroupTabProps,
  setup(prop: any, ctx: any) {
    //  渲染列表
    let showItems: any[] = [];
    //  子项列表
    const subItems: any[] = [];
    //  当前选中项
    const selectedUIItem: any = ref({});
    //  解构items
    const { items } = toRefs(prop);

    /**
     * 处理数据集
     *
     * @memberof AppQuickGroupTabComponent
     */
    const handleDataSet = (result: any[]) => {
      const list: Array<any> = [];
      if (result.length === 0) {
        return list;
      }
      result.forEach((codeItem: any) => {
        if (!codeItem.pvalue) {
          const valueField: string = codeItem.value;
          setChildCodeItems(valueField, result, codeItem);
          list.push(codeItem);
        }
      });
      return list;
    };

    /**
     * 设置子项数据
     *
     * @memberof AppQuickGroupTabComponent
     */
    const setChildCodeItems = (pValue: string, result: Array<any>, codeItem: any) => {
      result.forEach((item: any) => {
        if (item.pvalue == pValue) {
          const valueField: string = item.value;
          setChildCodeItems(valueField, result, item);
          if (!codeItem.children) {
            codeItem.children = [];
          }
          codeItem.children.push(item);
        }
      });
    };

    /**
     * 处理点击事件
     *
     * @memberof AppQuickGroupTabComponent
     */
    const handleClick = (item: any, isFirst: boolean = false) => {
      selectedUIItem.value = item;
      if (item.children) {
        if (subItems.length > 0) {
          subItems.length = 0;
        } else {
          if (!isFirst) {
            subItems.push(...item.children);
          }
        }
      } else {
        subItems.length = 0;
        items.value.forEach((item: any) => {
          item.selected = false;
          item.childSelected = false;
          item.selectChildLabel = '';
        });
        item.selected = true;
        if (item.pvalue) {
          items.value.forEach((item: any) => {
            if (item.value === item.pvalue) {
              item.childSelected = true;
              item.selectChildLabel = item.label;
            }
          });
        }
      }
      ctx.emit('valueChange', item);
    };

    if (items && items.value && items.value.length > 0) {
      const select = items.value.find((item: any) => {
        return item.default;
      });
      showItems = handleDataSet(items.value);
      handleClick(select ? select : items.value[0]);
    }

    return {
      showItems,
      subItems,
      selectedUIItem,
      handleClick,
    };
  },
  methods: {
    /**
     * 是否为选中项
     *
     * @param item
     * @returns {boolean}
     * @memberof AppQuickGroupTabComponent
     */
    isSelectedItem(item: any): boolean {
      if (this.selectedUIItem && this.selectedUIItem.id === item.id) {
        return true;
      } else {
        return false;
      }
    },

    /**
     * 关闭返回框
     *
     * @memberof AppQuickGroupTabComponent
     */
    closeBackdrop() {
      this.subItems.length = 0;
      this.$forceUpdate();
    },
  },

  /**
   * 组件渲染
   *
   * @memberof AppQuickGroupTabComponent
   */
  render() {
    return [
      <div class='app-quick-group'>
        {this.items &&
          this.items.map((item: any, index: number) => {
            return (
              <div
                key={index}
                class={{ 'group-item': true, 'group-item--active': this.isSelectedItem(item) || item.childSelected }}
                onClick={() => {
                  this.handleClick(item);
                }}
              >
                <div style={{ color: item.color }}>
                  {item.ioncls ? (
                    <ion-icon name={ViewTool.setIcon(item.iconcls)}></ion-icon>
                  ) : item.icon ? (
                    <img src={item.icon} />
                  ) : null}
                  <span class='group-item-label'>{item.selectChildLabel ? item.selectChildLabel : item.label}</span>
                  {item.children ? <ion-icon name='caret-down-outline' style='margin-left: 4px;'></ion-icon> : null}
                </div>
              </div>
            );
          })}
      </div>,
      <div
        ref='child-list'
        class={{ 'app-quick-group-popup': true, 'app-quick-group-popup--open': this.subItems.length > 0 }}
      >
        {this.subItems.map((item: any, index: number) => {
          return (
            <div
              key={index}
              class={{ 'group-popup-item': true, 'group-popup-item--active': item.selected }}
              onClick={() => {
                this.handleClick(item);
              }}
            >
              <span>
                {item.ioncls ? (
                  <ion-icon name={ViewTool.setIcon(item.iconcls)}></ion-icon>
                ) : item.icon ? (
                  <img src={item.icon} />
                ) : null}
                <span>{item.label}</span>
              </span>
              {item.selected ? (
                <ion-icon size='small' style='margin-left:auto; color:green;' name='checkmark-outline'></ion-icon>
              ) : null}
            </div>
          );
        })}
      </div>,
      this.subItems.length > 0 ? (
        <ion-backdrop
          style='height: 100vh; z-index: -1;'
          visible='true'
          tappable='true'
          onIonBackdropTap={this.closeBackdrop.bind(this)}
        ></ion-backdrop>
      ) : null,
    ];
  },
});
