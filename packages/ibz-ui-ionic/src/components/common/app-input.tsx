
import { defineComponent } from 'vue';
const AppInputProps = {
  /**
   * 双向绑定值
   * @type {any}
   * @memberof InputBox
   */
  value: [String, Number],

  /**
   * placeholder值
   * @type {String}
   * @memberof InputBox
   */
  placeholder: String,

  /**
   * 是否禁用
   * @type {boolean}
   * @memberof InputBox
   */
  disabled: Boolean,

  /**
   * 只读模式
   *
   * @type {boolean}
   */
  readonly: Boolean,

  /**
   * 属性类型
   *
   * @type {string}
   * @memberof InputBox
   */
  type: String,

  /**
   * 最大值
   * @type {string}
   * @memberof InputBox
   */
  maxValue: Number,

  /**
   * 最小值
   * @type {string}
   * @memberof InputBox
   */
  minValue: Number,

  /**
   * 最大长度
   * @type {string}
   * @memberof InputBox
   */
  maxLength: Number,

  /**
   * 最小长度
   * @type {string}
   * @memberof InputBox
   */
  minLength: Number,

  /**
   * 是否显示最大长度
   * @type {string}
   * @memberof InputBox
   */
  showMaxLength: Boolean,

  /**
   * 浮点精度
   * @type {string}
   * @memberof InputBox
   */
  precision: Number,

  /**
   * 模型服务
   * @type {Object}
   * @memberof InputBox
   */
  modelService: Object
};

export const AppInput = defineComponent({
  name: 'AppInput',
  props: AppInputProps,
  emits: ['editorValueChange'],
  methods: {
    /**
     * @description 输入框blur事件
     */
    IonBlur(e: any) {
      if (e.target.value) {
        if (Object.is(this.type, 'number') && this.precision) {
          this.$emit('editorValueChange', Number(e.target.value).toFixed(Number(this.precision)));
        } else {
          this.$emit('editorValueChange', e.target.value);
        }
      }
    },
  },
  render() {
    return (
      <div class='app-input'>
        <ion-input
          value={this.value}
          max={this.maxValue}
          min={this.minValue}
          maxlength={this.maxLength}
          minlength={this.minLength}
          disabled={this.disabled}
          readonly={this.readonly}
          placeholder={this.showMaxLength ? `${(this as any).$tl('common.input.maxlength','最大内容长度为')}${this.maxLength}` : this.placeholder}
          type={this.type}
          onIonBlur={(e: any) => {
            this.IonBlur(e);
          }}
        ></ion-input>
      </div>
    );
  },
});

