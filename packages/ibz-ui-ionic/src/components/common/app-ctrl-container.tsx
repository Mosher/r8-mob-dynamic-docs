import { defineComponent, h } from 'vue';
/**
 * @description 部件容器输入参数
 */
const AppCtrlContainerProps = {
  ctrlName: {
    type: String,
    default: '',
  },
};

/**
 * @description 部件容器组件
 */
export const AppCtrlContainer = defineComponent({
  name: 'AppCtrlContainer',
  props: AppCtrlContainerProps,
  setup(props: any, { slots }) {
    return () => {
      return h('div', { class: 'ctrl-container' }, slots.default && slots.default());
    };
  },
});
