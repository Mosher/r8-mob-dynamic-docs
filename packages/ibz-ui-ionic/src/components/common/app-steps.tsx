import { defineComponent } from 'vue';
import { IParam } from 'ibz-core';

const AppStepsProps = {
  steps: {
    type: Array
  },
  active: {
    type: String || Number,
    default: ''
  },
  panelInstance: Object
}

/**
 * @description 步骤条组件
 */
export const AppStepsComponent = defineComponent({
  name: 'AppSteps',
  props: AppStepsProps,
  setup(props: any, { emit }: any) {
    const stepItemClick = (step: IParam, event: any) => {
      event.stopPropagation();
      emit('stepClick', step, event);
    }

    const renderStep = (step: IParam, index: number) => {
      const { active } = props;
      let _index: number = 0;
      if ((typeof active) == 'number') {
        _index = active >= 0 ? active : 0;
      } else {
        _index = isNaN(Number(active)) ? 0 : Number(active);
      }
      return (
        <div
          class={[
            'step-item',
            _index >= index ? _index == index ? 'step-item--active' : 'step-item--finish' : ''
          ]}
          onClick={(event: any) => { stepItemClick(step, event) }}>
          <div class={["step-item__title", step.className ]}>
            {step.icon ? <app-icon name={step.icon}/> : null}
            {props.panelInstance.$tl(step.titleLanResTag,step.title)}
          {step.subTitle ? [<br/>,<span class="step-item__subtitle">{props.panelInstance.$tl(step.subTitleLanResTag,step.subTitle)}</span>] : null}
          </div>
          <div class="step-item__container">
            <i class={_index == index ? "step-item__circle--active" : 'step-item__circle'} />
          </div>
          <div class="step-item__line"></div>
        </div>
      )
    }

    return () => {
      const { steps } = props;
      return (
        <div class="app-steps">
          <div class={["steps-items", steps.find((step: IParam) => step.subTitle) ? 'has-subtitle' : '']}>
            {steps ? steps.map((step: IParam, index: number) => {
              return renderStep(step, index);
            }) : null}
          </div>
        </div>
      )
    }
  }
})