import { ComponentBase, GenerateComponent } from '../component-base';
import { IParam } from 'ibz-core';
export class AppIconItemProps {
  /**
   * @description 项数据
   * @type {IParam}
   * @memberof AppIconItemProps
   */
  item: IParam = {};

  /**
   * @description 索引
   * @type {number}
   * @memberof AppIconItemProps
   */
  index: number = 0;

  /**
   * @description 值格式化
   * @type {string}
   * @memberof AppIconItemProps
   */
  valueFormat: string = '';
}
export class AppIconItem extends ComponentBase<AppIconItemProps> {
  /**
   * @description 项数据
   * @type {IParam}
   * @memberof AppIconItem
   */
  public item: IParam = {};

  /**
   * @description 索引
   * @type {number}
   * @memberof AppIconItem
   */
  public index: number = 0;

  /**
   * @description 值格式化
   * @type {string}
   * @memberof AppIconItem
   */
  valueFormat: string = '';

  /**
   * @description 设置响应式
   * @memberof AppIconItem
   */
  setup() {
    this.initInputData(this.props);
  }

  /**
   * @description 初始化输入属性
   * @param {AppIconItemProps} opts 输入对象
   * @memberof AppIconItem
   */
  public initInputData(opts: AppIconItemProps) {
    this.item = opts.item;
    this.index = opts.index;
    this.valueFormat = opts.valueFormat;
  }

  /**
   * @description 输入属性值变更
   * @memberof AppIconItem
   */
  watchEffect() {
    this.initInputData(this.props);
    this.getIndexText();
  }

  /**
   * @description 获取索引样式
   * @memberof AppIconItem
   */
  getIndexText() {
    const colorArray = ['#ffa600', '#498cf2', '#f76e9a', '#f56ef7', '#a56ef7'];
    if (this.item?.srfmajortext) {
      this.item.indexText = this.item.srfmajortext[0];
    }
    this.item.indexColor = { 'background-color': colorArray[this.index % colorArray.length] };
  }

  /**
   * @description 点击事件
   * @memberof AppIconItem
   */
  onClick() {
    this.ctx.emit('itemClick', this.item);
  }

  /**
   * @description 绘制
   * @return {*}
   * @memberof AppIconItem
   */
  render() {
    return (
      <div class='app-icon-item' onClick={() => this.onClick()}>
        <div class='icon-item-container'>
          {this.item?.iconsrc ? (
            <app-icon iconSrc={this.item.iconsrc}></app-icon>
          ) : (
            <div class='icon-item-text' style={this.item?.indexColor}>
              {this.item?.indexText}{' '}
            </div>
          )}
        </div>
        <div class='icon-item-title'>
          <span v-format={this.valueFormat}>{this.item?.srfmajortext}</span>
        </div>
      </div>
    );
  }
}
export const AppIconItemComponent = GenerateComponent(AppIconItem, Object.keys(new AppIconItemProps()));
