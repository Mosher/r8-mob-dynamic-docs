import { defineComponent } from 'vue';
const AppMobAlertProps = {
  /**
   * 视图消息标示
   *
   * @type {string}
   */
  tag: {
    type: String,
  },

  /**
   * 显示位置
   *
   * @type {string}
   */
  position: {
    type: String,
  },

  /**
   * 应用上下文
   *
   * @type {*}
   */
  context: {
    type: Object,
  },

  /**
   * 视图参数
   *
   * @type {*}
   */
  viewParam: {
    type: Object,
  },

  /**
   * 视图消息集合
   *
   * @type {Array<any>}
   */
  messageDetails: {
    type: Array,
  },

  /**
   * 视图消息组标识
   *
   * @type {string}
   */
  infoGroup: {
    type: String,
  },

  /**
   * 视图名称
   *
   * @type {string}
   */
  viewName: {
    type: String,
  },
};

export const AppMobAlert = defineComponent({
  name: 'AppMobAlert',
  props: AppMobAlertProps,
  setup() {
    const items: any[] = [];
    return {
      items,
    };
  },
  created() {
    this.handleItems();
  },
  methods: {
    handleItems() {
      if (this.messageDetails && this.messageDetails.length > 0) {
        this.messageDetails.forEach((detail: any) => {
          this.handleItemOption(detail);
          const flag = this.handleItemCloseMode(detail);
          this.handleItemPosition(detail, flag);
          this.items.push(detail);
        });
      }
    },
    handleItemOption(detail: any) {
      //  是否存在内容
      detail.hasContent = true;
      if (!detail.title && !detail.content) {
        detail.hasContent = false;
      }
      //  关闭模式
      detail.closeable = detail.enableRemove;
      //  类型
      switch (detail.type) {
        case 'WARN':
          detail.type = 'warning';
          break;
        case 'SUCCESS':
          detail.type = 'success';
          break;
        case 'ERROR':
          detail.type = 'error';
          break;
        default:
          detail.type = 'info';
          break;
      }
    },

    /**
     * 处理数据关闭模式
     *
     * @memberof AppMobAlert
     */
    handleItemCloseMode(data: any) {
      let flag = true;
      data.showState = true;
      if (data.removeMode || data.removeMode == 0) {
        if (data.removeMode == 1) {
          const tag = this.viewName + '_' + this.infoGroup + '_' + data.codeName;
          const codeName = localStorage.getItem(tag);
          if (codeName) {
            data.showState = false;
            flag = false;
          }
        }
        if (data.removeMode == 0) {
          data.closeable = false;
        }
      }
      return flag;
    },

    /**
     * 处理数据显示位置
     *
     * @memberof AppMobAlert
     */
    handleItemPosition(data: any, flag: boolean) {
      if (data.position) {
        if (flag && Object.is('POPUP', data.position)) {
          //TODO 待补充弹出
          // const h = this.$createElement;
          // data.showState = false;
          // if(Object.is('HTML',data.messageType)) {
          //     //1.首先动态创建一个容器标签元素，如DIV
          //     let temp:any = document.createElement("div");
          //     //2.然后将要转换的字符串设置为这个元素的innerHTML
          //     temp.innerHTML = data.content;
          //     //3.最后返回这个元素的innerText，即得到经过HTML解码的字符串
          //     let output = temp.innerText || temp.textContent;
          //     temp = null;
          //     setTimeout(() => {
          //         this.$Notice.confirm(data.title,output);
          //     }, 0)
          // } else {
          //     setTimeout(() => {
          //         this.$Notice.confirm(data.title,data.content);
          //     }, 0)
          // }
        }
      }
    },

    /**
     * 视图消息关闭
     *
     * @memberof AppMobAlert
     */
    alertClose(data: any) {
      if (data.customClass) {
        const tempArr: any[] = data.customClass.toString().split(',');
        if (tempArr && tempArr.length > 0) {
          if (Object.is('1', tempArr[1])) {
            const tag = this.viewName + '_' + this.infoGroup + '_' + tempArr[0];
            localStorage.setItem(tag, data.customClass);
          }
        }
      }
      if (data.removeMode && data.removeMode == 1) {
        const tag = this.viewName + '_' + this.infoGroup + '_' + data.codeName;
        localStorage.setItem(tag, data.codeName);
      }
      const alert: any = this.$refs['mob-alert-' + data.codeName];
      if (alert) {
        alert.style.display = 'none';
      }
    },
  },
  render() {
    return (
      <div class='app-mob-alert'>
        {this.items.map((item: any, index: number) => {
          if (item.showState && item.hasContent && !Object.is('POPUP', item.position)) {
            return (
              <div
                class={['alert-item','alert-item-' + item.type]}
                key={index}
                ref={`mob-alert-${item.codeName}`}
              >
                <div class='alert-item-content'>
                  {item.title && <div class='item-content-title'>{item.title}</div>}
                  {item.content ? (
                    item.messageType == 'HTML' ? (
                      <div class='item-content-html' v-html={item.content}></div>
                    ) : (
                      <div class='item-content-text'>{item.content}</div>
                    )
                  ) : null}
                </div>
                {item.closeable && (
                  <div class='alert-item-close' onClick={this.alertClose.bind(this, item)}>
                    <app-icon name='close' />
                  </div>
                )}
              </div>
            );
          }
        })}
      </div>
    );
  },
});
