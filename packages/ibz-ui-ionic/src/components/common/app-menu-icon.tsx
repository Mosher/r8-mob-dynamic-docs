import { IPSAppMenuItem } from '@ibiz/dynamic-model-api';
import { IParam } from 'ibz-core';
import { ComponentBase, GenerateComponent } from '../component-base';

export class AppMenuIconProps {
  /**
   * @description 菜单
   * @type {any[]}
   * @memberof AppMenuIconProps
   */
  menu: any[] = [];

  /**
   * @description 应用界面服务对象
   * @type {IParam}
   * @memberof AppMenuIconProps
   */
  appUIService: IParam = {};

  /**
   * 模型服务
   *
   * @type {IParam}
   * @memberof AppMenuIconProps
   */
  modelService: IParam = {};
}

export class AppMenuIcon extends ComponentBase<AppMenuIconProps> {
  /**
   * 菜单项集合
   *
   * @type IPSAppMenuItem[]
   * @memberof AppMenuIcon
   */
  public menuItems: IPSAppMenuItem[] = [];

  /**
   * vue 生命周期
   *
   * @memberof AppMenuIcon
   */
  setup() {
    this.menuItems = this.props.menu;
  }

  /**
   * 打开视图
   *
   * @public
   * @memberof AppMenuIcon
   */
  public openView(item: IPSAppMenuItem) {
    this.ctx.emit('menuClick', item);
  }

  /**
   * @description 根据菜单项获取菜单权限
   * @param {IPSAppMenuItem} menuItem
   * @return {*}
   * @memberof AppMenuIcon
   */
  public getMenusPermission(menuItem: IPSAppMenuItem) {
    if (!App.isPreviewMode() && menuItem.accessKey) {
      return this.props.appUIService.getResourceOPPrivs(menuItem.accessKey);
    } else {
      return true;
    }
  }

  /**
   * 绘制快速菜单图标项
   *
   * @public
   * @param {IPSAppMenuItem} item
   * @memberof AppMenuIcon
   */
  public renderQuickMenuIconItem(item: IPSAppMenuItem) {
    if (item.hidden || !this.getMenusPermission(item)) {
      return null;
    }
    const cssName: string = item.getPSSysCss()?.cssName || '';
    const icon = item.getPSSysImage();
    return (
      <div class={[cssName, 'menu-item']} onClick={() => this.openView(item)}>
        {icon ? <app-icon icon={icon}></app-icon> : <app-icon name='home'></app-icon>}
        <span class='menu-item-label'>
          {this.$tl(item.getCapPSLanguageRes()?.lanResTag, item.caption)}
        </span>
      </div>
    );
  }

  /**
   * 绘制内容
   *
   * @memberof AppMenuIcon
   */
  render() {
    if (this.menuItems.length > 0) {
      return (
        <div class='app-menu-icon'>
          {this.menuItems.map((item: IPSAppMenuItem) => {
            return this.renderQuickMenuIconItem(item);
          })}
        </div>
      );
    }
  }
}

export const AppMenuIconComponent = GenerateComponent(AppMenuIcon, Object.keys(new AppMenuIconProps()));
