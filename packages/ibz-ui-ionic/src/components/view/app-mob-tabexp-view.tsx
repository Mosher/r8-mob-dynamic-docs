import { shallowReactive } from 'vue';
import { AppMobTabExpViewProps, IMobTabExpViewController, MobTabExpViewController } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { DEViewComponentBase } from './de-view-component-base';

/**
 * 移动端分页导航视图
 *
 * @class AppMobTabExpView
 * @extends DEViewComponentBase
 */
export class AppMobTabExpView extends DEViewComponentBase<AppMobTabExpViewProps> {
  /**
   * 视图控制器
   *
   * @protected
   * @memberof AppMobTabExpView
   */
  protected c!: IMobTabExpViewController;

  /**
   * 设置响应式
   *
   * @public
   * @memberof AppMobTabExpView
   */
  setup() {
    this.c = shallowReactive<IMobTabExpViewController>(
      this.getViewControllerByType('DEMOBTABEXPVIEW') as MobTabExpViewController,
    );
    super.setup();
  }

  /**
   * @description 渲染信息栏
   * @return {*} 
   * @memberof AppMobTabExpView
   */
  public renderDataInfoBar() {
    if (this.c.viewInstance.showDataInfoBar) {
      return (
        <span class="view-info-bar">
          {this.c.getData()?.srfmajortext}
        </span>
      )
    }
  }

  /**
   * @description 渲染视图组件
   * @return {*} 
   * @memberof AppMobTabExpView
   */
  public renderViewControls() {
    const controlObject = super.renderViewControls();
    Object.assign(controlObject, {
      'dataInfoBar': () => this.renderDataInfoBar()
    })
    return controlObject;
  }
}
//  移动端多数据视图组件
export const AppMobTabExpViewComponent = GenerateComponent(AppMobTabExpView, new AppMobTabExpViewProps());
