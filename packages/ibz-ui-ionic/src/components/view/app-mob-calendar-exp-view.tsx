import { shallowReactive } from 'vue';
import { AppMobCalendarExpViewProps, IMobCalendarExpViewController, MobCalendarExpViewController } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { DEExpViewComponentBase } from './de-exp-view-component-base';

/**
 * @description 移动端日历导航视图
 * @export
 * @class AppMobCalendarExpView
 * @extends {DEViewComponentBase<AppMobCalendarExpViewProps>}
 */
export class AppMobCalendarExpView extends DEExpViewComponentBase<AppMobCalendarExpViewProps> {

  /**
    * 视图控制器
    *
    * @protected
    * @memberof AppMobCalendarExpView
    */
  protected c!: IMobCalendarExpViewController;

  /**
    * 设置响应式
    *
    * @public
    * @memberof AppMobCalendarExpView
    */
  setup() {
    this.c = shallowReactive<MobCalendarExpViewController>(
      this.getViewControllerByType('DEMOBCALENDAREXPVIEW') as MobCalendarExpViewController
    );
    super.setup();
  }
}
// 移动端地图导航视图组件
export const AppMobCalendarExpViewComponent = GenerateComponent(AppMobCalendarExpView, new AppMobCalendarExpViewProps());