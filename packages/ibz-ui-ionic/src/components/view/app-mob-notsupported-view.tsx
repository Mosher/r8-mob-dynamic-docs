import { shallowReactive } from 'vue';
import { AppViewProps, AppViewControllerBase } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { ViewComponentBase } from './view-component-base';

export class AppMobNotSupportedView extends ViewComponentBase<AppViewProps> {
  /**
   * 设置响应式
   *
   * @public
   * @memberof AppIndexView
   */
  setup() {
    this.c = shallowReactive(new AppViewControllerBase(this.props));
    super.setup();
  }

  /**
   * 视图渲染
   *
   * @memberof AppDefaultNotSupportedView
   */
  render() {
    if (!this.viewIsLoaded || !this.c.viewInstance) {
      return null;
    }
    const flexStyle: string =
      'width: 100%; height: 100%; overflow: auto; display: flex;justify-content:center;align-items:center;';
    return (
      <ion-page class='app-view' style={flexStyle}>
        {`${this.$tl('view.notsupportview.tip','暂未支持')}${this.c?.viewInstance?.title}`}
      </ion-page>
    );
  }
}
// 应用首页组件
export const AppMobNotSupportedViewComponent = GenerateComponent(
  AppMobNotSupportedView,
  new AppViewProps(),
);
