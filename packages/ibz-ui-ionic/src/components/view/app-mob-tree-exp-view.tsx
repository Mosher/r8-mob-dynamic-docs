import { shallowReactive } from 'vue';
import { AppMobTreeExpViewProps, IMobTreeExpViewController, MobTreeExpViewController } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { DEExpViewComponentBase } from './de-exp-view-component-base';

/**
 * @description 移动端树导航视图
 * @export
 * @class AppMobTreeExpView
 * @extends {DEViewComponentBase<AppMobTreeExpViewProps>}
 */
export class AppMobTreeExpView extends DEExpViewComponentBase<AppMobTreeExpViewProps> {

  /**
    * 视图控制器
    *
    * @protected
    * @memberof AppMobTreeExpView
    */
  protected c!: IMobTreeExpViewController;

  /**
    * 设置响应式
    *
    * @public
    * @memberof AppMobTreeExpView
    */
  setup() {
    this.c = shallowReactive<MobTreeExpViewController>(
      this.getViewControllerByType('DEMOBTREEEXPVIEW') as MobTreeExpViewController
    );
    super.setup();
  }
}
// 移动端树导航视图组件
export const AppMobTreeExpViewComponent = GenerateComponent(AppMobTreeExpView, new AppMobTreeExpViewProps());