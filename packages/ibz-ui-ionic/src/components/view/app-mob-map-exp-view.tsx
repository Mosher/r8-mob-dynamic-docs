import { shallowReactive } from 'vue';
import { AppMobMapExpViewProps, IMobMapExpViewController, MobMapExpViewController } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { DEExpViewComponentBase } from './de-exp-view-component-base';

/**
 * @description 移动端地图导航视图
 * @export
 * @class AppMobMapExpView
 * @extends {DEViewComponentBase<AppMobMapExpViewProps>}
 */
export class AppMobMapExpView extends DEExpViewComponentBase<AppMobMapExpViewProps> {

  /**
    * 视图控制器
    *
    * @protected
    * @memberof AppMobMapExpView
    */
  protected c!: IMobMapExpViewController;

  /**
    * 设置响应式
    *
    * @public
    * @memberof AppMobMapExpView
    */
  setup() {
    this.c = shallowReactive<MobMapExpViewController>(
      this.getViewControllerByType('DEMOBMAPEXPVIEW') as MobMapExpViewController
    );
    super.setup();
  }
}
// 移动端地图导航视图组件
export const AppMobMapExpViewComponent = GenerateComponent(AppMobMapExpView, new AppMobMapExpViewProps());