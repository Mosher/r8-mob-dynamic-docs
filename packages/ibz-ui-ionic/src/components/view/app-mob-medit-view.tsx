import { shallowReactive } from 'vue';
import { AppMobMEditViewProps, IMobMEditViewController, MobMEditViewController } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { DEMultiDataViewComponentBase } from './de-multi-data-view-component-base';

/**
 * 移动端多表单编辑视图
 *
 * @class AppMobMEditView
 * @extends ViewComponentBase
 */
export class AppMobMEditView extends DEMultiDataViewComponentBase<AppMobMEditViewProps> {
  /**
   * @description 多表单编辑视图控制器
   * @protected
   * @type {IMobMEditViewController}
   * @memberof AppMobMEditView
   */
  protected c!: IMobMEditViewController;

  /**
   * @description 设置响应式
   * @memberof AppMobMEditView
   */
  setup() {
    this.c = shallowReactive<IMobMEditViewController>(
      this.getViewControllerByType('DEMOBMEDITVIEW9') as MobMEditViewController,
    );
    super.setup();
  }
}
//  移动端多表单编辑视图组件
export const AppMobMEditViewComponent = GenerateComponent(AppMobMEditView, new AppMobMEditViewProps());
