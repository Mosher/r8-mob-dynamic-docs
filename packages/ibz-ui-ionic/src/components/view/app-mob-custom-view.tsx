import { shallowReactive } from 'vue';
import { AppMobCustomViewProps, IMobCustomViewController, MobCustomViewController } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { DEViewComponentBase } from './de-view-component-base';
/**
 * 移动端自定义视图
 *
 * @class AppMobCustomView
 * @extends ViewComponentBase
 */
export class AppMobCustomView extends DEViewComponentBase<AppMobCustomViewProps> {
  /**
   * 视图控制器
   *
   * @protected
   * @memberof AppIndexView
   */
  protected c!: IMobCustomViewController;

  /**
   * 设置响应式
   *
   * @public
   * @memberof AppMobCustomView
   */
  setup() {
    this.c = shallowReactive<MobCustomViewController>(
      this.getViewControllerByType('DEMOBCUSTOMVIEW') as MobCustomViewController,
    );
    super.setup();
  }
}
//  移动端自定义视图组件
export const AppMobCustomViewComponent = GenerateComponent(AppMobCustomView, new AppMobCustomViewProps());
