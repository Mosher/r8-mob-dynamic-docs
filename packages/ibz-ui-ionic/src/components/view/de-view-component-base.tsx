import { AppDEViewProps } from 'ibz-core';
import { ViewComponentBase } from './view-component-base';

export class DEViewComponentBase<Props extends AppDEViewProps> extends ViewComponentBase<AppDEViewProps> {}
