import { IPSControl } from '@ibiz/dynamic-model-api';
import { AppMobPanelViewProps, IMobPanelViewController, MobPanelViewController } from 'ibz-core';
import { shallowReactive } from 'vue';
import { GenerateComponent } from '../component-base';
import { DEViewComponentBase } from './de-view-component-base';

export class AppMobPanelView extends DEViewComponentBase<AppMobPanelViewProps> {
  /**
   * 视图控制器
   *
   * @protected
   * @memberof AppMobPanelView
   */
  protected c!: IMobPanelViewController;

  /**
   * 设置响应式
   *
   * @public
   * @memberof AppMobMDView
   */
  setup() {
    this.c = shallowReactive<MobPanelViewController>(
      this.getViewControllerByType('DEMOBPANELVIEW') as MobPanelViewController,
    );
    super.setup();
  }
  
  /**
   * 额外部件参数
   *
   * @param ctrlProps 部件参数
   * @param controlInstance 部件
   */
  public extraCtrlParam(ctrlProps: any, controlInstance: IPSControl) {
    super.extraCtrlParam(ctrlProps, controlInstance);
    if (controlInstance?.controlType == 'PANEL') {
      Object.assign(ctrlProps, {
        isLoadDefault: this.c.isLoadDefault,
      });
    }
  }
}

export const AppMobPanelViewComponent = GenerateComponent(AppMobPanelView, new AppMobPanelViewProps());
