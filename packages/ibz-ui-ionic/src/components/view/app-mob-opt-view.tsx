import { shallowReactive } from 'vue';
import { AppMobOptViewProps, IMobOptViewController, MobOptViewController } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { DEViewComponentBase } from './de-view-component-base';

/**
 * @description 移动端选项操作视图
 * @export
 * @class AppMobOptView
 * @extends {DEViewComponentBase<AppMobOptViewProps>}
 */
export class AppMobOptView extends DEViewComponentBase<AppMobOptViewProps> {

  /**
    * 视图控制器
    *
    * @protected
    * @memberof AppMobOptView
    */
  protected c!: IMobOptViewController;

  /**
    * 设置响应式
    *
    * @public
    * @memberof AppMobOptView
    */
  setup() {
    this.c = shallowReactive<MobOptViewController>(
      this.getViewControllerByType('DEMOBOPTVIEW') as MobOptViewController
    );
    super.setup();
  }

  /**
   * @description 绘制底部按钮
   * @return {*}
   * @memberof AppMobOptView
   */
   public renderFooterButtons() {
    return (
        <div class="option-view-btnbox">
            <ion-button expand='full' size='large' class="option-btn-cancel" onClick={() => this.c.cancel()}>
            {`${this.$tl('share.cancel','取消')}`}
            </ion-button>
            <ion-button expand='full' size='large' class="option-btn-success" onClick={() => this.c.ok()}>
            {`${this.$tl('share.ok','确认')}`}
            </ion-button>     
        </div>
    );
  }

  /**
   * @description 绘制视图所有部件
   * @return {*}
   * @memberof AppMobMPickUpView
   */
  public renderViewControls() {
    const controlObject = super.renderViewControls();
    Object.assign(controlObject, {
      footerButtons: () => this.renderFooterButtons(),
    });
    return controlObject;
  }

}
// 移动端选项操作视图组件
export const AppMobOptViewComponent = GenerateComponent(AppMobOptView, new AppMobOptViewProps());