import { shallowReactive } from 'vue';
import { AppMobListExpViewProps, IMobListExpViewController, MobListExpViewController } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { DEExpViewComponentBase } from './de-exp-view-component-base';

/**
 * @description 移动端列表导航视图
 * @export
 * @class AppMobListExpView
 * @extends {DEViewComponentBase<AppMobListExpViewProps>}
 */
export class AppMobListExpView extends DEExpViewComponentBase<AppMobListExpViewProps> {

  /**
    * 视图控制器
    *
    * @protected
    * @memberof AppMobListExpView
    */
  protected c!: IMobListExpViewController;

  /**
    * 设置响应式
    *
    * @public
    * @memberof AppMobListExpView
    */
  setup() {
    this.c = shallowReactive<MobListExpViewController>(
      this.getViewControllerByType('DEMOBLISTEXPVIEW') as MobListExpViewController
    );
    super.setup();
  }
}
// 移动端列表导航视图组件
export const AppMobListExpViewComponent = GenerateComponent(AppMobListExpView, new AppMobListExpViewProps());