import { shallowReactive } from 'vue';
import { AppIndexViewProps, IMobIndexViewController, MobIndexViewController } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { ViewComponentBase } from './view-component-base';

/**
 * 应用首页视图交联组件
 *
 * @export
 * @class AppIndexView
 */
export class AppIndexView extends ViewComponentBase<AppIndexViewProps> {
  /**
   * @description 应用首页控制器
   * @protected
   * @type {IMobIndexViewController}
   * @memberof AppIndexView
   */
  protected c!: IMobIndexViewController;

  /**
   * @description 设置响应式
   * @memberof AppIndexView
   */
  setup() {
    this.c = shallowReactive<MobIndexViewController>(
      this.getViewControllerByType('APPINDEXVIEW') as MobIndexViewController,
    );
    super.setup();
  }
}
// 应用首页组件
export const AppIndexViewComponent = GenerateComponent(AppIndexView, new AppIndexViewProps());
