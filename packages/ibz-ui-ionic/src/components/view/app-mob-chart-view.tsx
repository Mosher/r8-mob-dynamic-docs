import { shallowReactive } from 'vue';
import { AppMobChartViewProps, IMobChartViewController } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { DEMultiDataViewComponentBase } from './de-multi-data-view-component-base';

/**
 * 移动端图表视图
 *
 * @class AppMobChartView
 * @extends DEMultiDataViewComponentBase
 */
export class AppMobChartView extends DEMultiDataViewComponentBase<AppMobChartViewProps> {
  /**
   * @description 图表视图控制器
   * @protected
   * @type {IMobChartViewController}
   * @memberof AppMobChartView
   */
  protected c!: IMobChartViewController;

  /**
   * @description 设置响应式
   * @memberof AppMobChartView
   */
  setup() {
    this.c = shallowReactive<IMobChartViewController>(
      this.getViewControllerByType('DEMOBCHARTVIEW') as IMobChartViewController,
    );
    super.setup();
  }
}
//  移动端日历视图组件
export const AppMobChartViewComponent = GenerateComponent(AppMobChartView, new AppMobChartViewProps());
