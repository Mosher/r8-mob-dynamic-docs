import { AppMobDEDashboardViewProps } from 'ibz-core';
import { shallowReactive } from 'vue';
import { IMobDEDashboardViewController, MobDEDashboardViewController } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { ViewComponentBase } from './view-component-base';

/**
 * 移动端实体数据看板视图
 *
 * @class AppMobDEDashboardView
 * @extends ViewComponentBase
 */
export class AppMobDEDashboardView extends ViewComponentBase<AppMobDEDashboardViewProps> {
  /**
   * 移动端实体数据看板视图控制器实例对象
   *
   * @type {IMobDEDashboardViewController}
   * @memberof AppMobDEDashboardView
   */
  protected c!: IMobDEDashboardViewController;

  /**
   * 设置响应式
   *
   * @memberof AppMobDEDashboardView
   */
  setup() {
    this.c = shallowReactive<MobDEDashboardViewController>(
      this.getViewControllerByType('DEMOBPORTALVIEW') as MobDEDashboardViewController,
    );
    super.setup();
  }

  /**
   * @description 渲染信息栏
   * @return {*} 
   * @memberof AppMobDEDashboardView
   */
  public renderDataInfoBar() {
    if (this.c.viewInstance.showDataInfoBar) {
      return (
        <span class="view-info-bar">
          {this.c.getData()?.srfmajortext}
        </span>
      )
    }
  }

  /**
   * @description 渲染视图组件
   * @return {*} 
   * @memberof AppMobDEDashboardView
   */
  public renderViewControls() {
    const controlObject = super.renderViewControls();
    Object.assign(controlObject, {
      'dataInfoBar': () => this.renderDataInfoBar()
    })
    return controlObject;
  }
}

//  移动端实体数据看板视图组件
export const AppMobDEDashboardViewComponent = GenerateComponent(
  AppMobDEDashboardView,
  new AppMobDEDashboardViewProps(),
);
