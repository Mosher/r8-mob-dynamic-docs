import { shallowReactive } from 'vue';
import { AppMobTreeViewProps, IMobTreeViewController, MobTreeViewController } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { DEMultiDataViewComponentBase } from './de-multi-data-view-component-base';

/**
 * 移动端树视图
 *
 * @class AppMobTreeView
 * @extends DEMultiDataViewComponentBase
 */
export class AppMobTreeView extends DEMultiDataViewComponentBase<AppMobTreeViewProps> {
  /**
   * @description 树视图控制器
   * @protected
   * @type {IMobTreeViewController}
   * @memberof AppMobTreeView
   */
  protected c!: IMobTreeViewController;

  /**
   * @description 设置响应式
   * @memberof AppMobTreeView
   */
  setup() {
    this.c = shallowReactive<MobTreeViewController>(
      this.getViewControllerByType('DEMOBTREEVIEW') as MobTreeViewController,
    );
    super.setup();
  }
}

//  移动端树视图组件
export const AppMobTreeViewComponent = GenerateComponent(AppMobTreeView, new AppMobTreeViewProps());
