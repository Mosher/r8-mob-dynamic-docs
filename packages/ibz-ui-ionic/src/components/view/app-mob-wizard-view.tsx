import { shallowReactive } from 'vue';
import { AppMobWizardViewProps, IMobWizardViewController, MobWizardViewController } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { DEViewComponentBase } from './de-view-component-base';

/**
 * @description 移动端向导视图
 * @export
 * @class AppMobWizardView
 * @extends {DEViewComponentBase<AppMobWizardViewProps>}
 */
export class AppMobWizardView extends DEViewComponentBase<AppMobWizardViewProps> {

  /**
    * 视图控制器
    *
    * @protected
    * @memberof AppMobWizardView
    */
  protected c!: IMobWizardViewController;

  /**
    * 设置响应式
    *
    * @public
    * @memberof AppMobWizardView
    */
  setup() {
    this.c = shallowReactive<MobWizardViewController>(
      this.getViewControllerByType('DEMOBWIZARDVIEW') as MobWizardViewController
    );
    super.setup();
  }
}
// 移动端向导视图组件
export const AppMobWizardViewComponent = GenerateComponent(AppMobWizardView, new AppMobWizardViewProps());