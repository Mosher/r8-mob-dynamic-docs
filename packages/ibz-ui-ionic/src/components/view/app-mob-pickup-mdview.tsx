import { shallowReactive } from 'vue';
import { AppMobPickUpMDViewProps, IMobPickUpMDViewController, MobPickUpMDViewController } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { DEMultiDataViewComponentBase } from './de-multi-data-view-component-base';
import { IPSControl } from '@ibiz/dynamic-model-api';
/**
 * 移动端多数据视图
 *
 * @class AppMobMDView
 * @extends ViewComponentBase
 */
export class AppMobPickUpMDView extends DEMultiDataViewComponentBase<AppMobPickUpMDViewProps> {
  /**
   * @description 选择数据视图控制器
   * @protected
   * @type {IMobPickUpMDViewController}
   * @memberof AppMobPickUpMDView
   */
  protected c!: IMobPickUpMDViewController;

  /**
   * @description 设置响应式
   * @memberof AppMobPickUpMDView
   */
  setup() {
    this.c = shallowReactive<MobPickUpMDViewController>(
      this.getViewControllerByType('DEMOBPICKUPMDVIEW') as MobPickUpMDViewController,
    );
    super.setup();
  }

  /**
   * 额外部件参数
   *
   * @param ctrlProps 部件参数
   * @param controlInstance 部件
   */
  public extraCtrlParam(ctrlProps: any, controlInstance: IPSControl) {
    super.extraCtrlParam(ctrlProps, controlInstance);
    if (controlInstance?.controlType == 'MOBMDCTRL') {
      Object.assign(ctrlProps, {
        isMultiple: this.c.isMultiple,
        ctrlShowMode: 'SELECT',
      });
    }
  }
}
//  移动端选择多数据视图组件
export const AppMobPickUpMDViewComponent = GenerateComponent(AppMobPickUpMDView, new AppMobPickUpMDViewProps());
