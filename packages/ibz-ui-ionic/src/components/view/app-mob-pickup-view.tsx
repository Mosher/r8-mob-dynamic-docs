import { shallowReactive } from 'vue';
import { AppMobPickUpViewProps, IMobPickUpViewController, MobPickUpViewController } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { DEViewComponentBase } from './de-view-component-base';
import { IPSControl } from '@ibiz/dynamic-model-api';
/**
 * 移动端数据选择视图
 *
 * @class AppMobPickUpView
 * @extends DEViewComponentBase
 */
export class AppMobPickUpView extends DEViewComponentBase<AppMobPickUpViewProps> {
  /**
   * @description 数据选择视图控制器
   * @protected
   * @type {IMobPickUpViewController}
   * @memberof AppMobPickUpView
   */
  protected c!: IMobPickUpViewController;

  /**
   * @description 设置响应式
   * @memberof AppMobPickUpView
   */
  setup() {
    this.c = shallowReactive<MobPickUpViewController>(
      this.getViewControllerByType('DEMOBPICKUPVIEW') as MobPickUpViewController,
    );
    super.setup();
  }

  /**
   * @description 绘制顶部按钮
   * @return {*}
   * @memberof AppMobPickUpView
   */
  public renderHeaderButtons() {
    return (
      <ion-toolbar>
        <ion-buttons slot='start'>
          <ion-button size='small' onClick={() => this.c.cancel()}>
          {`${this.$tl('share.cancel','取消')}`}
          </ion-button>
        </ion-buttons>
        <ion-buttons slot='primary'>
          <ion-button size='small' onClick={() => this.c.ok()}>
          {`${this.$tl('share.ok','确认')}`}
          </ion-button>
        </ion-buttons>
        <ion-title>{this.c.viewInstance.caption}</ion-title>
      </ion-toolbar>
    );
  }

  /**
   * @description 绘制视图所有部件
   * @return {*}
   * @memberof AppMobPickUpView
   */
  public renderViewControls() {
    const controlObject = super.renderViewControls();
    Object.assign(controlObject, {
      headerButtons: () => this.renderHeaderButtons(),
    });
    return controlObject;
  }

  /**
   * @description 额外部件参数
   * @param {*} ctrlProps 部件参数
   * @param {IPSControl} controlInstance 部件实例
   * @memberof AppMobPickUpView
   */
  public extraCtrlParam(ctrlProps: any, controlInstance: IPSControl) {
    super.extraCtrlParam(ctrlProps, controlInstance);
    if (controlInstance?.controlType == 'PICKUPVIEWPANEL') {
      Object.assign(ctrlProps, {
        isMultiple: false,
      });
    }
  }
}
//  移动端数据选择视图组件
export const AppMobPickUpViewComponent = GenerateComponent(AppMobPickUpView, new AppMobPickUpViewProps());
