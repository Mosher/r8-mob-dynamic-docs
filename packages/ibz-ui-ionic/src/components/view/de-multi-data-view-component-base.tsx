import { IPSControl } from '@ibiz/dynamic-model-api';
import { AppDEMultiDataViewProps, IAppDEMultiDataViewController } from 'ibz-core';
import { DEViewComponentBase } from './de-view-component-base';

/**
 * @description 多数据视图父类
 * @export
 * @class DEMultiDataViewComponentBase
 * @extends {DEViewComponentBase<AppDEMultiDataViewProps>}
 * @template Props
 */
export class DEMultiDataViewComponentBase<
  Props extends AppDEMultiDataViewProps,
> extends DEViewComponentBase<AppDEMultiDataViewProps> {
  /**
   * @description 视图控制器
   * @protected
   * @type {IAppDEMultiDataViewController}
   * @memberof DEMultiDataViewComponentBase
   */
  protected c!: IAppDEMultiDataViewController;

  /**
   * @description 快速搜索值变化
   * @param {*} 快速分组抛出值
   * @memberof DEMultiDataViewComponentBase
   */
  public quickGroupValueChange(data: any) {
    if (this.c.quickGroupValueChange && this.c.quickGroupValueChange instanceof Function) {
      this.c.quickGroupValueChange(data);
    }
  }

  /**
   * @description 快速搜索
   * @param {*} query 快速搜索值
   * @memberof DEMultiDataViewComponentBase
   */
  public quickSearch(query: string) {
    if (this.c.quickSearch && this.c.quickSearch instanceof Function) {
      this.c.quickSearch(query);
    }
  }

  /**
   * @description 渲染快速分组
   * @return {*}
   * @memberof DEMultiDataViewComponentBase
   */
  public renderQuickGroup() {
    if (this.c.viewInstance.enableQuickGroup) {
      return (
        <ion-header class='app-quick-group'>
          <ion-toolbar>
            <app-quick-group
              items={this.c.quickGroupModel}
              onValueChange={(event: any) => {
                this.quickGroupValueChange(event);
              }}
            />
          </ion-toolbar>
        </ion-header>
      );
    }
  }

  /**
   * @description 绘制快速搜索
   * @return {*}
   * @memberof DEMultiDataViewComponentBase
   */
  public renderQuickSearch(): any {
    return <app-quick-search onQuickSearch={(event: any) => this.quickSearch(event)}></app-quick-search>;
  }

  /**
   * @description 渲染视图部件
   * @return {*}
   * @memberof DEMultiDataViewComponentBase
   */
  public renderViewControls() {
    const viewControls: any = super.renderViewControls();
    if(this.c.viewInstance.enableQuickGroup){
      Object.assign(viewControls, {
        quickGroupSearch: () => {
          return this.renderQuickGroup();
        },
      });
    }
    if (this.c.viewInstance.enableQuickSearch) {
      Object.assign(viewControls, {
        quickSearch: () => this.renderQuickSearch(),
      });
    }
    return viewControls;
  }

  /**
   * @description 额外部件参数
   * @param {*} ctrlProps 部件参数
   * @param {IPSControl} controlInstance 部件实例
   * @memberof DEMultiDataViewComponentBase
   */
  public extraCtrlParam(ctrlProps: any, controlInstance: IPSControl) {
    super.extraCtrlParam(ctrlProps, controlInstance);
    if (controlInstance?.controlType == 'SEARCHFORM') {
      Object.assign(ctrlProps, {
        expandSearchForm: this.c.viewInstance.expandSearchForm,
      });
    }
  }
}
