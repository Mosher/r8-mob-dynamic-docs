import { AppDEExpViewProps } from 'ibz-core';
import { DEViewComponentBase } from './de-view-component-base';

/**
 * @description 移动端实体导航视图部件基类
 * @export
 * @class DEExpViewComponentBase
 * @extends {DEViewComponentBase<AppDEExpViewProps>}
 * @template Props
 */
export class DEExpViewComponentBase<Props extends AppDEExpViewProps> extends DEViewComponentBase<AppDEExpViewProps> {
  
}