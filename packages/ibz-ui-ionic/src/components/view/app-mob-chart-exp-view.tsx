import { shallowReactive } from 'vue';
import { AppMobChartExpViewProps, IMobChartExpViewController, MobChartExpViewController } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { DEExpViewComponentBase } from './de-exp-view-component-base';

/**
 * @description 移动端图表导航视图
 * @export
 * @class AppMobChartExpView
 * @extends {DEViewComponentBase<AppMobChartExpViewProps>}
 */
export class AppMobChartExpView extends DEExpViewComponentBase<AppMobChartExpViewProps> {
  /**
   * 视图控制器
   *
   * @protected
   * @memberof AppMobChartExpView
   */
  protected c!: IMobChartExpViewController;

  /**
   * 设置响应式
   *
   * @public
   * @memberof AppMobChartExpView
   */
  setup() {
    this.c = shallowReactive<MobChartExpViewController>(
      this.getViewControllerByType('DEMOBCHARTEXPVIEW') as MobChartExpViewController,
    );
    super.setup();
  }
}
// 移动端图表导航视图组件
export const AppMobChartExpViewComponent = GenerateComponent(AppMobChartExpView, new AppMobChartExpViewProps());
