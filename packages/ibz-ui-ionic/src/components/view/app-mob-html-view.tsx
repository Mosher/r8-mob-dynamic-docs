import { shallowReactive } from 'vue';
import { AppMobHtmlViewProps, IMobHtmlViewController, MobHtmlViewController, StringUtil } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { DEViewComponentBase } from './de-view-component-base';

/**
 * @description 移动端HTML视图
 * @export
 * @class AppMobHtmlView
 * @extends {DEViewComponentBase<AppMobHtmlViewProps>}
 */
export class AppMobHtmlView extends DEViewComponentBase<AppMobHtmlViewProps> {
  /**
   * @description 视图控制器
   * @protected
   * @type {IMobHtmlViewController}
   * @memberof AppMobHtmlView
   */
  protected c!: IMobHtmlViewController;

  /**
   * @description 设置响应式
   * @memberof AppMobHtmlView
   */
  setup() {
    this.c = shallowReactive<IMobHtmlViewController>(
      this.getViewControllerByType('DEMOBHTMLVIEW') as MobHtmlViewController,
    );
    super.setup();
  }

  /**
   * @description 绘制html内容
   * @return {*}
   * @memberof AppMobHtmlView
   */
  public renderHtmlContent() {
    if (this.c?.viewInstance?.htmlUrl) {
      const iframeUrl = StringUtil.fillStrData(this.c.viewInstance.htmlUrl, this.c.context, this.c.viewParam);
      return <iframe class='view-html-container' src={iframeUrl}></iframe>;
    } else {
      return (
        <div class='view-error-container'>
          <img src='assets/images/404.jpg' alt='404' />
          <div class='error-text'>{`${this.$tl('view.mobhtmlview.notexist','抱歉，您访问的页面不存在！')}`}</div>
        </div>
      );
    }
  }

  /**
   * @description 绘制视图所有部件
   * @return {*}
   * @memberof AppMobHtmlView
   */
  public renderViewControls() {
    const controlObject = super.renderViewControls();
    Object.assign(controlObject, {
      htmlContent: () => this.renderHtmlContent(),
    });
    return controlObject;
  }
}
// 移动端html视图组件
export const AppMobHtmlViewComponent = GenerateComponent(AppMobHtmlView, new AppMobHtmlViewProps());
