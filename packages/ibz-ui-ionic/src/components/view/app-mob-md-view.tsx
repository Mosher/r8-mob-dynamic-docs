import { shallowReactive } from 'vue';
import { AppMobMDViewProps, IMobMDViewController, MobMDViewController } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { DEMultiDataViewComponentBase } from './de-multi-data-view-component-base';

/**
 * 移动端多数据视图
 *
 * @class AppMobMDView
 * @extends ViewComponentBase
 */
export class AppMobMDView extends DEMultiDataViewComponentBase<AppMobMDViewProps> {
  /**
   * @description 多数据视图控制器
   * @protected
   * @type {IMobMDViewController}
   * @memberof AppMobMDView
   */
  protected c!: IMobMDViewController;

  /**
   * @description 设置响应式
   * @memberof AppMobMDView
   */
  setup() {
    this.c = shallowReactive<MobMDViewController>(this.getViewControllerByType('DEMOBMDVIEW') as MobMDViewController);
    super.setup();
  }
}
//  移动端多数据视图组件
export const AppMobMDViewComponent = GenerateComponent(AppMobMDView, new AppMobMDViewProps());
