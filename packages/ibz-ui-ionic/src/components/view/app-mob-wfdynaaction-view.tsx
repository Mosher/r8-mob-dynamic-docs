import { shallowReactive } from 'vue';
import { AppMobWFDynaActionViewProps, IMobWFDynaActionViewController, MobWFDynaActionViewController } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { DEViewComponentBase } from './de-view-component-base';

/**
 * @description 移动端动态工作流操作视图
 * @export
 * @class AppMobWFDynaActionView
 * @extends {DEViewComponentBase<AppMobWFDynaActionViewProps>}
 */
export class AppMobWFDynaActionView extends DEViewComponentBase<AppMobWFDynaActionViewProps> {

  /**
    * 视图控制器
    *
    * @protected
    * @memberof AppMobWFDynaActionView
    */
  protected c!: IMobWFDynaActionViewController;

  /**
    * 设置响应式
    *
    * @public
    * @memberof AppMobWFDynaActionView
    */
  setup() {
    this.c = shallowReactive<MobWFDynaActionViewController>(
      this.getViewControllerByType('DEMOBWFDYNAACTIONVIEW') as MobWFDynaActionViewController
    );
    super.setup();
  }

  /**
   * @description 提交
   * @param {*} event
   * @memberof AppMobWFDynaActionView
   */
  public submit(event: any) {
    this.c.handleOk();
  }

  /**
   * @description 取消
   * @param {*} event
   * @memberof AppMobWFDynaActionView
   */
  public cancel(event: any) {
    this.c.handleCancel();
  }

  /**
   * @description 渲染视图底部
   * @return {*} 
   * @memberof AppMobWFDynaActionView
   */
  public renderViewFooter() {
    return (
      <ion-footer class={['view-footer', 'wf-action-view-footer']}>
        <ion-buttons>
          <ion-button expand='full' size='large' onClick={(event: any) => { this.submit(event); }}>
          {`${this.$tl('view.wfdynaactionview.submit','提交')}`}
          </ion-button>
          <ion-button expand='full' size='large' onClick={(event: any) => { this.cancel(event); }}>
          {`${this.$tl('share.cancel','取消')}`}
          </ion-button>
        </ion-buttons>
      </ion-footer>
    )
  }

  /**
   * @description 渲染表单
   * @return {*} 
   * @memberof AppMobWFDynaActionView
   */
  public renderForm() {
    if (this.c.editFormInstance) {
      return this.computeTargetCtrlData(this.c.editFormInstance);
    } else {
      return null;
    }
  }

  /**
   * @description 渲染视图组件
   * @return {*} 
   * @memberof AppMobWFDynaActionView
   */
  public renderViewControls() {
    const controlObject = super.renderViewControls();
    Object.assign(controlObject, {
      viewFooter: () => this.renderViewFooter(),
      form: () => this.renderForm()
    });
    return controlObject;
  }
}
// 移动端动态工作流操作视图组件
export const AppMobWFDynaActionViewComponent = GenerateComponent(AppMobWFDynaActionView, new AppMobWFDynaActionViewProps());