import { shallowReactive } from 'vue';
import { AppPortalViewProps, IMobPortalViewController, MobPortalViewController } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { ViewComponentBase } from './view-component-base';

/**
 * 移动端应用看板视图
 *
 * @class AppPortalView
 * @extends ViewComponentBase
 */
class AppPortalView extends ViewComponentBase<AppPortalViewProps> {
  /**
   * 移动端应用看板视图控制器实例对象
   *
   * @type {IMobPortalViewController}
   * @memberof AppPortalView
   */
  protected c!: IMobPortalViewController;

  /**
   * 设置响应式
   *
   * @memberof AppPortalView
   */
  setup() {
    this.c = shallowReactive<MobPortalViewController>(
      this.getViewControllerByType('APPPORTALVIEW') as MobPortalViewController,
    );
    super.setup();
  }
}

//  移动端应用看板视图组件
export const AppPortalViewComponent = GenerateComponent(AppPortalView, new AppPortalViewProps());
