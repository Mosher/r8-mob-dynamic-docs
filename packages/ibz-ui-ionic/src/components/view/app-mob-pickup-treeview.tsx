import { shallowReactive } from 'vue';
import { IPSControl } from '@ibiz/dynamic-model-api';
import { AppMobPickupTreeViewProps, MobPickupTreeViewController } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { DEMultiDataViewComponentBase } from './de-multi-data-view-component-base';

/**
 * 移动端选择树视图
 *
 * @class AppMobPickupTreeView
 */
class AppMobPickupTreeView extends DEMultiDataViewComponentBase<AppMobPickupTreeViewProps> {
  /**
   * @description 选择树视图控制器实例对象
   * @protected
   * @type {MobPickupTreeViewController}
   * @memberof AppMobPickupTreeView
   */
  protected c!: MobPickupTreeViewController;

  /**
   * @description 设置响应式
   * @memberof AppMobPickupTreeView
   */
  setup() {
    this.c = shallowReactive<MobPickupTreeViewController>(
      this.getViewControllerByType('DEMOBPICKUPTREEVIEW') as MobPickupTreeViewController,
    );
    super.setup();
  }

  /**
   * @description 额外部件参数
   * @param {*} ctrlProps 部件参数
   * @param {IPSControl} controlInstance 部件实例
   * @memberof AppMobPickupTreeView
   */
  public extraCtrlParam(ctrlProps: any, controlInstance: IPSControl) {
    super.extraCtrlParam(ctrlProps, controlInstance);
    if (controlInstance?.controlType == 'TREEVIEW') {
      Object.assign(ctrlProps, {
        isMultiple: this.c.isMultiple,
        ctrlShowMode: 'SELECT',
      });
    }
  }
}

//  移动端选择树视图组件
export const AppMobPickupTreeViewComponent = GenerateComponent(AppMobPickupTreeView, new AppMobPickupTreeViewProps());
