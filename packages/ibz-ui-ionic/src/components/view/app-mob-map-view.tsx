import { shallowReactive } from 'vue';
import { AppMobMapViewProps, IMobMapViewController, MobMapViewController } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { DEMultiDataViewComponentBase } from './de-multi-data-view-component-base';

export class AppMobMapView extends DEMultiDataViewComponentBase<AppMobMapViewProps> {
  /**
   * 视图控制器
   *
   * @protected
   * @memberof AppMobMapView
   */
  protected c!: IMobMapViewController;

  /**
   * 设置响应式
   *
   * @public
   * @memberof AppMobMapView
   */
  setup() {
    this.c = shallowReactive<MobMapViewController>(
      this.getViewControllerByType('DEMOBMAPVIEW') as MobMapViewController,
    );
    super.setup();
  }
}
// 应用首页组件
export const AppMobMapViewComponent = GenerateComponent(AppMobMapView, new AppMobMapViewProps());
