import { shallowReactive } from 'vue';
import { AppMobEditViewProps, IMobEditViewController, MobEditViewController } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { DEViewComponentBase } from './de-view-component-base';

export class AppMobEditView extends DEViewComponentBase<AppMobEditViewProps> {
  /**
   * 视图控制器
   *
   * @protected
   * @memberof AppMobEditView
   */
  protected c!: IMobEditViewController;

  /**
   * 设置响应式
   *
   * @public
   * @memberof AppMobEditView
   */
  setup() {
    this.c = shallowReactive<MobEditViewController>(
      this.getViewControllerByType('DEMOBEDITVIEW') as MobEditViewController,
    );
    super.setup();
  }

  /**
   * @description 渲染信息栏
   * @return {*} 
   * @memberof AppMobEditView
   */
  public renderDataInfoBar() {
    if (this.c.viewInstance.showDataInfoBar) {
      return (
        <span class="view-info-bar">
          {this.c.getData()?.srfmajortext}
        </span>
      )
    }
  }

  /**
   * @description 渲染视图组件
   * @return {*} 
   * @memberof AppMobEditView
   */
  public renderViewControls() {
    const controlObject = super.renderViewControls();
    Object.assign(controlObject, {
      'dataInfoBar': () => this.renderDataInfoBar()
    })
    return controlObject;
  }
}
// 应用首页组件
export const AppMobEditViewComponent = GenerateComponent(AppMobEditView, new AppMobEditViewProps());
