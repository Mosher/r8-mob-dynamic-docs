import { shallowReactive } from 'vue';
import { AppMobWFDynaEditViewProps, IMobWFDynaEditViewController, MobWFDynaEditViewController } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { DEViewComponentBase } from './de-view-component-base';

/**
 * @description 移动端动态工作流编辑视图
 * @export
 * @class AppMobWFDynaEditView
 * @extends {DEViewComponentBase<AppMobWFDynaEditViewProps>}
 */
export class AppMobWFDynaEditView extends DEViewComponentBase<AppMobWFDynaEditViewProps> {

  /**
    * 视图控制器
    *
    * @protected
    * @memberof AppMobWFDynaEditView
    */
  protected c!: IMobWFDynaEditViewController;

  /**
    * 设置响应式
    *
    * @public
    * @memberof AppMobWFDynaEditView
    */
  setup() {
    this.c = shallowReactive<MobWFDynaEditViewController>(
      this.getViewControllerByType('DEMOBWFDYNAEDITVIEW') as MobWFDynaEditViewController
    );
    super.setup();
  }

  /**
   * @description 工具栏按钮点击
   * @param {*} linkItem 工具栏项
   * @param {*} event 源事件对象
   * @memberof AppMobWFDynaEditView
   */
  public toolbarClick(linkItem: any, event: any) {
    this.c.dynamicToolbarClick(linkItem, event);
  }

  /**
   * @description 渲染工具栏
   * @return {*} 
   * @memberof AppMobWFDynaEditView
   */
  public renderToolbar() {
    return (
      <ion-toolbar class={['wf-toolbar']} slot="fixed">
        <ion-buttons slot="end">
          {this.c.linkModel.map((linkItem: any, index: number) => {
            return (
            <ion-button key={index} expand='block' onClick={(event: any) => { this.toolbarClick(linkItem, event); }}>
                <span class="caption">{linkItem.sequenceFlowName}</span>
              </ion-button>
            )
          })}
        </ion-buttons>
      </ion-toolbar>
    )
  }

  /**
   * @description 渲染表单
   * @return {*} 
   * @memberof AppMobWFDynaEditView
   */
  public renderForm() {
    if (this.c.editFormInstance) {
      return this.computeTargetCtrlData(this.c.editFormInstance);
    } else {
      return null;
    }
  }

  /**
   * @description 渲染视图部件
   * @return {*} 
   * @memberof AppMobWFDynaEditView
   */
  public renderViewControls() {
    const controlObject = super.renderViewControls();
    Object.assign(controlObject, {
      righttoolbar: () => this.renderToolbar(),
      form: () => this.renderForm()
    });
    return controlObject;
  }
}
// 移动端动态工作流编辑视图组件
export const AppMobWFDynaEditViewComponent = GenerateComponent(AppMobWFDynaEditView, new AppMobWFDynaEditViewProps());