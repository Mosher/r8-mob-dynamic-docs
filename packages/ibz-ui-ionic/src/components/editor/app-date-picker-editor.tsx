import { h, shallowReactive, resolveComponent } from 'vue';
import { AppDatePickerProps, IMobDatePickerEditorController, MobDatePickerEditorController } from 'ibz-core';
import { AppDatePicker } from '../common';
import { GenerateComponent } from '../component-base';
import { EditorComponentBase } from './editor-component-base';
/**
 * 时间选择编辑器
 *
 * @export
 * @class AppDatePickerEditor
 * @extends {ComponentBase}
 */
export class AppDatePickerEditor extends EditorComponentBase<AppDatePickerProps> {
  /**
   * @description 编辑器控制器
   * @protected
   * @type {IMobDatePickerEditorController}
   * @memberof AppDatePickerEditor
   */
  protected c!: IMobDatePickerEditorController;

  /**
   * @description 设置响应式
   * @memberof AppDatePickerEditor
   */
  setup() {
    this.c = shallowReactive<MobDatePickerEditorController>(
      this.getEditorControllerByType('MOBDATE') as MobDatePickerEditorController,
    );
    super.setup();
  }

  /**
   * @description 设置编辑器组件
   * @memberof AppDatePickerEditor
   */
  setEditorComponent() {
    this.editorComponent = AppDatePicker;
  }

  /**
   * @description 绘制内容
   * @return {*} 
   * @memberof AppDatePickerEditor
   */
  render() {
    if (!this.c.editorIsLoaded) {
      return null;
    }
    return h(this.editorComponent, {
      value: this.c.value,
      disabled: this.c.disabled,
      ...this.c.customProps,
      onEditorValueChange: ($event: any) => {
        this.c.changeValue($event);
      },
    });
  }
}

// 时间选择组件
export const AppDatePickerEditorComponent = GenerateComponent(AppDatePickerEditor, new AppDatePickerProps());
