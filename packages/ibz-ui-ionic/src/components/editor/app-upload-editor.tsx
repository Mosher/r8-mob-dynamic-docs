import { h, shallowReactive } from 'vue';
import { AppUploadProps, IMobUploadEditorController, MobUploadEditorController } from 'ibz-core';
import { AppUpload, AppSignature } from '../common';
import { GenerateComponent } from '../component-base';
import { EditorComponentBase } from './editor-component-base';

/**
 * 上传组件编辑器
 *
 * @export
 * @class AppUploadEditor
 * @extends {ComponentBase}
 */
export class AppUploadEditor extends EditorComponentBase<AppUploadProps> {
  /**
   * @description 编辑器控制器
   * @protected
   * @type {IMobUploadEditorController}
   * @memberof AppUploadEditor
   */
  protected c!: IMobUploadEditorController;

  /**
   * @description 设置响应式
   * @memberof AppUploadEditor
   */
  setup() {
    this.c = shallowReactive<MobUploadEditorController>(
      this.getEditorControllerByType('MOBPICTURE') as MobUploadEditorController,
    );
    super.setup();
  }

  /**
   * @description 设置编辑器组件
   * @memberof AppUploadEditor
   */
  setEditorComponent() {
    const { editorType: type, editorStyle: style } = this.editorInstance;
    const editorTypeStyle: string = `${type}${style && style != 'DEFAULT' ? '_' + style : ''}`;
    switch (editorTypeStyle) {
      case 'MOBPICTURE':
      case 'MOBSINGLEFILEUPLOAD':
      case 'MOBPICTURELIST':
      case 'MOBMULTIFILEUPLOAD':
        this.editorComponent = AppUpload;
        break;
      case 'MOBPICTURE_DZQM':
        this.editorComponent = AppSignature;
        break;
    }
  }

  /**
   * @description 绘制内容
   * @return {*}
   * @memberof AppUploadEditor
   */
  render() {
    if (!this.c.editorIsLoaded) {
      return null;
    }
    return h(this.editorComponent, {
      value: this.c.value,
      disabled: this.c.disabled,
      ...this.c.customProps,
      onEditorValueChange: ($event: any) => {
        this.c.changeValue($event);
      },
    });
  }
}

// FileUpload组件
export const AppUploadEditorComponent = GenerateComponent(AppUploadEditor, new AppUploadProps());
