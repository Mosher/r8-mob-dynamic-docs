import { AppEditorEvents, IAppEditorHooks, IEditorControllerBase, IEditorEventParam, MobRichTextEditorController, MobUploadEditorController } from 'ibz-core';
import {
  MobCheckBoxEditorController,
  MobDatePickerEditorController,
  MobDropdownListController,
  AppEditorProps,
  MobRatingEditorController,
  MobSliderEditorController,
  MobSpanEditorController,
  MobStepperEditorController,
  MobSwitchEditorController,
  MobTextboxEditorController,
  MobDataPickerEditorController,
} from 'ibz-core';
import { LogUtil } from 'ibz-core';
import { ComponentBase } from '../component-base/component-base';

/**
 * 编辑器组件基类
 *
 * @export
 * @class EditorComponentBase
 * @template Props 输入属性接口
 */
export class EditorComponentBase<Props extends AppEditorProps> extends ComponentBase<AppEditorProps> {
  /**
   * @description 编辑器控制器
   * @protected
   * @type {IEditorControllerBase}
   * @memberof EditorComponentBase
   */
  protected c!: IEditorControllerBase;

  /**
   * @description 编辑器组件
   * @protected
   * @type {*}
   * @memberof EditorComponentBase
   */
  protected editorComponent: any;

  /**
   * @description 编辑器输入属性值变更
   * @memberof EditorComponentBase
   */
  watchEffect() {
    this.c.initInputData(this.props);
  }

  /**
   * @description 编辑器钩子
   * @readonly
   * @type {IAppEditorHooks}
   * @memberof EditorComponentBase
   */
  get hooks(): IAppEditorHooks {
    return this.c.hooks;
  }

  /**
   * @description 编辑器实例
   * @readonly
   * @memberof EditorComponentBase
   */
  get editorInstance() {
    return this.c.editorInstance;
  }

  /**
   * @description 根据编辑器类型获取编辑器控制器
   * @param {string} type 编辑器类型
   * @return {*} 
   * @memberof EditorComponentBase
   */
  public getEditorControllerByType(type: string) {
    switch (type) {
      case 'MOBTEXT':
        return new MobTextboxEditorController(this.props);
      case 'MOBNUMBER':
        return new MobTextboxEditorController(this.props);
      case 'MOBTEXTAREA':
        return new MobTextboxEditorController(this.props);
      case 'MOBPASSWORD':
        return new MobTextboxEditorController(this.props);
      case 'MOBSWITCH':
        return new MobSwitchEditorController(this.props);
      case 'MOBSLIDER':
        return new MobSliderEditorController(this.props);
      case 'MOBRADIOLIST':
        return new MobCheckBoxEditorController(this.props);
      case 'MOBDROPDOWNLIST':
        return new MobDropdownListController(this.props);
      case 'MOBCHECKLIST':
        return new MobDropdownListController(this.props);
      case 'SPAN':
        return new MobSpanEditorController(this.props);
      case 'MOBDATE':
        return new MobDatePickerEditorController(this.props);
      case 'MOBRATING':
        return new MobRatingEditorController(this.props);
      case 'MOBSTEPPER':
        return new MobStepperEditorController(this.props);
      case 'PICKER':
        return new MobDataPickerEditorController(this.props);
      case 'MOBPICTURE':
        return new MobUploadEditorController(this.props);
      case 'MOBPICTURELIST':
        return new MobUploadEditorController(this.props);
      case 'MOBSINGLEFILEUPLOAD':
        return new MobUploadEditorController(this.props);
      case 'MOBMULTIFILEUPLOAD':
        return new MobUploadEditorController(this.props);
      case 'MOBHTMLTEXT':
        return new MobRichTextEditorController(this.props);
      default:
        LogUtil.log(`暂未实现${type}类型编辑器`);
        return null;
    }
  }

  /**
   * @description 构建组件
   * @memberof EditorComponentBase
   */
  setup(): void {
    super.setup();
    this.setEditorComponent();
    this.emitValueChange = this.emitValueChange.bind(this);
    this.hooks.valueChange.tap(this.emitValueChange);
  }

  /**
   * @description 初始化编辑器
   * @memberof EditorComponentBase
   */
  init() {
    super.init();
    this.c.editorInit().then((result: boolean) => {
      this.emitEditorEvent({ editorName: this.c.editorInstance.name, action: AppEditorEvents.INITED, data: result });
    });
  }

  /**
   * @description 部件销毁
   * @memberof EditorComponentBase
   */
  public unmounted(): void {
    this.c.editorDestroy();
    this.hooks.valueChange.removeTap(this.emitValueChange);
  }

  /**
   * @description 设置编辑器组件
   * @memberof EditorComponentBase
   */
  public setEditorComponent() {}

  /**
   * @description 抛出编辑器值change事件
   * @param {*} args 参数
   * @memberof EditorComponentBase
   */
  public emitValueChange(args: any) {
    const { arg } = args;
    this.emitEditorEvent({ editorName: this.c.editorInstance.name, action: AppEditorEvents.VALUE_CHANGE, data: arg });
  }

  /**
   * @description 执行编辑器事件
   * @param {IEditorEventParam} args
   * @memberof EditorComponentBase
   */
  public emitEditorEvent(args: IEditorEventParam) {
    this.ctx.emit('editorEvent', args);
  }
}
