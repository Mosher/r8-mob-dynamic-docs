import { h, shallowReactive, resolveComponent } from 'vue';
import { AppDropdownListProps, IMobDropdownListController, MobDropdownListController } from 'ibz-core';
import { AppDropdownList } from '../common';
import { GenerateComponent } from '../component-base';
import { EditorComponentBase } from './editor-component-base';
/**
 * 时间选择编辑器
 *
 * @export
 * @class AppDropdownListEditor
 * @extends {ComponentBase}
 */
export class AppDropdownListEditor extends EditorComponentBase<AppDropdownListProps> {
  /**
   * @description 编辑器控制器
   * @protected
   * @type {IMobDropdownListController}
   * @memberof AppDropdownListEditor
   */
  protected c!: IMobDropdownListController;

  /**
   * @description 设置响应式
   * @memberof AppDropdownListEditor
   */
  setup() {
    this.c = shallowReactive<MobDropdownListController>(
      this.getEditorControllerByType('MOBDROPDOWNLIST') as MobDropdownListController,
    );
    super.setup();
  }

  /**
   * @description 设置编辑器组件
   * @memberof AppDropdownListEditor
   */
  setEditorComponent() {
    this.editorComponent = AppDropdownList;
  }

  /**
   * @description 绘制内容
   * @return {*} 
   * @memberof AppDropdownListEditor
   */
  render() {
    if (!this.c.editorIsLoaded) {
      return null;
    }
    return h(this.editorComponent, {
      value: this.c.value,
      disabled: this.c.disabled,
      ...this.c.customProps,
      onEditorValueChange: ($event: any) => {
        this.c.changeValue($event);
      },
    });
  }
}

// 下拉列表组件
export const AppDropdownListEditorComponent = GenerateComponent(AppDropdownListEditor, new AppDropdownListProps());
