import { h, shallowReactive, resolveComponent } from 'vue';
import { AppTextboxProps, IMobTextboxEditorController, MobTextboxEditorController } from 'ibz-core';
import { AppInput, AppTextArea } from '../common';
import { GenerateComponent } from '../component-base';
import { EditorComponentBase } from './editor-component-base';

/**
 * 文本框编辑器
 *
 * @export
 * @class AppTextboxEditor
 * @extends {ComponentBase}
 */
export class AppTextboxEditor extends EditorComponentBase<AppTextboxProps> {
  /**
   * @description 编辑器控制器
   * @protected
   * @type {IMobTextboxEditorController}
   * @memberof AppTextboxEditor
   */
  protected c!: IMobTextboxEditorController;

  /**
   * @description 设置响应式
   * @memberof AppTextboxEditor
   */
  setup() {
    this.c = shallowReactive<MobTextboxEditorController>(
      this.getEditorControllerByType('MOBTEXT') as MobTextboxEditorController,
    );
    super.setup();
  }

  /**
   * @description 设置编辑器组件
   * @memberof AppTextboxEditor
   */
  setEditorComponent() {
    const type = this.editorInstance.editorType;
    switch (type) {
      case 'MOBTEXT':
      case 'MOBNUMBER':
      case 'MOBPASSWORD':
        this.editorComponent = AppInput;
        break;
      case 'MOBTEXTAREA':
        this.editorComponent = AppTextArea;
        break;
      default:
        this.editorComponent = AppInput;
        break;
    }
  }

  /**
   * @description 绘制内容
   * @return {*} 
   * @memberof AppTextboxEditor
   */
  render() {
    if (!this.c.editorIsLoaded) {
      return null;
    }
    return h(this.editorComponent, {
      value: this.c.value,
      disabled: this.c.disabled,
      ...this.c.customProps,
      onEditorValueChange: ($event: any) => {
        this.c.changeValue($event);
      },
    });
  }
}

// Textbox组件
export const AppTextboxEditorComponent = GenerateComponent(AppTextboxEditor, new AppTextboxProps());
