import { h, shallowReactive, resolveComponent } from 'vue';
import { AppRichTextProps, IMobRichTextEditorController, MobRichTextEditorController } from 'ibz-core';
import { AppRichText } from '../common';
import { GenerateComponent } from '../component-base';
import { EditorComponentBase } from './editor-component-base';

/**
 * @description 富文本编辑器
 * @export
 * @class AppRichTextEditor
 * @extends {EditorComponentBase<AppRichTextProps>}
 */
export class AppRichTextEditor extends EditorComponentBase<AppRichTextProps> {
  /**
   * @description 编辑器控制器
   * @protected
   * @type {IMobRichTextEditorController}
   * @memberof AppRichTextEditor
   */
  protected c!: IMobRichTextEditorController;

  /**
   * @description 设置响应式
   * @memberof AppRichTextEditor
   */
  setup() {
    this.c = shallowReactive<MobRichTextEditorController>(
      this.getEditorControllerByType('MOBHTMLTEXT') as MobRichTextEditorController,
    );
    super.setup();
  }

  /**
   * @description 设置编辑器组件
   * @memberof AppRichTextEditor
   */
  setEditorComponent() {
    this.editorComponent = AppRichText;
  }

  /**
   * @description 绘制内容
   * @return {*}
   * @memberof AppRichTextEditor
   */
  render() {
    if (!this.c.editorIsLoaded) {
      return null;
    }
    return h(this.editorComponent, {
      value: this.c.value,
      disabled: this.c.disabled,
      ...this.c.customProps,
      onEditorValueChange: ($event: any) => {
        this.c.changeValue($event);
      },
    });
  }
}

// AppRichText组件
export const AppRichTextEditorComponent = GenerateComponent(AppRichTextEditor, new AppRichTextProps());
