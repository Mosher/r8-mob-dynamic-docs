import { h, shallowReactive, resolveComponent } from 'vue';
import { AppSliderProps, IMobSliderEditorController, MobSliderEditorController } from 'ibz-core';
import { AppSlider } from '../common';
import { GenerateComponent } from '../component-base';
import { EditorComponentBase } from './editor-component-base';

/**
 * 开关编辑器
 *
 * @export
 * @class AppSliderEditor
 * @extends {ComponentBase}
 */
export class AppSliderEditor extends EditorComponentBase<AppSliderProps> {
  /**
   * @description 编辑器控制器
   * @protected
   * @type {IMobSliderEditorController}
   * @memberof AppSliderEditor
   */
  protected c!: IMobSliderEditorController;

  /**
   * @description 设置响应式
   * @memberof AppSliderEditor
   */
  setup() {
    this.c = shallowReactive<MobSliderEditorController>(
      this.getEditorControllerByType('MOBSLIDER') as MobSliderEditorController,
    );
    super.setup();
  }

  /**
   * @description 设置编辑器组件
   * @memberof AppSliderEditor
   */
  setEditorComponent() {
    this.editorComponent = AppSlider;
  }

  /**
   * @description 绘制内容
   * @return {*} 
   * @memberof AppSliderEditor
   */
  render() {
    if (!this.c.editorIsLoaded) {
      return null;
    }
    return h(this.editorComponent, {
      value: this.c.value,
      disabled: this.c.disabled,
      ...this.c.customProps,
      onEditorValueChange: ($event: any) => {
        this.c.changeValue($event);
      },
    });
  }
}

// Slider组件
export const AppSliderEditorComponent = GenerateComponent(AppSliderEditor, new AppSliderProps());
