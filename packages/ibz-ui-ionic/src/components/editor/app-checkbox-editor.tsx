import { h, shallowReactive, resolveComponent } from 'vue';
import { AppCheckBoxProps, IMobCheckBoxEditorController, MobCheckBoxEditorController } from 'ibz-core';
import { AppRadioList } from '../common';
import { GenerateComponent } from '../component-base';
import { EditorComponentBase } from './editor-component-base';

export class AppCheckBoxEditor extends EditorComponentBase<AppCheckBoxProps> {
  /**
   * @description 编辑器控制器
   * @protected
   * @type {IMobCheckBoxEditorController}
   * @memberof AppCheckBoxEditor
   */
  protected c!: IMobCheckBoxEditorController;

  /**
   * @description 设置响应式
   * @memberof AppCheckBoxEditor
   */
  setup() {
    this.c = shallowReactive<MobCheckBoxEditorController>(
      this.getEditorControllerByType('MOBRADIOLIST') as MobCheckBoxEditorController,
    );
    super.setup();
  }

  /**
   * @description 设置编辑器组件
   * @memberof AppCheckBoxEditor
   */
  setEditorComponent() {
    this.editorComponent = AppRadioList;
  }

  /**
   * @description 绘制内容
   * @return {*} 
   * @memberof AppCheckBoxEditor
   */
  render() {
    if (!this.c.editorIsLoaded) {
      return null;
    }
    return h(this.editorComponent, {
      value: this.c.value,
      disabled: this.c.disabled,
      ...this.c.customProps,
      onEditorValueChange: ($event: any) => {
        this.c.changeValue($event);
      },
    });
  }
}

// CheckBox组件
export const AppCheckBoxEditorComponent = GenerateComponent(AppCheckBoxEditor, new AppCheckBoxProps());
