import { h, shallowReactive, resolveComponent } from 'vue';
import { AppSwitchProps, IMobSwitchEditorController, MobSwitchEditorController } from 'ibz-core';
import { AppSwitch } from '../common';
import { GenerateComponent } from '../component-base';
import { EditorComponentBase } from './editor-component-base';

/**
 * 开关编辑器
 *
 * @export
 * @class AppSwitchEditor
 * @extends {ComponentBase}
 */
export class AppSwitchEditor extends EditorComponentBase<AppSwitchProps> {
  /**
   * @description 编辑器控制器
   * @protected
   * @type {IMobSwitchEditorController}
   * @memberof AppSwitchEditor
   */
  protected c!: IMobSwitchEditorController;

  /**
   * @description 设置响应式
   * @memberof AppSwitchEditor
   */
  setup() {
    this.c = shallowReactive<MobSwitchEditorController>(
      this.getEditorControllerByType('MOBSWITCH') as MobSwitchEditorController,
    );
    super.setup();
  }

  /**
   * @description 设置编辑器组件
   * @memberof AppTextboxEditor
   */
  setEditorComponent() {
    this.editorComponent = AppSwitch;
  }

  /**
   * @description 绘制内容
   * @return {*} 
   * @memberof AppSwitchEditor
   */
  render() {
    if (!this.c.editorIsLoaded) {
      return null;
    }
    return h(this.editorComponent, {
      value: this.c.value,
      disabled: this.c.disabled,
      ...this.c.customProps,
      onEditorValueChange: ($event: any) => {
        this.c.changeValue($event);
      },
    });
  }
}

// Switch组件
export const AppSwitchEditorComponent = GenerateComponent(AppSwitchEditor, new AppSwitchProps());
