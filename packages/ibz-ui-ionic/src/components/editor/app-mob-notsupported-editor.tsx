import { shallowReactive } from 'vue';
import { AppEditorProps, EditorControllerBase } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { EditorComponentBase } from './editor-component-base';

/**
 * 开关编辑器
 *
 * @export
 * @class AppMobNotSupportedEditor
 * @extends {EditorComponentBase}
 */
export class AppMobNotSupportedEditor extends EditorComponentBase<AppEditorProps> {
  /**
   * 设置响应式
   *
   * @public
   * @memberof AppMobNotSupportedEditor
   */
  setup() {
    this.c = shallowReactive(new EditorControllerBase(this.props));
    super.setup();
  }

  /**
   * 绘制内容
   *
   * @public
   * @memberof AppMobNotSupportedEditor
   */
  render() {
    if (!this.c.editorIsLoaded) {
      return null;
    }
    const flexStyle: string =
      'width: 100%; height: 100%; overflow: hidden; display: flex;justify-content:center;align-items:center;';
    return (
      <div class='control-container' style={flexStyle}>
        暂未支持编辑器
      </div>
    );
  }
}

// Slider组件
export const AppMobNotSupportedEditorComponent = GenerateComponent(AppMobNotSupportedEditor, new AppEditorProps());
