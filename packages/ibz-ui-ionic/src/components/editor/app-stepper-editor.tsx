import { h, shallowReactive, resolveComponent } from 'vue';
import { AppStepperProps, IMobStepperEditorController, MobStepperEditorController } from 'ibz-core';
import { AppStepper } from '../common';
import { GenerateComponent } from '../component-base';
import { EditorComponentBase } from './editor-component-base';

/**
 * 评分编辑器
 *
 * @export
 * @class AppStepperEditor
 * @extends {ComponentBase}
 */
export class AppStepperEditor extends EditorComponentBase<AppStepperProps> {
  /**
   * @description 编辑器控制器
   * @protected
   * @type {IMobStepperEditorController}
   * @memberof AppStepperEditor
   */
  protected c!: IMobStepperEditorController;

  /**
   * @description 设置响应式
   * @memberof AppStepperEditor
   */
  setup() {
    this.c = shallowReactive<MobStepperEditorController>(
      this.getEditorControllerByType('MOBSTEPPER') as MobStepperEditorController,
    );
    super.setup();
  }

  /**
   * @description 设置编辑器组件
   * @memberof AppStepperEditor
   */
  setEditorComponent() {
    this.editorComponent = AppStepper;
  }

  /**
   * @description 绘制内容
   * @return {*} 
   * @memberof AppStepperEditor
   */
  render() {
    if (!this.c.editorIsLoaded) {
      return null;
    }

    return h(this.editorComponent, {
      value: this.c.value,
      disabled: this.c.disabled,
      ...this.c.customProps,
      onEditorValueChange: ($event: any) => {
        this.c.changeValue($event);
      },
    });
  }
}

// Stepper组件
export const AppStepperEditorComponent = GenerateComponent(AppStepperEditor, new AppStepperProps());
