import { h, shallowReactive } from 'vue';
import {
  AppMobPickUpViewPanelProps,
  IMobPickUpViewPanelCtrlController,
  MobPickUpViewPanelCtrlController,
} from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { CtrlComponentBase } from './ctrl-component-base';
import { IPSAppDEView } from '@ibiz/dynamic-model-api';

export class AppMobPickUpViewPanel extends CtrlComponentBase<AppMobPickUpViewPanelProps> {
  /**
   * @description 选择视图面板部件控制器
   * @protected
   * @type {IMobPickUpViewPanelCtrlController}
   * @memberof AppMobPickUpViewPanel
   */
  protected c!: IMobPickUpViewPanelCtrlController;

  /**
   * @description 设置响应式
   * @memberof AppMobPickUpViewPanel
   */
  setup() {
    this.c = shallowReactive<MobPickUpViewPanelCtrlController>(
      this.getCtrlControllerByType('PICKUPVIEWPANEL') as MobPickUpViewPanelCtrlController,
    );
    super.setup();
  }

  /**
   * @description 绘制选择视图面板
   * @return {*}
   * @memberof AppMobPickUpViewPanel
   */
  renderPickUpViewPanel() {
    const embeddedView = this.c.controlInstance.getEmbeddedPSAppDEView() as IPSAppDEView;
    const viewComponent = App.getComponentService().getViewTypeComponent(
      embeddedView.viewType,
      embeddedView.viewStyle,
      embeddedView.getPSSysPFPlugin()?.pluginCode,
    );
    if (viewComponent) {
      return h(viewComponent, {
        viewPath: embeddedView.modelPath,
        navContext: this.c.context,
        navParam: this.c.viewParam,
        navDatas: this.c.navDatas,
        isMultiple: this.c.isMultiple,
        viewState: this.c.viewState,
        isShowCaptionBar: false,
        viewShowMode: 'EMBEDDED',
        modelData:embeddedView,
        previewData: this.c.previewData,
        onViewEvent: ({ viewname, action, data }: { viewname: string; action: string; data: any }) => {
          this.c.handlePickUPViewEvent(action, data);
        },
      });
    } else {
      return <div class='no-embedded-view'>{this.$tl('widget.mobpickupviewpanel.nopickerview','无选择视图')}</div>;
    }
  }

  /**
   * @description
   * @return {*}
   * @memberof AppMobPickUpViewPanel
   */
  render() {
    if (!this.controlIsLoaded.value) {
      return null;
    }
    const { width, height } = this.c.controlInstance;
    const controlstyle = {
      width: width ? `${width}px` : '100%',
      height: height ? `${height}px` : '100%',
    };
    return (
      <div class={{ ...this.classNames }} style={controlstyle}>
        {this.renderPickUpViewPanel()}
      </div>
    );
  }
}
// 移动端选择视图部件
export const AppMobPickUpViewPanelComponent = GenerateComponent(
  AppMobPickUpViewPanel,
  Object.keys(new AppMobPickUpViewPanelProps()),
);
