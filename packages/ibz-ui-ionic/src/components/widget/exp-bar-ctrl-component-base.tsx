import { reactive, h } from 'vue';
import { AppMobListExpBarProps, IAppExpBarCtrlController, Util } from 'ibz-core';
import { CtrlComponentBase } from './ctrl-component-base';

export class ExpBarCtrlComponentBase<Props extends AppMobListExpBarProps> extends CtrlComponentBase<AppMobListExpBarProps> {

  /**
   * 部件控制器
   *
   * @protected
   * @memberof ExpBarCtrlComponentBase
   */
  protected c!: IAppExpBarCtrlController;

  /**
   * @description 导航视图UUID
   * @protected
   * @type {string}
   * @memberof ExpBarCtrlComponentBase
   */
  protected navViewUUID: string = '';

  /**
   * @description 初始化响应式属性
   * @memberof ExpBarCtrlComponentBase
   */
  public initReactive() {
    super.initReactive();
    this.c.selection = reactive(this.c.selection);
  }

  /**
   * @description 渲染多数据部件
   * @return {*} 
   * @memberof ExpBarCtrlComponentBase
   */
  public renderXDataControl() {
    if (this.c.xDataControl) {
      const expMode = this.c.controlInstance.getPSControlParam()?.ctrlParams?.MODE || 'DEFAULT';
      const otherParams = { expMode: expMode, isSelectFirstDefault: true, };
      return this.computeTargetCtrlData(this.c.xDataControl, otherParams);
    }
  }

  /**
   * @description 渲染导航视图
   * @return {*} 
   * @memberof ExpBarCtrlComponentBase
   */
  public renderNavView() {
    if (this.c.selection) {
      const { viewComponent, navContext, navParam, navView, viewPath } = this.c.selection;
      const key = navContext?.[this.c.xDataControl.getPSAppDataEntity?.()?.codeName?.toLowerCase() || ''];
      if (!this.navViewUUID) {
        this.navViewUUID = Util.createUUID();
      }
      if (viewComponent) {
        return h(viewComponent, {
          key: this.navViewUUID + key,
          navContext: navContext,
          navParam: navParam,
          viewPath: viewPath,
          viewState: this.c.viewState,
          viewShowMode: 'EMBEDDED',        
        })
      }
    }
  }

  /**
   * @description 渲染导航栏部件
   * @return {*} 
   * @memberof ExpBarCtrlComponentBase
   */
  render() {
    if (!this.controlIsLoaded.value) {
      return null;
    }
    const { width, height } = this.c.controlInstance;
    const controlStyle = {
      width: width ? width + 'px' : '',
      height: height ? height + 'px' : ''
    }
    return (
      <div class={{ 'exp-bar': true, ...this.classNames }} style={controlStyle}>
        <div class='exp-bar-container'>
          <div class="container__multi_data_ctrl">
            {this.renderXDataControl()}
          </div>
          <div class="container__nav_view">
            {this.renderNavView()}
          </div>
        </div>
      </div>
    )
  }
}