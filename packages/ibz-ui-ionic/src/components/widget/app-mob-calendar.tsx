import {
  AppMobCalendarProps,
  ModelTool,
  IMobCalendarCtrlController,
  MobCalendarCtrlController,
  IParam,
  Util,
  MobCalendarEvents,
} from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { MDCtrlComponentBase } from './md-ctrl-component-base';
import { shallowReactive, reactive, ref, Ref } from 'vue';
import { IPSLayoutPanel, IPSSysCalendar, IPSSysCalendarItem } from '@ibiz/dynamic-model-api';
import moment from 'moment';

export class AppMobCalendar extends MDCtrlComponentBase<AppMobCalendarProps> {
  /**
   * @description  日历部件控制器
   * @protected
   * @type {IMobCalendarCtrlController}
   * @memberof AppMobCalendar
   */
  protected c!: IMobCalendarCtrlController;

  /**
   * @description 当前年份
   * @private
   * @type {Ref<number>}
   * @memberof AppMobCalendar
   */
  private year: Ref<number> = ref(0);

  /**
   * @description 当前月份(0~11)
   * @private
   * @type {Ref<number>}
   * @memberof AppMobCalendar
   */
  private month: Ref<number> = ref(0);

  /**
   * @description 当前日期
   * @private
   * @type {Ref<number>}
   * @memberof AppMobCalendar
   */
  private day: Ref<number> = ref(0);

  /**
   * @description 当前时间
   * @private
   * @type {Ref<Date>}
   * @memberof AppMobCalendar
   */
  private currentDate: Ref<Date> = ref(new Date());

  /**
   * @description 当前激活数据
   * @type {*}
   * @memberof AppMobCalendar
   */
  public activeData: any = reactive({});

  /**
   * @description 默认选中值
   * @private
   * @type {Ref<any[]>}
   * @memberof AppMobCalendar
   */
  private value: Ref<any[]> = ref([]);

  /**
   * @description 事程内容
   * @private
   * @type {IParam[]}
   * @memberof AppMobCalendar
   */
  private tileContent: IParam[] = [];

  /**
   * @description 时间轴加载条数
   * @private
   * @type {IParam[]}
   * @memberof AppMobCalendar
   */
  private count: number[] = [];

  /**
   * @description 选中数组
   * @private
   * @type {IParam[]}
   * @memberof AppMobCalendar
   */
  private selectedArray: IParam[] = [];

  /**
   * @description 开始拖动位置
   * @private
   * @type {number}
   * @memberof AppMobCalendar
   */
  private StarttouchLength: number = 0;

  /**
   * @description 事程数据
   * @private
   * @type {*}
   * @memberof AppMobCalendar
   */
  private eventsDate: any = {};

  /**
   * @description 日历组件引用
   * @private
   * @type {Ref<any>}
   * @memberof AppMobCalendar
   */
  private calendar: Ref<any> = ref('calendar');

  /**
   * @description 长按定时器
   * @type {Ref<any>}
   * @memberof AppMobCalendar
   */
  public touchTimer: Ref<any> = ref(0);

  /**
   * @description 是否打开上下文菜单
   * @type {*}
   * @memberof AppMobCalendar
   */
  public isShowContextMenu: boolean = true;

  /**
   * @description 设置响应式
   * @memberof AppMobCalendar
   */
  setup() {
    this.c = shallowReactive<MobCalendarCtrlController>(
      this.getCtrlControllerByType('CALENDAR') as MobCalendarCtrlController,
    );
    this.initCurrentTime();
    this.initTimeLineCount();
    super.setup();
  }

  /**
   * @description 初始化响应式属性
   * @memberof AppMobCalendar
   */
  public initReactive() {
    super.initReactive();
    this.c.activeItem = reactive<String>(this.c.activeItem);
    this.c.sign = reactive(this.c.sign);
  }

  /**
   * @description 初始化当前时间
   * @private
   * @param {*} [curTime=new Date()] 当前时间
   * @memberof AppMobCalendar
   */
  private initCurrentTime(curTime: any = new Date()) {
    this.currentDate.value = curTime;
    this.year.value = curTime.getFullYear();
    this.month.value = curTime.getMonth();
    this.day.value = curTime.getDate();
  }

  /**
   * @description 清除定时器
   * @memberof AppMobCalendar
   */
  public clearTimer() {
    clearInterval(this.touchTimer.value);
    this.touchTimer.value = 0;
  }

  /**
   * @description 长按开始
   * @param {*} item 节点数据
   * @param {*} event 事件源
   * @memberof AppMobCalendar
   */
  public onTouchStart(item: any, event: MouseEvent) {
    this.touchTimer.value = setInterval(() => {
      this.activeData.value = {
        mouseEvent: event,
        data: item,
      };
      this.isShowContextMenu = true;
      this.clearTimer();
    }, 500);
  }

  /**
   * @description 初始化时间轴加载条数
   * @private
   * @memberof AppMobCalendar
   */
  private initTimeLineCount() {
    const days = moment(this.currentDate.value).daysInMonth();
    this.count = [];
    for (let i = 1; i <= days; i++) {
      this.count.push(i);
    }
  }

  /**
   * @description 分页节点切换
   * @private
   * @param {*} $event 数据源
   * @return {*}
   * @memberof AppMobCalendar
   */
  private ionChange($event: any) {
    const { detail: _detail } = $event;
    if (!_detail) {
      return;
    }
    const { value: _value } = _detail;
    if (_value) {
      this.c.activeItemChange(_value);
    }
  }

  /**
   * @description 查询天数
   * @private
   * @param {number} year 年
   * @param {number} month 月
   * @param {number} weekIndex 周索引
   * @memberof AppMobCalendar
   */
  private selectday(year: number, month: number, weekIndex: number) {
    this.value.value = [year, month, this.day];
  }

  /**
   * @description 上个
   * @private
   * @param {number} year 年
   * @param {number} month 月
   * @param {number} weekIndex 周索引
   * @memberof AppMobCalendar
   */
  private prev(year: number, month: number, weekIndex: number) {
    if (this.c.calendarStyle == 'MONTH_TIMELINE' || this.c.calendarStyle == 'MONTH') {
      this.selectday(year, month, this.day.value);
      this.initCurrentTime(new Date(year + '/' + month + '/' + '1'));
      this.c.formatDate(this.currentDate.value);
    }
    if (this.c.calendarStyle == 'WEEK_TIMELINE' || this.c.calendarStyle == 'WEEK') {
      this.countWeeks(year, month, weekIndex);
    }
  }

  /**
   * @description 下个
   * @private
   * @param {number} year 年
   * @param {number} month 月
   * @param {number} weekIndex 周索引
   * @memberof AppMobCalendar
   */
  private next(year: number, month: number, weekIndex: number) {
    if (this.c.calendarStyle == 'MONTH_TIMELINE' || this.c.calendarStyle == 'MONTH') {
      this.selectday(year, month, this.day.value);
      this.initCurrentTime(new Date(year + '/' + month + '/' + '1'));
      this.c.formatDate(this.currentDate.value);
    }
    if (this.c.calendarStyle == 'WEEK_TIMELINE' || this.c.calendarStyle == 'WEEK') {
      this.countWeeks(year, month, weekIndex);
    }
  }

  /**
   * @description 根据周下标计算事件
   * @private
   * @param {number} year 年
   * @param {number} month 月
   * @param {number} week 周
   * @memberof AppMobCalendar
   */
  private countWeeks(year: number, month: number, week: number) {
    const date = new Date(year + '/' + month + '/' + 1);
    const weekline = date.getDay();
    if (weekline == 0) {
      this.initCurrentTime(new Date(year + '/' + month + '/' + (week * 7 + 1)));
    } else {
      this.initCurrentTime(new Date(year + '/' + month + '/' + (week * 7 - weekline + 1)));
    }
    this.c.formatDate(this.currentDate.value);
  }

  /**
   * @description 选择年份事件的回调方法
   * @private
   * @param {number} year 年
   * @memberof AppMobCalendar
   */
  private selectYear(year: number) {
    this.value.value = [year, this.month, this.day];
    this.initCurrentTime(new Date(year + '/' + this.month + '/' + this.day));
    this.c.formatDate(this.currentDate.value);
  }

  /**
   * @description 选择月份事件的回调方法
   * @private
   * @param {number} month 月
   * @param {number} year 年
   * @memberof AppMobCalendar
   */
  private selectMonth(month: number, year: number) {
    this.selectday(year, month, this.day.value);
    this.initCurrentTime(new Date(year + '/' + month + '/' + this.day));
    this.c.formatDate(this.currentDate.value);
  }

  /**
   * @description 点击前一天
   * @private
   * @memberof AppMobCalendar
   */
  private prevDate() {
    const preDate = new Date(this.currentDate.value.getTime() - 24 * 60 * 60 * 1000); //前一天
    this.initCurrentTime(preDate);
    this.c.formatDate(this.currentDate.value);
  }

  /**
   * @description 点击后一天
   * @private
   * @memberof AppMobCalendar
   */
  private nextDate() {
    const nextDate = new Date(this.currentDate.value.getTime() + 24 * 60 * 60 * 1000); //后一天
    this.initCurrentTime(nextDate);
    this.c.formatDate(this.currentDate.value);
  }

  /**
   * @description 日历部件数据选择日期回调
   * @private
   * @param {*} data 日期数据
   * @memberof AppMobCalendar
   */
  private async clickDay(data: any,sign: any) {
    if (data) {
      const reTime = data.join('/');
      const temptime = new Date(reTime);
      this.year.value = temptime.getFullYear();
      this.month.value = temptime.getMonth();
      this.day.value = temptime.getDate();
      const start = (moment as any)(temptime).startOf('day').format('YYYY-MM-DD HH:mm:ss');
      const end = (moment as any)(temptime).endOf('day').format('YYYY-MM-DD HH:mm:ss');
      const args = { start: start, end: end };
      if (this.c.expMode == 'LEFT') {
        // 如果是嵌入左右导航栏，直接往外抛数据
        const calendarItem = (this.c.controlInstance as IPSSysCalendar).getPSSysCalendarItems()?.find((_calendarItem: IPSSysCalendarItem) => 
          Object.is(this.c.activeItem.toString(), _calendarItem.itemType.toLowerCase())
        );
        let arg = calendarItem;
        Object.assign(arg, {curData: args});
        this.c.ctrlEvent(MobCalendarEvents.SELECT_CHANGE,[arg]);
      } else {
        await this.c.load({}, args, false);
      }
    }
  }

  /**
   * @description 选中或取消事件
   * @private
   * @param {*} item 事件项
   * @memberof AppMobCalendar
   */
  private checkboxSelect(item: any) {
    const count = this.selectedArray.findIndex(i => {
      return i.mobile_entity1id == item.mobile_entity1id;
    });
    if (count == -1) {
      this.selectedArray.push(item);
    } else {
      this.selectedArray.splice(count, 1);
    }
  }

  /**
   * @description 开始滑动
   * @private
   * @param {*} e 事件源
   * @memberof AppMobCalendar
   */
  private gotouchstart(e: any) {
    const touch: any = e.touches[0];
    const startY = touch.pageY;
    this.StarttouchLength = startY;
  }

  /**
   * @description 触摸移动
   * @private
   * @param {*} e 事件源
   * @memberof AppMobCalendar
   */
  private gotouchmove(e: any) {
    const touch: any = e.touches[0];
    const startY = touch.pageY;
    const calendar: any = this.calendar;
    if (calendar) {
      if (startY - this.StarttouchLength < 0) {
        calendar.changeStyle2(false);
      } else {
        calendar.changeStyle2(true);
      }
    }
  }

  /**
   * @description 日程点击
   * @private
   * @param {IParam} $event 事件信息
   * @memberof AppMobCalendar
   */
  private onEventClick($event: IParam) {
    if (this.c.isSelectFirstDefault) {
      // 如果是嵌入导航栏，直接往外抛数据
      this.c.selections = [$event];
      this.c.ctrlEvent(MobCalendarEvents.SELECT_CHANGE,[$event]);
      return;
    } else {
      this.c.openView($event);
    }
  }

  /**
   * @description 删除事程
   * @private
   * @param {any[]} data 事程数据集
   * @memberof AppMobCalendar
   */
  private remove(data: any[]) {
    this.c.remove(data).then(() => {
      this.c.formatDate(this.currentDate.value);
    });
  }

  /**
   * @description 绘制日历样式----月
   * @return {*}
   * @memberof AppMobCalendar
   */
  public renderMonthCalendar() {
    return (
      <app-calendar
        ref={this.calendar}
        value={this.value.value}
        groupData={this.c.groupData}
        groupField={this.c.groupField}
        groupDetail={this.c.groupDetail}
        sign={this.c.sign}
        expMode={this.c.expMode}
        responsive={true}
        lunar={true}
        isChangeStyle={true}
        events={this.eventsDate}
        tileContent={this.tileContent}
        illustration={this.c.illustration}
        isSelectToday={this.c.isSelectFirstDefault}
        onPrev={this.prev.bind(this)}
        onNext={this.next.bind(this)}
        onSelect={this.clickDay.bind(this)}
        onSelectYear={this.selectYear.bind(this)}
        onSelectMonth={this.selectMonth.bind(this)}
      />
    );
  }

  /**
   * @description 绘制日历样式----天
   * @return {*}
   * @memberof AppMobCalendar
   */
  public renderDayCalendar() {
    // TODO 左右图标名错误
    return (
      <div class='calendar-tools'>
        <div class='calendar-prev' onClick={() => this.prevDate()}>
          <app-icon name='chevron-back-outline'></app-icon>
        </div>
        <div class='calendar-next' onClick={() => this.nextDate()}>
          <app-icon name='chevron-forward-outline'></app-icon>
        </div>
        <div class='calendar-info'>{`${this.year.value} ${this.$tl('share.year', '年')} ${
          this.month.value + 1
        } ${this.$tl('share.month', '月')} ${this.day.value} ${this.$tl(
          'share.day',
          '日',
        )}`}</div>
      </div>
    );
  }

  /**
   * @description 绘制日历样式----周
   * @return {*}
   * @memberof AppMobCalendar
   */
  public renderWeekCalendar() {
    return (
      <app-calendar
        ref={this.calendar}
        weekSwitch={true}
        sign={this.c.sign}
        expMode={this.c.expMode}        
        value={this.value.value}
        tileContent={this.tileContent}
        illustration={this.c.illustration}
        responsive={true}
        isSelectToday={this.c.isSelectFirstDefault}
        onPrev={this.prev.bind(this)}
        onNext={this.next.bind(this)}
        onSelect={this.clickDay.bind(this)}
        onSelectYear={this.selectYear.bind(this)}
        onSelectMonth={this.selectMonth.bind(this)}
      />
    );
  }

  /**
   * @description 绘制分页头
   * @param {IPSSysCalendarItem[]} calendarItems 日历项
   * @return {*}
   * @memberof AppMobCalendar
   */
  public renderSegment(calendarItems: IPSSysCalendarItem[]) {
    return (
      <ion-segment scrollable={true} value={this.c.activeItem} onIonChange={($event: any) => this.ionChange($event)}>
        {calendarItems.map((calendarItem: IPSSysCalendarItem) => {
          return (
            <ion-segment-button value={calendarItem?.itemType?.toLowerCase()}>
              <ion-label>
                {this.$tl(calendarItem.getNamePSLanguageRes()?.lanResTag, calendarItem.name)}
              </ion-label>
            </ion-segment-button>
          );
        })}
      </ion-segment>
    );
  }

  /**
   * @description 绘制日历样式----时间轴
   * @param {*} calendarStyle 日历样式
   * @return {*}
   * @memberof AppMobCalendar
   */
  public renderTimelineCalendar(calendarStyle: any) {
    switch (calendarStyle) {
      case 'TIMELINE':
        return this.renderTimeline();
      case 'MONTH_TIMELINE':
      case 'WEEK_TIMELINE':
        return this.renderWMTimeline();
      default:
        return this.renderEventList();
    }
  }

  /**
   * @description 绘制时间轴
   * @return {*}
   * @memberof AppMobCalendar
   */
  public renderTimeline() {
    return (
      <div class='calendar-timeline'>
        {this.count.map((i: any) => {
          return (
            <div class='timeline-item'>
              <p>{`${this.year.value}-${this.month.value + 1}-${i}`}</p>
              {this.c.sign?.map((it: any) => {
                if (
                  it.time == this.year.value + '-' + (this.month.value + 1) + '-' + i ||
                  it.time == this.year.value + '-' + '0' + (this.month.value + 1) + '-' + i
                ) {
                  const show = it.evens?.find((item: any) => Object.is(this.c.activeItem, item.itemType));
                  if (show) {
                    return (
                      <div class='even-container'>
                        {it.evens?.map((item: any) => {
                          if (this.c.activeItem == item.itemType) {
                            return (
                              <div class='even-item'>
                                {this.renderEventContent(item)}
                                <app-icon onClick={() => this.remove([item])} name='close'></app-icon>
                              </div>
                            );
                          }
                        })}
                      </div>
                    );
                  }
                }
              })}
            </div>
          );
        })}
      </div>
    );
  }

  /**
   * @description 绘制月-周时间轴
   * @return {*}
   * @memberof AppMobCalendar
   */
  public renderWMTimeline() {
    return (
      <div class='calendar-composite-timeline'>
        {this.c.sign?.map((i: any) => {
          return (
            <div class='timeline-item'>
              <p>{i.time}</p>
              {i.evens?.map((item: any) => {
                if (this.c.activeItem == item.itemType) {
                  return (
                    <div class='multiple-select'>
                      {this.c.isChoose && (
                        <ion-checkbox class='checkbox' onClick={() => this.checkboxSelect(item)}></ion-checkbox>
                      )}
                      <div class='even-container'>
                        {this.renderEventContent(item)}
                        {!this.c.isChoose && <app-icon onClick={() => this.remove([item])} name='close'></app-icon>}
                      </div>
                    </div>
                  );
                }
              })}
            </div>
          );
        })}
      </div>
    );
  }

  /**
   * @description 绘制事件内容
   * @param {*} item 日程事件
   * @return {*}
   * @memberof AppMobCalendar
   */
  public renderEventContent(item: any) {
    const calendarItems = (this.c.controlInstance as IPSSysCalendar).getPSSysCalendarItems() || [];
    const appDataEntity = this.c.controlInstance.getPSAppDataEntity();
    const renderTempJSX = (calendarItem: IPSSysCalendarItem) => {
      if (calendarItem.getTextPSAppDEField()) {
        return <div class='evenname'>{item['title']}</div>;
      } else if (calendarItem.getContentPSAppDEField()) {
        return (
          <div class='evenname'>{item[calendarItem?.getContentPSAppDEField()?.name?.toLowerCase() as string]}</div>
        );
      } else if (calendarItem.getIconPSAppDEField()) {
        return <div class='evenname'>{item[calendarItem?.getIconPSAppDEField()?.name?.toLowerCase() as string]}</div>;
      } else {
        const majorField = ModelTool.getAppEntityMajorField(appDataEntity);
        return <div class='evenname'>{majorField ? majorField?.codeName?.toLowerCase() : ''}</div>;
      }
    };
    return calendarItems.map((calendarItem: IPSSysCalendarItem) => {
      if (this.c.activeItem == calendarItem.itemType.toLowerCase()) {
        return <div onClick={() => this.onEventClick(item)}>{renderTempJSX(calendarItem)}</div>;
      }
    });
  }

  /**
   * @description 绘制事程列表
   * @return {*}
   * @memberof AppMobCalendar
   */
  public renderEventList() {
    if (this.c.isEnableGroup) {
      const pluginCode = this.c.controlInstance.getGroupPSSysPFPlugin()?.pluginCode;
      if (pluginCode) {
        const ctrlItemPluginInstance = App.getPluginService().getCtrlItemPluginByTag(
          pluginCode,
        );
        if (ctrlItemPluginInstance) {
          return ctrlItemPluginInstance.renderItem(this.c.groupData, this.c, this);
        }
      } else {
        return this.renderGroupEvent();
      }
    } else {
      return this.renderNoGroupEvent();
    }
  }

  /**
   * @description 绘制分组事程
   * @memberof AppMobCalendar
   */
  public renderGroupEvent() {
    const groupClassName = this.c.controlInstance.getGroupPSSysCss()?.cssName;
    const groupData = this.c.groupData;
    if (groupData?.length > 0) {
      return (
        <ion-list class={groupClassName}>
          {
            groupData.map((data: any) => {
              return (
                <div class='calendar-group'>
                  <ion-item-group>
                    <ion-item-divider>
                      <ion-label>{data.text}</ion-label>
                    </ion-item-divider>
                    {
                      data.items?.map((item: any, index: number) => {
                        const calendarItem = (this.c.controlInstance as IPSSysCalendar).getPSSysCalendarItems()?.find((_calendarItem: IPSSysCalendarItem) => 
                          Object.is(item.itemType?.toLowerCase(), _calendarItem.itemType.toLowerCase())
                        );
                        const itemStyle = {
                          '--color': item.textColor,
                          '--background': item.color,
                        }
                        return (
                          calendarItem?.getPSLayoutPanel() ? 
                          this.renderCalendarItemLayoutPanel(calendarItem, item, itemStyle) : 
                          this.renderCalendarItemDefault(calendarItem, item, itemStyle)
                        )
                      })
                    }
                  </ion-item-group>
                </div>
              )
            })
          }
        </ion-list>
      )
    } else {
      return (
        [
          this.renderNoData(),
          this.renderQuickToolbar(),
        ]
      )
    }
  }

  /**
   * @description 绘制无分组事程
   * @memberof AppMobCalendar
   */
  public renderNoGroupEvent() {
    const arr = this.c.calendarItems[this.c.activeItem.toString()];
    const calendarItem = (this.c.controlInstance as IPSSysCalendar).getPSSysCalendarItems()?.find((_calendarItem: IPSSysCalendarItem) => 
      Object.is(this.c.activeItem.toString(), _calendarItem.itemType.toLowerCase())
    );
    if (arr?.length > 0) {
      return (
        <ion-list class={calendarItem?.getPSSysCss()?.cssName}>
          {arr.map((item: any) => {
              const itemStyle = {
                '--color': item.textColor,
                '--background': item.color,
              }
              return (
                calendarItem?.getPSLayoutPanel() ? 
                this.renderCalendarItemLayoutPanel(calendarItem, item, itemStyle) : 
                this.renderCalendarItemDefault(calendarItem, item, itemStyle)
              )
            })}
        </ion-list>
      );
    } else {
      return (
        [
          this.renderNoData(),
          this.renderQuickToolbar(),
        ]
      )
    }
  }

  /**
   * @description 绘制日历项默认样式
   * @param {IPSSysCalendarItem} calendarItem 日历项
   * @param {IParam} item 日历项数据
   * @param {string} itemStyle 日历项样式
   * @memberof AppMobCalendar
   */
  public renderCalendarItemDefault(calendarItem: any, item: IParam, itemStyle: IParam) {
    return (
      <ion-item class='calendar-text-item' 
        style={itemStyle} 
        onTouchstart={(event: any) => {
          this.onTouchStart(item, event);
        }}
        onTouchend={(event: any) => {
          this.clearTimer();
        }}
        obTouchmove={(event: any) => {
          this.clearTimer();
        }}
        onClick={() => this.onEventClick(item)}>
        <ion-label class='calendar-text'>
          <h2>
            <app-icon iconSrc={item.iconSrc}/>
            {item.title}
          </h2>
          <p>{item.content}</p>
        </ion-label>
      </ion-item>
    )
  }

  /**
   * @description 绘制日历项布局面板
   * @param {IPSSysCalendarItem} calendarItem 日历项
   * @param {IParam} item 日历项数据
   * @param {string} itemStyle 日历项样式
   * @return {*} 
   * @memberof AppMobCalendar
   */
  public renderCalendarItemLayoutPanel(calendarItem: IPSSysCalendarItem, item: IParam, itemStyle: IParam) {
    const _context = Util.deepCopy(this.c.context);
    const entityCodeName = calendarItem.getPSAppDataEntity()?.codeName?.toLowerCase() as string;
    Object.assign(_context, { [entityCodeName]: item[entityCodeName] });
    const otherParams = {
      navDatas: [item],
      navContext: _context,
    };
    return (
      <ion-item class="calendar-item-layout" style={itemStyle}>
        {
          this.computeTargetCtrlData(calendarItem.getPSLayoutPanel() as IPSLayoutPanel, otherParams)
        }
      </ion-item>
    )
  }

  /**
   * @description 绘制上下文菜单
   * @return {*} 
   * @memberof AppMobCalendar
   */
  public renderContextMenu() {
    const mouseEvent = this.activeData.value?.mouseEvent;
    const data = this.activeData.value?.data;
    if (this.activeData.value) {
      const calendarItem = (this.c.controlInstance as IPSSysCalendar).getPSSysCalendarItems()?.find((_calendarItem: IPSSysCalendarItem) => 
        Object.is(data.itemType.toLowerCase(), _calendarItem.itemType.toLowerCase())
      );
      const contextMenu = ModelTool.findPSControlByName(
        calendarItem?.getPSDEContextMenu?.()?.name as string,
        this.c.controlInstance.getPSControls(),
      );
      if (contextMenu && (this.isShowContextMenu || App.isPreviewMode())) {
        this.isShowContextMenu = false;
        const otherParams = {
          key: Util.createUUID(),
          navDatas: [data.curData],
          mouseEvent: mouseEvent,
          contextMenuActionModel: {},
        };
        return this.computeTargetCtrlData(contextMenu, otherParams);
      }
    }
  }

  /**
   * @description 绘制日历部件
   * @return {*}
   * @memberof AppMobCalendar
   */
  render() {
    if (!this.controlIsLoaded.value) {
      return null;
    }
    const calendarStyle = this.c.calendarStyle;
    const calendarItems = (this.c.controlInstance as IPSSysCalendar).getPSSysCalendarItems() || [];
    return (
      <div
        class={{ ...this.classNames }}
        onTouchmove={($event: any) => this.gotouchmove($event)}
        onTouchstart={($event: any) => this.gotouchstart($event)}
      >
        {this.renderPullDownRefresh()}
        {this.renderContextMenu()}
        <div class={['calender-content', this.c.activeItem]}>
          {calendarStyle == 'MONTH' || calendarStyle == 'MONTH_TIMELINE' ? this.renderMonthCalendar() : null}
          {calendarStyle == 'DAY' ? this.renderDayCalendar() : null}
          {calendarStyle == 'WEEK' || calendarStyle == 'WEEK_TIMELINE' ? this.renderWeekCalendar() : null}
          {calendarItems && this.c.groupData.length == 0 && this.c.expMode !== 'LEFT' ? this.renderSegment(calendarItems) : null}
          {this.c.expMode !== 'LEFT' ? <div class='calendar-events'>
            {this.renderTimelineCalendar(calendarStyle)}
            {this.renderBatchToolbar()}
          </div> : null}
        </div>
      </div>
    );
  }
}
// 移动端日历部件
export const AppMobCalendarComponent = GenerateComponent(AppMobCalendar, Object.keys(new AppMobCalendarProps()));
