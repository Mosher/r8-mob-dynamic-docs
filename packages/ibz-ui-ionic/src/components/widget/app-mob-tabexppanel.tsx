import {
  AppMobMapProps,
  AppMobTabExpPanelProps,
  IMobTabExpPanelCtrlController,
  MobTabExpPanelCtrlController,
} from 'ibz-core';
import { reactive, shallowReactive } from 'vue';
import { CtrlComponentBase } from './ctrl-component-base';
import { GenerateComponent } from '../component-base';
import { IPSDETabViewPanel } from '@ibiz/dynamic-model-api';

/**
 * 移动端分页导航面板部件
 *
 * @export
 * @class AppMobTabExpPanel
 * @extends CtrlComponentBase
 */
export class AppMobTabExpPanel extends CtrlComponentBase<AppMobTabExpPanelProps> {
  /**
   * @description 部件控制器
   * @protected
   * @type {IMobTabExpPanelCtrlController}
   * @memberof AppMobTabExpPanel
   */
  protected c!: IMobTabExpPanelCtrlController;

  /**
   * @description 设置响应式
   * @memberof AppMobTabExpPanel
   */
  setup() {
    this.c = shallowReactive<MobTabExpPanelCtrlController>(
      this.getCtrlControllerByType('TABEXPPANEL') as MobTabExpPanelCtrlController,
    );
    super.setup();
  }

  /**
   * @description 初始化响应式属性
   * @memberof AppMobTabExpPanel
   */
  public initReactive() { 
    super.initReactive();
    this.c.activeItem = reactive(this.c.activeItem);
  }

  /**
   * @description 分页节点切换
   * @private
   * @param {*} $event 节点数据
   * @return {*}
   * @memberof AppMobTabExpPanel
   */
  private ionChange($event: any) {
    const { detail: _detail } = $event;
    if (!_detail) {
      return;
    }
    const { value: _value } = _detail;
    if (_value) {
      this.c.activeItemChange(_value);
    }
  }

  /**
   * @description 输出分页头部
   * @param {IPSDETabViewPanel[]} allControls 所有分页视图面板部件
   * @return {*}
   * @memberof AppMobTabExpPanel
   */
  public renderSegment(allControls: IPSDETabViewPanel[]) {
    return (
      <ion-segment
        class='tabexppanel-header'
        scrollable={true}
        value={this.c.activeItem}
        onIonChange={($event: any) => this.ionChange($event)}
      >
        {allControls.map((viewPanel: IPSDETabViewPanel) => {
          const counterData = this.c.getCounterData(viewPanel);
          return (
            <ion-segment-button value={viewPanel?.name}>
              <ion-label>
                {viewPanel.getPSSysImage() && <app-icon icon={viewPanel.getPSSysImage()}></app-icon>}
                {this.$tl(viewPanel.getCapPSLanguageRes()?.lanResTag,viewPanel.caption) }
                {counterData && Object.is(this.c.activeItem, viewPanel.name) ? (
                  <ion-badge class='badge'>{counterData}</ion-badge>
                ) : null}
              </ion-label>
            </ion-segment-button>
          );
        })}
      </ion-segment>
    );
  }

  /**
   * @description 分页视图面板部件渲染
   * @param {IPSDETabViewPanel[]} allControls
   * @return {*}
   * @memberof AppMobTabExpPanel
   */
  public renderTabViewPanel(allControls: IPSDETabViewPanel[]) {
    return allControls.map((item: IPSDETabViewPanel) => {
      const otherParams = {
        key: item.name,
      };
      if (Object.is(this.c.activeItem, item.name)) {
        return this.computeTargetCtrlData(item, otherParams);
      }
    });
  }

  /**
   * @description 分页导航面板部件渲染
   * @return {*}
   * @memberof AppMobTabExpPanel
   */
  render() {
    if (!this.controlIsLoaded.value) {
      return null;
    }
    const { width, height } = this.c.controlInstance;
    const chartControlstyle = {
      width: width ? `${width}px` : '',
      height: height ? `${height}px` : '100%',
    };
    const allControls = this.c.controlInstance.getPSControls() as IPSDETabViewPanel[];
    return (
      <div class={{ ...this.classNames }} style={chartControlstyle}>
        {this.renderPullDownRefresh()}
        {this.renderSegment(allControls)}
        {this.renderTabViewPanel(allControls)}
      </div>
    );
  }
}

export const AppMobTabExpPanelComponent = GenerateComponent(AppMobTabExpPanel, Object.keys(new AppMobMapProps()));
