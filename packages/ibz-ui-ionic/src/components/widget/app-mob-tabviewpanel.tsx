import { h, shallowReactive } from 'vue';
import { IPSAppDEView } from '@ibiz/dynamic-model-api';
import { AppMobTabViewPanelProps, IMobTabViewPanelCtrlController, MobTabViewPanelCtrlController } from 'ibz-core';
import { CtrlComponentBase } from './ctrl-component-base';
import { GenerateComponent } from '../component-base';

/**
 * 移动端分页视图面板部件
 *
 * @export
 * @class AppMobTabViewPanel
 * @extends CtrlComponentBase
 */
export class AppMobTabViewPanel extends CtrlComponentBase<AppMobTabViewPanelProps> {
  /**
   * @description 部件控制器
   * @protected
   * @type {IMobTabViewPanelCtrlController}
   * @memberof AppMobTabViewPanel
   */
  protected c!: IMobTabViewPanelCtrlController;

  /**
   * @description 嵌入视图组件
   * @type {string}
   * @memberof AppMobTabViewPanel
   */
  public viewComponent: string = '';

  /**
   * @description 设置响应式
   * @memberof AppMobTabViewPanel
   */
  setup() {
    this.c = shallowReactive<MobTabViewPanelCtrlController>(
      this.getCtrlControllerByType('TABVIEWPANEL') as MobTabViewPanelCtrlController,
    );
    super.setup();
  }

  /**
   * @description 初始化视图组件名称
   * @param {IPSAppDEView} embeddedView
   * @memberof AppMobTabViewPanel
   */
  public getViewComponent(embeddedView: IPSAppDEView) {
    this.viewComponent = App.getComponentService().getViewTypeComponent(
      embeddedView?.viewType,
      embeddedView?.viewStyle,
      embeddedView?.getPSSysPFPlugin()?.pluginCode,
    );
  }

  /**
   * @description 绘制关系视图
   * @public
   * @memberof AppMobTabViewPanel
   */
  public renderEmbedView() {
    const embeddedView = this.c.controlInstance.getEmbeddedPSAppDEView();
    if (!embeddedView) {
      App.getNoticeService().warning(this.$tl('widget.mobtabviewpanel.noembeddedview', '嵌入视图不存在'));
      return null;
    }
    this.getViewComponent(embeddedView);
    const { context, viewParam } = this.c.getNavParams();
    return h(this.viewComponent, {
      viewPath: embeddedView?.modelPath,
      navContext: context,
      navParam: viewParam,
      viewState: this.c.viewState,
      viewShowMode: 'EMBEDDED',
      isShowCaptionBar: false,
      modelData:embeddedView
    });
  }

  /**
   * @description 分页视图面板部件渲染
   * @return {*}
   * @memberof AppMobTabViewPanel
   */
  render() {
    if (!this.controlIsLoaded.value) {
      return null;
    }
    return <div class={{ ...this.classNames }}>{this.renderEmbedView()}</div>;
  }
}

export const AppMobTabViewPanelComponent = GenerateComponent(AppMobTabViewPanel, Object.keys(new AppMobTabViewPanelProps()));
