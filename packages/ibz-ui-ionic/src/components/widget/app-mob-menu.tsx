import { reactive, shallowReactive, Ref, ref } from 'vue';
import { IPSAppMenuItem } from '@ibiz/dynamic-model-api';
import { IMobMenuCtrlController, IParam, MobMenuCtrlController } from 'ibz-core';
import { AppMobMenuProps } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { CtrlComponentBase } from './ctrl-component-base';

/**
 * 应用菜单
 *
 * @export
 * @class AppMobMenu
 * @extends {ComponentBase}
 */
export class AppMobMenu extends CtrlComponentBase<AppMobMenuProps> {
  /**
   * @description 部件控制器
   * @protected
   * @type {IMobMenuCtrlController}
   * @memberof AppMobMenu
   */
  protected c!: IMobMenuCtrlController;

  /**
   * @description 菜单项集合
   * @type {IPSAppMenuItem[]}
   * @memberof AppMobMenu
   */
  public menuItems!: IPSAppMenuItem[];

  /**
   * @description 设置响应式
   * @memberof AppMobMenu
   */
  setup() {
    this.c = shallowReactive<MobMenuCtrlController>(this.getCtrlControllerByType('APPMENU') as MobMenuCtrlController);
    super.setup();
  }

  /**
   * @description 部件初始化
   * @memberof AppMobMenu
   */
  init() {
    super.init();
    this.menuItems = this.c.controlInstance.getPSAppMenuItems() || [];
    this.setDefaultSelectMenu();
  }

  /**
   * @description 设置默认选中菜单
   * @return {*}
   * @memberof AppMobMenu
   */
  public setDefaultSelectMenu() {
    if (Object.is(this.c.ctrlShowMode, 'QUICKMENU')) {
      return null;
    }
    let defaultSelectMenu:any = null;
    this.menuItems.map((menu: IPSAppMenuItem) => {
      if (menu.openDefault) {
        defaultSelectMenu = menu;
      }
    });
    if (!defaultSelectMenu) {
      defaultSelectMenu = this.menuItems[0];
    }
    if (defaultSelectMenu) {
      this.menuItemClick(defaultSelectMenu, true);
    }
  }

  /**
   * @description 获取计数器计数
   * @param {IPSAppMenuItem} item
   * @return {*}
   * @memberof AppMobMenu
   */
  public getCounter(item: IPSAppMenuItem) {
    const counterService = this.c.getCounterService();
    if (item.counterId && counterService) {
      return counterService?.counterData?.[item.counterId.toLowerCase()];
    } else {
      return null;
    }
  }

  /**
   * @description 根据菜单项获取菜单权限
   * @param {IPSAppMenuItem} menuItem
   * @return {*}
   * @memberof AppMobMenu
   */
  public getMenusPermission(menuItem: IPSAppMenuItem) {
    if (!App.isPreviewMode() && menuItem.accessKey) {
      return this.c.appUIService.getResourceOPPrivs(menuItem.accessKey);
    } else {
      return true;
    }
  }

  /**
   * @description 菜单项点击
   * @param {IPSAppMenuItem} item
   * @memberof AppMobMenu
   */
  public async menuItemClick(item: IPSAppMenuItem, isGlobal: boolean) {
    const func = item.getPSAppFunc();
    if (App.isPreviewMode()) {
      return;
    }
    if (func && func.id) {
      const path: string = await App.getFuncService().executeAppFunction(
        func.id,
        this.c.context,
        this.c.viewParam,
        this,
        isGlobal,
      );
      if (path) {
        App.getOpenViewService().openView(path);
      }
    }
  }

  /**
   * @description 绘制菜单项
   * @param {IPSAppMenuItem} item
   * @return {*}
   * @memberof AppMobMenu
   */
  public renderMenuItem(item: IPSAppMenuItem) {
    if (item.hidden || !this.getMenusPermission(item)) {
      return null;
    }
    const cssName: string = item.getPSSysCss()?.cssName || '';
    const icon = item.getPSSysImage();
    return (
      <ion-tab-button
        onClick={() => this.menuItemClick(item, true)}
        v-badge={{ count: this.getCounter(item), offset: [15, 10] }}
        tab={item.name}
        class={cssName}
      >
        <ion-label>
          {this.$tl(item.getCapPSLanguageRes()?.lanResTag, item.caption)}
        </ion-label>
        {icon ? <app-icon icon={icon}></app-icon> : <app-icon name='home'></app-icon>}
      </ion-tab-button>
    );
  }

  /**
   * @description 绘制快速菜单项
   * @return {*}
   * @memberof AppMobMenu
   */
  public renderQuickMenu() {
    switch (this.c.controlInstance.controlStyle?.toLocaleLowerCase()) {
      case 'listview':
        return (
          <app-menu-list
            menu={this.menuItems}
            appUIService={this.c.appUIService}
            modelService={this.c.modelService}
            onMenuClick={(item: IPSAppMenuItem) => this.menuItemClick(item, false)}
          ></app-menu-list>
        );
      default:
        return (
          <app-menu-icon
            menu={this.menuItems}
            appUIService={this.c.appUIService}
            modelService={this.c.modelService}
            onMenuClick={(item: IPSAppMenuItem) => this.menuItemClick(item, false)}
          ></app-menu-icon>
        );
    }
  }

  /**
   * @description 绘制内容
   * @return {*}
   * @memberof AppMobMenu
   */
  render() {
    if (!this.controlIsLoaded.value) {
      return null;
    }
    if (Object.is(this.c.ctrlShowMode, 'QUICKMENU')) {
      return <div class={{ ...this.classNames }}>{this.renderQuickMenu()}</div>;
    } else {
      return (
        <ion-tabs class={{ ...this.classNames }}>
          <ion-router-outlet></ion-router-outlet>
          <ion-tab-bar slot='bottom'>
            {this.menuItems.map((item: IPSAppMenuItem) => {
              return this.renderMenuItem(item);
            })}
          </ion-tab-bar>
        </ion-tabs>
      );
    }
  }
}

// 应用菜单组件
export const AppMobMenuComponent = GenerateComponent(AppMobMenu, Object.keys(new AppMobMenuProps()));
