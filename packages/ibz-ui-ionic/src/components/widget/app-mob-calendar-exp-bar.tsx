import { h, shallowReactive } from 'vue';
import { AppMobCalendarExpBarProps, IMobCalendarExpBarController, MobCalendarExpBarController, Util } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { ExpBarCtrlComponentBase } from './exp-bar-ctrl-component-base';

/**
 * @description 移动端日历导航部件
 * @export
 * @class AppMobCalendarExpBar
 * @extends {ExpBarCtrlComponentBase<AppMobCalendarExpBarProps>}
 */
export class AppMobCalendarExpBar extends ExpBarCtrlComponentBase<AppMobCalendarExpBarProps> {
  /**
   * 部件控制器
   *
   * @protected
   * @memberof AppMobCalendarExpBar
   */
  protected c!: IMobCalendarExpBarController;

  /**
   * 设置响应式
   *
   * @public
   * @memberof AppMobCalendarExpBar
   */
  setup() {
    this.c = shallowReactive<MobCalendarExpBarController>(
      this.getCtrlControllerByType('CALENDAREXPBAR') as MobCalendarExpBarController,
    );
    super.setup();
  }

  /**
   * @description 渲染导航视图
   * @return {*} 
   * @memberof AppMobCalendarExpBar
   */
  public renderNavView() {
    if (this.c.selection) {
      const { viewComponent, navContext, navParam, viewPath } = this.c.selection;
      if (viewComponent) {
        return h(viewComponent, {
          key: Util.createUUID(),
          navContext: navContext,
          navParam: navParam,
          viewPath: viewPath,
          viewState: this.c.viewState,
          viewShowMode: 'EMBEDDED',        
        })
      }
    }
  }
  
  /**
   * @description 渲染导航栏部件
   * @return {*}
   * @memberof AppMobChartExpBar
   */
  render() {
    if (!this.controlIsLoaded.value) {
      return null;
    }
    const { width, height } = this.c.controlInstance;
    const controlStyle = {
      width: width ? width + 'px' : '',
      height: height ? height + 'px' : '',
    };
    const expMode = this.c.controlInstance.getPSControlParam()?.ctrlParams?.MODE;
    return (
      <div class={{ 'exp-bar': true, ...this.classNames, 'exp-bar-left':expMode == 'LEFT' }} style={controlStyle}>
        <div class='exp-bar-container'>
          <div class='container__multi_data_ctrl'>{this.renderXDataControl()}</div>
          {expMode == 'LEFT' ? 
          <div class="container__nav_view">
            {this.renderNavView()}
          </div> : null}      
        </div>
      </div>
    );
  }
}
// 移动端地图导航部件 组件
export const AppMobCalendarExpBarComponent = GenerateComponent(AppMobCalendarExpBar, Object.keys(new AppMobCalendarExpBarProps()));
