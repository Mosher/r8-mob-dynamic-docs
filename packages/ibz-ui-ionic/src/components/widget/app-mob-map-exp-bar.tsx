import { shallowReactive } from 'vue';
import { AppMobMapExpBarProps, IMobMapExpBarController, MobMapExpBarController } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { ExpBarCtrlComponentBase } from './exp-bar-ctrl-component-base';

/**
 * @description 移动端地图导航部件
 * @export
 * @class AppMobMapExpBar
 * @extends {DEViewComponentBase<AppMobMapExpBarProps>}
 */
export class AppMobMapExpBar extends ExpBarCtrlComponentBase<AppMobMapExpBarProps> {
  /**
   * 部件控制器
   *
   * @protected
   * @memberof AppMobMapExpBar
   */
  protected c!: IMobMapExpBarController;

  /**
   * 设置响应式
   *
   * @public
   * @memberof AppMobMapExpBar
   */
  setup() {
    this.c = shallowReactive<MobMapExpBarController>(
      this.getCtrlControllerByType('MAPEXPBAR') as MobMapExpBarController,
    );
    super.setup();
  }
  
  /**
   * @description 渲染导航栏部件
   * @return {*}
   * @memberof AppMobChartExpBar
   */
  render() {
    if (!this.controlIsLoaded.value) {
      return null;
    }
    const { width, height } = this.c.controlInstance;
    const controlStyle = {
      width: width ? width + 'px' : '',
      height: height ? height + 'px' : '',
    };
    const expMode = this.c.controlInstance.getPSControlParam()?.ctrlParams?.MODE;
    return (
      <div class={{ 'exp-bar': true, ...this.classNames, 'exp-bar-left':expMode == 'LEFT' }} style={controlStyle}>
        <div class='exp-bar-container'>
          <div class='container__multi_data_ctrl'>{this.renderXDataControl()}</div>
          {expMode == 'LEFT' ? 
          <div class="container__nav_view">
            {this.renderNavView()}
          </div> : null}           
        </div>
      </div>
    );
  }
}
// 移动端地图导航部件 组件
export const AppMobMapExpBarComponent = GenerateComponent(AppMobMapExpBar, Object.keys(new AppMobMapExpBarProps()));
