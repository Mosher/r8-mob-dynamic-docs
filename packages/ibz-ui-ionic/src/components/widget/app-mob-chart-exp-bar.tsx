import { shallowReactive } from 'vue';
import { AppMobChartExpBarProps, IMobChartExpBarController, MobChartExpBarController } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { ExpBarCtrlComponentBase } from './exp-bar-ctrl-component-base';

/**
 * @description 移动端图表导航栏
 * @export
 * @class AppMobChartExpBar
 * @extends {DEViewComponentBase<AppMobChartExpBarProps>}
 */
export class AppMobChartExpBar extends ExpBarCtrlComponentBase<AppMobChartExpBarProps> {
  /**
   * 部件控制器
   *
   * @protected
   * @memberof AppMobChartExpBar
   */
  protected c!: IMobChartExpBarController;

  /**
   * 设置响应式
   *
   * @public
   * @memberof AppMobChartExpBar
   */
  setup() {
    this.c = shallowReactive<MobChartExpBarController>(
      this.getCtrlControllerByType('CHARTEXPBAR') as MobChartExpBarController,
    );
    super.setup();
  }

  /**
   * @description 渲染导航栏部件
   * @return {*}
   * @memberof AppMobChartExpBar
   */
  render() {
    if (!this.controlIsLoaded.value) {
      return null;
    }
    const { width, height } = this.c.controlInstance;
    const controlStyle = {
      width: width ? width + 'px' : '',
      height: height ? height + 'px' : '',
    };
    return (
      <div class={{ 'exp-bar': true, ...this.classNames }} style={controlStyle}>
        <div class='exp-bar-container'>
          <div class='container__multi_data_ctrl'>{this.renderXDataControl()}</div>
        </div>
      </div>
    );
  }
}
// 移动端图表导航栏 组件
export const AppMobChartExpBarComponent = GenerateComponent(AppMobChartExpBar, Object.keys(new AppMobChartExpBarProps()));
