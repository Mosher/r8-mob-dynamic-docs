import { shallowReactive } from 'vue';
import {IPSDEEditForm } from '@ibiz/dynamic-model-api';
import { AppMobWizardPanelProps, IMobWizardPanelController, MobWizardPanelController } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { CtrlComponentBase } from './ctrl-component-base';

/**
 * @description 移动端向导面板部件
 * @export
 * @class AppMobWizardPanel
 * @extends {DEViewComponentBase<AppMobWizardPanelProps>}
 */
export class AppMobWizardPanel extends CtrlComponentBase<AppMobWizardPanelProps> {

  /**
   * @description 部件控制器
   * @protected
   * @type {IMobWizardPanelController}
   * @memberof AppMobWizardPanel
   */
  protected c!: IMobWizardPanelController;

  /**
   * @description 设置响应式
   * @memberof AppMobWizardPanel
   */
  setup() {
    this.c = shallowReactive<MobWizardPanelController>(
      this.getCtrlControllerByType('WIZARDPANEL') as MobWizardPanelController
    );
    super.setup();
  }

  /**
   * @description 获取激活步骤表单实例对象
   * @protected
   * @param {string} name
   * @return {*}  {(IPSDEEditForm | undefined)}
   * @memberof AppMobWizardPanel
   */
  protected getActiveFormInstance(name: string): IPSDEEditForm | undefined {
    const editForms = this.c.controlInstance.getPSDEEditForms() || [];
    return editForms.find((_form: IPSDEEditForm) => { return _form.name.toLowerCase() === name.toLowerCase(); });
  }

  /**
   * @description 获取激活步骤
   * @protected
   * @return {*}  {number}
   * @memberof AppMobWizardPanel
   */
  protected getActiveStep(): number {
    return this.c.wizardForms.indexOf(this.c.activeForm);
  }

  /**
   * @description 按钮显示状态
   * @protected
   * @param {string} type 按钮步骤类型
   * @return {*}  {boolean}
   * @memberof AppMobWizardPanel
   */
  protected buttonStatus(type: string): boolean {
    const actions: string[] = this.c.stepActions[this.c.activeForm]?.actions || [];
    if (actions && actions.indexOf(type) < 0) {
      return false;
    }
    return true;
  }

  /**
   * @description 上一步
   * @protected
   * @param {*} event 源事件对象
   * @memberof AppMobWizardPanel
   */
  protected onClickPrev(event: any) {
    this.c.handlePrevious();
  }

  /**
   * @description 下一步
   * @protected
   * @param {*} event 源事件对象
   * @memberof AppMobWizardPanel
   */
  protected onClickNext(event: any) {
    this.c.handleNext();
  }

  /**
   * @description 完成
   * @protected
   * @param {*} event 源事件对象
   * @memberof AppMobWizardPanel
   */
  protected onClickFinish(event: any) {
    this.c.handleFinish();
  }

  /**
   * @description 渲染向导步骤表单
   * @memberof AppMobWizardPanel
   */
  public renderWizardStepForm() {
    const formInstance = this.getActiveFormInstance(this.c.activeForm);
    if (formInstance) {
      const otherParams = {
        viewState: this.c.wizardState,
        key: `${this.c.controlInstance.codeName}-${formInstance.codeName}`
      }
      return this.computeTargetCtrlData(formInstance, otherParams);
    }
  }

  /**
   * @description 渲染向导面板组件
   * @return {*} 
   * @memberof AppMobWizardPanel
   */
  render() {
    if (!this.controlIsLoaded.value) {
      return;
    }
    const { width, height } = this.c.controlInstance;
    const controlStyle = {
      width: width ? width + 'px' : '',
      height: height ? height + 'px' : ''
    }
    const wizard = this.c.controlInstance.getPSDEWizard();
    return (
      <div class={{ ...this.classNames }} style={controlStyle}>
        {
          this.c.controlInstance.showStepBar ?
            <div class="wizard__steps_container">
              <app-steps
                active={this.getActiveStep()}
                steps={this.c.steps}
                panelInstance={this}
              />
            </div> : null
        }
        <div class="wizard__step_form_container">
          {this.renderWizardStepForm()}
        </div>
        <ion-footer>
          <ion-buttons>
            {this.buttonStatus('PREV') && wizard ?
              <ion-button onClick={(event: any) => { this.onClickPrev(event); }}>
                {this.$tl(wizard.getPrevCapPSLanguageRes()?.lanResTag, wizard.prevCaption) || this.$tl('share.previous', '上一步')}
              </ion-button> : null}
            {this.buttonStatus('NEXT') && wizard?
              <ion-button onClick={(event: any) => { this.onClickNext(event); }}>
                {this.$tl(wizard.getNextCapPSLanguageRes()?.lanResTag, wizard.nextCaption) || this.$tl('share.next', '下一步')}
              </ion-button> : null}
            {this.buttonStatus('FINISH') && wizard?
              <ion-button onClick={(event: any) => { this.onClickFinish(event); }}>
                {this.$tl(wizard.getFinishCapPSLanguageRes()?.lanResTag, wizard.finishCaption) || this.$tl('share.finish', '完成')}
              </ion-button> : null}
          </ion-buttons>
        </ion-footer>
      </div>
    )
  }
}
// 移动端向导面板部件 组件
export const AppMobWizardPanelComponent = GenerateComponent(AppMobWizardPanel, Object.keys(new AppMobWizardPanelProps()));