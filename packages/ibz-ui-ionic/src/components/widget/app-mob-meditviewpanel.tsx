import { h, Ref, ref, shallowReactive } from 'vue';
import { MDCtrlComponentBase } from './md-ctrl-component-base';
import { GenerateComponent } from '../component-base';
import {
  AppMobMEditViewPanelProps,
  IMobMEditViewPanelCtrlController,
  MobMEditViewPanelCtrlController,
  AppViewEvents,
} from 'ibz-core';
import { IPSAppDEView } from '@ibiz/dynamic-model-api';

export class AppMobMEditViewPanel extends MDCtrlComponentBase<AppMobMEditViewPanelProps> {
  /**
   * @description 多表单编辑视图面板部件控制器
   * @protected
   * @type {IMobMEditViewPanelCtrlController}
   * @memberof AppMobMEditViewPanel
   */
  protected c!: IMobMEditViewPanelCtrlController;

  /**
   * @description 激活项
   * @private
   * @type {Ref<string>}
   * @memberof AppMobMEditViewPanel
   */
  private activeItem: Ref<string> = ref('');

  /**
   * @description 设置响应式
   * @memberof AppMobMEditViewPanel
   */
  setup() {
    this.c = shallowReactive<IMobMEditViewPanelCtrlController>(
      this.getCtrlControllerByType('MULTIEDITVIEWPANEL') as MobMEditViewPanelCtrlController,
    );
    super.setup();
  }

  /**
   * @description 分页节点切换
   * @private
   * @param {*} $event 数据源
   * @return {*}
   * @memberof AppMobMEditViewPanel
   */
  private ionChange($event: any) {
    const { detail: _detail } = $event;
    if (!_detail) {
      return;
    }
    const { value: _value } = _detail;
    if (_value) {
      this.activeItem.value = _value;
    }
  }

  /**
   * @description 处理视图事件
   * @private
   * @param {string} viewname 视图名
   * @param {string} action 行为
   * @param {*} data 数据
   * @memberof AppMobMEditViewPanel
   */
  private handleViewEvent(viewname: string, action: string, data: any) {
    switch (action) {
      case AppViewEvents.DATA_CHANGE:
        this.c.viewDataChange(data);
        break;
    }
  }

  /**
   * @description 绘制嵌入视图
   * @param {*} item 项数据
   * @return {*}
   * @memberof AppMobMEditViewPanel
   */
  renderEmbedView(item: any) {
    const embedView = this.c.controlInstance.getEmbeddedPSAppView() as IPSAppDEView;
    const viewComponent = App.getComponentService().getViewTypeComponent(
      embedView.viewType,
      embedView.viewStyle,
      embedView.getPSSysPFPlugin()?.pluginCode,
    );
    if (viewComponent) {
      return h(viewComponent, {
        key: item.id,
        viewPath: embedView.modelPath,
        navContext: item.context,
        navParam: item.viewParam,
        navDatas: this.c.navDatas,
        viewState: this.c.viewState,
        isShowCaptionBar: false,
        viewShowMode: 'EMBEDDED',
        modelData:embedView,
        previewData: this.c.previewData,        
        onViewEvent: ({ viewname, action, data }: { viewname: string; action: string; data: any }) => {
          this.handleViewEvent(viewname, action, data);
        },
      });
    } else {
      return <div class='no-embedded-view'>{this.$tl('widget.mobmeditviewpanel.noembeddedview','无嵌入视图')}</div>;
    }
  }

  /**
   * @description 绘制行记录样式
   * @return {*}
   * @memberof AppMobMEditViewPanel
   */
  renderRow() {
    return this.c.items.map((item: any) => {
      return [
        <div class='multieditviewpanel-row'>
          <ion-card>
            <ion-card-content>{this.renderEmbedView(item)}</ion-card-content>
          </ion-card>
        </div>,
      ];
    });
  }

  /**
   * @description 绘制上分页样式
   * @return {*}
   * @memberof AppMobMEditViewPanel
   */
  renderTabtop() {
    return (
      <div class='multieditviewpanel-tabtop'>
        {this.renderPageHead()}
        {this.renderTabTopContent()}
      </div>
    );
  }

  /**
   * @description 绘制分页头
   * @return {*}
   * @memberof AppMobMEditViewPanel
   */
  renderPageHead() {
    if (!this.activeItem.value) {
      this.activeItem.value = this.c.items[0].id;
    }
    return (
      <ion-segment
        scrollable={true}
        value={this.activeItem.value}
        onIonChange={($event: any) => this.ionChange($event)}
      >
        {this.c.items.map((item: any) => {
          return (
            <ion-segment-button value={item.id}>
              <ion-label>{item.srfmajortext}</ion-label>
            </ion-segment-button>
          );
        })}
      </ion-segment>
    );
  }

  /**
   * @description 绘制上分页内容区
   * @return {*}
   * @memberof AppMobMEditViewPanel
   */
  renderTabTopContent() {
    return (
      <div class='tabtop-content'>
        {this.c.items.map((item: any) => {
          if (item.id == this.activeItem.value) {
            return this.renderEmbedView(item);
          }
        })}
      </div>
    );
  }

  /**
   * @description 绘制内容区
   * @return {*}
   * @memberof AppMobMEditViewPanel
   */
  renderContent() {
    if (this.c.items?.length > 0) {
      if (Object.is('ROW', this.c.panelStyle)) {
        return this.renderRow();
      } else {
        return this.renderTabtop();
      }
    }
  }

  /**
   * @description 绘制多表单编辑面板
   * @return {*}
   * @memberof AppMobMEditViewPanel
   */
  render() {
    if (!this.controlIsLoaded.value) {
      return null;
    }
    const { width, height } = this.c.controlInstance;
    const controlstyle = {
      width: width ? `${width}px` : '100%',
      height: height ? `${height}px` : '100%',
    };
    return (
      <div class={{ ...this.classNames }} style={controlstyle}>
        {this.renderPullDownRefresh()}
        {this.renderContent()}
        <div class='meditviewpanel-add-icon'>
          <app-icon onClick={() => this.c.add()} name='add'></app-icon>
        </div>
      </div>
    );
  }
}
// 多表单编辑视图面板
export const AppMobMEditViewPanelComponent = GenerateComponent(AppMobMEditViewPanel, Object.keys(new AppMobMEditViewPanelProps()));
