import { shallowReactive } from 'vue';
import { AppCtrlProps, AppCtrlControllerBase } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { CtrlComponentBase } from './ctrl-component-base';

/**
 * 未支持部件
 *
 * @export
 * @class AppMobNotSupportedControl
 * @extends {ComponentBase}
 */
export class AppMobNotSupportedControl extends CtrlComponentBase<AppCtrlProps> {
  /**
   * 设置响应式
   *
   * @public
   * @memberof AppMobNotSupportedControl
   */
  setup() {
    this.c = shallowReactive(new AppCtrlControllerBase(this.props));
    super.setup();
  }

  /**
   * 绘制内容
   *
   * @public
   * @memberof AppMobNotSupportedControl
   */
  render() {
    if (!this.controlIsLoaded.value) {
      return null;
    }
    const flexStyle: string =
      'width: 100%; height: 100%; overflow: auto; display: flex;justify-content:center;align-items:center;';
    return (
      <div class='app-ctrl' style={flexStyle}>
        {`${this.$tl('share.notsupported','暂未支持')} ${this.c?.controlInstance?.controlType} ${this.$tl('share.widget','部件')}`}
      </div>
    );
  }
}

// 应用菜单组件
export const AppMobNotSupportedControlComponent = GenerateComponent(AppMobNotSupportedControl, Object.keys(new AppCtrlProps()));
