import { shallowReactive, Ref, ref } from 'vue';
import { IPSDEToolbarItem } from '@ibiz/dynamic-model-api';
import {
  AppMobContextMenuProps,
  IParam,
  MobContextMenuCtrlController,
  MobContextMenuEvents,
  IMobContextMenuCtrlController,
} from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { CtrlComponentBase } from './ctrl-component-base';

/**
 * 移动端上下文菜单
 *
 * @exports
 * @class AppMobContextMenu
 * @extends CtrlComponentBase
 */
class AppMobContextMenu extends CtrlComponentBase<AppMobContextMenuProps> {
  /**
   * @description
   * @protected
   * @type {MobContextMenuCtrlController}
   * @memberof AppMobContextMenu
   */
  protected c!: IMobContextMenuCtrlController;

  /**
   * @description 是否显示弹窗
   * @type {Ref<boolean>}
   * @memberof AppMobContextMenu
   */
  public isShowPopover: Ref<boolean> = ref(true);

  /**
   * @description 上下文菜单项集合
   * @type {IPSDEToolbarItem[]}
   * @memberof AppMobContextMenu
   */
  public toolbarItems!: IPSDEToolbarItem[];

  /**
   * @description 触发上下文菜单的数据源
   * @type {IParam}
   * @memberof AppMobContextMenu
   */
  public item: IParam = {};

  /**
   * @description 响应式
   * @memberof AppMobContextMenu
   */
  setup() {
    this.c = shallowReactive<MobContextMenuCtrlController>(
      this.getCtrlControllerByType('CONTEXTMENU') as MobContextMenuCtrlController,
    );
    super.setup();
  }

  /**
   * @description 部件初始化
   * @memberof AppMobContextMenu
   */
  init() {
    super.init();
    this.toolbarItems = this.c.controlInstance.getPSDEToolbarItems() || [];
    this.item = this.c.navDatas[0];
  }

  /**
   * @description 设置弹窗状态
   * @param {boolean} state 状态
   * @memberof AppMobContextMenu
   */
  public setOpenState(state: boolean) {
    this.isShowPopover.value = state;
  }

  /**
   * @description 上下文菜单项点击
   * @param {IPSDEToolbarItem} detail 界面行为模型
   * @param {MouseEvent} event 点击事件源
   * @memberof AppMobContextMenu
   */
  public itemClick(detail: IPSDEToolbarItem, event: MouseEvent) {
    const data = {
      name: this.c.controlInstance.name,
      detail: detail,
      event: event,
      data: this.item,
    };
    this.setOpenState(false);
    this.emitCtrlEvent({
      controlname: this.c.controlInstance.controlType?.toLowerCase(),
      action: MobContextMenuEvents.MENUITEM_CLICK,
      data: data,
    });
  }

  /**
   * @description 绘制界面行为
   * @param {IPSDEToolbarItem} item 界面行为模型
   * @return {*}
   * @memberof AppMobContextMenu
   */
  public renderUIAction(item: IPSDEToolbarItem) {
    const visible = this.c.contextMenuActionModel[item.name]
      ? this.c.contextMenuActionModel[item.name]?.visabled
      : true;
    const disabled = this.c.contextMenuActionModel[item.name]
      ? this.c.contextMenuActionModel[item.name]?.disabled
      : false;
    if (visible) {
      return (
        <ion-item
          button
          class='context-menu-item'
          detail={false}
          disabled={disabled}
          onClick={(event: MouseEvent) => this.itemClick(item, event)}
        >
          <ion-label>
            {item?.showIcon && item.getPSSysImage() && <app-icon icon={item.getPSSysImage()}></app-icon>}
            {item.showCaption && 
              <span>
                {this.$tl(item.getCapPSLanguageRes()?.lanResTag, item.caption)}
              </span>
            }
          </ion-label>
        </ion-item>
      );
    }
  }

  /**
   * @description 根据项类型绘制项
   * @param {IPSDEToolbarItem} item 项模型
   * @return {*}
   * @memberof AppMobContextMenu
   */
  public renderToolbarItemsByType(item: IPSDEToolbarItem) {
    if (item.itemType === 'DEUIACTION') {
      return this.renderUIAction(item);
    } else if (item.itemType === 'SEPERATOR') {
      // todo 分割线
      return;
    } else if (item.itemType === 'ITEMS') {
      // todo 分组
      return;
    } else if (item.itemType === 'RAWITEM') {
      // return <span>{item.rawContent}</span>
    }
  }

  /**
   * @description 绘制上下文菜单内容
   * @return {*}
   * @memberof AppMobContextMenu
   */
  renderContextMenuContent() {
    return (
      <ion-list>
        {this.toolbarItems.map((item: IPSDEToolbarItem) => {
          return this.renderToolbarItemsByType(item);
        })}
      </ion-list>
    );
  }

  /**
   * @description 绘制上下文菜单
   * @return {*}
   * @memberof AppMobContextMenu
   */
  render() {
    if (!this.controlIsLoaded.value) {
      return;
    }
    return (
      <ion-popover
        cssClass='app-mob-contextmenu'
        isOpen={this.isShowPopover.value}
        event={this.c.mouseEvent}
        onDidDismiss={() => this.setOpenState(false)}
      >
        {this.renderContextMenuContent()}
      </ion-popover>
    );
  }
}

// 移动端上下文菜单组件
export const AppMobContextMenuComponent = GenerateComponent(AppMobContextMenu, Object.keys(new AppMobContextMenuProps()));
