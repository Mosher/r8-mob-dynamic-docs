import { AppMDCtrlProps, IAppMDCtrlController, ModelTool } from 'ibz-core';
import { reactive } from 'vue';
import { CtrlComponentBase } from './ctrl-component-base';

/**
 * 多数据部件组件基类
 *
 * @export
 * @class MDCtrlComponentBase
 */
export class MDCtrlComponentBase<Props extends AppMDCtrlProps> extends CtrlComponentBase<AppMDCtrlProps> {
  /**
   * @description 多数据部件控制器
   * @protected
   * @type {IAppMDCtrlController}
   * @memberof MDCtrlComponentBase
   */
  protected c!: IAppMDCtrlController;

  /**
   * @description 初始化响应式属性
   * @memberof MDCtrlComponentBase
   */
  public initReactive() {
    super.initReactive();
    this.c.items = reactive(this.c.items);
    this.c.sort = reactive(this.c.sort);
    this.c.groupDetail = reactive(this.c.groupDetail);
    this.c.groupData = reactive(this.c.groupData);
    this.c.selections = reactive(this.c.selections);
    this.c.dataMap = reactive(this.c.dataMap);
  }

  /**
   * @description 绘制快速操作栏
   * @memberof MDCtrlComponentBase
   */
  public renderQuickToolbar() {
    const quickToolbar = ModelTool.findPSControlByName(
      `${this.c.controlInstance.name}_quicktoolbar`,
      (this.c.controlInstance as any).getPSControls(),
    );
    if (quickToolbar) {
      return this.computeTargetCtrlData(quickToolbar);
    }
  }
  
  /**
   * @description 绘制批操作工具栏
   * @memberof MDCtrlComponentBase
   */
  public renderBatchToolbar() {
    if (this.c.selections.length > 0) {
      const batchToolbar = ModelTool.findPSControlByName(
        `${this.c.controlInstance.name}_batchtoolbar`,
        (this.c.controlInstance as any).getPSControls(),
      );
      if (batchToolbar) {
        return this.computeTargetCtrlData(batchToolbar);
      }
    }
  }
}
