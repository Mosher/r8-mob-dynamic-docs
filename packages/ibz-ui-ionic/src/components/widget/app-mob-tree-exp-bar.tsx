import { shallowReactive } from 'vue';
import { AppMobTreeExpBarProps, IMobTreeExpBarController, MobTreeExpBarController } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { ExpBarCtrlComponentBase } from './exp-bar-ctrl-component-base';

/**
 * @description 移动端树导航部件
 * @export
 * @class AppMobTreeExpBar
 * @extends {DEViewComponentBase<AppMobTreeExpBarProps>}
 */
export class AppMobTreeExpBar extends ExpBarCtrlComponentBase<AppMobTreeExpBarProps> {
  /**
   * 部件控制器
   *
   * @protected
   * @memberof AppMobTreeExpBar
   */
  protected c!: IMobTreeExpBarController;

  /**
   * 设置响应式
   *
   * @public
   * @memberof AppMobTreeExpBar
   */
  setup() {
    this.c = shallowReactive<MobTreeExpBarController>(
      this.getCtrlControllerByType('TREEEXPBAR') as MobTreeExpBarController,
    );
    super.setup();
  }

  /**
   * @description 渲染导航栏部件
   * @return {*}
   * @memberof AppMobChartExpBar
   */
  render() {
    if (!this.controlIsLoaded.value) {
      return null;
    }
    const { width, height } = this.c.controlInstance;
    const controlStyle = {
      width: width ? width + 'px' : '',
      height: height ? height + 'px' : '',
    };
    return (
      <div class={{ 'exp-bar': true, ...this.classNames }} style={controlStyle}>
        <div class='exp-bar-container'>
          <div class='container__multi_data_ctrl'>{this.renderXDataControl()}</div>
        </div>
      </div>
    );
  }
}
// 移动端树导航部件 组件
export const AppMobTreeExpBarComponent = GenerateComponent(AppMobTreeExpBar, Object.keys(new AppMobTreeExpBarProps()));
