import { shallowReactive } from 'vue';
import { AppMobDashBoardProps, MobDashboardController, IMobDashboardController, ICtrlActionResult } from 'ibz-core';
import { GenerateComponent } from '../component-base';
import { CtrlComponentBase } from './ctrl-component-base';
import { IPSControl, IPSDBPortletPart } from '@ibiz/dynamic-model-api';

/**
 * 移动端数据看板部件
 *
 * @class AppMobDashboard
 */
class AppMobDashboard extends CtrlComponentBase<AppMobDashBoardProps> {
  /**
   * @description 移动端数据看板部件控制器基类实例对象
   * @type {IMobDashboardController}
   * @memberof AppMobDashboard
   */
  protected c!: IMobDashboardController;

  /**
   * @description 设置响应式
   * @public
   * @memberof AppMobDashboard
   */
  setup() {
    this.c = shallowReactive<MobDashboardController>(
      this.getCtrlControllerByType('DASHBOARD') as MobDashboardController,
    );
    super.setup();
  }

  /**
   * @description 处理自定义面板
   * @memberof AppMobDashboard
   */
  public handleCustom(event: MouseEvent) {
    if (this.c.handleCustom && this.c.handleCustom instanceof Function) {
      const view = {
        viewComponentName: 'app-dashboard-design',
      };
      this.c.handleCustom(view, event).then((ret: ICtrlActionResult) => {});
    }
  }

  /**
   * @description 渲染门户部件
   * @memberof AppMobDashboard
   */
  public renderPortlet(item: any, isCustom: boolean = false) {
    return this.computeTargetCtrlData(
      item,
      isCustom
        ? {
            isAdaptiveSize: true,
            class: 'dashboard-item user-customize',
          }
        : undefined,
    );
  }

  /**
   * @description 渲染自定义数据看板部件
   * @memberof AppMobDashboard
   */
  public renderCustomizedDashboard() {
    if (this.c.customDashboardModelData.length > 0) {
      return this.c.customDashboardModelData.map((item: any) => {
        return this.renderPortlet(item.modelData, true);
      });
    }
  }

  /**
   * @description 渲染静态数据看板内容
   * @memberof AppMobDashboard
   */
  public renderStaticDashboard() {
    const controls = this.c.controlInstance.getPSControls() || [];
    return controls.map((control: IPSControl, index: number) => {
      return this.renderPortlet(control as IPSDBPortletPart);
    });
  }

  /**
   * @description 渲染
   * @memberof AppMobDashboard
   */
  render() {
    if (!this.controlIsLoaded) {
      return;
    }
    return (
      <ion-grid class={{ ...this.classNames }}>
        {this.c.enableCustomized ? (
          <div
            class='dashboard-customized'
            onClick={(event: MouseEvent) => {
              this.handleCustom(event);
            }}
          >
            <app-icon name='settings-outline' />
          </div>
        ) : null}
        {this.c.hasCustomized ? this.renderCustomizedDashboard() : this.renderStaticDashboard()}
      </ion-grid>
    );
  }
}

//  移动端数据看板部件组件
export const AppMobDashboardComponent = GenerateComponent(AppMobDashboard, Object.keys(new AppMobDashBoardProps()));
