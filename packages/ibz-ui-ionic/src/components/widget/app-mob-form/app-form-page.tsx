import { shallowRef, renderSlot, Ref, ref } from 'vue';
import { FormPageController, IParam, IViewStateParam } from 'ibz-core';
import { Subject, Unsubscribable } from 'rxjs';
import { ComponentBase, GenerateComponent } from '../../component-base';

/**
 * 表单分页组件输入参数
 *
 * @class AppFormPageProps
 */
class AppFormPageProps {
  /**
   * 表单分页控制器
   *
   * @type {FormButtonController}
   * @memberof AppFormPageProps
   */
  c: IParam = {};

  /**
   * @description 部件name
   * @type {string}
   * @memberof AppFormPageProps
   */
  name: string = '';

  /**
   * @description 表单数据
   * @type {IParam}
   * @memberof AppFormPageProps
   */
  data: IParam = {};

  /**
   * 表单状态
   *
   * @type {Subject<IViewStateParam>}
   * @memberof AppFormPageProps
   */
  formState: Subject<IViewStateParam> | null = null;

  /**
   * 当前分页
   *
   * @type {string}
   * @memberof AppFormPageProps
   */
  currentPage: string = '';

  /**
   * 模型服务
   *
   * @type {IParam}
   * @memberof AppFormPageProps
   */
  modelService: IParam = {};
}

/**
 * 表单分页
 *
 * @class AppFormPage
 */
export default class AppFormPage extends ComponentBase<AppFormPageProps> {
  /**
   * @description 表单分页控制器实例对象
   * @type {FormPageController}
   * @memberof AppFormPage
   */
  public c!: FormPageController;

  /**
   * @description 部件name
   * @type {string}
   * @memberof AppFormPage
   */
  public name: string = '';

  /**
   * @description 表单状态
   * @type {Subject<IViewStateParam>}
   * @memberof AppFormPage
   */
  public formState: Subject<IViewStateParam> = new Subject();

  /**
   * @description 表单事件
   * @type {(Unsubscribable | undefined)}
   * @memberof AppFormPage
   */
  public formStateEvent: Unsubscribable | undefined;

  /**
   * @description 表单旧数据
   * @type {IParam}
   * @memberof AppFormPage
   */
  public oldFormData: IParam = {};

  /**
   * @description 表单数据
   * @type {IParam}
   * @memberof AppFormPage
   */
  public data: IParam = {};

  /**
   * @description 当前页
   * @type {IParam}
   * @memberof AppFormPage
   */
  public currentPage: Ref<string> = ref('');

  /**
   * 设置响应式
   *
   * @memberof AppFormPage
   */
  setup() {
    this.c = this.props.c as FormPageController;
    this.currentPage = shallowRef<string>(this.props.currentPage);
    this.name = this.props.name;
  }

  /**
   * 初始化
   *
   * @memberof AppFormPage
   */
  init() {
    this.c.initInputData(this.props);
  }

  public renderContent() {
    const { noTabHeader } = this.c.parentController.controlInstance;
    const { codeName } = this.c.model;
    const customClass = this.c.model.getPSSysCss()?.cssName;
    if (noTabHeader) {
      return <div class={[`${this.name}-page`, customClass]}>{renderSlot(this.ctx.slots, 'default')}</div>;
    } else {
      const customStyle = this.c.visible && this.currentPage.value == codeName ? '' : 'display: none;';
      return (
        <div class={[`${this.name}-page`, customClass]} style={customStyle}>
          {renderSlot(this.ctx.slots, 'default')}
        </div>
      );
    }
  }

  render() {
    const plugin = this.c.model.getPSSysPFPlugin?.();
    if (plugin && plugin.pluginCode) {
      const ctrlItemPluginInstance = App.getPluginService().getCtrlItemPluginByTag(plugin.pluginCode || '');
      if (ctrlItemPluginInstance) {
        return ctrlItemPluginInstance.renderItem(this.c, this.c.parentController, this);
      }
    } else {
      return this.renderContent();
    }
  }
}

//  表单分页组件
export const AppFormPageComponent = GenerateComponent(AppFormPage, Object.keys(new AppFormPageProps()));
