import { FormButtonController, IParam, IViewStateParam } from 'ibz-core';
import { Subject, Unsubscribable } from 'rxjs';
import { ComponentBase, GenerateComponent } from '../../component-base';

class AppFormButtonProps {
  /**
   * @description 表单按钮控制器实例对象
   * @type {IParam}
   * @memberof AppFormButtonProps
   */
  c: IParam = {};

  /**
   * @description 名称
   * @type {string}
   * @memberof AppFormButtonProps
   */
  name: string = '';

  /**
   * @description 表单数据
   * @type {IParam}
   * @memberof AppFormButtonProps
   */
  data: IParam = {};

  /**
   * @description 表单状态
   * @type {(Subject<IViewStateParam> | null)}
   * @memberof AppFormButtonProps
   */
  formState: Subject<IViewStateParam> | null = null;

  /**
   * 模型服务
   *
   * @type {IParam}
   * @memberof AppFormButtonProps
   */
   modelService: IParam = {};
}

/**
 * 表单按钮
 */
export class AppFormButton extends ComponentBase<AppFormButtonProps> {
  /**
   * @description 表单按钮控制器实例对象
   * @type {FormButtonController}
   * @memberof AppFormButton
   */
  public c!: FormButtonController;

  /**
   * @description 名称
   * @type {string}
   * @memberof AppFormButton
   */
  public name: string = '';

  /**
   * @description 表单状态
   * @type {Subject<IViewStateParam>}
   * @memberof AppFormButton
   */
  public formState: Subject<IViewStateParam> = new Subject();

  /**
   * @description 表单事件
   * @type {(Unsubscribable | undefined)}
   * @memberof AppFormButton
   */
  public formStateEvent: Unsubscribable | undefined;

  /**
   * @description 表单旧数据
   * @type {IParam}
   * @memberof AppFormButton
   */
  public oldFormData: IParam = {};

  /**
   * @description 表单数据
   * @type {IParam}
   * @memberof AppFormButton
   */
  public data: IParam = {};

  /**
   * @description 设置响应式
   * @memberof AppFormButton
   */
  setup() {
    this.c = this.props.c as FormButtonController;
  }

  /**
   * @description 初始化
   * @memberof AppFormButton
   */
  init() {
    this.c.initInputData(this.props);
  }

  /**
   * @description 表单按钮点击
   * @param event 源事件
   */
  public buttonClick(event: any) {
    this.c.buttonClick(event);
  }

  /**
   * @description 渲染表单按钮
   * @memberof AppFormButton
   */
  render() {
    const { width, height, showCaption, caption } = this.c.model;
    const sysImage = this.c.model.getPSSysImage();
    const sysCss = this.c.model.getPSSysCss();
    const captionClass = this.c.model.getLabelPSSysCss()?.cssName;
    const btnClass = width > 0 && height > 0 ? { width: `${width}px`, height: `${height}px` } : '';
    // 自定义类名
    const controlClassNames: any = { 'form-button': true };
    if (sysCss?.cssName) {
      Object.assign(controlClassNames, { [sysCss.cssName]: true });
    }
    return (
      <ion-button
        class={controlClassNames}
        style={btnClass}
        onClick={($event: any) => this.buttonClick($event)}
        disabled={this.c.disabled}
      >
        {sysImage && <app-icon icon={sysImage}></app-icon>}
        {showCaption && 
          <span class={captionClass}>
            {this.$tl(this.c.model?.getCapPSLanguageRes()?.lanResTag,caption)}
          </span>
        }
      </ion-button>
    );
  }
}

//  表单按钮组件
export const AppFormButtonComponent = GenerateComponent(AppFormButton, Object.keys(new AppFormButtonProps()));
