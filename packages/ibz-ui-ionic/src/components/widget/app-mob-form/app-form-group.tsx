import { renderSlot, Ref, ref } from 'vue';
import { Subject, Unsubscribable } from 'rxjs';
import { FormGroupPanelController, IParam, IViewStateParam } from 'ibz-core';
import { ComponentBase, GenerateComponent } from '../../component-base';

class AppFormGroupProps {
  /**
   * @description 表单分组控制器实例对象
   * @type {IParam}
   * @memberof AppFormGroupProps
   */
  c: IParam = {};

  /**
   * @description 部件name
   * @type {string}
   * @memberof AppFormGroupProps
   */
  name: string = '';

  /**
   * @description 表单数据
   * @type {IParam}
   * @memberof AppFormGroupProps
   */
  data: IParam = {};

  /**
   * @description 表单状态
   * @type {(Subject<IViewStateParam> | null)}
   * @memberof AppFormGroupProps
   */
  formState: Subject<IViewStateParam> | null = null;

  /**
   * 模型服务
   *
   * @type {IParam}
   * @memberof AppFormGroupProps
   */
   modelService: IParam = {};
}

export class AppFormGroup extends ComponentBase<AppFormGroupProps> {
  /**
   * @description 表单按钮控制器实例对象
   * @type {FormGroupPanelController}
   * @memberof AppFormGroup
   */
  public c!: FormGroupPanelController;

  /**
   * @description 部件name
   * @type {string}
   * @memberof AppFormGroup
   */
  public name: string = '';

  /**
   * @description 表单状态
   * @type {Subject<IViewStateParam>}
   * @memberof AppFormGroup
   */
  public formState: Subject<IViewStateParam> = new Subject();

  /**
   * @description 表单事件
   * @type {(Unsubscribable | undefined)}
   * @memberof AppFormGroup
   */
  public formStateEvent: Unsubscribable | undefined;

  /**
   * @description 表单旧数据
   * @type {IParam}
   * @memberof AppFormGroup
   */
  public oldFormData: IParam = {};

  /**
   * @description 表单数据
   * @type {IParam}
   * @memberof AppFormGroup
   */
  public data: IParam = {};

  /**
   * @description 收缩内容
   * @type {Ref<boolean>}
   * @memberof AppFormGroup
   */
  public collapseContent: Ref<boolean> = ref(false);

  /**
   * @description 是否显示弹窗
   * @type {Ref<boolean>}
   * @memberof AppFormGroup
   */
  public isShowPopover: Ref<boolean> = ref(false);

  /**
   * @description 弹窗位置
   * @type {Ref<any>}
   * @memberof AppFormGroup
   */
  public event: Ref<any> = ref();

  /**
   * @description 设置响应式
   * @memberof AppFormGroup
   */
  setup() {
    this.c = this.props.c as FormGroupPanelController;
    this.name = this.props.name;
  }

  /**
   * @description 初始化
   * @memberof AppFormGroup
   */
  init() {
    this.c.initInputData(this.props);
  }

  /**
   * @description 打开弹窗
   * @param state 状态
   * @memberof AppFormGroup
   */
  public setOpenState(state: boolean, $event?: any) {
    this.event.value = $event;
    this.isShowPopover.value = state;
  }

  /**
   * @description 点击展开
   * @memberof AppFormGroup
   */
  public clickCollapse() {
    const titleBarCloseMode = Number(this.c.model.titleBarCloseMode) || 0;
    if (titleBarCloseMode === 0) {
      return;
    }
    this.collapseContent.value = !this.collapseContent.value;
  }

  /**
   * @description 显示更多按钮点击
   * @memberof AppFormGroup
   */
  public onShowMoreButtonClick(event: any) {
    this.c.setManageContainerStatus(!this.c.manageContainerStatus);
  }

  /**
   * @description 执行分组行为
   * @param $event 事件源
   * @param item 数据
   * @memberof AppFormGroup
   */
  public handleGroupUIActionClick($event: any, item: any) {
    this.c.onGroupUIActionClick($event, item);
  }

  /**
   * @description  选中
   * @param $event 事件源
   * @param item 数据
   */
  public onSelect($event: any, item: any) {
    this.handleGroupUIActionClick($event, item);
    this.setOpenState(false);
  }

  /**
   * @description 绘制项展开
   * @param uiActionGroup 界面行为组
   * @memberof AppFormGroup
   */
  public renderItemsExpend(uiActionGroup: any) {
    return uiActionGroup.details.map((item: any) => {
      if (item.visabled) {
        return (
          <div class='action-item'>
            <ion-buttons
              onClick={($event: any) => this.handleGroupUIActionClick($event, item)}
              disabled={item.disabled}
            >
              {item.isShowIcon && item.icon && <app-icon icon={item.icon}></app-icon>}
              {item.isShowCaption && <span>{item.caption}</span>}
            </ion-buttons>
          </div>
        );
      }
    });
  }

  /**
   * @description 绘制分组展开
   * @param uiActionGroup 界面行为组
   * @memberof AppFormGroup
   */
  public renderGroupExpend(uiActionGroup: any) {
    return (
      <ion-buttons onClick={($event: any) => this.setOpenState(true, $event)}>
        {uiActionGroup.caption}
        <ion-popover
          cssClass='group-actions'
          isOpen={this.isShowPopover.value}
          event={this.event.value}
          onDidDismiss={() => this.setOpenState(false)}
        >
          <ion-list>
            {uiActionGroup.details.map((item: any) => {
              if (item.visabled) {
                return (
                  <ion-item
                    button
                    detail={false}
                    disabled={item.disabled}
                    onClick={($event: any) => this.onSelect($event, item)}
                  >
                    {item.isShowIcon && item.icon && <app-icon icon={item.icon}></app-icon>}
                    {item.isShowCaption && <span>{item.caption}</span>}
                  </ion-item>
                );
              }
            })}
          </ion-list>
        </ion-popover>
      </ion-buttons>
    );
  }

  /**
   * @description 渲染表单分组界面行为组
   * @memberof AppFormGroup
   */
  public renderUIActionGroup() {
    const uiActionGroup = this.c.uiActionGroup;
    if (uiActionGroup?.details?.length > 0) {
      return (
        <div class={`${this.name}-group-action`}>
          {Object.is(uiActionGroup.extractMode, 'ITEM')
            ? this.renderItemsExpend(uiActionGroup)
            : this.renderGroupExpend(uiActionGroup)}
        </div>
      );
    }
  }

  /**
   * @description 绘制底部
   * @memberof AppFormGroup
   */
  public renderFooter() {
    if (this.c.isManageContainer) {
      return (
        <ion-button
          class='show-more'
          onClick={(event: any) => {
            this.onShowMoreButtonClick(event);
          }}
        >
          {this.c.manageContainerStatus ? this.$tl('widget.mobformgroup.hidden','隐藏') : this.$tl('widget.mobformgroup.more','显示更多')}
        </ion-button>
      );
    }
  }

  /**
   * @description 渲染表单分组内容区
   * @memberof AppFormGroup
   */
  public renderContent() {
    return (
      <ion-row style={{ display: this.collapseContent.value ? 'none' : 'block' }} class={`${this.name}-group-content`}>
        {this.ctx.slots.default ? renderSlot(this.ctx.slots, 'default') : null}
      </ion-row>
    );
  }

  /**
   * @description 渲染表单分组标题
   * @memberof AppFormGroup
   */
  public renderLabel() {
    const { showCaption, caption, captionItemName } = this.c.model;
    const cssName = this.c.model.getLabelPSSysCss?.()?.cssName;
    const icon = this.c.model.getPSSysImage?.();
    if (showCaption) {
      return (
        <ion-item-divider class={cssName || ''}>
          <ion-label onClick={() => this.clickCollapse()}>
            <span class={`${this.name}-group-title`}>
              {icon ? <app-icon icon={icon}></app-icon> : null}
              {
                captionItemName ? 
                this.c.data[captionItemName.toLowerCase()] : 
                this.$tl(this.c.model?.getCapPSLanguageRes()?.lanResTag,caption)
              }
            </span>
          </ion-label>
          {this.renderUIActionGroup()}
        </ion-item-divider>
      );
    }
  }

  /**
   * @description 渲染表单分组
   * @memberof AppFormGroup
   */
  render() {
    return (
      <ion-row class={[ 'form-group', `form-group-${this.c.name}` ]}>
        {this.renderLabel()}
        {this.renderContent()}
        {this.renderFooter()}
      </ion-row>
    );
  }
}

//  表单分组组件
export const AppFormGroupComponent = GenerateComponent(AppFormGroup, Object.keys(new AppFormGroupProps()));
