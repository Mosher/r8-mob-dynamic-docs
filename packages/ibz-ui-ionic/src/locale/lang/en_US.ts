import common_en_US from '../components/common/common_en_US';
import view_en_US from '../components/view/view_en_US';
import widget_en_US from '../components/widget/widget_en_US';
// 组件国际化（英文）
export const en_US = () => {
  const app_en_US = {
    // 共享
    share: {
      ok: 'Ok',
      cancel: 'Cancel',
      notsupported: 'Temporary does not support',
      widget: 'Widget',
      previous: 'Previous',
      next: 'Next',
      finish: 'Finish',
      emptytext: 'Temporarily no data',
      year: 'Year',
      month: 'Month',
      day: 'Day',
      loading: 'Loading···'
    },
    // 通用组件
    common: common_en_US(),
    // 视图组件
    view: view_en_US(),
    // 部件组件
    widget: widget_en_US(),
  };
  return app_en_US;
};
