import common_zh_CN from '../components/common/common_zh_CN';
import view_zh_CN from '../components/view/view_zh_CN';
import widget_zh_CN from '../components/widget/widget_zh_CN';
// 组件国际化（中文）
export const zh_CN = () => {
  const app_zh_CN = {
    // 共享
    share: {
      ok: '确认',
      cancel: '取消',
      notsupported: '暂未支持',
      widget: '部件',
      previous: '上一步',
      next: '下一步',
      finish: '完成',
      emptytext: '暂无数据',
      year: '年',
      month: '月',
      day: '日',
      loading: '加载中···',
    },
    // 通用组件
    common: common_zh_CN(),
    // 视图组件
    view: view_zh_CN(),
    // 部件组件
    widget: widget_zh_CN(),
  };
  return app_zh_CN;
};
