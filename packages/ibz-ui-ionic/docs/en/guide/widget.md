# 部件

R8Mob动态模板的部件同视图一样，将与UI不相关的逻辑分隔开来，使用控制器完成对业务数据的操作或对用户行为的控制。

## 控制器

### 部件初始化

| 流程               | 方法名             | 说明                                                  |
| ------------------ | ------------------ | ----------------------------------------------------- |
| 初始化部件模型     | ctrlModelInit      | 构建部件模型                                          |
| 部件模型数据加载   | ctrlModelLoad      | 对部件模型进行填充                                    |
| 初始化部件基础数据 | ctrlBasicInit      | 对部件的基础数据进行解析，包括行为、界面模型等        |
| 初始化挂载状态集合 | initMountedMap     | 初始化部件挂载集合，用于判断部件的挂载状态            |
| 初始化实体服务     | initAppDataService | 根据部件的实体初始化实体服务                          |
| 初始化UI服务       | initAppUIService   | 根据部件的实体初始化UI服务                            |
| 初始化计数器服务   | initCounterService | 初始化部件的界面计数器，详情参见[计数器服务]()        |
| 初始化部件逻辑     | initControlLogic   | 初始化部件逻辑，详情参见[部件逻辑]()                  |
| 初始化部件         | ctrlInit           | 用于设置视图订阅对象(ViewState)，监听通知完成相应操作 |

::: tip 提示

更多逻辑参见具体部件

:::

### 部件挂载

R8Mob动态模板的部件下也会存在部件(例如导航栏部件下有多数据类部件)，因此需要等待当前部件下的所有部件完成挂载后，当前部件再执行加载逻辑。

部件挂载向上抛出挂载完成事件。

::: tip 提示

更多逻辑参见具体部件

:::

### 计数器刷新

当计数器对象发生变化时(例如切换分页)，部件进行计数器的刷新，重新获取界面计数器数据。

### 部件销毁

为防止内存溢出，在部件销毁时，R8Mob动态模板会从内存出删除该部件关联的资源，如：取消部件订阅，销毁计数器定时器，销毁部件逻辑定时器等。

## UI层

### 渲染部件

如果当前部件下还存在部件，则根据该部件实例对象计算出部件组件，计算组件参数后通过Vue渲染出该部件。

### 渲染下拉刷新

如果当前部件的视图开启了下拉刷新，同时该部件也支持，则会渲染下拉刷新。当用户下拉界面时，调用控制器的下拉刷新逻辑。

::: tip 提示

详细逻辑参见具体部件

:::

### 渲染无数据

部件在加载数据完成之前或后台无返回数据时会进行无数据界面的渲染，内容为用户配置的无数据提示内容。

## 部件分类

### 应用类

| <div style="width: 213px;text-align: left;">名称</div> | <div style="width: 213px;text-align: left;">说明</div>       | <div style="width: 213px;text-align: left;">链接</div> |
| ------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------ |
| 数据看板                                               | 数据看板用于展示门户部件，直接内容和快捷菜单。               | [数据看板](../widget/app/app-mob-dashboard.md)         |
| 门户部件                                               | 门户部件通常是用来呈现门户视图数据的展现，经常被数据看板部件引用。 | [门户部件](../widget/app/app-mob-portlet.md)           |

### 表单类

| <div style="width: 213px;text-align: left;">名称</div> | <div style="width: 213px;text-align: left;">说明</div> | <div style="width: 213px;text-align: left;">链接</div> |
| ------------------------------------------------------ | ------------------------------------------------------ | ------------------------------------------------------ |
| 实体表单                                               | 获取一个实体对应的数据的主要部件                       | [实体表单](../widget/app-mob-form.md)                  |

### 导航类

| <div style="width: 213px;text-align: left;">名称</div> | <div style="width: 213px;text-align: left;">说明</div>       | <div style="width: 213px;text-align: left;">链接</div>       |
| ------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 实体移动端图表导航部件                             | 实体移动端图表导航部件是用于图表导航视图中以导航栏的方式呈现的。 | [实体移动端图表导航部件](../widget/exp/app-mob-chart-exp-bar.md) |

### 多数据类

| <div style="width: 213px;text-align: left;">名称</div> | <div style="width: 213px;text-align: left;">说明</div>       | <div style="width: 213px;text-align: left;">链接</div> |
| ------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------ |
| 实体移动端多数据部件                                   | 实体移动端多数据部件是对多条业务数据的呈现，目前支持列表和图标两种显示样式。 | [实体移动端多数据部件](../widget/md/app-mob-mdctrl.md) |
| 实体移动端图表部件                                   | 实体移动端图表部件用来呈现数据的变化规律。 | [实体移动端图表部件](../widget/md/app-mob-chart.md) |
| 实体移动端日历部件                                   | 用于展示日期，以及日历对应的数据。 | [实体移动端日历部件](../widget/md/app-mob-calendar.md) |

### 面板类

| <div style="width: 213px;text-align: left;">名称</div> | <div style="width: 213px;text-align: left;">说明</div>       | <div style="width: 213px;text-align: left;">链接</div>       |
| ------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 实体移动端分页导航面板部件                             | 分页导航面板部件是用于分页视图中tab分页的方式呈现的，导航分页tab中展示出页面标题。 | [实体移动端分页导航面板部件](../widget/app-mob-tabexppanel.md) |
| 实体移动端分页视图面板部件                             | 分页实体的承载部件，用于解析视图数据并显示                   | [实体移动端分页视图面板部件](../widget/app-mob-tabviewpanel.md) |

### 其他类