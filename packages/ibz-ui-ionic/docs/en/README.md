---
home: true
title: Home
heroImage: /images/logo.png
actions:
  - text: Get Started
    link: /en/guide/
    type: primary
features:
  - title: Overall design
    details: Here is the description
  - title: Document structure
    details: Here is the description
  - title: Reading people
    details: Here is the description
  - title: Read suggestions
    details: Here is the description
  - title: Thanks
    details: Here is the description
footer: MIT Licensed | Copyright © 2018-present Evan You
---