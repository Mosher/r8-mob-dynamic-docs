# 多语言

R8Mob-Dynamic模板中集成了vue.js的Vue I18n。切换语言后会将应用的语言全部切换，其中应用中的多语言数据分为两种，一种为静态多语言数据，为基础文件中的多语言，数据固定不可配置。另一种为动态多语言数据，为模板文件中发布的多语言数据，也是配置平台配置的多语言数据。

## 资源

### 静态多语言数据

多语言配置的入口文件为src\locale，在这里引入了系统级的多语言和组件级的多语言。

系统级的多语言为系统级组件，如404、500、app-setting、app-theme等系统组件。

组件级多语言文件路径为packages\ibz-ui-ionic\src\locale，这里将组件中的多语言分为4类，分别为全局共享的share、通用组件common、部件组件widget和视图组件view。

- 使用方式

```ts
//系统级
this.$tl('login.password', '密码')}
//共享share
this.$tl('share.ok', '确认')}
//通用组件common
this.$tl('common.span.nocodelist', '代码表不存在')}
//部件widget
this.$tl('widget.mobformdruipart.tooltip', '请先保存主数据')}
//视图view
this.$tl('view.mobhtmlview.notexist', '抱歉，您访问的页面不存在！')}
```

### 动态多语言数据

配置平台可配置多语言数据，这些数据就是动态多语言数据。这些数据会以json模型的形式发布出来，获取这些数据时会调用modalService获取这些动态数据。

```ts
$model-service.ts

/**
 * 获取模型服务
 *
 */
export const GetModelService: any = function (param: any): Promise<any> {
  if (param && param.instTag && param.instTag2) {
    return GlobalHelp.getModelServiceByTag(param.instTag, param.instTag2);
  } else {
    return GlobalHelp.getModelService(param?.srfdynainstid);
  }
};
```



## 操作

### 获取多语言数据

在应用的入口文件中，添加了一个vue的全局多语言api，只要在vue中调用该方法就可拿取多语言数据。translate方法中首先判断是否为静态多语言，如果是就直接拿取，如果不是就调用modalService服务获取动态多语言数据。

::: tip
$te和$t方法为I18n中的方法，分别为判断是否存在多语言数据和获取多语言数据。
:::

```ts
$main.ts

// 添加全局翻译api
app.config.globalProperties.$tl = function (key: string, value?: string){
  return translate(key,this,value);
};
```



```ts
$packages\ibz-ui-ionic\src\locale\index.ts

// 多语言翻译
export const translate: Function = (key: string, context: any, value?: string) => {
    if(key){
        if (context.$te(key)) {
            return context.$t(key);
        } else {
            if (context.modelService) {
                const lanResource: any = context.modelService.getPSLang(key);
                return lanResource ? lanResource : value ? value : key;
            } else {
                return value ? value : key;
            }
        }
    }else{
        return value;
    }
}
```

### 切换多语言语种

多语言切换逻辑在app-setting组件中，切换语种后调用app-base.ts中的setActiveLanguage方法，该方法直接切换I18n插件的语种。

```ts
$app-base.ts

/**
 * 设置应用激活语言
 *
 * @memberof AppBase
 */
setActiveLanguage(lan: string): void {
  this.activeLanguage = lan;
  this.getI18n().global.locale = lan;
  localStorage.setItem('activeLanguage',lan);
}
```

::: tip
app-setting组件为应用系统配置组件，系统通知、系统消息、多语言、主题都在这个组件中。
:::

