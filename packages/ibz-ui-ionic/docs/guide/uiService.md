# UI服务
## 简介

UI服务主要服务于UI，位于ui-service目录下。主要包含以下几个服务：动作面板服务，应用权限服务，应用中心服务，应用组件服务，数据看板设计服务，模态服务，抽屉服务，loading服务，消息弹框服务，消息提示服务，主题服务，第三方服务和视图打开服务。以上这些服务都是由应用全局对象App来统一管理。

## 应用全局对象

在获取应用数据之后将application实例挂载到App上，这样在packages包中就获取到应用全局对象App。

```ts
    /**
     * 初始化应用服务
     *
     * @param {*} [router] 路由对象
     *
     * @memberof AuthGuard
     */
    public async initAppService(router: any): Promise<any> {
        try {
            const service = new AppModelService();
            ......
            this.application.setModel(service.app);
            this.application.setEnvironment(Environment);
            UIInstall(this.application);
            ServiceInstall(this.application);
            return true;
        } catch (error) {
            return false;
        }
    }
    
    /**
     * 安装插件
     *
     * @param config 配置
     */
    export const UIInstall = function (App: IApp): void {
      window.App = App;
    };
```

UI服务：

| 名称             | 说明                                                         | 链接                                                         |
| ---------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| 动作面板服务     | 该服务主要用于门户部件打开动作面板                           | [动作面板服务](../uiservice/app-action-sheet-service.md)     |
| 应用权限服务     | 该服务主要用于设置用户登录时的token和获取用户的界面访问权限  | [应用权限服务](../uiservice/app-auth-service.md)             |
| 应用中心服务     | 该服务主要用于全局的状态管理通知                             | [应用中心服务](../uiservice/app-center-service.md)           |
| 应用组件服务     | 该服务主要用于预置组件的设置和获取                           | [应用组件服务](../uiservice/app-component-service.md)        |
| 数据看板设计服务 | 该服务主要用于数据看板定制时模型数据的保存和门户部件的加载   | [数据看板设计服务](../uiservice/app-dashboard-design-service.md) |
| 模态服务     | 该服务主要用于实现视图的模态打开                             | [模态服务](../uiservice/app-modal-service.md)            |
| 抽屉服务     | 该服务主要用于实现视图的抽屉打开                             | [抽屉服务](../uiservice/app-drawer-service.md)           |
| loading服务      | 该服务主要用于提供全局的loading控制                          | [loading服务](../uiservice/app-loading-service.md)           |
| 消息弹框服务     | 该服务主要用于提供全局的消息弹窗控制                         | [消息弹框服务](../uiservice/app-msgbox-service.md)           |
| 消息提示服务     | 该服务主要用于提供全局的消息，成功，警告，错误的信息         | [消息提示服务](../uiservice/app-notice-service.md)           |
| 主题服务         | 该服务主要用于提供主题相关的功能，如设置主题，自定义主题等   | [主题服务](../uiservice/app-theme-service.md)                |
| 第三方服务       | 该服务主要用于管理第三方登录相关的服务                       | [第三方服务](../uiservice/app-third-party-service.md)        |
| 视图打开服务     | 该服务主要用于提供视图的4种打开方式（抽屉，模态，路由，新标签页） | [视图打开服务](../uiservice/app-view-open-service.md)        |



