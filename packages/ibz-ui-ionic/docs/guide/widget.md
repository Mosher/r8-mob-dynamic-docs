# 部件

R8移动端组件库将逻辑与ui分离，以便于管理与维护，且逻辑与ui都分为三层，分别为视图、部件、编辑器，它们的关系为一一对应。

视图绘制部件时，部件首先在初始化时解析模型数据，模型数据中有该部件所需的所有数据，模型解析完成之后才会绘制ui。每种部件都有其特有的绘制逻辑与数据解析逻辑，具体可参考相应部件。

## 部件分类

<table>
<thead>
<tr>
<th>部件分类</th>
<th>名称</th>
<th>说明</th>
<th>链接</th>
</tr>
</thead>
<tbody>
<tr>
<th rowspan="3">应用类</th>
<th>数据看板</th>
<th>数据看板用于展示门户部件，直接内容和快捷菜单。</th>
<th><a href="/widget/app/app-mob-dashboard.html">数据看板</a></th>
</tr>
<tr>
<th>门户部件</th>
<th>门户部件通常是用来呈现门户视图数据的展现，经常被数据看板部件引用。</th>
<th><a href="/widget/app/app-mob-portlet.html">门户部件</a></th>
</tr>
<tr>
<th>应用菜单</th>
<th>应用菜单是管理呈现视图页面的主要入口</th>
<th><a href="/widget/app/app-mob-menu.html">应用菜单</a></th>
</tr>
<tr>
<th rowspan="2">表单类</th>
<th>实体表单</th>
<th>由输入框、选择器、单选框、多选框等控件组成，用以收集、校验、提交数据。
</th>
<th><a href="/widget/form/app-mob-form.html">实体表单</a></th>
</tr>
<tr>
<th>搜索表单</th>
<th>跟普通表单组成结构相同，由输入框、选择器、单选框等空间组成。嵌入视图中，用于过滤视图中的数据。</th>
<th><a href="/widget/form/app-mob-searchform.html">搜索表单</a></th>
</tr>
<tr>
<th rowspan="5">导航类</th>
<th>实体移动端图表导航部件</th>
<th>实体移动端图表导航部件是用于图表导航视图中以导航栏的方式呈现的。</th>
<th><a href="/widget/exp/app-mob-chart-exp-bar.html">实体移动端图表导航部件</a></th>
</tr>
<tr>
<th>实体移动端地图导航部件</th>
<th>点击地图部件中的数据项将跳转到导航视图，数据项的呈现方式为点、线、区域</th>
<th><a href="/widget/exp/app-mob-map-exp-bar.html">实体移动端地图导航部件</a></th>
</tr>
<tr>
<th>实体移动端列表导航部件</th>
<th>列表导航部件以列表的形式呈现数据，可以打开配置的导航视图。</th>
<th><a href="/widget/exp/app-mob-list-exp-bar.html">实体移动端列表导航部件</a></th>
</tr>
<tr>
<th>实体移动端树导航栏部件</th>
<th>树导航部件以树的形式呈现数据，可以打开树项上配置的导航视图。</th>
<th><a href="/widget/exp/app-mob-tree-exp-bar.html">实体移动端树导航栏部件</a></th>
</tr>
<tr>
<th>实体移动端日历导航栏部件</th>
<th>日历导航部件以日历的形式呈现数据，可以点击日历项打开配置的导航视图。</th>
<th><a href="/widget/exp/app-mob-calendar-exp-bar.html">实体移动端日历导航栏部件</a></th>
</tr>
<tr>
<th rowspan="5">多数据类</th>
<th>实体移动端多数据部件</th>
<th>实体移动端多数据部件是对多条业务数据的呈现，目前支持列表和图标两种显示样式。</th>
<th><a href="/widget/md/app-mob-mdctrl.html">实体移动端多数据部件</a></th>
</tr>
<tr>
<th>实体移动端图表部件</th>
<th>实体移动端图表部件用来呈现数据的变化规律。</th>
<th><a href="/widget/md/app-mob-chart.html">实体移动端图表部件</a></th>
</tr>
<tr>
<th>实体移动端日历部件</th>
<th>用于展示日期，以及日历对应的数据。</th>
<th><a href="/widget/md/app-mob-calendar.html">实体移动端日历部件</a></th>
</tr>
<tr>
<th>实体移动端地图部件</th>
<th>展示系统地图部件</th>
<th><a href="/widget/md/app-mob-map.html">实体移动端地图部件</a></th>
</tr>
<tr>
<th>实体移动端树部件</th>
<th>展示系统树部件</th>
<th><a href="/widget/md/app-mob-tree.html">实体移动端树部件</a></th>
</tr>
<tr>
<th rowspan="7">面板类</th>
<th>实体移动端分页导航面板部件</th>
<th>分页导航面板部件是用于分页视图中tab分页的方式呈现的，导航分页tab中展示出页面标题。</th>
<th><a href="/widget/panel/app-mob-tabexppanel.html">实体移动端分页导航面板部件</a></th>
</tr>
<tr>
<th>实体移动端分页视图面板部件</th>
<th>分页实体的承载部件，用于解析视图数据并显示</th>
<th><a href="/widget/panel/app-mob-tabviewpanel.html">实体移动端分页视图面板部件</a></th>
</tr>
<tr>
<th>实体移动端选择视图面板部件</th>
<th>多数据类的选择视图的承载部件，用于解析视图数据并显示</th>
<th><a href="/widget/panel/app-mob-pickupviewpanel.html">实体移动端选择视图面板部件</a></th>
</tr>
<tr>
<th>实体移动端向导面板部件</th>
<th>向导视图的主要呈现部件，用于呈现向导的内容和控制向导逻辑</th>
<th><a href="/widget/panel/app-mob-wizard-panel.html">实体移动端向导面板部件</a></th>
</tr>
<tr>
<th>实体移动端状态向导面板部件</th>
<th>带有状态属性的向导面板，用于联接实体向导功能以及实体向导视图，为实体向导视图提供具体的向导内容。</th>
<th><a href="/widget/panel/app-mob-state-wizard-panel.html">实体移动端状态向导面板部件</a></th>
</tr>
<tr>
<th>实体移动端面板部件</th>
<th>面板部件可以方便的进行布局和展示数据</th>
<th><a href="/widget/panel/app-mob-panel.html">实体移动端面板部件</a></th>
</tr>
<tr>
<th>实体移动端多表单编辑视图面板部件</th>
<th>用来呈现业务单据，把有关联的数据全部呈现在页面</th>
<th><a href="/widget/md/app-mob-meditviewpanel.html">实体移动端多表单编辑视图面板部件</a></th>
</tr>
<tr>
<th rowspan="2">其他</th>
<th>工具栏</th>
<th>视图中的工具栏，可配置界面行为项触发特定行为，如保存、删除选中行等</th>
<th><a href="/widget/other/app-mob-toolbar.html">工具栏</a></th>
</tr>
<tr>
<th>上下文菜单</th>
<th>上下文菜单应用于当前选择数据的操作</th>
<th><a href="/widget/other/app-mob-contextmenu.html">上下文菜单</a></th>
</tr>
</tbody>
</table>

