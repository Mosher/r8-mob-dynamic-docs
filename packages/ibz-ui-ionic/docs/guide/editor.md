# 编辑器

R8移动端组件库将逻辑与ui分离，以便于管理与维护，且逻辑与ui都分为三层，分别为视图、部件、编辑器，它们的关系为一一对应。

R8移动端组件库将ui分为三层，分别为视图层、部件层、编辑器。编辑器主要负责用户交互、数据输入等，基本存在于表单部件和面板部件上。

编辑器按功能不同分为6类，分别为展示类、选择类、数据选择类、时间选择类、基础类、文件选择类，每种分类都有不同的功能。

<table>
<thead>
<tr>
<th>编辑器分类</th>
<th>名称</th>
<th>说明</th>
<th>链接</th>
</tr>
</thead>
<tbody>
<tr>
<th>展示类</th>
<th>标签</th>
<th>展示信息的唯一编辑器，默认的标签可展示基础类型数据、数据类型数据、代码表类型数据，其他类型数据需自行添加插件</th>
<th><a href="/editor/app-span.html">标签</a></th>
</tr>
<tr>
<th rowspan="7">基础类</th>
<th>文本框</th>
<th>用于输入信息，如文本、账号、密码</th>
<th><a href="/editor/app-input.html">文本框</a></th>
</tr>
<tr>
<th>步进器</th>
<th>步进器由增加按钮、减少按钮和输入框组成，用于在一定范围内输入、调整数字。</th>
<th><a href="/editor/app-stepper.html">步进器</a></th>
</tr>
<tr>
<th>开关部件</th>
<th>用于在打开和关闭状态之间进行切换。</th>
<th><a href="/editor/app-switch.html">开关部件</a></th>
</tr>
<tr>
<th>多行输入框</th>
<th>通过键盘输入多行内容。</th>
<th><a href="/editor/app-textarea.html">多行输入框</a></th>
</tr>
<tr>
<th>滑动输入条</th>
<th>滑动输入条，用于在给定的范围内选择一个值。</th>
<th><a href="/editor/app-slider.html">滑动输入条</a></th>
</tr>
<tr>
<th>评分器</th>
<th>用于对事物进行评级操作。</th>
<th><a href="/editor/app-rating.html">评分器</a></th>
</tr>
<tr>
<th>html编辑器</th>
<th>用于输入复杂的信息、如图片、链接等。</th>
<th><a href="/editor/app-rich-text.html">html编辑器</a></th>
</tr>
<tr>
<th rowspan="2">选择类</th>
<th>单选列表</th>
<th>在一组备选项中进行单选。</th>
<th><a href="/editor/app-radio-list.html">单选列表</a></th>
</tr>
<tr>
<th>下拉列表</th>
<th>以弹出框的形式在一组备选项中进行单选或多项。</th>
<th><a href="/editor/app-dropdown-list.html">下拉列表</a></th>
</tr>
<tr>
<th>数据选择类</th>
<th>数据选择</th>
<th>选择配置的选择视图上的数据。</th>
<th><a href="/editor/app-data-picker.html">数据选择</a></th>
</tr>
<tr>
<th>时间选择类</th>
<th>时间选择</th>
<th>选择时间</th>
<th><a href="/editor/app-date-picker.html">时间选择</a></th>
</tr>
<tr>
<th>文件上传类</th>
<th>文件上传</th>
<th>选择文件进行上传</th>
<th><a href="/editor/app-upload.html">文件上传</a></th>
</tr>
</tbody>
</table>
