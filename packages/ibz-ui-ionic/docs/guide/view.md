# 视图
R8移动端组件库将逻辑与ui分离，以便于管理与维护，且逻辑与ui都分为三层，分别为视图、部件、编辑器，它们的关系为一一对应。

绘制视图的为路由壳组件（app-route-shell.tsx），通过解析传递的route参数获取到对应的视图模型数据，然后解析模型中的视图类型与样式获取对应的实体名，最后绘制对应的视图并将视图模型路径（viewPath）传给视图。

绘制视图时会在初始化时通过视图模型路径（viewPath）获取对应模型，解析视图数据，完成后才会绘制ui。

## 视图分类
<table>
<thead>
<tr>
<th>视图分类</th>
<th>名称</th>
<th>说明</th>
<th>链接</th>
</tr>
</thead>
<tbody>
<tr>
<th rowspan="2">应用视图</th>
<th>应用首页视图</th>
<th>首页视图是整个系统的展示界面</th>
<th><a href="/view/app/app-index-view.html">应用首页视图</a></th>
</tr>
<tr>
<th>应用看板视图</th>
<th>应用看板视图用于承载数据看板部件，在该视图数据看板中快速菜单栏，嵌入视图和直接内容都作为门户部件来处理</th>
<th><a href="/view/app/app-portal-view.html">应用看板视图</a></th>
</tr>
<tr>
<th rowspan="7">单数据视图</th>
<th>实体编辑视图</th>
<th>获取用户输入数据的主要视图</th>
<th><a href="/view/de/app-mob-edit-view.html">实体编辑视图</a></th>
</tr>
<tr>
<th>实体数据看板视图</th>
<th>数据看板的呈现界面，数据看板中定义了系统门户部件以及直接内容</th>
<th><a href="/view/de/app-mob-dashboard-view.html">实体数据看板视图</a></th>
</tr>
<tr>
<th>实体分页导航视图</th>
<th>对多个视图进行整合，用于快速查看相应视图</th>
<th><a href="/view/de/app-mob-tabexp-view.html">实体分页导航视图</a></th>
</tr>
<tr>
<th>实体HTML视图</th>
<th>以iframe嵌入其他网页页面</th>
<th><a href="/view/de/app-mob-html-view.html">实体HTML视图</a></th>
</tr>
<tr>
<th>实体向导视图</th>
<th>以向导的形式对数据进行快速入录和修改</th>
<th><a href="/view/de/app-mob-wizard-view.html">实体向导视图</a></th>
</tr>
<tr>
<th>实体面板视图</th>
<th>以面板的形式呈现数据</th>
<th><a href="/view/de/app-mob-panel-view.html">实体面板视图</a></th>
</tr>    
<tr>
<th>实体选项操作视图</th>
<th>提供确认、取消的默认操作，通常用于弹出窗口的数据操作</th>
<th><a href="/view/de/app-mob-opt-view.html">实体选项操作视图</a></th>
</tr> 
<tr>
<th rowspan="6">多数据视图</th>
<th>实体多数据视图</th>
<th>展示业务数据的主要视图</th>
<th><a href="/view/demultidata/app-mob-md-view.html">实体多数据视图</a></th>
</tr>
<tr>
<th>实体图表视图</th>
<th>展示系统图表的主要视图</th>
<th><a href="/view/demultidata/app-mob-chart-view.html">实体图表视图</a></th>
</tr>
<tr>
<th>实体日历视图</th>
<th>展示与时间安排相关的信息的视图</th>
<th><a href="/view/demultidata/app-mob-calendar-view.html">实体日历视图</a></th>
</tr>
<tr>
<th>视图地图视图</th>
<th>展示地图数据信息</th>
<th><a href="/view/demultidata/app-mob-map-view.html">视图地图视图</a></th>
</tr>
<tr>
<th>实体树视图</th>
<th>展示树形结构数据信息</th>
<th><a href="/view/demultidata/app-mob-tree-view.html">实体树视图</a></th>
</tr>
<tr>
<th>实体多表单编辑视图</th>
<th>展示业务实体之间关联关系的视图</th>
<th><a href="/view/demultidata/app-mob-medit-view.html">实体多表单编辑视图</a></th>
</tr>
<tr>
<th rowspan="4">选择视图</th>
<th>实体数据选择视图</th>
<th>展示单项选择的视图</th>
<th><a href="/view/pickup/app-mob-pickup-view.html">实体数据选择视图</a></th>
</tr>
<tr>
<th>实体数据多项选择视图</th>
<th>展示多项选择的视图</th>
<th><a href="/view/pickup/app-mob-mpickup-view.html">实体数据多项选择视图</a></th>
</tr>
<tr>
<th>实体多数据选择视图</th>
<th>展示多数据呈现形式的选择视图</th>
<th><a href="/view/pickup/app-mob-pickup-mdview.html">实体多数据选择视图</a></th>
</tr>
<tr>
<th>实体树选择视图</th>
<th>展示树呈现形式的选择视图</th>
<th><a href="/view/pickup/app-mob-pickup-treeview.html">实体多数据选择视图</a></th>
</tr>

<tr>
<th rowspan="5">导航视图</th>
<th>实体图表导航视图</th>
<th>图表基础上增加导航功能</th>
<th><a href="/view/deexp/app-mob-chart-exp-view.html">实体图表导航视图</a></th>
</tr>
<tr>
<th>实体列表导航视图</th>
<th>列表基础上增加导航功能</th>
<th><a href="/view/deexp/app-mob-list-exp-view.html">实体列表导航视图</a></th>
</tr>
<tr>
<th>实体地图导航视图</th>
<th>地图基础上增加导航功能</th>
<th><a href="/view/deexp/app-mob-map-exp-view.html">实体地图导航视图</a></th>
</tr>
<tr>
<th>实体树导航视图</th>
<th>树基础上增加导航功能</th>
<th><a href="/view/deexp/app-mob-tree-exp-view.html">实体树导航视图</a></th>
</tr>
<tr>
<th>实体日历导航视图</th>
<th>日历基础上增加导航功能</th>
<th><a href="/view/deexp/app-mob-calendar-exp-view.html">实体日历导航视图</a></th>
</tr>

<tr>
<th>其他视图</th>
<th>实体自定义视图</th>
<th>可以根据需求来自定义部件</th>
<th><a href="/view/other/app-mob-custom-view.html">实体自定义视图</a></th>
</tr>
</tbody>
</table>

