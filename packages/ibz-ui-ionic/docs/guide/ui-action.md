# 界面行为

界面行为，是应用与用户交互的逻辑单元，用于完成应用业务逻辑行为功能。如：当用户点击按钮，应用进行页面跳转或者前后端交互对当前数据进行处理。其中，页面跳转和前后端交互这块逻辑都属于界面行为。根据界面行为是否绑定应用实体可分为实体级界面行为和应用级界面行为。

## 界面行为类型

根据界面行为的应用场景以及功能，可分为前台调用、后台调用、系统预置、工作流前台调用和工作流后台调用5个类别。目前，已经支持前台调用、后台调用和系统预置3种类别，另外工作流前台调用和工作流后台调用2种类别的界面行为待后续完善。

<table>
  <thead>
  <tr>
    <th width="115">界面行为类型</th>
    <th>说明</th>
    <th width="100">链接</th>
  </tr>
  </thead>
  <tr>
    <td>前台调用</td>
    <td>界面行为前台业务行为逻辑，常见有通过不同的方式打开目标视图再进行数据处理。</td>
    <td><a href="/ui-action/front-action.html">前台调用</a></td>
  </tr>
  <tr>
    <td>后台调用</td>
    <td>界面行为与后台做数据交互的业务行为逻辑，该界面行为支持打开视图，获取数据作为目标数据与后台交互。</td>
    <td><a href="/ui-action/backend-action.html">后台调用</a></td>
  </tr>
  <tr>
    <td>系统预置</td>
    <td>系统预置界面行为，是iBiz模型预定义的一批公共业务处理逻辑行为，包括保存、编辑、新建、删除和刷新等。</td>
    <td><a href="/ui-action/sys-action.html">系统预置</a></td>
  </tr>
  <tr>
    <td>工作流前台调用</td>
    <td>工作流界面行为前台业务行为逻辑，如打开工作流启动视图，工作流操作视图等。</td>
    <td>暂未支持</td>
  </tr>
  <tr>
    <td>工作流后台调用</td>
    <td>工作流界面行为与后台做数据交互的业务行为逻辑。</td>
    <td>暂未支持</td>
  </tr>
</table>

## 触发逻辑

以工具栏为例，界面的触发逻辑如下。

首先是工具栏点击，会抛出点击事件给视图

```ts
/**
 * @description 工具栏项点击
 * @param {string} name
 * @param {MouseEvent} e
 * @memberof AppMobToolbar
 */
public itemClick(item: any, e: MouseEvent): void {
  this.emitCtrlEvent({
    action: MobToolbarEvents.TOOLBAR_CLICK,
    controlname: this.c.controlInstance.name,
    data: { tag: `${this.c.name}_${item.name}_click`, event: e },
  });
  this.closeToolbar();
}
```

视图的控制器里会处理点击事件,用`getUIDataParam`方法获取视图上下文和视图参数等逻辑UI数据，用`getUIEnvironmentParam`获取逻辑环境数据。用`AppViewLogicUtil`执行视图逻辑的处理。

```ts
  /**
   * @description 处理部件事件
   * @param {string} controlname 部件名称
   * @param {string} action 部件行为
   * @param {*} data 数据
   * @memberof AppViewControllerBase
   */
  public handleCtrlEvent(controlname: string, action: string, data: any) {
    switch (action) {
      case MobToolbarEvents.TOOLBAR_CLICK:
        this.handleToolBarClick(data.tag, data.event);
        break;
        /* .... */
    }
  }

  /**
   * @description 处理工具栏点击事件
   * @param {string} tag 标识
   * @param {*} event 事件源
   * @return {*}
   * @memberof AppViewControllerBase
   */
  public handleToolBarClick(tag: string, event: MouseEvent) {
    if (App.getEnvironment() && App.getEnvironment()?.isPreviewMode) {
      return;
    }
    const appViewLogic: IPSAppViewLogic | null = this.viewInstance.findPSAppViewLogic(tag);
    const UIDataParam: IUIDataParam = this.getUIDataParam(this.getDatas());
    Object.assign(UIDataParam, { event: event });
    const UIEnvironmentParam: IUIEnvironmentParam = this.getUIEnvironmentParam();
    Object.assign(UIEnvironmentParam, { ctrl: this.ctrlRefsMap.get(this.xDataControlName) });
    AppViewLogicUtil.executeViewLogic(appViewLogic, UIDataParam, UIEnvironmentParam, this.viewCtx);
  }

```

视图逻辑处理，`executeViewLogic`方法用来找到最终的视图逻辑对象，`executeLogic`则是获取界面行为模型，然后通过界面行为的实体获取对应的实体UI服务，若界面行为没有实体，则根据视图模型的实体来，若视图也没有实体，则执行全局系统预置的界面行为。

```ts
/**
 * 视图逻辑服务
 *
 * @export
 * @class AppViewLogicUtil
 */
export class AppViewLogicUtil {
  /**
   * 执行视图逻辑
   *
   * @param item 触发标识
   * @param UIDataParam 操作数据参数
   * @param UIEnvironmentParam 操作环境参数
   * @param UIUtilParam 操作工具参数
   * @param viewlogicData 当前容器视图逻辑集合
   * @param params 附加参数
   * @memberof AppViewLogicUtil
   */
  public static executeViewLogic(
    item: string | IPSAppViewLogic | null,
    UIDataParam: IUIDataParam,
    UIEnvironmentParam: IUIEnvironmentParam,
    UIUtilParam: IUIUtilParam,
    viewlogicData?: Array<IPSAppViewLogic> | null,
    params?: IParam,
  ) {
    if (!item) {
      LogUtil.warn('无法找到当前视图逻辑');
      return;
    }
    let targetViewLogic: any;
    if (typeof item === 'string') {
      targetViewLogic = (viewlogicData as IPSAppViewLogic[]).find((_item: any) => {
        return _item.name === item;
      });
    } else {
      targetViewLogic = item;
    }
    this.executeLogic(targetViewLogic, UIDataParam, UIEnvironmentParam, UIUtilParam, params);
  }

  /**
   * 执行逻辑
   *
   * @param viewLogic 触发逻辑
   * @param UIDataParam 操作数据参数
   * @param UIEnvironmentParam 操作环境参数
   * @param UIUtilParam 操作工具参数
   * @param params 附加参数
   */
  public static async executeLogic(
    viewLogic: IPSAppViewLogic,
    UIDataParam: IUIDataParam,
    UIEnvironmentParam: IUIEnvironmentParam,
    UIUtilParam: IUIUtilParam,
    params?: IParam,
  ) {
    if (!viewLogic) {
      LogUtil.warn('无事件参数未支持');
      return;
    }
    if (!Object.is(viewLogic.logicType, 'APPVIEWUIACTION') || !viewLogic.getPSAppViewUIAction) {
      return;
    }
    const { sender: actionContext } = UIDataParam;
    const targetViewAction: IPSAppViewUIAction | null = viewLogic.getPSAppViewUIAction();
    if (!targetViewAction) {
      LogUtil.warn('视图界面行为不存在');
      return;
    }
    await (targetViewAction as IPSAppViewUIAction).fill();
    const targetUIAction = (await getPSUIActionByModelObject(targetViewAction)) as IPSAppDEUIAction;
    const contextJO: any = {};
    const paramJO: any = {};
    if (targetUIAction && targetUIAction.getPSAppDataEntity()) {
      const targetParentObject: IPSAppDEDataView | IPSControl = viewLogic.getParentPSModelObject() as
        | IPSAppDEDataView
        | IPSControl;
      const targetUIService: any = await App.getUIService().getService(
        actionContext.context,
        `${targetUIAction.getPSAppDataEntity()?.codeName.toLowerCase()}`,
      );
      await targetUIService.loaded();
      const targetParentDataEntity = targetParentObject?.getPSAppDataEntity?.();
      Object.assign(contextJO, { srfparentdemapname: targetParentDataEntity?.getPSDEName() });
      UIEnvironmentParam.parentDeCodeName = targetParentDataEntity?.codeName?.toLowerCase() || '';
      targetUIService.excuteAction(
        targetUIAction.uIActionTag,
        UIDataParam,
        UIEnvironmentParam,
        UIUtilParam,
        contextJO,
        paramJO,
      );
    } else {
      if (viewLogic.getParentPSModelObject() && viewLogic.getParentPSModelObject().M.getPSAppDataEntity) {
        const targetParentObject: IPSAppDEDataView | IPSControl = viewLogic.getParentPSModelObject() as
          | IPSAppDEDataView
          | IPSControl;
        const targetParentDataEntity = targetParentObject?.getPSAppDataEntity?.();
        Object.assign(contextJO, { srfparentdemapname: targetParentDataEntity?.getPSDEName() });
        UIEnvironmentParam.parentDeCodeName = targetParentDataEntity?.codeName.toLowerCase() || '';
        AppGlobalUtil.executeGlobalAction(
          targetUIAction.uIActionTag,
          UIDataParam,
          UIEnvironmentParam,
          UIUtilParam,
          contextJO,
          paramJO,
        );
      } else {
        AppGlobalUtil.executeGlobalAction(targetUIAction.uIActionTag, UIDataParam, UIEnvironmentParam, UIUtilParam);
      }
    }
  }
}
```

通过实体UI服务执行界面行为或者通过全局界面行为服务`AppGlobalUtil`来执行。具体详情看上面[界面行为类型](./ui-action.md#界面行为类型)。

## 界面逻辑

界面逻辑可以执行一系列附加的逻辑处理，详情看[界面逻辑](/ui-action/ui-logic.md)。根据界面逻辑附加类型，可以决定界面逻辑的执行时机。
- REPLACE：替换执行，原有的界面行为逻辑不执行，执行对应的界面逻辑来代替。
- AFTER：执行之后，在原有的界面行为执行之后，执行界面逻辑。