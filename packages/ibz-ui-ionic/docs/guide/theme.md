# 主题

主题用于页面的显示风格，每个主题都有不同的样式，如背景色、激活色、禁用色、字体色、字体大小等。

::: tip
也可使用主题的自定义构造器创建一个个性化的主题样式。
:::



### 核心逻辑

#### 初始化

应用初始化时app.ts中也会初始化应用主题，实现为调用主题服务的初始化主题方法。

```ts
$app.ts

/**
 * @description 初始化应用主题
 * @memberof App
 */
public initAppTheme() {
    const appThemeService = AppThemeService.getInstance();
    appThemeService.initAppTheme();
}
```

主题服务中的初始化主题方法中，先获取当前缓存中的激活主题，如果缓存中没有激活主题，则选择默认主题。获取主题后会将当前主题添加到应用的根节点上的class类中。

```ts
$app-theme-service.ts

/**
 * @description 初始化应用主题
 * @return {*}  {{ themeOptions: IParam[], activeTheme: { isCustom: boolean, theme: string } }}
 * @memberof AppThemeService
 */
public initAppTheme(): { themeOptions: IParam[]; activeTheme: { isCustom: boolean; theme: string } } {
  const activeTheme = this.getActiveUITheme();
  this.htmlElement.classList.add(activeTheme.theme);
  return {
    themeOptions: this.getThemeOptions(),
    activeTheme: activeTheme,
  };
}
/**
 * @description 获取当前激活主题
 * @return {*}  {{ isCustom: boolean, theme: string }}
 * @memberof AppThemeService
 */
public getActiveUITheme(): { isCustom: boolean; theme: string } {
  const active = localStorage.getItem('activeUITheme');
  if (active) {
    return JSON.parse(active);
  }
  return {
    isCustom: false,
    theme: 'default',
  };
}
```

主题的样式变量会默认全部添加到应用的样式表中，会根据根节点上的类名应用相应的主题变量。主题的样式文件路径为packages\ibz-ui-ionic\src\styles\core\themes。

::: tip
主题的相关逻辑中除了初始化外的逻辑都在app-theme组件中。
:::

#### 主题切换

当点击了app-theme组件中的其他预置主题后会调用主题服务的切换主题服务,实现逻辑也是改变根元素的class。

```ts
$app-theme.tsx

/**
 * @description 切换主题
 * @param {IParam} item 主题
 * @param {*} event 源事件对象
 * @return {*}
 */
changeInternalTheme(item: IParam, event: any) {
  this.showCustomPanel = false;
  if (this.activeTheme.theme === item.name && !this.activeTheme.isCustom) {
    return;
  }
  this.appThemeService.changeInternalTheme(this.activeTheme.theme, item.name, this.themeOptions);
  this.activeTheme.theme = item.name;
  this.activeTheme.isCustom = false;
},
```



```ts
$app-theme-service.ts

/**
 * @description 切换预置主题
 * @param {string} oldVal 旧主题标识
 * @param {string} newVal 新主题标识
 * @param {IParam[]} options 主题配置
 * @return {*}
 * @memberof AppThemeService
 */
public changeInternalTheme(oldVal: string, newVal: string, options: IParam[]) {
  const element = document.documentElement;
  if (oldVal) {
    element.classList.remove(oldVal);
  }
  if (newVal) {
    element.classList.add(newVal);
  }
  this.setActiveUITheme(newVal);
  this.resetAppTheme(options);
}
```



### 自定义应用主题

app-theme组件中点击自定义主题按钮会展开自定义主题面板，这里可以更改所有可供选择主题样式变量值。

::: tip
当改变某个样式变量后会将改变的变量以dom元素的行间样式添加到根元素中，以达到覆盖默认样式。
:::

```ts
$app-theme.tsx

/**
 * @description 颜色选择变化
 * @param {IParam} item
 * @param {*} event
 */
colorPickerChange(item: IParam, event: any) {
  item.isChange = true;
  const doc = document.documentElement;
  doc.style.setProperty(item.key, item.default);
  if (item.shade) {
    const shade = this.getColorShade(item.default);
    item.shade = shade;
    doc.style.setProperty(`${item.key}-shade`, shade);
  }
  if (item.tint) {
    const tint = this.getColorTint(item.default);
    item.tint = tint;
    doc.style.setProperty(`${item.key}-tint`, tint);
  }
},
```

### 主题配置详情

主题的实现逻辑为通过给根节点添加css变量，其他组件直接调用这些全局变量从而实现应用主题。这些全局变量分为三部分，分别为基础色、分页栏和工具栏三部分。基础色为ionic自带的主题配置，它控制了应用大部分的主题色。

::: tip
切换主题样式就是切换根节点的这些全局变量，直接更改它的颜色。
:::

#### 基础色

Ionic 有九种默认颜色，可用于更改许多组件的颜色。每种颜色实际上是多个属性的集合，包括在整个 Ionic 中使用的`shade`和`tint`。

| <div style="width: 213px;text-align: left;">名称</div>|<div style="width: 213px;text-align: left;">变量名</div>|<div style="width: 213px;text-align: left;">说明</div>|
| -------------- | --------------------------- | ---------------------------------- |
| 主要色         | --ion-color-primary         | 应用的主要颜色                     |
| 主要色（阴影） | --ion-color-primary-shade   | 主要颜色略深的版本                 |
| 主要色（浅色） | --ion-color-primary-tint    | 主要颜色略浅的版本                 |
| 次要色         | --ion-color-secondary       | 应用的次要颜色                     |
| 次要色（阴影） | --ion-color-secondary-shade | 次要颜色略深的版本                 |
| 次要色（浅色） | --ion-color-secondary-tint  | 次要颜色略浅的版本                 |
| 三次色         | --ion-color-tertiary        | 应用的第三颜色                     |
| 三次色（阴影） | --ion-color-tertiary-shade  | 三次色略深的版本                   |
| 三次色（浅色） | --ion-color-tertiary-tint   | 三次色略浅的版本                   |
| 成功色         | --ion-color-success         | 成功提示颜色                       |
| 成功色（阴影） | --ion-color-success-shade   | 成功色略深的版本                   |
| 成功色（浅色） | --ion-color-success-tint    | 成功色略浅的版本                   |
| 警告色         | --ion-color-warning         | 警告提示颜色                       |
| 警告色（阴影） | --ion-color-warning-shade   | 警告色略深的版本                   |
| 警告色（浅色） | --ion-color-warning-tint    | 警告色略浅的版本                   |
| 危险色         | --ion-color-danger          | 错误提示颜色                       |
| 危险色（阴影） | --ion-color-danger-shade    | 危险色略深的版本                   |
| 危险色（浅色） | --ion-color-danger-tint     | 危险色略浅的版本                   |
| 暗色           | --ion-color-dark            | 与亮色对立的颜色，如背景色与字体色 |
| 暗色（阴影）   | --ion-color-dark-shade      | 暗色略深的版本                     |
| 暗色（浅色）   | --ion-color-dark-tint       | 暗色略浅的版本                     |
| 中间色         | --ion-color-medium          | 暗色与亮色的中间色                 |
| 中间色（阴影） | --ion-color-medium-shade    | 中间色略深的版本                   |
| 中间色（浅色） | --ion-color-medium-tint     | 中间色略浅的版本                   |
| 亮色           | --ion-color-light           | 与暗色对立的颜色，如背景色与字体色 |
| 亮色（阴影）   | --ion-color-light-shade     | 亮色略深的版本                     |
| 亮色（浅色）   | --ion-color-light-tint      | 亮色略浅的版本                     |

#### 分页栏

首页底部菜单栏的样式。

| <div style="width: 213px;text-align: left;">名称</div>|<div style="width: 213px;text-align: left;">变量</div>|<div style="width: 213px;text-align: left;">说明</div>|
| -------------- | -------------------------------- | ---------------- |
| 背景色         | --ion-tab-bar-background         | 分页栏默认背景色 |
| 背景色（聚焦） | --ion-tab-bar-background-focused | 分页栏选中背景色 |
| 颜色           | --ion-tab-bar-color              | 分页栏字体色     |
| 选中色         | --ion-tab-bar-color-selected     | 分页栏选中字体色 |

#### 工具栏

系统顶部的背景与字体

| <div style="width: 213px;text-align: left;">名称</div>|<div style="width: 213px;text-align: left;">变量</div> | <div style="width: 213px;text-align: left;">说明</div>|
| ------ | ------------------------ | ------------ |
| 背景色 | --ion-toolbar-background | 工具栏背景色 |
| 颜色   | --ion-toolbar-color      | 工具栏字体色 |

### 添加预置主题

目前系统的预置主题有两种，分别为默认的白色主题与深色主题。预置主题的变量会默认全部添加到系统的样式表中，系统初始化时给跟节点添加类名来使用对应的主题变量。

1. 如果需要添加预置主题需要在packages\ibz-ui-ionic\src\styles\core\themes目录下新建一个文件，在里面添加所有的全局变量颜色值，并且添加类名限制。

```scss
:root.dark {
  /* Ionic Colors */
  --ion-color-primary: #549ee7;
  --ion-color-primary-contrast: #fff;
  --ion-color-primary-contrast-rgb: 255, 255, 255;
  ......
  }
```

​	2.还需要在src\config\theme-config.ts文件中添加对应的预置主题可选项配置，这样才可以在app-theme组件中进行主题切换。

```ts
{
    name: 'default',
    title: '默认',
    lanResTag: 'theme.default',
    color: '#f4f5f8',
  },
  {
    name: 'dark',
    title: '灰暗',
    lanResTag: 'theme.dark',
    color: '#343d46',
  }
```

::: tip
lanResTag为多语言标识，app-theme组件中用于切换主题的可选项也需支持多语言，所以添加了预置主题也需添加对应的多语言。
:::
