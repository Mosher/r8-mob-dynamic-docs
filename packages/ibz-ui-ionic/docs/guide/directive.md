# 指令
## 简介 
R8移动端组件库支持vue自定义指令，现支持的自定义指令如下：

| <div style="width: 155px;text-align: left;">名称</div>       | <div style="width: 150px;text-align: left;">指令</div>     | <div style="width: 150px;text-align: left;">说明</div>                     | <div style="width: 150px;text-align: left;">链接</div> |
| ---------- | -------- | ------------------------ | ---- |
| 徽标   | v-badge  | 给绑定元素添加徽标       |  [徽标](../directive/badge.md)    |
| 格式化 | v-format | 格式化绑定元素的文本内容 |  [格式化](../directive/format.md)    |

