# 简介
R8移动端组件库是iBiz前端团队提供的一套全新的移动端前端UI，ibz-ui-ionic包是基于Vue3和Ionic组件库构建而成的UI包。 该组件库专注于UI层面，主要包含了UI所需的视图、部件、编辑器、指令、样式、UI服务等内容。 

## 目录结构

```
|─ ─ src
	|─ ─ assets                                 静态资源文件
	|─ ─ components                             预置组件
		|─ ─ common                               基础组件，主要包含编辑器组件和其他全局使用的组件
        |─ ─ component-base                       组件基类、统一生成Vue3组件的方法  
        |─ ─ editor                               编辑器组件
        |─ ─ layout                       		  视图布局组件
        |─ ─ view                          		  视图组件
        |─ ─ widget                            	  部件组件
	|─ ─ config                               	主题相关配置文件
    |─ ─ directives                             指令文件，包含徽章和格式化等
    |─ ─ locale                         		多语言文件
	|─ ─ styles                                	样式文件
    |─ ─ ui-service     						界面服务文件
```

## 常见问题