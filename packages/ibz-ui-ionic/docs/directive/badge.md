# 徽标
R8Mob-Dynamic模板支持格式化指令，在元素上绑定v-badge，可以为元素添加徽标，如下所示：

```tsx
<ion-label v-badge={this.badge}> </ion-label>
```

badge配置：

| <div style="width: 155px;text-align: left;">配置项</div> | <div style="width: 150px;text-align: left;">说明</div> | <div style="width: 150px;text-align: left;">类型</div> | <div style="width: 150px;text-align: left;">默认值</div> |
| -------------------------------------------------------- | ------------------------------------------------------ | ------------------------------------------------------ | -------------------------------------------------------- |
| count                                                    | 徽标显示内容                                           | string \| number                                       | —                                                        |
| className                                                | 徽标自定义类名                                         | string                                                 | —                                                        |
| showZero                                                 | 是否显示0                                              | boolean                                                | true                                                     |
| offset                                                   | 徽标自定义位置                                         | [number,number]                                        | —                                                        |

## 逻辑

```ts
  /**
   * 指令初始化
   *
   * @param {HTMLDivElement} el
   * @param {*} binding
   * @param {VNode} vNode
   * @param {VNode} oldVNode
   */
  beforeMount(el: HTMLDivElement, binding: any, vNode: VNode, oldVNode: VNode) {
    bc.init(el, binding);
  },

  /**
   * 指令更新
   *
   * @param {HTMLDivElement} el
   * @param {*} binding
   * @param {VNode} vNode
   * @param {VNode} oldVNode
   */
  updated(el: HTMLDivElement, binding: any, vNode: VNode, oldVNode: VNode) {
    bc.update(el, binding);
  },
```

### 初始化

```ts
  public init(el: HTMLDivElement, binding: any): void {
    const item: any = binding.value;
    if (Object.keys(item).length > 0 && Util.isExistAndNotEmpty(item.count)) {
      if (!item.showZero && item.count == 0) {
        return;
      }
      const badge: HTMLElement = document.createElement('sup');
      badge.innerHTML = String(item.count);
      badge.classList.add('ibiz-badge');
      this.el = badge;
      el.append(badge);
      this.setBadgeclass(item.type);
      this.setBadgeclass(item.size);
      this.setBadgeclass(item.className);
      this.setBadgeOffset(item.offset);
    }
  }
```

### 更新

```ts
  public update(el: HTMLDivElement, binding: any): void {
    const item: any = binding.value;
    if (Object.keys(item).length > 0 && Util.isExistAndNotEmpty(item.count)) {
      if (!item.showZero && item.count == 0) {
        return;
      }
      this.el.innerHTML = String(item.count);
    }
  }
```

