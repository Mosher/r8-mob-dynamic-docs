# 门户部件

门户部件通常是用来呈现门户视图数据的展现，经常被数据看板部件引用。门户部件主要有视图，工具栏，操作栏，HTML，列表，图表，快捷菜单，直接内容，自定义这几种类型。根据类型的不同去加载不同的组件。

<component-iframe router="iframe/widget/app-mob-portlet" />

## 控制器

### 部件初始化

| <div style="width: 213px;text-align: left;">逻辑</div> | <div style="width: 213px;text-align: left;">方法名</div> | <div style="width: 213px;text-align: left;">说明</div> |
| ------------------------------------------------------ | -------------------------------------------------------- | ------------------------------------------------------ |
| 初始化界面行为模型                                     | initActionBarModel                                       | 根据部件模型初始化门户部件界面行为组                   |

#### 初始化界面行为模型

根据部件模型初始化门户部件界面行为组。

```ts
  public initActionBarModel() {
    const groupDetails = this.controlInstance.getPSUIActionGroup?.()?.getPSUIActionGroupDetails?.() || [];
    this.actionBarModel = [];
    groupDetails.forEach((detail: IPSUIActionGroupDetail) => {
      const uiAction = detail.getPSUIAction();
      if (uiAction) {
        this.actionBarModel.push({
          viewLogicName: detail.name,
          icon: uiAction.getPSSysImage?.()?.cssClass,
          name: uiAction.caption,
          language: uiAction.getCapPSLanguageRes()?.lanResTag,
          disabled: false,
          visabled: true,
          noPrivDisplayMode: (uiAction as IPSDEUIAction).noPrivDisplayMode || 6,
        });
      }
    });
  }
```

### 初始化部件

监听通知并执行相应行为。

```ts
  public ctrlInit() {
    super.ctrlInit();
    if (this.viewState) {
      this.viewStateEvent = this.viewState.subscribe(({ tag, action, data }: IViewStateParam) => {
        if (Object.is(tag, 'all-portlet')) {
          switch (action) {
            case MobPortletEvents.LOAD_MODEL:
              this.calcUIActionAuthState(data);
              break;
            case MobPortletEvents.REFRESH_ALL:
              this.refreshAll(data);
              break;
          }
          return;
        }
        if (!Object.is(tag, this.name)) {
          return;
        }
        if (action == MobPortletEvents.LOAD) {
          this.notifyControlLoad(data);
        }
      });
    }
  }
```

::: tip 提示

门户部件控制器继承部件控制器基类，因此此处逻辑只是门户部件特有初始化逻辑。

基类初始化逻辑参见 [部件 > 控制器 > 部件初始化](../app-ctrl-base.md#部件初始化)

:::

### 部件挂载

在门户部件挂载时，如果该门户部件类型为视图，则会等到门户部件下的视图挂载完成之后，才会去挂载该门户部件。

```ts
  /**
   * 处理视图事件
   *
   * @param {string} viewName 视图名称
   * @param {string} action 行为标识
   * @param {*} data 数据
   */
  public handleViewEvent(viewName: string, action: string, data: any) {
    const view = (this.controlInstance as IPSDBAppViewPortletPart).getPortletPSAppView?.();
    if (view && Object.is(view.name, viewName)) {
      switch (action) {
        case AppViewEvents.MOUNTED:
          this.hasViewMounted = true;
          this.ctrlMounted();
          break;
      }
    }
  }

  /**
   * @description 部件挂载
   * @param {*} [args]
   * @memberof MobPortletController
   */
  public ctrlMounted(args?: any) {
    this.hasCtrlMounted = true;
    if (Object.is('VIEW', this.controlInstance.portletType)) {
      if (this.hasViewMounted) {
        this.ctrlEvent(AppCtrlEvents.MOUNTED, this);
        this.hooks.mounted.callSync({ arg: this });
      }
    } else {
      this.ctrlEvent(AppCtrlEvents.MOUNTED, this);
      this.hooks.mounted.callSync({ arg: this });
    }
  }
```

::: tip 提示

门户部件控制器继承部件控制器基类，因此此处逻辑只是门户部件特有挂载逻辑。

基类部件挂载逻辑参见 [部件 > 控制器 > 部件挂载](../app-ctrl-base.md#部件挂载)

:::

### 通知部件加载

门户部件接到数据看板下发的load通知之后，会根据门户部件类型去下发加载通知。

```ts
  /**
   * @description 通知部件加载
   * @param data
   * @memberof MobPortletController
   */
  private notifyControlLoad(args: any) {
    try {
      const portletType = this.controlInstance.portletType;
      switch (portletType) {
        case 'VIEW':
          const view = (this.controlInstance as IPSDBAppViewPortletPart).getPortletPSAppView?.();
          this.viewState.next({ tag: view?.name, action: MobPortletEvents.LOAD, data: Util.deepCopy(args) });
          break;
        case 'CHART':
        case 'LIST':
          const controls = this.controlInstance.getPSControls?.();
          this.viewState.next({ tag: controls?.[0]?.name, action: MobPortletEvents.LOAD, data: Util.deepCopy(args) });
          break;
      }
      return { ret: true, data: args ? [args] : [] };
    } catch (error) {
      return { ret: false, data: args ? [args] : [] };
    }
  }
```

### 刷新

门户部件接到数据看板下发的刷新全部通知之后，会根据门户部件类型去下发刷新通知。

```ts
  public async refresh(
    args?: IParam,
    opts?: IParam,
    showInfo: boolean = this.showBusyIndicator,
    isloadding: boolean = true,
  ): Promise<ICtrlActionResult> {
    try {
      const portletType = this.controlInstance.portletType;
      switch (portletType) {
        case 'VIEW':
          const view = (this.controlInstance as IPSDBAppViewPortletPart).getPortletPSAppView?.();
          this.viewState.next({ tag: view?.name, action: MobPortletEvents.REFRESH, data: Util.deepCopy(args) });
          break;
        case 'CHART':
        case 'LIST':
          const controls = this.controlInstance.getPSControls?.();
          this.viewState.next({ tag: controls?.[0]?.name, action: MobPortletEvents.REFRESH, data: Util.deepCopy(args) });
          break;
        case 'ACTIONBAR':
          this.ctrlEvent(MobPortletEvents.REFRESH_ALL, Util.deepCopy(args));
          break;
      }
      return { ret: true, data: args ? [args] : [] };
    } catch (error) {
      return { ret: false, data: args ? [args] : [] };
    }
  }
```

## UI层

### 绘制

门户部件在绘制的时候会根据门户部件类型分别进行绘制

```tsx
  public renderByPortletType() {
    switch (this.c.controlInstance.portletType) {
      case 'VIEW':
        return this.renderView();
      case 'APPMENU':
        return this.renderAppMenu();
      case 'CUSTOM':
        return this.renderCustom();
      case 'ACTIONBAR':
        return this.renderActionBar();
      case 'TOOLBAR':
        return this.renderToolbar();
      case 'HTML':
        return this.renderHtml();
      case 'RAWITEM':
        return this.renderRawItem();
      default:
        return this.renderControl();
    }
  }
```
### 逻辑

#### 打开动作面板

如果门户部件配置有界面行为组，在点击图标后会通过动作面板服务打开动作面板，点击动作面板项后触发绑定的方法。

```ts
  public openActionSheet(event: any) {
    const buttons: IAppActionSheetButton[] = [];
    this.c.actionBarModel.forEach((model: any) => {
      buttons.push({
        text: model.name,
        icon: model.icon,
        handler: () => {
          this.onActionBarItemClick(model.viewLogicName, event);
        },
      });
    });
    buttons.push({
      text: this.$tl('share.cancel','取消'),
      role: 'cancel',
      handler: () => {},
    });
    const options: IAppActionSheetOptions = { buttons: buttons };
    App.getActionSheetService()
      .create(options)
      .then((result: ICtrlActionResult) => {});
  }
```

::: tip 提示

门户部件继承部件基类，因此此处逻辑只是门户部件的特有ui逻辑。

基类部件ui逻辑参见 [部件 > UI层](../app-ctrl-base.md#ui层)

**参考文件**

packages\ibz-ui-ionic\src\components\widget\app-mob-portlet.tsx

:::
