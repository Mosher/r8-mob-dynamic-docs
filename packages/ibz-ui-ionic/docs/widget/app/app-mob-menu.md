# 应用菜单
应用菜单是管理呈现视图页面的主要入口；通常我们会通过应用功能将视图进行“包装“处理，挂靠至系统的应用菜单当中，用于对应用系统菜单进行结构性布局。同时门户部件的快捷菜单也会走该部件。

<component-iframe router="iframe/widget/app-mob-menu" />

## 控制器

::: tip 提示

应用菜单控制器继承部件控制器基类。

部件控制器基类逻辑参见 [部件 > 控制器](../app-ctrl-base.md#控制器)

:::

## UI层

### 绘制

#### 应用菜单绘制

```tsx
  render() {
  	......
	return (
        <ion-tabs class={{ ...this.classNames }}>
          <ion-router-outlet></ion-router-outlet>
          <ion-tab-bar slot='bottom'>
            {this.menuItems.map((item: IPSAppMenuItem) => {
              return this.renderMenuItem(item);
            })}
          </ion-tab-bar>
        </ion-tabs>
      );
  }
```

#### 快捷菜单绘制

绘制快捷菜单时需要给菜单部件传入部件显示模式ctrlShowMode为'QUICKMENU'，并可配置快捷菜单的部件样式，目前支持列表样式和图标样式，默认显示图表样式。

```tsx
  public renderQuickMenu() {
    switch (this.c.controlInstance.controlStyle?.toLocaleLowerCase()) {
      case 'listview':
        return (
          <app-menu-list
            menu={this.menuItems}
            appUIService={this.c.appUIService}
            modelService={this.c.modelService}
            onMenuClick={(item: IPSAppMenuItem) => this.menuItemClick(item, false)}
          ></app-menu-list>
        );
      default:
        return (
          <app-menu-icon
            menu={this.menuItems}
            appUIService={this.c.appUIService}
            modelService={this.c.modelService}
            onMenuClick={(item: IPSAppMenuItem) => this.menuItemClick(item, false)}
          ></app-menu-icon>
        );
    }
  }
```

### 逻辑

#### 默认展开

在应用菜单中如果菜单项配置有默认展开项，则加载时默认展开该项，否则展开第一项菜单。

```ts
  public setDefaultSelectMenu() {
    if (Object.is(this.c.ctrlShowMode, 'QUICKMENU')) {
      return null;
    }
    let defaultSelectMenu:any = null;
    this.menuItems.map((menu: IPSAppMenuItem) => {
      if (menu.openDefault) {
        defaultSelectMenu = menu;
      }
    });
    if (!defaultSelectMenu) {
      defaultSelectMenu = this.menuItems[0];
    }
    if (defaultSelectMenu) {
      this.menuItemClick(defaultSelectMenu, true);
    }
  }
```

#### 菜单点击

应用菜单中菜单点击时如果该菜单项配置有应用功能，则会通过应用功能服务获取路由，再通过视图打开服务路由打开该视图（应用菜单中只支持路由打开）。

```ts
  public async menuItemClick(item: IPSAppMenuItem, isGlobal: boolean) {
    const func = item.getPSAppFunc();
    if (App.isPreviewMode()) {
      return;
    }
    if (func && func.id) {
      const path: string = await App.getFuncService().executeAppFunction(
        func.id,
        this.c.context,
        this.c.viewParam,
        this,
        isGlobal,
      );
      if (path) {
        App.getOpenViewService().openView(path);
      }
    }
  }
```

::: tip 提示

应用菜单UI继承部件基类，因此此处逻辑只是应用菜单UI特有逻辑。

部件基类UI逻辑参见 [部件 > UI层](../app-ctrl-base.md#ui层)

:::
##### 参考文件：packages\ibz-ui-ionic\src\components\widget\app-mob-menu.tsx