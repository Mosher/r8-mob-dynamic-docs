# 数据看板
数据看板用于展示门户部件，直接内容和快捷菜单。
<component-iframe router="iframe/widget/app-mob-dashboard" />

## 控制器

### 部件初始化

| <div style="width: 213px;text-align: left;">逻辑</div> | <div style="width: 213px;text-align: left;">方法名</div> | <div style="width: 213px;text-align: left;">说明</div> |
| ------------------------------------------------------ | -------------------------------------------------------- | ------------------------------------------------------ |
| 初始化工具服务                                         | initUtilService                                          | 根据模型初始化工具服务                                 |
| 加载门户部件集合                                       | loadPortletList                                          | 如果数据看板支持定制，则会根据模型初始化门户部件集合   |

#### 初始化工具服务

根据模型初始化工具服务。

```ts
  private async initUtilService() {
    const dashboardUtil = this.controlInstance.getPSAppDynaDashboardUtil?.();
    if (dashboardUtil && dashboardUtil.codeName && !App.isPreviewMode()) {
      this.utilServiceName = dashboardUtil.codeName.toLowerCase();
      this.utilService = await App.getUtilService().getService(this.context, dashboardUtil.codeName.toLowerCase());
    }
  }
```

#### 加载门户部件集合

如果数据看板支持定制，则会根据模型初始化门户部件集合。

```ts
  public async loadPortletList() {
    const model = App.getModel();
    const list: any = [];
    if (model?.getAllPSAppPortlets?.()?.length) {
      for (const portlet of model.getAllPSAppPortlets() as IPSAppPortlet[]) {
        // 门户部件实例
        const portletInstance = portlet.getPSControl();
        const temp: any = {
          portletCodeName: portlet.codeName,
          modelData: portletInstance,
        };
        list.push(temp);
      }
    }
    this.portletList = list;
  }
```

### 初始化部件

监听通知并执行相应行为。

```ts
  public ctrlInit() {
    super.ctrlInit();
    if (this.viewState) {
      this.viewStateEvent = this.viewState.subscribe(({ tag, action, data }: IViewStateParam) => {
        if (!Object.is(tag, this.name)) {
          return;
        }
        if (Object.is('load', action)) {
          this.isSearchMode = data?.isSearchMode;
          this.loadModel();
        }
        if (Object.is('loaddata', action)) {
          this.notifyState(data);
        }
      });
    }
  }
```

::: tip 提示

实体数据看板控制器继承部件控制器基类，因此此处逻辑只是实体数据看板特有初始化逻辑。

基类初始化逻辑参见 [部件 > 控制器 > 部件初始化](../app-ctrl-base.md#部件初始化)

:::

### 部件挂载

当所有门户部件挂载完成之后，如果视图没有配置搜索表单，则会直接通知门户部件执行load行为，如果视图配置有搜索表单，则会等待视图下发的loaddata行为通知，然后去通知门户部件加载。

```ts
  /**
   * @description 设置已完成绘制状态
   * @param name 部件名
   * @param data 数据
   * @memberof MobDashboardController
   */
  setIsMounted(name: string = 'self', data?: any) {
    super.setIsMounted(name, data);
    if ([...this.mountedMap.values()].indexOf(false) == -1 && (this.mountedMap.size > 1)) {
      // 执行通知方法
      if (!this.isSearchMode) {
        this.notifyState(this.viewParam);
      }
    }
  }
  /**
   * @description 通知部件加载数据
   * @private
   * @param {*} data 参数
   * @memberof MobDashboardController
   */
  private notifyState(data: any) {
    const controls = this.controlInstance.getPSControls() || [];
    if (this.viewState) {
      controls.forEach((control: IPSControl) => {
        this.viewState.next({
          tag: control.name,
          action: 'load',
          data: Util.deepCopy(data),
        });
      });
    }
  }
```

::: tip 提示

实体数据看板控制器继承部件控制器基类，因此此处逻辑只是实体数据看板特有挂载逻辑。

基类部件挂载逻辑参见 [部件 > 控制器 > 部件挂载](../app-ctrl-base.md#部件挂载)

:::
### 加载模型

如果启用了定制，会去查询定制模型数据。

```ts
  /**
   * @description 加载模型
   * @memberof MobDashboardController
   */
  public async loadModel() {
    try {
      if (this.enableCustomized && this.utilService) {
        const res = await this.utilService.loadModelData(Util.deepCopy(this.context), {
          modelid: this.modelId,
          utilServiceName: this.appDeCodeName?.toLowerCase(),
        });
        if (res && res.status == 200) {
          const data: any = res.data;
          if (data && data.length > 0) {
            this.customDashboardModelData = data;
            for (const model of this.customDashboardModelData) {
              model.modelData = this.getPortletInstance(model);
            }
            this.hasCustomized = true;
          } else {
            App.getNoticeService().error('数据错误');
          }
        } else {
          App.getNoticeService().error('服务异常');
        }
      }
    } catch (error) {
      App.getNoticeService().error('加载面板错误');
      this.hasCustomized = false;
    }
  }
```

### 定制仪表盘

点击定制仪表盘时，会去加载数据模型，模态打开仪表盘控制界面，对仪表盘进行控制。

```ts
  /**
   * @description 处理私人定制按钮
   * @param event 源对象
   * @memberof MobDashboardController
   */
  public async handleCustom(view: IParam, event: any): Promise<ICtrlActionResult> {
    return new Promise((resolve: any, reject: any) => {
      const viewparams: any = {
        modelid: this.modelId,
        utilServiceName: this.utilServiceName,
        appdeNamePath: this.controlInstance?.getPSAppDataEntity()?.codeName || 'app',
      };
      this.loadModel()
        .then(() => {
          const param = {
            customModel: Util.deepCopy(this.customDashboardModelData),
            ...Util.deepCopy(this.context),
            ...viewparams,
          };
          App.getOpenViewService()
            .openModal(view, this.context, param)
            .subscribe((result: IParam) => {
              if (result && Object.is(result.ret, 'OK')) {
                resolve({ ret: true, data: [] });
              } else {
                reject({ ret: false, data: [] });
              }
            });
        })
        .catch((error: any) => {
          reject({ ret: false, data: [] });
        });
    });
  }
```

### 处理部件事件

当门户部件抛出refreshAll刷新全部事件时，给所有门户部件发送一个刷新通知。

```ts
  public handleCtrlEvent(controlname: string, action: string, data: any) {
    if (action == MobDashboardEvents.REFRESH_ALL) {
      this.viewState.next({
        tag: 'all-portlet',
        action: MobDashboardEvents.REFRESH_ALL,
        data: data,
      });
      return;
    }
    super.handleCtrlEvent(controlname, action, data);
  }
```

## UI层

### 绘制

```tsx
  /**
   * @description 渲染
   * @memberof AppMobDashboard
   */
  render() {
    if (!this.controlIsLoaded) {
      return;
    }
    return (
      <ion-grid class={{ ...this.classNames }}>
        {this.c.enableCustomized ? (
          <div
            class='dashboard-customized'
            onClick={(event: MouseEvent) => {
              this.handleCustom(event);
            }}
          >
            <app-icon name='settings-outline' />
          </div>
        ) : null}
        {this.c.hasCustomized ? this.renderCustomizedDashboard() : this.renderStaticDashboard()}
      </ion-grid>
    );
  }
```

::: tip 提示

实体数据看板控制器继承部件控制器基类，因此此处逻辑只是实体数据看板特有ui逻辑。

基类部件ui逻辑参见 [部件 > UI层](../app-ctrl-base.md#ui层)

**参考文件**

packages\ibz-ui-ionic\src\components\widget\app-mob-dashboard.tsx

:::