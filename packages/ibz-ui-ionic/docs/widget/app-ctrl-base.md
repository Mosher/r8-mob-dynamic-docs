# 部件基类
R8Mob动态模板的部件同视图一样，将与UI不相关的逻辑分隔开来，使用控制器完成对业务数据的操作或对用户行为的控制。

## 控制器

### 部件初始化

| 流程               | 方法名             | 说明                                                         |
| ------------------ | ------------------ | ------------------------------------------------------------ |
| 初始化输入数据     | initInputData      | 对部件接收的输入数据进行初始化                               |
| 初始化部件模型     | ctrlModelInit      | 构建部件模型                                                 |
| 部件模型数据加载   | ctrlModelLoad      | 对部件模型进行填充                                           |
| 初始化部件基础数据 | ctrlBasicInit      | 对部件的基础数据进行解析，包括实体行为、loading服务，界面行为模型等 |
| 初始化挂载状态集合 | initMountedMap     | 初始化部件挂载集合，用于判断部件的挂载状态                   |
| 初始化实体服务     | initAppDataService | 根据部件的实体初始化实体服务                                 |
| 初始化UI服务       | initAppUIService   | 根据部件的实体初始化UI服务                                   |
| 初始化计数器服务   | initCounterService | 初始化部件的界面计数器服务                                   |
| 初始化部件逻辑     | initControlLogic   | 初始化部件逻辑，包括实体界面逻辑，预置界面逻辑，扩展插件，脚本代码4种逻辑类型。 |
| 初始化部件         | ctrlInit           | 用于设置视图订阅对象(ViewState)，监听通知完成相应操作        |

### 部件挂载

R8Mob动态模板的部件下也会存在部件(例如导航栏部件下有多数据类部件)，因此需要等待当前部件下的所有部件完成挂载后，当前部件向上抛出挂载完成事件onMounted。

```ts
  /**
   * @description 设置已经绘制完成状态
   * @param {string} [name='self'] 部件类型名
   * @param {*} [data] 数据
   * @memberof AppCtrlControllerBase
   */
  public setIsMounted(name: string = 'self', data?: any) {
    this.mountedMap.set(name, true);
    if (data) {
      this.ctrlRefsMap.set(name, data);
    }
    if ([...this.mountedMap.values()].indexOf(false) == -1) {
      // 执行ctrlMounted
      if (!this.hasCtrlMounted) {
        this.ctrlMounted();
      }
    }
  }

  /**
   * @description 部件挂载
   * @param {*} [args] 挂载参数
   * @memberof AppCtrlControllerBase
   */
  public ctrlMounted(args?: any) {
    this.hasCtrlMounted = true;
    this.ctrlEvent(AppCtrlEvents.MOUNTED, this);
    this.hooks.mounted.callSync({ arg: this })
	......
  }
```
### 计数器刷新

当计数器对象发生变化时(例如切换分页)，部件进行计数器的刷新，重新获取界面计数器数据。

```ts
  public counterRefresh() {
    if (this.counterServiceArray && this.counterServiceArray.length > 0) {
      this.counterServiceArray.forEach((item: any) => {
        const counterService = item.service;
        if (
          counterService &&
          counterService.refreshCounterData &&
          counterService.refreshCounterData instanceof Function
        ) {
          counterService.refreshCounterData(this.context, this.viewParam);
        }
      });
    }
  }
```

### 下拉刷新

下拉刷新时触发该方法调用部件刷新。

```ts
  public async pullDownRefresh(): Promise<IParam> {
    if (this.refresh && this.refresh instanceof Function) {
      return await this.refresh();
    } else {
      return new Promise((resolve: any) => {
        resolve({ ret: false, data: null });
      });
    }
  }
```

### 处理部件界面行为

点击多数据部件的左右滑动行为组，表单分组界面行为，门户部件操作栏等部件上绑定的界面行为时，会触发该方法来执行部件界面行为。

```ts
  public handleActionClick(data: IParam, event: MouseEvent, detail: IParam) {
    const { name } = this.controlInstance;
    const getPSAppViewLogics = this.controlInstance.getPSAppViewLogics();
    const UIDataParam: IUIDataParam = this.getUIDataParam();
    Object.assign(UIDataParam, { data: data, event: event });
    AppViewLogicUtil.executeViewLogic(
      `${name}_${detail.name}_click`,
      UIDataParam,
      this.getUIEnvironmentParam(),
      this.viewCtx,
      getPSAppViewLogics,
    );
  }
```

### 部件销毁

为防止内存溢出，在部件销毁时，R8Mob动态模板会从内存出删除该部件关联的资源，如：取消部件订阅，销毁计数器定时器，销毁部件逻辑定时器等。

```ts
  public async controlDestroy() {
    if (this.viewStateEvent) {
      this.viewStateEvent.unsubscribe();
    }
    // 销毁计数器定时器
    if (this.counterServiceArray && this.counterServiceArray.length > 0) {
      this.counterServiceArray.forEach((item: any) => {
        const counterService = item.service;
        if (counterService && counterService.destroyCounter && counterService.destroyCounter instanceof Function) {
          counterService.destroyCounter();
        }
      });
    }
    // 销毁部件定时器逻辑
    this.destroyLogicTimer();
    if (this.appStateEvent) {
      this.appStateEvent.unsubscribe();
    }
    this.cancelSubscribe();
  }
```


### 初始化部件逻辑

当目标逻辑类型类型logicType为实体界面逻辑、系统预置界面逻辑、前端扩展插件、脚本代码时

如果逻辑触发logicTrigger为TIMER时， 界面触发逻辑Map就添加一个新的界面定时器触发逻辑引擎AppTimerEngine对象；

如果逻辑触发logicTrigger为CTRLEVENT时，界面触发逻辑Map就添加一个新的界面部件事件触发逻辑引擎AppCtrlEventEngine对象；

支持原生界面触发逻辑。

支持绑定用户自定义事件。

```ts

  /**
   * @description 初始化部件逻辑
   * @memberof AppCtrlControllerBase
   */
  public async initControlLogic() {
    if (
      this.controlInstance &&
      this.controlInstance.getPSControlLogics() &&
      this.controlInstance.getPSControlLogics().length > 0
    ) {
      this.realCtrlTriggerLogicArray = [];
      this.controlInstance.getPSControlLogics().forEach((element: any) => {
        // 目标逻辑类型类型为实体界面逻辑、系统预置界面逻辑、前端扩展插件、脚本代码
        if (
          element &&
          element.triggerType &&
          (Object.is(element.logicType, 'DEUILOGIC') ||
            Object.is(element.logicType, 'SYSVIEWLOGIC') ||
            Object.is(element.logicType, 'PFPLUGIN') ||
            Object.is(element.logicType, 'SCRIPT'))
        ) {
          switch (element.triggerType) {
            case 'CUSTOM':
              this.ctrlTriggerLogicMap.set(element.name.toLowerCase(), new AppCustomEngine(element));
              break;
            case 'CTRLEVENT':
              if (element.eventNames.startsWith('__')) {
                // 部件原生控件事件
                this.ctrlTriggerLogicMap.set(element.eventNames.toLowerCase(), new AppCtrlEventEngine(element));
              } else {
                if (element.eventNames.startsWith('_')) {
                  // 部件注册自定义事件
                  this.ctrlTriggerLogicMap.set(element.eventNames.toLowerCase(), new AppCtrlEventEngine(element));
                } else {
                  // 部件内置事件
                  this.ctrlTriggerLogicMap.set(element.eventNames.toLowerCase(), new AppCtrlEventEngine(element));
                }
              }
              break;
            case 'TIMER':
              this.ctrlTriggerLogicMap.set(element.name.toLowerCase(), new AppTimerEngine(element));
              break;
            default:
              LogUtil.log(`${element.triggerType}类型暂未支持`);
              break;
          }
        }
        // 初始化原生界面触发逻辑
        if (element.eventNames && element.eventNames.startsWith('__')) {
          const eventNames = element.eventNames.slice('__'.length);
          this.realCtrlTriggerLogicArray.push(eventNames);
        }
        // 绑定用户自定义事件
        if (element.eventNames && element.eventNames.startsWith('_') && !element.eventNames.startsWith('__')) {
          const eventName = element.eventNames.toLowerCase().slice(1);
          this.on(eventName, (...args: any) => {
            this.handleCtrlCustomEvent(element.eventNames.toLowerCase(), this.getData(), args);
          });
        }
      });
    }
  }
```

## UI层

### 绘制

#### 绘制无数据

部件在加载数据完成之前或后台无返回数据时会进行无数据界面的渲染，内容为用户配置的无数据提示内容。

```tsx
  public renderNoData() {
    return (
      <div class='no-data'>
        {
          this.$tl(
            (this.c.controlInstance as IPSDEMobMDCtrl)?.getEmptyTextPSLanguageRes?.()?.lanResTag, 
            (this.c.controlInstance as IPSDEMobMDCtrl).emptyText
          )
          || this.$tl('share.emptytext', '暂无数据')
        }
      </div>
    )
  }
```

#### 绘制下拉刷新

如果当前部件的视图开启了下拉刷新，同时该部件也支持，则会渲染下拉刷新。当用户下拉界面时，调用控制器的下拉刷新逻辑。

```tsx
  public renderPullDownRefresh() {
    if (this.c.enablePullDownRefresh) {
      return (
        <ion-refresher
          slot='fixed'
          onIonRefresh={(event: any) => {
            this.pullDownRefresh(event);
          }}
        >
          <ion-refresher-content />
        </ion-refresher>
      );
    }
  }
```

### 逻辑

#### 部件初始化

部件初始化时，注册部件钩子，并设置响应式数据。

```ts
  setup() {
    this.controlIsLoaded = toRef(this.c, 'controlIsLoaded');
    this.initReactive();
    this.emitCtrlEvent = this.emitCtrlEvent.bind(this);
    this.c.hooks.event.tap(this.emitCtrlEvent);
    this.emitCloseView = this.emitCloseView.bind(this);
    this.c.hooks.closeView.tap(this.emitCloseView);
    this.ctrlMounted = this.ctrlMounted.bind(this);
    this.c.hooks.mounted.tap(this.ctrlMounted);
  }
```

部件初始化完成之后向上抛出初始化完成事件onInited。

```ts
  init() {
    this.c.controlInit().then((result: boolean) => {
      this.emitCtrlEvent({ controlname: this.c.controlInstance?.name, action: AppCtrlEvents.INITED, data: result });
    });
  }
```

#### 获取控制器

在部件初始化时，会根据部件类型来获取对应的部件控制器。

```ts
  public getCtrlControllerByType(type: string) {
    switch (type) {
      case 'APPMENU':
        return new MobMenuCtrlController(this.props);
      case 'TOOLBAR':
        return new MobToolbarCtrlController(this.props);
      ......
      case 'MAPEXPBAR':
        return new MobMapExpBarController(this.props);
      default:
        console.log(`暂未实现${type}类型部件`);
        return null;
    }
  }
```

#### 输入值变更

当部件监听到输入值变更时，会重新初始化输入数据。

```ts
  watchEffect() {
    this.c.initInputData(this.props);
  }
```

#### 部件销毁

调用控制器的销毁，并删除钩子抛出部件销毁事件onDestroyed。

```ts
  public unmounted(): void {
    this.c.controlDestroy();
    this.c.hooks.event.removeTap(this.emitCtrlEvent);
    this.c.hooks.closeView.removeTap(this.emitCloseView);
    this.c.hooks.mounted.removeTap(this.ctrlMounted);
    this.emitCtrlEvent({ controlname: this.c.controlInstance?.name, action: AppCtrlEvents.DESTROYED, data: true });
  }
```

::: tip 参考文件

packages\ibz-ui-ionic\src\components\widget\ctrl-component-base.tsx

:::
