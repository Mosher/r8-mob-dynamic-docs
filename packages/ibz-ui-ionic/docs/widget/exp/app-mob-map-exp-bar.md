# 实体移动端地图导航栏部件

将经纬度数据展示在地图部件上，点击这些数据会跳转到对应的导航视图上。

<component-iframe router="iframe/widget/app-mob-map-exp-bar" />

## 控制器

### 选中数据变化

重写基类中的选中数据变化方法，这是地图导航栏特有的一块逻辑，当选中地图中数据项时，地图导航部件就将触发该方法。该方法获取点击的数据项，解析视图上下文和视图参数，然后根据导航栏部件的展现模式去打开导航视图。

如果展现模式为默认，则将这些参数传给导航栏基类中的打开视图导航视图方法openExplorerView，以此来打开导航视图。如果展现模式为左右布局，则组装渲染导航视图必要参数，导航栏基类中的绘制导航视图方法监听到这些参数改变重新去绘制导航视图，该展现模式下会默认打开第一个部件项上配置的导航视图。

```ts
  public async onSelectionChange(args: any): Promise<void> {
    let tempContext: any = {};
    let tempViewParam: any = {};
    if (args.length === 0) {
      this.calcToolbarItemState(true);
      return;
    }
    const arg: any = args[0];
    if (this.context) {
      Object.assign(tempContext, Util.deepCopy(this.context));
    }
    const mapItem: IPSSysMapItem | null | undefined = ((this.xDataControl as IPSSysMap).getPSSysMapItems() || []).find((item: IPSSysMapItem) => {
      return item.itemType === arg.itemType;
    });
    const mapItemEntity: IPSAppDataEntity | null | undefined = mapItem?.getPSAppDataEntity();
    if (mapItem && mapItemEntity) {
      Object.assign(tempContext, { [mapItemEntity.codeName?.toLowerCase()]: arg[mapItemEntity.codeName?.toLowerCase()] });
      Object.assign(tempContext, { srfparentdename: mapItemEntity.codeName, srfparentdemapname: (mapItemEntity as any)?.getPSDEName(), srfparentkey: arg[mapItemEntity.codeName?.toLowerCase()] });
      if (this.navFilter && this.navFilter[arg.itemType] && !Object.is(this.navFilter[arg.itemType], "")) {
        Object.assign(tempViewParam, { [this.navFilter[arg.itemType]]: arg[mapItemEntity.codeName?.toLowerCase()] });
      }
      if (this.navPSDer && this.navFilter[arg.itemType] && !Object.is(this.navPSDer[arg.itemType], "")) {
        Object.assign(tempViewParam, { [this.navPSDer[arg.itemType]]: arg[mapItemEntity.codeName?.toLowerCase()] });
      }
      if (this.navParam && this.navParam[arg.itemType] && this.navParam[arg.itemType].navigateContext && Object.keys(this.navParam[arg.itemType].navigateContext).length > 0) {
        let _context: any = Util.computedNavData(arg.curdata, tempContext, tempViewParam, this.navParam[arg.itemType].navigateContext);
        Object.assign(tempContext, _context);
      }
      if (this.navParam && this.navParam[arg.itemType] && this.navParam[arg.itemType].navigateParams && Object.keys(this.navParam[arg.itemType].navigateParams).length > 0) {
        let _params: any = Util.computedNavData(arg.curdata, tempContext, tempViewParam, this.navParam[arg.itemType].navigateParams);
        Object.assign(tempViewParam, _params);
      }
      const navView = mapItem?.getNavPSAppView();
      if (navView) {
        if (this.ctrlShowMode == 'LEFT') {
          if (!navView?.isFill) {
            await navView?.fill(true);
          }          
          this.navView = navView.modelPath;
          this.navViewComponent = App.getComponentService().getViewTypeComponent(navView.viewType, navView.viewStyle, navView.getPSSysPFPlugin?.()?.pluginCode);
          // 组装selection,渲染导航视图必要参数
          this.selection = {};
          Object.assign(this.selection, {
            viewComponent: this.navViewComponent,
            navContext: tempContext,
            navParam: tempViewParam,
            navView: navView,
            viewPath: this.navView
          });
        } else{
          this.openExplorerView(navView, args, tempContext, tempViewParam);
        }
      }
    }
    this.calcToolbarItemState(false);
    this.ctrlEvent(AppExpBarCtrlEvents.SELECT_CHANGE, args);
  }
```

### 处理部件事件

监听数据部件抛出的onSelectionChange事件，触发导航栏部件的onSelectionChange方法。

```ts
  public handleCtrlEvent(controlname: string, action: string, data: any) {
    if (controlname === this.xDataControlName && action == MobMapEvents.SELECT_CHANGE) {
      this.onSelectionChange(data);
    } else {
      super.handleCtrlEvent(controlname, action, data);
    }
  }
```

::: tip 提示

实体移动端地图导航栏部件控制器继承导航栏部件控制器基类，因此此处逻辑只是该部件特有控制器逻辑。

基类控制器逻辑参见 [导航栏部件 > 控制器 > 部件初始化](./exp-ctrl-base.md#部件初始化)

:::

## UI层

### 绘制

地图导航栏目前有两种展现模式，默认的一种为单独一个地图，通过点击地图数据项去打开对应的导航视图，因此渲染时不需要额外再去渲染导航视图，只需要展示地图；另外一种需要配置导航栏控件动态参数MODE=LEFT，这种为左右展现模式，左边为地图部件，右边为导航视图。

```tsx
  /**
   * @description 渲染导航栏部件
   * @return {*}
   * @memberof AppMobChartExpBar
   */
  render() {
    if (!this.controlIsLoaded.value) {
      return null;
    }
    const { width, height } = this.c.controlInstance;
    const controlStyle = {
      width: width ? width + 'px' : '',
      height: height ? height + 'px' : '',
    };
    const expMode = this.c.controlInstance.getPSControlParam()?.ctrlParams?.MODE;
    return (
      <div class={{ 'exp-bar': true, ...this.classNames, 'exp-bar-left':expMode == 'LEFT' }} style={controlStyle}>
        <div class='exp-bar-container'>
          <div class='container__multi_data_ctrl'>{this.renderXDataControl()}</div>
          {expMode == 'LEFT' ? 
          <div class="container__nav_view">
            {this.renderNavView()}
          </div> : null}           
        </div>
      </div>
    );
  }
```

::: tip 提示

实体移动端地图导航栏部件UI层继承导航栏部件UI层基类，因此此处逻辑只是该部件特有UI层逻辑。

基类UI层逻辑参见 [导航栏部件 > UI层](./exp-ctrl-base.md#UI层)

**参考文件**

packages\ibz-ui-ionic\src\components\widget\app-mob-map-exp-bar.tsx

:::