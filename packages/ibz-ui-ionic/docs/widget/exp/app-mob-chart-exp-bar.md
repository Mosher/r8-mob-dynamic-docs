# 实体移动端图表导航栏部件

用于展示图表，并可通过点击图表项导航到对应视图。

<component-iframe router="iframe/widget/app-mob-chart-exp-bar" />

## 控制器

### 选中数据变化

重写基类中的选中数据变化方法，这是图表导航栏特有的一块逻辑， 通过点击图表项改变选中的数据，从图表数据序列集合中找出对应该数据的序列，由该序列的分类属性和数据属性组装导航上下文以及导航参数，并得到该序列上配置的导航视图，然后根据导航栏部件的展现模式去打开导航视图。

如果展现模式为默认，则将这些参数传给导航栏基类中的打开视图导航视图方法openExplorerView，以此来打开导航视图。如果展现模式为左右布局，则组装渲染导航视图必要参数，导航栏基类中的绘制导航视图方法监听到这些参数改变重新去绘制导航视图，该展现模式下会默认打开第一个部件项上配置的导航视图。

```ts
  public async onSelectionChange(args: any): Promise<void> {
    let tempContext: any = {};
    let tempViewParam: any = {};
    if (args.length === 0) {
      this.calcToolbarItemState(true);
      return;
    }
    const arg: any = args[0];
    if (this.context) {
      Object.assign(tempContext, Util.deepCopy(this.context));
    }
    const seriesItem: IPSDEChartSeries | null | undefined = ((this.xDataControl as IPSDEChart).getPSDEChartSerieses() || []).find((item: IPSDEChartSeries) => {
      return item.name.toLowerCase() === arg._chartName.toLowerCase();
    });
    const appDataEntity: IPSAppDataEntity | null = this.xDataControl?.getPSAppDataEntity();
    if (seriesItem && appDataEntity) {
      Object.assign(tempContext, { [appDataEntity.codeName?.toLowerCase()]: arg[(ModelTool.getAppEntityKeyField(appDataEntity) as any)?.codeName?.toLowerCase()] });
      Object.assign(tempContext, { srfparentdename: appDataEntity.codeName, srfparentdemapname: (appDataEntity as any)?.getPSDEName(), srfparentkey: arg[appDataEntity.codeName?.toLowerCase()] });
      //  分类属性
      if (seriesItem.catalogField) {
        Object.assign(tempContext, { [seriesItem.catalogField]: arg[seriesItem.catalogField] });
        Object.assign(tempViewParam, { [seriesItem.catalogField]: arg[seriesItem.catalogField] });
      }
      //  数据属性
      if (seriesItem.valueField) {
        Object.assign(tempContext, { [seriesItem.valueField]: arg[seriesItem.valueField] });
        Object.assign(tempViewParam, { [seriesItem.valueField]: arg[seriesItem.valueField] });
      }
      if (this.navFilter && this.navFilter[arg._chartName] && !Object.is(this.navFilter[arg._chartName], "")) {
        Object.assign(tempViewParam, { [this.navFilter[arg._chartName]]: arg[appDataEntity.codeName?.toLowerCase()] });
      }
      if (this.navPSDer && this.navFilter[arg._chartName] && !Object.is(this.navPSDer[arg._chartName], "")) {
        Object.assign(tempViewParam, { [this.navPSDer[arg._chartName]]: arg[appDataEntity.codeName?.toLowerCase()] });
      }
      if (this.navParam && this.navParam[arg._chartName] && this.navParam[arg._chartName].navigateContext && Object.keys(this.navParam[arg._chartName].navigateContext).length > 0) {
        let _context: any = Util.computedNavData(arg, tempContext, tempViewParam, this.navParam[arg._chartName].navigateContext);
        Object.assign(tempContext, _context);
      }
      if (this.navParam && this.navParam[arg._chartName] && this.navParam[arg._chartName].navigateParams && Object.keys(this.navParam[arg._chartName].navigateParams).length > 0) {
        let _params: any = Util.computedNavData(arg, tempContext, tempViewParam, this.navParam[arg._chartName].navigateParams);
        Object.assign(tempViewParam, _params);
      }
      const navView = seriesItem?.getNavPSAppView();
      if (navView) {
        if (this.ctrlShowMode == 'LEFT') {
          if (!navView?.isFill) {
            await navView?.fill(true);
          }
          this.navView = navView.modelPath;
          this.navViewComponent = App.getComponentService().getViewTypeComponent(navView.viewType, navView.viewStyle, navView.getPSSysPFPlugin?.()?.pluginCode);
          // 组装selection,渲染导航视图必要参数
          this.selection = {};
          Object.assign(this.selection, {
            viewComponent: this.navViewComponent,
            navContext: tempContext,
            navParam: tempViewParam,
            navView: navView,
            viewPath: this.navView
          });
        } else{
          this.openExplorerView(navView, args, tempContext, tempViewParam);
        }
      }
    }
    this.calcToolbarItemState(false);
    this.ctrlEvent(AppExpBarCtrlEvents.SELECT_CHANGE, args);
  }
```

### 处理部件事件

监听数据部件抛出的onSelectionChange事件，触发导航栏部件的onSelectionChange方法。

```ts
  public handleCtrlEvent(controlname: string, action: string, data: any) {
    if (controlname === this.xDataControlName && action == MobChartEvents.SELECT_CHANGE) {
      this.onSelectionChange(data);
    } else {
      super.handleCtrlEvent(controlname, action, data);
    }
  }
```

::: tip 提示

实体移动端图表导航栏部件控制器继承导航栏部件控制器基类，因此此处逻辑只是该部件特有控制器逻辑。

基类控制器逻辑参见 [导航栏部件基类 > 控制器 > 部件初始化](./exp-ctrl-base.md#部件初始化)

:::

## UI层

### 绘制

图表导航栏目前有两种展现模式，默认的一种为单独一个图表，通过点击图表部件项去打开对应的导航视图，因此渲染时不需要额外再去渲染导航视图，只需要展示图表；另外一种需要配置导航栏控件动态参数MODE=LEFT，这种为左右展现模式，左边为图表部件，右边为导航视图。

```tsx
  /**
   * @description 渲染导航栏部件
   * @return {*}
   * @memberof AppMobChartExpBar
   */
  render() {
    if (!this.controlIsLoaded.value) {
      return null;
    }
    const { width, height } = this.c.controlInstance;
    const controlStyle = {
      width: width ? width + 'px' : '',
      height: height ? height + 'px' : '',
    };
    const expMode = this.c.controlInstance.getPSControlParam()?.ctrlParams?.MODE;
    return (
      <div class={{ 'exp-bar': true, ...this.classNames, 'exp-bar-left':expMode == 'LEFT' }} style={controlStyle}>
        <div class='exp-bar-container'>
          <div class='container__multi_data_ctrl'>{this.renderXDataControl()}</div>
          {expMode == 'LEFT' ? 
          <div class="container__nav_view">
            {this.renderNavView()}
          </div> : null}              
        </div>
      </div>
    );
  }
```

::: tip 提示

实体移动端图表导航栏部件UI层继承导航栏部件UI层基类，因此此处逻辑只是该部件特有UI层逻辑。

基类UI层逻辑参见 [导航栏部件基类 > UI层](./exp-ctrl-base.md#UI层)

**参考文件**

packages\ibz-ui-ionic\src\components\widget\app-mob-chart-exp-bar.tsx
:::