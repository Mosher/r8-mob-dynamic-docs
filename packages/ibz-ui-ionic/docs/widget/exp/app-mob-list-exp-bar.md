# 实体移动端列表导航栏部件

左侧以列表的形式进行展示，点击某一列表项会将数据传递给右侧的导航视图上。

<component-iframe router="iframe/widget/app-mob-list-exp-bar" />

## 控制器

### 处理多数据部件配置

列表导航重写了基类的handleXDataOptions方法，用来解析导航视图相关参数配置。

```ts
protected async handleXDataOptions() {
  await super.handleXDataOptions();
  const navPSAppView: IPSAppView = await this.xDataControl?.getNavPSAppView()?.fill() as IPSAppView;
  if (navPSAppView) {
    this.navView = navPSAppView.modelFilePath;
    this.navViewComponent = App.getComponentService().getViewTypeComponent(navPSAppView.viewType, navPSAppView.viewStyle, navPSAppView.getPSSysPFPlugin?.()?.pluginCode);
  }
  this.navFilter = this.xDataControl.navFilter || '';
  const navPSDer = this.xDataControl.getNavPSDER();
  if (navPSDer) {
    this.navPSDer = navPSDer ? "n_" + navPSDer.minorCodeName?.toLowerCase() + "_eq" : "";
  }
}
```

### 处理部件事件

监听数据部件抛出的onRowSelected事件，触发导航栏部件的onSelectionChange方法。

```ts
  public handleCtrlEvent(controlname: string, action: string, data: any) {
    if (controlname === this.xDataControlName && action == MobMDCtrlEvents.ROW_SELECTED) {
      this.onSelectionChange(data);
    } else {
      super.handleCtrlEvent(controlname, action, data);
    }
  }
```

::: tip 提示

实体移动端列表导航栏部件控制器继承导航栏部件控制器基类，因此此处逻辑只是该部件特有控制器逻辑。

基类控制器逻辑参见 [导航栏部件 > 控制器 > 部件初始化](./exp-ctrl-base.md#部件初始化)

:::

## UI层

::: tip 提示

实体移动端地图导航栏部件继承导航栏部件基类，因此此处逻辑只是该部件特有UI层逻辑。

导航栏部件基类UI层逻辑参见 [导航栏部件 > UI层](./exp-ctrl-base.md#UI层)

**参考文件**

packages\ibz-ui-ionic\src\components\widget\app-mob-list-exp-bar.tsx

:::