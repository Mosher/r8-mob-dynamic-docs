# 实体移动端树导航栏部件
用于展示树，并可通过点击节点导航到对应的视图。

<component-iframe router="iframe/widget/app-mob-tree-exp-bar" />

## 控制器

### 选中数据变化

重写基类中的选中数据变化方法，这是树导航栏特有的一块逻辑，当点击树节点时，树导航部件就将触发该方法。该方法获取点击的数据项，计算导航数据，打开配置的导航视图。

如果展现模式为默认，则将这些参数传给导航栏基类中的打开视图导航视图方法openExplorerView，以此来打开导航视图。如果展现模式为左右布局或上下布局，则组装渲染导航视图必要参数，导航栏基类中的绘制导航视图方法监听到这些参数改变重新去绘制导航视图，该展现模式下会默认打开第一个树节点上配置的导航视图。

```ts
  protected async onSelectionChange(uiDataParam: IUIDataParam) {
    const args = uiDataParam?.data || [];
    if (args.length === 0) {
      this.calcToolbarItemState(true);
      return;
    }
    const arg: any = args[0];
    if (!arg.id) {
      return;
    }
    const nodetype = arg.id.split(';')[0];
    const refview: any = await this.getExpItemView({ nodetype: nodetype });
    if (Object.is(this.ctrlShowMode,'TOP') || Object.is(this.ctrlShowMode,'LEFT')) {
      this.navView = refview.viewModelData.modelFilePath;
      this.navViewComponent = App.getComponentService().getViewTypeComponent(
        refview.viewModelData.viewType,
        refview.viewModelData.viewStyle,
        refview.viewModelData.getPSSysPFPlugin?.()?.pluginCode,
      );
      this.fillSelectView(arg)
      Object.assign(this.selection, {
        navView: {showCaptionBar: refview.viewModelData.showCaptionBar},
      });
      this.ctrlEvent(AppExpBarCtrlEvents.SELECT_CHANGE, args);
      return;
    }
    if (!refview) {
      this.calcToolbarItemState(true);
      return;
    }
    let { tempContext, tempViewparam } = this.computeNavParams(arg);
    this.openExplorerView(refview.viewModelData, args, tempContext, tempViewparam)
    this.selection = {};
    this.calcToolbarItemState(false);
  }
```

### 打开导航视图

```ts
  async openExplorerView(explorerView: IPSAppView, args: any, tempContext: any, tempViewParam: any) {
    let deResParameters: any[] = [];
    let parameters: any[] = [];
    if (!explorerView?.isFill) {
      await explorerView?.fill(true);
    }
    if (explorerView?.getPSAppDataEntity()) {
      deResParameters = Util.formatAppDERSPath(tempContext, (explorerView as IPSAppDEView)?.getPSAppDERSPaths());
    }
    if (explorerView?.getPSAppDataEntity()) {
      deResParameters = Util.formatAppDERSPath(tempContext, (explorerView as IPSAppDEView)?.getPSAppDERSPaths());
    }
    if (explorerView?.getPSAppDataEntity()) {
      parameters = [
        {
          pathName: Util.srfpluralize(
            (explorerView.getPSAppDataEntity() as IPSAppDataEntity)?.codeName,
          ).toLowerCase(),
          parameterName: (explorerView.getPSAppDataEntity() as IPSAppDataEntity)?.codeName.toLowerCase(),
        },
        {
          pathName: 'views',
          parameterName: ((explorerView as IPSAppDEView).getPSDEViewCodeName() as string).toLowerCase(),
        },
      ];
    } else {
      parameters = [{ pathName: 'views', parameterName: explorerView?.codeName.toLowerCase() }];
    }
    const routePath = ViewTool.buildUpRoutePath(tempContext, deResParameters, parameters, args, tempViewParam);
    App.getOpenViewService().openView(routePath);
  }
```

### 处理部件事件

监听数据部件抛出的onSelectionChange事件，触发树导航栏部件的onSelectionChange方法。

```ts
  public handleCtrlEvent(controlname: string, action: string, data: any) {
    if (controlname === this.xDataControlName && action == MobMapEvents.SELECT_CHANGE) {
      this.onSelectionChange(data);
    } else {
      super.handleCtrlEvent(controlname, action, data);
    }
  }
```

::: tip 提示

实体移动端树导航栏部件控制器继承导航栏部件控制器基类，因此此处逻辑只是该部件特有控制器逻辑。

基类控制器逻辑参见 [导航栏部件 > 控制器 > 部件初始化](./exp-ctrl-base.md#部件初始化)

:::

## UI层

### 绘制

```tsx
  render() {
    if (!this.controlIsLoaded.value) {
      return null;
    }
    const { width, height } = this.c.controlInstance;
    const controlStyle = {
      width: width ? width + 'px' : '',
      height: height ? height + 'px' : '',
    };
    return (
      <div class={{ 'exp-bar': true, ...this.classNames }} style={controlStyle}>
        {this.renderExpBar()}
      </div>
    );
  }
```

### 绘制导航栏

树导航栏部件目前有三种展现模式，默认的一种为单独一个树，通过点击树节点去打开对应的导航视图，因此渲染时不需要额外再去渲染导航视图，只需要展示树；第二种需要配置导航栏控件动态参数MODE=LEFT，这种为左右展现模式，左边为树部件，右边为导航视图；第三种需要配置导航栏控件动态参数MODE=TOP，这种为上下展现模式，上边为树部件，下边为导航视图。

```ts
  /**
   * @description 绘制导航栏
   * @return {*} 
   * @memberof AppMobTreeExpBar
   */
  public renderExpBar() {
    if (Object.is(this.c.ctrlShowMode, 'TOP')) {
      return (
        <div class={'exp-bar-container exp-bar-top'}>
          <div class='container__multi_data_ctrl'>{this.renderXDataControl()}</div>
          <div class='container__nav_view'>{this.renderNavView()}</div>
        </div>
      );
    } else if (Object.is(this.c.ctrlShowMode, 'LEFT')) {
      return (
        <div class='exp-bar-container exp-bar-left'>
          <div class='container__multi_data_ctrl'>{this.renderXDataControl()}</div>
          <div class='container__nav_view'>{this.renderNavView()}</div>
        </div>
      );
    } else {
      return (
        <div class='exp-bar-container'>
          <div class='container__multi_data_ctrl'>{this.renderXDataControl()}</div>
        </div>
      );
    }
  }
```

::: tip 提示

实体移动端树导航栏部件UI层继承导航栏部件UI层基类，因此此处逻辑只是该部件特有UI层逻辑。

基类UI层逻辑参见 [导航栏部件 > UI层](./exp-ctrl-base.md#UI层)

**参考文件**

packages\ibz-ui-ionic\src\components\widget\app-mob-tree-exp-bar.tsx

:::