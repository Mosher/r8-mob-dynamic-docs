# 导航栏部件基类
所有导航栏部件的公共基类，继承部件基类。
## 控制器

### 部件初始化

| <div style="width: 213px;text-align: left;">逻辑</div> | <div style="width: 213px;text-align: left;">方法名</div> | <div style="width: 213px;text-align: left;">说明</div>   |
| ---------------- | ------------- | ---------------------------------------------- |
| 处理多数据部件配置 | handleXDataOptions | 根据部件模型初始化导航上下文和导航参数 |

#### 处理多数据部件配置

根据部件模型初始化导航上下文与导航参数。

```ts
  protected async handleXDataOptions() {
    //  导航上下文
    const navContext = (this.xDataControl as any).getPSNavigateContexts?.();
    if (navContext) {
      this.navigateContext = Util.formatNavParam(navContext);
    }
    // 导航参数
    const navParams = (this.xDataControl as any).getPSNavigateParams?.();
    if (navParams) {
      this.navigateParams = Util.formatNavParam(navParams);
    }
  }
```

### 初始化部件

监听通知并将通知发送给多数据部件。

```ts
  public ctrlInit(args?: any) {
    super.ctrlInit(args);
    if (this.viewState) {
      this.viewStateEvent = this.viewState.subscribe(({ tag, action, data }: IViewStateParam) => {
        if (!Object.is(tag, this.name)) {
          return;
        }
        if (this.xDataControl) {
          this.viewState.next({ tag: this.xDataControlName, action: action, data: data });
        }
      });
    }
  }
```

### 选中数据变化

```ts
  protected onSelectionChange(uiDataParam: IUIDataParam): void {
    let tempContext: any = {};
    let tempViewParam: any = {};
    const args = uiDataParam?.data || [];
    if (args.length === 0) {
      this.calcToolbarItemState(true);
      return;
    }
    const arg: any = args[0];
    if (this.context) {
      Object.assign(tempContext, Util.deepCopy(this.context));
    }
    if (this.xDataControl) {
      const appDataEntity: IPSAppDataEntity | null = this.xDataControl?.getPSAppDataEntity();
      if (appDataEntity) {
        Object.assign(tempContext, {
          [`${appDataEntity.codeName?.toLowerCase()}`]: arg[appDataEntity.codeName?.toLowerCase()],
        });
        Object.assign(tempContext, {
          srfparentdename: appDataEntity.codeName,
          srfparentdemapname: (appDataEntity as any)?.getPSDEName(),
          srfparentkey: arg[appDataEntity.codeName?.toLowerCase()],
        });
        if (this.navFilter && !Object.is(this.navFilter, '')) {
          Object.assign(tempViewParam, { [this.navFilter]: arg[appDataEntity.codeName?.toLowerCase()] });
        }
        if (this.navPSDer && !Object.is(this.navPSDer, '')) {
          Object.assign(tempViewParam, { [this.navPSDer]: arg[appDataEntity.codeName?.toLowerCase()] });
        }
      }
      if (this.navigateContext && Object.keys(this.navigateContext).length > 0) {
        let _context: any = Util.computedNavData(arg, tempContext, tempViewParam, this.navigateContext);
        Object.assign(tempContext, _context);
      }
      if (this.navigateParams && Object.keys(this.navigateParams).length > 0) {
        let _params: any = Util.computedNavData(arg, tempContext, tempViewParam, this.navigateParams);
        Object.assign(tempViewParam, _params);
      }
      this.selection = {};
      Object.assign(tempContext, { viewpath: this.navView });
      Object.assign(this.selection, {
        viewCompoent: this.navViewComponent,
        navContext: tempContext,
        navParam: tempViewParam,
      });
      this.calcToolbarItemState(false);
      this.ctrlEvent(AppExpBarCtrlEvents.SELECT_CHANGE, args);
    }
  }
```

### 打开导航视图

如果在图表导航和地图导航中选中数据发生变化时，会调用该方法去准备参数，通过视图打开服务去路由打开导航视图。

```ts
  async openExplorerView(explorerView: IPSAppView, args: any, tempContext: any, tempViewParam: any) {
    let deResParameters: any[] = [];
    let parameters: any[] = [];
    if (!explorerView?.isFill) {
      await explorerView?.fill(true);
    }
    const entity: IPSAppDataEntity | null = explorerView?.getPSAppDataEntity();
    if (!entity?.isFill) {
      await entity?.fill(true)
    }
    if (entity) {
      deResParameters = Util.formatAppDERSPath(tempContext, (explorerView as IPSAppDEView)?.getPSAppDERSPaths());
      parameters = [
        {
          pathName: Util.srfpluralize(entity.codeName).toLowerCase(),
          parameterName: entity.codeName.toLowerCase(),
        },
        {
          pathName: 'views',
          parameterName: ((explorerView as IPSAppDEView).getPSDEViewCodeName() as string).toLowerCase(),
        },
      ];
    } else {
      parameters = [{ pathName: 'views', parameterName: explorerView?.codeName.toLowerCase() }];
    }
    const routePath = ViewTool.buildUpRoutePath(tempContext, deResParameters, parameters, args, tempViewParam);
    App.getOpenViewService().openView(routePath);
  }
```

## UI层

### 绘制

#### 绘制导航栏

导航栏左侧为多数据部件，右侧为导航视图。

```tsx
  render() {
	......
    return (
      <div class={{ 'exp-bar': true, ...this.classNames }} style={controlStyle}>
        <div class='exp-bar-container'>
          <div class="container__multi_data_ctrl">
            {this.renderXDataControl()}
          </div>
          <div class="container__nav_view">
            {this.renderNavView()}
          </div>
        </div>
      </div>
    )
  }
```

#### 绘制多数据部件

如果配置了部件动态参数mode=LEFT,则会把这个参数传给多数据部件

```tsx
  /**
   * @description 渲染多数据部件
   * @return {*} 
   * @memberof ExpBarCtrlComponentBase
   */
  public renderXDataControl() {
    if (this.c.xDataControl) {
      const expMode = this.c.controlInstance.getPSControlParam()?.ctrlParams?.MODE || 'DEFAULT';
      const otherParams = { expMode: expMode, isSelectFirstDefault: true, };
      return this.computeTargetCtrlData(this.c.xDataControl, otherParams);
    }
  }

```

#### 绘制导航视图

```tsx
  /**
   * @description 渲染导航视图
   * @return {*} 
   * @memberof ExpBarCtrlComponentBase
   */
  public renderNavView() {
    if (this.c.selection) {
      const { viewComponent, navContext, navParam, navView, viewPath } = this.c.selection;
      const key = navContext?.[this.c.xDataControl.getPSAppDataEntity?.()?.codeName?.toLowerCase() || ''];
      if (!this.navViewUUID) {
        this.navViewUUID = Util.createUUID();
      }
      if (viewComponent) {
        return h(viewComponent, {
          key: this.navViewUUID + key,
          navContext: navContext,
          navParam: navParam,
          viewPath: viewPath,
          viewState: this.c.viewState,
          viewShowMode: 'EMBEDDED',        
        })
      }
    }
  }
```

::: tip 提示

导航栏部件基类继承部件基类，更多逻辑可参见 [部件基类](../app-ctrl-base.md)

**参考文件**

packages\ibz-ui-ionic\src\components\widget\exp-bar-ctrl-component-base.tsx

:::