# 实体移动端日历导航栏部件

用于展示日历，并可通过点击日历项导航到对应视图。

<component-iframe router="iframe/widget/app-mob-calendar-exp-bar" />

## 控制器

### 选中数据变化

重写基类中的选中数据变化方法，这是日历导航栏特有的一块逻辑， 通过点击日历项改变选中的数据，从图表数据序列集合中找出对应该数据的日历项，由日历项组装导航上下文以及导航参数，并得到该日历项上配置的导航视图，然后根据导航栏部件的展现模式去打开导航视图。

如果展现模式为默认，则将这些参数传给导航栏基类中的打开视图导航视图方法openExplorerView，以此来打开导航视图。如果展现模式为左右布局，则组装渲染导航视图必要参数，导航栏基类中的绘制导航视图方法监听到这些参数改变重新去绘制导航视图，该展现模式下会默认打开第一个部件项上配置的导航视图。

```ts
  public async onSelectionChange(args: any): Promise<void> {
    let tempContext: any = {};
    let tempViewParam: any = {};
    if (args.length === 0) {
      this.calcToolbarItemState(true);
      return;
    }
    const arg: any = args[0];
    if (this.context) {
      Object.assign(tempContext, Util.deepCopy(this.context));
    }
    const calendarItem: IPSSysCalendarItem | null | undefined = ((this.xDataControl as IPSSysCalendar).getPSSysCalendarItems() || []).find((item: IPSSysCalendarItem) => {
      return item.itemType === arg.itemType;
    });
    const calendarItemEntity: IPSAppDataEntity | null | undefined = calendarItem?.getPSAppDataEntity();
    if (calendarItem && calendarItemEntity) {
      Object.assign(tempContext, { [calendarItemEntity.codeName?.toLowerCase()]: arg[calendarItemEntity.codeName?.toLowerCase()] });
      Object.assign(tempContext, { srfparentdename: calendarItemEntity.codeName, srfparentdemapname: (calendarItemEntity as any)?.getPSDEName(), srfparentkey: arg[calendarItemEntity.codeName?.toLowerCase()] });
      if (this.navFilter && this.navFilter[arg.itemType] && !Object.is(this.navFilter[arg.itemType], "")) {
        Object.assign(tempViewParam, { [this.navFilter[arg.itemType]]: arg[calendarItemEntity.codeName?.toLowerCase()] });
      }
      if (this.navPSDer && this.navFilter[arg.itemType] && !Object.is(this.navPSDer[arg.itemType], "")) {
        Object.assign(tempViewParam, { [this.navPSDer[arg.itemType]]: arg[calendarItemEntity.codeName?.toLowerCase()] });
      }
      if (this.navParam && this.navParam[arg.itemType] && this.navParam[arg.itemType].navigateContext && Object.keys(this.navParam[arg.itemType].navigateContext).length > 0) {
        let _context: any = Util.computedNavData(arg.curData, tempContext, tempViewParam, this.navParam[arg.itemType].navigateContext);
        Object.assign(tempContext, _context);
      }
      if (this.navParam && this.navParam[arg.itemType] && this.navParam[arg.itemType].navigateParams && Object.keys(this.navParam[arg.itemType].navigateParams).length > 0) {
        let _params: any = Util.computedNavData(arg.curData, tempContext, tempViewParam, this.navParam[arg.itemType].navigateParams);
        Object.assign(tempViewParam, _params);
      }
      // 通过视图类型和样式获取视图组件
      const navView:any = calendarItem?.getNavPSAppView();
      if (navView) {
        if (this.ctrlShowMode == 'LEFT') {
          if (!navView?.isFill) {
            await navView?.fill(true);
          }          
          this.navView = navView.modelPath;
          this.navViewComponent = App.getComponentService().getViewTypeComponent(navView.viewType, navView.viewStyle, navView.getPSSysPFPlugin?.()?.pluginCode);
          // 组装selection,渲染导航视图必要参数
          this.selection = {};
          Object.assign(this.selection, {
            viewComponent: this.navViewComponent,
            navContext: tempContext,
            navParam: tempViewParam,
            navView: navView,
            viewPath: this.navView
          });
        } else{
          this.openExplorerView(navView, args, tempContext, tempViewParam);
        }
      }
    }
    this.calcToolbarItemState(false);
    this.ctrlEvent(AppExpBarCtrlEvents.SELECT_CHANGE, args);
  }
```

### 处理部件事件

监听数据部件抛出的onSelectionChange事件，触发导航栏部件的onSelectionChange方法。

```ts
  public handleCtrlEvent(controlname: string, action: string, data: any) {
    if (controlname === this.xDataControlName && action == MobChartEvents.SELECT_CHANGE) {
      this.onSelectionChange(data);
    } else {
      super.handleCtrlEvent(controlname, action, data);
    }
  }
```

### 处理多数据部件配置

获取日历项上配置的导航视图、导航参数、导航关系、导航过滤项等参数。

```ts
  /**
   * @description 处理多数据部件配置
   * @protected
   * @memberof MobCalendarExpBarController
   */
  protected async handleXDataOptions() {
    await super.handleXDataOptions();
    const calendarItems = (this.xDataControl as any).getPSSysCalendarItems();
    let navViewName = {};
    let navParam = {};
    let navFilter = {};
    let navPSDer = {};
    if (calendarItems && calendarItems.length > 0) {
        calendarItems.forEach((item: IPSSysCalendarItem) => {
            const viewName = {
                [item.itemType]: item.getNavPSAppView() ? item.getNavPSAppView()?.modelPath : "",
            };
            Object.assign(navViewName, viewName);
            const param = {
                [item.itemType]: {
                    navigateContext: this.initNavParam(item.getPSNavigateContexts()),
                    navigateParams: this.initNavParam(item.getPSNavigateParams()),
                }
            }
            Object.assign(navParam, param);
            const filter = {
                [item.itemType]: item.navFilter ? item.navFilter : "",
            }
            Object.assign(navFilter, filter);
            const psDer = {
                [item.itemType]: item.getNavPSDER() ? "n_" + (item.getNavPSDER() as IPSDERBase).minorCodeName?.toLowerCase() + "_eq" : "",
            }
            Object.assign(navPSDer, psDer);
        })
    }
    this.navViewName = navViewName;
    this.navParam = navParam;
    this.navFilter = navFilter;
    this.navPSDer = navPSDer;
  }
```

::: tip 提示

实体移动端日历导航栏部件控制器继承导航栏部件控制器基类，因此此处逻辑只是该部件特有控制器逻辑。

基类控制器逻辑参见 [导航栏部件基类 > 控制器 > 部件初始化](./exp-ctrl-base.md#部件初始化)

:::

## UI层

### 绘制

日历导航栏目前有两种展现模式，默认的一种为单独一个日历，通过点击日历下方事件项去打开对应的导航视图，因此渲染时不需要额外再去渲染导航视图，只需要展示日历；另外一种需要配置导航栏控件动态参数MODE=LEFT，这种为左右展现模式，左边为日历部件，右边为导航视图。

```tsx
  /**
   * @description 渲染导航栏部件
   * @return {*}
   * @memberof AppMobCalendarExpBar
   */
  render() {
    if (!this.controlIsLoaded.value) {
      return null;
    }
    const { width, height } = this.c.controlInstance;
    const controlStyle = {
      width: width ? width + 'px' : '',
      height: height ? height + 'px' : '',
    };
    const expMode = this.c.controlInstance.getPSControlParam()?.ctrlParams?.MODE;
    return (
      <div class={{ 'exp-bar': true, ...this.classNames, 'exp-bar-left':expMode == 'LEFT' }} style={controlStyle}>
        <div class='exp-bar-container'>
          <div class='container__multi_data_ctrl'>{this.renderXDataControl()}</div>
          {expMode == 'LEFT' ? 
          <div class="container__nav_view">
            {this.renderNavView()}
          </div> : null}              
        </div>
      </div>
    );
  }
```

::: tip 提示

实体移动端日历导航栏部件UI层继承导航栏部件UI层基类，因此此处逻辑只是该部件特有UI层逻辑。

基类UI层逻辑参见 [导航栏部件基类 > UI层](./exp-ctrl-base.md#UI层)

**参考文件**

packages\ibz-ui-ionic\src\components\widget\app-mob-calendar-exp-bar.tsx
:::