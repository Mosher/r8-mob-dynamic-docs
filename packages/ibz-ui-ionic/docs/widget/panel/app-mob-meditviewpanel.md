# 实体移动端多表单编辑视图面板部件

多编辑视图面板部件通常用来呈现业务单据，简单多编辑视图面板可以完成数据库行数或1:1数据展现并且与多编辑视图面板关联的表单1:N的数据展现；多编辑视图面板部件中的面板样式分为：上分页、行记录，上分页是每条数据为一个分页呈现的业务单据，行记录是把有关联的数据全部呈现在页面的业务单据。

<component-iframe router="iframe/widget/app-mob-meditviewpanel" />

## 控制器

### 部件初始化

| 逻辑               | 方法名         | 说明                                                       |
| ------------------ | -------------- | ---------------------------------------------------------- |
| 初始化嵌入视图参数 | initParameters | 解析配置模型上的嵌入视图及其实体，获取关键信息             |
| 加载数据           | load           | 获取配置的每一个数据项上的数据（视图参数、视图上下文等）。 |

#### 初始化嵌入应用视图及实体参数对象

```ts
  /**
   * @description 初始化嵌入应用视图及实体参数对象
   * @memberof MobMEditViewPanelCtrlController
   */
  public async initParameters() {
    const emView = this.controlInstance.getEmbeddedPSAppView() as IPSAppView;
    const emViewEntity = emView?.getPSAppDataEntity();
    if (emView && emViewEntity) {
      this.deResParameters = Util.formatAppDERSPath(this.context, (emView as IPSAppDEView).getPSAppDERSPaths());
      this.parameters = [
        {
          pathName: Util.srfpluralize(emViewEntity.codeName).toLowerCase(),
          parameterName: emViewEntity.codeName?.toLowerCase(),
          srfmajortext: (ModelTool.getAppEntityMajorField(emViewEntity) as IPSAppDEField).codeName?.toLowerCase(),
        },
      ];
    } else {
      this.deResParameters = [];
      this.parameters = [];
    }
  }
```

### 加载数据

部件接到加载通知后执行加载数据，合并查询参数后执行自定义数据加载前逻辑，之后抛出onBeforeLoad事件，之后调用部件服务执行查询。

```ts
  /**
   * @description 加载数据
   * @param {IParam} [opts] 行为参数
   * @param {IParam} [args] 额外行为参数
   * @param {boolean} [showInfo=true] 是否显示提示信息
   * @param {boolean} [loadding=true] 是否显示loadding效果
   * @return {*}  {Promise<ICtrlActionResult>}
   * @memberof MobMEditViewPanelCtrlController
   */
  public async load(
    opts?: IParam,
    args?: IParam,
    showInfo: boolean = true,
    loadding: boolean = true,
  ): Promise<ICtrlActionResult> {
    if (!this.fetchAction) {
      App.getNoticeService().error('未配置 fetchAction 行为');
      return Promise.resolve({ ret: false, data: null });
    }
    const arg: any = {};
    this.items = [];
    Object.assign(arg, { viewParam: Object.assign(opts, this.viewParam) });
    const tempContext: any = Util.deepCopy(this.context);
    const beforeUIDataParam: IUIDataParam = this.getUIDataParam();
    Object.assign(beforeUIDataParam, { action: this.fetchAction, navContext: tempContext, navParam: arg });
    const beforeLoadResult: any = await this.executeCtrlEventLogic(
      MobMEditViewPanelEvents.BEFORE_LOAD,
      beforeUIDataParam,
    );
    if (beforeLoadResult && beforeLoadResult?.hasOwnProperty('srfret') && !beforeLoadResult.srfret) {
      return Promise.resolve({ ret: false, data: null });
    }
    this.ctrlEvent(MobMEditViewPanelEvents.BEFORE_LOAD, beforeUIDataParam);
    this.onControlRequset('load', tempContext, arg, loadding);
    const response = await this.ctrlService.get(this.fetchAction, tempContext, arg, loadding);
    this.onControlResponse('load', response);
    if (response && response.status === 200) {
      const successUIDataParam: IUIDataParam = this.getUIDataParam();
      Object.assign(successUIDataParam, { action: this.fetchAction, navContext: tempContext, navParam: arg });
      const loadSuccessResult: any = await this.executeCtrlEventLogic(
        MobMEditViewPanelEvents.LOAD_SUCCESS,
        successUIDataParam,
      );
      if (loadSuccessResult && loadSuccessResult?.hasOwnProperty('srfret') && !loadSuccessResult.srfret) {
        return Promise.resolve({ ret: false, data: null });
      }
      this.doItems(response.data);
      this.ctrlEvent(MobMEditViewPanelEvents.LOAD_SUCCESS, successUIDataParam);
      return Promise.resolve({ ret: true, data: response.data });
    } else {
      const errorUIDataParam: IUIDataParam = this.getUIDataParam();
      Object.assign(errorUIDataParam, {
        action: this.fetchAction,
        navContext: tempContext,
        navParam: arg,
        data: [response?.data],
      });
      const loadErrorResult: any = await this.executeCtrlEventLogic(
        MobMEditViewPanelEvents.LOAD_ERROR,
        errorUIDataParam,
      );
      if (loadErrorResult && loadErrorResult?.hasOwnProperty('srfret') && !loadErrorResult.srfret) {
        return Promise.resolve({ ret: false, data: null });
      }
      this.ctrlEvent(MobMEditViewPanelEvents.LOAD_ERROR, errorUIDataParam);
      return Promise.resolve({ ret: false, data: null });
    }
  }
```

### 保存数据

通过视图通讯对象监听事件并进行保存操作。

```ts
  /**
   * @description 保存数据
   * @param {IParam} [opts] 行为参数
   * @param {IParam} [args] 额外行为参数
   * @param {boolean} [showInfo=true] 是否显示提示信息
   * @param {boolean} [loadding=true] 是否显示loadding效果
   * @memberof MobMEditViewPanelCtrlController
   */
  public save(opts?: IParam, args?: IParam, showInfo: boolean = true, loadding: boolean = true) {
    this.count = 0;
    if (this.items.length > 0) {
      Object.assign(opts, { showInfo: false });
      const embedViewName = this.controlInstance.getEmbeddedPSAppView()?.codeName;
      this.viewState.next({ tag: embedViewName, action: 'save', data: opts });
    } else {
      this.ctrlEvent(MobMEditViewPanelEvents.DATA_SAVE, this.getUIDataParam());
    }
  }
```

### 新增数据

UI层点击了新增图标后调用该方法，合并查询参数后执行数据新增前逻辑，之后抛出onBeforeLoad事件，之后调用部件服务执行loadDraft查询，请求成功后调用处理数据方法

```ts
  /**
   * @description 新增数据
   * @param {IParam} [opts] 行为参数
   * @param {IParam} [args] 额外行为参数
   * @param {boolean} [showInfo=true] 是否显示提示信息
   * @param {boolean} [loadding=true] 是否显示loadding效果
   * @return {*}  {Promise<ICtrlActionResult>}
   * @memberof MobMEditViewPanelCtrlController
   */
  public async add(
    opts?: IParam,
    args?: IParam,
    showInfo: boolean = true,
    loadding: boolean = true,
  ): Promise<ICtrlActionResult> {
    if (!this.loaddraftAction) {
      App.getNoticeService().error('未配置 loaddraftAction 行为');
      return Promise.resolve({ ret: false, data: null });
    }
    const arg: any = {};
    Object.assign(arg, opts, { viewparams: this.viewParam });
    const tempContext: any = Util.deepCopy(this.context);
    const beforeUIDataParam: IUIDataParam = this.getUIDataParam();
    Object.assign(beforeUIDataParam, { action: this.loaddraftAction, navContext: tempContext, navParam: arg });
    const beforeLoadResult: any = await this.executeCtrlEventLogic(
      MobMEditViewPanelEvents.BEFORE_ADD,
      beforeUIDataParam,
    );
    if (beforeLoadResult && beforeLoadResult?.hasOwnProperty('srfret') && !beforeLoadResult.srfret) {
      return Promise.resolve({ ret: false, data: null });
    }
    this.ctrlEvent(MobMEditViewPanelEvents.BEFORE_ADD, beforeUIDataParam);
    this.onControlRequset('add', tempContext, arg, loadding);
    const response = await this.ctrlService.loadDraft(this.loaddraftAction, tempContext, arg, this.showBusyIndicator);
    this.onControlResponse('add', response);
    if (response && response.status === 200) {
      const successUIDataParam: IUIDataParam = this.getUIDataParam();
      Object.assign(successUIDataParam, { action: this.loaddraftAction, navContext: tempContext, navParam: arg });
      const addSuccessResult: any = await this.executeCtrlEventLogic(
        MobMEditViewPanelEvents.ADD_SUCCESS,
        successUIDataParam,
      );
      if (addSuccessResult && addSuccessResult?.hasOwnProperty('srfret') && !addSuccessResult.srfret) {
        return Promise.resolve({ ret: false, data: null });
      }
      this.doItems([response.data]);
      this.ctrlEvent(MobMEditViewPanelEvents.ADD_SUCCESS, successUIDataParam);
      if (showInfo) {
        App.getNoticeService().success('添加成功！');
      }
      return Promise.resolve({ ret: true, data: [response.data] });
    } else {
      const errorUIDataParam: IUIDataParam = this.getUIDataParam();
      Object.assign(errorUIDataParam, {
        action: this.loaddraftAction,
        navContext: tempContext,
        navParam: arg,
        data: [response?.data],
      });
      const addErrorResult: any = await this.executeCtrlEventLogic(MobMEditViewPanelEvents.ADD_ERROR, errorUIDataParam);
      if (addErrorResult && addErrorResult?.hasOwnProperty('srfret') && !addErrorResult.srfret) {
        return Promise.resolve({ ret: false, data: null });
      }
      this.ctrlEvent(MobMEditViewPanelEvents.ADD_ERROR, errorUIDataParam);
      if (showInfo) {
        App.getNoticeService().error('添加失败！');
      }
      return Promise.resolve({ ret: false, data: null });
    }
  }
```

### 处理数据

处理关系应用实体参数，当前视图参数，合并视图参数。

```ts
  /**
   * @description 处理数据
   * @private
   * @param {any[]} datas 数据集
   * @memberof MobMEditViewPanelCtrlController
   */
  private doItems(datas: any[]): void {
    const [{ parameterName }] = this.parameters;
    datas.forEach((arg: any) => {
      const id: string = arg[parameterName] ? arg[parameterName] : Util.createUUID();
      const item: any = { id: id, context: {}, viewParam: {} };
      Object.assign(item.context, ViewTool.getIndexViewParam());
      Object.assign(item.context, this.context);
      // 关系应用实体参数
      this.deResParameters.forEach(({ pathName, parameterName }: { pathName: string; parameterName: string }) => {
        if (this.context[parameterName] && !Object.is(this.context[parameterName], '')) {
          Object.assign(item.context, { [parameterName]: this.context[parameterName] });
        } else if (arg[parameterName] && !Object.is(arg[parameterName], '')) {
          Object.assign(item.context, { [parameterName]: arg[parameterName] });
        }
      });
      // 当前视图参数（应用实体视图）
      this.parameters.forEach(
        ({
          pathName,
          parameterName,
          srfmajortext,
        }: {
          pathName: string;
          parameterName: string;
          srfmajortext: string;
        }) => {
          if (arg[parameterName] && !Object.is(arg[parameterName], '')) {
            Object.assign(item.context, { [parameterName]: arg[parameterName] });
          }
          // 当前页面实体主信息
          if (arg[srfmajortext] && !Object.is(arg[srfmajortext], '')) {
            Object.assign(item, { srfmajortext: arg[srfmajortext] });
          }
        },
      );
      //合并视图参数
      Object.assign(item.viewParam, this.viewParam);
      this.items.push(item);
    });
  }
```

::: tip 提示

实体移动端多表单编辑视图面板部件继承多数据部件基类，因此此处逻辑只是该部件特有ui逻辑。

ui逻辑参见 [多数据部件 > 控制器](../md/md-ctrl-base.md#控制器)

**参考文件**

packages\ibz-core\src\ui-core\controllers\widget\mob-meditviewpanel-ctrl-controller.ts

:::

## UI层

### 渲染多表单编辑面板

多编辑视图面板部件中的面板样式分为：上分页、行记录，上分页是每条数据为一个分页呈现的业务单据，行记录是把有关联的数据全部呈现在页面的业务单据。根据不同的样式绘制不同的UI。

```ts
  /**
   * @description 绘制多表单编辑面板
   * @return {*}
   * @memberof AppMobMEditViewPanel
   */
  render() {
    if (!this.controlIsLoaded.value) {
      return null;
    }
    const { width, height } = this.c.controlInstance;
    const controlstyle = {
      width: width ? `${width}px` : '100%',
      height: height ? `${height}px` : '100%',
    };
    return (
      <div class={{ ...this.classNames }} style={controlstyle}>
        {this.renderPullDownRefresh()}
        {this.renderContent()}
        <div class='meditviewpanel-add-icon'>
          <app-icon onClick={() => this.c.add()} name='add'></app-icon>
        </div>
      </div>
    );
  }
  
  /**
   * @description 绘制内容区
   * @return {*}
   * @memberof AppMobMEditViewPanel
   */
  renderContent() {
    if (this.c.items?.length > 0) {
      if (Object.is('ROW', this.c.panelStyle)) {
        return this.renderRow();
      } else {
        return this.renderTabtop();
      }
    }
  }  
```

#### 绘制行记录样式

遍历数据去渲染相应的嵌入视图。

```ts
  /**
   * @description 绘制行记录样式
   * @return {*}
   * @memberof AppMobMEditViewPanel
   */
  renderRow() {
    return this.c.items.map((item: any) => {
      return [
        <div class='multieditviewpanel-row'>
          <ion-card>
            <ion-card-content>{this.renderEmbedView(item)}</ion-card-content>
          </ion-card>
        </div>,
      ];
    });
  }
```

#### 绘制上分页样式

渲染分页头和内容主体。

```ts
  /**
   * @description 绘制上分页样式
   * @return {*}
   * @memberof AppMobMEditViewPanel
   */
  renderTabtop() {
    return (
      <div class='multieditviewpanel-tabtop'>
        {this.renderPageHead()}
        {this.renderTabTopContent()}
      </div>
    );
  }
  
  /**
   * @description 绘制分页头
   * @return {*}
   * @memberof AppMobMEditViewPanel
   */
  renderPageHead() {
    if (!this.activeItem.value) {
      this.activeItem.value = this.c.items[0].id;
    }
    return (
      <ion-segment
        scrollable={true}
        value={this.activeItem.value}
        onIonChange={($event: any) => this.ionChange($event)}
      >
        {this.c.items.map((item: any) => {
          return (
            <ion-segment-button value={item.id}>
              <ion-label>{item.srfmajortext}</ion-label>
            </ion-segment-button>
          );
        })}
      </ion-segment>
    );
  }  
  
  /**
   * @description 绘制上分页内容区
   * @return {*}
   * @memberof AppMobMEditViewPanel
   */
  renderTabTopContent() {
    return (
      <div class='tabtop-content'>
        {this.c.items.map((item: any) => {
          if (item.id == this.activeItem.value) {
            return this.renderEmbedView(item);
          }
        })}
      </div>
    );
  }
```

#### 绘制嵌入视图

```ts
  /**
   * @description 绘制嵌入视图
   * @param {*} item 项数据
   * @return {*}
   * @memberof AppMobMEditViewPanel
   */
  renderEmbedView(item: any) {
    const embedView = this.c.controlInstance.getEmbeddedPSAppView() as IPSAppDEView;
    const viewComponent = App.getComponentService().getViewTypeComponent(
      embedView.viewType,
      embedView.viewStyle,
      embedView.getPSSysPFPlugin()?.pluginCode,
    );
    if (viewComponent) {
      return h(viewComponent, {
        key: item.id,
        viewPath: embedView.modelPath,
        navContext: item.context,
        navParam: item.viewParam,
        navDatas: this.c.navDatas,
        viewState: this.c.viewState,
        isShowCaptionBar: false,
        viewShowMode: 'EMBEDDED',
        onViewEvent: ({ viewname, action, data }: { viewname: string; action: string; data: any }) => {
          this.handleViewEvent(viewname, action, data);
        },
      });
    } else {
      return <div class='no-embedded-view'>{this.$tl('widget.mobmeditviewpanel.noembeddedview','无嵌入视图')}</div>;
    }
  }
```

### 分页节点切换

分页头点击后调用该方法，改变当前激活项。

```ts
  /**
   * @description 分页节点切换
   * @private
   * @param {*} $event 数据源
   * @return {*}
   * @memberof AppMobMEditViewPanel
   */
  private ionChange($event: any) {
    const { detail: _detail } = $event;
    if (!_detail) {
      return;
    }
    const { value: _value } = _detail;
    if (_value) {
      this.activeItem.value = _value;
    }
  }
```

::: tip 提示

实体移动端多表单编辑视图面板部件继承多数据部件基类，因此此处逻辑只是该部件特有ui逻辑。

ui逻辑参见 [多数据部件 > UI层](../md/md-ctrl-base.md#ui层)

**参考文件**

packages\ibz-ui-ionic\src\components\widget\app-mob-meditviewpanel.tsx

:::