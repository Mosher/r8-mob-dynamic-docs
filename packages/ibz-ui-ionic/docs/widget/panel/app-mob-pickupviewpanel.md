# 实体移动端选择视图面板部件

选择视图面板主要是用来呈现实体选择视图，选择视图面板用于实体数据多项选择视图、实体数据选择视图。


<component-iframe router="iframe/widget/app-mob-pickupviewpanel" />

## 控制器

### 数据多选

通过获取传递的输入参数判断是否为多选，然后打开配置的选择视图时也将该参数传给选择视图。

```ts
/**
   * @description 初始化输入参数
   * @param {*} [opts] 输入参数
   * @memberof MobPickUpViewPanelCtrlController
   */
  public initInputData(opts?: any) {
    super.initInputData(opts);
    this.isMultiple = opts.isMultiple;
  }
```

### 数据改变

当选中选择视图中的某一项数据或多项数据后，选择视图向选择视图面板抛出ViewEvent事件，视图面板再向上抛出ctrlEvent事件。

```ts
/**
   * @description 处理选择视图事件
   * @param {string} action 视图行为行为
   * @param {*} data 数据
   * @memberof MobPickUpViewPanelCtrlController
   */
  public handlePickUPViewEvent(action: string, data: IParam) {
    switch (action) {
      case AppViewEvents.DATA_CHANGE:
        this.ctrlEvent(MobPickupViewPanelEvents.SELECT_CHANGE, data);
        break;
      default:
        console.log(`${action}暂未识别`);
    }
  }
```

::: tip 提示

选择视图面板控制器继承部件控制器基类，因此此处逻辑只是选择视图面板特有初始化逻辑。

基类初始化逻辑参见 [部件基类 > 控制器 > 部件初始化](../app-ctrl-base.md#部件初始化)

**参考文件**

packages\ibz-core\src\ui-core\controllers\widget\mob-pickupviewpanel-ctrl-controller.ts

:::

## ui层

选择视图面板负责绘制选择视图，当选中选择视图中的数据时将其抛出到父组件上。

### 绘制选择视图面板

选择视图面板是一个中间件类型的部件，它负责解析配置好的选择视图数据并绘制它。

```tsx
/**
   * @description 绘制选择视图面板
   * @return {*}
   * @memberof AppMobPickUpViewPanel
   */
  renderPickUpViewPanel() {
    const embeddedView = this.c.controlInstance.getEmbeddedPSAppDEView() as IPSAppDEView;
    const viewComponent = App.getComponentService().getViewTypeComponent(
      embeddedView.viewType,
      embeddedView.viewStyle,
      embeddedView.getPSSysPFPlugin()?.pluginCode,
    );
    if (viewComponent) {
      return h(viewComponent, {
        viewPath: embeddedView.modelPath,
        navContext: this.c.context,
        navParam: this.c.viewParam,
        navDatas: this.c.navDatas,
        isMultiple: this.c.isMultiple,
        viewState: this.c.viewState,
        isShowCaptionBar: false,
        viewShowMode: 'EMBEDDED',
        onViewEvent: ({ viewname, action, data }: { viewname: string; action: string; data: any }) => {
          this.c.handlePickUPViewEvent(action, data);
        },
      });
    } else {
      return <div class='no-embedded-view'>{this.$tl('widget.mobpickupviewpanel.nopickerview','无选择视图')}</div>;
    }
  }
```

::: tip 提示

选择视图面板部件继承部件组件基类，因此此处逻辑只是该部件特有ui逻辑。

ui逻辑参见 [部件基类 > UI层](../app-ctrl-base.md#ui层)

**参考文件**

packages\ibz-ui-ionic\src\components\widget\app-mob-pickupviewpanel.tsx

:::
