# 实体移动端向导面板部件

向导面板部件是用于联接实体向导功能以及实体向导视图，为实体向导视图提供具体的向导内容。主要控制向导逻辑和呈现对应的向导表单的内容。

<component-iframe router="iframe/widget/app-mob-wizard-panel" />

## 控制器

### 部件初始化

| 逻辑                     | 方法名           | 说明                                                         |
| ------------------------ | ---------------- | ------------------------------------------------------------ |
| 初始化激活表单           | initActiveForm         | 根据向导模型初始化最初激活的表单                      |
| 注册表单步骤行为 | regFormActions | 获取模型中的步骤行为，并将每个步骤行为添加到相应的步骤表单上。 |
| 面板初始化 | doInit | 调用部件服务的init方法，获取到相应数据后加载对应的步骤表单 |

### 初始化激活表单

部件初始化时将模型中的第一个向导表单设置为激活表单。

```ts
/**
   * @description 初始化激活表单
   * @protected
   * @memberof MobWizardPanelController
   */
  protected initActiveForm() {
    const wizard: IPSDEWizard | null = this.controlInstance.getPSDEWizard();
    const wizardForms: Array<IPSDEWizardForm> = wizard?.getPSDEWizardForms() || [];
    if (wizard && wizardForms.length > 0) {
      const firstForm: IPSDEWizardForm = wizardForms.find((form: IPSDEWizardForm) => {
        return form.firstForm;
      }) as IPSDEWizardForm;
      this.activeForm = `${this.controlInstance.name}_form_${firstForm.formTag?.toLowerCase()}`;
    }
  }
```

### 注册表单步骤行为

将每一个步骤表单中的步骤行为添加到stepActions对象中，并以步骤表单的名称作为键名。

```tsx
/**
   * @description 注册表单步骤行为
   * @protected
   * @memberof MobWizardPanelController
   */
  protected regFormActions() {
    const wizard: IPSDEWizard | null = this.controlInstance.getPSDEWizard();
    const wizardForms: Array<IPSDEWizardForm> = wizard?.getPSDEWizardForms() || [];
    const wizardSteps: Array<IPSDEWizardStep> = wizard?.getPSDEWizardSteps() || [];
    if (wizard && wizardForms.length > 0) {
      wizardForms.forEach((stepForm: IPSDEWizardForm) => {
        const formName = `${this.controlInstance.name}_form_${stepForm.formTag?.toLowerCase()}`;
        const editForm: IPSDEWizardEditForm = (this.controlInstance.getPSDEEditForms() || []).find(
          (form: IPSDEEditForm) => {
            return form.name === formName;
          },
        ) as IPSDEWizardEditForm;
        const action = {
          loadAction: editForm?.getGetPSControlAction()?.actionName || 'Get',
          preAction: editForm?.getGoBackPSControlAction()?.actionName,
          saveAction: editForm?.getUpdatePSControlAction()?.actionName || 'Update',
          actions: stepForm.getStepActions() || [],
        };
        this.regFormAction(
          formName,
          action,
          this.getStepTag(wizardSteps, stepForm?.getPSDEWizardStep()?.stepTag as string),
        );
      });
    }
    if (wizardSteps.length > 0) {
      wizardSteps.forEach((step: IPSDEWizardStep) => {
        this.steps.push({
          tag: step.stepTag,
          title: step.title,
          subTitle: step.subTitle,
          titleLanResTag: step.getTitlePSLanguageRes()?.lanResTag,
          subTitleLanResTag: step.getSubTitlePSLanguageRes()?.lanResTag,
          className: step.getTitlePSSysCss?.()?.cssName,
          icon: step.getPSSysImage?.()?.cssClass
        })
      });
    }
  }
```

### 面板初始化
根据上下文以及视图参数调用向导面板部件服务获取向导表单参数，再根据向导表单参数加载指定向导表单。详细逻辑如下：

```ts
  /**
   * @description 执行初始化
   * @protected
   * @param {*} data 数据
   * @memberof MobWizardPanelController
   */
  protected doInit(opt: IParam = {}) {
    const arg: any = { ...opt };
    Object.assign(arg, { viewParam: this.viewParam });
    let tempContext: any = JSON.parse(JSON.stringify(this.context));
    this.onControlRequset('doInit', tempContext, arg);
    const post: Promise<any> = this.ctrlService.init(this.initAction, tempContext, arg, this.showBusyIndicator);
    post.then((response: any) => {
      this.onControlResponse('doInit', response);
      if (response && response.status === 200) {
        this.formParam = response.data;
        if (response.data[this.appDeCodeName.toLowerCase()]) {
          Object.assign(this.context, {
            [this.appDeCodeName.toLowerCase()]: response.data[this.appDeCodeName.toLowerCase()],
          });
        }
        this.formLoad();
      }
    })
    .catch((response: any) => {
      this.onControlResponse('doInit', response);
      App.getNoticeService().error(response.message);
    });
  }
```

#### 表单加载

若向导面板存在状态属性，则计算当前激活表单；同时通过视图通讯对象通知该表单执行相应逻辑。详细逻辑如下：

```ts
  /**
   * @description 表单加载
   * @return {*}
   * @memberof MobWizardPanelController
   */
  public formLoad() {
    if (!this.mountedMap.get(this.activeForm)) {
      this.hasFormLoadBlocked = true;
      return;
    }
    if (this.activeForm) {
      this.wizardState.next({
        tag: this.activeForm,
        action: 'panelAction',
        data: { action: this.stepActions[this.activeForm].loadAction, emitAction: MobWizardPanelEvents.LOAD_SUCCESS, data: this.formParam },
      });
    }
  }
```

#### 表单保存

向导表单保存完成后抛出save事件，向导面板捕获进行处理。

当前状态为NEXT，即下一步时
当有下一个向导表单时，切换当前激活表单并通知表单加载。若没有时，执行向导的完成逻辑。
当前状态为PREV，即上一步时
当有上一个向导表单时，切换当前激活表单并通知表单加载。若没有时，不做任何操作。
当前状态为FINISH，即完成时
执行完成逻辑。

```ts
  /**
   * 向导表单保存完成
   *
   * @param {string} name 向导表单名称
   * @param {*} args 数据
   * @memberof WizardPanelControlBase
   */
  public wizardPanelFormSave(name: string, args: any) {
    Object.assign(this.formParam, args);
    if (Object.is(this.curState, 'NEXT')) {
      this.historyForms.push(name);
      if (this.getNextForm()) {
        this.activeForm = this.getNextForm();
        setTimeout(() => {
          this.formLoad();
        }, 1);
      } else {
        this.doFinish();
      }
    } else if (Object.is(this.curState, 'PREV')) {
      const length = this.historyForms.length;
      if (length > 1) {
        this.activeForm = this.historyForms[length - 1];
        setTimeout(() => {
          this.formLoad();
        }, 1);
        this.historyForms.splice(length - 1, 1);
      }
    } else if (Object.is(this.curState, 'FINISH')) {
      this.doFinish();
    }
  }
```

### 步骤切换逻辑

此部分包含向导面板初始化、上一步、下一步、完成、向导表单加载、向导表单保存逻辑。这些逻辑相辅相成，共同完成向导面板步骤切换的全部逻辑。

### 上一步

当用户点击上一步的时候，修改当前状态为PREV。向导面板会通知当前表单调用上一步操作对应的实体行为，然后重新展示上一个表单并通知其执行加载操作。

```ts
  /**
   * 上一步
   *
   * @memberof WizardPanelControlBase
   */
  public handlePrevious() {
    const length = this.historyForms.length;
    if (length > 1) {
      this.curState = 'PREV';
      if (this.stepActions[this.activeForm].preAction) {
        this.wizardState.next({
          tag: this.activeForm,
          action: 'panelAction',
          data: { action: this.stepActions[this.activeForm].preAction, emitAction: MobWizardPanelEvents.SAVE_SUCCESS, data: this.formParam },
        });
      } else {
        this.activeForm = this.historyForms[length - 1];
        setTimeout(() => {
          this.formLoad();
        }, 1);
        this.historyForms.splice(length - 1, 1);
      }
    }
  }
```

### 下一步

当用户点击下一步的时候，修改当前状态为NEXT。向导面板会通知当前表单调用下一步操作对应的实体行为，然后展示下一个表单并通知其执行加载操作。

```ts
  /**
   * 下一步
   *
   * @memberof WizardPanelControlBase
   */
  public handleNext() {
    const Environment = App.getEnvironment();
    if (Environment && Environment.isPreviewMode) {
      return;
    }
    if (this.activeForm) {
      const form = this.ctrlRefsMap.get(this.activeForm);
      if (form && form.formValidateStatus && form.formValidateStatus instanceof Function) {
        form.formValidateStatus().then((state: boolean) => {
          if (state) {
            this.curState = 'NEXT';
            this.wizardState.next({
              tag: this.activeForm,
              action: 'panelAction',
              data: { action: this.stepActions[this.activeForm].saveAction, emitAction: MobWizardPanelEvents.SAVE_SUCCESS, data: this.formParam },
            });
          } else {
            App.getNoticeService().error('值规则校验异常');
          }
        }).catch((error: any) => {
          App.getNoticeService().error(error);
        })
      }
    }
  }
```

### 完成

当用户点击完成的时候，修改当前状态为FINISH。向导面板会通知当前表单调用下一步操作对应的实体行为，然后在表单的保存回调中调用向导的完成逻辑。
```ts
  /**
   * 完成
   *
   * @memberof WizardPanelControlBase
   */
  public handleFinish() {
    if (this.activeForm) {
      const form = this.ctrlRefsMap.get(this.activeForm);
      if (form && form.formValidateStatus && form.formValidateStatus instanceof Function) {
        form.formValidateStatus().then((state: boolean) => {
          console.log(this.stepActions[this.activeForm].saveAction, this.formParam);
          if (state) {
            this.curState = 'FINISH';
            this.wizardState.next({
              tag: this.activeForm,
              action: 'panelAction',
              data: { action: this.stepActions[this.activeForm].saveAction, emitAction: MobWizardPanelEvents.SAVE_SUCCESS, data: this.formParam },
            });
          } else {
            App.getNoticeService().error('值规则校验异常');
          }
        }).catch((error: any) => {
          App.getNoticeService().error(error);
        })
      }
    }
  }
```



### 向导完成逻辑

当用户点击完成后，最后一个表单执行玩对应的保存逻辑后，执行向导的完成逻辑。此时向导面板根据向导表单参数和视图参数调用部件服务执行finish行为，执行成功后提示成功信息，并携带数据抛出finish事件，最后通过应用中心服务通知相应消息。详细逻辑如下：

```ts
  /**
   * 完成行为
   *
   * @memberof WizardPanelControlBase
   */
  public doFinish() {
    let arg: any = {};
    Object.assign(arg, this.formParam);
    Object.assign(arg, { viewParam: this.viewParam });
    let tempContext: any = JSON.parse(JSON.stringify(this.context));
    this.onControlRequset('doFinish', tempContext, arg);
    const post: Promise<any> = this.ctrlService.finish(this.finishAction, tempContext, arg, this.showBusyIndicator);
    post.then((response: any) => {
      this.onControlResponse('doFinish', response);
      if (response && response.status === 200) {
        const data = response.data;
        const uiDataParams = this.getUIDataParam();
        Object.assign(uiDataParams, { data: data });
        this.ctrlEvent(MobWizardPanelEvents.FINISH, uiDataParams);
        App.getCenterService().notifyMessage(this.appDeCodeName, 'appRefresh', data);
      }
    })
    .catch((response: any) => {
      this.onControlResponse('doFinish', response);
      App.getNoticeService().error(response.message);
    });
  }
```

::: tip 提示

向导表单控制器继承多数据部件控制器基类，因此此处逻辑只是该部件特有逻辑。

基类逻辑参见 [部件基类 > 控制器](../app-ctrl-base.md#部件初始化)

**参考文件**

packages\ibz-core\src\ui-core\controllers\widget\mob-wizard-panel-controller.ts

:::

## ui层

向导表单中除了有一个步骤表单的表单部件外，就只有底部的按钮，每个按钮根据其对应的步骤表单上配置的步骤行为进行绘制。

### 绘制步骤表单

```tsx
/**
   * @description 渲染向导步骤表单
   * @memberof AppMobWizardPanel
   */
  public renderWizardStepForm() {
    const formInstance = this.getActiveFormInstance(this.c.activeForm);
    if (formInstance) {
      const otherParams = {
        viewState: this.c.wizardState,
        key: `${this.c.controlInstance.codeName}-${formInstance.codeName}`
      }
      return this.computeTargetCtrlData(formInstance, otherParams);
    }
  }
```

::: tip 提示

computeTargetCtrlData方法为部件基类中的方法，通过传递的参数绘制相应的部件。

:::

### 绘制底部按钮

根据每个向导表单的名称拿取对应的stepActions中的数据。如果存在就绘制，没有就不绘制。

```tsx
/**
   * @description 渲染向导面板组件
   * @return {*} 
   * @memberof AppMobWizardPanel
   */
  render() {
    if (!this.controlIsLoaded.value) {
      return;
    }
    const { width, height } = this.c.controlInstance;
    const controlStyle = {
      width: width ? width + 'px' : '',
      height: height ? height + 'px' : ''
    }
    const wizard = this.c.controlInstance.getPSDEWizard();
    return (
      <div class={{ ...this.classNames }} style={controlStyle}>
        {
          this.c.controlInstance.showStepBar ?
            <div class="wizard__steps_container">
              <app-steps
                active={this.getActiveStep()}
                steps={this.c.steps}
                panelInstance={this}
              />
            </div> : null
        }
        <div class="wizard__step_form_container">
          {this.renderWizardStepForm()}
        </div>
        <ion-footer>
          <ion-buttons>
            {this.buttonStatus('PREV') && wizard ?
              <ion-button onClick={(event: any) => { this.onClickPrev(event); }}>
                {this.$tl(wizard.getPrevCapPSLanguageRes()?.lanResTag, wizard.prevCaption) || this.$tl('share.previous', '上一步')}
              </ion-button> : null}
            {this.buttonStatus('NEXT') && wizard?
              <ion-button onClick={(event: any) => { this.onClickNext(event); }}>
                {this.$tl(wizard.getNextCapPSLanguageRes()?.lanResTag, wizard.nextCaption) || this.$tl('share.next', '下一步')}
              </ion-button> : null}
            {this.buttonStatus('FINISH') && wizard?
              <ion-button onClick={(event: any) => { this.onClickFinish(event); }}>
                {this.$tl(wizard.getFinishCapPSLanguageRes()?.lanResTag, wizard.finishCaption) || this.$tl('share.finish', '完成')}
              </ion-button> : null}
          </ion-buttons>
        </ion-footer>
      </div>
    )
  }
```

::: tip 提示

向导面板部件继承部件基类，因此此处逻辑只是向导面板部件的特有ui逻辑。

ui逻辑参见 [部件基类 > UI层](../app-ctrl-base.md#ui层)

**参考文件**

packages\ibz-ui-ionic\src\components\widget\app-mob-wizard-panel.tsx

:::
