# 实体移动端分页视图面板部件

分页视图面板部件是用于实体分页导航视图，以分页的形式呈现分页视图，部件名称为TABVIEWPANEL的默认显示，标题是分页导航的分页名称。

<component-iframe router="iframe/widget/app-mob-tabviewpanel" />

## 控制器

### 刷新

分页视图面板接收到分页导航面板发送的刷新通知时调用刷新方法，然后将该方法通知给下一层的嵌入视图，本质也是传递消息。

```ts
  /**
   * 刷新
   *
   * @param opts 行为参数
   * @param args 补充逻辑完成参数
   * @param showInfo 是否显示提示信息
   * @param loadding 是否显示loading效果
   * @returns {Promise<IParam>}
   * @memberof MobTabExpPanelCtrlController
   */
  public async refresh(
    opts?: IParam,
    args?: IParam,
    showInfo: boolean = true,
    loadding: boolean = true,
  ): Promise<ICtrlActionResult> {
    return new Promise((resolve: any) => {
      const embeddedView = this.controlInstance.getEmbeddedPSAppDEView() as IPSAppDEView;
      this.viewState.next({ tag: embeddedView.name, action: 'refresh', data: opts });
      resolve({ ret: true, data: null });
    });
  }
```

### 获取视图参数

分页视图面板负责解析嵌入视图数据和绘制嵌入视图，此处为解析分页导航视图上的嵌入视图数据。

```ts
  /**
   * @description 获取导航参数
   * @return {*}  {{ context: IParam, viewParam: IParam }}
   * @memberof MobTabViewPanelCtrlController
   */
  public getNavParams(): { context: IParam; viewParam: IParam } {
    const tempContext = Util.deepCopy(this.context);
    const tempViewParam = Util.deepCopy(this.viewParam);
    Object.assign(tempContext, ModelTool.getNavigateContext(this.controlInstance));
    Object.assign(tempViewParam, ModelTool.getNavigateParams(this.controlInstance));
    return { context: tempContext, viewParam: tempViewParam };
  }
```

::: tip 提示

分页视图面板控制器继承部件控制器基类，因此此处逻辑只是分页视图面板特有初始化逻辑。

基类初始化逻辑参见 [部件基类 > 控制器 > 部件初始化](../app-ctrl-base.md#部件初始化)

**参考文件**

packages\ibz-core\src\ui-core\controllers\widget\mob-tabviewpanel-ctrl-controller.ts

:::

## ui层

分页视图面板是分页导航面板的下一层，负责绘制嵌入视图，并传递通知。

```
|─ ─ tabexpview                                 分页导航视图
	|─ ─ tabexppanel                            分页导航面板
		|─ ─ tabviewpanel                       分页视图面板
			|─ ─ embedview                      嵌入视图
```

### 绘制嵌入视图

将控制器解析后的视图相关参数传给嵌入视图并绘制。

```tsx
/**
   * @description 绘制关系视图
   * @public
   * @memberof AppMobTabViewPanel
   */
  public renderEmbedView() {
    const embeddedView = this.c.controlInstance.getEmbeddedPSAppDEView();
    if (!embeddedView) {
      App.getNoticeService().warning(this.$tl('widget.mobtabviewpanel.noembeddedview', '嵌入视图不存在'));
      return null;
    }
    this.getViewComponent(embeddedView);
    const { context, viewParam } = this.c.getNavParams();
    return h(this.viewComponent, {
      viewPath: embeddedView?.modelPath,
      navContext: context,
      navParam: viewParam,
      viewState: this.c.viewState,
      viewShowMode: 'EMBEDDED',
      isShowCaptionBar: false,
    });
  }
```

::: tip 提示

分页视图面板部件继承部件组件基类，因此此处逻辑只是该部件特有ui逻辑。

ui逻辑参见 [部件基类 > UI层](../app-ctrl-base.md#ui层)

**参考文件**

packages\ibz-ui-ionic\src\components\widget\app-mob-tabviewpanel.tsx

:::
