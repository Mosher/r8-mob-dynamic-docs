# 实体移动端分页导航面板部件

分页导航面板部件是用于分页视图中tab分页的方式呈现的，导航分页tab中展示出页面标题。

<component-iframe router="iframe/widget/app-mob-tabexppanel" />

## 控制器

### 部件初始化

| <div style="width: 204px;text-align: left;">逻辑</div>逻辑 | <div style="width: 204px;text-align: left;">方法名</div> | <div style="width: 204px;text-align: left;">说明</div> |
| ---------------------------------------------------------- | -------------------------------------------------------- | ------------------------------------------------------ |
| 初始化激活项                                               | initActiveItem                                           | 设置分页导航面板默认激活项                             |

### 初始化激活项

默认选分页导航的第一项为激活项，可通过设置视图参数里 srftabactivate或者 srfnavtag来设置默认激活项

```ts
  /**
   * @description 初始化激活项
   * @private
   * @memberof MobTabExpPanelCtrlController
   */
  private initActiveItem() {
    const allControls = this.controlInstance.getPSControls() as IPSDETabViewPanel[];
    this.activeItem = allControls[0].name;
    if (this.viewParam && (this.viewParam.srftabactivate || this.viewParam.srfnavtag)) {
      const activate = this.viewParam.srftabactivate || this.viewParam.srfnavtag;
      if (activate) {
        this.activeItem = activate;
      }
    } 
  }
```

### 刷新

当点击分页栏切换分页后将触发刷新逻辑，具体实现为通知视图面板刷新。

```ts
/**
   * 刷新
   *
   * @param opts 行为参数
   * @param args 补充逻辑完成参数
   * @param showInfo 是否显示提示信息
   * @param loadding 是否显示loading效果
   * @returns {Promise<IParam>}
   * @memberof MobTabExpPanelCtrlController
   */
  public async refresh(
    opts?: IParam,
    args?: IParam,
    showInfo: boolean = true,
    loadding: boolean = true,
  ): Promise<ICtrlActionResult> {
    return new Promise((resolve: any) => {
      this.viewState.next({ tag: this.activeItem, action: 'refresh', data: opts });
      resolve({ ret: true, data: null });
    });
  }
```

### 获取计数器数据

分页导航的分页栏可配置计数器，绘制分页栏时调用控制器上的getCounterData方法，解析分页视图面板上配置的计数器服务数据并使用v-badge指令绘制。

```ts
/**
   * @description 获取计数器数据
   * @param {IPSDETabViewPanel} viewPanel
   * @return {string | number | null}  {(string | number | null)}
   * @memberof MobTabExpPanelCtrlController
   */
  public getCounterData(viewPanel: IPSDETabViewPanel): string | number | null {
    const counterRef = viewPanel.getPSAppCounterRef() as IPSAppCounterRef;
    const counterService = this.getCounterService(counterRef);
    if (counterService && viewPanel.counterId) {
      return counterService?.counterData?.[viewPanel.counterId.toLowerCase()];
    }
    return null;
  }
```

::: tip 提示

分页导航控制器继承部件控制器基类，因此此处逻辑只是分页导航特有初始化逻辑。

基类初始化逻辑参见 [部件基类 > 控制器 > 部件初始化](../app-ctrl-base.md#部件初始化)

**参考文件**

packages\ibz-core\src\ui-core\controllers\widget\mob-tabexppanel-ctrl-controller.ts

:::

### 激活项改变

点击分页导航面板项，切换当前选中分页视图面板

```ts
/**
   * @description 激活项改变
   * @param {string} activeItem 激活项
   * @memberof MobTabExpPanelCtrlController
   */
  public activeItemChange(activeItem: string) {
    this.activeItem = activeItem;
  }
```

## ui层

分页导航面板是分页导航视图下一层的部件，它主要负责绘制分页栏，并将分页导航视图上的通知传递给相应视图。嵌入视图的解析与绘制由分页视图面板处理。

```
|─ ─ tabexpview                                 分页导航视图
	|─ ─ tabexppanel                            分页导航面板
		|─ ─ tabviewpanel                       分页视图面板
			|─ ─ embedview                      嵌入视图
```

### 绘制分页栏

使用ionic组件库的ion-segment组件绘制模型中的分页项，点击分页栏触发控制器中的activeItemChange方法。

```tsx
/**
   * @description 输出分页头部
   * @param {IPSDETabViewPanel[]} allControls 所有分页视图面板部件
   * @return {*}
   * @memberof AppMobTabExpPanel
   */
  public renderSegment(allControls: IPSDETabViewPanel[]) {
    return (
      <ion-segment
        class='tabexppanel-header'
        scrollable={true}
        value={this.c.activeItem}
        onIonChange={($event: any) => this.ionChange($event)}
      >
        {allControls.map((viewPanel: IPSDETabViewPanel) => {
          const counterData = this.c.getCounterData(viewPanel);
          return (
            <ion-segment-button value={viewPanel?.name}>
              <ion-label>
                {viewPanel.getPSSysImage() && <app-icon icon={viewPanel.getPSSysImage()}></app-icon>}
                {this.$tl(viewPanel.getCapPSLanguageRes()?.lanResTag,viewPanel.caption) }
                {counterData && Object.is(this.c.activeItem, viewPanel.name) ? (
                  <ion-badge class='badge'>{counterData}</ion-badge>
                ) : null}
              </ion-label>
            </ion-segment-button>
          );
        })}
      </ion-segment>
    );
  }
```

### 绘制分页视图面板

分页导航面板只负责绘制分页栏。嵌入视图由分页视图面板绘制，所以这里只需将模型中的每一项绘制即可。

```tsx
/**
   * @description 分页视图面板部件渲染
   * @param {IPSDETabViewPanel[]} allControls
   * @return {*}
   * @memberof AppMobTabExpPanel
   */
  public renderTabViewPanel(allControls: IPSDETabViewPanel[]) {
    return allControls.map((item: IPSDETabViewPanel) => {
      const otherParams = {
        key: item.name,
      };
      if (Object.is(this.c.activeItem, item.name)) {
        return this.computeTargetCtrlData(item, otherParams);
      }
    });
  }
```

::: tip 提示

分页导航面板部件继承部件组件基类，因此此处逻辑只是该部件特有ui逻辑。

ui逻辑参见 [部件基类 > UI层](../app-ctrl-base.md#ui层)

**参考文件**

packages\ibz-ui-ionic\src\components\widget\app-mob-tabexppanel.tsx

:::