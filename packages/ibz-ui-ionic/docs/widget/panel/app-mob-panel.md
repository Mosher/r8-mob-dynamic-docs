# 实体移动端面板部件

面板部件通常是用来呈现面板数据的部件，可以被面板视图或者多数据等其他视图引用。面板部件主要有面板容器、面板按钮、面板属性、直接内容、分页面板、分页部件这几种类型。根据类型的不同去加载不同的组件。

<component-iframe router="iframe/widget/app-mob-panel" />

## 控制器

### 部件初始化

| <div style="width: 213px;text-align: left;">逻辑</div> | <div style="width: 213px;text-align: left;">方法名</div> | <div style="width: 213px;text-align: left;">说明</div> |
| ------------------------------------------------------ | -------------------------------------------------------- | ------------------------------------------------------ |
| 初始化值规则                                           | initRules                                                | 根据传入的面板项初始化值规则以便后续的校验             |
| 计算UI展示数据                                         | computedUIData                                           | 获取数据项集合组装面板数据，计算数据记载模式           |
| 计算面板按钮权限状态                                   | computeButtonState                                       | 更新detailsModel里的按钮的权限状态值                   |
| 面板逻辑                                               | panelLogic                                               | 动态控制相关，根据数据的不同决定面板组件的显示与隐藏等 |

#### 计算UI展示数据

获取数据项集合组装面板数据，计算数据记载模式。

```ts
  /**
   * @description 计算UI展示数据
   * @param {*} [newVal]
   * @memberof MobPanelController
   */
  public async computedUIData(newVal?: any) {
    if (this.controlInstance?.getAllPSPanelFields() && this.getDataItems().length > 0) {
      this.getDataItems().forEach((item: any) => {
        this.data[item.name] = null;
      });
    }
    const dataMode = this.controlInstance.dataMode;
    if (dataMode === 3) {
      this.viewCtx.appGlobal[this.controlInstance.dataName] = this.data;
    } else if (dataMode === 4) {
      this.viewCtx.routeViewGlobal[this.controlInstance.dataName] = this.data;
    } else if (dataMode === 5) {
      this.viewCtx.viewGlobal[this.controlInstance.dataName] = this.data;
    } else {
      if (this.isLoadDefault) {
        await this.computeLoadState(dataMode);
      }
    }
  }
```

#### 计算数据加载模式

监听通知并执行相应行为。当订阅收到行为为load时，只要面板数据模式不为3或4或5，就会去计算数据加载模式从而加载面板的数据。

::: tip 面板数据模式

0：不获取（使用传入数据）、 1：未传入时获取、 2：始终获取、 3：绑定到应用全局变量、 4：绑定到路由视图会话变量、 5：绑定到当前视图会话变量

:::

```ts
/**
   * @description 计算数据加载模式
   * @param {number} dataMode
   * @param args 参数
   * @memberof MobPanelController
   */
  public async computeLoadState(dataMode: number, args?: any) {
    if (dataMode === 0) {
      //  0：不获取，使用传入数据
      if (this.navDatas && this.navDatas.length > 0) {
        this.fillPanelData(this.navDatas[0]);
      }
    } else if (dataMode === 1) {
      //  1：存在传入数据时，不获取
      if (this.navDatas && this.navDatas.length > 0) {
        this.fillPanelData(this.navDatas[0]);
      } else {
        await this.loadPanelData(args);
      }
    } else if (dataMode === 2) {
      //  2：始终获取
      await this.loadPanelData(args);
    }
  }
```

#### 初始化值规则

根据传入的面板项初始化值规则以便后续的校验。

```ts
  /**
   * @description 初始化值规则
   * @param {(IPSPanelItem[] | null)} panelItems
   * @memberof MobPanelController
   */
  public initRules(panelItems: IPSPanelItem[] | null) {
    // 初始化非空值规则和数据类型值规则
    panelItems?.forEach((item: IPSPanelItem) => {
      const panelItem = item as IPSPanelContainer;
      if ((panelItem?.getPSPanelItems?.() as any)?.length > 0) {
        this.initRules(panelItem.getPSPanelItems());
      } else {
        const panelItem = item as IPSPanelField;
        if (panelItem?.getPSEditor?.()) {
          const editorRules: any = Verify.buildVerConditions(panelItem.getPSEditor());
          this.rules[panelItem.name] = [
            // 非空值规则
            {
              validator: (rule: any, value: any, callback: any) => {
                return !(this.detailsModel[panelItem.name].required && !value);
              },
              message: `${panelItem.caption} 必须填写`,
            },
            // 编辑器基础值规则
            ...editorRules,
          ];
        }
      }
    });
  }
```

#### 计算面板按钮权限状态

更新detailsModel里的按钮的权限状态值，绘制时直接根据该值判断是否显示或隐藏

```ts
  /**
   * @description 计算面板按钮权限状态
   * @param {*} data
   * @memberof MobPanelController
   */
  public computeButtonState(data: any) {
    const targetData: any = this.transformData(data);
    ViewTool.calcActionItemAuthState(targetData, this.actionModel, this.appUIService);
    // 更新detailsModel里的按钮的权限状态值
    if (this.detailsModel && Object.keys(this.detailsModel).length > 0) {
      Object.keys(this.detailsModel).forEach((name: any) => {
        const model = this.detailsModel[name];
        if (model?.itemType == 'BUTTON' && model.uiaction?.tag) {
          model.visible = this.actionModel[model.uiaction.tag].visabled;
          model.disabled = this.actionModel[model.uiaction.tag].disabled;
          model.isPower = this.actionModel[model.uiaction.tag].dataActionResult === 1 ? true : false;
        }
      });
    }
  }
```

#### 动态控制面板逻辑

动态控制相关，根据数据的不同决定面板组件的显示与隐藏等。

```ts
  /**
   * @description 面板逻辑
   * @param {{ name: string; newVal: any; oldVal: any }} { name, newVal, oldVal }
   * @memberof MobPanelController
   */
  public panelLogic({ name, newVal, oldVal }: { name: string; newVal: any; oldVal: any }): void {
    const allPanelItemGroupLogic = this.allPanelItemGroupLogic;
    if (allPanelItemGroupLogic.length > 0) {
      allPanelItemGroupLogic.forEach((panelItem: any) => {
        panelItem.panelItemGroupLogic?.forEach((logic: any) => {
          const relatedItemNames: string[] = logic.getRelatedItemNames?.() || [];
          if (Object.is(name, '') || relatedItemNames.indexOf(name) !== -1) {
            const ret = this.verifyGroupLogic(this.data, logic);
            switch (logic.logicCat) {
              // 动态空输入，不满足则必填
              case 'ITEMBLANK':
                this.detailsModel[panelItem.name].required = !ret;
                break;
              // 动态启用，满足则启用
              case 'ITEMENABLE':
                this.detailsModel[panelItem.name].disabled = !ret;
                break;
              // 动态显示，满足则显示
              case 'PANELVISIBLE':
                this.detailsModel[panelItem.name].visible = ret;
                break;
            }
          }
        });
      });
    }
  }
```

### 加载数据

计算数据加载模式后调用该方法，通过service去获取面板数据。

```ts
  /**
   * @description 加载数据
   * @param arg 参数
   * @memberof MobPanelController
   */
  public async loadPanelData(args?: any) {
    const action = this.controlInstance.getGetPSControlAction?.()?.getPSAppDEMethod?.()?.codeName || '';
    if (!action) {
      LogUtil.warn('getAction未配置');
    }
    if (args && Object.keys(args).length > 0) {
      Object.assign(this.data, args);
    }
    const service:any = await App.getEntityService()
      .getService(this.context, this.appDeCodeName.toLowerCase())
      .catch((error: any) => {
        LogUtil.warn('面板部件未获取到实体服务');
      });
    if (service && service[action] && service[action] instanceof Function) {
      try {
        const response: any = await service[action](Util.deepCopy(this.context), {...this.data});
        if (response && response.status == 200 && response.data) {
          this.fillPanelData(response.data);
        }
      } catch (response: any) {
        // this.$throw(response, 'load');
      }
    }
  }
```

### 面板编辑项值变更

改变面板编辑项后改变面板的数据并往外抛出事件，并进行动态校验。

```ts
  /**
   * @description 面板编辑项值变更
   * @param {IParam} event
   * @return {*}  {void}
   * @memberof MobPanelController
   */
  public onPanelItemValueChange(event: IParam): void {
    if (!event || !event.name || Object.is(event.name, '') || !this.data.hasOwnProperty(event.name)) {
      return;
    }
    this.data[event.name] = event.value;
    this.panelEditItemChange(event.name, event.value);
    this.panelLogic({ name: event.name, newVal: event.value, oldVal: null });
  }
  
  /**
   * @description 面板编辑项值变化
   * @param {string} property
   * @param {*} value
   * @memberof MobPanelController
   */
  public panelEditItemChange(property: string, value: any) {
    // 面板数据变化事件
    if (this.getDataItems().length > 0) {
      const modelitem = this.getDataItems().find((item: any) => {
        return item.name === property;
      });
      if (modelitem) {
        this.ctrlEvent(MobPanelEvents.PANEL_DATA_CHANGE, { [modelitem.prop]: value });
      }
    }
  }  
  
```

::: tip 提示

面板部件控制器继承部件控制器基类，因此此处逻辑只是面板部件特有挂载逻辑。

基类部件挂载逻辑参见 [部件基类 > 控制器](../app-ctrl-base.md#控制器)

**参考文件**

packages\ibz-core\src\ui-core\controllers\widget\mob-panel-controller.ts

:::

## UI层

面板是用来灵活的绘制部件项数据的部件，可绘制按钮、编辑器、直接内容，位置与样式页可随意配置修改。

### 绘制部件项

根据面板项类型来判断绘制相应的部件项，如容器、按钮、编辑器等。

```ts
    /**
   * @description 根据类型渲染面板项
   * @param {*} modelJson 面板项对象
   * @param {number} index 序列标识
   * @return {*}
   * @memberof AppMobPanel
   */
  public renderByDetailType(modelJson: any, index: number): any {
    if (modelJson.getPSSysPFPlugin()?.pluginCode) {
      const ctrlItemPluginInstance = App.getPluginService().getCtrlItemPluginByTag(
        modelJson.getPSSysPFPlugin()?.pluginCode,
      );
      if (ctrlItemPluginInstance) {
        return ctrlItemPluginInstance.renderItem(this.c.data[modelJson.name], this.c, this);
      }
    } else {
      switch (modelJson.itemType) {
        case 'CONTAINER':
          return this.renderContainer(modelJson, index);
        case 'BUTTON':
          return this.renderButton(modelJson, index);
        case 'FIELD':
          return this.renderField(modelJson, index);
        case 'RAWITEM':
          return this.renderRawItem(modelJson, index);
        case 'TABPANEL':
          return this.renderTabPanel(modelJson, index);
        case 'TABPAGE':
          return this.renderTabPage(modelJson, index);
      }
    }
  }
```
### 绘制编辑器

面板是可以承载编辑器的部件，根据模型中的项类型为编辑器时会绘制编辑器组件，与表单类似，将编辑器需要的参数传给编辑器组件，同时接收编辑器抛出的EditorEvent事件。

```tsx
/**
   * @description 渲染编辑器
   * @param {IPSEditor} editor 编辑器实例对象
   * @param {IPSPanelItem} parentItem 面板项对象
   * @return {*}
   * @memberof AppMobPanel
   */
  public renderEditor(editor: IPSEditor, parentItem: IPSPanelItem): any {
    const targetCtrlComponent: any = App.getComponentService().getEditorComponent(
      editor?.editorType,
      editor?.editorStyle ? editor?.editorStyle : 'DEFAULT',
      editor.getPSSysPFPlugin()
        ? `${editor?.editorType}_${editor?.editorStyle}`
        : undefined,
    );
    return h(targetCtrlComponent, {
      editorInstance: editor,
      containerCtrl: this.c.controlInstance,
      parentItem: parentItem,
      contextData: this.c.data,
      navContext: Util.deepCopy(this.c.context),
      navParam: Util.deepCopy(this.c.viewParam),
      disabled: this.c.detailsModel[editor.name]?.disabled,
      value: this.c.data[editor?.name || ''],
      onEditorEvent: (event: IEditorEventParam) => {
        this.c.handleEditorEvent(event);
      },
    });
  }
```

### 绘制按钮

面板中配置按钮时，面板也将绘制按钮，并将按钮的样式、位置通过AppLayoutItem组件绘制在面板上。

```tsx
/**
   * @description 渲染面板按钮
   * @param {IPSPanelButton} modelJson 按钮模型数据
   * @param {number} index 序列标识
   * @return {*}
   * @memberof AppMobPanel
   */
  public renderButton(modelJson: IPSPanelButton, index: number): any {
    const { caption, name, showCaption } = modelJson;
    const detailModel = this.c.detailsModel?.[name];
    //  模型显示
    if (detailModel && !detailModel.visible) {
      return;
    }

    const icon = modelJson.getPSSysImage();
    const uiAction = modelJson.getPSUIAction();
    const iconName = icon?.cssClass
      ? icon.cssClass
      : uiAction?.getPSSysImage()?.cssClass
      ? uiAction.getPSSysImage()?.cssClass
      : null;

    return (
      <AppLayoutItem
        class={this.getDetailClass(modelJson)}
        layoutMode={(modelJson.getParentPSModelObject() as any).getPSLayout()?.layout}
        layoutPos={modelJson.getPSLayoutPos() || undefined}
      >
        <ion-button
          disabled={detailModel && detailModel[name]?.disabled}
          onClick={(event: any) => {
            this.buttonClick(modelJson, event);
          }}
        >
          {iconName ? <app-icon name={iconName} /> : null}
          {showCaption ? this.$tl(modelJson.getCapPSLanguageRes()?.lanResTag,caption) : ''}
        </ion-button>
      </AppLayoutItem>
    );
  }
```

### 绘制直接内容

面板也支持配置直接内容，可绘制html、图片、直接内容。

```tsx
/**
   * @description 渲染直接内容
   * @param {IPSPanelButton} modelJson 直接内容模型数据
   * @param {number} index 序列标识
   * @return {*}
   * @memberof AppMobPanel
   */
  public renderRawItem(modelJson: IPSPanelRawItem, index: number): any {
    //  模型显示
    const detailModel = this.c.detailsModel?.[modelJson.name];
    if (detailModel && !detailModel.visible) {
      return;
    }
    const { contentType, htmlContent, getPSSysImage, rawContent } = modelJson;
    let element: any = undefined;
    if (contentType == 'HTML' && htmlContent) {
      element = h('div', {
        innerHTML: htmlContent,
      });
    }
    let content: any;
    switch (contentType) {
      case 'HTML':
        content = element ? element : htmlContent;
        break;
      case 'RAW':
        content = rawContent;
        break;
      case 'IMAGE':
        content = <img src={getPSSysImage?.()?.imagePath} />;
        break;
      case 'MARKDOWN':
        content = <div>{this.$tl('widget.mobpanel.nomarkdown','MARKDOWN直接内容暂未支持')}</div>;
        break;
    }
    return (
      <AppLayoutItem
        class={this.getDetailClass(modelJson)}
        layoutMode={(modelJson.getParentPSModelObject() as any).getPSLayout()?.layout}
        layoutPos={modelJson.getPSLayoutPos() || undefined}
      >
        {content}
      </AppLayoutItem>
    );
  }
```

::: tip 提示

面板部件继承部件基类，因此此处逻辑只是面板部件的特有ui逻辑。

ui逻辑参见 [部件基类 > UI层](../app-ctrl-base.md#ui层)

**参考文件**

packages\ibz-ui-ionic\src\components\widget\app-mob-panel.tsx

:::
