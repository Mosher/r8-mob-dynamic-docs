# 实体移动端状态向导面板部件

带有状态属性的向导面板，用于联接实体向导功能以及实体向导视图，为实体向导视图提供具体的向导内容。

<component-iframe router="iframe/widget/app-mob-state-wizard-panel" />

## 控制器

### 部件初始化

| 逻辑                     | 方法名           | 说明                                                         |
| ------------------------ | ---------------- | ------------------------------------------------------------ |
| 初始化首表单        | initFirstForm | 根据向导模型初始化首表单                   |
| 注册表单步骤行为 | regFormActions | 与步骤表单一样，将每个向导表单中的步骤行为添加到stepActions对象中，并以步骤表单的名称作为键名。 |
| 初始化表单 | doInit | 调用部件服务整合向导表单参数并计算激活表单 |

### 初始化首表单

与步骤向导面板类似，将模型中的第一个步骤表单作为第一个表单，此处相当于设置默认首表单。

```ts
  protected initFirstForm() {
    const wizard: IPSDEWizard | null = this.controlInstance.getPSDEWizard();
    const wizardForms: Array<IPSDEWizardForm> = wizard?.getPSDEWizardForms() || [];
    if (wizard && wizardForms.length > 0) {
      const firstForm: IPSDEWizardForm = wizardForms.find((form: IPSDEWizardForm) => {
        return form.firstForm;
      }) as IPSDEWizardForm;
      this.firstForm = `${this.controlInstance.name}_form_${firstForm.formTag?.toLowerCase()}`;
    }
  }
```

### 注册表单步骤行为

每一个表单都有配置对应的步骤行为，将这些数据从模型中解析并添加到对应的表单上，绘制时就看是否有对应的步骤行为，没有就隐藏。

```ts
  public regFormActions() {
    const wizard: IPSDEWizard | null = this.controlInstance.getPSDEWizard();
    const wizardForms: Array<IPSDEWizardForm> = wizard?.getPSDEWizardForms() || [];
    const wizardSteps = wizard?.getPSDEWizardSteps?.() || [];
    if (wizard && wizardForms.length > 0) {
      wizardForms.forEach((stepForm: IPSDEWizardForm) => {
        const formName = `${this.controlInstance.name}_form_${stepForm.formTag?.toLowerCase()}`;
        const editForm: IPSDEWizardEditForm = (this.controlInstance.getPSDEEditForms() || []).find(
          (form: IPSDEEditForm) => {
            return form.name === formName;
          },
        ) as IPSDEWizardEditForm;
        const action = {
          loadAction: editForm?.getGetPSControlAction()?.actionName || 'Get',
          preAction: editForm?.getGoBackPSControlAction()?.actionName,
          nextAction: editForm?.getUpdatePSControlAction()?.actionName || 'Update',
          actions: stepForm.getStepActions() || [],
        };
        this.regFormAction(
          formName,
          action,
          this.getStepTag(wizardSteps, stepForm?.getPSDEWizardStep()?.stepTag as string),
        );
      });
    }
    if (wizardSteps.length > 0) {
      wizardSteps.forEach((step: IPSDEWizardStep) => {
        this.steps.push({
          tag: step.stepTag,
          title: step.title,
          subTitle: step.subTitle,
          className: step.getTitlePSSysCss?.()?.cssName,
          icon: step.getPSSysImage?.()?.cssClass,
        });
      });
    }
  }
```

### 初始化表单

根据上下文以及视图参数调用状态向导面板部件服务获取向导表单参数，再根据状态向导表单参数加载指定向导表单。

```ts
  public doInit(opt: any = {}) {
    const arg: any = { ...opt };
    Object.assign(arg, { viewParam: this.viewParam });
    let tempContext: any = JSON.parse(JSON.stringify(this.context));
    this.onControlRequset('doInit', tempContext, arg);
    const post: Promise<any> = this.ctrlService.init(this.initAction, tempContext, arg, this.showBusyIndicator);
    post
      .then((response: any) => {
        this.onControlResponse('doInit', response);
        if (response && response.status === 200) {
          this.formParam = response.data;
          if (response.data[this.appDeCodeName.toLowerCase()]) {
            Object.assign(this.context, {
              [this.appDeCodeName.toLowerCase()]: response.data[this.appDeCodeName.toLowerCase()],
            });
          }
          this.computedActiveForm(this.formParam);
        }
      })
      .catch((response: any) => {
        this.onControlResponse('doInit', response);
        App.getNoticeService().error(response?.message);
      });
  }
```

### 计算激活表单

根据当前状态属性获取到相应步骤表单，并将其设置为激活表单，如果没有就将第一个表单设置为激活表单

```ts
/**
   * @description 计算激活表单
   * @param {*} data
   * @memberof MobStateWizardPanelController
   */
  public computedActiveForm(data: any) {
    if (data[this.stateField]) {
      if (Object.keys(this.stepTags).length > 0) {
        Object.keys(this.stepTags).forEach((name: string) => {
          if (this.stepTags[name] === data[this.stateField]) {
            this.activeForm = name;
          }
        });
      }
      if (!this.activeForm) {
        this.activeForm = this.firstForm;
      }
    } else {
      this.activeForm = this.firstForm;
    }
    if (this.activeForm) {
      let index = this.wizardForms.indexOf(this.activeForm);
      this.wizardForms.forEach((item: any, inx: number) => {
        if (inx <= index) {
          this.historyForms.push(item);
        }
      });
    }
  }
```

#### 表单保存

向导表单保存完成后抛出save事件，状态向导面板捕获进行处理。

当前状态为NEXT，即下一步时
当有下一个向导表单时，切换当前激活表单并通知表单加载。若没有时，执行向导的完成逻辑。
当前状态为PREV，即上一步时
当有上一个向导表单时，切换当前激活表单并通知表单加载。若没有时，不做任何操作。
当前状态为FINISH，即完成时
执行完成逻辑。

```ts
  public wizardPanelFormSave(name: string, args: any) {
    Object.assign(this.formParam, args);
    if (Object.is(this.curState, 'NEXT')) {
      this.historyForms.push(name);
      if (!this.stateField) {
        if (this.getNextForm()) {
          this.activeForm = this.getNextForm();
          setTimeout(() => {
            this.formLoad(this.formParam);
          }, 1);
        } else {
          this.doFinish();
        }
      } else {
        setTimeout(() => {
          this.formLoad(this.formParam);
        }, 1);
      }
    } else if (Object.is(this.curState, 'PREV')) {
      if (this.stateField) {
        setTimeout(() => {
          this.formLoad(this.formParam);
        }, 1);
      }
    } else if (Object.is(this.curState, 'FINISH')) {
      this.doFinish();
    }
  }
```

### 步骤切换逻辑

此部分包含上一步、下一步、完成、向导表单保存逻辑。这些逻辑相辅相成，共同完成向导面板步骤切换的全部逻辑。

#### 上一步

当用户点击上一步的时候，修改当前状态为PREV。向导面板会通知当前表单调用上一步操作对应的实体行为，然后重新展示上一个表单并通知其执行加载操作。

```ts
  public handlePrevious() {
    if (!this.stateField) {
      const length = this.historyForms.length;
      if (length > 1) {
        this.curState = 'PREV';
        this.activeForm = this.historyForms[length - 1];
        setTimeout(() => {
          this.formLoad(this.formParam);
        }, 1);
        this.historyForms.splice(length - 1, 1);
      }
    } else {
      if (this.activeForm) {
        this.wizardState.next({
          tag: this.activeForm,
          action: 'panelAction',
          data: {
            action: this.stepActions[this.activeForm].preAction,
            emitAction: MobWizardPanelEvents.SAVE_SUCCESS,
            data: this.formParam,
          },
        });
      }
    }
  }
```

#### 下一步

当用户点击下一步的时候，校验当前表单，校验成功之后修改当前状态为NEXT。向导面板会通知当前表单调用下一步操作对应的实体行为，然后展示下一个表单并通知其执行加载操作。

```ts
  public handleNext() {
    if (this.activeForm) {
      const form = this.ctrlRefsMap.get(this.activeForm);
      if (form && form.formValidateStatus && form.formValidateStatus instanceof Function) {
        form
          .formValidateStatus()
          .then((state: boolean) => {
            if (state) {
              this.curState = 'NEXT';
              this.wizardState.next({
                tag: this.activeForm,
                action: 'panelAction',
                data: {
                  action: this.stepActions[this.activeForm].nextAction,
                  emitAction: MobWizardPanelEvents.SAVE_SUCCESS,
                  data: this.formParam,
                },
              });
            } else {
              App.getNoticeService().error('值规则校验异常');
            }
          })
          .catch((error: any) => {
            App.getNoticeService().error(error);
          });
      }
    }
  }
```

#### 完成

当用户点击完成的时候，校验当前表单，校验成功之后修改当前状态为FINISH。向导面板会通知当前表单调用下一步操作对应的实体行为，然后在表单的保存回调中调用向导的完成逻辑。
```ts
  public handleFinish() {
    if (this.activeForm) {
      const form = this.ctrlRefsMap.get(this.activeForm);
      if (form && form.formValidateStatus && form.formValidateStatus instanceof Function) {
        form
          .formValidateStatus()
          .then((state: boolean) => {
            if (state) {
              this.curState = 'FINISH';
              this.wizardState.next({
                tag: this.activeForm,
                action: 'panelAction',
                data: {
                  action: this.stepActions[this.activeForm].saveAction,
                  emitAction: MobWizardPanelEvents.SAVE_SUCCESS,
                  data: this.formParam,
                },
              });
            } else {
              App.getNoticeService().error('值规则校验异常');
            }
          })
          .catch((error: any) => {
            App.getNoticeService().error(error);
          });
      }
    }
  }
```

#### 向导完成逻辑

当用户点击完成后，最后一个表单执行玩对应的保存逻辑后，执行向导的完成逻辑。此时状态向导面板根据向导表单参数和视图参数调用部件服务执行finish行为，执行成功后抛出finish行为。详细逻辑如下：

```ts
  public doFinish() {
    let arg: any = {};
    Object.assign(arg, this.formParam);
    Object.assign(arg, { viewparams: this.viewParam });
    let tempContext: any = JSON.parse(JSON.stringify(this.context));
    this.onControlRequset('doFinish', tempContext, arg);
    const post: Promise<any> = this.ctrlService.finish(this.finishAction, tempContext, arg, this.showBusyIndicator);
    post
      .then((response: any) => {
        this.onControlResponse('doFinish', response);
        if (response && response.status === 200) {
          const data = response.data;
          this.ctrlEvent('finish', data);
        }
      })
      .catch((response: any) => {
        this.onControlResponse('doFinish', response);
        App.getNoticeService().error(response?.message);
      });
  }
```

::: tip 提示

状态向导表单控制器继承多数据部件控制器基类，因此此处逻辑只是该部件特有逻辑。

基类逻辑参见 [部件基类 > 控制器](../app-ctrl-base.md#部件初始化)

**参考文件**

packages\ibz-core\src\ui-core\controllers\widget\mob-state-wizard-panel-controller.ts

:::

## ui层

状态向导表单中除了有一个步骤表单的表单部件外，就只有底部的按钮，每个按钮根据其对应的步骤表单上配置的步骤行为进行绘制。

### 绘制步骤表单

```tsx
/**
   * @description 渲染向导步骤表单
   * @return {*} 
   * @memberof AppMobStateWizardPanel
   */
  public renderWizardStepForm() {
    const formInstance = this.getActiveFormInstance(this.c.activeForm);
    if (formInstance) {
      const otherParams = {
        viewState: this.c.wizardState,
        key: `${this.c.controlInstance.codeName}-${formInstance.codeName}`
      }
      return this.computeTargetCtrlData(formInstance, otherParams);
    }
  }
```

::: tip 提示

computeTargetCtrlData方法为部件基类中的方法，通过传递的参数绘制相应的部件。

:::

### 绘制底部按钮

根据每个向导表单的名称拿取对应的stepActions中的数据。如果存在就绘制，没有就不绘制。

```tsx
/**
   * @description 渲染状态向导面板
   * @return {*} 
   * @memberof AppMobStateWizardPanel
   */
  render() {
    if (!this.controlIsLoaded.value) {
      return;
    }
    const { width, height } = this.c.controlInstance;
    const controlStyle = {
      width: width ? width + 'px' : '',
      height: height ? height + 'px' : ''
    }
    const wizard = this.c.controlInstance.getPSDEWizard();
    return (
      <div class={{ 'app-mob-state-wizard': true , ...this.classNames }} style={controlStyle}>
        {
          this.c.controlInstance.showStepBar ?
            <div class="wizard__steps_container">
              <app-steps
                active={this.getActiveStep()}
                steps={this.c.steps}
                panelInstance={this}
              />
            </div> : null
        }
        <div class="wizard__step_form_container">
          {this.renderWizardStepForm()}
        </div>
        <ion-footer>
          <ion-buttons>
            {this.buttonStatus('PREV') && wizard ?
              <ion-button onClick={(event: any) => { this.onClickPrev(event); }}>
                {this.$tl(wizard.getPrevCapPSLanguageRes()?.lanResTag, wizard.prevCaption) || this.$tl('share.previous', '上一步')}
              </ion-button> : null}
            {this.buttonStatus('NEXT') && wizard?
              <ion-button onClick={(event: any) => { this.onClickNext(event); }}>
                {this.$tl(wizard.getNextCapPSLanguageRes()?.lanResTag, wizard.nextCaption) || this.$tl('share.next', '下一步')}
              </ion-button> : null}
            {this.buttonStatus('FINISH') && wizard?
              <ion-button onClick={(event: any) => { this.onClickFinish(event); }}>
                {this.$tl(wizard.getFinishCapPSLanguageRes()?.lanResTag, wizard.finishCaption) || this.$tl('share.finish', '完成')}
              </ion-button> : null}
          </ion-buttons>
        </ion-footer>
      </div>
    )
  }
```

::: tip 提示

状态向导面板部件继承部件基类，因此此处逻辑只是状态向导面板部件的特有ui逻辑。

ui逻辑参见 [部件基类 > UI层](../app-ctrl-base.md#ui层)

**参考文件**

packages\ibz-ui-ionic\src\components\widget\app-mob-state-wizard-panel.tsx

:::
