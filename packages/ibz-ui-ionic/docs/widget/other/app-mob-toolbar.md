# 实体移动端工具栏

工具栏部件通常是用来呈现视图界面操作功能，对所有数据表格有系统提供的新建、编辑、删除、导出等操作，对一般表单有系统提供的保存、保存并关闭、关闭，对流程表单有保存、开始流程、流程节点中的交互线等操作，可以自定义界面行为。

<component-iframe router="iframe/widget/app-mob-toolbar" />

## 控制器

### 部件初始化

| 逻辑           | 方法名                   | 说明                                                         |
| -------------- | ------------------------ | ------------------------------------------------------------ |
| 计算工具栏权限 | calcToolbarItemAuthState | 通过配置的系统统一资源和工具栏项上配置的无权限显示模式来判断是否显示或禁用 |

#### 计算工具栏权限

工具栏部件控制器初始化(ctrlInit())时,会给视图通讯对象(viewState)添加一个事件监听器(permissionValid),当视图中数据发生改变等情况将触发工具栏的这个事件(permissionValid)，工具栏就执行calcToolbarItemAuthState方法并按照配置模型对工具栏项做出相应操作，如隐藏、禁用。

```ts
$mob-toolbar-ctrl-controller.ts

/**
   * @description 计算工具栏权限
   * @param {*} data
   * @memberof MobToolbarCtrlController
   */
  public calcToolbarItemAuthState(data: any) {
    if (this.toolbarModels.length > 0) {
      this.toolbarModels.forEach((item: any) => {
        if (Object.is(item.itemType, 'DEUIACTION')) {
          const uiaction: IPSUIAction | null = (item as IPSDETBUIActionItem).getPSUIAction();
          if (uiaction && uiaction.dataAccessAction) {
            let dataActionResult: any;
            if (uiaction && (Object.is(uiaction.actionTarget, 'NONE') || Object.is(uiaction.actionTarget, ''))) {
              if (
                Object.is(uiaction.actionTarget, '') &&
                Object.is(uiaction.uIActionTag, 'Save') &&
                this.appUIService.enableDEMainState()
              ) {
                if (data && Object.keys(data).length > 0) {
                  dataActionResult = this.appUIService.getAllOPPrivs(data, uiaction.dataAccessAction);
                }
              } else {
                dataActionResult = this.appUIService.getResourceOPPrivs(uiaction.dataAccessAction);
              }
            } else {
              if (data && Object.keys(data).length > 0 && this.appUIService.enableDEMainState()) {
                dataActionResult = this.appUIService.getAllOPPrivs(data, uiaction.dataAccessAction);
              }
            }
            if (dataActionResult) {
              item.noPrivHidden = false;
              item.noPrivDisabled = false;
            } else {
              // 禁用:1;隐藏:2;隐藏且默认隐藏:6
              if (item.noPrivDisplayMode === 1) {
                item.noPrivDisabled = true;
              }
              if (item.noPrivDisplayMode === 2 || item.noPrivDisplayMode === 6) {
                item.noPrivHidden = true;
              } else {
                item.noPrivHidden = false;
              }
            }
          }
        }
      });
    }
  }
```

::: tip 提示

工具栏控制器继承部件控制器基类。

部件控制器基类逻辑参见 [部件基类 > 控制器](../app-ctrl-base.md#控制器)

**参考文件**

packages\ibz-core\src\ui-core\controllers\widget\mob-toolbar-ctrl-controller.ts

:::

## UI层

工具栏位置为视图头右侧，工具栏的样式也有多种，分别为默认样式（平铺）、左侧抽屉弹出、右侧抽屉弹出、底部抽屉弹出。ui绘制时会根据工具栏模型绘制对应的工具栏样式。

### 绘制默认样式（平铺）

直接将工具栏项以按钮平铺到视图头右侧。

```tsx
	/**
   * @description 绘制平铺工具栏
   * @protected
   * @return {*}
   * @memberof AppMobToolbar
   */
  protected renderDefaultToolbar() {
    if (this.c.toolbarModels.length == 0) {
      return null;
    }
    if (Object.is(this.toolbarStyle, 'TOOLBAR')) {
      return this.c.toolbarModels.map((item: any) => {
        if (item.hiddenItem || item.noPrivHidden) {
          return null;
        }
        if (item.getPSSysPFPlugin()?.pluginCode && !App.isPreviewMode()) {
          const ctrlItemPluginInstance = App.getPluginService().getCtrlItemPluginByTag(
            item.getPSSysPFPlugin()?.pluginCode,
          );
          if (ctrlItemPluginInstance) {
            return ctrlItemPluginInstance.renderItem(item, this.c, this);
          }
        } else {
          const buttonStyle: IParam = { width: item.width ? item.width + 'px' : 'auto' };
          const cssName: string = item.getPSSysCss()?.cssName || '';
          return (
            <ion-button
              disabled={item.noPrivDisabled}
              style={buttonStyle}
              class={cssName}
              onClick={(e: any) => this.itemClick(item, e)}
              expand='block'
            >
              <app-icon icon={item.getPSSysImage()}></app-icon>
              {this.$tl(item.getCapPSLanguageRes()?.lanResTag,item.caption)}
            </ion-button>
          );
        }
      });
    } else {
      return this.renderToolbarButton();
    }
  }
```
::: warning

视图头右侧的空间原本就很少，所以建议当工具栏项少时（最多两项）可以使用该样式，项多时将会覆盖视图标题，极大的破坏了视图美感。

:::

### 绘制侧边工具栏（左侧、右侧）

使用ionic组件库中的ion-menu组件，工具栏的默认位置则绘制一个图标按钮，点击该按钮就会在侧边弹出菜单，菜单中的数据就是工具栏项。

```tsx
/**
   * @description 绘制侧边工具栏
   * @param {string} side
   * @return {*}
   * @memberof AppMobToolbar
   */
  public renderToolbar(side: string) {
    if (this.c.toolbarModels.length == 0) {
      return null;
    }
    return (
      <ion-menu
        slot='fixed'
        class='app-toolbar-popup'
        ref={this.toolbarRef}
        side={side}
        contentId={this.contentId}
        menuId={this.toolbarID}
      >
        <ion-list>
          {this.c.toolbarModels.map((item: IPSDEToolbarItem) => {
            return this.renderToolbarItem(item);
          })}
        </ion-list>
      </ion-menu>
    );
  }
```

### 绘制底部工具栏

使用ionic组件库的ion-action-sheet组件，默认位置也是绘制一个图表按钮，点击按钮就从底部弹出一个ion-action-sheet组件，其中的数据为工具栏项。

```tsx
/**
   * @description 打开底部菜单
   * @memberof AppMobToolbar
   */
  public async openBottomToolbar() {
    const buttons: any[] = [];
    this.c.toolbarModels.forEach((item: any) => {
      const cssName: string = item.getPSSysCss()?.cssName || '';
      const icon: string = this.getIcon(item);
      buttons.push({
        text: this.$tl(item.getCapPSLanguageRes()?.lanResTag,item.caption),
        icon: icon,
        cssClass: cssName,
        handler: (e: any) => {
          this.itemClick(item, e);
        },
      });
    });
    const actionSheet = await actionSheetController.create({
      buttons: buttons,
    });
    await actionSheet.present();
  }
```

### 工具栏项点击

工具栏项点击后会关闭侧边弹框或底部弹框，同时也会向视图抛出工具栏点击事件，并将工具栏项上的界面行为通过拼接为tag的方式传给视图，视图接收后通过tag判断对应的界面行为然后执行。

```tsx
/**
   * @description 工具栏项点击
   * @param {string} name
   * @param {MouseEvent} e
   * @memberof AppMobToolbar
   */
  public itemClick(item: any, e: MouseEvent): void {
    this.emitCtrlEvent({
      action: MobToolbarEvents.TOOLBAR_CLICK,
      controlname: this.c.controlInstance.name,
      data: { tag: `${this.c.name}_${item.name}_click`, event: e },
    });
    this.closeToolbar();
  }
```

::: tip 提示

工具栏UI继承部件基类，因此此处逻辑只是工具栏UI特有逻辑。

部件基类UI逻辑参见 [部件基类 > UI层](../app-ctrl-base.md#ui层)

**参考文件**

packages\ibz-ui-ionic\src\components\widget\app-mob-toolbar.tsx

:::
