# 上下文菜单
上下文菜单应用于当前选择数据的操作，常应用于树视图节点和日历事程，如树节点或者日历事程上长按进行新建、编辑等操作。

<component-iframe router="iframe/widget/app-mob-contextmenu" />

## 控制器

### 部件初始化

| <div style="width: 204px;text-align: left;">逻辑</div> | <div style="width: 204px;text-align: left;">方法名</div> | <div style="width: 204px;text-align: left;">说明</div> |
| ------------------------------------------------------ | -------------------------------------------------------- | ------------------------------------------------------ |
| 初始化输入数据                                         | initInputData                                            | 初始化时接收父组件传递的参数                           |

### 初始化输入参数

由于上下文菜单是树部件和日历部件长按后进行绘制的，且绘制时将长按的事件和上下文行为模型一同传递了过去，上下文菜单控制器中将这两个参数解析，ui绘制时将根据这两个参数进行具体绘制。

```ts
/**
   * @description 初始化输入参数
   * @param {IParam} opts
   * @memberof MobContextMenuCtrlController
   */
  public initInputData(opts: IParam) {
    super.initInputData(opts);
    this.mouseEvent = opts.mouseEvent;
    this.contextMenuActionModel = opts.contextMenuActionModel;
  }
```

::: tip 提示

上下文菜单控制器继承部件控制器基类。

部件控制器基类逻辑参见 [部件基类 > 控制器](../app-ctrl-base.md#控制器)

**参考文件**

packages\ibz-core\src\ui-core\controllers\widget\mob-contextmenu-ctrl-controller.ts

:::

## UI层

### 绘制弹框

绘制上下文菜单时，会根据控制器中的长按事件来确定上下文菜单的位置。

```tsx
  render() {
    if (!this.controlIsLoaded.value) {
      return;
    }
    return (
      <ion-popover
        cssClass='app-mob-contextmenu'
        isOpen={this.isShowPopover.value}
        event={this.c.mouseEvent}
        onDidDismiss={() => this.setOpenState(false)}
      >
        {this.renderContextMenuContent()}
      </ion-popover>
    );
  }
```

### 绘制数据项

绘制上下文菜单部件模型中的工具栏项，且通过控制器中的上下文行为模型数据判断是否显示或禁用。

```tsx
/**
   * @description 绘制界面行为
   * @param {IPSDEToolbarItem} item 界面行为模型
   * @return {*}
   * @memberof AppMobContextMenu
   */
  public renderUIAction(item: IPSDEToolbarItem) {
    const visible = this.c.contextMenuActionModel[item.name]
      ? this.c.contextMenuActionModel[item.name]?.visabled
      : true;
    const disabled = this.c.contextMenuActionModel[item.name]
      ? this.c.contextMenuActionModel[item.name]?.disabled
      : false;
    if (visible) {
      return (
        <ion-item
          button
          class='context-menu-item'
          detail={false}
          disabled={disabled}
          onClick={(event: MouseEvent) => this.itemClick(item, event)}
        >
          <ion-label>
            {item?.showIcon && item.getPSSysImage() && <app-icon icon={item.getPSSysImage()}></app-icon>}
            {item.showCaption && 
              <span>
                {this.$tl(item.getCapPSLanguageRes()?.lanResTag, item.caption)}
              </span>
            }
          </ion-label>
        </ion-item>
      );
    }
  }
```

### 逻辑

#### 菜单项点击

在上下文菜单点击时关闭上下文菜单弹框并抛出onMenuItemClick事件，在多数据部件基类中处理上下文菜单部件事件。

```ts
  public itemClick(detail: IPSDEToolbarItem, event: MouseEvent) {
    const data = {
      name: this.c.controlInstance.name,
      detail: detail,
      event: event,
      data: this.item,
    };
    this.setOpenState(false);
    this.emitCtrlEvent({
      controlname: this.c.controlInstance.controlType?.toLowerCase(),
      action: MobContextMenuEvents.MENUITEM_CLICK,
      data: data,
    });
  }
```

::: tip 提示

上下文菜单UI继承部件基类，因此此处逻辑只是应用菜单UI特有逻辑。

部件基类UI逻辑参见 [部件基类 > UI层](../app-ctrl-base.md#ui层)

**参考文件**

packages\ibz-ui-ionic\src\components\widget\app-mob-contextmenu.tsx

:::
