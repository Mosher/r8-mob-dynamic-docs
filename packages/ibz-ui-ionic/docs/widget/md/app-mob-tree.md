# 实体移动端树部件

树部件是用来呈现节点间有树形层级关系的部件，并具备展开收起、长按显示上下文菜单等交互功能。

<component-iframe router="iframe/widget/app-mob-tree" />

## 控制器

### 数据加载

整合当前节点上下文和视图参数，从部件服务对象中获取节点数据，设置附加标题栏、默认展开节点和默认选中。

```ts
  /**
   * @description 加载数据
   * @param {IParam} [args={}] 行为参数
   * @param {IParam} [opts] 额外行为参数
   * @param {boolean} [showInfo=this.showBusyIndicator] 是否显示提示信息
   * @param {boolean} [loadding=true] 是否显示loadding效果
   * @return {*}  {Promise<ICtrlActionResult>}
   * @memberof MobTreeCtrlController
   */
  public async load(
    args: IParam = {},
    opts?: IParam,
    showInfo: boolean = this.showBusyIndicator,
    loadding: boolean = true,
  ): Promise<ICtrlActionResult> {
    const params: any = {
      srfnodeid: args.data && args.data.id ? args.data.id : '#',
      srfnodefilter: this.nodeFilter,
    };
    const tempViewParams: any = Util.deepCopy(this.viewParam);
    if (tempViewParams.selectedData) {
      delete tempViewParams.selectedData;
    }
    Object.assign(tempViewParams, args);
    if (tempViewParams.data) {
      delete tempViewParams.data;
    }
    ...
  }
```

### 刷新

树部件的刷新与其他部件不同，由于树可存在多个实体，所以有必要判断刷新模式，如刷新当前节点、刷新所有节点等。

```ts
/**
   * @description 刷新
   * @param {any[]} args
   * @memberof MobTreeCtrlController
   */
  public async refresh(
    opts?: IParam,
    args?: IParam,
    showInfo: boolean = true,
    loadding: boolean = true,
  ): Promise<ICtrlActionResult> {
    if (opts && opts.length > 0) {
      const refreshMode = opts[0].refreshMode;
      switch (refreshMode) {
        case 1:
          return await this.refreshCurrent();
        case 2:
          return await this.refreshParent();
        case 3:
          return await this.refreshAll();
        default:
          return await this.refreshCurrent();
      }
    } else {
      return await this.refreshAll();
    }
  }
```

### 设置默认选中

如果是导航视图默认选中第一项，否则从选中数组中找出对应的选中数据项 。当父节点选中时，会选中所有子节点。

```ts
/**
   * @description 设置默认选中,回显数项，选中所有子节点
   * @param {any[]} items 当前节点所有子节点集合
   * @param {boolean} [isRoot=false] 是否是加载根节点
   * @param {boolean} [isSelectedAll=false] 是否选中所有子节点
   * @return {*}  {void}
   * @memberof MobTreeCtrlController
   */
  private setDefaultSelection(items: any[], isRoot: boolean = false, isSelectedAll: boolean = false): void {
    if (items.length == 0) {
      return;
    }
    const UIDataParam = this.getUIDataParam();
    let defaultData: any;
    // 导航中选中第一条配置的默认选中,没有选中第一条
    if (this.isSelectFirstDefault) {
      ...
    }
    // 已选数据的回显
    if (this.selections && this.selections.length > 0) {
      ...
    }
    // 父节点选中时，选中所有子节点
    if (isSelectedAll) {
      const leafNodes = items.filter((item: any) => item.leaf);
      this.selections = this.selections.concat(leafNodes);
      Object.assign(UIDataParam, { data: this.selections });
      this.ctrlEvent(MobTreeEvents.SELECT_CHANGE, UIDataParam);
    }
    //  修改选中
    items.forEach((item: any) => {
      const index = this.selections.findIndex((selection: any) => {
        return item.srfkey === selection.srfkey && item.srfmajortext === selection.srfmajortext;
      });
      item.selected = index !== -1;
    });
  }
```

### 计数器

树节点上可配置计数器标识，绘制时通过v-badge指令添加到树节点上。

```ts
/**
   * @description 获取计数器数据
   * @param {IParam} node 节点数据
   * @return {*}
   * @memberof MobTreeCtrlController
   */
  public getCounterData(node: IParam) {
    const ctrlCounterRef: IPSAppCounterRef = this.controlInstance.getPSAppCounterRef() as IPSAppCounterRef;
    const counterService = this.getCounterService(ctrlCounterRef);
    if (node.counterId && counterService) {
      return counterService?.counterData?.[node.counterId.toLowerCase()];
    } else {
      return null;
    }
  }
```

::: tip 提示

树部件控制器继承多数据部件控制器基类，因此此处逻辑只是该部件特有初始化逻辑。

基类初始化逻辑参见 [多数据部件 > 控制器 > 部件初始化](./md-ctrl-base.md#部件初始化)

**参考文件**

packages\ibz-core\src\ui-core\controllers\widget\mob-tree-ctrl-controller.ts

:::

## UI层

### 渲染树主体内容

如果部件显示模式配为选择（'SELECT'），则看是否是多选还是单选（isMultiple），相应的绘制成多选树与单选树，否则绘制默认树。

```ts
  /**
   * @description 渲染树主体内容
   * @return {*}
   * @memberof AppMobTree
   */
  public renderTreeMainContent() {
    if (Object.is('SELECT', this.c.ctrlShowMode)) {
      if (this.c.isMultiple) {
        return this.renderMultipleTree();
      } else {
        return this.renderSingleTree();
      }
    } else {
      return this.renderDefaultTree();
    }
  }
```

### 绘制多选树

树部件数据项调用树节点绘制方法。

```ts
  /**
   * @description 渲染多选树
   * @return {*}
   * @memberof AppMobTree
   */
  public renderMultipleTree() {
    return (
      <div class="tree-node--container is-multiple">
        {this.c.items.map((item: IParam) => {
          const node = this.getNodeByType(item.nodeType);
          return this.renderTreeNode(node as IPSDETreeNode, item);
        })}
      </div>
    )
  }
```

### 绘制单选树

使用了ion-radio-group单选组件，里面调用了树部件数据项调用树节点绘制方法。

```ts
  /**
   * @description 渲染单选树
   * @return {*}
   * @memberof AppMobTree
   */
  public renderSingleTree() {
    const value = this.c.selections[0]?.id;
    return (
      <div class="tree-node--container is-single">
        <ion-radio-group class="tree-node--radio-group" value={value}>
          {this.c.items.map((item: IParam) => {
            const node = this.getNodeByType(item.nodeType);
            return this.renderTreeNode(node as IPSDETreeNode, item);
          })}
        </ion-radio-group>
      </div>
    )
  }
```

### 绘制默认树

同绘制多选树

```ts
  /**
   * @description 渲染默认树
   * @return {*}
   * @memberof AppMobTree
   */
  public renderDefaultTree() {
    return (
      <div class="tree-node--container">
        {this.c.items.map((item: IParam) => {
          const node = this.getNodeByType(item.nodeType);
          return this.renderTreeNode(node as IPSDETreeNode, item);
        })}
      </div>
    )
  }
```

### 渲染树节点

通过是否单选（isMultiple）区分绘制多选checkbox还是单选radio。树节点上绑定了点击事件和长按事件，点击事件决定数据的选中，而长按事件用来显示该项绑定的上下文菜单

```ts
  /**
   * @description 渲染树节点
   * @param {IPSDETreeNode} node
   * @param {IParam} item
   * @return {*} 
   * @memberof AppMobTree
   */
  public renderTreeNode(node: IPSDETreeNode, item: IParam) {
    const hasChildren = node?.hasPSDETreeNodeRSs?.();
    return (
      <div class={["tree-node", item.cssName]}>
        <ion-item
          class="tree-node--content"
          onTouchstart={(event: any) => {
            this.onTouchStart(node, event);
          }}
          onTouchend={(event: any) => {
            this.clearTimer();
          }}
          onTouchmove={(event: any) => {
            this.clearTimer();
          }}
          onClick={(event: any) => {
            this.handleTreeNodeClick(item, node, event);
          }}>
          {Object.is('SELECT', this.c.ctrlShowMode) && !hasChildren ?
            this.c.isMultiple ? 
              <ion-checkbox slot="start" value={item.selected}></ion-checkbox> :
              <ion-radio slot="start" value={item.id}></ion-radio> : null}
          {this.renderNodeLabel(item, hasChildren ? hasChildren : false)}
        </ion-item>
        {hasChildren && item.expanded && item.children && item.children.length > 0 ?
          <div class="tree-node--children">
            {item.children.map((_item: IParam) => {
              const _node = this.getNodeByType(_item.nodeType);
              return this.renderTreeNode(_node as IPSDETreeNode, _item);
            })}
          </div> : null}
      </div>
    )
  }
```

### 绘制上下文菜单

长按树节点项会弹出上下文菜单框，该菜单框中的行为项为配置项，该配置在树部件中的数据项中进行配置。

```ts
  /**
   * @description 渲染上下文菜单
   * @return {*}
   * @memberof AppMobTree
   */
  public renderContextMenu() {
    const type = this.activeNode.value?.type || '';
    const mouseEvent = this.activeNode.value?.mouseEvent;
    const navDatas = this.activeNode.value?.navDatas;
    if (type && mouseEvent) {
      const treeNode = (this.c.controlInstance.getPSDETreeNodes() || []).find((node: IPSDETreeNode) => {
        return node.nodeType.toLowerCase() == type.toLowerCase();
      });
      const contextMenu = ModelTool.findPSControlByName(
        treeNode?.getPSDEContextMenu?.()?.name as string,
        this.c.controlInstance.getPSControls(),
      );
      if (treeNode && contextMenu && this.isShowContextMenu) {
        this.isShowContextMenu = false;
        const otherParams = {
          key: Util.createUUID(),
          navDatas: [navDatas],
          mouseEvent: mouseEvent,
          contextMenuActionModel: {},
        };
        return this.computeTargetCtrlData(contextMenu, otherParams);
      }
    }
  }
```

::: tip 提示

树部件继承部件组件基类，因此此处逻辑只是该部件特有ui逻辑。

ui逻辑参见 [多数据部件基类 > UI层](./md-ctrl-base.md#ui层)

**参考文件**

packages\ibz-ui-ionic\src\components\widget\app-mob-tree.tsx

:::
