# 实体移动端地图部件

地图部件负责解析地图数据并绘制地图，并将地图部件上的的数据绘制出来，点击这些数据将会抛出事件，如果该地图部件配置在地图导航视图上还会跳转到导航视图上。

<component-iframe router="iframe/widget/app-mob-map" />

## 控制器

### 部件初始化

| <div style="width: 213px;text-align: left;">逻辑</div> | <div style="width: 213px;text-align: left;">方法名</div> | <div style="width: 213px;text-align: left;">说明</div> |
| ------------------------------------------------------ | -------------------------------------------------------- | ------------------------------------------------------ |
| 初始化部件行为模型                                     | initCtrlActionModel                                      | 根据模型初始化上下文菜单                               |

#### 初始化部件行为模型

根据模型初始化上下文菜单。

```ts
  public initCtrlActionModel() {
    const mapItems = this.controlInstance.getPSSysMapItems() || [];
    const tempModel: any = {};
    if (mapItems?.length > 0) {
      mapItems.forEach((item: IPSSysMapItem) => {
        if (item.getPSDEContextMenu() && item.getPSDEContextMenu()?.getPSDEToolbarItems()) {
          const toobarItems = item.getPSDEContextMenu()?.getPSDEToolbarItems() as IPSDEToolbarItem[];
          if (toobarItems?.length > 0) {
            toobarItems.forEach((toolbarItem: IPSDEToolbarItem) => {
              this.initActionModelItem(toolbarItem, item, tempModel);
            });
          }
        }
      });
    }
    this.actionModel = {};
    Object.assign(this.actionModel, tempModel);
  }
```

### 数据加载

地图部件上配置的数据项中每一项都会配置一个实体、并且每一项都会有经度纬度，load方法就是获取这些经纬度信息。并将这些数据以配置的形式绘制在地图上，如点、线、区域。

```ts
  public load(data: any = {}): void {
    if (!this.fetchAction) {
      App.getNoticeService().error(`未配置fetchAction行为`);
      return;
    }
    this.items = [];
    const parentData: any = {};
    this.ctrlEvent(MobMapEvents.BEFORE_LOAD, data);
    Object.assign(data, parentData);
    const tempViewParams: any = parentData.viewparams ? parentData.viewparams : {};
    if (this.viewParam) {
      Object.assign(tempViewParams, Util.deepCopy(this.viewParam));
    }
    Object.assign(tempViewParams, data);
    for (const item in this.mapItems) {
      let sort = '';
      if (this.mapItems[item].sort) {
        sort += this.mapItems[item].sort + ',';
      }
      if (sort) {
        sort = sort + 'desc';
        Object.assign(tempViewParams, { sort: sort });
      }
    }
    Object.assign(data, { viewParam: tempViewParams });
    const tempContext: any = Util.deepCopy(this.context);
    this.onControlRequset('load', tempContext, data);
    this.ctrlService.search(this.fetchAction, this.context, data).then(
      (response: any) => {
        if (!response || response.status !== 200) {
          App.getNoticeService().error(response.error?.message || '加载数据异常');
        }
        this.items = response.data;
        this.ctrlEvent(MobMapEvents.LOAD_SUCCESS, this.items);
        // 处理地图所有数据需要一定时间，所以将关闭遮罩放置在地图绘制完成之后
        this.onControlResponse('load', response);
      },
      (response: any) => {
        this.onControlResponse('load', response);
        App.getNoticeService().error(response.error?.message || '加载数据异常');
      },
    );
  }
```

::: tip 提示

实体移动端地图部件控制器继承多数据部件控制器基类，因此此处逻辑只是该部件特有初始化逻辑。

多数据部件基类初始化逻辑参见 [多数据部件 > 控制器 > 部件初始化](./md-ctrl-base.md#部件初始化)

:::

## UI层

### 绘制

#### 绘制主内容

```tsx
  render() {
    ......
    return (
      <div class={{ ...this.classNames }}>
        {this.renderPullDownRefresh()}
        <div style="height:auto">{this.renderContextMenu()}</div>
        <div id={this.mapID} ref='map' class='map'></div>
      </div>
    );
  }
```

#### 绘制上下文菜单

右键点击地图部件会弹出上下文菜单框，该菜单框中的行为项为配置项，该配置在系统地图部件中的数据项中进行配置。

```tsx
 public renderContextMenu() {
     if (this.activeData.value) {
      const mouseEvent = this.activeData.value?.mouseEvent;
      const data = this.activeData.value?.data[0];
      const mapItem = (this.c.controlInstance as IPSSysMap).getPSSysMapItems()?.find((_mapItem: IPSSysMapItem) => 
        Object.is(data.itemType.toLowerCase(), _mapItem.itemType.toLowerCase())
      );
      const contextMenu = ModelTool.findPSControlByName(
        mapItem?.getPSDEContextMenu?.()?.name as string,
        this.c.controlInstance.getPSControls(),
      );
      if (contextMenu && this.isShowContextMenu) {
        this.isShowContextMenu = false;
        const otherParams = {
          key: Util.createUUID(),
          navDatas: [data.curData],
          mouseEvent: mouseEvent,
          contextMenuActionModel: {},
        };
        return this.computeTargetCtrlData(contextMenu, otherParams);
      }
    }
  }
```

### 逻辑

### 初始化地图模型

将控制器load方法中获取的数据整合到mapItems中，并将这些数据以相应形式绘制到配置的地图中。其中数据的显示方法有点、线、区域三种方式，每种方式分别解析数据后绘制。

```ts
  public initMapModel() {
    const mapItems: IPSSysMapItem[] | null = this.c.controlInstance.getPSSysMapItems();
    this.showLegends = [];
    if (mapItems) {
      	......
        // 设置图例
        this.initOptions.legend.data.push(item.name);
        this.initOptions.visualMap.push({
          type: 'piecewise',
          left: 'left',
          top: 'bottom',
          splitNumber: 1,
          ......
        });
        // 数据项点样式
        if (Object.is('POINT', item.itemStyle)) {
          this.initOptions.series.push({
            name: item.name,
            type: 'scatter',
            coordinateSystem: 'geo',
            itemType: item.itemType?.toLowerCase(),
            color: item.color,
            selectedMode: 'single',
            .......
          });
            // 数据项线样式
        } else if (Object.is('LINE', item.itemStyle)) {
          this.initOptions.series.push({
            name: item.name,
            type: 'lines',
            geoIndex: 0,
            itemType: item.itemType?.toLowerCase(),
            coordinateSystem: 'geo',
            polyline: true,
            ......
          });
           // 数据项 区域图样式
        } else if (Object.is('REGION', item.itemStyle)) {
          this.initOptions.series.push({
            name: item.name,
            type: 'custom',
            geoIndex: 0,
            itemType: item.itemType?.toLowerCase(),
            coordinateSystem: 'geo',
            renderItem: (params: any, api: any) => this.renderRegion(params, api),
            .......
          });
        }
      });
    }
  }
```

#### 初始化地图

由于每个地图的map数据过大，所以为了优化用户体验使用了按需引入的方式，需自行配置用户参数引入特定的map数据，如果未配置默认为中国地图。

```ts
 public registerMap() {
    // TODO地图不能配自定义参数，默认给中国地图
    const userParams: any = this.c.controlInstance.userParams || {};
    if (userParams?.mapName) {
      const geoJson = require(`../../../assets/json/map/${userParams?.mapName}.json`);
      registerMap(userParams?.mapName, geoJson);
    } else {
      const geoJson = require(`../../../assets/json/map/china.json`);
      registerMap('china', geoJson);
    }
  }
```

#### 地图挂载

在地图挂载时监听地图事件，执行相应行为。

```ts
  public ctrlMounted(args?: any){
    super.ctrlMounted(args);
    this.registerMap();
    const map: any = document.getElementById(this.mapID);
    if (map) {
      this.map = init(map);
    }
    if (this.map) {
      this.map.setOption(this.initOptions);
      this.setMapSize();
      // 如果是导航视图默认选中第一项
      this.map.on('selectchanged', 'series', (params: any) => {
        ......
      });
      // 监听地图触发的点击事件
      this.map.on('click', 'series', (params: any) => {
       ......
      });
      // 监听地图触发的按下事件
      this.map.on('mousedown', 'series', (params: any) => {
        // 阻止浏览器默认行为
        params.event.event.preventDefault();  
        ......
      });    
      // 监听地图触发的离开事件        
      this.map.on('mouseup', 'series', (params: any) => {
        if (Object.is(params.seriesType, 'scatter') && params.data?.value) {         
          this.clearTimer();
        }
      });      
      // 图例改变过滤地图数据
      this.map.on('legendselectchanged', (params: any) => {
          ......
          // 重新设置地图数据
          this.valueMax = 0;
          const data = this.getAreaValueList();
          this.map.setOption({
            ......
          });
        }
      });
    }        
  }
```

#### 初始化地图数据

将加载回来的数据整合到地图序列中，交由echarts绘制地图。

```ts
  public handleMapOptions() {
    const series: Array<any> = this.initOptions.series;
    if (!series || series.length == 0) {
      return;
    }
    series.forEach((serie: any) => {
      const seriesData: Array<any> = [];
      this.items.forEach((item: any) => {
        if (Object.is(item.itemType, serie.itemType)) {
          seriesData.push(item);
        }
      });
      this.handleSeriesOptions(seriesData, serie);
    });
    Object.assign(this.initOptions.series, series);
  }
```

#### 点击地图数据

点击地图数据时抛出onSelectionChange事件。

```ts
  public onClick(data: any[],event?:any) {
    this.c.selections = data;
    this.c.ctrlEvent(MobChartEvents.SELECT_CHANGE, this.c.selections);
  }
```

#### 长按

长按地图数据设置激活数据打开上下文菜单。

```ts
  public onMouseDown(data: any, event: MouseEvent) {
    let timeStart = this.getTimeNow();//获取鼠标按下时的时间
    this.touchTimer.value = setInterval(() => {
      let timeEnd = this.getTimeNow();
      if(timeEnd - timeStart > 1000){
        this.activeData.value = {
          mouseEvent: event,
          data: data,
        };
        this.isShowContextMenu = true;
        this.clearTimer();
      }
    }, 100);
  }
```

::: tip 提示

实体移动端地图部件控制器继承多数据部件基类，因此此处逻辑只是该部件特有UI。

多数据部件基类初始化逻辑参见 [多数据部件 > UI层](./md-ctrl-base.md#ui层)

**参考文件**

packages\ibz-ui-ionic\src\components\widget\app-mob-map.tsx
:::