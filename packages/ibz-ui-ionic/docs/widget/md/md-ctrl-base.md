# 多数据部件基类

所有多数据部件的公共基类，继承部件基类。

## 控制器

### 部件初始化

| 逻辑                 | 方法名           | 说明                                                         |
| -------------------- | ---------------- | ------------------------------------------------------------ |
| 初始化代码表分组集合 | initGroupDetail  | 若多数据部件配置有代码表分组，则根据部件模型初始化出多数据部件的代码表分组集合模型 |
| 初始化分组配置       | initGroupOptions | 根据部件模型初始化分组配置，包括分组模式、分组属性等         |
| 初始化排序           | initSort         | 根据部件模型初始化排序配置，包括排序字段、排序方向等         |
| 初始化选中值         | initSelects      | 若当前部件的导航视图参数中存在选中数据字段(selectedData)，则会将该数据存放在多数据部件的选中集合中 |
| 初始化数据映射       | initDataMap      | 根据部件模型初始化数据映射关系，具体部件的初始化逻辑见其描述 |

#### 初始化代码表分组集合

若多数据部件配置有代码表分组，则根据部件模型初始化出多数据部件的代码表分组集合模型。

```ts
  private async initGroupDetail() {
    const groupCodeList = this.controlInstance.getGroupPSCodeList?.() as IPSAppCodeList;
    if (this.isEnableGroup && groupCodeList) {
      await groupCodeList.fill?.();
      const codeListItems = await App.getCodeListService().getDataItems({
        tag: groupCodeList.codeName,
        type: groupCodeList.codeListType,
        navContext: this.context,
        navViewParam: this.viewParam,
      });
      if (codeListItems && codeListItems.length) {
        codeListItems.forEach((item: any) => {
          if (item.value && item.text) {
            this.groupDetail.push({
              value: item.value,
              text: item.text,
            });
          }
        });
      }
    }
  }
```

#### 初始化分组配置

根据部件模型初始化是否开启分组，分组模式，分组属性。

```ts
  private initGroupOptions() {
    this.isEnableGroup = this.controlInstance?.enableGroup;
    if (this.isEnableGroup) {
      this.groupMode = this.controlInstance.groupMode;
      const groupField = this.controlInstance.getGroupPSAppDEField?.()?.codeName;
      if (groupField) {
        this.groupField = groupField.toLowerCase() as string;
      }
    }
  }
```

#### 初始化排序

根据部件模型初始化是否禁用排序，排序字段，排序方向，排序对象。

```ts
  private initSort() {
    this.isNoSort = this.controlInstance.noSort;
    this.minorSortDir = this.controlInstance.minorSortDir;
    this.minorSortPSDEF = this.controlInstance.getMinorSortPSAppDEField?.()?.codeName?.toLowerCase();
    this.sort = { sort: '' };
    if (this.minorSortPSDEF) {
      Object.assign(this.sort, {
        sort: `${this.minorSortPSDEF},${this.minorSortDir ? this.minorSortDir : 'ASC'}`,
      });
    }
  }
```

#### 初始化选中值

若当前部件的导航视图参数中存在选中数据字段(selectedData)，则会将该数据存放在多数据部件的选中集合中。

```ts
  private initSelects() {
    if (this.viewParam?.selectedData) {
      this.selections = this.viewParam.selectedData;
    }
  }
```

### 获取分组数据

如果多数据部件开启分组，则多数据部加载完数据之后会调用该方法对数据进行分组。

```ts
  protected getGroupData() {
    if (
      this.getGroupDataByCodeList &&
      this.getGroupDataByCodeList instanceof Function &&
      Object.is(this.groupMode, 'CODELIST')
    ) {
      this.getGroupDataByCodeList(this.items);
    } else if (
      this.getGroupDataAuto &&
      this.getGroupDataAuto instanceof Function &&
      Object.is(this.groupMode, 'AUTO')
    ) {
      this.getGroupDataAuto(this.items);
    }
  }
```

### 处理部件事件

多数据部件上可能配置有上下文菜单，快速工具栏，批操作工具栏等部件，该方法则用来处理这些部件的抛出的点击事件。

```ts
  public handleCtrlEvent(controlname: string, action: string, data: IParam) {
    super.handleCtrlEvent(controlname, action, data);
    if (Object.is('contextmenu', controlname)) {
      this.handleContextMenuEvent(action, data);
    }
    if (Object.is(`${this.controlInstance.name}_quicktoolbar`, controlname) || Object.is(`${this.controlInstance.name}_batchtoolbar`, controlname)) {
      this.handleToolbarEvent(action, data);
    }
  }
```

## UI层

### 绘制

#### 绘制批操作工具栏

批操作工具栏是为了方便执行批量操作所提供的一种快速执行某种批量行为的工具栏，如批量删除、批量拷贝等。

因此当用户选中多条数据，并且当前部件配置有批操作工具栏时，UI层会将该工具栏绘制出来，并将相关导航数据传递下去。

```tsx
  public renderBatchToolbar() {
    if (this.c.selections.length > 0) {
      const batchToolbar = ModelTool.findPSControlByName(
        `${this.c.controlInstance.name}_batchtoolbar`,
        (this.c.controlInstance as any).getPSControls(),
      );
      if (batchToolbar) {
        return this.computeTargetCtrlData(batchToolbar);
      }
    }
  }
```

#### 绘制快速工具栏

快速操作工具栏是在无数据的情况下提供的一种快速执行某项行为的工具栏，如新建等。

因此当多数据部件无数据且配置有快速操作工具栏时，UI层会将该工具栏绘制出来，并将相关导航数据传递下去。

```tsx
  public renderQuickToolbar() {
    const quickToolbar = ModelTool.findPSControlByName(
      `${this.c.controlInstance.name}_quicktoolbar`,
      (this.c.controlInstance as any).getPSControls(),
    );
    if (quickToolbar) {
      return this.computeTargetCtrlData(quickToolbar);
    }
  }
```

::: tip 提示

多数据部件基类继承部件基类，更多逻辑可参见 [部件基类](../app-ctrl-base.md)

**参考文件**

packages\ibz-ui-ionic\src\components\widget\md-ctrl-component-base.tsx
:::