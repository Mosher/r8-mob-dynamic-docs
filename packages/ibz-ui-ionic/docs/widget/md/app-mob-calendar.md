# 实体移动端日历部件

用于展示日期，以及日历对应的数据，并可通过相应的配置对日程数据进行操作。

<component-iframe router="iframe/widget/app-mob-calendar" />

## 控制器

### 部件初始化

| <div style="width: 213px;text-align: left;">逻辑</div> | <div style="width: 213px;text-align: left;">方法名</div> | <div style="width: 213px;text-align: left;">说明</div> |
| ------------------------------------------------------ | -------------------------------------------------------- | ------------------------------------------------------ |
| 初始化日历项模型                                       | initCalendarItemsModel                                   | 根据模型初始化日历项数据模型                           |
| 初始化激活日历项                                       | initActiveItem                                           | 初始化日历激活项                                       |

#### 初始化日历项模型

```ts
  private initCalendarItemsModel() {
    const calendarItems = (this.controlInstance as IPSSysCalendar).getPSSysCalendarItems() || [];
    if (calendarItems.length > 0) {
      calendarItems.forEach((calendarItem: IPSSysCalendarItem) => {
        const _appde = calendarItem.getPSAppDataEntity();
        const idField = calendarItem.getIdPSAppDEField();
        const keyField = ModelTool.getAppEntityKeyField(_appde);
        const majorField = ModelTool.getAppEntityMajorField(_appde);
        const itemType = calendarItem.itemType.toLowerCase();
        const obj = {
          appde: _appde?.codeName?.toLowerCase(),
          keyPSAppDEField: idField?.codeName ? idField.codeName.toLowerCase() : keyField?.codeName?.toLowerCase(),
          majorPSAppDEField: majorField ? majorField?.codeName.toLowerCase() : 'srfmajortext',
        };
        this.calendarItemsModel.set(itemType, obj);
      });
    }
  }
```
#### 初始化激活日历项

```ts
  private initActiveItem() {
    const calendarItems = (this.controlInstance as IPSSysCalendar).getPSSysCalendarItems() || [];
    if (calendarItems && calendarItems[0]) {
      this.activeItem = calendarItems[0].itemType.toLowerCase();
    }
  }
```

#### 部件初始化
监听通知并执行相应行为。
```ts
  public ctrlInit() {
    super.ctrlInit();
    if (this.viewState) {
      this.viewStateEvent = this.viewState.subscribe(({ tag, action, data }: IViewStateParam) => {
        if (!Object.is(this.name, tag)) {
          return;
        }
        if (Object.is(action, 'load')) {
          this.formatDate(new Date(), data);
        }
        if (Object.is(action, 'search')) {
          this.formatDate(this.curTime, data);
        }
      });
    }
  }
```
::: tip 提示

实体移动端日历部件控制器继承多数据部件控制器基类，因此此处逻辑只是该部件特有初始化逻辑。

基类初始化逻辑参见 [多数据部件 > 控制器 > 部件初始化](./md-ctrl-base.md#部件初始化)

:::
### 时间格式化

在加载数据之前会根据日历样式对查询时间进行格式化，之后会根据该起止时间查询数据。

```ts
  /**
   * @description 格式化时间（根据日历样式格式化开始时间和结束时间）
   * @param {*} curtime 当前时间
   * @param {IParam} [opts] 行为参数
   * @param {IParam} [args] 附加行为参数
   * @param {boolean} [isSetEvent=true] 是否设置事程
   * @param {boolean} [loadding=true] 是否显示loadding效果
   * @return {*}  {Promise<ICtrlActionResult>}
   * @memberof MobCalendarCtrlController
   */
  public formatDate(
    curtime: IParam,
    opts?: IParam,
    args?: IParam,
    isSetEvent: boolean = true,
    loadding: boolean = true,
  ): Promise<ICtrlActionResult> {
    this.curTime = curtime;
    switch (this.calendarStyle) {
      case 'DAY':
        this.start = (moment as any)(curtime).startOf('day').format('YYYY-MM-DD HH:mm:ss');
        this.end = (moment as any)(curtime).endOf('day').format('YYYY-MM-DD HH:mm:ss');
        break;
      case 'MONTH':
      case 'MONTH_TIMELINE':
        this.start = (moment as any)(curtime).startOf('month').format('YYYY-MM-DD HH:mm:ss');
        this.end = (moment as any)(curtime).endOf('month').format('YYYY-MM-DD HH:mm:ss');
        break;
      case 'WEEK':
      case 'WEEK_TIMELINE':
        this.start = (moment as any)(curtime).startOf('week').format('YYYY-MM-DD HH:mm:ss');
        this.end = (moment as any)(curtime).endOf('week').format('YYYY-MM-DD HH:mm:ss');
        break;
    }
    const _args = { ...args };
    Object.assign(_args, { start: this.start, end: this.end });
    return this.load(opts, _args, isSetEvent, loadding);
  }
```

### 打开视图

如果日历项配置有相关的编辑视图（引用模式为：EDITVIEW），在点击该日历项数据时会直接打开该编辑视图。

```ts
  public async openView($event: IParam): Promise<any> {
    const calendarItems = (this.controlInstance as IPSSysCalendar).getPSSysCalendarItems() || [];
    const itemType = $event.itemType.toLowerCase();
    const calendarItem = calendarItems.find((item: IPSSysCalendarItem) => {
      return itemType == item.itemType.toLowerCase();
    });
    if (!calendarItem) {
      return;
    }
    const appde = calendarItem.getPSAppDataEntity()?.codeName.toLowerCase() as string;
    const tempContext: any = Util.deepCopy(this.context);
    tempContext[appde] = $event[appde];
    const view = await this.getEditView(calendarItem);
    if (!view) {
      return;
    }
    if (Object.is(view.placement, 'INDEXVIEWTAB') || Object.is(view.placement, '')) {
      const path: string = ViewTool.buildUpRoutePath(
        tempContext,
        view.deResParameters,
        view.parameters,
        [$event],
        this.viewParam,
        true,
      );
      App.getOpenViewService().openView(path);
    } else {
      let container: Subject<any>;
      if (Object.is(view.placement, 'POPUPMODAL')) {
        container = App.getOpenViewService().openModal({ viewModel: view.viewModel }, tempContext, this.viewParam);
      } else {
        container = App.getOpenViewService().openDrawer({ viewModel: view.viewModel }, tempContext, this.viewParam);
      }
      container.subscribe((result: any) => {
        if (!result || !Object.is(result.ret, 'OK')) {
          return;
        }
        const args = { start: this.start, end: this.end };
        this.load({}, args, false);
      });
    }
  }
```

## UI层

### 绘制

#### 绘制部件内容

根据配置的日历样式绘制不同的日历。

```tsx
  render() {
    ......
    return (
      <div
        class={{ ...this.classNames }}
        onTouchmove={($event: any) => this.gotouchmove($event)}
        onTouchstart={($event: any) => this.gotouchstart($event)}
      >
        {this.renderPullDownRefresh()}
        {this.renderContextMenu()}
        <div class={['calender-content', this.c.activeItem]}>
          {calendarStyle == 'MONTH' || calendarStyle == 'MONTH_TIMELINE' ? this.renderMonthCalendar() : null}
          {calendarStyle == 'DAY' ? this.renderDayCalendar() : null}
          {calendarStyle == 'WEEK' || calendarStyle == 'WEEK_TIMELINE' ? this.renderWeekCalendar() : null}
          {calendarItems && this.c.groupData.length == 0 ? this.renderSegment(calendarItems) : null}
          <div class='calendar-events'>
            {this.renderTimelineCalendar(calendarStyle)}
            {this.renderBatchToolbar()}
          </div>
        </div>
      </div>
    );
  }
```
#### 绘制日程

日历可以配置分组和分组插件，对其进行分组绘制，如果日历项上还配置有布局面板则会绘制项布局面板。

```tsx
  public renderEventList() {
    if (this.c.isEnableGroup) {
      const pluginCode = this.c.controlInstance.getGroupPSSysPFPlugin()?.pluginCode;
      if (pluginCode) {
        const ctrlItemPluginInstance = App.getPluginService().getCtrlItemPluginByTag(
          pluginCode,
        );
        if (ctrlItemPluginInstance) {
          return ctrlItemPluginInstance.renderItem(this.c.groupData, this.c, this);
        }
      } else {
        return this.renderGroupEvent();
      }
    } else {
      return this.renderNoGroupEvent();
    }
  }
```

#### 绘制上下文菜单

如果日历项上配置有上下文菜单，长按数据项改变激活数据，打开上下文菜单。

```tsx
  public renderContextMenu() {
    const mouseEvent = this.activeData.value?.mouseEvent;
    const data = this.activeData.value?.data;
    if (this.activeData.value) {
      const calendarItem = (this.c.controlInstance as IPSSysCalendar).getPSSysCalendarItems()?.find((_calendarItem: IPSSysCalendarItem) => 
        Object.is(data.itemType.toLowerCase(), _calendarItem.itemType.toLowerCase())
      );
      const contextMenu = ModelTool.findPSControlByName(
        calendarItem?.getPSDEContextMenu?.()?.name as string,
        this.c.controlInstance.getPSControls(),
      );
      if (contextMenu && this.isShowContextMenu) {
        this.isShowContextMenu = false;
        const otherParams = {
          key: Util.createUUID(),
          navDatas: [data.curData],
          mouseEvent: mouseEvent,
          contextMenuActionModel: {},
        };
        return this.computeTargetCtrlData(contextMenu, otherParams);
      }
    }
  }
```

### 逻辑

#### 长按

长按改变激活数据，绘制上下文菜单。

```tsx
  public onTouchStart(item: any, event: MouseEvent) {
    this.touchTimer.value = setInterval(() => {
      this.activeData.value = {
        mouseEvent: event,
        data: item,
      };
      this.isShowContextMenu = true;
      this.clearTimer();
    }, 500);
  }
```

#### 上个

切换日历到上一天，上一周或者是上个月时会触发该事件。

```tsx
  private prev(year: number, month: number, weekIndex: number) {
    if (this.c.calendarStyle == 'MONTH_TIMELINE' || this.c.calendarStyle == 'MONTH') {
      this.selectday(year, month, this.day.value);
      this.initCurrentTime(new Date(year + '/' + month + '/' + '1'));
      this.c.formatDate(this.currentDate.value);
    }
    if (this.c.calendarStyle == 'WEEK_TIMELINE' || this.c.calendarStyle == 'WEEK') {
      this.countWeeks(year, month, weekIndex);
    }
  }
```

#### 下个

切换日历到下一周或者是下个月时会触发该事件。

```tsx
  private next(year: number, month: number, weekIndex: number) {
    if (this.c.calendarStyle == 'MONTH_TIMELINE' || this.c.calendarStyle == 'MONTH') {
      this.selectday(year, month, this.day.value);
      this.initCurrentTime(new Date(year + '/' + month + '/' + '1'));
      this.c.formatDate(this.currentDate.value);
    }
    if (this.c.calendarStyle == 'WEEK_TIMELINE' || this.c.calendarStyle == 'WEEK') {
      this.countWeeks(year, month, weekIndex);
    }
  }
```

#### 点击日期

点击日期时加载该日期的日程数据。

```ts
  private clickDay(data: any) {
    if (data) {
      const reTime = data.join('/');
      const temptime = new Date(reTime);
      this.year.value = temptime.getFullYear();
      this.month.value = temptime.getMonth();
      this.day.value = temptime.getDate();
      const start = (moment as any)(temptime).startOf('day').format('YYYY-MM-DD HH:mm:ss');
      const end = (moment as any)(temptime).endOf('day').format('YYYY-MM-DD HH:mm:ss');
      const args = { start: start, end: end };
      this.c.load({}, args, false);
    }
  }
```

#### 点击日程

点击日程后会打开该日历项关联的编辑视图。

```ts
  private onEventClick($event: IParam) {
    this.c.openView($event);
  }
```

::: tip 提示

实体移动端日历部件继承多数据部件基类，因此此处逻辑只是该部件特有ui逻辑。

ui逻辑参见 [多数据部件 > UI层](./md-ctrl-base.md#ui层)

**参考文件**

packages\ibz-ui-ionic\src\components\widget\app-mob-calendar.tsx

:::
