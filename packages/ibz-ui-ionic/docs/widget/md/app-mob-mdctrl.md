# 实体移动端多数据部件

实体移动端多数据部件是对多条业务数据的呈现，目前支持列表和图标两种显示样式，支持数据分组和项布局面板。

<component-iframe router="iframe/widget/app-mob-mdctrl" />

## 控制器

### 部件初始化

| <div style="width: 213px;text-align: left;">逻辑</div> | <div style="width: 213px;text-align: left;">方法名</div> | <div style="width: 213px;text-align: left;">说明</div> |
| ------------------ | ------------------- | ------------------------------------ |
| 初始化数据映射     | initDataMap         | 初始化数据映射                       |
| 初始化界面行为模型 | initCtrlActionModel | 初始化左滑界面行为组和右滑界面行为组 |

#### 初始化数据映射

用于将列表项相关信息传递给面板，如是否为自定义代码。

```ts
  public initDataMap() {
    const dataItems: IPSDEListDataItem[] | null = this.controlInstance.getPSDEListDataItems();
    if (dataItems && dataItems.length > 0) {
      dataItems.forEach((dataItem: IPSDEListDataItem) => {
        this.dataMap.set(dataItem.name,{ customCode: dataItem.customCode ? true : false });
      });
    }
  }
```

#### 初始化界面行为模型

```ts
  public initCtrlActionModel() {
    //  左滑界面行为集合
    const swipeLeftActionGroupDetails =
      this.controlInstance.getPSDEUIActionGroup?.()?.getPSUIActionGroupDetails?.() || [];
    swipeLeftActionGroupDetails.forEach((detail: IPSUIActionGroupDetail) => {
      const uiAction = detail.getPSUIAction() as IPSUIAction;
      this.actionModel[uiAction.uIActionTag] = this.getCtrlActionModelDetail(detail);
    });
    //  右滑界面行为集合
    const swipeRightActionGroupDetails =
      this.controlInstance.getPSDEUIActionGroup2?.()?.getPSUIActionGroupDetails?.() || [];
    swipeRightActionGroupDetails.forEach((detail: IPSUIActionGroupDetail) => {
      const uiAction = detail.getPSUIAction() as IPSUIAction;
      const actionModelDetail: any = this.getCtrlActionModelDetail(detail);
      Object.assign(actionModelDetail, {
        isShowCaption: detail.showCaption,
        isShowIcon: detail.showIcon,
      });
      this.actionModel[uiAction.uIActionTag] = actionModelDetail;
    });
  }
```

### 初始化部件

监听通知并执行相应行为。

```ts
  public ctrlInit(args?: IParam) {
    super.ctrlInit(args);
    if (this.viewState) {
      this.viewStateEvent = this.viewState.subscribe(({ tag, action, data = {} }: IViewStateParam) => {
        if (!Object.is(this.name, tag)) {
          return;
        }
        if (Object.is(action, 'load')) {
          this.curPage = 0;
          this.load(data);
        }
        if (Object.is(action, 'search')) {
          this.curPage = 0;
          this.load(data);
        }
        if (Object.is(action, 'quickSearch')) {
          this.load(data);
        }
      });
    }
  }
```

::: tip 提示

实体移动端多数据部件控制器继承多数据部件控制器基类，因此此处逻辑只是该部件特有初始化逻辑。

多数据部件基类初始化逻辑参见 [多数据部件 > 控制器 > 部件初始化](./md-ctrl-base.md#部件初始化)

:::

### 数据加载

部件接到加载通知后执行加载数据，合并查询参数后执行自定义数据加载前逻辑，之后抛出onBeforeLoad事件，在引擎中合并搜索表单的查询参数，之后调用部件服务执行查询。根据查询结果执行数据加载成功逻辑或数据加载失败逻辑，之后计算列表项左滑右滑禁用状态，如果有分组情况，则对数据进行分组。

```ts
  public async load(
    opts?: IParam,
    args?: IParam,
    showInfo: boolean = true,
    loadding: boolean = true,
  ): Promise<ICtrlActionResult> {
    const tempViewParams: any = {};
    // 合并page，size
    Object.assign(tempViewParams, {
      page: this.curPage,
      size: this.limit,
    });
    // 合并sort
    if (this.sort) {
      Object.assign(tempViewParams, this.sort);
    }
    Object.assign(tempViewParams, Util.deepCopy(opts));
    Object.assign(tempViewParams, Util.deepCopy(this.viewParam));
    opts ? opts : opts = {};
    Object.assign(opts, { viewParam: tempViewParams });
    // 多实例查询数据处理
    const appEnvironment = App.getEnvironment();
    if (appEnvironment?.bDynamic) {
      if (tempViewParams.hasOwnProperty('srfdynainstid')) {
        const dynainstParam: DynamicInstanceConfig = this.modelService?.getDynaInsConfig();
        Object.assign(tempViewParams, { srfinsttag: dynainstParam.instTag, srfinsttag2: dynainstParam.instTag2 });
        delete tempViewParams.srfdynainstid;
      } else {
        if (!tempViewParams.hasOwnProperty('srfinsttag')) {
          Object.assign(tempViewParams, { srfinsttag: '__srfstdinst__' });
        }
      }
    } else {
      if (tempViewParams.hasOwnProperty('srfwf')) {
        Object.assign(tempViewParams, { srfinsttag: '__srfstdinst__' });
      }
    }
    const tempContext: any = Util.deepCopy(this.context);
    const beforeUIDataParam: IUIDataParam = this.getUIDataParam();
    Object.assign(beforeUIDataParam, {
      action: this.fetchAction,
      navContext: tempContext,
      navParam: opts,
      data: this.items,
    });
    const beforeloadResult: any = await this.executeCtrlEventLogic('onbeforeload', beforeUIDataParam);
    if (beforeloadResult && beforeloadResult?.hasOwnProperty('srfret') && !beforeloadResult.srfret) {
      return { ret: false, data: this.items };
    }
    this.ctrlEvent(MobMDCtrlEvents.BEFORE_LOAD, beforeUIDataParam);
    this.onControlRequset('load', { ...this.context }, opts, loadding);
    try {
      const response = await this.ctrlService.search(this.fetchAction, this.context, opts, showInfo);
      this.onControlResponse('load', response);
      if (response && response.status == 200) {
        const successUIDataParam: IUIDataParam = this.getUIDataParam();
        Object.assign(successUIDataParam, {
          action: this.fetchAction,
          navContext: tempContext,
          navParam: opts,
          data: response.data,
        });
        const loadsuccessResult: any = await this.executeCtrlEventLogic('onloadsuccess', successUIDataParam);
        if (loadsuccessResult && loadsuccessResult?.hasOwnProperty('srfret') && !loadsuccessResult.srfret) {
          return { ret: false, data: response.data };
        }
        this.ctrlEvent(MobMDCtrlEvents.LOAD_SUCCESS, successUIDataParam);
        this.totalRecord = response.total;
        if (args?.type == 'loadMore') {
          if (response.data?.length) {
            const tempItems = this.items;
            this.items = [];
            this.items = tempItems.concat(response.data);
          }
        } else {
          this.items = [];
          this.items = response.data;
        }
        // 将选中数据补充完整
        if (this.selections?.length > 0) {
          const selections: any[] = [];
          this.selections.forEach((selection: any) => {
            const _selection = this.items.find((item: any) => Object.is(selection.srfkey, item.srfkey));
            if (_selection) {
              selections.push(_selection);
            }
          });
          this.selections = selections;
        }
        this.items.forEach((item: any) => {
          // 计算是否选中
          const index = this.selections.findIndex((temp: any) => {
            return temp.srfkey == item.srfkey;
          });
          if (index != -1 || Object.is(this.selectedValue, item.srfkey)) {
            item.checked = true;
          } else {
            item.checked = false;
          }
          Object.assign(item, this.getActionState(item));
          // 计算权限
          this.computeSlidingDisabled(item);
        });
        if (this.isEnableGroup) {
          this.getGroupData();
        }
      } else {
        const errorUIDataParam: IUIDataParam = this.getUIDataParam();
        Object.assign(errorUIDataParam, {
          action: this.fetchAction,
          navContext: tempContext,
          navParam: opts,
          data: response.data,
        });
        const loaderrorResult: any = await this.executeCtrlEventLogic('onloaderror', errorUIDataParam);
        if (loaderrorResult && loaderrorResult?.hasOwnProperty('srfret') && !loaderrorResult.srfret) {
          return { ret: false, data: response.data };
        }
        this.ctrlEvent(MobMDCtrlEvents.LOAD_ERROR, errorUIDataParam);
        App.getNoticeService().error(response.error?.message || '加载数据异常');
      }
      return { ret: true, data: response.data };
    } catch (error: any) {
      this.onControlResponse('load', error);
      const errorUIDataParam: IUIDataParam = this.getUIDataParam();
      Object.assign(errorUIDataParam, {
        action: this.fetchAction,
        navContext: tempContext,
        navParam: opts,
        data: [error.data],
      });
      const loaderrorResult: any = await this.executeCtrlEventLogic('onloaderror', errorUIDataParam);
      if (loaderrorResult && loaderrorResult?.hasOwnProperty('srfret') && !loaderrorResult.srfret) {
        return { ret: false, data: error.data };
      }
      this.ctrlEvent(MobMDCtrlEvents.LOAD_ERROR, errorUIDataParam);
      if (showInfo) {
        App.getNoticeService().error(error.error?.message || '加载数据异常');
      }
      return { ret: false, data: error.data };
    }
  }
```

### 选择数据变化

在选择多数据视图（部件视图）中可以对数据项进行选择。

```ts
  public selectionChange(selections: any[]) {
    this.selections = selections;
    const UIDataParams = this.getUIDataParam();
    Object.assign(UIDataParams, { data: this.selections });
    this.ctrlEvent(MobMDCtrlEvents.SELECT_CHANGE, UIDataParams);
  }
```

### 行选中

```ts
  public onRowSelect(item: any, event: MouseEvent) {
	  const selectUIParam: IUIDataParam = this.getUIDataParam();
      Object.assign(selectUIParam, { data: [item], event: event });
      this.ctrlEvent(MobMDCtrlEvents.ROW_SELECTED, selectUIParam);
  }
```

### 上拉加载更多

用户上拉多数据部件时，UI层调用控制器相关逻辑。控制器根据当前部件配置整合查询参数，随后调用load行为加载数据并整合到原业务数据中，同时将处理结果返回给UI层。

```ts
  public async loadBottom(): Promise<ICtrlActionResult> {
    if (!this.needLoadMore) {
      return { ret: false, data: this.items };
    }
    this.curPage++;
    const params = {};
    if (this.viewParam) {
      Object.assign(params, this.viewParam);
    }
    Object.assign(params, { page: this.curPage, size: this.limit });
    return await this.load(params, { type: 'loadMore' }, this.showBusyIndicator, false);
  }
```

## UI层

### 绘制

#### 绘制主内容

根据部件样式绘制列表样式或者是图标样式。

```tsx
  public renderMDCtrlContent() {
    if (this.c.items && this.c.items.length > 0) {
      this.initItemRef(this.c.items);
      if (this.c.controlInstance.controlStyle == 'ICONVIEW') {
        return this.renderMdctrlItemsIconView();
      } else {
        return this.renderMdctrlItemsListView();
      }
    }
  }
```

#### 绘制选择列表

```tsx
  public renderSelectMDCtrl() {
    if (!this.c.isMultiple) {
      // 单选列表
      return this.renderSimpleList();
    } else {
      // 多选列表
      return this.renderMultipleList();
    }
  }
```

#### 绘制有分组

```tsx
  public renderHaveGroup() {
    if (this.c.controlInstance.controlStyle == 'ICONVIEW') {
      return (
        <div class='iconview-group'>
          {this.c.groupData.map((data: any) => {
            return (
              <div class='iconview-group-item'>
                <div class='group-title'>{data.text}</div>
                <div class='group-content'>
                  {data.items &&
                    data.items.length > 0 &&
                    data.items.map((item: any, index: number) => {
                      return this.renderMDCtrlItem(item, index);
                    })}
                </div>
              </div>
            );
          })}
        </div>
      );
    } else {
      return this.c.groupData.map((data: any) => {
        return (
          <div class='listview-group'>
            <ion-item-group>
              <ion-item-divider>
                <ion-label>{data.text}</ion-label>
              </ion-item-divider>
              {data.items &&
                data.items.length > 0 &&
                data.items.map((item: any, index: number) => {
                  return this.renderMDCtrlItem(item, index);
                })}
            </ion-item-group>
          </div>
        );
      });
    }
  }
```

#### 绘制无分组

```tsx
  public renderNoGroup() {
    if (this.c.controlInstance.controlStyle == 'ICONVIEW') {
      return (
        <div class='iconview-nogroup'>
          {this.c.items.map((item: any, index: number) => {
            return this.renderMDCtrlItem(item, index);
          })}
        </div>
      );
    } else {
      return this.c.items.map((item: any, index: number) => {
        return this.renderMDCtrlItem(item, index);
      });
    }
  }
```

#### 绘制项

如果配置有项布局面板则绘制项布局面板，否则走默认绘制，并且在列表样式上支持绘制左右滑动界面行为。

```tsx
  public renderMDCtrlItem(item: any, index: number) {
    if (this.c.controlInstance.controlStyle == 'ICONVIEW') {
      return (
        <div ref={this.refsMap.get(item.srfkey)} class='icon-item'>
          {this.c.controlInstance.getItemPSLayoutPanel()
            ? this.renderItemForLayoutPanel(item)
            : this.renderItemForDefault(item, index)}
        </div>
      );
    } else {
      return (
        <ion-item-sliding ref={this.refsMap.get(item.srfkey)} class='list-item' onIonDrag={this.ionDrag.bind(this)}>
          {this.renderListItemAction(item)}
          {this.c.controlInstance.getItemPSLayoutPanel()
            ? this.renderItemForLayoutPanel(item)
            : this.renderItemForDefault(item, index)}
        </ion-item-sliding>
      );
    }
  }
```

#### 绘制触底加载

```tsx
  public renderBottomRefresh() {
    return (
      <ion-infinite-scroll
        onIonInfinite={(event: any) => {
          this.loadMore(event);
        }}
        threshold='100px'
        disabled={this.loadMoreDisabled.value}
      >
        <ion-infinite-scroll-content loading-spinner='bubbles' loading-text={this.$tl('share.loading','加载中···')}></ion-infinite-scroll-content>
      </ion-infinite-scroll>
    );
  }
```

### 逻辑

#### 左右滑动行为

在列表样式多数据部件上配置有左右滑动界面行为组时会去绘制左右滑动，当用户左右滑动列表项时会触发该方法执行界面行为。

```ts
  public mdCtrlClick($event: any, detail: any, item: any): void {
    $event.stopPropagation();
    if (this.c.handleActionClick && this.c.handleActionClick instanceof Function) {
      this.c.handleActionClick(item, $event, detail);
    }
    this.closeSlidings(item);
  }
```

#### 多选列表选中值改变

```ts
  public multipleChange($event: any) {
    const { detail } = $event;
    if (!detail) {
      return;
    }
    const { value } = detail;
    const selections: any[] = [...this.c.selections];
    const selectItem = this.c.items.find((item: any) => {
      return Object.is(item.srfkey, value);
    });
    if (selectItem) {
      if (detail.checked) {
        selections.push(selectItem);
      } else {
        const index = selections.findIndex((i: any) => {
          return i.srfkey === selectItem.srfkey;
        });
        if (index != -1) {
          selections.splice(index, 1);
        }
      }
    }
    this.c.selectionChange(selections);
  }
```

#### 单选列表值改变

```ts
  public simpleChange($event: any) {
    const { detail } = $event;
    if (!detail) {
      return;
    }
    const { value } = detail;
    const selections: any[] = [];
    const selectItem = this.c.items.find((item: any) => {
      return Object.is(item.srfkey, value);
    });
    selections.push(selectItem);
    this.c.selectionChange(selections);
  }
```

#### 加载更多

当用户滑动到底部时会触发该方法去加载更多数据。

```ts
  public loadMore(event: any) {
    if (this.c.needLoadMore) {
      if (this.c.loadBottom && this.c.loadBottom instanceof Function) {
        this.c
          .loadBottom()
          .then((response: any) => {
            event.target.complete();
          })
          .catch((error: any) => {
            event.target.complete();
          });
      }
    } else {
      this.loadMoreDisabled.value = true;
    }
  }
```
::: tip 提示

实体移动端多数据部件控制器继承多数据部件控制器基类，因此此处逻辑只是该部件特有UI。

多数据部件基类UI逻辑参见 [多数据部件 > UI层](./md-ctrl-base.md#ui层)

**参考文件**

packages\ibz-ui-ionic\src\components\widget\app-mob-mdctrl.tsx

:::
