# 实体移动端图表部件

数据图表用来呈现数据的变化规律等，您可以通过图表定义、坐标定义以及数据序列的定义来完成图表的部件。

<component-iframe router="iframe/widget/app-mob-chart" />

## 控制器

### 部件初始化

| <div style="width: 213px;text-align: left;">逻辑</div> | <div style="width: 213px;text-align: left;">方法名</div> | <div style="width: 213px;text-align: left;">说明</div> |
| ------------------------------------------------------ | -------------------------------------------------------- | ------------------------------------------------------ |
| 初始化图表配置                                         | initChartBaseOPtion                                      | 根据模型初始化图表配置                                 |

#### 初始化图表配置

根据模型初始化图表配置。

```ts
  private initChartBaseOPtion() {
    this.chartBaseOPtion = new Function('return {' + this.controlInstance?.baseOptionJOString + '}')();
  }
```

#### 部件初始化

监听通知并执行行为。

```ts
  public ctrlInit() {
    super.ctrlInit();
    if (this.viewState) {
      this.viewStateEvent = this.viewState.subscribe(({ tag, action, data }: IViewStateParam) => {
        if (!Object.is(tag, this.name)) {
          return;
        }
        if (Object.is('load', action)) {
          this.load(data);
        }
        if (Object.is('search', action)) {
          this.load(data);
        }
        if (Object.is('refresh', action)) {
          this.refresh(data);
        }
      });
    }
  }
```

::: tip 提示

实体移动端图表部件控制器继承多数据部件控制器基类，因此此处逻辑只是该部件特有初始化逻辑。

多数据部件基类初始化逻辑参见 [多数据部件 > 控制器 > 部件初始化](./md-ctrl-base.md#部件初始化)

:::

## UI层

### 绘制

```tsx
  render() {
	......
    return (
      <div class={{ ...this.classNames }} style={chartControlstyle}>
        {this.renderPullDownRefresh()}
        {this.c.isNoData ? this.renderNoData() : <div class='echarts' id={this.c.chartId}></div>}
      </div>
    );
  }
```

### 逻辑

#### 初始化序列模型

图表支持区域图(Area)，条形图(Bar)，K线图(Candlestick)，折线图(Line)，饼图(Pie)，雷达图(Radar)，散点图(Scatter)，柱状图(Column)，漏斗图(Funnel)，自定义(Custom) 等序列类型。在初始化图表部件时初始化序列模型并创建对应序列模型的控制器。

```ts
  private async initSeriesModel() {
    if (!this.c.controlInstance.getPSDEChartSerieses()) {
      return;
    }
    for (let index = 0; index < (this.c.controlInstance.getPSDEChartSerieses() as any)?.length; index++) {
      const series: IPSDEChartSeries = (this.c.controlInstance.getPSDEChartSerieses() as any)[index];
      if (series) {
        this.initChartSeries(await this.getSeriesModelParam(series), series);
      }
    }
  }
```

序列模型有9种：折线型(ChartLineSeries)，漏斗型(ChartFunnelSeries)，饼型(ChartPieSeries)，柱型(ChartBarSeries)，雷达型(ChartRadarSeries)，散点型(ChartScatterSeries)，仪表盘(ChartGaugeSeries)，K线图(ChartCandlestickSeries)，和自定义(CharCustomSeries)。前6种模型发布信息包含：发布序列名称，序列标识属性，分类代码表信息，值属性，索引对象，图表数据集对象信息，用户自定义参数信息(标签：label,标记：labelLine,样式：itemStyle,重点：emphasis)，结果集行列模式，基础配置Json内容等。参数格式为**ECX.xxx={yyy}**，比如：**ECX.label={show: true,position: 'outside'}** 。

#### 初始化图表参数

平台配置的图表标题，图例，绘图网格，雷达图坐标系，坐标轴(X轴和Y轴)，提示框，图表数据集，序列等参数将会发布到初始化图表所需参数chartOption中。

```ts
  /**
   * @description 初始化图表参数
   * @private
   * @memberof MobChartCtrlController
   */
  private initChartOption() {
    const series: any = [];
    // 填充series
    const indicator: any = [];
    this.c.controlInstance.getPSDEChartSerieses()?.forEach((_series: IPSDEChartSeries) => {
      series.push(this.fillSeries(_series, indicator));
    });
    // 填充xAxis
    const xAxis: any = [];
    (this.c.controlInstance as any).getPSChartXAxises()?.forEach((_xAxis: IPSChartXAxis) => {
      xAxis.push(this.fillAxis(_xAxis));
    });
    // 填充yAxis
    const yAxis: any = [];
    (this.c.controlInstance as any).getPSChartYAxises()?.forEach((_yAxis: IPSChartYAxis) => {
      yAxis.push(this.fillAxis(_yAxis));
    });
    // 填充grid
    const grid: any = [];
    (this.c.controlInstance as any).getPSChartGrids()?.forEach((_grid: IPSDEChartGrid) => {
      grid.push({
        ...(_grid.baseOptionJOString ? new Function('return {' + _grid.baseOptionJOString + '}')() : {}),
      });
    });
    // chartOption参数
    const opt = {
      tooltip: { show: true },
      dataset: [],
      series: series,
      xAxis: xAxis,
      yAxis: yAxis,
      // grid: grid,
    };
    this.fillTitleOption(opt);
    this.fillLegendOption(opt);
    this.registerMap();
    // 合并chartOption
    Object.assign(this.c.chartOption, opt);
    // 雷达图特殊参数
    Object.assign(this.c.chartOption, { radar: { indicator } });
  }
```

#### 初始化图表用户自定义参数

用户扩展中的自定义参数将会发布到图表自定义参数集合chartUserParams中。其中自定义参数格式为 **EC.xxx = yyy** ,**xxx**为想要自定义的图表参数字段名，**yyy**为自定义参数；如自定义图例距标题高为25，参数格式为**EC.legend={ top: "25px" }**。

```ts
  /**
   * @description 初始化图表用户自定义参数
   * @private
   * @memberof MobChartCtrlController
   */
  private initChartUserParams() {
    this.fillUserParam(this.c.controlInstance, this.c.chartUserParams, 'EC');
  }
  
  private fillUserParam(param: any, opts: any, tag: string) {
    if (!param.userParams) {
      return;
    }
    const userParam = param.userParams;
    switch (tag) {
      case 'ECX':
        if (userParam['ECX.label']) {
          opts['label'] = eval('(' + userParam['ECX.label'] + ')');
        }
        if (userParam['ECX.labelLine']) {
          opts['labelLine'] = eval('(' + userParam['ECX.labelLine'] + ')');
        }
        if (userParam['ECX.itemStyle']) {
          opts['itemStyle'] = eval('(' + userParam['ECX.itemStyle'] + ')');
        }
        if (userParam['ECX.emphasis']) {
          opts['emphasis'] = eval('(' + userParam['ECX.emphasis'] + ')');
        }
        for (const key in userParam) {
          if (Object.prototype.hasOwnProperty.call(userParam, key)) {
            if (key.indexOf('EC.') != -1) {
              const value = userParam[key].trim();
              opts[key.replace('EC.', '')] = value;
            }
          }
        }
        break;
      case 'EC':
        for (const key in userParam) {
          if (Object.prototype.hasOwnProperty.call(userParam, key)) {
            if (key.indexOf('EC.') != -1) {
              const value = userParam[key].trim();
              opts[key.replace('EC.', '')] = this.isJson(value)
                ? this.deepJsonParseFun(JSON.parse(value))
                : this.isArray(value)
                ? eval(value)
                : value;
            }
          }
        }
        break;
      default:
        break;
    }
  }  
```

### 实体数据集转化为图表数据集

将查询回来的实体数据集转化为图表数据集，最后使用图表数据集去绘制图表。

```ts
  private async transformToBasicChartSetData(data: any, callback: Function) {
    if (!data || !Array.isArray(data) || data.length === 0) {
      this.c.isNoData = true;
      if (this.c.myChart) {
        this.c.myChart.dispose();
        this.c.myChart = undefined;
      }
      return;
    }
    this.c.isNoData = false;
    //获取代码表值
    const allCodeList: any = await this.getChartAllCodeList();
    if (Object.values(this.c.seriesModel).length > 0) {
      Object.values(this.c.seriesModel).forEach((singleSeries: any, index: number) => {
        // 值属性为srfcount设置{srfcount:1}到data
        const valueField = singleSeries.dataSetFields.find((datasetField: any) => {
          return datasetField.name === singleSeries.valueField;
        });
        if (valueField && valueField.name && Object.is(valueField.name, 'srfcount')) {
          data.forEach((singleData: any) => {
            Object.assign(singleData, { srfcount: 1 });
          });
        }
        // 分组属性
        const groupField = singleSeries.dataSetFields.find((datasetField: any) => {
          return datasetField.name === singleSeries.categorField;
        });
        const tempChartSetData: Array<any> = [];
        let tempSeriesValues: Map<string, any> = new Map();
        data.forEach((item: any) => {
          const tempChartSetDataItem: any = {};
          // 序列属性不存在
          if (!singleSeries.seriesIdField) {
            Object.assign(tempChartSetDataItem, { name: singleSeries.name });
            if (singleSeries.dataSetFields && singleSeries.dataSetFields.length > 0) {
              singleSeries.dataSetFields.forEach((singleDataSetField: any) => {
                this.handleSingleDataSetField(item, singleDataSetField, allCodeList, tempChartSetDataItem, groupField);
              });
            }
          } else {
            // 序列属性存在时
            // 序列代码表存在时,翻译tempSeriesValues的键值对
            if (singleSeries.seriesCodeList) {
              const seriesCodeList: Map<string, any> = allCodeList.get(singleSeries.seriesCodeList.tag);
              const tempSeriesValueItem = tempSeriesValues.get(seriesCodeList.get(item[singleSeries.seriesIdField]));
              if (!tempSeriesValueItem) {
                tempSeriesValues.set(
                  seriesCodeList.get(item[singleSeries.seriesIdField]),
                  seriesCodeList.get(item[singleSeries.seriesNameField]),
                );
              }
            } else {
              const tempSeriesValueItem = tempSeriesValues.get(item[singleSeries.seriesIdField]);
              if (!tempSeriesValueItem) {
                tempSeriesValues.set(item[singleSeries.seriesIdField], item[singleSeries.seriesNameField]);
              }
            }
            Object.assign(tempChartSetDataItem, { name: item[singleSeries.seriesIdField] });
            if (singleSeries.dataSetFields && singleSeries.dataSetFields.length > 0) {
              singleSeries.dataSetFields.forEach((singleDataSetField: any) => {
                this.handleSingleDataSetField(item, singleDataSetField, allCodeList, tempChartSetDataItem, groupField);
              });
            }
          }
          tempChartSetData.push(tempChartSetDataItem);
        });
        // 补全数据集合
        this.completeDataSet(tempChartSetData, singleSeries, allCodeList);
        // 序列代码表存在时,补全序列
        if (singleSeries.seriesCodeList) {
          const seriesCodeList: Map<string, any> = allCodeList.get(singleSeries.seriesCodeList.tag);
          tempSeriesValues = new Map();
          seriesCodeList.forEach((item: any) => {
            tempSeriesValues.set(item, item);
          });
        }
        singleSeries.seriesValues = [...tempSeriesValues.keys()];
        const tempSeriesMapObj: any = {};
        tempSeriesValues.forEach((value: any, key: any) => {
          tempSeriesMapObj[key] = value;
        });
        singleSeries.seriesMap = tempSeriesMapObj;
        const callbackFunction: any = index === Object.values(this.c.seriesModel).length - 1 ? callback : null;
        this.transformToChartSeriesDataSet(tempChartSetData, singleSeries, callbackFunction, allCodeList);
      });
    }
  }
```

### 绘制图表

echarts所需参数转换完成后通过setOption方法绘制图表。

```ts
  /**
   * @description 绘制图表
   * @private
   * @memberof MobChartCtrlController
   */
  private drawCharts() {
    if (!this.c.myChart) {
      const element: any = document.getElementById(this.c.chartId);
      if (element) {
        this.c.myChart = init(element);
      }
    }
    //判断刷新时dom是否存在
    if (!Object.is(this.c.myChart._dom.offsetHeight, 0) && !Object.is(this.c.myChart._dom.offsetWidth, 0)) {
      const _chartOption = this.handleChartOPtion();
      this.c.chartRenderOption = { ..._chartOption };
      if (this.c.myChart) {
        this.c.myChart.setOption(_chartOption);
        this.onChartEvents();
        this.handleDefaultSelect();
        this.c.myChart.resize();
      }
    }
  }
```

#### 图表点击

图表点击之后，抛出onSelectionChange事件。

```ts
  private onChartClick(event: any) {
    if (!event || !event.name) {
      return;
    }
    const data: any = event.data;
    Object.assign(data, { _chartName: event.seriesId });
    this.c.selections = [data];
    this.c.ctrlEvent(MobChartEvents.SELECT_CHANGE, this.c.selections);
  }
```

::: tip 提示

实体移动端图表部件控制器继承多数据部件基类，因此此处逻辑只是该部件特有UI。

多数据部件基类初始化逻辑参见 [多数据部件 > UI层](./md-ctrl-base.md#ui层)

**参考文件**

packages\ibz-ui-ionic\src\components\widget\app-mob-chart.tsx
:::