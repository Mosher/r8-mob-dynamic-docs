# 实体表单
由输入框、选择器、单选框、多选框等控件组成，用以收集、校验、提交数据。
<component-iframe router="iframe/widget/app-mob-form" />

## 控制器

### 部件初始化

| 逻辑                     | 方法名           | 说明                                                         |
| ------------------------ | ---------------- | ------------------------------------------------------------ |
| 初始化表单数据           | initData         | 根据表单项模型进行表单数据data的初始化                       |
| 初始化表单成员运行时模型 | initDetailsModel | 根据表单成员构建其相应的表单成员控制器                       |
| 初始化值规则             | initRules        | 实体表单部件支持值规则，包括系统值规则、属性值规则、非空值规则、数据类型值规则等 |
| 初始化关系界面           | initFormDRPart   | 实体表单部件支持配置关系界面，初始化时根据表单成员运行时模型填充表单关系界面状态Map |

#### 初始化表单数据

根据表单项模型进行表单数据data的初始化。

```ts
  protected initData() {
    this.controlInstance.getPSDEFormItems()?.forEach((formItem: IPSDEFormItem) => {
      this.data[formItem.id] = null;
    });
  }
```

#### 初始化表单成员运行时模型

根据表单成员构建其相应的表单成员控制器。

```ts
  protected initDetailsModel() {
    ......
        switch (detail.detailType) {
          ......
          case 'TABPAGE':
            detailModel = new FormTabPageController(detailOpts);
            break;
          case 'FORMPAGE':
            detailModel = new FormPageController(detailOpts);
            break;
          case 'FORMPART':
            detailModel = new FormPartController(detailOpts);
            break;
          case 'DRUIPART':
            detailModel = new FormDruipartController(detailOpts);
            break;
         ......
        }
        this.detailsModel[detail.name] = detailModel;
   ......
  }
```

#### 初始化值规则

根据模型数据初始化值规则，包括系统值规则、属性值规则、非空值规则、数据类型值规则等。

```ts
  protected initRules() {
    // 先初始化系统值规则和属性值规则
    const staticRules: any = {};
    const allFormItemVRs = this.controlInstance.getPSDEFormItemVRs();
    allFormItemVRs?.forEach((item: IPSDEFormItemVR) => {
     ......
      // 系统值规则
      if (valueRuleType == 'SYSVALUERULE' && sysRule) {
        // 正则值规则
        if (sysRule.ruleType == 'REG') {
          staticRules[formItemName].push({
            pattern: new RegExp(sysRule.regExCode),
            message: sysRule.ruleInfo,
            trigger: ['change', 'blur'],
          });
          // 脚本值规则
        } else if (sysRule.ruleType == 'SCRIPT') {
          staticRules[formItemName].push({
            validator: (rule: any, value: any, callback: any) => {
              // 空值时不校验
              if (Util.isEmpty(this.data[formItemName])) {
                return true;
              }
              const source = this.data;
              try {
                eval(sysRule.scriptCode);
              } catch (error: any) {
                App.getNoticeService().error(error?.message);
              }
              return true;
            },
            trigger: ['change', 'blur'],
          });
        }
        // 属性值规则
      } else if (valueRuleType == 'DEFVALUERULE' && deRule) {
        // 有值项的情况，校验值项的值
        const formItem = this.controlInstance.findPSDEFormItem(formItemName);
        const valueName = formItem?.valueItemName || formItemName;
        staticRules[formItemName].push({
          validator: (rule: any, value: any, callback: any, source: any) => {
            // 空值时不校验
            if (Util.isEmpty(this.data[valueName])) {
              return true;
            }
            const { isPast, infoMessage } = Verify.verifyDeRules(
              valueName,
              this.data,
              deRule.getPSDEFVRGroupCondition(),
            );
            if (!isPast) {
              callback(new Error(infoMessage || deRule.ruleInfo));
            }
            return true;
          },
          trigger: ['change', 'blur'],
        });
      }
    });
    // 初始化非空值规则和数据类型值规则
    this.rules = {};
    const allFormItems = ModelTool.getAllFormItems(this.controlInstance) as IPSDEEditFormItem[];
    if (allFormItems && allFormItems.length > 0) {
      for (const detail of allFormItems) {
        if (detail.detailType == 'FORMITEM' && detail.getPSEditor()?.editorType != 'HIDDEN' && !detail.compositeItem) {
          const otherRules = staticRules[detail.name] || [];
          const editorRules = Verify.buildVerConditions(detail.getPSEditor());
          this.rules[detail.name] = [
            {
              validator: (rule: any, value: any, callback: any) => {
                return !(
                  this.detailsModel[detail.name].required &&
                  (value === null || value === undefined || value === '')
                );
              },
              message: `${detail.caption || detail.name} 必须填写`,
            },
            // 表单值规则
            ...otherRules,
            // 编辑器基础值规则
            ...editorRules,
          ];
        }
  	......
  }
```

#### 初始化关系界面

实体表单部件支持配置关系界面，初始化时根据表单成员运行时模型填充表单关系界面状态Map。

```ts
  protected initFormDRPart() {
    if (this.detailsModel && Object.keys(this.detailsModel).length > 0) {
      Object.keys(this.detailsModel).forEach((item: string) => {
        if (this.detailsModel[item] instanceof FormDruipartController) {
          this.formDRStateMap.set(item, false);
        }
      });
    }
  }
```

### 初始化部件

监听通知并执行相应行为。

```ts
  public ctrlInit(args?: any) {
    super.ctrlInit(args);
    this.formState = new Subject();
    if (this.isAutoLoad) {
      this.autoLoad({}, { srfkey: this.context[this.appDeCodeName.toLowerCase()] });
    }
    if (this.viewState) {
      this.viewStateEvent = this.viewState.subscribe(({ tag, action, data = {} }: IViewStateParam) => {
        if (!Object.is(tag, this.name)) {
          return;
        }
        if (Object.is('loadDraft', action)) {
          this.loadDraft(data);
        }
        if (Object.is('load', action)) {
          this.load(data);
        }
        if (Object.is('save', action)) {
          this.save(data);
        }
        if (Object.is('remove', action)) {
          this.remove(data);
        }
        if (Object.is('panelAction', action)) {
          this.panelAction(data);
        }
      });
    }
  }
```

::: tip 提示

实体表单控制器继承部件控制器基类，因此此处逻辑只是实体表单特有初始化逻辑。

基类初始化逻辑参见 [部件 > 控制器 > 部件初始化](../app-ctrl-base.md#部件初始化)

:::

### 部件挂载

::: tip 提示

实体表单控制器继承部件控制器基类，因此此处逻辑只是实体表单特有挂载逻辑。

基类初始化逻辑参见 [部件 > 控制器 > 部件初始化](../app-ctrl-base.md#部件挂载)

:::

### 表单项值变更

当用户更改表单数据时触发表单项值更新逻辑，同时会执行表单值规则校验、重置表单项、表单逻辑。并且如果当前表单设置了自动保存，则会进行数据的保存。

```ts
  public onFormItemValueChange(event: IParam): void {
    if (!(event?.name && event.name in this.data)) {
      return;
    }
    this.data[event.name] = event.value;
    this.validate(event.name);
    this.resetFormData({ name: event.name });
    this.formLogic({ name: event.name });
    if (this.isAutoSave) {
      this.save();
    }
  }
```

### 表单值规则校验

提交表单数据和修改表单数据时对表单数据进行数据校验。可分为系统值规则和属性值规则，其中系统值规则已支持正则式和脚本，属性值规则已支持常规规则、正则式规则、数值范围规则、字符长度规则、系统值规则和规则组（前5种类型的值规则的组合）。

```ts
  protected validate(name: string): Promise<any> {
    return new Promise((resolve, reject) => {
      Util.validateItem(name, this.data, this.rules)
        .then(() => {
          this.detailsModel[name].setError(null);
          resolve(true);
        })
        .catch(({ errors, fields }: any) => {
          this.detailsModel[name].setError(errors?.[0].message);
          resolve(false);
        });
    });
  }
```

::: tip 提示

表单校验使用第三方库实现，更多高级用法可参考 [async-validator](https://github.com/yiminghe/async-validator)

:::

### 重置表单项

如果某个表单项配置的重置项为，当重置项值发生变更时，会将该表单项值置空。

```ts
  protected resetFormData({ name }: { name: string }): void {
    const formItems: IPSDEEditFormItem[] = ModelTool.getAllFormItems(this.controlInstance);
    if (formItems && formItems.length > 0) {
      for (const item of formItems) {
        if (item.resetItemName && item.resetItemName == name) {
          this.onFormItemValueChange({ name: item.name, value: null });
          if (item.valueItemName) {
            this.onFormItemValueChange({ name: item.valueItemName, value: null });
          }
        }
      }
    }
  }
```

### 表单逻辑

表单数据变化时会触发表单逻辑，对表单动态逻辑和表单更新项做统一管理。

根据表单数据状态对表单项进行动态控制，共三种动态控制模式

- 动态显示（控制表单成员的显示与隐藏）
- 动态启用（控制表单项成员是否可编辑）
- 动态空输入（控制表单项是否为必填）

#### 核心逻辑

在表单初始化时，构建了表单成员运行时模型，每个模型对应一个表单成员控制器用于控制表单成员的动态显示、启用、空输入。当表单数据发生变化时，触发表单动态逻辑（formLogic函数）动态地修改运行时模型，驱动UI变化。并且如果该表单项配置有表单更新项，则会触发表单更新项的逻辑。

```ts
  protected async formLogic({ name }: { name: string }) {
    const allFormDetails: IPSDEFormDetail[] = ModelTool.getAllFormDetails(this.controlInstance);
    // 表单动态逻辑
    allFormDetails?.forEach((detail: IPSDEFormDetail) => {
      detail.getPSDEFDGroupLogics()?.forEach((logic: IPSDEFDCatGroupLogic) => {
        const relatedNames = logic.getRelatedDetailNames() || [];
        if (Object.is(name, '') || relatedNames.indexOf(name) != -1) {
          const ret = this.verifyGroupLogic(this.data, logic);
          switch (logic.logicCat) {
            // 动态空输入，不满足则必填
            case 'ITEMBLANK':
              this.detailsModel[detail.name].required = !ret;
              break;
            // 动态启用，满足则启用
            case 'ITEMENABLE':
              this.detailsModel[detail.name].setDisabled(!ret);
              break;
            // 动态显示，满足则显示
            case 'PANELVISIBLE':
              this.detailsModel[detail.name].setVisible(ret);
              break;
          }
        }
      });
    });
    // 表单项更新
    const formDetail: IPSDEFormItem = ModelTool.getFormDetailByName(this.controlInstance, name);
    const formItemUpdate: IPSDEFormItemUpdate | null = formDetail?.getPSDEFormItemUpdate?.();
    if (formItemUpdate) {
      if (await this.checkItem(formDetail.name)) {
        if (formItemUpdate.customCode) {
          if (formItemUpdate.scriptCode) {
            const context = Util.deepCopy(this.context);
            const viewparams = Util.deepCopy(this.viewParam);
            const data = this.data;
            eval(formItemUpdate.scriptCode);
          }
        } else {
          const showBusyIndicator = formItemUpdate.showBusyIndicator;
          const getPSAppDEMethod = formItemUpdate.getPSAppDEMethod();
          const getPSDEFIUpdateDetails = formItemUpdate.getPSDEFIUpdateDetails();
          const details: string[] = [];
          getPSDEFIUpdateDetails?.forEach((item: IPSDEFIUpdateDetail) => {
            details.push(item.name);
          });
          this.updateFormItems(getPSAppDEMethod?.codeName as string, this.data, details, showBusyIndicator);
        }
      }
    }
  }
```

### 默认值

在表单填充数据时，会根据传入的行为执行新建默认值或更新默认值。

目前已支持的更新值类型有

- 网页请求（CONTEXT）
- 用户全局对象（SESSION）
- 当前应用数据（APPDATA）
- 当前操作用户（OPERATORNAME）
- 当前操作用户（OPERATOR）
- 当前时间（CURTIME）
- 数据对象属性（PARAM）

并且更新默认值多一种置空当前值（RESET）。

如没设置更新值类型则当直接值处理。

```ts
  protected fillForm(data: any = {}, action: string): void {
    Object.keys(data).forEach((name: string) => {
      if (this.data.hasOwnProperty(name)) {
        this.data[name] = data[name];
      }
    });
    if (Object.is(action, 'loadDraft')) {
      this.createDefault();
    }
    if (Object.is(action, 'load')) {
      this.updateDefault();
    }
  }
```

### 后台值校验

表单部件处理响应时对后台值校验错误进行处理，将值校验错误信息合并到detailsModel的对应表单项的error上，最后由UI层对错误信息进行呈现。

```ts
    public onControlResponse(action: string, response: any) {
		......
        if (response && response.status && response.status != 200 && response.data) {
            const data: any = response.data;
            if (data.code && Object.is(AppErrorCode.INPUTERROR, data.code) && data.details?.length > 0) {
                let errorMsg: string = '';
                data.details.forEach((detail: any) => {
                    if (!Object.is(EntityFieldErrorCode.ERROR_OK, detail.fielderrortype) && detail.fieldname) {
                        const tempFormItem: any = this.findFormItemByField(detail.fieldname);
                        if (tempFormItem) {
                            Object.assign(this.detailsModel[tempFormItem.name], { error: new String(detail.fielderrorinfo ? detail.fielderrorinfo : data.message) });
                        } else {
                            errorMsg += `${detail.fieldlogicname}${detail.fielderrorinfo ? detail.fielderrorinfo : data.message}<br/>`;
                        }
                    }
                })
                response.data.message = errorMsg ? errorMsg : '填写信息有误，请检查';
            }
        }
    }
```

### 处理编辑器事件

编辑器抛出onValueChange事件时，更新表单项值。

```ts
  public handleEditorEvent(args: IEditorEventParam): void {
    const { editorName, action, data } = args;
    if (Object.is(action, AppEditorEvents.VALUE_CHANGE)) {
      this.onFormItemValueChange(data as IParam);
    }
  }
```

## UI层

### 绘制

#### 表单内容绘制

```tsx
  public renderFormContent() {
    const { noTabHeader } = this.c.controlInstance;
    const formPages = this.c.controlInstance.getPSDEFormPages();
    if (!this.currentPage.value && formPages) {
      this.currentPage.value = formPages[0].codeName;
    }
    if (formPages && formPages.length > 0) {
      if (noTabHeader) {
        return formPages.map((item: IPSDEFormPage, index: number) => {
          return this.renderFormPage(item, index);
        });
      } else {
        const controlName = this.c.controlInstance.name;
        return (
          <div class={`${controlName}-container`}>
            <div class={`${controlName}-header`}>
              <ion-segment value={this.currentPage.value} onIonChange={($event: any) => this.segmentChanged($event)}>
                {formPages.map((item: IPSDEFormPage, index: number) => {
                  // 表单分页标题样式
                  const pageClass = item.getLabelPSSysCss()?.cssName;
                  return (
                    <ion-segment-button value={item.codeName} class={pageClass}>
                      <ion-label>
                        {item.getPSSysImage() && <app-icon icon={item.getPSSysImage()}></app-icon>}
                        {this.$tl(item?.getCapPSLanguageRes()?.lanResTag, item.caption)}
                      </ion-label>
                    </ion-segment-button>
                  );
                })}
              </ion-segment>
            </div>
            <div class={`${controlName}-body`}>
              {formPages.map((item: IPSDEFormPage, index: number) => {
                return this.renderFormPage(item, index);
              })}
            </div>
          </div>
        );
      }
    }
  }
```

#### 成员绘制

表单只支持表单分页，表单分组，表单项，关系界面和表单按钮的绘制。

```tsx
  public renderByDetailType(modelJson: any, index: number) {
    if (modelJson.getPSSysPFPlugin()?.pluginCode) {
      const ctrlItemPluginInstance = App.getPluginService().getCtrlItemPluginByTag(
        modelJson.getPSSysPFPlugin()?.pluginCode,
      );
      if (ctrlItemPluginInstance) {
        return ctrlItemPluginInstance.renderItem(this.c.data[modelJson.name], this.c, this);
      }
    } else {
      switch (modelJson.detailType) {
        case 'FORMPAGE':
          return this.renderFormPage(modelJson as IPSDEFormPage, index);
        case 'GROUPPANEL':
          return this.renderGroupPanel(modelJson as IPSDEFormGroupPanel, index);
        case 'FORMITEM':
          return this.renderFormItem(modelJson as IPSDEFormItem, index);
        case 'DRUIPART':
          return this.renderDruipart(modelJson as IPSDEFormDRUIPart, index);
        case 'BUTTON':
          return this.renderButton(modelJson as IPSDEFormButton, index);
        default:
          LogUtil.log(`暂未实现 ${modelJson.detailType} 类型表单成员`);
      }
    }
  }
```

::: tip 提示

表单部件继承部件基类，因此此处逻辑只是表单部件的特有UI。

基类部件UI参见 [部件 > UI层](../app-ctrl-base.md#ui层)

**参考文件**

packages\ibz-ui-ionic\src\components\widget\app-mob-form\app-mob-form.tsx

:::