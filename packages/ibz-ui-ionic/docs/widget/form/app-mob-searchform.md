# 搜索表单
跟普通表单组成结构相同，由输入框、选择器、单选框等空间组成。嵌入视图中，用于过滤视图中的数据。在R8动态模板中支持默认展开搜索表单（直接呈现），点击过滤展开（抽屉打开）两种展示方式，并且也支持快速搜索表单（直接呈现）
<component-iframe router="iframe/widget/app-mob-searchform" />

## 控制器

### 部件初始化

::: tip 提示

搜索表单控制器继承表单控制器，因此此处逻辑只是搜索表单特有初始化逻辑。

基类初始化逻辑参见 [部件 > 实体表单 > 部件初始化](./app-mob-form.md#部件初始化)

:::

### 部件挂载

::: tip 提示

搜索表单控制器继承表单控制器，因此此处逻辑只是搜索表单特有挂载逻辑。

表单挂载逻辑参见 [部件 > 实体表单 > 部件挂载](./app-mob-form.md#部件挂载)

:::

### 表单项值变更

在处理完表单值变更逻辑之后抛出onValueChange事件。

```ts
  public onFormItemValueChange(event: { name: string; value: any }): void {
    super.onFormItemValueChange(event);
    this.ctrlEvent(MobSearchFormEvents.VALUE_CHANGE, this.data);
  }
```

::: tip 提示

表单项值变更逻辑参见 [部件 > 实体表单 > 表单项值变更](./app-mob-form.md#表单项值变更)

:::

### 重置

```ts
  public onReset() {
    this.ctrlEvent(MobSearchFormEvents.RESET, this.data);
  }
```

### 搜索

```ts
  public onSearch() {
    this.ctrlEvent(MobSearchFormEvents.SEARCH, this.data);
  }
```

## UI层

### 绘制

搜索表单有两种呈现方式：抽屉打开和直接呈现（快速搜索表单和默认打开时的呈现方式）。

#### 绘制直接呈现

```tsx
  renderQuickSearchForm(className: any, controlStyle: any) {
    return (
      <ion-header>
        <div class={className} style={controlStyle}>
          {this.renderFormContent()}
        </div>
      </ion-header>
    );
  }
```

#### 绘制抽屉打开

```tsx
  renderSearchForm(className: any, controlStyle: any) {
    return [
      <ion-header class={'searchForm-header'}>
        <ion-toolbar>
          <ion-buttons slot='end'>
            <ion-button onClick={() => this.openSearchForm()} id='searchForm'>
              {this.$tl('widget.mobsearchform.filter', '过滤')}
            </ion-button>
          </ion-buttons>
        </ion-toolbar>
      </ion-header>,
      <ion-menu side='end' menuId={this.uuid} contentId='searchForm'>
        <ion-content>
          <div class={className} style={controlStyle}>
            {this.renderFormContent()}
          </div>
        </ion-content>
        <ion-footer class={'searchForm-footer'}>
          <ion-toolbar>
            <ion-button size='small' onClick={() => this.onSearch()}>
            {this.$tl('widget.mobsearchform.search', '搜索')}
            </ion-button>
            <ion-button size='small' onClick={() => this.onReset()}>
            {this.$tl('widget.mobsearchform.reset', '重置')}
            </ion-button>
          </ion-toolbar>
        </ion-footer>
      </ion-menu>,
    ];
  }
```

#### 成员绘制

搜索表单只支持表单分页，表单分组，表单项的绘制。

```tsx
  public renderByDetailType(modelJson: any, index: number) {
    if (modelJson.getPSSysPFPlugin()?.pluginCode) {
      const ctrlItemPluginInstance = App.getPluginService().getCtrlItemPluginByTag(
        modelJson.getPSSysPFPlugin()?.pluginCode || '',
      );
      if (ctrlItemPluginInstance) {
        return ctrlItemPluginInstance.renderItem(this.c.data[modelJson.name], this.c, this);
      }
    } else {
      switch (modelJson.detailType) {
        case 'FORMPAGE':
          return this.renderFormPage(modelJson as IPSDEFormPage, index);
        case 'GROUPPANEL':
          return this.renderGroupPanel(modelJson as IPSDEFormGroupPanel, index);
        case 'FORMITEM':
          return this.renderFormItem(modelJson as IPSDEFormItem, index);
        default:
          LogUtil.log(`${this.$tl('share.notsupported', '暂未实现')} ${modelJson.detailType} ${this.$tl('widget.mobsearchform.detailtype', '类型表单成员')}`);
      }
    }
  }
```
::: tip 提示

搜索表单部件继承表单部件，因此此处逻辑只是搜索表单部件的特有UI。

表单部件UI逻辑参见 [部件 > 实体表单 > UI层](./app-mob-form.md#ui层)

**参考文件**

packages\ibz-ui-ionic\src\components\widget\app-mob-searchform.tsx

:::
