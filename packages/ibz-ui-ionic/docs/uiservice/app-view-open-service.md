# 视图打开服务
视图打开服务提供了视图的4种打开方式：模态打开，抽屉打开，路由打开，新标签页打开。当需要打开某视图时可以通过应用全局对象App来调用该服务来打开视图。

```ts
App.getOpenViewService()
```

## 核心逻辑

### 模态打开

```ts
  openModal(
    view: {
      viewComponent?: any | undefined;
      viewModel?: IParam;
      customClass?: string | undefined;
      customStyle?: IParam;
    },
    context?: IContext,
    navParam?: IParam,
    navDatas?: any,
    otherParam?: IParam,
  ): Subject<{ ret: boolean; datas?: IParam[] }> {
    return AppModalService.getInstance().openModal(view, context, navParam, navDatas, otherParam);
  }
```

### 抽屉打开

```ts
  openDrawer(
    view: {
      viewComponent?: any | undefined;
      viewModel?: IParam;
      customClass?: string | undefined;
      customStyle?: IParam;
    },
    context?: IContext,
    navParam?: IParam,
    navDatas?: any,
    otherParam?: IParam,
  ): Subject<{ ret: boolean; datas?: IParam[] }> {
    return AppDrawerService.getInstance().openDrawer(view, context, navParam, navDatas, otherParam);
  }
```

### 路由打开

```ts
  public openView(path: string): void {
    if (path) {
      this.router.push(path);
    }
  }
```

### 浏览器新标签页

```ts
  public openPopupApp(url: string): void {
    window.open(url, '_blank');
  }
```

