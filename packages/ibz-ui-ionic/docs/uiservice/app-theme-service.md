# 主题服务
主题服务提供主题相关的操作服务，如设置主题，设置自定义配置等功能。R8Mob-Dynamic模板目前有两种预置主题：默认主题和灰暗主题，此外还可以通过主题组件来自定义主题。在应用初始化时会通过主题服务来初始化当前应用主题。主题服务可以通过应用全局对象App来调用。

```ts
App.getAppThemeService()
```

## 应用初始化

在创建应用实例对象时通过主题服务来初始化主题。

```ts
    /**
     * Creates an instance of App.
     * @memberof App
     */
    constructor() {
        super();
        this.initAppLanguage();
        this.initAppTheme();
        this.initPlatFormType();
    }
    public initAppTheme() {
        const appThemeService = AppThemeService.getInstance();
        appThemeService.initAppTheme();
    }
```

## 核心逻辑

### 初始化主题

```ts
  public initAppTheme(): { themeOptions: IParam[]; activeTheme: { isCustom: boolean; theme: string } } {
    const activeTheme = this.getActiveUITheme();
    this.htmlElement.classList.add(activeTheme.theme);
    return {
      themeOptions: this.getThemeOptions(),
      activeTheme: activeTheme,
    };
  }
```

### 获取主题配置

```ts
  public getThemeOptions(): IParam[] {
    const options: IParam[] = [];
    let backOptions: IParam[] = [];
    const backOptionsStr = localStorage.getItem('customUIThemeOptions');
    if (backOptionsStr) {
      backOptions = [...JSON.parse(backOptionsStr)];
    }
    this.themeOptions.forEach((group: IParam) => {
      const items: IParam[] = [];
      if (group.items && group.items.length > 0) {
        group.items.forEach((item: IParam) => {
          const key = `--ion-${group.name.toLowerCase()}-${item.name.toLowerCase()}`;
          const changeItem = backOptions.find((item: IParam) => key === item.key);
          if (changeItem) {
            this.setCustomOptions(changeItem);
          }
          if (group.name === 'color') {
            Object.assign(item, {
              key: key,
              default: changeItem ? changeItem.default : this.getCssVariablet(key),
              shade: changeItem ? changeItem.shade : this.getCssVariablet(`${key}-shade`),
              tint: changeItem ? changeItem.tint : this.getCssVariablet(`${key}-tint`),
              showChild: false,
              isChange: changeItem ? true : false,
            });
          } else {
            Object.assign(item, {
              key: key,
              default: changeItem ? changeItem.default : this.getCssVariablet(key),
              isChange: changeItem ? true : false,
            });
          }
          items.push(item);
        });
      }
      options.push({
        name: group.name,
        title: group.title,
        lanResTag: group.lanResTag,
        items: items,
      });
    });
    this.themeOptions = options;
    return options;
  }
```

### 切换主题

```ts
  public changeInternalTheme(oldVal: string, newVal: string, options: IParam[]) {
    const element = document.documentElement;
    if (oldVal) {
      element.classList.remove(oldVal);
    }
    if (newVal) {
      element.classList.add(newVal);
    }
    this.setActiveUITheme(newVal);
    this.resetAppTheme(options);
  }
```

### 自定义主题

```ts
  public applyCustomTheme(theme: string, options: IParam[]) {
    this.setActiveUITheme(theme, true);
    const changeOptions: IParam[] = this.findChangeOptions(options);
    if (changeOptions.length > 0) {
      localStorage.setItem('customUIThemeOptions', JSON.stringify(changeOptions));
    }
  }
```

### 重置主题

```ts
  public resetAppTheme(options: any) {
    options.forEach((option: IParam) => {
      (option.items || []).forEach((item: IParam) => {
        if (item.isChange) {
          item.isChange = false;
          this.removeCustomOptions(item);
          item.default = this.getCssVariablet(item.key);
          if (option.name == 'color') {
            item.shade = this.getCssVariablet(`${item.key}-shade`);
            item.tint = this.getCssVariablet(`${item.key}-tint`);
          }
        }
      });
    });
    this.removeLocalStorage('customUIThemeOptions');
  }
```

