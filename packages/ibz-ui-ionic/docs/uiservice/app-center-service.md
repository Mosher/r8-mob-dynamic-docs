# 应用中心服务
应用中心服务维护一个全局的数据状态管理对象，并可通过该对象来发出一个全局的状态通知，应用中心服务可通过应用全局对象App来调用。

```ts
App.getCenterService()
```

## 核心逻辑
### 获取消息中心

```ts
  public getMessageCenter(): Subject<{ name: string; action: string; data: IParam }> {
    return this.subject;
  }
```
### 发送通知

```ts
  public notifyMessage(name: string, action: string, data: IParam): void {
    this.subject.next({ name, action, data });
  }
```
## 应用场景

### 发送通知
在向导面板中执行完成行为时会发送一个全局的刷新行为消息。

```ts
  public doFinish() {
    ......
    const post: Promise<any> = this.ctrlService.finish(this.finishAction, tempContext, arg, this.showBusyIndicator);
    post.then((response: any) => {
      ......
        App.getCenterService().notifyMessage(this.appDeCodeName, 'appRefresh', data);
    })
  }
```
### 接收通知
在部件基类中监听应用中心服务发送的通知，符合条件的将执行刷新行为。

```ts
  public async controlInit(): Promise<boolean> {
    ......
    if (App.getCenterService().getMessageCenter()) {
      this.appStateEvent = App.getCenterService()
        .getMessageCenter()
        .subscribe(({ name, action, data }: { name: string; action: string; data: any }) => {
          if (!this.appDeCodeName || !Object.is(name, this.appDeCodeName)) {
            return;
          }
          if (Object.is(action, 'appRefresh')) {
            this.refresh(data);
          }
        });
    }
    return this.controlIsLoaded;
  }
```



