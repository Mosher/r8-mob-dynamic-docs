# 抽屉服务
抽屉服务主要用于创建一个抽屉组件，并将视图嵌入该模态组件之中。可以通过视图打开服务来创建一个抽屉。

```ts
App.getOpenViewService().openDrawer(
    {
      viewComponent: '',
      viewModel: {},
      viewPath: '',
      customClass: '',
      customStyle: {},
      placement: '',
    },
    navContext,
    navParam,
    navDatas[],
    otherParam{},
)
```

## 参数说明

| <div style="width: 335px;text-align: left;">参数名</div> | <div style="width: 335px;text-align: left;">说明</div>       |
| -------------------------------------------------------- | ------------------------------------------------------------ |
| viewComponent                                            | 视图组件名称                                                 |
| viewPath                                                 | 视图模型路径                                                 |
| viewModel                                                | 视图模型                                                     |
| customClass                                              | 自定义类名                                                   |
| customStyle                                              | 自定义样式                                                   |
| placement                                                | 抽屉打开方向，可选值有'DRAWER_LEFT' \| 'DRAWER_RIGHT'，默认左侧打开 |
| navContext                                               | 上下文                                                       |
| navParam                                                 | 视图参数                                                     |
| navDatas                                                 | 导航数据                                                     |
| otherParam                                               | 其他参数                                                     |

viewComponent（视图组件名称）和viewModel（视图模型）必须传其中一个，其余参数可不传。

## 核心逻辑
### 抽屉打开
```ts
  public openDrawer(
    view: {
      viewComponent?: string;
      viewModel?: IParam;
      viewPath?: string;
      customClass?: string;
      customStyle?: IParam;
      placement?: 'DRAWER_LEFT' | 'DRAWER_RIGHT';
    },
    navContext: any = {},
    navParam: any = {},
    navDatas: Array<any> = [],
    otherParam: any = {},
  ): Subject<any> {
    const subject: Subject<{ ret: boolean; datas?: IParam[] }> = new Subject<{ ret: boolean; datas?: IParam[] }>();
    try {
      const _navContext: any = {};
      Object.assign(_navContext, navContext);
      if (!view.viewComponent || !view.placement) {
        this.fillView(view);
      }
      const uuid = Util.createUUID();
      this.createVueExample(view, _navContext, navParam, navDatas, otherParam, uuid, subject);
      return subject;
    } catch (error) {
      LogUtil.warn(error);
      return subject;
    }
  }
```

## 应用场景

在需要抽屉打开某个视图时，可以通过视图打开服务来抽屉打开该视图。

```ts
    openDrawer(view: any, context: any, viewParam: any) {
      const customStyle = {};
      if (this.dropdownViewWidght) {
        Object.assign(customStyle, { '--width': Util.calcBoxSize(Number(this.dropdownViewWidght)) });
      }
      const container: Subject<any> = App.getOpenViewService().openDrawer(
        { viewModel: view, customStyle: customStyle },
        context,
        viewParam,
      );
      container.subscribe((result: any) => {
        if (!result || !Object.is(result.ret, 'OK')) {
          return;
        }
        this.openViewClose(result.datas);
      });
    },
```

