# 数据看板设计服务
数据看板设计服务是为数据看板设计组件提供的服务，用于数据看板设计组件加载门户部件及保存模型数据。

## 核心逻辑
### 加载门户部件

```ts
  public async loadPortletList(context: IParam, viewParam: IParam): Promise<IParam> {
    const app: IPSApplication = App.getModel();
    const list: any[] = [];
    const portletCats = app.getAllPSAppPortletCats() || [];
    const portlets = app.getAllPSAppPortlets() || [];
    portlets.forEach((portlet: IPSAppPortlet) => {
      const appDe = portlet.getPSAppDataEntity?.();
      const portletCat = portletCats.find(
        (cat: IPSAppPortletCat) => cat.codeName == portlet.getPSAppPortletCat?.()?.codeName,
      );
      const temp = {
        type: 'app',
        portletType: (portlet as any).portletType,
        portletCodeName: portlet.codeName,
        portletName: portlet.name,
        portletImage: (portlet.getPSControl?.() as any)?.getPSSysImage?.()?.cssClass,
        groupCodeName: portletCat?.codeName || '',
        groupName: portletCat?.name || '',
        appCodeName: appDe?.codeName || app.pKGCodeName,
        appName: appDe?.logicName || app.name,
        detailText: portlet.userTag,
        modelData: portlet.getPSControl?.(),
      };
      list.push(temp);
    });
    const datas: any[] = this.filterData(list, viewParam.appdeNamePath);
    const result = this.prepareList(datas);
    const groups = this.prepareGroup(datas);
    return { data: datas, result: result.reverse(), groups: groups };
  }
```

### 保存模型数据

```ts
  public saveModelData(serviceKey: string, context: IParam, viewParam: IParam): Promise<IParam> {
    return new Promise((resolve: any, reject: any) => {
      App.getUtilService()
        .getService(context, serviceKey)
        .then((utilService: IUtilService) => {
          const saveModel: IParam[] = [];
          if (viewParam.model) {
            for (const model of viewParam.model) {
              const temp = { ...model };
              delete temp.modelData;
              saveModel.push(temp);
            }
          }
          viewParam.model = saveModel;
          utilService
            .saveModelData(context, viewParam)
            .then((result: IParam) => {
              resolve(result);
            })
            .catch((error: IParam) => {
              reject(error);
            });
        });
    });
  }
```

## 应用场景

数据看板设计服务为数据看板设计组件提供服务，在数据看板设计组件挂载之前会去调用服务加载门户部件，数据看板设计组件保存时会去调用服务来保存模型。

### 挂载

组件挂载之前，通过数据看板设计服务去加载门户部件集合，之后将选中门户部件和未选中门户部件分别绘制。

```ts
    onBeforeMount(() => {
      const { navContext, navParam } = toRefs(props);
      designService.loadPortletList(navContext.value, navParam.value).then((result: IParam) => {
        handleSelections(result.data);
      });
    });
```

### 保存

```ts
    const saveModel = (isReset: boolean = false) => {
      const { navParam, navContext } = toRefs(props);
      const tempViewParam = {};
      Object.assign(tempViewParam, {
        ...navParam.value,
        model: isReset ? [] : selections,
      });
      designService.saveModelData(navParam.value?.utilServiceName, navContext.value, tempViewParam);
    };
```

