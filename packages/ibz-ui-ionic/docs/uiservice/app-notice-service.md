# 消息提示服务
消息提示服务提供有关操作的反馈或显示系统消息，可以通过应用全局对象App来调用该服务。

```ts
App.getNoticeService()
```

## 核心逻辑
### 消息提示

消息提示有4中类型：提示，成功，警告，错误。

```ts
  public info(message: string, time?: number): void {
    const type = 'secondary';
    this.createToast(type, message, time);
  }
  public success(message: string, time?: number): void {
    const type = 'success';
    this.createToast(type, message, time);
  }
  public warning(message: string, time?: number): void {
    const type = 'warning';
    this.createToast(type, message, time);
  }
  public error(message: string, time?: number): void {
    const type = 'danger';
    this.createToast(type, message, time);
  }
```

## 应用场景

在需要进行操作反馈和显示系统消息处调用该服务的消息提示。