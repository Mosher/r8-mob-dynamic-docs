# 模态服务
模态服务主要用于创建一个模态组件，并将视图嵌入该模态组件之中。可以通过视图打开服务来创建一个模态。

```ts
App.getOpenViewService().openModal(
    {
      viewComponent: '',
      viewPath: '',
      viewModel: {},
      customClass: '',
      customStyle: {},
    },
    navContext,
    navParam,
    navDatas,
    otherParam,
)
```

## 参数说明

| <div style="width: 335px;text-align: left;">参数名</div> | <div style="width: 335px;text-align: left;">说明</div> |
| -------------------------------------------------------- | ------------------------------------------------------ |
| viewComponent                                            | 视图组件名称                                           |
| viewPath                                                 | 视图模型路径                                           |
| viewModel                                                | 视图模型                                               |
| customClass                                              | 自定义类名                                             |
| customStyle                                              | 自定义样式                                             |
| navContext                                               | 上下文                                                 |
| navParam                                                 | 视图参数                                               |
| navDatas                                                 | 导航数据                                               |
| otherParam                                               | 其他参数                                               |

viewComponent（视图组件名称）和viewModel（视图模型）必须传其中一个，其余参数可不传。
## 核心逻辑
### 模态打开

```ts
  public openModal(
    view: {
      viewComponent?: string;
      viewPath?: string;
      viewModel?: IParam;
      customClass?: string;
      customStyle?: IParam;
    },
    navContext: any = {},
    navParam: any = {},
    navDatas: Array<any> = [],
    otherParam: any = {},
  ): Subject<any> {
    const subject: Subject<{ ret: boolean; datas?: IParam[] }> = new Subject<{ ret: boolean; datas?: IParam[] }>();
    try {
      const _navContext: any = {};
      Object.assign(_navContext, navContext);
      if (!view.viewComponent) {
        this.fillView(view);
      }
      const uuid = Util.createUUID();
      this.createVueExample(view, _navContext, navParam, navDatas, otherParam, uuid, subject);
      return subject;
    } catch (error) {
      LogUtil.warn(error);
      return subject;
    }
  }
```

## 应用场景

在需要模态打开某个视图时，可以通过视图打开服务来模态打开该视图。

```ts
  public openModal(context: any, viewParam: any, appView: IPSAppView, container: any, isGlobal?: boolean, arg?: any) {
    if (appView.modelPath) {
      Object.assign(context, { viewpath: appView.modelPath });
    }
    const appmodal = App.getOpenViewService().openModal({ viewModel: appView });
    appmodal.subscribe((result: any) => {
      console.log(result);
    });
  }
```

