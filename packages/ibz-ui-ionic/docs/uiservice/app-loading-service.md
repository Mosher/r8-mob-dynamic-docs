# Loading服务

loading服务提供加载时的loading效果，在部件加载数据之前开启loading效果，在部件加载数据之后关闭loading效果。该服务可以通过应用全局对象App来调用。

```ts
App.getLoadingService();
```
## 核心逻辑
### 开始加载

开始加载时，添加loading效果。

```ts
  public beginLoading() {
    const beginLoading = (selector: any) => {
      this.isLoading = true;
      // 自定义loading元素
      const userEle = document.createElement('div');
      userEle.classList.add('loading-container');
      const innerDiv = document.createElement('div');
      innerDiv.classList.add('loading');
      for (let i = 0; i < 4; i++) {
        const dot = document.createElement('span');
        innerDiv.appendChild(dot);
      }
      userEle.appendChild(innerDiv);
      // 挂载
      if (selector) {
        selector.appendChild(userEle);
      }
    };

    const body = document.querySelector('body');
    if (this.loadingCount === 0) {
      beginLoading(body);
    }
    this.loadingCount++;
  }
```

### 加载结束

加载结束之后，关闭loading效果。

```ts
  public endLoading(): void {
    const endLoading = (selector: any) => {
      if (!this.isLoading) {
        return;
      }
      if (selector) {
        const loadMask = selector.querySelector('.loading-container');
        if (loadMask && selector.contains(loadMask)) {
          selector.removeChild(loadMask);
        }
      }
      this.isLoading = false;
    };

    const body = document.querySelector('body');
    if (this.loadingCount > 0) {
      this.loadingCount--;
    }
    if (this.loadingCount === 0) {
      endLoading(body);
    }
  }
```

## 应用场景

在加载数据之前调用`App.getLoadingService().beginLoading()`启用loading效果；在加载数据之后调用`App.getLoadingService().endLoading()`关闭loading效果。如部件加载时：

```ts
  public onControlRequset(action: string, context: any, viewParam: any, loadding: boolean = true) {
    if (loadding) {
      this.ctrlBeginLoading();
    }
  }

  public onControlResponse(action: string, response: any) {
    this.ctrlEndLoading();
    ......
  }
```

