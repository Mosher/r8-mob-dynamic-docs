# 应用组件服务

应用组件服务通过对模型数据的解析来找到对应的视图组件，布局组件，部件组件和编辑器组件。该服务可通过应用全局对象App来调用。

```ts
App.getComponentService()
```

## 核心逻辑
### 注册应用组件

```ts
  public registerAppComponents() {
    this.registerViewTypeComponents();
    this.registerLayoutComponent();
    this.registerControlComponents();
    this.registerEditorComponents();
  }
```

### 获取应用组件
调用该服务，通过指定的方法，传入该模型的类型，样式及插件代码就可以获取到指定的应用组件。
#### 获取视图组件

传入参数为视图类型，视图样式和插件代码。

```ts
  public getViewTypeComponent(viewType: string, viewStyle: string = 'DEFAULT', pluginCode?: string): any {
    let component = AppMobNotSupportedViewComponent;
    if (pluginCode) {
      component = this.viewTypeMap.get(`${viewType}_${pluginCode}`);
    } else {
      component = this.viewTypeMap.get(`${viewType}_${viewStyle}`);
    }
    return component || AppMobNotSupportedViewComponent;
  }
```

#### 获取布局组件

传入参数为视图类型，视图样式

```ts
  public getLayoutComponent(viewType: string, viewStyle: string, pluginCode?: string): any {
    return this.layoutMap.get(`${viewType}_${viewStyle}`);
  }
```

#### 获取部件组件

传入参数为部件类型，部件样式和插件代码。

```ts
  public getControlComponent(ctrlType: string, ctrlStyle: string = 'DEFAULT', pluginCode?: string): any {
    let component = AppMobNotSupportedControlComponent;
    if (pluginCode) {
      component = this.controlMap.get(`${ctrlType}_${pluginCode}`);
    } else {
      component = this.controlMap.get(`${ctrlType}_${ctrlStyle}`);
    }
    return component || AppMobNotSupportedControlComponent;
  }
```

#### 获取编辑器组件

传入参数为编辑器类型，编辑器样式，插件代码，是否为信息表单。在信息表单中获取到的编辑器都为标签编辑器。

```ts
  public getEditorComponent(
    editorType: string,
    editorStyle: string = 'DEFAULT',
    pluginCode?: string,
    infoMode: boolean = false,
  ): any {
    let component: any = AppMobNotSupportedEditorComponent;
    if (pluginCode) {
      component = this.editorMap.get(`${pluginCode}`);
    } else {
      if (infoMode) {
        component = this.computeInfoModeEditor(editorType, editorStyle);
      } else {
        component = this.editorMap.get(`${editorType}_${editorStyle}`);
      }
    }
    return component || AppMobNotSupportedEditorComponent;
  }
```

## 应用场景

在视图绘制部件时通过`App.getComponentService().getControlComponent()`方法传入部件类型，部件样式及插件代码就可以获取到指定的部件。

```ts
  public computeTargetCtrlData(controlInstance: IPSControl) {
    const targetCtrlComponent: any = App.getComponentService().getControlComponent(
      controlInstance?.controlType,
      controlInstance?.controlStyle ? controlInstance?.controlStyle : 'DEFAULT',
      controlInstance.getPSSysPFPlugin() ? `${controlInstance.getPSSysPFPlugin()?.pluginCode}` : undefined,
    );
    ......
    return h(AppCtrlContainer, null, {
      default: () => {
        return h(targetCtrlComponent, targetCtrlProps);
      },
    });
  }
```

