# 消息弹框服务
消息弹框服务用于创建一个消息弹框组件，在执行某个行为时可以通过应用全局对象App来打开一个消息弹窗来询问用户是否继续执行。

```ts
App.getMsgboxService().open(options);
```

## 消息弹框配置

| <div style="width: 150px;text-align: left;">配置项</div> | <div style="width: 152px;text-align: left;">说明</div> | <div style="width: 150px;text-align: left;">类型</div> | <div style="width: 155px;text-align: left;">默认值</div> |
| -------------------------------------------------------- | ------------------------------------------------------ | ------------------------------------------------------ | -------------------------------------------------------- |
| title                                                    | 标题                                                   | string                                                 | —                                                        |
| showMode                                                 | 显示模式                                               | string                                                 | —                                                        |
| content                                                  | 内容                                                   | string                                                 | —                                                        |
| type                                                     | 类型                                                   | string                                                 | —                                                        |
| cssClass                                                 | 样式                                                   | string                                                 | 'message-container'                                      |
| buttonType                                               | 按钮类型                                               | string                                                 | 'okcancel'                                               |
| buttons                                                  | 按钮                                                   | IParam[]                                               | []                                                       |

## 核心逻辑

### 打开消息弹框

```ts
  public open(options: IMsgboxOptions): Subject<string> {
    this.openMsgbox(options);
    return this.subject;
  }
  private async openMsgbox(options: IMsgboxOptions): Promise<any> {
    if (options.buttons && options.buttons.length > 0) {
      this.buttons = Util.deepCopy(options.buttons);
    } else {
      this.initButtonModel(options.buttonType);
    }
    this.clacMsgboxClass(options);
    try {
      const alert = await alertController.create({
        header: options.title,
        message: options.content,
        cssClass: this.cssClass,
        buttons: this.buttons,
      });
      await alert.present();
    } catch (error) {
      LogUtil.error(error);
    }
  }
```

## 应用场景

一般用于是否继续执行后续行为时，弹出弹窗进行询问。

```ts
  public async remove(
    datas: IParam[],
    args?: IParam,
    showInfo: boolean = true,
    loadding: boolean = true,
  ): Promise<any> {
    ......
    const _remove = async () => {
      ......
    };
    const confirm: Subject<string> | null = App.getMsgboxService().open({
      title: '警告',
      content: `是否确认删除 ${infoStr}`,
    });
    if (confirm) {
      confirm.subscribe(async (result: string) => {
        if (result == 'ok') {
          return await _remove();
        } else {
          return { ret: false, data: this.items };
        }
      });
    }
  }
```

