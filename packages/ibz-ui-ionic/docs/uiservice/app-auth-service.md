# 应用权限服务
应用权限服务提供了和权限相关的方法，其中包含：设置token过期时间，判断token是否过期，刷新token和获取用户访问权限。

应用权限服务可通过应用全局对象App来调用。

```ts
App.getAppAuthService()
```

## 核心逻辑
### 设置token过期时间

在登录成功后设置token过期时间。

```ts
  public async login(_data: { loginname: string; password: string }): Promise<IParam> {
    const response = await this.http.post(App.Environment.RemoteLogin, _data, true);
    const { status, data } = response;
    if (status == 200) {
      this.setExpiredDate(new Date(data.expirdate));
    }
    return response;
  } 
  setExpiredDate(date: Date): void {
    setCookie('ibzuaa-expired', date.getTime().toString(), 7, true);
  }
  
```

### 判断token是否过期

```ts
  isTokenExpired(date: Date): boolean {
    if (this.getExpiredDate()) {
      if (App.Environment) {
        return date.getTime() > this.getExpiredDate().getTime() - App.Environment.refreshTokenTime;
      } else {
        return false;
      }
    } else {
      this.setExpiredDate(new Date());
      return false;
    }
  }
```

### 刷新token

```ts
  public async refreshToken(data: IParam): Promise<boolean> {
    if (
      data &&
      (data.url == '/v7/refreshToken' ||
        data.url == '/v7/login' ||
        data.url == '/appdata' ||
        data.url.startsWith('./assets'))
    ) {
      return true;
    }
    try {
      const response = await Http.getInstance().get('/v7/refreshToken');
      if (response && response.status === 200) {
        const data = response.data;
        this.setExpiredDate(new Date(data.expirdate));
        if (data) {
          setCookie('ibzuaa-token', data.token, 7, true);
        }
        return true;
      } else {
        LogUtil.log('刷新token出错');
        return false;
      }
    } catch (error) {
      return false;
    }
  }
```

### 获取用户访问权限

传入指定视图，即可获取指定视图的用户访问权限。

```ts
  public async getUserAccessAuth(viewModel: IParam): Promise<boolean> {
    const token = getCookie('ibzuaa-token');
    switch (viewModel.accUserMode) {
      // 未启用访问用户模式
      case 0:
        return true;
      // 匿名用户
      case 1:
        return token ? false : true;
      // 登录用户
      case 2:
        return token ? true : false;
      // 匿名用户及登录用户
      case 3:
        return true;
      // 登录用户且拥有指定资源能力
      case 4:
        const UIServiceBase = await App.getUIService().getService({}, {});
        return UIServiceBase.getResourceOPPrivs(viewModel.accessKey);
      default:
        return true;
    }
  }
```

## 应用场景

在路由控件app-route-shell中，每次跳转路由之前都会获取用户的访问权限来判断用户是否能访问该视图。

```ts
  init() {
    this.computeDynaModelFilePath(this.route);
    this.router.beforeEach(async (to, from) => {
      return await this.accessUserAuth(to);
    });
  }
  
  public async accessUserAuth(route: any) {
    const dynaModelFilePath = await this.getDynaModelFilePath(route);
    const viewModel = await ((await GetModelService()).getPSAppView(dynaModelFilePath)) as IPSAppView;
    return await App.getAppAuthService().getUserAccessAuth(viewModel);
  }
```

