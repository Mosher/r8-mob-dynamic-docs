# 动作面板服务

动作面板服务打开一个显示一组选项的对话框动作面板，点击背景或按下桌面的退出键可以关闭该动作面板。

动作面板来调用可通过应用全局对象App来调用并创建，options为动作面板的配置。

```ts
App.getActionSheetService().create(options);
```

## 配置

### options配置

| <div style="width: 145px;text-align: left;">配置项</div> | <div style="width: 148px;text-align: left;">说明</div> | <div style="width: 148px;text-align: left;">类型</div> | <div style="width: 137px;text-align: left;">默认值</div> |
| -------------------------------------------------------- | ------------------------------------------------------ | ------------------------------------------------------ | -------------------------------------------------------- |
| id                                                       | 标识                                                   | string                                                 | —                                                        |
| header                                                   | 动作面板标题                                           | string                                                 | —                                                        |
| subHeader                                                | 动作面板副标题                                         | string                                                 | —                                                        |
| cssClass                                                 | 动作面板自定义CSS类名                                  | string \| string[]                                     | —                                                        |
| buttons                                                  | 按钮集合                                               | IAppActionSheetButton[]                                | []                                                       |
| backdropDismiss                                          | 点击背景是否可关闭                                     | boolean                                                | true                                                     |
| translucent                                              | 是否透明                                               | boolean                                                | false                                                    |

### buttons配置

| <div style="width: 155px;text-align: left;">配置项</div> | <div style="width: 155px;text-align: left;">说明</div> | <div style="width: 155px;text-align: left;">类型</div> | <div style="width: 140px;text-align: left;">默认值</div> |
| -------------------------------------------------------- | ------------------------------------------------------ | ------------------------------------------------------ | -------------------------------------------------------- |
| text                                                     | 显示内容                                               | string                                                 | —                                                        |
| role                                                     | 默认权限                                               | string                                                 | 'cancel'                                                 |
| icon                                                     | 图标                                                   | string                                                 | —                                                        |
| cssClass                                                 | 按钮自定义CSS类名                                      | string                                                 | —                                                        |
| handler                                                  | 按钮点击方法                                           | Function                                               | —                                                        |

## 核心逻辑
### 创建动作面板
在动作面板创建时，会将options配置转为ionic配置，并调用ionic的actionSheetController来创建动作面板。

```ts
async create(options: IAppActionSheetOptions): Promise<ICtrlActionResult> {
    try {
        const ionicASOptions = this.configConvert(options) as ActionSheetOptions;
        const actionSheet = await actionSheetController.create(ionicASOptions);
        await actionSheet.present();
    	return { ret: true, data: [] };
    } catch (error: any) {
        return { ret: false, data: [error] };
    }
}
```

## 应用场景

如果门户部件配置有界面行为组，点击图标打开界面行为组动作面板。通过配置在buttons上的handler执行该界面行为。

```ts
  /**
   * 打开动作面板
   *
   * @param event 源事件对象
   * @memberof AppMobPortlet
   */
  public openActionSheet(event: any) {
    const buttons: IAppActionSheetButton[] = [];
    this.c.actionBarModel.forEach((model: any) => {
      buttons.push({
        text: model.name,
        icon: model.icon,
        handler: () => {
          this.onActionBarItemClick(model.viewLogicName, event);
        },
      });
    });
    buttons.push({
      text: this.$tl('share.cancel','取消'),
      role: 'cancel',
      handler: () => {},
    });
    const options: IAppActionSheetOptions = { buttons: buttons };
    App.getActionSheetService()
      .create(options)
      .then((result: ICtrlActionResult) => {});
  }
```

