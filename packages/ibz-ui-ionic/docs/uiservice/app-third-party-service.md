# 第三方服务
R8Mob-Dynamic模板支持多种搭载平台，如钉钉，微信，安卓端等等。第三方服务就是用来统一管理这些搭载平台及搭载平台服务的一个服务类，在应用初始化时会通过第三方服务来获取当前的搭载平台。

## 应用初始化

在创建应用实例对象时通过第三方服务获取当前搭载平台。

```ts
    /**
     * Creates an instance of App.
     * @memberof App
     */
    constructor() {
        super();
        this.initAppLanguage();
        this.initAppTheme();
        this.initPlatFormType();
    }
     /**
     * @description 初始化搭载平台
     * @memberof App
     */
    public async initPlatFormType() {
        const appThirdPartyService = AppThirdPartyService.getInstance();
        const platform: any = await appThirdPartyService.getAppPlatform();
        this.setPlatFormType(platform);
    }
```

## 核心逻辑

### 初始化

在创建第三方服务实例时，会初始化搭载平台及搭载平台服务（钉钉服务和微信服务）

```ts
  /**
   * Creates an instance of AppThirdPartyService.
   * @memberof AppThirdPartyService
   */
  private constructor() {
    this.thirdPartyInit();
  }

  /**
   *  第三方初始化
   *
   * @memberof AppThirdPartyService
   */
  public thirdPartyInit() {
    this.initAppPlatForm();
    this.initPlatformService();
  }
```

### 获取搭载平台

```ts
  public async getAppPlatform(): Promise<'ios' | 'android' | 'dd' | 'wx-mini' | 'wx-pn' | 'desktop' | 'web'> {
    let platform: 'ios' | 'android' | 'dd' | 'wx-mini' | 'wx-pn' | 'desktop' | 'web';
    if (this.platform === 'wx') {
      const isMini = await (this.platformService as WeChatService).isMini();
      platform = isMini ? 'wx-mini' : 'wx-pn';
    } else {
      platform = this.platform;
    }
    return platform;
  }
```

### 获取搭载平台服务类

```ts
  public getPlatformService(): DingTalkService | WeChatService {
    return this.platformService;
  }
```

