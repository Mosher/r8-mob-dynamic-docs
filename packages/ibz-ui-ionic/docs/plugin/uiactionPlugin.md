# 界面行为插件

界面行为没有绘制逻辑，只有数据处理逻辑，配置界面行为插件可重写数据处理逻辑。

### 配置界面行为插件

1. 新建前端模板插件，可参考[前端模板插件](../guide/plugin.md#前端模板插件)

   ::: tip

   选择应用界面行为类型

   :::

2. 配置界面行为插件

   ![uiaction_plugin](../.vuepress/public/images/uiaction_plugin.png)

  ::: tip

  在配置界面行为的位置进行配置，如工具栏行为项上配置界面行为插件。

  :::

### 代码发布

界面行为插件与部件项发布类似，其中代码模板是部件项插件中renderItem中的代码，代码模板2是导入代码，代码模板3是部件项除render的其他方法，代码模板4暂未使用。

- 代码模板

```tsx
console.log("界面行为插件触发");
```

- 代码模板2

```tsx
import { Util } from 'ibz-core';
```

- 代码模板3

```tsx
public test() {
    console.log(123);
}
```

- 成果物代码


```tsx

import { IPSAppDEUIAction } from "@ibiz/dynamic-model-api";
import { IUIDataParam, IUIEnvironmentParam, IUIUtilParam } from "ibz-core";
import { Util } from 'ibz-core';


/**
 * 测试界面行为插件
 *
 * @export
 * @class TestActionPlugin
 * @class TestActionPlugin
 */
export default class TestActionPlugin {

    /**
     * 模型数据
     * 
     * @memberof TestActionPlugin
     */
    private actionModel !: IPSAppDEUIAction;

    public test() {
      console.log(123);
    }

    /**
     * 初始化 TestActionPlugin
     * 
     * @memberof TestActionPlugin
     */
    constructor(opts: any, context?: any) {
        this.actionModel = opts;
    }

    /**
     * 执行界面行为
     *
     * @param context 附加上下文
     * @param params 附加参数
     * @param UIDataParam 操作数据参数
     * @param UIEnvironmentParam 操作环境参数
     * @param UIUtilParam 操作工具参数
     * @param srfParentDeName 父值参数
     * @param deUIService 界面UI服务
     *
     * @memberof TestActionPlugin
     */
    public async execute(
        context: any = {},
        params: any = {},
        UIDataParam: IUIDataParam,
        UIEnvironmentParam: IUIEnvironmentParam,
        UIUtilParam: IUIUtilParam,
        srfParentDeName?: string,
        deUIService?: any
    ) {
        console.log("界面行为插件触发");
    }
}
```

插件标识代码会发布在src\plugins\app-plugin-service.ts文件中，绘制时会根据插件标识获取插件组件。

```ts
/**
 * 注册界面行为插件
 * 
 * @memberof AppPluginService
 */
private registerUIActionPlugin(){
    this.UIActionMap.set('testActionPlugin',() => import('./plugin/deuiaction/test-action-plugin'));
}
```

### 数据逻辑

部件上触发界面行为后抛出ctrlEvent事件，视图接收到后传递给视图控制器处理，视图控制器处理后会执行全局界面行为，最后将会执行插件中的execute方法。

```ts
/**
 * @description 执行全局界面行为
 * @static
 * @param {string} tag 界面行为标识
 * @param {IUIDataParam} UIDataParam 操作环境参数
 * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
 * @param {IUIUtilParam} UIUtilParam 操作工具参数
 * @param {*} [contextJO={}] 行为附加上下文
 * @param {*} [paramJO={}] 附加参数
 * @param {string} [srfParentDeName] 应用实体名称
 * @memberof AppGlobalUtil
 */
public static executeGlobalAction(
  tag: string,
  UIDataParam: IUIDataParam,
  UIEnvironmentParam: IUIEnvironmentParam,
  UIUtilParam: IUIUtilParam,
  contextJO: any = {},
  paramJO: any = {},
  srfParentDeName?: string,
) {
  if (!this.isInitPluginAction) {
    this.initGlobalPluginAction();
  }
  if (this.globalPluginAction.get(tag)) {
    const curActionPlugin = this.globalPluginAction.get(tag);
    const importPlugin: any = App.getPluginService().getUIActionByTag(curActionPlugin?.getPSSysPFPlugin()?.pluginCode);
    if (importPlugin) {
      importPlugin().then((importModule: any) => {
        const actionPlugin = new importModule.default(curActionPlugin);
        actionPlugin.execute(UIDataParam, UIEnvironmentParam, UIUtilParam, contextJO, paramJO, srfParentDeName);
      })
    }
  } else {
    // 分发逻辑
      ......
  }
}
```



