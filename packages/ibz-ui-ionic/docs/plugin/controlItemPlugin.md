# 部件项插件

部件项插件与其他插件不同，只有一些部件上可配置部件项插件。

### 配置部件项插件

1. 配置前端模板插件，可参考[前端模板插件](../guide/plugin.md#前端模板插件)

   ::: tip

   选择部件项插件类型，如列表项绘制插件

   :::

2. 在部件项上配置前端扩展插件，选择对应的部件项插件。

  ![controlItem_plugin1](../.vuepress/public/images/controlItem_plugin1.png)

   ::: tip

   部件项插件只在某些部件上可配置，如表单项、列表项。

   :::

### 代码发布

模板插件中有4块代码模板，其中代码模板是部件项插件中renderItem中的代码，代码模板2是导入代码，代码模板3是部件项除renderItem的其他方法，代码模板4暂未使用。

- 代码模板

```tsx
return <div class="controlItem-plugin">部件项插件测试内容</div>
```

- 代码模板2

```tsx
import { Util } from 'ibz-core';
```

- 代码模板3

```tsx
public test() {
    console.log(123);
}
```

- 成果物代码


```tsx
import { IParam } from "ibz-core";
import { Util } from 'ibz-core';

/**
 * 工具栏项插件
 *
 * @export
 * @class ToolbarItem
 */
export class ToolbarItem {

    public test() {
      console.log(123);
    }

    /**
     * 绘制项数据
     * 
     * @param {IParam} item 项数据
     * @param {*} controller 部件控制器
     * @param {*} container 部件组件容器
     * @memberof ToolbarItem
     */
    public renderItem(item: IParam, controller: any, container: any) {
        return <div class="controlItem-plugin">部件项插件测试内容</div>
    }
}
```

部件项插件发布位置与视图部件不同，部件项插件标识代码发布在app-plugin-service文件中。

```ts
$src\plugins\app-plugin-service.ts

/**
 * 注册部件成员插件
 * 
 * @memberof AppPluginService
 */
private registerControlItemPlugin(){
    this.controlItemMap.set('ToolbarItem',new ToolbarItem());
}
```

### 绘制逻辑

部件中绘制部件项时也是先判断是否存在部件项插件，如果存在插件就执行该插件组件的renderItem方法

```tsx
/**
 * @description 根据detailType绘制对应detail
 * @param {*} modelJson 模型数据
 * @param {number} index 下标
 * @memberof AppMobForm
 */
public renderByDetailType(modelJson: any, index: number) {
  if (modelJson.getPSSysPFPlugin()?.pluginCode) {
    const ctrlItemPluginInstance = App.getPluginService().getCtrlItemPluginByTag(
      modelJson.getPSSysPFPlugin()?.pluginCode,
    );
    if (ctrlItemPluginInstance) {
      return ctrlItemPluginInstance.renderItem(this.c.data[modelJson.name], this.c, this);
    }
  } else {
    switch (modelJson.detailType) {
      case 'FORMPAGE':
        return this.renderFormPage(modelJson as IPSDEFormPage, index);
      case 'GROUPPANEL':
        return this.renderGroupPanel(modelJson as IPSDEFormGroupPanel, index);
      case 'FORMITEM':
        return this.renderFormItem(modelJson as IPSDEFormItem, index);
      case 'DRUIPART':
        return this.renderDruipart(modelJson as IPSDEFormDRUIPart, index);
      case 'BUTTON':
        return this.renderButton(modelJson as IPSDEFormButton, index);
      default:
        LogUtil.log(`暂未实现 ${modelJson.detailType} 类型表单成员`);
    }
  }
}
```

::: warning

  该部件项插件为表单项插件，其他部件项插件绘制逻辑有可能会出现不同。

  :::

```ts
$src\plugins\app-plugin-service.ts

/**
 * 获取项插件
 * 
 * @param tag 界面行为标识
 * @memberof IPluginService
 */
public getCtrlItemPluginByTag(tag: string) {
    return this.controlItemMap.get(tag);
}
```

