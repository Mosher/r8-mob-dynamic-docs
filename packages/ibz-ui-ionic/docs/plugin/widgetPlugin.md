# 部件插件

部件插件与视图插件逻辑类似，只对ui进行重绘，不会影响数据来源。数据来源为模型，经视图控制器传递给部件控制器。

### 配置部件插件

1. 配置前端模板插件，可参考[前端模板插件](../guide/plugin.md#前端模板插件)

   ::: tip

   选择部件插件类型，不是部件项插件类型

   :::

2. 在部件上配置前端扩展插件，选择对应的部件插件。

   ![widget_plugin1](../.vuepress/public/images/widget_plugin1.png)

   ::: tip

   部件配置插件有两个地方可进行配置，可在使用该部件的视图上配置，也可在部件中进行配置

   :::

### 代码发布

模板插件中有4块代码模板，主要用到两块，其中代码模板是视图插件的绘制代码，代码模板3是样式代码。

- 代码模板

```tsx
import { shallowReactive } from "vue";
import { CtrlComponentBase, GenerateComponent } from "ibz-ui-ionic";
import { AppCtrlProps } from "ibz-core";
import { testComponentBase } from 'ibz-ui-ionic';
import '../plugin-style.scss';

export class  CustomPortletPlugin extends testComponentBase<AppCtrlProps> {
 	render(){
    	return <div class="widget-plugin">部件插件测试内容</div>
	}
}

// 自定义门户部件插件组件
export const CustomPortletPluginComponent = GenerateComponent(CustomPortletPlugin, new AppCtrlProps());
```

- 代码模板3

```scss
.widget-plugin {
    color: red;
}
```

- 成果物代码


```tsx
$src\plugins\plugin\portlet-custom\custom-portlet-plugin.tsx

import { shallowReactive } from "vue";
import { CtrlComponentBase, GenerateComponent } from "ibz-ui-ionic";
import { AppCtrlProps } from "ibz-core";
import { testComponentBase } from 'ibz-ui-ionic';
import '../plugin-style.scss';

export class  CustomPortletPlugin extends testComponentBase<AppCtrlProps> {
 	render(){
    	return <div class="widget-plugin">部件插件测试内容</div>
	}
}

// 自定义门户部件插件组件
export const CustomPortletPluginComponent = GenerateComponent(CustomPortletPlugin, new AppCtrlProps());
```

```scss
$src\plugins\plugin\plugin-style.scss

.widget-plugin {
    color: red;
}
```

插件标识代码也会发布在src\core\app\component-service.ts文件中，绘制时会根据插件标识获取插件组件。

```ts
/**
 * @description 注册部件组件
 * @return {*} 
 * @memberof ComponentService
 */
protected registerControlComponents() {
    super.registerControlComponents();
    // 注册部件插件标识
    this.controlMap.set('MAPEXPBAR_TEST', CustomPortletPluginComponent);
}
```

### 绘制逻辑

视图基类中统一绘制部件时会先判断是否存在部件插件，如果存在插件就直接绘制插件。

```tsx
/**
 * @description 计算目标部件所需参数
 * @param {IPSControl} controlInstance 部件模型实例
 * @return {*}
 * @memberof ViewComponentBase
 */
public computeTargetCtrlData(controlInstance: IPSControl) {
  const targetCtrlComponent: any = App.getComponentService().getControlComponent(
    controlInstance?.controlType,
    controlInstance?.controlStyle ? controlInstance?.controlStyle : 'DEFAULT',
    controlInstance.getPSSysPFPlugin() ? `${controlInstance.getPSSysPFPlugin()?.pluginCode}` : undefined,
  );
  const targetCtrlProps = {
    controlInstance: controlInstance,
    viewState: this.c.viewState,
    viewCtx: this.c.viewCtx,
    navContext: Util.deepCopy(this.c.context),
    navParam: Util.deepCopy(this.c.viewParam),
    viewCounterServiceArray: this.c.counterServiceArray,
    enablePullDownRefresh: (this.c.viewInstance as any).enablePullDownRefresh,
    modelService:this.c.modelService,
    onCtrlEvent: ({ controlname, action, data }: { controlname: string; action: string; data: any }) => {
      this.c.handleCtrlEvent(controlname, action, data);
    },
  };
  this.extraCtrlParam(targetCtrlProps, controlInstance);
  //  工具栏特殊处理
  if (controlInstance.controlType == 'TOOLBAR') {
    return h(targetCtrlComponent, targetCtrlProps);
  }
  return h(AppCtrlContainer, null, {
    default: () => {
      return h(targetCtrlComponent, targetCtrlProps);
    },
  });
}
```

```ts
$packages\ibz-ui-ionic\src\components\view\view-component-base.tsx

/**
 * @description 获取部件组件
 * @param {string} ctrlType 部件类型
 * @param {string} ctrlStyle 部件样式
 * @param {string} [pluginCode] 插件标识
 * @return {string}
 * @memberof AppComponentService
 */
public getControlComponent(ctrlType: string, ctrlStyle: string, pluginCode?: string): any {
  let component = AppMobNotSupportedControlComponent;
  if (pluginCode) {
    component = this.controlMap.get(`${ctrlType}_${pluginCode}`);
  } else {
    component = this.controlMap.get(`${ctrlType}_${ctrlStyle}`);
  }
  return component || AppMobNotSupportedControlComponent;
}
```

部件插件的本质也是对render方法的重写，只是重写的只是配置对应部件的render方法。

