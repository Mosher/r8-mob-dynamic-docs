# 视图插件

视图的数据来源于模型，数据的解析为视图的控制器，视图插件都不会改变这些，视图插件是对视图ui进行重绘。

### 配置视图插件

1. 配置前端模板插件，可参考[前端模板插件](../guide/plugin.md#前端模板插件)

   ::: tip

   插件类型选择实体视图绘制插件

   :::

2. 在视图上配置前端扩展插件，在配置视图的地方配置对应的前端扩展插件，选择对应的视图插件

   ![view_plugin1](../.vuepress/public/images/view_plugin1.png)

   ::: tip

   该配置是在配置视图的地方，如菜单打开视图就应该在菜单项上配置前端扩展插件

   :::

### 代码发布

模板插件中有4块代码模板，主要用到两块，其中代码模板是视图插件的绘制代码，代码模板3是样式代码。

- 代码模板

```tsx
import { AppViewProps } from "ibz-core";
import { GenerateComponent, ViewComponentBase } from "ibz-ui-ionic";
import { shallowReactive } from "vue";
import { DEMultiDataViewComponentBase } from 'ibz-ui-ionic';
import { AppMobMapViewProps } from 'ibz-core';
import '../subview-style.scss';

/**
 * 测试视图插件
 *
 * @export
 * @class TestViewPlugin
 */
export class TestViewPlugin extends DEMultiDataViewComponentBase<AppMobMapViewProps> {

	public render(){
        return <div class="view-plugin">视图插件测试内容</div>
    }
}
// 测试视图插件组件
export const TestViewPluginComponent = GenerateComponent(TestViewPlugin, new AppViewProps());
```

- 代码模板3

```scss
.view-plugin {
    color: red;
}
```

- 成果物代码


```tsx
$src\plugins\view\view-custom\test-view-plugin.tsx

import { AppViewProps } from "ibz-core";
import { GenerateComponent, ViewComponentBase } from "ibz-ui-ionic";
import { shallowReactive } from "vue";
import { DEMultiDataViewComponentBase } from 'ibz-ui-ionic';
import { AppMobMapViewProps } from 'ibz-core';
import '../subview-style.scss';

/**
 * 测试视图插件
 *
 * @export
 * @class TestViewPlugin
 */
export class TestViewPlugin extends DEMultiDataViewComponentBase<AppMobMapViewProps> {

	public render(){
        return <div class="view-plugin">视图插件测试内容</div>
    }
}
// 测试视图插件组件
export const TestViewPluginComponent = GenerateComponent(TestViewPlugin, new AppViewProps());
```

```scss
$src\plugins\view\subview-style.scss

.view-plugin {
    color: red;
}
```

插件标识代码也会发布在src\core\app\component-service.ts文件中，绘制时会根据插件标识获取插件组件。

```ts
/**
 * @description 注册视图类型组件
 * @return {*} 
 * @memberof ComponentService
 */
protected registerViewTypeComponents() {
    super.registerViewTypeComponents();
    // 注册视图插件
    this.viewTypeMap.set("DEMOBMAPVIEW_testViewPlugin", TestViewPluginComponent);
}
```

### 绘制逻辑

路由跳转前会先判断是否存在视图插件，如果存在插件就绘制插件内容。

```tsx
$src\components\app-route-shell\app-route-shell.tsx

/**
 * @description 加载动态模型数据
 * @return {*} 
 * @memberof AppRouteShell
 */
public async loadDynamicModelData() {
  const modeldata = await ((await GetModelService()).getPSAppView(this.dynaModelFilePath)) as IPSAppView;
  //  未找到模型数据跳转404页面
  if (Util.isEmpty(modeldata)) {
    this.router.push('/404');
    return;
  } else {
    this.viewComponent = App.getComponentService().getViewTypeComponent(
      (modeldata as IPSAppView).viewType,
      (modeldata as IPSAppView).viewStyle,
      (modeldata as IPSAppView)?.getPSSysPFPlugin()?.pluginCode
    );
    this.forceUpdate();
  }
}
```

```ts
$packages\ibz-ui-ionic\src\ui-service\app-component-service\app-component-service.ts

/**
 * @description 获取视图类型组件
 * @param {string} viewType 视图类型
 * @param {string} viewStyle 视图样式
 * @param {string} [pluginCode] 插件代码
 * @return {string}
 * @memberof AppComponentService
 */
public getViewTypeComponent(viewType: string, viewStyle: string, pluginCode?: string): any {
  let component = AppMobNotSupportedViewComponent;
  if (pluginCode) {
    component = this.viewTypeMap.get(`${viewType}_${pluginCode}`);
  } else {
    component = this.viewTypeMap.get(`${viewType}_${viewStyle}`);
  }
  return component || AppMobNotSupportedViewComponent;
}
```

视图插件的本质是对packages\ibz-ui-ionic\src\components\view\view-component-base.tsx文件中的render方法进行重写。

```tsx
$packages\ibz-ui-ionic\src\components\view\view-component-base.tsx

/**
   * @description 绘制视图
   * @return {*}
   * @memberof ViewComponentBase
   */
  render() {
    if (!unref(this.viewIsLoaded) || !this.c.viewInstance) {
      return null;
    }
    const targetViewLayoutComponent: string = App.getComponentService().getLayoutComponent(
      this.c.viewInstance?.viewType,
      this.c.viewInstance?.viewStyle,
    );
    return h(
      targetViewLayoutComponent,
      {
        viewInstance: this.c.viewInstance,
        navContext: this.c.context,
        navParam: this.c.viewParam,
        navDatas: this.c.navDatas,
        isShowCaptionBar: this.c.isShowCaptionBar,
        modelService:this.c.modelService
      },
      this.renderViewControls(),
    );
  }
```

