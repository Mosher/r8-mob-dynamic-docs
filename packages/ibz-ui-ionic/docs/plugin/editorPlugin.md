# 编辑器插件

编辑器插件为ui的最基础绘制逻辑，只存在表单和布局面板中。配置编辑器插件与其他插件逻辑也与其他不同。

### 配置编辑器插件

1. 新建前端模板插件，可参考[前端模板插件](../guide/plugin.md#前端模板插件)

   ::: tip

   选择编辑器自定义绘制插件

   :::

2. 新建系统编辑器样式，依次点击系统导航、系统全局界面、系统编辑器样式、新建。选择编辑器样式、输入编辑器名称、样式代码。

   ![editor_plugin1](../.vuepress/public/images/editor_plugin1.png)

3. 编辑刚刚新建的系统编辑器样式，选择对应的前端模板插件。

   ![editor_plugin2](../.vuepress/public/images/editor_plugin2.png)

1. 在编辑器上选择刚刚新建的系统编辑器样式。

   ![editor_plugin3](../.vuepress/public/images/editor_plugin3.png)

### 代码发布

模板插件中有4块代码模板，其中代码模板是编辑器插件的绘制代码，代码模板2是导入代码，代码模板3是样式代码，代码模板4是继承类代码。

- 代码模板

```tsx
public render(){
    return <div class="editor-plugin">编辑器插件测试内容</div>
}
```

- 代码模板2

```tsx
import { testComponentBase } from 'ibz-ui-ionic';
```

- 代码模板3

```scss
.editor-plugin {
    color: red;
}
```

- 代码模板4

```ts
testComponentBase<AppEditorProps>
```

- 成果物代码


```tsx

import { shallowReactive } from 'vue';
import { AppEditorProps } from 'ibz-core';
import { EditorComponentBase, GenerateComponent } from 'ibz-ui-ionic';
import { testComponentBase } from 'ibz-ui-ionic';


/**
 * 测试编辑器插件
 *
 * @export
 * @class TestEditorPlugin
 * @extends {EditorComponentBase}
 */
export class TestEditorPlugin extends testComponentBase<AppEditorProps> {

    render() {
        return <div class="editor-plugin">编辑器插件测试内容</div>
    }
}
// 测试编辑器插件组件
export const TestEditorPluginComponent = GenerateComponent(TestEditorPlugin, new AppEditorProps());
```

```scss
$src\plugins\editor\editor-style.scss
.editor-plugin {
    color: red;
}
```

插件标识代码也会发布在src\core\app\component-service.ts文件中，绘制时会根据插件标识获取插件组件。

```ts
/**
 * @description 注册编辑器组件
 * @protected
 * @memberof ComponentService
 */
protected registerEditorComponents() {
    super.registerEditorComponents();
    // 注册编辑器插件
    this.editorMap.set("MOBTEXT_testEdiotrStyle", TestEditorPluginComponent);
}
```

### 绘制逻辑

视图基类中统一绘制部件时会先判断是否存在编辑器插件，如果存在插件就直接绘制插件。

```tsx
/**
 * @description 绘制编辑器
 * @returns {*}
 * @memberof AppMobForm
 */
public renderEditor(editor: IPSEditor, parentItem: any): any {
  const targetCtrlComponent: any = App.getComponentService().getEditorComponent(
    editor?.editorType,
    editor?.editorStyle ? editor?.editorStyle : 'DEFAULT',
    editor.getPSSysPFPlugin()
      ? `${editor?.editorType}_${editor?.editorStyle}`
      : undefined,
    this.c.detailsModel[editor.name]?.infoMode
  );
  return h(targetCtrlComponent, {
    editorInstance: editor,
    containerCtrl: this.c.controlInstance,
    parentItem: parentItem,
    contextData: this.c.data,
    contextState: this.c.formState,
    ctrlService: this.c.ctrlService,
    navContext: Util.deepCopy(this.c.context),
    navParam: Util.deepCopy(this.c.viewParam),
    disabled: this.c.detailsModel[editor.name]?.disabled,
    value: this.c.data[editor?.name || ''],
    modelService: this.c.modelService,
    onEditorEvent: (event: IEditorEventParam) => {
      this.c.handleEditorEvent(event);
    },
  });
}
```

```ts
/**
 * @description 获取编辑器组件
 * @param {string} editorType 编辑器类型
 * @param {string} editorStyle 编辑器样式
 * @param {string} [pluginCode] 编辑器插件标识
 * @return {string}
 * @memberof AppComponentService
 */
public getEditorComponent(
  editorType: string,
  editorStyle: string,
  pluginCode?: string,
  infoMode: boolean = false,
): any {
  let component: any = AppMobNotSupportedEditorComponent;
  if (pluginCode) {
    component = this.editorMap.get(`${pluginCode}`);
  } else {
    if (infoMode) {
      component = this.computeInfoModeEditor(editorType, editorStyle);
    } else {
      component = this.editorMap.get(`${editorType}_${editorStyle}`);
    }
  }
  return component || AppMobNotSupportedEditorComponent;
}
```

