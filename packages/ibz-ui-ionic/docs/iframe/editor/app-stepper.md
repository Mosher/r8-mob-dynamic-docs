---
layout: component-iframe-layout
---

<ion-app class="iframe-html">
  <ion-header translucent>
    <ion-toolbar>
      <ion-title>步进器</ion-title>
    </ion-toolbar>
  </ion-header>
  <ion-content fullscreen>
    <ion-list>
      <ion-item>
        <ion-label>默认</ion-label>
        <component-demo type="EDITOR" path="/editor/app-stepper/default.json"/>
      </ion-item>
      <ion-item>
        <ion-label>禁用</ion-label>
        <component-demo type="EDITOR" path="/editor/app-stepper/disabled.json"/>
      </ion-item>
      <ion-item>
        <ion-label>只读</ion-label>
        <component-demo type="EDITOR" path="/editor/app-stepper/readonly.json"/>
      </ion-item>
      <ion-item>
        <ion-label>范围</ion-label>
        <component-demo type="EDITOR" path="/editor/app-stepper/range.json"/>
      </ion-item>
      <ion-item>
        <ion-label>步进值</ion-label>
        <component-demo type="EDITOR" path="/editor/app-stepper/stepValue.json"/>
      </ion-item>
      <ion-item>
        <ion-label>精度系数</ion-label>
        <component-demo type="EDITOR" path="/editor/app-stepper/precision.json"/>
      </ion-item>
    </ion-list>
  </ion-content>
</ion-app>