---
layout: component-iframe-layout
---

<ion-app class="iframe-html">
  <ion-header translucent>
    <ion-toolbar>
      <ion-title>下拉列表框</ion-title>
    </ion-toolbar>
  </ion-header>
  <ion-content fullscreen>
    <ion-list>
      <ion-item>
        <ion-label>默认</ion-label>
        <component-demo type="EDITOR" path="/editor/app-dropdown-list/default.json"/>
      </ion-item>
      <ion-item>
        <ion-label>禁用</ion-label>
        <component-demo type="EDITOR" path="/editor/app-dropdown-list/disabled.json"/>
      </ion-item>
      <ion-item>
        <ion-label>只读</ion-label>
        <component-demo type="EDITOR" path="/editor/app-dropdown-list/readonly.json"/>
      </ion-item>
      <ion-item>
        <ion-label>多选</ion-label>
        <component-demo type="EDITOR" path="/editor/app-dropdown-list/multiple.json"/>
      </ion-item>
      <ion-item>
        <ion-label>气泡打开</ion-label>
        <component-demo type="EDITOR" path="/editor/app-dropdown-list/popover.json"/>
      </ion-item>
    </ion-list>
  </ion-content>
</ion-app>

