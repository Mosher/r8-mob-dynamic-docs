---
layout: component-iframe-layout
---

<ion-app class="iframe-html">
  <ion-header translucent>
    <ion-toolbar>
      <ion-title>多行文本框</ion-title>
    </ion-toolbar>
  </ion-header>
  <ion-content fullscreen>
    <ion-list>
      <ion-item>
        <ion-label>默认</ion-label>
        <component-demo type="EDITOR" path="/editor/app-textarea/default.json"/>
      </ion-item>
      <ion-item>
        <ion-label>禁用</ion-label>
        <component-demo type="EDITOR" path="/editor/app-textarea/disabled.json"/>
      </ion-item>
      <ion-item>
        <ion-label>只读</ion-label>
        <component-demo type="EDITOR" path="/editor/app-textarea/readonly.json"/>
      </ion-item>
      <ion-item>
        <ion-label>最大长度</ion-label>
        <component-demo type="EDITOR" path="/editor/app-textarea/maxLength.json"/>
      </ion-item>
    </ion-list>
  </ion-content>
</ion-app>