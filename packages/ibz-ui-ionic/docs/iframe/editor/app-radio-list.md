---
layout: component-iframe-layout
---

<ion-app class="iframe-html">
  <ion-header translucent>
    <ion-toolbar>
      <ion-title>单选列表框</ion-title>
    </ion-toolbar>
  </ion-header>
  <ion-content fullscreen>
    <ion-list>
        <ion-list-header>默认</ion-list-header>
        <component-demo type="EDITOR" path="/editor/app-radio-list/default.json"/>
    </ion-list>
    <ion-list>
        <ion-list-header>禁用</ion-list-header>
        <component-demo type="EDITOR" path="/editor/app-radio-list/disabled.json"/>
    </ion-list>
    <ion-list>
        <ion-list-header>只读</ion-list-header>
        <component-demo type="EDITOR" path="/editor/app-radio-list/readonly.json"/>
    </ion-list>
  </ion-content>
</ion-app>