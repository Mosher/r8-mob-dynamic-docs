---
layout: component-iframe-layout
---

<ion-app class="iframe-html">
  <ion-header translucent>
    <ion-toolbar>
      <ion-title>开关</ion-title>
    </ion-toolbar>
  </ion-header>
  <ion-content fullscreen>
    <ion-list>
      <ion-item>
        <ion-label>默认</ion-label>
        <component-demo type="EDITOR" path="/editor/app-switch/default.json"/>
      </ion-item>
      <ion-item>
        <ion-label>禁用</ion-label>
        <component-demo type="EDITOR" path="/editor/app-switch/disabled.json"/>
      </ion-item>
      <ion-item>
        <ion-label>只读</ion-label>
        <component-demo type="EDITOR" path="/editor/app-switch/readonly.json"/>
      </ion-item>
    </ion-list>
  </ion-content>
</ion-app>