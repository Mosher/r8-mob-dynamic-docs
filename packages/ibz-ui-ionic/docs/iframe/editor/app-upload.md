---
layout: component-iframe-layout
---

<ion-app class="iframe-html">
  <ion-header translucent>
    <ion-toolbar>
      <ion-title>文件上传</ion-title>
    </ion-toolbar>
  </ion-header>
  <ion-content fullscreen>
    <ion-list>
    <ion-item-group>
      <ion-item-divider>
        <ion-label>默认</ion-label>
      </ion-item-divider>
      <ion-item>
        <component-demo type="EDITOR" path="/editor/app-upload/default.json"/>
      </ion-item>
      <ion-item-divider>
        <ion-label>禁用</ion-label>
      </ion-item-divider>
      <ion-item>
        <component-demo type="EDITOR" path="/editor/app-upload/disabled.json"/>
      </ion-item>
       <ion-item-divider>
        <ion-label>只读</ion-label>
      </ion-item-divider>
      <ion-item>
        <component-demo type="EDITOR" path="/editor/app-upload/readonly.json"/>
      </ion-item>
      <ion-item-divider>
        <ion-label>文件大小限制</ion-label>
      </ion-item-divider>
      <ion-item>
        <component-demo type="EDITOR" path="/editor/app-upload/sizeLimit.json"/>
      </ion-item>
      <ion-item-divider>
        <ion-label>文件个数限制</ion-label>
      </ion-item-divider>
      <ion-item>
        <component-demo type="EDITOR" path="/editor/app-upload/numberLimit.json"/>
      </ion-item>
      <ion-item-divider>
        <ion-label>图片上传（单选）</ion-label>
      </ion-item-divider>
      <ion-item>
        <component-demo type="EDITOR" path="/editor/app-upload/picture.json"/>
      </ion-item>
      <ion-item-divider>
        <ion-label>图片上传（多选）</ion-label>
      </ion-item-divider>
      <ion-item>
        <component-demo type="EDITOR" path="/editor/app-upload/pictureList.json"/>
      </ion-item>
      <ion-item-divider>
        <ion-label>图片上传（电子签名）</ion-label>
      </ion-item-divider>
      <ion-item>
        <component-demo type="EDITOR" path="/editor/app-upload/sign.json"/>
      </ion-item>
      </ion-item-group>
    </ion-list>
  </ion-content>
</ion-app>
