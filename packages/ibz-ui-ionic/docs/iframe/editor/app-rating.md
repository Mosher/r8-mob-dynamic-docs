---
layout: component-iframe-layout
---

<ion-app class="iframe-html">
  <ion-header translucent>
    <ion-toolbar>
      <ion-title>评分器</ion-title>
    </ion-toolbar>
  </ion-header>
  <ion-content fullscreen>
    <ion-list>
      <ion-item>
        <ion-label>默认</ion-label>
        <component-demo type="EDITOR" path="/editor/app-rating/default.json"/>
      </ion-item>
      <ion-item>
        <ion-label>禁用</ion-label>
        <component-demo type="EDITOR" path="/editor/app-rating/disabled.json"/>
      </ion-item>
      <ion-item>
        <ion-label>只读</ion-label>
        <component-demo type="EDITOR" path="/editor/app-rating/readonly.json"/>
      </ion-item>
      <ion-item>
        <ion-label>图标大小</ion-label>
        <component-demo type="EDITOR" path="/editor/app-rating/size.json"/>
      </ion-item>
      <ion-item>
        <ion-label>最大值与最小值</ion-label>
        <component-demo type="EDITOR" path="/editor/app-rating/valueLimit.json"/>
      </ion-item>
      <ion-item>
        <ion-label>步进值</ion-label>
        <component-demo type="EDITOR" path="/editor/app-rating/step.json"/>
      </ion-item>
      <ion-item>
        <ion-label>精度系数</ion-label>
        <component-demo type="EDITOR" path="/editor/app-rating/precision.json"/>
      </ion-item>
    </ion-list>
  </ion-content>
</ion-app>