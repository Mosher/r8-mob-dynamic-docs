---
layout: component-iframe-layout
---

<ion-app class="iframe-html">
  <ion-header translucent>
    <ion-toolbar>
      <ion-title>标签</ion-title>
    </ion-toolbar>
  </ion-header>
  <ion-content fullscreen>
    <ion-list>
      <ion-item>
        <ion-label>默认</ion-label>
        <component-demo type="EDITOR" path="/editor/app-span/default.json"/>
      </ion-item>
      <ion-item>
        <ion-label>禁用</ion-label>
        <component-demo type="EDITOR" path="/editor/app-span/disabled.json"/>
      </ion-item>
      <ion-item>
        <ion-label>精度(数值类型)</ion-label>
        <component-demo type="EDITOR" path="/editor/app-span/precision.json"/>
      </ion-item>
      <ion-item>
        <ion-label>值格式化</ion-label>
        <component-demo type="EDITOR" path="/editor/app-span/valueFormat.json"/>
      </ion-item>
    </ion-list>
  </ion-content>
</ion-app>