---
layout: component-iframe-layout
---

<ion-app class="iframe-html">
  <ion-header translucent>
    <ion-toolbar>
      <ion-title>时间选择</ion-title>
    </ion-toolbar>
  </ion-header>
  <ion-content fullscreen>
    <ion-list>
      <ion-item>
        <ion-label>默认</ion-label>
        <component-demo type="EDITOR" path="/editor/app-date-picker/default.json"/>
      </ion-item>
      <ion-item>
        <ion-label>禁用</ion-label>
        <component-demo type="EDITOR" path="/editor/app-date-picker/disabled.json"/>
      </ion-item>
      <ion-item>
        <ion-label>只读</ion-label>
        <component-demo type="EDITOR" path="/editor/app-date-picker/readonly.json"/>
      </ion-item>
      <ion-item>
        <ion-label>精确到天</ion-label>
        <component-demo type="EDITOR" path="/editor/app-date-picker/day.json"/>
      </ion-item>
      <ion-item>
        <ion-label>精确到小时</ion-label>
        <component-demo type="EDITOR" path="/editor/app-date-picker/hour.json"/>
      </ion-item>
      <ion-item>
        <ion-label>精确到分钟</ion-label>
        <component-demo type="EDITOR" path="/editor/app-date-picker/min.json"/>
      </ion-item>
      <ion-item>
        <ion-label>只有分钟小时</ion-label>
        <component-demo type="EDITOR" path="/editor/app-date-picker/noDay.json"/>
      </ion-item>
    </ion-list>
  </ion-content>
</ion-app>

