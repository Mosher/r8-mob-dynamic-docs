---
layout: component-iframe-layout
---

<ion-app class="iframe-html">
  <ion-header translucent>
    <ion-toolbar>
      <ion-title>数据选择</ion-title>
    </ion-toolbar>
  </ion-header>
  <ion-content fullscreen>
    <ion-list>
      <ion-item-group>
          <ion-item-divider>
            <ion-label>默认</ion-label>
          </ion-item-divider>
          <ion-item>
            <component-demo type="EDITOR" path="/editor/app-data-picker/default.json"/>
          </ion-item>
          <ion-item-divider>
            <ion-label>禁用</ion-label>
          </ion-item-divider>
          <ion-item>
            <component-demo type="EDITOR" path="/editor/app-data-picker/disabled.json"/>
          </ion-item>
          <ion-item-divider>
            <ion-label>只读</ion-label>
          </ion-item-divider>
          <ion-item>
            <component-demo type="EDITOR" path="/editor/app-data-picker/readonly.json"/>
          </ion-item>
          <ion-item-divider>
            <ion-label>弹出框宽高</ion-label>
          </ion-item-divider>
          <ion-item>
            <component-demo type="EDITOR" path="/editor/app-data-picker/range.json"/>
          </ion-item>
        </ion-item-group>
    </ion-list>
  </ion-content>
</ion-app>