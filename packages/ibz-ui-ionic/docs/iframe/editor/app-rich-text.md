---
layout: component-iframe-layout
---

<ion-app class="iframe-html">
  <ion-header translucent>
    <ion-toolbar>
      <ion-title>HTML编辑器</ion-title>
    </ion-toolbar>
  </ion-header>
  <ion-content fullscreen>
    <ion-list>
      <ion-list-header>
          默认
      </ion-list-header>
      <ion-item>
        <ion-label></ion-label>
        <component-demo type="EDITOR" path="/editor/app-rich-text/default.json"/>
      </ion-item>
    </ion-list>
    <ion-list>
      <ion-list-header>
          禁用
      </ion-list-header>
      <ion-item>
        <ion-label></ion-label>
        <component-demo type="EDITOR" path="/editor/app-rich-text/disabled.json"/>
      </ion-item>
    </ion-list>
    <ion-list>
      <ion-list-header>
          只读
      </ion-list-header>
      <ion-item>
        <ion-label></ion-label>
        <component-demo type="EDITOR" path="/editor/app-rich-text/readonly.json"/>
      </ion-item>
    </ion-list>
  </ion-content>
</ion-app>