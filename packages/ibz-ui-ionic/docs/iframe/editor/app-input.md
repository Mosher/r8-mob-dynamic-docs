---
layout: component-iframe-layout
---

<ion-app class="iframe-html">
  <ion-header translucent>
    <ion-toolbar>
      <ion-title>输入框</ion-title>
    </ion-toolbar>
  </ion-header>
  <ion-content fullscreen>
    <ion-list>
      <ion-item>
        <ion-label>默认</ion-label>
        <component-demo type="EDITOR" path="/editor/app-input/default.json"/>
      </ion-item>
      <ion-item>
        <ion-label>禁用</ion-label>
        <component-demo type="EDITOR" path="/editor/app-input/disabled.json"/>
      </ion-item>
      <ion-item>
        <ion-label>只读</ion-label>
        <component-demo type="EDITOR" path="/editor/app-input/readonly.json"/>
      </ion-item>
      <ion-item>
        <ion-label>精度系数</ion-label>
        <component-demo type="EDITOR" path="/editor/app-input/precision.json"/>
      </ion-item>
      <ion-item>
        <ion-label>最大值与最小值（数值框）</ion-label>
        <component-demo type="EDITOR" path="/editor/app-input/valueLimit.json"/>
      </ion-item>
      <ion-item>
        <ion-label>最大长度</ion-label>
        <component-demo type="EDITOR" path="/editor/app-input/lengthLimit.json"/>
      </ion-item>
      <ion-item>
        <ion-label>数值框</ion-label>
        <component-demo type="EDITOR" path="/editor/app-input/number.json"/>
      </ion-item>
      <ion-item>
        <ion-label>密码框</ion-label>
        <component-demo type="EDITOR" path="/editor/app-input/password.json"/>
      </ion-item>
    </ion-list>
  </ion-content>
</ion-app>