---
layout: component-iframe-layout
---

<ion-app class="iframe-html">
  <ion-content fullscreen>
    <component-demo type="VIEW" path="/view/AppMobHtmlView.json"/>
  </ion-content>
</ion-app>