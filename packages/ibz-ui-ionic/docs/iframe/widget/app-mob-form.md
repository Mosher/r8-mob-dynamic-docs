---
layout: component-iframe-layout
---

<ion-app class="iframe-html">
  <ion-header translucent>
    <ion-toolbar>
      <ion-title>表单</ion-title>
    </ion-toolbar>
  </ion-header>
  <ion-content fullscreen>
    <component-demo type="CONTROL" path="/widget/MobForm.json"/>
  </ion-content>
</ion-app>