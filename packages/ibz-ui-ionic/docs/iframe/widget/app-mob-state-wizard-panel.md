---
layout: component-iframe-layout
---

<ion-app class="iframe-html">
  <ion-header translucent>
    <ion-toolbar>
      <ion-title>状态向导面板</ion-title>
    </ion-toolbar>
  </ion-header>
  <ion-content fullscreen>
    <component-demo type="CONTROL" path="/widget/MobStateWizardPanel.json"/>
  </ion-content>
</ion-app>