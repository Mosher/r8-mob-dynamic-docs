---
layout: component-iframe-layout
---

<ion-app class="iframe-html">
  <ion-header translucent>
    <ion-toolbar>
      <ion-title>图表导航栏</ion-title>
    </ion-toolbar>
  </ion-header>
  <ion-content fullscreen>
    <component-demo type="CONTROL" path="/widget/MobChartExpBar.json"/>
  </ion-content>
</ion-app>