# 系统预置

系统预置界面行为，是iBiz模型预定义的一批公共业务处理逻辑行为，包括保存、编辑、新建、删除和刷新等。

系统的预置界面行为其具体实现是由ibz-core包下的`src\ui-core\utils\app-global-action-util.ts`文件里的全局界面行为服务`AppGlobalUtil`维护的。

## 界面行为插件

系统预置的界面行为也可以支持插件，`AppGlobalUtil`在执行全局界面行为时，初始化时把所有界面行为插件缓存下来，根据插件缓存判断一下，如果存在则走插件逻辑，若不存在则走预置几面行为逻辑，关键代码如下：

```ts
/**
 * 全局界面行为服务
 *
 * @export
 * @class AppGlobalUtil
 */
export class AppGlobalUtil {

  /**
   * @description 初始化全局界面行为Map
   * @private
   * @memberof AppGlobalUtil
   */
  private static initGlobalPluginAction() {
    // 获取全部的界面行为并缓存其中配了插件的。
    const appDEUIActions = App.getModel().getAllPSAppDEUIActions();
    if (appDEUIActions && appDEUIActions.length > 0) {
      appDEUIActions.forEach((action: any) => {
        if (action.getPSSysPFPlugin()) {
          this.globalPluginAction.set(action.codeName, action);
        }
      });
    }
    this.isInitPluginAction = true;
  }

  /**
   * @description 执行全局界面行为
   * @static
   * @param {string} tag 界面行为标识
   * @param {IUIDataParam} UIDataParam 操作环境参数
   * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
   * @param {IUIUtilParam} UIUtilParam 操作工具参数
   * @param {*} [contextJO={}] 行为附加上下文
   * @param {*} [paramJO={}] 附加参数
   * @param {string} [srfParentDeName] 应用实体名称
   * @memberof AppGlobalUtil
   */
  public static executeGlobalAction(
    tag: string,
    UIDataParam: IUIDataParam,
    UIEnvironmentParam: IUIEnvironmentParam,
    UIUtilParam: IUIUtilParam,
    contextJO: any = {},
    paramJO: any = {},
    srfParentDeName?: string,
  ) {
    if (!this.isInitPluginAction) {
      this.initGlobalPluginAction();
    }
    if (this.globalPluginAction.get(tag)) {
      const curActionPlugin = this.globalPluginAction.get(tag);
      const importPlugin: any = App.getPluginService().getUIActionByTag(curActionPlugin?.getPSSysPFPlugin()?.pluginCode);
      if (importPlugin) {
        importPlugin().then((importModule: any) => {
          const actionPlugin = new importModule.default(curActionPlugin);
          actionPlugin.execute(UIDataParam, UIEnvironmentParam, UIUtilParam, contextJO, paramJO, srfParentDeName);
        })
      }
    } else {
      // 系统预置界面行为逻辑
    }
  }
}
```

::: tip 提示
关于插件的具体使用可以参考 [插件 > 界面行为插件](./plugin/uiactionPlugin.md#配置界面行为插件)
:::

## 支持情况

<table>
  <thead>
    <tr>
      <th width="100">界面行为名称</th>
      <th width="100">界面行为标识</th>
      <th>说明</th>
      <th width="100">支持情况</th>
    </tr>
  </thead>
  <tr>
    <td>保存</td>
    <td>SAVE</td>
    <td>通过调用数据能力部件的保存方法，来执行数据的保存。</td>
    <td style="text-align: center;">支持</td>
  </tr>
  <tr>
    <td>保存并关闭</td>
    <td>SaveAndExit</td>
    <td>通过调用数据能力部件的保存方法，来执行数据的保存，并在保存结束后关闭该视图。</td>
    <td style="text-align: center;">支持</td>
  </tr>
  <tr>
    <td>保存并新建</td>
    <td>SAVE</td>
    <td>通过调用数据能力部件的保存方法，来执行数据的保存，并在保存结束后清空数据，加载草稿，用来提供新建新数据的能力。</td>
    <td style="text-align: center;">支持</td>
  </tr>
  <tr>
    <td>保存</td>
    <td>SaveAndNew</td>
    <td>通过调用数据能力部件的保存方法，来执行数据的保存。</td>
    <td style="text-align: center;">支持</td>
  </tr>
  <tr>
    <td>编辑</td>
    <td>Edit</td>
    <td>通过调用数据能力部件的opendata方法，来打开对应的编辑视图，用以对当前数据进行修改。</td>
    <td style="text-align: center;">支持</td>
  </tr>
  <tr>
    <td>新建</td>
    <td>New</td>
    <td>通过调用数据能力部件的newdata方法，来打开对应的编辑视图，用以对当前数据进行修改。</td>
    <td style="text-align: center;">支持</td>
  </tr>
  <tr>
    <td>当前流程步骤</td>
    <td>ViewWFStep</td>
    <td>通过调用数据能力部件的wfsubmit方法，来执行提交工作流。完成之后如果判断该视图是非路由打开，则抛出视图值变更事件和视图关闭事件。</td>
    <td style="text-align: center;">支持</td>
  </tr>
  <tr>
    <td>开始流程</td>
    <td>SaveAndStart</td>
    <td>通过调用数据能力部件的wfstart方法，来开启工作流。如果工作流有多个版本，则在开启之前会模态提示选择版本。</td>
    <td style="text-align: center;">支持</td>
  </tr>
  <tr>
    <td>关闭视图</td>
    <td>ViewWFStep</td>
    <td>由于视图和部件都有对应的closeView方法，通过调用当前上下文的closeView方法关闭视图。</td>
    <td style="text-align: center;">支持</td>
  </tr>
  <tr>
    <td>过滤</td>
    <td>ToggleFilter</td>
    <td>控制搜索表单的展示与否的开关。</td>
    <td style="text-align: center;">支持</td>
  </tr>
  <tr>
    <td>拷贝</td>
    <td>Copy</td>
    <td>控制搜索表单的展示与否的开关。</td>
    <td style="text-align: center;">支持</td>
  </tr>
  <tr>
    <td>删除</td>
    <td>Remove</td>
    <td>通过调用数据能力部件的remove方法，删除对应的数据。</td>
    <td style="text-align: center;">支持</td>
  </tr>
  <tr>
    <td>删除并关闭</td>
    <td>RemoveAndExit</td>
    <td>通过调用数据能力部件的remove方法或removeAndExit方法，删除对应的数据，并在删除结束后关闭视图。</td>
    <td style="text-align: center;">支持</td>
  </tr>
  <tr>
    <td>刷新</td>
    <td>Refresh</td>
    <td>通过调用数据能力部件的refresh方法，刷新部件的数据。</td>
    <td style="text-align: center;">支持</td>
  </tr>
  <tr>
    <td>树刷新父数据</td>
    <td>RefreshParent</td>
    <td>通过调用数据能力部件的refresh方法，刷新部件的数据。</td>
    <td style="text-align: center;">支持</td>
  </tr>
  <tr>
    <td>树刷新全部数据</td>
    <td>RefreshAll</td>
    <td>通过调用数据能力部件的refreshAll方法，刷新整个树的全部数据。</td>
    <td style="text-align: center;">支持</td>
  </tr>
  <tr>
    <td>帮助</td>
    <td>HELP</td>
    <td>无</td>
    <td style="text-align: center;">暂未支持</td>
  </tr>
  <tr>
    <td>保存行</td>
    <td>HELP</td>
    <td>无</td>
    <td style="text-align: center;">暂未支持</td>
  </tr>
  <tr>
    <td>查看</td>
    <td>View</td>
    <td>无</td>
    <td style="text-align: center;">暂未支持</td>
  </tr>
  <tr>
    <td>打印</td>
    <td>PRINT</td>
    <td>无</td>
    <td style="text-align: center;">暂未支持</td>
  </tr>
  <tr>
    <td>导出</td>
    <td>ExportExcel</td>
    <td>无</td>
    <td style="text-align: center;">暂未支持</td>
  </tr>
  <tr>
    <td>第一个记录</td>
    <td>FirstRecord</td>
    <td>无</td>
    <td style="text-align: center;">暂未支持</td>
  </tr>
  <tr>
    <td>上一个记录</td>
    <td>PrevRecord</td>
    <td>无</td>
    <td style="text-align: center;">暂未支持</td>
  </tr>
  <tr>
    <td>下一个记录</td>
    <td>NextRecord</td>
    <td>无</td>
    <td style="text-align: center;">暂未支持</td>
  </tr>
  <tr>
    <td>最后一个记录</td>
    <td>LastRecord</td>
    <td>无</td>
    <td style="text-align: center;">暂未支持</td>
  </tr>
  <tr>
    <td>数据导入</td>
    <td>Import</td>
    <td>无</td>
    <td style="text-align: center;">暂未支持</td>
  </tr>
  <tr>
    <td>新建行</td>
    <td>NewRow</td>
    <td>无</td>
    <td style="text-align: center;">暂未支持</td>
  </tr>
  <tr>
    <td>行编辑</td>
    <td>ToggleRowEdit</td>
    <td>无</td>
    <td style="text-align: center;">暂未支持</td>
  </tr>
</table>