# 界面逻辑

对界面行为已有功能的进行扩展，提供额外附加逻辑的能力。界面逻辑由若干个逻辑节点和节点间的逻辑连接组成。可以灵活的搭配组成各种自定义的逻辑。

## 逻辑参数

逻辑参数定义了，界面逻辑过程中维护的公共参数，是各个节点之间共享的变量，可以通过逻辑参数实现逻辑之间的数据交互。

## 逻辑节点

逻辑节点是组成界面逻辑最小的逻辑单元，其节点类型有以下几种：

### 开始处理

开始处理节点作为整个界面逻辑的逻辑起始点而存在，每个界面逻辑必须要有一个开始处理节点，并从它开始延伸去执行其他节点逻辑。开始处理节点本身没有行为能力，但在开始节点之前，界面逻辑会执行逻辑的初始化操作，主要加载逻辑模型，准备逻辑参数，并把数据对象存储给默认逻辑参数

```ts 
  /**
   * 构造函数
   *
   * @param {*} logic 处理逻辑模型对象
   * @param {any[]} args 数据对象
   * @param {*} context 应用上下文
   * @param {*} params 视图参数
   * @param {*} $event 事件源对象
   * @param {*} xData 部件对象
   * @param {*} actioncontext 界面容器对象
   * @param {*} srfParentDeName 关联父应用实体代码名称
   * @memberof UIActionContext
   */
  constructor(
    logic: any,
    UIDataParam: IUIDataParam,
    UIEnvironmentParam: IUIEnvironmentParam,
    UIUtilParam: IUIUtilParam,
  ) {
    // 处理参数
    const {
      args: args,
      actionContext: actionContext,
      event: event,
      xData: xData,
    } = this.beforeExecute(UIDataParam, UIEnvironmentParam, UIUtilParam);
    // 执行逻辑
    this.appContext = UIDataParam?.navContext;
    this.viewParam = UIDataParam?.navParam;
    this.data = args?.length > 0 ? args[0] : {};
    this.otherParams = {
      event: event,
      control: xData,
      container: actionContext,
    };
    // 初始化界面逻辑处理参数
    if (logic.getPSDEUILogicParams() && (logic.getPSDEUILogicParams() as IPSDEUILogicParam[]).length > 0) {
      for (const logicParam of logic.getPSDEUILogicParams() as IPSDEUILogicParam[]) {
        this.paramsMap.set(logicParam.codeName, logicParam.default ? this.data : {});
        if (logicParam.default) this.defaultParamName = logicParam.codeName;
        if (logicParam.navContextParam) this.navContextParamName = logicParam.codeName;
        if (logicParam.navViewParamParam) this.navViewParamParamName = logicParam.codeName;
      }
    }
  }

  /**
   * 执行界面逻辑之前（准备参数）
   *
   * @param UIDataParam 操作数据参数
   * @param UIEnvironmentParam 操作环境参数
   * @param UIUtilParam 操作工具参数
   *
   * @memberof UIActionContext
   */
  public beforeExecute(UIDataParam: IUIDataParam, UIEnvironmentParam: IUIEnvironmentParam, UIUtilParam: IUIUtilParam) {
    let args: any[] = [];
    if (UIDataParam.data) {
      if (Array.isArray(UIDataParam.data)) {
        args = UIDataParam.data;
      } else {
        args = [UIDataParam.data];
      }
    }
    const { sender: actionContext, event: event } = UIDataParam;
    const { ctrl: xData } = UIEnvironmentParam;
    return { args: args, actionContext: actionContext, event: event, xData: xData };
  }
```

### 准备参数处理

准备参数处理，主要是进行逻辑参数之间的赋值。

根据参数操作类型的不同，可以分为，设置变量，拷贝变量，重置变量。

赋值操作中被赋值的对象被称为目标参数，可以选择任意逻辑参数来担任。被赋值对象的属性称为目标属性，即所选逻辑参数的属性。赋值中被取值的对象称为源对象，被取值的字段被称为源属性。这部分关键代码如下：

```ts
  /**
   * 设置参数(根据配置把源逻辑参数的值赋给目标逻辑参数)
   *
   * @param {IPSDEUILogicNode} logicNode 逻辑节点模型数据
   * @param {UIActionContext} actionContext 界面逻辑上下文
   * @memberof AppUILogicPrepareParamNode
   */
  public setParam(logicNode: IPSDEUILogicNode, actionContext: UIActionContext) {
    if (!logicNode || !logicNode.getPSDEUILogicNodeParams()) {
      return;
    }
    for (const nodeParam of logicNode.getPSDEUILogicNodeParams() as IPSDEUILogicNodeParam[]) {
      // 源类型参数和目标逻辑参数缺一跳过不做处理
      if (!nodeParam.getDstPSDEUILogicParam() || !nodeParam.srcValueType) {
        LogUtil.warn(`源类型参数或者目标逻辑参数缺失`);
        continue;
      }
      // 源逻辑参数处理
      const srcParam: any = actionContext.getParam((nodeParam.getSrcPSDEUILogicParam() as IPSDEUILogicParam)?.codeName);
      const srcFieldName = nodeParam.srcFieldName?.toLowerCase?.();
      // 目标逻辑参数处理
      const dstParam: any = actionContext.getParam((nodeParam.getDstPSDEUILogicParam() as IPSDEUILogicParam)?.codeName);
      // 目标逻辑参数属性，有实体拿实体属性codeName,没有就那dstFieldName
      const dstFieldName = nodeParam.dstFieldName?.toLowerCase();
      // 根据srcValueType，对目标逻辑参数的目标属性进行赋值。
      const targetValue = this.computeTargetParam(nodeParam, srcParam, srcFieldName, actionContext);
      if (dstParam && dstFieldName) dstParam[dstFieldName] = targetValue;
    }
  }
```

根据源值类型的不同，可以有不同的取值逻辑。
1. 源逻辑参数，网页请求上下文，当前视图参数，其值从源对象中取
2. 系统全局对象，用户全局对象，应用上下文，数据上下文，其值从视图上下文中取
3. 当前环境参数，其值从应用全局的Environment里取。
4. 直接值，其值就是模型中的直接内容。
5. 无值,其值就是undefined
6. 空值，其值就是null
7. 计算式，其值就是通过计算配置的表达式来得到的结果。其中${xxx}会被替换为data.xxx

```ts
  /**
   * 计算目标值
   *
   * @param {IPSDEUILogicNodeParam} nodeParam 节点参数
   * @param {*} srcParam  源数据
   * @param {string} srcFieldName  源属性
   * @param {ActionContext} actionContext  逻辑上下文
   * @memberof AppUILogicPrepareParamNode
   */
  public computeTargetParam(
    nodeParam: IPSDEUILogicNodeParam,
    srcParam: any,
    srcFieldName: string,
    actionContext: UIActionContext,
  ) {
    let targetValue: any;
    switch (nodeParam.srcValueType) {
      case 'SRCDLPARAM': // 源逻辑参数
      case 'WEBCONTEXT': // 网页请求上下文
      case 'VIEWPARAM': // 当前视图参数
        targetValue = srcParam[srcFieldName];
        break;
      case 'APPLICATION': // 系统全局对象
      case 'SESSION': // 用户全局对象
      case 'APPDATA': // 应用上下文
      case 'DATACONTEXT': // 数据上下文
        const { context } = actionContext;
        targetValue = context[srcFieldName];
        break;
      case 'ENVPARAM': // 当前环境参数
        const Environment = App.getEnvironment();
        targetValue = Environment && Environment[srcFieldName];
        break;
      case 'EXPRESSION': // 计算式
        targetValue = this.computeExpRessionValue(nodeParam, actionContext);
        break;
      case 'SRCVALUE': // 直接值
        targetValue = nodeParam?.srcValue;
        break;
      case 'NONEVALUE': // 无值（NONE）
        targetValue = undefined;
        break;
      case 'NULLVALUE': // 空值（NULL）
        targetValue = null;
        break;
      default:
        LogUtil.warn(`源值类型${nodeParam.srcValueType}暂未支持`);
    }
    return targetValue;
  }
  /**
   * 计算表达式值
   *
   * @param {IPSDEUILogicNodeParam} nodeParam 节点参数
   * @param {ActionContext} actionContext  逻辑上下文
   * @memberof AppUILogicPrepareParamNode
   */
  public computeExpRessionValue(nodeParam: any, actionContext: UIActionContext) {
    let expression: string = nodeParam?.expression;
    const { context, data } = actionContext;
    if (!expression) {
      LogUtil.warn(`表达式不能为空`);
      return;
    }
    try {
      expression = this.translateExpression(expression);
      return eval(expression);
    } catch (error) {
      LogUtil.warn(`表达式计算异常: ${error}`);
      return undefined;
    }
  }

  /**
   * 解析表达式
   *
   * @param {string} expression 表达式
   * @memberof AppUILogicPrepareParamNode
   */
  public translateExpression(expression: string): string {
    if (expression.indexOf('${') != -1 && expression.indexOf('}') != -1) {
      const start: number = expression.indexOf('${');
      const end: number = expression.indexOf('}');
      const contentStr: string = expression.slice(start + 2, end);
      expression = expression.replace(expression.slice(start, end + 1), `data.${contentStr}`);
      return this.translateExpression(expression);
    }
    return expression;
  }
```

### 调用实体界面行为

调用指定的实体界面行为来完成该节点逻辑

### 调用实体行为

调用指定的实体行为来完成该节点逻辑，并把返回的结果存储到指定的逻辑参数里。

### 消息弹窗

弹出消息弹窗供用户选择，并把结果存储到逻辑参数里。根据配置的弹窗的消息类型，按钮类型，标题，消息内容，显示模式，来调用全局消息弹窗服务以呈现不同的弹窗。根据按钮类型来存储弹窗的选择结果。

```ts
  /**
   * 执行节点
   *
   * @param {IPSDEUIMsgBoxLogic} logicNode 逻辑节点模型数据
   * @param {UIActionContext} actionContext 界面逻辑上下文
   * @memberof AppUILogicMsgboxNode
   */
  public async executeNode(logicNode: IPSDEUIMsgBoxLogic, actionContext: UIActionContext) {
    return new Promise<void>(resolve => {
      if (logicNode) {
        const msgBoxParam: any = actionContext.getParam((logicNode.getMsgBoxParam() as IPSDEUILogicParam)?.codeName);
        const options = {
          type: logicNode.msgBoxType?.toLowerCase(),
          title: msgBoxParam?.title ? msgBoxParam.title : logicNode.title,
          content: msgBoxParam?.message ? msgBoxParam.message : logicNode.message,
          buttonType: logicNode.buttonsType?.toLowerCase(),
          showMode: logicNode.showMode?.toLowerCase(),
        };
        const subject: Subject<any> | null = App.getMsgboxService().open(options);
        const subscription = subject?.subscribe((result: any) => {
          resolve(this.handleResponse(logicNode, actionContext, options, result));
          subscription!.unsubscribe();
          subject.complete();
        });
      } else {
        LogUtil.warn('消息弹窗逻辑节点参数不足');
      }
    });
  }

  /**
   * 处理响应
   *
   * @param {IPSDEUIMsgBoxLogic} logicNode 逻辑节点模型数据
   * @param {UIActionContext} actionContext 界面逻辑上下文
   * @param {string} result 响应结果
   * @memberof AppUILogicMsgboxNode
   */
  public handleResponse(logicNode: IPSDEUIMsgBoxLogic, actionContext: UIActionContext, options: any, result: string) {
    const { buttonsType } = logicNode;
    if (
      !Object.is(buttonsType, 'YESNO') &&
      !Object.is(buttonsType, 'YESNOCANCEL') &&
      !Object.is(buttonsType, 'OK') &&
      !Object.is(buttonsType, 'OKCANCEL')
    ) {
      LogUtil.warn(`${buttonsType}未实现`);
      return;
    }
    const msgBoxParam: any = actionContext.getParam((logicNode.getMsgBoxParam() as IPSDEUILogicParam)?.codeName);
    if (msgBoxParam) msgBoxParam.result = result;
    return this.computeNextNodes(logicNode, actionContext);
  }
```

::: tip 提示
消息弹窗的主要逻辑是由全局的消息弹框服务实现，更多逻辑参见 [UI服务 > 消息弹框服务](/uiservice/app-msgbox-service.md#消息弹框配置)
:::

### 直接前台代码

直接执行配置的前台代码片段。其中提供了预置的局部变量如下：
- context：视图上下文
- data：数据对象
- logicNode：逻辑节点模型对象
- actionContext：界面逻辑上下文，含逻辑参数等属性。

## 逻辑连接

节点与节点之间通过逻辑连接来联结的，每个节点可能拥有多个逻辑连接，在一个节点完成之后，会通过依次判断每个逻辑连接的连接条件组，当满足条件组或没有条件组时，才会执行逻辑连接指向的下一个逻辑节点。具体代码如下

```ts
  /**
   * 根据处理连接计算后续逻辑节点
   *
   * @param {*} logicNode 逻辑节点模型数据
   * @param {UIActionContext} actionContext 界面逻辑上下文
   * @memberof AppUILogicNodeBase
   */
  public computeNextNodes(logicNode: any, actionContext: UIActionContext) {
    LogUtil.log(`已完成执行${logicNode?.name}节点，操作参数数据如下:`);
    if (actionContext.paramsMap && actionContext.paramsMap.size > 0) {
      for (const [key, value] of actionContext.paramsMap) {
        if (key && Object.is(key, 'Context')) {
          LogUtil.log(`${key}:`, actionContext.context);
        } else {
          LogUtil.log(`${key}:`, value);
        }
      }
    }
    const result: any = { nextNodes: [], actionContext };
    if (
      logicNode &&
      logicNode.getPSDEUILogicLinks() &&
      (logicNode.getPSDEUILogicLinks() as IPSDEUILogicLink[]).length > 0
    ) {
      for (const logicLink of logicNode.getPSDEUILogicLinks() as IPSDEUILogicLink[]) {
        const nextNode = logicLink.getDstPSDEUILogicNode();
        // 没有连接条件组或有条件组且满足条件组时执行下一个节点
        if (
          !logicLink?.getPSDEUILogicLinkGroupCond?.() ||
          this.computeCond(logicLink.getPSDEUILogicLinkGroupCond() as IPSDEUILogicLinkGroupCond, actionContext)
        ) {
          LogUtil.log(`即将执行${nextNode?.name}节点`);
          result.nextNodes.push(nextNode);
        }
      }
    }
    return result;
  }
```