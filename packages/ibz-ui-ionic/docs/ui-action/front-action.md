# 前台调用

界面行为前台业务行为逻辑，常见有通过不同的方式打开目标视图再进行数据处理。详情如下：

## 处理参数

影响前台调用参数处理的配置有：

- 数据目标：主要数据参数和主键的处理，目前多项数据暂未支持(MULTIDATA)
- 导航参数：导航参数可以自定义后续逻辑中的视图上下文和视图参数。

前台调用处理参数会根据行为操作目标，实体，导航参数等计算得出对应的上下文，视图参数和数据。

如果界面行为时实体的界面行为，那么会根据实体和数据添加默认的导航参数。

1. 数据里是否有实体主键字段的值
  - 如果有：往context里添加`{ '实体代码名称小写' : '%实体主键字段小写%' }`的视图上下文导航参数。
  - 如果没有：往context里添加`{ '实体代码名称小写' : '%实体代码名称小写%' }`的视图上下文导航参数。
2. 往params里添加`{ '实体主键字段小写' : '%实体主键字段小写%' }`的视图参数导航参数
3. 往params里添加`{ '实体主信息字段小写' : '%实体主信息字段小写%' }`的视图参数导航参数
4. 如果数据目标是单项数据，则把args的第一个元素，作为界面行为的数据参数。

具体代码如下：

```ts
  const _args: any[] = Util.deepCopy(args);
  const appDataEntity = this.actionModel.getPSAppDataEntity();
  if (appDataEntity) {
    const entityName = appDataEntity.codeName.toLowerCase();
    const key = (
      ModelTool.getAppEntityKeyField(appDataEntity) as IPSAppDEField
    )?.codeName.toLowerCase();
    const majorKey = (
      ModelTool.getAppEntityMajorField(appDataEntity) as IPSAppDEField
    )?.codeName.toLowerCase();
    if (_args[0]?.[key]) {
      Object.assign(context, { [entityName!]: `%${key}%` });
    } else {
      Object.assign(context, { [entityName!]: `%${entityName}%` });
    }
    Object.assign(params, { [key!]: `%${key}%` });
    Object.assign(params, { [majorKey]: `%${majorKey}%` });
    if (Object.is(actionTarget, 'SINGLEDATA')) {
      data = args[0];
    }
  }
```

如果配置了自定义导航参数，则会在默认导航参数之后处理，因此其中自定义导航参数优先级大于预置导航参数。

```ts
  // 自定义导航参数优先级大于预置导航参数
  const navgiteContexts: IPSNavigateContext[] | null = this.actionModel.getPSNavigateContexts();
  if (navgiteContexts && navgiteContexts.length > 0) {
    const localContext = Util.formatNavParam(navgiteContexts);
    Object.assign(context, localContext);
  }
  const navgiteParams: IPSNavigateParam[] | null = this.actionModel.getPSNavigateParams();
  if (navgiteParams && navgiteParams.length > 0) {
    const localParam = Util.formatNavParam(navgiteParams);
    Object.assign(params, localParam);
  }
```

接着就是计算出最终界面行为执行用的视图上下文参数，视图参数和数据参数，

1. 把context和params里的%%换算成实际对应字段的值。其换算优先级为数据参数 > 视图上下文 > 视图参数。其中如果数据目标是MULTIKEY（多项数据主键），换算时会把数据数组里每一项对应的字段的值拼接在一起，用`,`分隔
2. 把换算过后的params合并到数据参数data里去。
3. 根据操作环境参数UIEnvironmentParam，往data和context里添加srfparentdename和srfparentkey

```ts
if (_this.context) {
  parentContext = _this.context;
}
if (_this.viewparams) {
  parentViewParam = _this.viewparams;
}
context = UIActionTool.handleContextParam(actionTarget, _args, parentContext, parentViewParam, context);
if (Object.is(actionTarget, 'SINGLEDATA')) {
  tempData = UIActionTool.handleActionParam(actionTarget, _args, parentContext, parentViewParam, params);
  Object.assign(data, tempData);
} else {
  data = UIActionTool.handleActionParam(actionTarget, _args, parentContext, parentViewParam, params);
}
context = Object.assign({}, actionContext.context, context);
// 构建srfparentdename和srfparentkey
const parentObj: any = {
  srfparentdename: UIEnvironmentParam.parentDeCodeName ? UIEnvironmentParam.parentDeCodeName : null,
  srfparentkey: UIEnvironmentParam.parentDeCodeName
    ? context[UIEnvironmentParam.parentDeCodeName.toLowerCase()]
    : null,
};
Object.assign(data, parentObj);
Object.assign(context, parentObj);
```

## 打开视图方式逻辑
支持打开重定向视图，通过模型和当前数据计算出要重定向的视图。

看前台处理类型
  - 【OPENHTMLPAGE】HTML页面，打开一个新的浏览器标签页，显示指定的Html路径
  - 【TOP】顶级视图和【WIZARD】打开顶级视图或向导（模态），看视图的打开方式
    -  【INDEXVIEWTAB】顶级容器分页。路由打开
    -  【POPUPMODAL】模式弹出。模态打开
    -  【DRAWER*】抽屉弹出。抽屉打开
    -  【POPUPAPP】独立应用程序打开。浏览器新标签页打开
    -  其他打开方式暂未支持

## 是否关闭编辑视图

如果配置了关闭编辑视图，则在关闭打开的视图后会关闭原有的视图。

## 是否刷新数据

如果配置了关闭编辑视图，则在关闭打开的视图后会刷新原有的视图的数据。

## 后续界面行为

如果配置了后续的界面行为，则在关闭打开的视图后会执行配置的界面行为。

## 是否先保存目标数据

如果界面行为模型的saveTargetFirst为true且存在数据能力部件，则在界面行为逻辑执行之前会先保存当前视图数据，并把保存方法返回的数据作为界面行为的数据参数进行后续处理。

```ts
if (this.actionModel.saveTargetFirst && xData) {
  const result: any = await xData.save(args, false);
  if (Object.is(actionTarget, 'SINGLEDATA')) {
    Object.assign(args[0], result.data);
  } else {
    args = [result.data];
  }
}
```

## 界面逻辑

界面逻辑可以执行一系列附加的逻辑处理，详情看[界面逻辑](./ui-logic.md)。根据界面逻辑附加类型，可以决定界面逻辑的执行时机。
- REPLACE：替换执行，原有的界面行为逻辑不执行，执行对应的界面逻辑来代替。
- AFTER：执行之后，在原有的界面行为执行之后，执行界面逻辑。