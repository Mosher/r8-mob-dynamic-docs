# 标签

展示信息的唯一编辑器，默认的标签可展示基础类型数据、数据类型数据、代码表类型数据，其他类型数据需自行添加插件
<component-iframe router="/iframe/editor/app-span" />

## 界面表现

### 基础用法

```ts
{
  "editorParams": {
    "value": "标签测试"
  },
  "editorType": "SPAN",
  "name": "span"
}
```

### 禁用

通过 `disabled` 属性指定是否禁用标签编辑器,由于标签只展示数据，禁用它将不会显示数据。

```ts
{
  "editorParams": {
    "value": "禁用标签测试",
    "disabled": "true"
  },
  "editorType": "SPAN",
  "name": "span"
}
```

### 精度

当编辑器数据类型为数值类型时，可配置精度系数进行展示。

```ts
{
  "editorParams": {
    "value": "1.2345",
    "precision": "2"
  },
  "editorType": "SPAN",
  "name": "span"
}
```

### 代码表

### 值格式化

```ts
{
  "editorParams": {
    "value": 50,
    "valueFormat": "第#次"
  },
  "editorType": "SPAN",
  "name": "span"
}
```

标签编辑器支持excel的格式化，详情可参考[指令 > 格式化](../directive/format.md)

### Attributes
|<div style="width: 114px;text-align: left;">参数</div>|<div style="width: 114px;text-align: left;">说明</div>|<div style="width: 114px;text-align: left;">类型</div>|<div style="width: 114px;text-align: left;">可选值</div>|<div style="width: 114px;text-align: left;">默认值</div> |
| ----------- | -------- | --------------- | ------ | ------ |
| value       | 绑定值   | string / number | —      | —      |
| disabled    | 是否禁用 | boolean         | —      | false  |
| precision   | 精度系数 | number          | —      | —      |
| valueFormat | 值格式   | string          | —      | —      |

## 控制器

标签特有的数据逻辑有解析代码表与值格式化展示。

### 初始化代码表

将代码表中的tag标识与代码表类型传给标签组件，在组件内部调用代码表服务获取代码表数据。

```ts
/**
   * @description 初始化代码表参数
   * @memberof MobSpanEditorController
   */
  public initCodeListParams() {
    const codeList = (this.editorInstance as IPSCodeListEditor)?.getPSAppCodeList?.();
    if (codeList) {
      Object.assign(this.customProps, {
        tag: codeList.codeName,
        codeListType: codeList.codeListType,
      });
    }
  }
```

### 初始化值格式化

值格式化是标签特有的逻辑，由于有三个地方可以配置值格式化参数，所以此处对该参数做了优先级处理，实体属性上配置的值格式化优先级最低，其次是表单项上配置的值格式化，优先级最高的是编辑器参数上配置的值格式化参数。

```ts
/**
   * @description 初始化值格式化参数
   * @memberof MobSpanEditorController
   */
  public initFormatParams() {
    const appDeField: IPSAppDEField = this.parentItem?.getPSAppDEField?.();
    if (appDeField?.valueFormat) {
      this.customProps.valueFormat = appDeField?.valueFormat;
    }
    if (this.parentItem?.valueFormat) {
      this.customProps.valueFormat = this.parentItem.valueFormat;
    }
    if (this.editorInstance.editorParams?.valueFormat) {
      this.customProps.valueFormat = this.editorInstance.editorParams?.valueFormat;
    }
  }
```

::: tip 提示

更多逻辑参见具体[编辑器基类](./app-editor-base.md)

**参考文件**

packages\ibz-ui-ionic\src\components\common\app-input.tsx

:::
