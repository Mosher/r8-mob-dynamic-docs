# 开关

用于在打开和关闭状态之间进行切换。
<component-iframe router="/iframe/editor/app-switch" />

## 界面表现

### 基础用法

```ts
{
  "editorParams": {
    "value": "1"
  },
  "editorType": "MOBSWITCH",
  "name": "switch"
}
```

### 禁用

无法进行开关切换，且颜色明显不同

```ts
{
  "editorParams": {
    "value": "1",
    "disabled": "true"
  },
  "editorType": "MOBSWITCH",
  "name": "switch"
}
```

### 只读

无法进行开关切换

```ts
{
  "editorParams": {
    "value": "1"
  },
  "editorType": "MOBSWITCH",
  "name": "switch",
  "readOnly": true
}
```


### Attributes

| <div style="width: 114px;text-align: left;">参数</div> | <div style="width: 114px;text-align: left;">说明</div> | <div style="width: 114px;text-align: left;">类型</div> | <div style="width: 114px;text-align: left;">可选值</div> | <div style="width: 114px;text-align: left;">默认值</div> |
| ------------------------------------------------------ | ------------------------------------------------------ | ------------------------------------------------------ | -------------------------------------------------------- | -------------------------------------------------------- |
| value                                                  | 绑定值                                                 | string                                                 | 1/0                                                      | —                                                        |
| disabled                                               | 是否禁用                                               | boolean                                                | —                                                        | false                                                    |
| readonly                                               | 是否只读                                               | boolean                                                | —                                                        | false                                                    |

## 控制器

开关控制器无特殊逻辑。

::: tip 提示

更多逻辑参见具体[编辑器基类](./app-editor-base.md)

**参考文件**

packages\ibz-ui-ionic\src\components\common\app-switch.tsx

:::
