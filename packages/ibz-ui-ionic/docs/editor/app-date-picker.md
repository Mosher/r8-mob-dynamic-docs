# 时间选择

<component-iframe router="/iframe/editor/app-date-picker" />

点击选择时间日期

## 界面表现

### 基础用法

```ts
{
  "editorParams": {},
  "editorType": "MOBDATE",
  "name": "datepicker",
  "placeHolder": "默认时间选择"
}
```

### 禁用

disabled为true时无法选择时间，且颜色明显不同

```ts
{
  "editorParams": {
    "disabled": "true"
  },
  "editorType": "MOBDATE",
  "name": "datepicker",
  "placeHolder": "禁用时间选择"
}
```

### 只读

readonly为true时可查看时间，但无法选择，颜色不变

```ts
{
  "editorParams": {
    "value": "2021-01-01 10:10:10"
  },
  "editorType": "MOBDATE",
  "name": "datepicker",
  "placeHolder": "只读时间选择",
  "readOnly": true
}
```

### 精确到天

可选择年、月、日

```ts
{
  "dateTimeFormat": "YYYY-MM-DD",
  "editorStyle": "Auto6",
  "editorType": "MOBDATE",
  "name": "datepicker",
  "placeHolder": "默认时间选择"
}
```

### 精确到小时

可选择年、月、日、小时

```ts
{
  "dateTimeFormat": "YYYY-MM-DD HH",
  "editorStyle": "Auto8",
  "editorType": "MOBDATE",
  "name": "datepicker",
  "placeHolder": "默认时间选择"
}
```

### 精确到分钟

可选择年、月、日、小时、分钟

```ts
{
  "dateTimeFormat": "YYYY-MM-DD HH:mm",
  "editorStyle": "Auto1",
  "editorType": "MOBDATE",
  "name": "datepicker",
  "placeHolder": "默认时间选择"
}
```

### 只有分钟小时

可选择小时、分钟

```ts
{
  "dateTimeFormat": "HH:mm",
  "editorStyle": "Auto3",
  "editorType": "MOBDATE",
  "name": "datepicker",
  "placeHolder": "默认时间选择"
}
```

### Attributes

| <div style="width: 114px;text-align: left;">参数</div> | <div style="width: 114px;text-align: left;">说明</div> | <div style="width: 114px;text-align: left;">类型</div> | <div style="width: 114px;text-align: left;">可选值</div> | <div style="width: 114px;text-align: left;">默认值</div> |
| ------------- | -------------- | --------------- | ------ | ------------------- |
| value         | 绑定值         | string         | —      | —                   |
| placeholder   | 输入框占位文本 | string          | —      | —                   |
| disabled      | 是否禁用       | boolean         | —      | false               |
| readonly      | 是否只读       | boolean         | —      | false               |
| displayFormat | 时间格式       | string          | —      | YYYY-MM-DD HH:mm:ss |

## 控制器

时间选择编辑器相关编辑器组件都是使用的app-date-picker组件，只是传的参数displayFormat不同，控制器初始化时按照编辑器类型传递相应的时间格式参数。

```ts
/**
   * @description 设置编辑器的自定义参数
   * @memberof MobDatePickerEditorController
   */
  public async setCustomProps() {
    await super.setCustomProps();
    const type = this.editorInstance.editorStyle;
    switch (type) {
      // 只有小时分钟
      case 'Auto3':
        Object.assign(this.customProps, {
          displayFormat: 'HH:mm',
        });
        break;
      // 精确分钟
      case 'Auto1':
        Object.assign(this.customProps, {
          displayFormat: 'YYYY-MM-DD HH:mm',
        });
        break;
      // 精确天
      case 'Auto6':
        Object.assign(this.customProps, {
          displayFormat: 'YYYY-MM-DD',
        });
        break;
      // 精确小时
      case 'Auto8':
        Object.assign(this.customProps, {
          displayFormat: 'YYYY-MM-DD HH',
        });
        break;
      default:
        Object.assign(this.customProps, {
          displayFormat: 'YYYY-MM-DD HH:mm:ss',
        });
        break;
    }
  }
```

::: tip 提示

更多逻辑参见具体[编辑器基类](./app-editor-base.md)

**参考文件**

packages\ibz-ui-ionic\src\components\common\app-date-picker.tsx

:::
