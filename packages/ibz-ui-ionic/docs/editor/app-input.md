# 输入框

用于输入信息，如文本、账号、密码
<component-iframe router="/iframe/editor/app-input" />

## 界面表现

### 基础用法

输入基本数据，类型为文本

```ts
{
  "editorParams": {},
  "editorType": "MOBTEXT",
  "name": "input",
  "placeHolder": "请输入内容"
}
```

### 禁用

disabled为true时无法输入数据，且颜色明显不同

```ts
{
  "editorParams": {
    "disabled": "true"
  },
  "editorType": "MOBTEXT",
  "name": "input",
  "placeHolder": "请输入内容"
}
```

### 只读

readonly为true时同样无法输入数据

```ts
{
  "editorParams": {},
  "editorType": "MOBTEXT",
  "name": "input",
  "placeHolder": "只读内容",
  "readOnly": true
}
```

### 精度系数

编辑器样式为数值框时，可配置编辑器参数中的precision，输入框取消焦点时将自动添加精度

```ts
{
  "editorParams": {
    "precision": "2"
  },
  "editorType": "MOBNUMBER",
  "name": "input",
  "placeHolder": "请输入内容"
}
```

### 最大值与最小值（数值框）

编辑器样式为数值框时，可配置编辑器参数中的maxValue和minValue，点击输入框右侧的添加减少按钮到达界限时按钮将无效

```ts
{
  "editorParams": {
    "minValue": "1",
    "maxValue": "8"
  },
  "editorType": "MOBNUMBER",
  "name": "number",
  "placeHolder": "请输入内容"
}
```

### 最大长度

输入框可输入数据的最大长度，无类型限制

```ts
{
  "editorParams": {
    "maxLength": "4",
    "showMaxLength": "true"
  },
  "editorType": "MOBTEXT",
  "name": "input",
  "placeHolder": "请输入内容"
}
```

### 数值框

只能输入数值类型数据

```ts
{
  "editorType": "MOBNUMBER",
  "name": "number",
  "placeHolder": "请输入数值"
}
```

### 密码框

输入数据无法查看的密码输入框

```ts
{
  "editorType": "MOBPASSWORD",
  "name": "password",
  "placeHolder": "请输入密码"
}
```

### Attributes

| <div style="width: 114px;text-align: left;">参数</div> | <div style="width: 114px;text-align: left;">说明</div> | <div style="width: 114px;text-align: left;">类型</div> | <div style="width: 114px;text-align: left;">可选值</div> | <div style="width: 67px;text-align: left;">默认值</div> |
| ------------------------------------------------------ | ------------------------------------------------------ | ------------------------------------------------------ | -------------------------------------------------------- | ------------------------------------------------------- |
| value                                                  | 绑定值                                                 | string / number                                        | —                                                        | —                                                       |
| placeholder                                            | 输入框占位文本                                         | string                                                 | —                                                        | —                                                       |
| disabled                                               | 是否禁用                                               | boolean                                                | —                                                        | false                                                   |
| type                                                   | 输入框类型                                             | string                                                 | text/number/password                                     | text                                                    |
| readonly                                               | 是否只读                                               | boolean                                                | —                                                        | false                                                   |
| precision                                              | 精度系数                                               | number                                                 | —                                                        | —                                                       |
| maxValue                                               | 最大值                                                 | number                                                 | —                                                        | —                                                       |
| minValue                                               | 最小值                                                 | number                                                 | —                                                        | —                                                       |
| maxLength                                              | 最大长度                                               | number                                                 | —                                                        | —                                                       |
| showMaxLength                                          | 是否显示最大长度提示                                   | boolean                                                | —                                                        | false                                                   |

由于文本框、数值框、密码框与多行文本框的编辑器壳组件都是同一个组件，所以在该组件内还有解析编辑器类型获取相应编辑器组件逻辑。

```tsx
/**
   * @description 设置编辑器组件
   * @memberof AppTextboxEditor
   */
  setEditorComponent() {
    const type = this.editorInstance.editorType;
    switch (type) {
      case 'MOBTEXT':
      case 'MOBNUMBER':
      case 'MOBPASSWORD':
        this.editorComponent = AppInput;
        break;
      case 'MOBTEXTAREA':
        this.editorComponent = AppTextArea;
        break;
      default:
        this.editorComponent = AppInput;
        break;
    }
  }
```

## 控制器

输入框控制器特有的逻辑为解析输入框类型，将数值框类型设为number、密码框设为password。

```ts
/**
   * @description 设置编辑器的自定义参数
   * @memberof MobTextboxEditorController
   */
  public async setCustomProps() {
    await super.setCustomProps();
    const type = this.editorInstance.editorType;
    Object.assign(this.customProps, {
      showMaxLength: this.editorInstance.editorParams?.['showMaxLength'] ? 
      JSON.parse(this.editorInstance.editorParams['showMaxLength'] as string) : false,
    })
    switch (type) {
      case 'MOBNUMBER':
        Object.assign(this.customProps, {
          type: 'number',
        });
        break;
      case 'MOBPASSWORD':
        Object.assign(this.customProps, {
          type: 'password',
        });
        break;
      default:
        Object.assign(this.customProps, {
          type: 'text',
        });
        break;
    }
  }
```

::: tip 提示

更多逻辑参见具体[编辑器基类](./app-editor-base.md)

**参考文件**

packages\ibz-ui-ionic\src\components\common\app-input.tsx

:::
