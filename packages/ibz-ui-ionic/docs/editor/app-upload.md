# 文件上传

选择文件进行上传。
<component-iframe router="/iframe/editor/app-upload" />

## 界面表现

### 基础用法

```ts
{
  "editorParams" : {
  },
  "editorType" : "MOBSINGLEFILEUPLOAD",
  "name" : "upload"
}
```

### 禁用

无法选取文件

```ts
{
  "editorParams" : {
    "disabled": "true"
  },
  "editorType" : "MOBSINGLEFILEUPLOAD",
  "name" : "upload"
}
```

### 只读

按钮隐藏，但可查看文件

```ts
{
  "editorParams" : {
  },
  "editorType" : "MOBSINGLEFILEUPLOAD",
  "name" : "upload",
  "readOnly": true
}
```

### 文件大小限制

文件上传的大小限制，超过将无法上传

```ts
{
  "editorParams": {
    "maxSize": 500
  },
  "editorType": "MOBSINGLEFILEUPLOAD",
  "name": "upload"
}
```

### 文件个数限制

上传文件个数限制

```ts
{
  "editorParams": {
    "limit": "2"
  },
  "editorType": "MOBMULTIFILEUPLOAD",
  "name": "upload"
}
```

### 图片上传（单选）

只能选取单个图片类型文件进行上传

```ts
{
  "editorType": "MOBPICTURE",
  "name": "picture"
}
```

### 图片上传（多选）

可选取多个图片文件进行上传

```ts
{
  "editorType" : "MOBPICTURELIST",
  "name" : "picture"
}
```

### 图片上传（电子签名）

图片上传编辑器的拓展，不选取图片文件，而是直接绘制个性签名，然后保存为图片。

```ts
{
  "editorStyle" : "DZQM",
  "editorType" : "MOBPICTURE",
  "name" : "electronicsignature"
}
```

### Attributes

| <div style="width: 114px;text-align: left;">参数</div> | <div style="width: 114px;text-align: left;">说明</div> | <div style="width: 114px;text-align: left;">类型</div> | <div style="width: 114px;text-align: left;">可选值</div> | <div style="width: 114px;text-align: left;">默认值</div> |
| ------------------------------------------------------ | ------------------------------------------------------ | ------------------------------------------------------ | -------------------------------------------------------- | -------------------------------------------------------- |
| value                                                  | 绑定值                                                 | string                                                 | —                                                        | —                                                        |
| disabled                                               | 是否禁用                                               | boolean                                                | —                                                        | false                                                    |
| readonly                                               | 是否只读                                               | boolean                                                | —                                                        | false                                                    |
| multiple                                               | 是否多选                                               | boolean                                                | —                                                        | false                                                    |
| maxSize                                                | 文件大小                                               | number                                                 | —                                                        | —                                                        |
| limit                                                  | 文件上传数                                             | number                                                 | —                                                        | 1                                                        |
| accept                                                 | 文件选择类型                                           | string                                                 | MIME                                                     | *                                                        |
| uploadType                                             | 文件上传类型                                           | string                                                 | file/image                                               | file                                                     |

由于文件上传相关编辑器不是用的同一个组件，所以绘制时先根据编辑器类型与样式判断应该绘制的组件。

```tsx
/**
   * @description 设置编辑器组件
   * @memberof AppUploadEditor
   */
  setEditorComponent() {
    const { editorType: type, editorStyle: style } = this.editorInstance;
    const editorTypeStyle: string = `${type}${style && style != 'DEFAULT' ? '_' + style : ''}`;
    switch (editorTypeStyle) {
      case 'MOBPICTURE':
      case 'MOBSINGLEFILEUPLOAD':
      case 'MOBPICTURELIST':
      case 'MOBMULTIFILEUPLOAD':
        this.editorComponent = AppUpload;
        break;
      case 'MOBPICTURE_DZQM':
        this.editorComponent = AppSignature;
        break;
    }
  }
```

## 控制器

文件上传编辑器中每个类型都有其特定的参数，控制器初始化时将对应的参数传给组件。

```ts
/**
   * @description 设置编辑器的自定义参数
   * @memberof MobUploadEditorController
   */
  public async setCustomProps() {
    await super.setCustomProps();
    const type = this.editorInstance.editorType;
    switch (type) {
      case 'MOBSINGLEFILEUPLOAD':
        Object.assign(this.customProps, {
          uploadType: 'file',
          multiple: false,
          accept: this.editorInstance.editorParams?.['accept'] ?
            this.editorInstance.editorParams['accept'] : '*',
          limit: this.editorInstance.editorParams?.['limit'] ? 
            JSON.parse(this.editorInstance.editorParams['limit'] as string) : 1,
        });
        break;
      case 'MOBMULTIFILEUPLOAD':
        Object.assign(this.customProps, {
          uploadType: 'file',
          multiple: true,
          accept: this.editorInstance.editorParams?.['accept'] ?
            this.editorInstance.editorParams['accept'] : '*',
          limit: this.editorInstance.editorParams?.['limit'] ? 
            JSON.parse(this.editorInstance.editorParams['limit'] as string) : 9999,
        });
        break;
      case 'MOBPICTURE':
        Object.assign(this.customProps, {
          uploadType: 'picture',
          multiple: false,
          accept: this.editorInstance.editorParams?.['accept'] ?
            this.editorInstance.editorParams['accept'] : 'image/*',
          limit: this.editorInstance.editorParams?.['limit'] ? 
            JSON.parse(this.editorInstance.editorParams['limit'] as string) : 1,
        });
        break;
      case 'MOBPICTURELIST':
        Object.assign(this.customProps, {
          uploadType: 'picture',
          multiple: true,
          accept: this.editorInstance.editorParams?.['accept'] ?
            this.editorInstance.editorParams['accept'] : 'image/*',
          limit: this.editorInstance.editorParams?.['limit'] ? 
            JSON.parse(this.editorInstance.editorParams['limit'] as string) : 9999,
        });
        break;
      default:
        Object.assign(this.customProps, {
          uploadType: 'file',
          multiple: false,
          accept: this.editorInstance.editorParams?.['accept'] ?
            this.editorInstance.editorParams['accept'] : '*',
          limit: this.editorInstance.editorParams?.['limit'] ? 
            JSON.parse(this.editorInstance.editorParams['limit'] as string) : 1,
        });
        break;
    }
  } 
```

::: tip 提示

更多逻辑参见具体[编辑器基类](./app-editor-base.md)

**参考文件**

packages\ibz-ui-ionic\src\components\common\app-upload.tsx

:::
