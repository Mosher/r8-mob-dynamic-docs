# 下拉列表框

以弹出框的形式在一组备选项中进行单选或多项。

<component-iframe router="/iframe/editor/app-dropdown-list" />

## 界面表现

### 基础用法

解析代码边数据为可选项，点击可进行选择

```ts
{
  "editorParams": {
    "value": "food"
  },
  "editorType": "MOBDROPDOWNLIST",
  "name": "dropList",
  "getPSAppCodeList": {
    "codeName": "custom",
    "codeListType": "STATIC"
  }
}
```

### 禁用

disabled为true时无法选择数据，且颜色明显不同

```ts
{
  "editorParams": {
    "disabled": "true",
    "value": "food"
  },
  "editorType": "MOBDROPDOWNLIST",
  "name": "dropList",
  "getPSAppCodeList": {
    "codeName": "custom",
    "codeListType": "STATIC"
  }
}
```

### 只读

readonly为true时可查看数据，但无法选择

```ts
{
  "editorParams": {
    "value": "food"
  },
  "editorType": "MOBDROPDOWNLIST",
  "name": "dropList",
  "getPSAppCodeList": {
    "codeName": "custom",
    "codeListType": "STATIC"
  },
  "readOnly": true
}
```

### 是否多选(alert打开模式)

以弹出框形式打开可选项

```ts
{
  "editorParams": {
    "multiple": "true",
    "value": "food"
  },
  "editorType": "MOBDROPDOWNLIST",
  "name": "dropList",
  "getPSAppCodeList": {
    "codeName": "custom",
    "codeListType": "STATIC"
  }
}
```

### popover打开模式

以气泡形式打开可选项

```ts
{
  "editorParams": {
    "value": "food",
    "openMode": "popover"
  },
  "editorType": "MOBDROPDOWNLIST",
  "name": "dropList",
  "getPSAppCodeList": {
    "codeName": "custom",
    "codeListType": "STATIC"
  }
}
```

### Attributes

| <div style="width: 114px;text-align: left;">参数</div> | <div style="width: 114px;text-align: left;">说明</div> | <div style="width: 114px;text-align: left;">类型</div> | <div style="width: 114px;text-align: left;">可选值</div> | <div style="width: 91px;text-align: left;">默认值</div> |
| ------------------------------------------------------ | ------------------------------------------------------ | ------------------------------------------------------ | -------------------------------------------------------- | ------------------------------------------------------- |
| value                                                  | 绑定值                                                 | string / number                                        | —                                                        | —                                                       |
| disabled                                               | 是否禁用                                               | boolean                                                | —                                                        | false                                                   |
| readonly                                               | 是否只读                                               | boolean                                                | —                                                        | false                                                   |
| multiple                                               | 是否多选                                               | boolean                                                | —                                                        | false                                                   |
| openMode                                               | 打开模式                                               | string                                                 | action-sheet/alert/popover                               | action-sheet                                            |

## 控制器

将代码表标识tag和代码表类型传给组件，数据获取由组件处理，判断编辑器类型是否多选，并将该参数传给下拉列表组件。

```ts
/**
   * @description 设置编辑器的自定义参数
   * @memberof MobDropdownListController
   */
  public async setCustomProps() {
    await super.setCustomProps();
    const type = this.editorInstance.editorType;
    const codeList = (this.editorInstance as IPSCodeListEditor)?.getPSAppCodeList?.();
    this.customProps.context = this.context;
    this.customProps.viewParam = this.viewParam;
    this.customProps.contextData = this.contextData;
    if (codeList) {
      Object.assign(this.customProps, {
        tag: codeList.codeName,
        codeListType: codeList.codeListType,
      });
    }
    if (Object.is('MOBCHECKLIST', type)) {
      Object.assign(this.customProps, {
        multiple: true,
      });
    }
  }
```

::: tip 提示

更多逻辑参见具体[编辑器基类](./app-editor-base.md)

**参考文件**

packages\ibz-ui-ionic\src\components\common\app-dropdown-list.tsx

:::
