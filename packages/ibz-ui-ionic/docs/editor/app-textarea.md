# 多行输入框

通过键盘输入多行内容。
<component-iframe router="/iframe/editor/app-textarea" />

## 界面表现

### 基础用法

与文本框类似，只是可输入多行

```ts
{
  "editorType": "MOBTEXTAREA",
  "name": "textarea",
  "placeHolder": "请输入多行文本"
}
```

### 禁用

无法输入，且颜色明显不同

```ts
{
  "editorParams": {
    "disabled": "true"
  },
  "editorType": "MOBTEXTAREA",
  "name": "textarea",
  "placeHolder": "请输入多行文本"
}
```

### 只读

无法输入

```ts
{
  "editorParams": {
    "value": "只读内容"
  },
  "editorType": "MOBTEXTAREA",
  "name": "textarea",
  "placeHolder": "请输入多行文本",
  "readOnly": true
}
```

### 最大长度

限制输入数据长度

```ts
{
  "editorParams": {
    "maxLength": "4",
    "showMaxLength": "true"
  },
  "editorType": "MOBTEXTAREA",
  "name": "textarea",
  "placeHolder": "请输入多行文本"
}
```

### Attributes

| <div style="width: 114px;text-align: left;">参数</div> | <div style="width: 114px;text-align: left;">说明</div> | <div style="width: 114px;text-align: left;">类型</div> | <div style="width: 114px;text-align: left;">可选值</div> | <div style="width: 114px;text-align: left;">默认值</div> |
| ------------------------------------------------------ | ------------------------------------------------------ | ------------------------------------------------------ | -------------------------------------------------------- | -------------------------------------------------------- |
| value                                                  | 绑定值                                                 | string                                                 | —                                                        | —                                                        |
| placeholder                                            | 提示信息                                               | string                                                 | —                                                        | —                                                        |
| disabled                                               | 是否禁用                                               | boolean                                                | —                                                        | false                                                    |
| readonly                                               | 是否只读                                               | boolean                                                | —                                                        | false                                                    |
| maxLength                                              | 最大长度                                               | number                                                 | —                                                        | —                                                        |
| showMaxLength                                          | 是否显示最大长度提示                                   | boolean                                                | —                                                        | false                                                    |

由于文本框、数值框、密码框与多行文本框的编辑器壳组件都是同一个组件，所以在该组件内还有解析编辑器类型获取相应编辑器组件逻辑。

```tsx
/**
   * @description 设置编辑器组件
   * @memberof AppTextboxEditor
   */
  setEditorComponent() {
    const type = this.editorInstance.editorType;
    switch (type) {
      case 'MOBTEXT':
      case 'MOBNUMBER':
      case 'MOBPASSWORD':
        this.editorComponent = AppInput;
        break;
      case 'MOBTEXTAREA':
        this.editorComponent = AppTextArea;
        break;
      default:
        this.editorComponent = AppInput;
        break;
    }
  }
```

## 控制器

多行文本框控制器与输入框控制器是同一个，且该控制器无多行文本框相关逻辑。

::: tip 提示

更多逻辑参见具体[编辑器基类](./app-editor-base.md)

**参考文件**

packages\ibz-ui-ionic\src\components\common\app-textarea.tsx

:::
