# 评分器

用于对事物进行评级操作。
<component-iframe router="/iframe/editor/app-rating" />

## 界面表现

### 基础用法

```ts
{
  "editorParams" : {
  },
  "editorType" : "MOBRATING",
  "name" : "rating"
}
```

### 禁用

disabled为true时无法输入数据，已选中数据颜色不变，未选中颜色变浅

```ts
{
  "editorParams" : {
    "disabled": "true",
    "value": "3"
  },
  "editorType" : "MOBRATING",
  "name" : "rating"
}
```

### 只读

readonly为true时只显示以选中数据

```ts
{
  "editorParams": {
    "value": "3"
  },
  "editorType": "MOBRATING",
  "name": "rating",
  "readOnly": true
}
```

### 图标大小

配置size编辑器参数可改变星的大小

```ts
{
  "editorParams": {
    "size": "30"
  },
  "editorType": "MOBRATING",
  "name": "rating"
}
```

### 最大值与最小值

可选择的范围

```ts
{
  "editorParams": {
    "maxValue": "8",
    "minValue": "3"
  },
  "editorType": "MOBRATING",
  "name": "rating"
}
```

### 步进值

配置stepValue为0.5时可选择半星

```ts
{
  "editorParams": {
    "stepValue": "0.5",
    "size": "30"
  },
  "editorType": "MOBRATING",
  "name": "rating"
}
```

### Attributes

| <div style="width: 114px;text-align: left;">参数</div> | <div style="width: 114px;text-align: left;">说明</div> | <div style="width: 114px;text-align: left;">类型</div> | <div style="width: 114px;text-align: left;">可选值</div> | <div style="width: 114px;text-align: left;">默认值</div> |
| ------------------------------------------------------ | ------------------------------------------------------ | ------------------------------------------------------ | -------------------------------------------------------- | -------------------------------------------------------- |
| value                                                  | 绑定值                                                 | string / number                                        | —                                                        | —                                                        |
| disabled                                               | 是否禁用                                               | boolean                                                | —                                                        | false                                                    |
| readonly                                               | 是否只读                                               | boolean                                                | —                                                        | false                                                    |
| precision                                              | 精度系数                                               | number                                                 | 0/1                                                      | 0                                                        |
| maxValue                                               | 最大值                                                 | number                                                 | —                                                        | 5                                                        |
| minValue                                               | 最小值                                                 | number                                                 | —                                                        | 1                                                        |
| stepValue                                              | 步进值                                                 | number                                                 | 0.5/1                                                    | 1                                                        |
| size                                                   | 大小                                                   | number                                                 | —                                                        | 20                                                       |

## 控制器

评分器控制器无特殊逻辑。

::: tip 提示

更多逻辑参见具体[编辑器基类](./app-editor-base.md)

**参考文件**

packages\ibz-ui-ionic\src\components\common\app-rating.tsx

:::

