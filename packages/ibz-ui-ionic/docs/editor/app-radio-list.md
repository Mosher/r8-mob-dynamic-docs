# 单选列表框

在一组备选项中进行单选。
<component-iframe router="/iframe/editor/app-radio-list" />

## 界面表现

### 基础用法

```ts
{
  "editorParams": {},
  "editorType": "MOBRADIOLIST",
  "name": "radioList",
  "getPSAppCodeList": {
    "codeName": "custom",
    "codeListType": "STATIC"
  }
}
```

### 禁用

disabled为true时无法选择选项，且颜色明显不同

```ts
{
  "editorParams": {
    "disabled": "true"
  },
  "editorType": "MOBRADIOLIST",
  "name": "radioList",
  "getPSAppCodeList": {
    "modelref": true,
    "path": "PSSYSAPPS/TestMob/PSAPPCODELISTS/CodeList.json"
  }
}
```

### 只读

readonly为true时无法选择选项，且颜色明显不同

```ts
{
  "editorParams": {
    "value": "food"
  },
  "editorType": "MOBRADIOLIST",
  "name": "radioList",
  "readOnly": true,
  "getPSAppCodeList": {
    "modelref": true,
    "path": "PSSYSAPPS/TestMob/PSAPPCODELISTS/CodeList.json"
  }
}
```

### Attributes

| <div style="width: 114px;text-align: left;">参数</div> | <div style="width: 114px;text-align: left;">说明</div> | <div style="width: 114px;text-align: left;">类型</div> | <div style="width: 114px;text-align: left;">可选值</div> | <div style="width: 114px;text-align: left;">默认值</div> |
| ------------------------------------------------------ | ------------------------------------------------------ | ------------------------------------------------------ | -------------------------------------------------------- | -------------------------------------------------------- |
| value                                                  | 绑定值                                                 | string                                                 | —                                                        | —                                                        |
| disabled                                               | 是否禁用                                               | boolean                                                | —                                                        | false                                                    |
| readonly                                               | 是否只读                                               | boolean                                                | —                                                        | false                                                    |

## 控制器

将代码表对象传给编辑器组件，数据解析由组件内部处理。

```ts
/**
   * @description 设置编辑器的自定义参数
   * @memberof MobCheckBoxEditorController
   */
  public async setCustomProps() {
    await super.setCustomProps();
    const codeList = (this.editorInstance as IPSCodeListEditor)?.getPSAppCodeList?.();
    if (codeList) {
      Object.assign(this.customProps, {
        codeList: codeList,
      });
    }
  }
```

::: tip 提示

更多逻辑参见具体[编辑器基类](./app-editor-base.md)

**参考文件**

packages\ibz-ui-ionic\src\components\common\app-radio-list.tsx

:::
