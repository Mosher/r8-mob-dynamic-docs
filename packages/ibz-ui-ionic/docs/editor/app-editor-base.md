# 编辑器基类

与部件视图一样。将ui呈现与数据层分离，ui只进行绘制，编辑器相关的数据逻辑都将在编辑器的控制器上进行处理。

## 控制器

部件绘制编辑器时首先会绘制编辑器各类型对应的壳组件，这个组件初始化时将数据传给控制器，控制器将解析这些数据，这些数据都解析完成之后才会绘制编辑器组件。

| 流程                 | 方法名          | 说明                                                         |
| -------------------- | --------------- | ------------------------------------------------------------ |
| 编辑器模型初始化     | editorModelInit | 填充编辑器实列的实体与代码表                                 |
| 设置编辑器参数       | setEditorParams | 将平台配置的编辑器参数进行解析后传给编辑器组件，包括特殊的导航参数 |
| 设置编辑器自定义参数 | setCustomProps  | 将平台配置有的但不是编辑器参数的相关参加解析后传给编辑器组件，现只有提示信息与只读 |

### 值改变逻辑

当编辑器数据发生改变时，编辑器组件将抛出editorValueChange事件，并将值以字符串的形式传给编辑器壳组件，编辑器壳组件接收到editorValueChange事件后触发控制器中的changeValue方法，changeValue方法向部件抛出EditorEvent事件。

::: tip 参考文件

packages\ibz-core\src\ui-core\controllers\editor\editor-controller-base.ts

:::

## ui层

编辑器属于部件的下一层，其中由于只有特定的部件才具有获取用户输入数据的需求，所以编辑器只存在于表单和面板部件中。以表单为列，表单绘制表单项时绘制编辑器，并将编辑器所需参数都传给编辑器组件，其中包括编辑器对象、导航上下文、导航参数、值等。

绘制编辑器时会根据模型中的编辑器对象去判断具体的编辑器，如编辑器类型为SPAN，编辑器样式为DEFAULT，那么获取到的编辑器组件就是AppSpanEditorComponent。

```tsx
$app-mob-form.tsx

/**
   * @description 绘制编辑器
   * @returns {*}
   * @memberof AppMobForm
   */
  public renderEditor(editor: IPSEditor, parentItem: any): any {
    const targetCtrlComponent: any = App.getComponentService().getEditorComponent(
      editor?.editorType,
      editor?.editorStyle ? editor?.editorStyle : 'DEFAULT',
      editor.getPSSysPFPlugin()
        ? `${editor?.editorType}_${editor?.editorStyle}`
        : undefined,
      this.c.detailsModel[editor.name]?.infoMode
    );
    return h(targetCtrlComponent, {
      editorInstance: editor,
      containerCtrl: this.c.controlInstance,
      parentItem: parentItem,
      contextData: this.c.data,
      contextState: this.c.formState,
      ctrlService: this.c.ctrlService,
      navContext: Util.deepCopy(this.c.context),
      navParam: Util.deepCopy(this.c.viewParam),
      disabled: this.c.detailsModel[editor.name]?.disabled,
      value: this.c.data[editor?.name || ''],
      modelService: this.c.modelService,
      onEditorEvent: (event: IEditorEventParam) => {
        this.c.handleEditorEvent(event);
      },
    });
  }
```

```ts
$app-component-service.ts

/**
   * @description 获取编辑器组件
   * @param {string} editorType 编辑器类型
   * @param {string} editorStyle 编辑器样式
   * @param {string} [pluginCode] 编辑器插件标识
   * @return {string}
   * @memberof AppComponentService
   */
  public getEditorComponent(
    editorType: string,
    editorStyle: string,
    pluginCode?: string,
    infoMode: boolean = false,
  ): any {
    let component: any = AppMobNotSupportedEditorComponent;
    if (pluginCode) {
      component = this.editorMap.get(`${pluginCode}`);
    } else {
      if (infoMode) {
        component = this.computeInfoModeEditor(editorType, editorStyle);
      } else {
        component = this.editorMap.get(`${editorType}_${editorStyle}`);
      }
    }
    return component || AppMobNotSupportedEditorComponent;
  }
```

::: tip

此处如果配置插件就将直接绘制插件，插件相关可参考[插件>编辑器](../plugin/editorPlugin.md)

:::

此处绘制的是编辑器壳组件，每一个编辑器类型都有一个这样的组件，这个组件负责将接收到的数据传给编辑器控制器以及绘制编辑器组件。

```ts
|─ ─ view						                视图
  |─ ─ widget                                   部件
    |─ ─ editor-component                       编辑器组件壳
    	|─ ─ app-editor                         编辑器组件
```

绘制编辑器组件之前，也会对编辑器模型进行判断，通过判断得到相应的编辑器组件，比如图片上传类型的编辑器拓展电子签名，表单绘制它时也是绘制图片上传类型的编辑器壳组件，然后在这个壳组件里通过判断编辑器样式后绘制的电子签名组件。

绘制编辑器组件时将表单项的值与表单是否禁用传给组件，并接收onEditorValueChange事件来触发控制器的changeValue事件。

::: tip 参考文件

packages\ibz-ui-ionic\src\components\editor\editor-component-base.tsx

:::

