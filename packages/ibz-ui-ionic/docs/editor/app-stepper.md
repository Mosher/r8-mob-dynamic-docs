# 步进器

步进器由增加按钮、减少按钮和输入框组成，用于在一定范围内输入、调整数字。
<component-iframe router="/iframe/editor/app-stepper" />

## 界面表现

### 基础用法

```ts
{
  "editorParams": {},
  "editorType": "MOBSTEPPER",
  "name": "stepper"
}
```

### 禁用

无法输入数据也无法点击按钮

```ts
{
  "editorParams": {
    "disabled": "true"
  },
  "editorType": "MOBSTEPPER",
  "name": "stepper"
}
```

### 只读

无法输入数据也无法点击按钮

```ts
{
  "editorParams": {
    "value": "10"
  },
  "editorType": "MOBSTEPPER",
  "readOnly": true,
  "name": "stepper"
}
```

### 范围

点击按钮时会进行范围限定，超过点击将无效

```ts
{
  "editorParams": {
    "maxValue": "10",
    "minValue": "5"
  },
  "editorType": "MOBSTEPPER",
  "name": "stepper"
}
```

### 步进值

点击按钮添加或减少的值

```ts
{
  "editorParams": {
    "stepValue": "0.5"
  },
  "editorType": "MOBSTEPPER",
  "name": "stepper"
}
```

### 精度系数

输入框展示的精度系数

```ts
{
  "editorParams": {
    "precision": "2"
  },
  "editorType": "MOBSTEPPER",
  "name": "stepper"
}
```

### Attributes

| <div style="width: 114px;text-align: left;">参数</div> | <div style="width: 114px;text-align: left;">说明</div> | <div style="width: 114px;text-align: left;">类型</div> | <div style="width: 114px;text-align: left;">可选值</div> | <div style="width: 114px;text-align: left;">默认值</div> |
| ------------------------------------------------------ | ------------------------------------------------------ | ------------------------------------------------------ | -------------------------------------------------------- | -------------------------------------------------------- |
| value                                                  | 绑定值                                                 | string                                                 | —                                                        | —                                                        |
| disabled                                               | 是否禁用                                               | boolean                                                | —                                                        | false                                                    |
| readonly                                               | 是否只读                                               | boolean                                                | —                                                        | false                                                    |
| precision                                              | 精度系数                                               | number                                                 | —                                                        | —                                                        |
| maxValue                                               | 最大值                                                 | number                                                 | —                                                        | —                                                        |
| minValue                                               | 最小值                                                 | number                                                 | —                                                        | —                                                        |
| maxLength                                              | 最大长度                                               | number                                                 | —                                                        | —                                                        |
| stepValue                                              | 步进值                                                 | number                                                 | —                                                        | —                                                        |

## 控制器

步进器控制器无特殊逻辑。

::: tip 提示

更多逻辑参见具体[编辑器基类](./app-editor-base.md)

**参考文件**

packages\ibz-ui-ionic\src\components\common\app-stepper.tsx

:::
