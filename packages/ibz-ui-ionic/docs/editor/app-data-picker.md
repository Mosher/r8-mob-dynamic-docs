# 数据选择

选择配置的选择视图上的数据。

<component-iframe router="/iframe/editor/app-data-picker" />

## 界面表现

### 基础用法

```ts
{
  "editorParams" : {
  },
  "editorType" : "MOBPICKER",
  "name" : "datapicker"
}
```

### 禁用

disabled为true时将不能打开选择视图

```ts
{
  "editorParams": {
    "disabled": "true"
  },
  "editorType": "MOBPICKER",
  "name": "datapicker"
}
```

### 只读

readonly为true时只能查看数据，无法进行选择

```ts
{
  "editorParams": {},
  "editorType": "MOBPICKER",
  "name": "datapicker",
  "readOnly": true
}
```

### 弹出框宽高

弹出框高度与打开模式相关，弹窗宽度影响侧边抽屉打开，弹窗高度影响下方弹框。模态打开将都不影响。

```ts
{
  "editorParams": {
    "dropdownViewHeight": "500",
    "dropdownViewWidght": "0.8"
  },
  "editorType": "MOBPICKER",
  "name": "datapicker"
}
```

### Attributes

| <div style="width: 114px;text-align: left;">参数</div> | <div style="width: 114px;text-align: left;">说明</div> | <div style="width: 114px;text-align: left;">类型</div> | <div style="width: 90px;text-align: left;">可选值</div> | <div style="width: 97px;text-align: left;">默认值</div> |
| ------------------ | ---------------------- | --------------- | ------ | ------ |
| value              | 绑定值                 | string         | —      | —      |
| placeholder        | 输入框占位文本         | string          | —      | —      |
| disabled           | 是否禁用               | boolean         | —      | false  |
| readonly           | 是否只读               | boolean         | —      | false  |
| dropdownViewWidght | 弹窗宽度(侧方弹框生效) | number          | —      | —      |
| dropdownViewHeight | 弹窗高度(下方弹框生效) | number          | —      | —      |

## 控制器

由于数据选择编辑器拥有打开视图相关逻辑，所以将视图相关的参数都传给数据选择组件，解析导航视图与打开视图方式等都由数据选择组件处理。

```ts
/**
   * @description 设置编辑器的自定义参数
   * @memberof MobDataPickerEditorController
   */
  public async setCustomProps() {
    await super.setCustomProps();
    this.customProps.deMajorField = ModelTool.getEditorMajorName(this.editorInstance);
    this.customProps.deKeyField = ModelTool.getEditorKeyName(this.editorInstance);
    this.customProps.context = this.context;
    this.customProps.viewParam = this.viewParam;
    this.customProps.contextData = this.contextData;
    this.customProps.valueItem = this.parentItem?.valueItemName;
    this.customProps.name = this.editorInstance.name;
    this.customProps.pickUpView = await (this.editorInstance as IPSPickerEditor).getPickupPSAppView()?.fill(true);
    this.customProps.linkView = await (this.editorInstance as IPSPicker).getLinkPSAppView()?.fill(true);
  }
```

::: tip 提示

更多逻辑参见具体[编辑器基类](./app-editor-base.md)

**参考文件**

packages\ibz-ui-ionic\src\components\common\app-data-picker.tsx

:::
