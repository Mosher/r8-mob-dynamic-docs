# html编辑器

用于输入复杂的信息、如图片、链接等。
<component-iframe router="/iframe/editor/app-rich-text" />

## 界面表现

### 基础用法

```ts
{
  "editorType": "MOBHTMLTEXT",
  "name": "htmlText1"
}
```

### 禁用

无法输入数据、也无法操作工具栏

```ts
{
  "editorParams": {
    "disabled": "true",
    "value": "<h1>禁用内容</h1>"
  },
  "editorType": "MOBHTMLTEXT",
  "name": "htmlText2"
}
```

### 只读

无法输入数据、也无法操作工具栏

```ts
{
  "editorParams": {
    "value": "<h1>只读内容</h1>"
  },
  "editorType": "MOBHTMLTEXT",
  "name": "htmlText3",
  "readOnly": true
}
```

### Attributes

| <div style="width: 114px;text-align: left;">参数</div> | <div style="width: 114px;text-align: left;">说明</div> | <div style="width: 114px;text-align: left;">类型</div> | <div style="width: 114px;text-align: left;">可选值</div> | <div style="width: 114px;text-align: left;">默认值</div> |
| ----------- | -------------- | --------------- | ------ | ------ |
| value       | 绑定值         | string         | —      | —      |
| placeholder | 输入框占位文本 | string          | —      | —      |
| disabled    | 是否禁用       | boolean         | —      | false  |
| readonly    | 是否只读       | boolean         | —      | false  |

## 控制器

html编辑器控制器无特殊逻辑。

::: tip 提示

更多逻辑参见具体[编辑器基类](./app-editor-base.md)

**参考文件**

packages\ibz-ui-ionic\src\components\common\app-rich-text.tsx

:::
