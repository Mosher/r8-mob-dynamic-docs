# 滑动输入条

滑动输入条，用于在给定的范围内选择一个值。

<component-iframe router="/iframe/editor/app-slider" />

## 界面表现

### 基础用法

```ts
{
  "editorParams": {},
  "editorType": "MOBSLIDER",
  "name": "slider"
}
```

### 禁用

disabled为true时，将无法滑动滑块，且颜色明显不同

```ts
{
  "editorParams": {
    "disabled": "true"
  },
  "editorType": "MOBSLIDER",
  "name": "slider"
}
```

### 只读

无法滑动滑块，但颜色未改变

```ts
{
  "editorParams": {
    "value": "15"
  },
  "editorType": "MOBSLIDER",
  "name": "slider",
  "readOnly": true
}
```

### 范围

支持选择某一数值范围

```ts
{
  "editorParams": {
    "maxValue": "50",
    "minValue": "5"
  },
  "editorType": "MOBSLIDER",
  "name": "slider"
}
```

### 步进值

滑动滑块会以步进值为单位

```ts
{
  "editorParams": {
    "stepValue": "5"
  },
  "editorType": "MOBSLIDER",
  "name": "slider"
}
```

### Attributes

| <div style="width: 114px;text-align: left;">参数</div> | <div style="width: 114px;text-align: left;">说明</div> | <div style="width: 114px;text-align: left;">类型</div> | <div style="width: 114px;text-align: left;">可选值</div> | <div style="width: 114px;text-align: left;">默认值</div> |
| ------------------------------------------------------ | ------------------------------------------------------ | ------------------------------------------------------ | -------------------------------------------------------- | -------------------------------------------------------- |
| value                                                  | 绑定值                                                 | string                                                 | —                                                        | —                                                        |
| disabled                                               | 是否禁用                                               | boolean                                                | —                                                        | false                                                    |
| readonly                                               | 是否只读                                               | boolean                                                | —                                                        | false                                                    |
| precision                                              | 精度系数                                               | number                                                 | —                                                        | —                                                        |
| maxValue                                               | 最大值                                                 | number                                                 | —                                                        | 100                                                      |
| minValue                                               | 最小值                                                 | number                                                 | —                                                        | 0                                                        |
| stepValue                                              | 步进值                                                 | number                                                 | —                                                        | 1                                                        |

## 控制器

滑动输入框控制器无特殊逻辑。

::: tip 提示

更多逻辑参见具体[编辑器基类](./app-editor-base.md)

**参考文件**

$packages\ibz-ui-ionic\src\components\common\app-slider.tsx

:::
