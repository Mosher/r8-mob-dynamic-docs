# 实体移动端地图导航视图

地图导航视图基于地图导航栏部件，在地图部件基础上增加了导航功能。

<component-iframe router="/iframe/view/deexp/app-mob-map-exp-view" />

## 控制器

### 引擎初始化

引擎是R8Mob动态模板中视图的固有逻辑对象，用于处理视图的事件以及视图与部件的交互。

当实体移动端地图导航视图(以下简称：视图)下的所有部件挂载完成后，视图执行挂载逻辑，此时初始化引擎。

引擎初始化完成后，通过视图状态订阅对象(ViewState)通知相应部件加载数据。

```ts
  /**
   * @description 引擎加载
   * @param {*} opts
   * @return {*}  {*}
   * @memberof MobMapExpViewEngine
   */
  public load(opts: any): any {
    super.load(opts);
    if (this.getMapExpBar()) {
      const tag = this.getMapExpBar().name;
      Object.assign(this.view.viewParam, opts);
      this.setViewState2({ tag: tag, action: 'load', viewdata: { ...this.view.viewparams } });
    } else {
      this.isLoadDefault = true;
    }
  }
```

由上述逻辑可知，当视图存在地图导航栏部件时，引擎会该部件执行load行为。

::: tip 提示
实体移动端地图导航视图控制器继承导航视图控制器基类，更多逻辑参见 [导航视图 > 控制器](./de-exp-view.md#控制器)
:::



## UI层

::: tip 提示
实体移动端地图导航视图继承导航视图，更多逻辑参见 [导航视图 > UI层](./de-exp-view.md#UI层)
:::

### 布局

#### 绘制视图主容器内容

重写视图主容器内容，绘制地图导航栏部件插槽。

```ts
  /**
   * @description 视图主容器内容
   * @return {*}
   * @memberof AppDefaultMobMapExpViewLayout
   */
  renderViewMainContainerContent(): any {
    return <div class='view-main-container-content'>{renderSlot(this.ctx.slots, 'mapexpbar')}</div>;
  }
```

::: tip 提示
该视图布局继承导航视图布局，更多逻辑参见 [导航视图 > UI层 > 布局](./de-exp-view.md#布局)
:::