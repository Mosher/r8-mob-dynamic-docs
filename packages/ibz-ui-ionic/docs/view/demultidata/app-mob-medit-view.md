# 实体移动端多表单编辑视图

应用系统中必然存在着业务实体之间的关联关系，在实际情况中存在着需要快速查看明细数据并新建的要求；在此要求之上平台提供了多表单编辑视图的形式在主单据上快速的进行明细数据的建立。

<component-iframe router="/iframe/view/demultidata/app-mob-medit-view" />

## 控制器

### 引擎初始化

引擎是R8Mob动态模板中视图的固有逻辑对象，用于处理视图的事件以及视图与部件的交互。

当实体移动端多表单编辑视图(以下简称：视图)下的所有部件挂载完成后，视图执行挂载逻辑，此时初始化引擎。

引擎初始化完成后，通过视图状态订阅对象(ViewState)通知相应部件加载数据。

核心逻辑如下：

``` ts 
  /**
   * 引擎加载
   *
   * @param {*} [opts={}]
   * @memberof MobMEditView9Engine
   */
  public load(opts: any = {}): void {
    super.load(opts);
    if (this.getMeditviewpanel()) {
      const tag = this.getMeditviewpanel().name;
      Object.assign(this.view.viewParam, opts);
      this.setViewState2({ tag: tag, action: 'load', viewdata: { ...this.view.viewParam } });
    }
  }
```

由上述逻辑可知，当视图存在多表单编辑视图面板部件时，引擎会通知其执行load行为；

### 多编辑表单面板事件分发

判断当前事件名称根据事件名称 抛出对应的视图事件,具体包括加载成功、保存成功、新增成功三种事件。

```ts
  /**
   * 多编辑表单面板事件分发
   *
   * @param {*} [opts={}]
   * @memberof MobMEditView9Engine
   */
  public editviewpanelEvent(eventName: string, args: any) {
    if (Object.is(eventName, MobMEditViewPanelEvents.LOAD_SUCCESS)) {
      this.emitViewEvent(AppViewEvents.DATA_CHANGE, { action: 'load', data: args, status: 'success' });
    }
    if (Object.is(eventName, MobMEditViewPanelEvents.DATA_SAVE)) {
      this.emitViewEvent(AppViewEvents.DATA_CHANGE, { action: 'save', data: args, status: 'success' });
    }
    if (Object.is(eventName, MobMEditViewPanelEvents.ADD_SUCCESS)) {
      this.emitViewEvent(AppViewEvents.DATA_CHANGE, {
        action: MobMEditViewPanelEvents.DATA_CHANGE,
        data: args,
        status: 'success',
      });
    }
  }
```

::: tip 提示
实体移动端多数据视图控制器继承多数据视图控制器基类，更多逻辑参见 [多数据视图 > 控制器](./de-multi-data-view.md#控制器)
:::
## UI层

::: tip 提示
实体移动端多数据视图继承多数据视图，更多逻辑参见 [多数据视图 > UI层](./de-multi-data-view.md#UI层)
:::

### 布局

#### 绘制视图主容器内容

重写视图主容器内容，绘制多编辑表单部件插槽。

```ts
  /**
   * @description 视图主容器内容
   * @return {*}  {any}
   * @memberof AppDefaultMobMEditViewLayout
   */
  renderViewMainContainerContent(): any {
    return <div class='view-main-container-content'>{[renderSlot(this.ctx.slots, 'meditviewpanel')]}</div>;
  }
```

::: tip 提示
该视图布局继承多数据视图布局，更多逻辑参见 [多数据视图 > UI层 > 布局](./de-multi-data-view.md#布局)
:::