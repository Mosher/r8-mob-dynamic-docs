# 多数据视图基类

多数据视图基类是需要同时展示多条业务数据的视图的基类，继承主数据视图。

## 控制器

### 视图初始化

| 逻辑               | 方法名        | 说明                                         |
| ------------------ | ------------- | -------------------------------------------- |
| 初始化视图基础数据 | viewBasicInit | 初始化视图快速分组代码表，详细逻辑见下方描述 |

### 初始化快速分组

初始化快速分组事件（initQuickGroup）是多数据视图基类控制器固有的逻辑对象，在视图初始化时调用，用于初始化**快速分组模型**（用于渲染快速分组UI）数据

主要逻辑：

​	判断当前视图是否启用快速分组以及是否存在快速分组代码表

​	填充快速分组代码表

​	获取代码表服务，加载代码表数据，填充快速分组模型

```ts
  /**
   * @description 初始化快速分组
   * @return {*}
   * @memberof AppDEMultiDataViewController
   */
  public async initQuickGroup() {
    if (!this.isEnableQuickGroup || !this.quickGroupCodeList) {
      return;
    }
    await this.quickGroupCodeList.fill(true);
    try {
      const quickGroupItems: any[] = await App.getCodeListService().getDataItems({
        tag: this.quickGroupCodeList.codeName,
        type: this.quickGroupCodeList.codeListType,
        navContext: this.context,
        navViewParam: this.viewParam,
      });
      this.quickGroupModel = this.handleQuickGroup(quickGroupItems);
    } catch (error: any) {
      LogUtil.log(`----${this.quickGroupCodeList.codeName}----暂未找到`);
    }
  }
```

### 快速搜索

快速搜索事件（quickSearch）是多数据视图基类控制器固有的逻辑对象，用户在搜索栏中输入过滤参数后，UI层将过滤参数传递给控制器，控制器执行快速搜索逻辑。

参数：query（用户输入的搜索值）

核心逻辑：

​	控制器将数据整理到查询参数中，通过视图通讯对象(ViewState)通知多数据部件执行search行为。

```ts
  /**
   * @description 快速搜索
   * @param {string} query
   * @memberof AppDEMultiDataViewController
   */
  public quickSearch(query: string) {
    Object.assign(this.searchParam, { query: query });
    if (this.xDataControlName && this.ctrlRefsMap.get(this.xDataControlName)?.name) {
      this.viewState.next({
        tag: this.ctrlRefsMap.get(this.xDataControlName)?.name,
        action: 'search',
        data: Util.deepCopy(this.searchParam),
      });
    }
  }
```

### 快速分组值变化

快速分组值变化事件（quickGroupValueChange）是多数据视图基类控制器固有的逻辑对象，用户切换快速分组值会触发

参数：event：{data：（快速分组值）}

核心逻辑：UI层将当前选中的快速分组值传递给控制器，控制器将其整合到查询参数中，通过视图通讯对象(ViewState)通知多数据部件执行搜索行为。

```ts
  /**
   * @description 快速分组值变化
   * @param {*} event
   * @memberof AppDEMultiDataViewController
   */
  public quickGroupValueChange(event: any) {
    if (event) {
      this.quickGroupParam = event.data;
      if (this.isEmitQuickGroupValue && this.xDataControlName && this.ctrlRefsMap.get(this.xDataControlName)?.name) {
        this.viewState.next({
          tag: this.ctrlRefsMap.get(this.xDataControlName)?.name,
          action: 'search',
          data: Util.deepCopy(this.quickGroupParam),
        });
      }
    }
    this.isEmitQuickGroupValue = true;
  }
```

### 搜索表单事件

快速分组值变化事件（quickGroupValueChange）是多数据视图引擎固有的逻辑对象，搜索表单部件在执行加载、搜索、重置操作时调用。

参数：eventName: （事件名称）, args: （事件参数）

主要逻辑：

​	接收搜索表单事件触发根据事件名称触发搜索表单对应事件(加载、搜索、重置)

```ts
  /**
   * @description 搜索表单事件
   * @param {string} eventName
   * @param {*} [args={}]
   * @memberof DEMultiDataViewEngine
   */
  public searchFormEvent(eventName: string, args: any = {}): void {
    if (Object.is(eventName, MobSearchFormEvents.LOAD_DRAFT_SUCCESS)) {
      this.onSearchFormLoad(args);
    }
    if (Object.is(eventName, MobSearchFormEvents.SEARCH)) {
      this.onSearchFormSearch(args);
    }
    if (Object.is(eventName, MobSearchFormEvents.RESET)) {
      this.onSearchFormReset();
    }
  }
```

### 搜索表单加载完成

搜索表单加载完成事件（onSearchFormLoad）是多数据视图引擎固有的逻辑对象，搜索表单部件在执行加载行为时调用。

主要逻辑：

​	判断当前多数据部件是否存在，下发加载(`load`)通知给多数据部件

```ts
  /**
   * @description 搜索表单加载完成
   * @param {*} [args={}]
   * @memberof DEMultiDataViewEngine
   */
  public onSearchFormLoad(args: any = {}): void {
    if (this.getMDCtrl() && this.isLoadDefault) {
      const tag = this.getMDCtrl().name;
      this.setViewState2({ tag: tag, action: 'load', viewdata: { ...this.view.viewParam } });
    }
    this.isLoadDefault = true;
  }
```



### 搜索表单搜索

搜索表单搜索事件（onSearchFormSearch）是多数据视图引擎固有的逻辑对象，搜索表单部件在执行搜索行为时调用。

参数：args：（搜索表单数据）

主要逻辑：判断当前多数据部件是否存在，下发搜索(`search`)通知给多数据部件

```ts
  /**
   * @description 搜索表单搜索
   * @param {*} [args={}]
   * @memberof DEMultiDataViewEngine
   */
  public onSearchFormSearch(args: any = {}): void {
    if (this.getMDCtrl()) {
      const tag = this.getMDCtrl().name;
      this.setViewState2({ tag: tag, action: 'search', viewdata: { ...args } });
    }
  }
```

### 搜索表单重置

搜索表单重置事件（onSearchFormReset）是多数据视图引擎固有的逻辑对象，搜索表单部件在执行重置行为时调用。

主要逻辑：判断当前搜索表单部件是否存在，下发初始化通知给搜索(`loadDraft`)表单

```ts
  /**
   * @description 搜索表单重置
   * @param {*} [args={}]
   * @memberof DEMultiDataViewEngine
   */
  public onSearchFormReset(args: any = {}) {
    if (this.getSearchForm()) {
      const tag = this.getSearchForm().name;
      this.setViewState2({ tag: tag, action: 'loadDraft', viewdata: { ...this.view.viewParam } });
    }
  }
```

### 选中变化

选中变化事件（selectionChange）是多数据视图引擎固有的逻辑对象，多数据部件在选择时调用。

参数：UIDataParam: {data:（选中数据）}

主要逻辑：

- 执行视图事件(event)钩子，事件名称为`onDatasChange`（可参见[视图基类 > 视图事件处理](../de/de-view.md#视图事件处理))

- 计算工具栏权限状态(可参见[视图基类 > 计算工具栏状态](../de/de-view.md#计算工具栏状态))

```ts
  /**
   * @description 选中变化
   * @param {IUIDataParam} UIDataParam
   * @memberof DEMultiDataViewEngine
   */
  public selectionChange(UIDataParam: IUIDataParam): void {
    const args = UIDataParam.data;
    if (this.view) {
      this.emitViewEvent(AppViewEvents.DATA_CHANGE, UIDataParam);
    }
    const state = args && args.length > 0 && !Object.is(args?.[0].srfkey, '') ? false : true;
    this.calcToolbarItemState(state);
    if (args && args.length > 0) {
      this.calcToolbarItemAuthState(args[0]);
    } else {
      this.calcToolbarItemAuthState(null);
    }
  }
```



### 多数据部件加载前

多数据部件加载前（MDCtrlBeforeLoad）是多数据视图引擎固有的逻辑对象，多数据部件在加载前调用。

参数：data：需要合并的参数

核心逻辑：

​	合并快速搜索、快速代码表、搜索表单、排序等其他过滤参数

```
  /**
   * @description 多数据部件加载前
   * @param {*} [data={}]
   * @return {*}  {void}
   * @memberof DEMultiDataViewEngine
   */
  public MDCtrlBeforeLoad(data: any = {}): void {
    if (!data || !data.navParam) {
      return;
    }
    // 快速搜索
    if (this.view && this.view.searchParam && Object.keys(this.view.searchParam).length > 0) {
      Object.keys(this.view.searchParam).forEach((key: string) => {
        data.navParam.viewParam[key] = this.view.searchParam[key];
      });
    }
    // 快速代码表
    if (this.view && this.view.quickGroupParam && Object.keys(this.view.quickGroupParam).length > 0) {
      Object.keys(this.view.quickGroupParam).forEach((key: string) => {
        data.navParam.viewParam[key] = this.view.quickGroupParam[key];
      });
    }
    const otherQueryParam: any = {};
    if (this.getSearchForm()) {
      Object.assign(otherQueryParam, this.getSearchForm().getData());
    }
    if (this.view && this.view.sortValue) {
      Object.assign(otherQueryParam, this.view.sortValue);
    }
    if (otherQueryParam && Object.keys(otherQueryParam).length > 0) {
      Object.keys(otherQueryParam).forEach((key: string) => {
        data.navParam.viewParam[key] = otherQueryParam[key];
      });
    }
  }
```



### 多数据部件加载完成

多数据部件加载完成（MDCtrlLoad）是多数据视图引擎固有的逻辑对象，多数据部件在加载完成后调用。

参数：args：加载的数据

主要逻辑：

- 执行视图事件(event)钩子，事件名称为`onLoad`（可参见[视图基类 > 视图事件处理](../de/de-view.md#视图事件处理))

- 计算工具栏权限状态(可参见[视图基类 > 计算工具栏状态](../de/de-view.md#计算工具栏状态))

```
  /**
   * @description 多数据部件加载完成
   * @param {any[]} args
   * @memberof DEMultiDataViewEngine
   */
  public MDCtrlLoad(args: any[]) {
    if (this.view) {
      this.emitViewEvent(AppViewEvents.LOAD, args);
    }
    this.calcToolbarItemState(true);
    this.calcToolbarItemAuthState(null);
  }
```



::: tip 提示

如果该视图配置有界面计数器，则还会触发计数器刷新逻辑，详情参见 [视图 > 控制器 > 计数器刷新](../app-view-base.md#计数器刷新)。

:::

## UI层

### 渲染快速分组

如果该视图配置有快速分组代码表，则UI层依据控制器初始化好的快速分组模型数据绘制快速分组栏。

```ts
  /**
   * @description 渲染快速分组
   * @return {*}
   * @memberof DEMultiDataViewComponentBase
   */
  public renderQuickGroup() {
    if (this.c.viewInstance.enableQuickGroup) {
      return (
        <ion-header class='app-quick-group'>
          <ion-toolbar>
            <app-quick-group
              items={this.c.quickGroupModel}
              onValueChange={(event: any) => {
                this.quickGroupValueChange(event);
              }}
            />
          </ion-toolbar>
        </ion-header>
      );
    }
  }
```

### 快速搜索值变化

当用户切换快速分组值时，UI层传递选中数据给控制器，由控制器执行后续逻辑。

```ts
  /**
   * @description 快速搜索值变化
   * @param {*} 快速分组抛出值
   * @memberof DEMultiDataViewComponentBase
   */
  public quickGroupValueChange(data: any) {
    if (this.c.quickGroupValueChange && this.c.quickGroupValueChange instanceof Function) {
      this.c.quickGroupValueChange(data);
    }
  }
```

### 渲染快速搜索

如果该视图配置支持快速搜索，则UI层会绘制快速搜索栏。

当用户输入过滤参数时，UI层将过滤参数传递给控制器，由控制器执行后续搜索逻辑。

```ts
  /**
   * @description 绘制快速搜索
   * @return {*}
   * @memberof DEMultiDataViewComponentBase
   */
  public renderQuickSearch(): any {
    return <app-quick-search onQuickSearch={(event: any) => this.quickSearch(event)}></app-quick-search>;
  }
```

### 快速搜索

当用户输入查询字符串点击快速搜索，UI层传递选中数据给控制器，由控制器执行后续逻辑。

```ts
  /**
   * @description 快速搜索
   * @param {*} query 快速搜索值
   * @memberof DEMultiDataViewComponentBase
   */
  public quickSearch(query: string) {
    if (this.c.quickSearch && this.c.quickSearch instanceof Function) {
      this.c.quickSearch(query);
    }
  }
```

### 渲染视图部件

重写视图基类里的renderViewControls方法，支持了快速搜索和快速分组的绘制。

```ts
  /**
   * @description 渲染视图部件
   * @return {*}
   * @memberof DEMultiDataViewComponentBase
   */
  public renderViewControls() {
    const viewControls: any = super.renderViewControls();
    if(this.c.viewInstance.enableQuickGroup){
      Object.assign(viewControls, {
        quickGroupSearch: () => {
          return this.renderQuickGroup();
        },
      });
    }
    if (this.c.viewInstance.enableQuickSearch) {
      Object.assign(viewControls, {
        quickSearch: () => this.renderQuickSearch(),
      });
    }
    return viewControls;
  }
```

### 额外部件参数

重写基类里的extraCtrlParam方法，在计算目标部件所需参数时会调用该方法去计算额外的参数，如果部件类型为搜索表单SEARCHFORM，则给搜索表单部件的输入参数里加入expandSearchForm属性，值为该视图的expandSearchForm属性。

```ts
  /**
   * @description 额外部件参数
   * @param {*} ctrlProps 部件参数
   * @param {IPSControl} controlInstance 部件实例
   * @memberof DEMultiDataViewComponentBase
   */
  public extraCtrlParam(ctrlProps: any, controlInstance: IPSControl) {
    super.extraCtrlParam(ctrlProps, controlInstance);
    if (controlInstance?.controlType == 'SEARCHFORM') {
      Object.assign(ctrlProps, {
        expandSearchForm: this.c.viewInstance.expandSearchForm,
      });
    }
  }
```

::: tip 提示

多数据视图基类UI层继承主数据视图基类UI层，更多逻辑参见参见 [主数据视图 > UI层](../de/de-view.md#ui层)。

:::

### 布局

#### 绘制视图主容器头部

重写基类里的绘制视图主容器头部方法,绘制快速分组、快速搜索、搜索表单、快速搜索表单等插槽。

```ts
  /**
   *
   * @description 视图主容器头部
   * @return {*}
   * @memberof MultiDataViewLayoutComponentBase
   */
  renderViewMainContainerHeader(): any {
    return (
      <div class='view-main-container-header'>
        {[
          renderSlot(this.ctx.slots, 'quickGroupSearch'),
          renderSlot(this.ctx.slots, 'quickSearch'),
          renderSlot(this.ctx.slots, 'searchform'),
          renderSlot(this.ctx.slots, 'quicksearchform'),
        ]}
      </div>
    );
  }
```

::: tip 提示
多数据视图基类布局继承主数据视图基类布局，更多逻辑参见 [主数据视图 > UI层 > 布局](../de/de-view.md#布局)
:::