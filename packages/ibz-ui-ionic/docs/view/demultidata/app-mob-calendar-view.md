# 实体移动端日历视图

实体日历视图由工具栏、搜索表单、日历等部件来组成，工具栏是对实体相应的操作，搜索表单是对日历的过滤，日历部件呈现与时间安排相关的信息。

<component-iframe router="/iframe/view/demultidata/app-mob-calendar-view" />

## 控制器

### 引擎初始化

引擎是R8Mob动态模板中视图的固有逻辑对象，用于处理视图的事件以及视图与部件的交互。

当实体移动端多数据视图(以下简称：视图)下的所有部件挂载完成后，视图执行挂载逻辑，此时初始化引擎。

引擎初始化完成后，通过视图状态订阅对象(ViewState)通知相应部件加载数据。

核心逻辑如下：

``` ts 
  /**
   * @description 引擎加载
   * @param {*} [opts={}]
   * @memberof DEMultiDataViewEngine
   */
  public load(opts: any = {}): void {
    super.load(opts);
    if (this.getSearchForm()) {
      const tag = this.getSearchForm().name;
      this.setViewState2({ tag: tag, action: 'loadDraft', viewdata: { ...this.view.viewParam } });
    } else if (this.getMDCtrl() && this.isLoadDefault) {
      const tag = this.getMDCtrl().name;
      Object.assign(this.view.viewParam, opts);
      this.setViewState2({ tag: tag, action: 'load', viewdata: { ...this.view.viewParam } });
    } else {
      this.isLoadDefault = true;
    }
  }
```

由上述逻辑可知，当视图存在搜索表单时，引擎会通知搜索表单执行loadDraft行为，搜索表单执行相应行为完成后通知多数据部件执行load行为；若不存在搜索表单引擎则会直接通知多数据部件执行load行为。

### 日历数据加载完成

抛出视图加载完成(`load`)和数据变化(`onDatasChange`)事件

```ts
  /**
   * 日历数据加载完成
   *
   * @param {*} args
   * @memberof MobCalendarViewEngine
   */
  public onCalendarLoad(arg: any): void {
    this.emitViewEvent(AppViewEvents.LOAD, arg);
    this.emitViewEvent(AppViewEvents.DATA_CHANGE, arg);
  }
```

::: tip 提示
实体移动端日历视图控制器继承多数据视图控制器基类，更多逻辑参见 [多数据视图 > 控制器](./de-multi-data-view.md#控制器)
:::

## UI层

```ts
  /**
   *
   * @description 视图主容器内容
   * @return {*}  {*}
   * @memberof AppDefaultMobCalendarViewLayout
   */
  renderViewMainContainerContent(): any {
    return <div class='view-main-container-content'>{[renderSlot(this.ctx.slots, 'calendar')]}</div>;
  }
```

::: tip 提示
实体移动端日历视图继承多数据视图，更多逻辑参见 [多数据视图 > UI层](./de-multi-data-view.md#UI层)
:::

### 布局

#### 绘制视图主容器内容

重写视图主容器内容，绘制日历部件插槽。

```ts
  /**
   *
   * @description 视图主容器内容
   * @return {*}  {*}
   * @memberof AppDefaultMobCalendarViewLayout
   */
  renderViewMainContainerContent(): any {
    return <div class='view-main-container-content'>{[renderSlot(this.ctx.slots, 'calendar')]}</div>;
  }
```

::: tip 提示
该视图布局继承多数据视图布局，更多逻辑参见 [多数据视图 > UI层 > 布局](./de-multi-data-view.md#布局)
:::