# 实体移动端多数据视图

实体移动端多数据视图具备常规的数据列表清单界面，展示了当前业务实体的数据。由移动端多数据部件、工具栏、搜索表单等部件来组成。工具栏是对应实体相应的操作，搜索表单是对业务数据的过滤，移动端多数据部件用来呈现当前业务实体的数据。

<component-iframe router="/iframe/view/demultidata/app-mob-md-view" />

## 控制器

### 引擎初始化

引擎是R8Mob动态模板中视图的固有逻辑对象，用于处理视图的事件以及视图与部件的交互。

当实体移动端多数据视图(以下简称：视图)下的所有部件挂载完成后，视图执行挂载逻辑，此时初始化引擎。

引擎初始化完成后，通过视图状态订阅对象(ViewState)通知相应部件加载数据。

核心逻辑如下：

``` ts 
  /**
   * @description 引擎加载
   * @param {*} [opts={}]
   * @memberof DEMultiDataViewEngine
   */
  public load(opts: any = {}): void {
    super.load(opts);
    if (this.getSearchForm()) {
      const tag = this.getSearchForm().name;
      this.setViewState2({ tag: tag, action: 'loadDraft', viewdata: { ...this.view.viewParam } });
    } else if (this.getMDCtrl() && this.isLoadDefault) {
      const tag = this.getMDCtrl().name;
      Object.assign(this.view.viewParam, opts);
      this.setViewState2({ tag: tag, action: 'load', viewdata: { ...this.view.viewParam } });
    } else {
      this.isLoadDefault = true;
    }
  }
```

由上述逻辑可知，当视图存在搜索表单时，引擎会通知搜索表单执行loadDraft行为，搜索表单执行相应行为完成后通知多数据部件执行load行为；若不存在搜索表单引擎则会直接通知多数据部件执行load行为。

### 多数据部件事件处理

判断当前事件是否为行选中 若是 则打开编辑操作。

```ts
  /**
   * 多数据部件事件处理
   *
   * @param {string} eventName
   * @param {any[]} args
   * @memberof MobMDViewEngine
   */
  public MDCtrlEvent(eventName: string, args: any): void {
    super.MDCtrlEvent(eventName, args);
    if (Object.is(eventName, MobMDCtrlEvents.ROW_SELECTED)) {
      this.doEdit(args);
    }
  }
```

### 多数据项界面_编辑操作

解构参数，打开编辑视图。

```ts
  /**
   * 多数据项界面_编辑操作
   *
   * @param {*} [params={}]
   * @returns {void}
   * @memberof MobMDViewEngine
   */
  public doEdit(params: IUIDataParam): void {
    const { data, event, navParam, sender } = params;
    if (data) {
      ViewTool.opendata(data, this.view, data[0], navParam, event, sender);
    }
  }
```

::: tip 提示
实体移动端多数据视图控制器继承多数据视图控制器基类，更多逻辑参见 [多数据视图 > 控制器](./de-multi-data-view.md#控制器)
:::

## UI层

::: tip 提示
实体移动端多数据视图继承多数据视图，更多逻辑参见 [多数据视图 > UI层](./de-multi-data-view.md#UI层)
:::

### 布局

#### 绘制视图主容器内容

重写视图主容器内容，绘制多数据部件插槽。

```ts
  /**
   * @description 视图主容器内容
   * @return {*}  {any}
   * @memberof AppDefaultMobMDViewLayout
   */
  renderViewMainContainerContent(): any {
    return <div class='view-main-container-content'>{renderSlot(this.ctx.slots, 'mdctrl')}</div>;
  }
```

::: tip 提示
该视图布局继承多数据视图布局，更多逻辑参见 [多数据视图 > UI层 > 布局](./de-multi-data-view.md#布局)
:::