# 实体移动端数据多项选择视图

实体移动端数据多项选择视图用于展示多数据视图的在多项选择场景下的界面呈现。由选择视图面板部件组成，通过选择视图面板对应的视图来呈现当前业务实体的数据。

<component-iframe router="/iframe/view/pickup/app-mob-mpickup-view" />

## 控制器

### 引擎初始化

引擎是R8Mob动态模板中视图的固有逻辑对象，用于处理视图的事件以及视图与部件的交互。

当实体移动端数据多项选择视图(以下简称：视图)下的所有部件挂载完成后，视图执行挂载逻辑，此时初始化引擎。

引擎初始化完成后，通过视图状态订阅对象(ViewState)通知相应部件加载数据。

核心逻辑如下：

``` ts 
  /**
   * 引擎加载
   *
   * @memberof MobPickupViewEngine
   */
  public load(opts?: any): void {
    super.load(opts);
    if (this.view) {
      this.view.viewSelections = [];
    }
  }
```

由上述逻辑可知，数据多项选择视图加载时会情况当前视图选中的数据。

::: tip 提示
实体移动端数据多项选择视图控制器继承实体移动端数据选择视图控制器基类，更多逻辑参见 [实体移动端数据多项选择视图 > 控制器](../pickup/app-mob-pickup-view.md#控制器)
:::
## UI层
::: tip 提示
实体移动端数据多项选择视图继承主数据视图，更多逻辑参见 [主数据视图 > UI层](../de/de-view.md#UI层)
:::

### 布局

#### 绘制视图主容器内容

重写视图主容器内容，绘制选择视图面板部件插槽。

```ts
  /**
   * @description 视图主容器内容
   * @return {*}  {any}
   * @memberof AppDefaultMobMPickupViewLayout
   */
  renderViewMainContainerContent(): any {
    return <div class='view-main-container-content'>{[renderSlot(this.ctx.slots, 'pickupviewpanel')]}</div>;
  }
```

::: tip 提示
该视图布局继承主数据视图布局，更多逻辑参见 [主数据视图 > UI层 > 布局](../de/de-view.md#布局)
:::