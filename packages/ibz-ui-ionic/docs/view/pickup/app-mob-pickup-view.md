# 实体移动端数据选择视图

实体移动端数据选择视图用于展示多数据视图的在单项选择场景下的界面呈现。由选择视图面板部件组成，通过选择视图面板对应的视图来呈现当前业务实体的数据。

<component-iframe router="/iframe/view/pickup/app-mob-pickup-view" />

## 控制器

### 引擎初始化

引擎是R8Mob动态模板中视图的固有逻辑对象，用于处理视图的事件以及视图与部件的交互。

当实体移动端数据选择视图(以下简称：视图)下的所有部件挂载完成后，视图执行挂载逻辑，此时初始化引擎。

引擎初始化完成后，通过视图状态订阅对象(ViewState)通知相应部件加载数据。

核心逻辑如下：

``` ts 
  /**
   * 引擎加载
   *
   * @memberof MobPickupViewEngine
   */
  public load(opts?: any): void {
    super.load(opts);
    if (this.view) {
      this.view.viewSelections = [];
    }
  }
```

由上述逻辑可知，数据选择视图加载时会清空当前视图选中的数据。

### 确定

点击顶部确认按钮会触发该方法，抛出视图事件，参数为视图选中数据 。

```ts
  /**
   * @description 确定
   * @memberof MobPickUpViewController
   */
  public ok(): void {
    this.viewEvent(AppViewEvents.DATA_CHANGE, this.viewSelections);
    this.viewEvent(AppViewEvents.CLOSE, this.viewSelections);
  }
```

### 取消

点击顶部取消按钮会触发该方法，抛出视图事件，参数为视图选中数据 。

```ts
  /**
   * @description 取消
   * @memberof MobPickUpViewController
   */
  public cancel(): void {
    this.viewEvent(AppViewEvents.CLOSE, this.viewSelections);
  }
```

### 值选中变化

设置视图选中值viewSelections。

```ts
  /**
   * 值选中变化
   *
   * @param {any[]} args
   * @memberof MobPickupViewEngine
   */
  public onSelectionChange(ctrlName: any, args: IUIDataParam): void {
    this.view.viewSelections = [];
    this.view.viewSelections = args.data;
  }
```

::: tip 提示
实体移动端数据选择视图控制器继承主数据视图控制器基类，更多逻辑参见 [主数据视图 > 控制器](../de/de-view.md#控制器)
:::

## UI层
### 绘制视图所有部件

重写基类里的绘制视图所有部件方法，新增绘制顶部按钮。

```ts
  /**
   * @description 绘制视图所有部件
   * @return {*}
   * @memberof AppMobPickUpView
   */
  public renderViewControls() {
    const controlObject = super.renderViewControls();
    Object.assign(controlObject, {
      headerButtons: () => this.renderHeaderButtons(),
    });
    return controlObject;
  }
```

### 绘制顶部按钮

顶部按钮包括确认与取消两个按钮。

```ts
  /**
   * @description 绘制顶部按钮
   * @return {*}
   * @memberof AppMobPickUpView
   */
  public renderHeaderButtons() {
    return (
      <ion-toolbar>
        <ion-buttons slot='start'>
          <ion-button size='small' onClick={() => this.c.cancel()}>
          {`${this.$tl('share.cancel','取消')}`}
          </ion-button>
        </ion-buttons>
        <ion-buttons slot='primary'>
          <ion-button size='small' onClick={() => this.c.ok()}>
          {`${this.$tl('share.ok','确认')}`}
          </ion-button>
        </ion-buttons>
        <ion-title>{this.c.viewInstance.caption}</ion-title>
      </ion-toolbar>
    );
  }
```

### 额外部件参数

如果部件类型为选择视图面板部件，给其参数新增是否多选isMultiple属性为false。

```ts
  /**
   * @description 额外部件参数
   * @param {*} ctrlProps 部件参数
   * @param {IPSControl} controlInstance 部件实例
   * @memberof AppMobPickUpView
   */
  public extraCtrlParam(ctrlProps: any, controlInstance: IPSControl) {
    super.extraCtrlParam(ctrlProps, controlInstance);
    if (controlInstance?.controlType == 'PICKUPVIEWPANEL') {
      Object.assign(ctrlProps, {
        isMultiple: false,
      });
    }
  }
```

::: tip 提示
实体移动端数据选择视图继承主数据视图，更多逻辑参见 [主数据视图 > UI层](../de/de-view.md#UI层)
:::

### 布局

#### 绘制视图主容器内容

重写视图主容器内容，绘制选择视图面板部件插槽。

```ts
  /**
   * @description 视图主容器内容
   * @return {*}  {any}
   * @memberof AppDefaultMobPickupViewLayout
   */
  renderViewMainContainerContent(): any {
    return <div class='view-main-container-content'>{[renderSlot(this.ctx.slots, 'pickupviewpanel')]}</div>;
  }
```

::: tip 提示
该视图布局继承主数据视图布局，更多逻辑参见 [主数据视图 > UI层 > 布局](../de/de-view.md#布局)
:::