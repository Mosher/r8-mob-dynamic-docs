# 应用看板视图

应用看板视图用于承载数据看板部件，在该视图数据看板中快速菜单栏，嵌入视图和直接内容都作为门户部件来处理。

<component-iframe router="/iframe/view/app/app-portal-view" />

## 控制器
::: tip 提示
应用看板视图控制器继承视图控制器基类，更多逻辑参见 [视图 > 控制器](../app-view-base.md#控制器)
:::

## UI层
::: tip 提示

应用看板视图UI层继承视图UI层基类，更多逻辑参见 [视图 > UI层](../app-view-base.md#ui层)

:::

### 布局

重写视图主容器内容，绘制数据看板插槽。

```ts
  /**
   * @description 视图主容器内容
   * @return {*}
   * @memberof AppDefaultPortalViewLayout
   */
  renderViewMainContainerContent(): any {
    return <div class='view-main-container-content'>{[renderSlot(this.ctx.slots, 'dashboard')]}</div>;
  }
```
::: tip 提示
该视图布局继承视图布局基类，更多逻辑参见 [视图 > UI层 > 布局](../app-view-base.md#布局)
:::