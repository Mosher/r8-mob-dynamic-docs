# 应用首页视图

首页是整个系统的展示界面，平台定义了首页视图的形式，可以将定义的菜单与门户视图进行集成，同时在集成时提供了多样的菜单呈现模式及权限控制体系。

<component-iframe router="/iframe/view/app/app-index-view" />

R8Mob动态模板不同于之前的R7动态模板，此次采用全新的模式，将UI和逻辑层分开。用户进行的操作通过UI层传递给控制器，控制器完成具体逻辑后将数据又返回给UI层完成数据的更新。

## 控制器

::: tip 提示
应用首页视图控制器继承视图控制器基类，更多逻辑参见 [视图 > 控制器](../app-view-base.md#控制器)
:::

## UI层

::: tip 提示

应用首页视图UI层继承视图UI层基类，更多逻辑参见 [视图 > UI层](../app-view-base.md#ui层)

:::

### 布局

#### 绘制视图主容器内容

重写视图主容器内容，绘制菜单插槽。

```ts
  /**
   * @description 视图主容器内容
   * @return {*}  {any[]}
   * @memberof AppDefaultIndexViewLayout
   */
  renderViewMainContainerContent(): any {
    return <div class='view-main-container-content'>{[renderSlot(this.ctx.slots, 'appmenu')]}</div>;
  }
```

#### 绘制视图头

首页视图不需要视图头。

```ts
  /**
   * @description 渲染视图头
   * @return {*}
   * @memberof AppDefaultIndexViewLayout
   */
  renderViewHeader() {
    return null;
  }
```

#### 绘制视图底部

首页视图不需要视图底部。

```ts
  /**
   * @description 渲染视图底部
   * @return {*}
   * @memberof AppDefaultIndexViewLayout
   */
  renderViewFooter() {
    return null;
  }
```

::: tip 提示
该视图布局继承视图布局基类，更多逻辑参见 [视图 > UI层 > 布局](../app-view-base.md#布局)
:::