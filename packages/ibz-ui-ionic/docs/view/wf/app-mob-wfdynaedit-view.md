# 实体移动端动态工作流编辑视图

实体移动端动态工作流编辑视图用于展示工作流在某个流程中的表单数据。由编辑表单部件组成，部件组成与布局与常规编辑视图相同。

<component-iframe router="/iframe/view/wf/app-mob-wfdynaedit-view" />

## 控制器

### 引擎初始化

引擎是R8Mob动态模板中视图的固有逻辑对象，用于处理视图的事件以及视图与部件的交互。

当实体移动端动态工作流编辑视图(以下简称：视图)下的所有部件挂载完成后，视图执行挂载逻辑，此时初始化引擎。

核心逻辑如下：

``` ts 
    /**
     * 引擎加载
     *
     * @param {*} [opts={}]
     * @memberof WFDynaEditViewEngine
     */
    public load(opts: any = {}): void {
        if (this.view.getWFLinkModel && this.view.getWFLinkModel instanceof Function) {
            this.view.getWFLinkModel();
        }
    }
```

由上述逻辑可知，动态工作流编辑视图加载时会调用视图的 **[获取工作流模型](#获取工作流模型)**

### 获取工作流模型

获取工作流模型事件（getWFLinkModel）在视图引擎初始化时会被调用，主要根据业务主键和当前步骤获取操作路径以及视图相关模型。

主要逻辑：

- 合并当前工作流的业务主键和当期那步骤标识

- 请求后端数据
- 根据后端数据设置当前视图的工具栏模型

- 根据请求头中的process-mobform 设置当前激活表单

```ts
  /**
   * @description 获取工具栏按钮
   * @param {*} arg
   * @return {*}  {Promise<any>}
   * @memberof MobWFDynaEditViewController
   */
  public async getWFLinkModel(): Promise<any> {
    let datas: any = {};
    if (Object.keys(this.viewParam).length > 0) {
      Object.assign(datas, { processDefinitionKey: this.viewParam.processDefinitionKey });
      Object.assign(datas, { taskDefinitionKey: this.viewParam.taskDefinitionKey });
    }
    const response: any = await this.appDataService.GetWFLink({ ...this.context }, datas);
    if (response && response.status === 200) {
      this.linkModel = response.data;
      if (response.headers && response.headers['process-mobform']) {
        this.computeActivedForm(response.headers['process-mobform']);
      } else {
        this.computeActivedForm(null);
      }
    } else {
      const { data: _data } = response;
      App.getNoticeService().error(_data.message);
    }
    return response;
  }
```

### 动态工具栏点击

动态工具栏点击事件（dynamicToolbarClick）在视图的按钮被点击时触发。主要是提交工作流或结束工作流，进入下一流程或结束。

主要逻辑：

​	合并当前激活表单数据

​	判断当前按钮的类型，若是结束（finish）或不存在 则执行[工作流操作行为](#工作流操作行为)逻辑

​	判断当前按钮的类型，若不是结束则执行 [处理工作流辅助功能](#处理工作流辅助功能) 逻辑

```ts
  /**
   * @description 动态工具栏点击
   * @param {*} linkItem
   * @param {*} $event
   * @memberof MobWFDynaEditViewController
   */
  public dynamicToolbarClick(linkItem: any, $event: any) {
    const _this: any = this;
    let datas: any[] = [];
    let xData: any = this.ctrlRefsMap.get('form');
    if (xData.getDatas && xData.getDatas instanceof Function) {
      datas = [...xData.getDatas()];
    }
    ········
    if (linkItem && linkItem.type) {
      if (Object.is(linkItem.type, 'finish')) {
        submitAction();
      } else {
        this.handleWFAddiFeature(linkItem);
      }
    } else {
      submitAction();
    }
  }
```

### 工作流提交

工作流提交事件 由点击视图工具栏操作触发，根据当前表单数据，提交工作流程

```ts
    const submit: Function = (submitData: any, linkItem: any) => {
      xData.wfsubmit(submitData, linkItem).then((response: any) => {
        if (!response || response.status !== 200) {
          return;
        }
        const { data: _data } = response;
        if (_this.viewdata) {
          this.viewEvent(AppViewEvents.DATA_CHANGE, [{ ..._data }])
          this.viewEvent(AppViewEvents.CLOSE, {});
        } else {
          this.closeView([{ ..._data }]);
        }
      });
    };
```

### 工作流操作行为

工作流操作行为事件 由点击视图工具栏操作触发，

主要逻辑：

- 判断按钮模型是否需要打开其他工作流视图不存在则，直接[提交工作流](#工作流提交)

- 否则通过模态打开工作流操作视图，等待视图操作完毕，合并操作参数到工作流提交参数中，执行[工作流提交](#工作流提交)。

```ts
const submitAction: Function = () => {
      if (linkItem && linkItem.sequenceflowview) {
          	······
              appmodal.subscribe((result: any) => {
                if (!result || !Object.is(result.ret, 'OK')) {
                  return;
                }
                let tempSubmitData: any = Util.deepCopy(datas[0]);
                if (result.datas && result.datas[0]) {
                  const resultData: any = result.datas[0];
                  if (Object.keys(resultData).length > 0) {
                    let tempData: any = {};
                    Object.keys(resultData).forEach((key: any) => {
                      if (resultData[key] && key !== 'srfuf') tempData[key] = resultData[key];
                    });
                    Object.assign(tempSubmitData, tempData);
                  }
                }
                submit([tempSubmitData], linkItem);
              });
            });
          });
        }
      } else {
        submit(datas, linkItem);
      }
    };
```



### 处理工作流辅助功能

提交工作流辅助功能(submitWFAddiFeature) 由点击视图工具栏操作触发，根据当前操作按钮行为，提交工作流程。

参数：linkItem：操作按钮

主要逻辑：

​	判断当前工作流附加功能类型映射关系对象（wfAddiFeatureRef）的按钮行为是否存在，不存在则直接结束

​	获取当前激活**表单数据**，根据当前操作工具栏模型获取操作视图，和操作表单。

​	打开工作流操作视图，操作完毕合并操作数据进**表单数据** 执行[提交工作流辅助功能](#提交工作流辅助功能)逻辑

```ts
  /**
   * @description 处理工作流辅助功能
   * @param {*} linkItem
   * @return {*} 
   * @memberof MobWFDynaEditViewController
   */
  public handleWFAddiFeature(linkItem: any) {
    let featureTag: string = this.wfAddiFeatureRef[linkItem?.type]?.featureTag;
    if (!featureTag) return;
    let targetViewRef: any = this.viewRefData.find((item: any) => {
      return item.name === `WFUTILACTION@${featureTag}`;
    });
    if (!targetViewRef) {
      LogUtil.warn('将待办任务标记为已读失败');
      return;
    }
    // 准备参数
    let datas: any[] = [];
    let xData: any = this.ctrlRefsMap.get('form');
    if (xData.getDatas && xData.getDatas instanceof Function) {
      datas = [...xData.getDatas()];
    }
    let tempContext: any = Util.deepCopy(this.context);
    Object.assign(tempContext, { [this.appDeCodeName.toLowerCase()]: datas && datas[0].srfkey });
    let tempViewParam: any = { actionView: linkItem.sequenceflowview, actionForm: linkItem.sequenceflowform };
    Object.assign(tempContext, { viewpath: targetViewRef?.getRefPSAppView?.path });
    GetModelService(tempContext).then((modelService: AppModelService) => {
      modelService.getPSAppView(targetViewRef?.getRefPSAppView?.path).then((viewResult: IPSAppView) => {
        const viewComponent = App.getComponentService().getViewTypeComponent(
          viewResult.viewType,
          viewResult.viewStyle,
          viewResult.getPSSysPFPlugin()?.pluginCode,
        );
        const appmodal = App.getOpenViewService().openModal(
          {
            viewComponent: viewComponent,
          },
          tempContext,
          tempViewParam,
        );
        appmodal.subscribe((result: any) => {
          if (!result || !Object.is(result.ret, 'OK')) {
            return;
          }
          let tempSubmitData: any = Util.deepCopy(datas[0]);
          if (result.datas && result.datas[0]) {
            const resultData: any = result.datas[0];
            if (Object.keys(resultData).length > 0) {
              let tempData: any = {};
              Object.keys(resultData).forEach((key: any) => {
                if (resultData[key] && key !== 'srfuf') tempData[key] = resultData[key];
              });
              Object.assign(tempSubmitData, tempData);
            }
            this.submitWFAddiFeature(linkItem, tempSubmitData);
          }
        });
      });
    });
  }
```



### 提交工作流辅助功能

提交工作流辅助功能(submitWFAddiFeature) 由点击视图工具栏操作触发，根据当前操作按钮行为，提交工作流程。
参数：linkItem：操作按钮、submitData：提交数据

主要逻辑：

​	判断当前工作流附加功能类型映射关系对象（wfAddiFeatureRef）的按钮行为是否存在，不存在则直接结束

​	若存在直接调用当前工作流附加功能行为提交工作流程

​	关闭当前视图，发送应用刷新通知

```ts
  /**
   * @description 提交工作流辅助功能
   * @param {*} linkItem
   * @param {*} submitData
   * @return {*} 
   * @memberof MobWFDynaEditViewController
   */
  public submitWFAddiFeature(linkItem: any, submitData: any) {
    let tempSubmitData: any = Object.assign(linkItem, { activedata: submitData });
    let action: string = this.wfAddiFeatureRef[linkItem?.type]?.action;
    if (!action) return;
    const service: any = this.appDataService;
    if (service && service[action] && service[action] instanceof Function) {
      service[action](Util.deepCopy(this.context), tempSubmitData)
        .then((response: any) => {
          const { data: data } = response;
          if (!response || response.status !== 200) {
            App.getNoticeService().error(response);
            return;
          }
          let _this: any = this;
          if (_this.viewdata) {
            _this.$emit('view-event', {
              viewName: this.viewInstance.name,
              action: 'viewdataschange',
              data: [{ ...data }],
            });
            _this.$emit('view-event', { viewName: this.viewInstance.name, action: 'close', data: null });
          } else {
            this.closeView([{ ...data }]);
          }
          App.getCenterService().notifyMessage(this.appDeCodeName, 'appRefresh', data);
          App.getNoticeService().success(data?.message ? data.message : '提交数据成功');
        })
        .catch((error: any) => {
          App.getNoticeService().error(error);
        });
    }
  }
```

::: tip 提示
实体移动端动态工作流编辑视图控制器继承主数据视图控制器基类，更多逻辑参见 [主数据视图 > 控制器](../de/de-view.md#控制器)
:::

## UI层
### 绘制视图工具栏

重写基类里的绘制工具栏按钮方法，通过动态的工具栏按钮模型渲染

```ts
  /**
   * @description 渲染工具栏
   * @return {*} 
   * @memberof AppMobWFDynaEditView
   */
  public renderToolbar() {
    return (
      <ion-toolbar class={['wf-toolbar']} slot="fixed">
        <ion-buttons slot="end">
          {this.c.linkModel.map((linkItem: any, index: number) => {
            return (
            <ion-button key={index} expand='block' onClick={(event: any) => { this.toolbarClick(linkItem, event); }}>
                <span class="caption">{linkItem.sequenceFlowName}</span>
              </ion-button>
            )
          })}
        </ion-buttons>
      </ion-toolbar>
    )
  }
```

### 绘制视图部件

右侧工具栏和当前工作流激活表单

```ts
  /**
   * @description 渲染视图部件
   * @return {*} 
   * @memberof AppMobWFDynaEditView
   */
  public renderViewControls() {
    const controlObject = super.renderViewControls();
    Object.assign(controlObject, {
      righttoolbar: () => this.renderToolbar(),
      form: () => this.renderForm()
    });
    return controlObject;
  }
```

### 绘制表单

如果部件类型为选择视图面板部件，给其参数新增是否多选isMultiple属性为false。

```ts
  /**
   * @description 渲染表单
   * @return {*} 
   * @memberof AppMobWFDynaEditView
   */
  public renderForm() {
    if (this.c.editFormInstance) {
      return this.computeTargetCtrlData(this.c.editFormInstance);
    } else {
      return null;
    }
  }
```

::: tip 提示
实体移动端动态工作流编辑视图继承主数据视图，更多逻辑参见 [主数据视图 > UI层](../de/de-view.md#UI层)
:::

### 布局

#### 绘制视图主容器内容

重写视图主容器内容，绘制视图激活表单插槽。

```ts
  /**
   * @description 渲染部件
   * @return {*}
   * @memberof AppDefaultMobWFDynaEditViewLayout
   */
  public renderViewMainContainerContent(): any {
    return <div class='view-main-container-content'>{[renderSlot(this.ctx.slots, 'form')]}</div>;
  }
```

::: tip 提示
该视图布局继承主数据视图布局，更多逻辑参见 [主数据视图 > UI层 > 布局](../de/de-view.md#布局)
:::