# 实体移动端动态工作操作辑视图

实体移动端动态工作操作辑视图用于展示工作流在某个流程中的表单数据。由编辑表单部件组成，部件组成与布局与常规操作视图相同。

<component-iframe router="/iframe/view/wf/app-mob-wfdynaaction-view" />

## 控制器

### 视图初始化

视图重写视图初始化逻辑，根据当前视图参数，执行[计算当前激活表单](#计算激活表单)逻辑 ，下发加载通知给部件

```ts
  /**
   * @description 视图挂载
   * @memberof MobWFDynaActionViewController
   */
  public viewMounted() {
    super.viewMounted();
    if (this.viewParam && this.viewParam.actionForm) {
      this.computeActivedForm(this.viewParam.actionForm);
    } else {
      this.computeActivedForm(null);
    }
    setTimeout(() => {
      const Environment = App.getEnvironment();
      if (Environment && Environment.isPreviewMode) {
        return;
      }
      this.viewState.next({
        tag: this.editFormInstance.name,
        action: 'autoload',
        data: { srfkey: this.context[this.appDeCodeName?.toLowerCase()] },
      });
    }, 0);
  }
```

### 计算激活表单

计算激活表单事件（computeActivedForm）由视图初始化调用，主要用于计算当前操作视图的表单模型

参数：inputForm：表单名称

主要逻辑：

​	判断当前表单名称是否存在，不存在则用默认表单，存在则使用工作流表单

```ts
  /**
   * @description 计算激活表单
   * @param {*} inputForm
   * @memberof MobWFDynaActionViewController
   */
  public computeActivedForm(inputForm: any) {
    if (!inputForm) {
      this.editFormInstance = ModelTool.findPSControlByName('form', this.viewInstance.getPSControls());
    } else {
      this.editFormInstance = ModelTool.findPSControlByName(
        `wfform_${inputForm.toLowerCase()}`,
        this.viewInstance.getPSControls(),
      );
    }
  }
```

### 确认

确认事件（handleOk）由视图确认按钮触发，主要用于关闭视图，提交数据

主要逻辑：

​		获取表单数据，执行事件event钩子（事件名称为onDatasChange）

​		执行事件event钩子（事件名称为close） （可参见[视图基类 > 视图事件处理](../de/de-view.md#视图事件处理))

```ts
  /**
   * @description 确认
   * @memberof MobWFDynaActionViewController
   */
  public handleOk() {
    let xData: any = this.ctrlRefsMap.get('form');
    if (xData) {
      let preFormData: any = xData.getData();
      let nextFormData: any = xData.transformData(preFormData);
      Object.assign(preFormData, nextFormData);
      this.viewEvent(AppViewEvents.DATA_CHANGE, [preFormData])
      this.viewEvent(AppViewEvents.CLOSE, {});
    }
  }
```

### 取消

取消事件（handleCancel）由视图取消按钮触发，主要用于关闭视图

主要逻辑：

​		执行事件event钩子（事件名称为close） （可参见[视图基类 > 视图事件处理](../de/de-view.md#视图事件处理))

```ts
  /**
   * @description 取消
   * @memberof MobWFDynaActionViewController
   */
  public handleCancel() {
    this.viewEvent(AppViewEvents.CLOSE, {});
  }
```

::: tip 提示
实体移动端动态工作操作辑视图控制器继承主数据视图控制器基类，更多逻辑参见 [主数据视图 > 控制器](../de/de-view.md#控制器)
:::

## UI层
### 绘制视图工具栏

重写基类里的绘制视图底部方法，绘制确认和取消按钮

```ts
  /**
   * @description 渲染视图底部
   * @return {*} 
   * @memberof AppMobWFDynaActionView
   */
  public renderViewFooter() {
    return (
      <ion-footer class={['view-footer', 'wf-action-view-footer']}>
        <ion-buttons>
          <ion-button expand='full' size='large' onClick={(event: any) => { this.submit(event); }}>
          {`${this.$tl('view.wfdynaactionview.submit','提交')}`}
          </ion-button>
          <ion-button expand='full' size='large' onClick={(event: any) => { this.cancel(event); }}>
          {`${this.$tl('share.cancel','取消')}`}
          </ion-button>
        </ion-buttons>
      </ion-footer>
    )
  }
```

### 绘制视图部件

右侧工具栏和当前工作流激活表单

```ts
  /**
   * @description 渲染视图部件
   * @return {*} 
   * @memberof AppMobWFDynaEditView
   */
  public renderViewControls() {
    const controlObject = super.renderViewControls();
    Object.assign(controlObject, {
      righttoolbar: () => this.renderToolbar(),
      form: () => this.renderForm()
    });
    return controlObject;
  }
```

### 绘制表单

如果部件类型为选择视图面板部件，给其参数新增是否多选isMultiple属性为false。

```ts
  /**
   * @description 渲染表单
   * @return {*} 
   * @memberof AppMobWFDynaEditView
   */
  public renderForm() {
    if (this.c.editFormInstance) {
      return this.computeTargetCtrlData(this.c.editFormInstance);
    } else {
      return null;
    }
  }
```

::: tip 提示
实体移动端动态工作操作辑视图继承主数据视图，更多逻辑参见 [主数据视图 > UI层](../de/de-view.md#UI层)
:::

### 布局

#### 绘制视图主容器内容

重写视图主容器内容，绘制视图激活表单插槽。

```tsx
  /**
   * @description 渲染部件
   * @return {*}
   * @memberof AppDefaultMobWFDynaEditViewLayout
   */
  public renderViewMainContainerContent(): any {
    return <div class='view-main-container-content'>{[renderSlot(this.ctx.slots, 'form')]}</div>;
  }
```

#### 绘制视图底部内容

重写视图底部内容，绘制视图确认和取消按钮

```tsx
  /**
   * @description 绘制视图底部
   * @return {*}
   * @memberof AppDefaultMobWFDynaActionViewLayout
   */
  public renderViewFooter() {
    return renderSlot(this.ctx.slots, 'viewFooter');
  }
```



::: tip 提示
该视图布局继承主数据视图布局，更多逻辑参见 [主数据视图 > UI层 > 布局](../de/de-view.md#布局)
:::