# 实体移动端数据看板视图

实体移动端数据看板视图即数据看板的呈现界面，数据看板中定义了系统门户部件以及直接内容。

<component-iframe router="/iframe/view/de/app-mob-dashboard-view" />

## 控制器

### 引擎初始化

引擎是R8Mob动态模板中视图的固有逻辑对象，用于处理视图的事件以及视图与部件的交互。

当实体移动端数据看板视图下的所有部件挂载完成后，视图执行挂载逻辑，此时初始化引擎。

引擎初始化完成后，通过视图状态订阅对象(ViewState)通知相应部件加载数据。

核心逻辑如下：

``` ts
  /**
   * @description 引擎加载
   * @memberof MobDEDashboardViewEngine
   */
  public load(opts?: any): void {
    super.load(opts);
    if (this.getSearchForm()) {
      const tag = this.getSearchForm().name;
      this.setViewState2({ tag: tag, action: 'loadDraft', viewdata: { ...this.view.viewParam } });
    }
    if (this.getDashboard() && this.isLoadDefault) {
      const tag = this.getDashboard().name;
      this.setViewState2({ tag: tag, action: 'load', viewdata: { isSearchMode: this.getSearchForm() ? true : false } });
    } else {
      this.isLoadDefault = true;
    }
  }
```

由上述代码可知，如果实体移动端数据看板视图配置了搜索表单则会先通知搜索表单加载，然后通知数据看板部件执行load。

### 实体数据看板

实体数据看板事件事件（dashboardEvent）是实体看板视图引擎固有的逻辑对象，在实体数据看板部件加载完成后执行。

执行视图事件(event)钩子，事件名称为`onLoad`（可参见[视图基类 > 视图事件处理](../de/de-view.md#视图事件处理))

```ts
  /**
   * @description 实体数据看板事件
   * @param {string} eventName 事件名
   * @param {IUIDataParam} args 参数
   * @memberof MobDEDashboardViewEngine
   */
  public dashboardEvent(eventName: string, args: IUIDataParam): void {
    if (Object.is(eventName, 'load')) {
      this.emitViewEvent(AppViewEvents.LOAD, args);
    }
  }
```

### 搜索表单搜索

搜索表单搜索事件（onSearchFormSearch）是实体看板视图引擎固有的逻辑对象，在搜索表单部件点击搜索按钮后执行。

参数： args（搜索表单数据）

核心逻辑：

​	合并加载完成参数，判断当前看板是否存在，下发加载（`loaddata`）通知给数据看板

```ts
  /**
   * @description 搜索表单搜索
   * @param {*} [args={}]
   * @memberof MobDEDashboardViewEngine
   */
  public onSearchFormSearch(args: any = {}): void {
    if (this.getDashboard() && this.isLoadDefault) {
      const tag = this.getDashboard().name;
      Object.assign(this.view.viewParam, args);
      this.setViewState2({ tag: tag, action: 'loaddata', viewdata: { ...this.view.viewParam } });
    }
  }

```



### 搜索表单重置

搜索表单重置事件（onSearchFormReset）是实体看板视图引擎固有的逻辑对象，在搜索表单部件点击重置按钮后执行。

核心逻辑：判断搜索表单是否存在，下发初始化（`loadDraft`）通知给搜索表单

```ts
  /**
   * @description 搜索表单重置
   * @param {*} [args={}]
   * @memberof MobDEDashboardViewEngine
   */
  public onSearchFormReset(args: any = {}) {
    if (this.getSearchForm()) {
      const tag = this.getSearchForm().name;
      this.setViewState2({ tag: tag, action: 'loadDraft', viewdata: { ...this.view.viewParam } });
    }
  }
```



::: tip 提示
实体移动端数据看板视图控制器继承主数据视图控制器基类，更多逻辑参见 [主数据视图 > 控制器](./de-view.md#控制器)
:::

## UI层

### 绘制信息栏

支持信息栏，用于展示当前业务数据的主要信息。

```ts
  /**
   * @description 渲染信息栏
   * @return {*} 
   * @memberof AppMobDEDashboardView
   */
  public renderDataInfoBar() {
    if (this.c.viewInstance.showDataInfoBar) {
      return (
        <span class="view-info-bar">
          {this.c.getData()?.srfmajortext}
        </span>
      )
    }
  }
```

### 绘制视图组件

重写绘制视图组件，加入绘制信息栏renderDataInfoBar。

```ts
  /**
   * @description 渲染视图组件
   * @return {*} 
   * @memberof AppMobDEDashboardView
   */
  public renderViewControls() {
    const controlObject = super.renderViewControls();
    Object.assign(controlObject, {
      'dataInfoBar': () => this.renderDataInfoBar()
    })
    return controlObject;
  }
}
```

::: tip 提示
实体移动端数据看板视图继承主数据视图基类，更多逻辑参见 [主数据视图 > UI层](./de-view.md#ui层)
:::

### 布局

#### 绘制视图主容器内容

重写视图主容器内容，绘制数据看板部件插槽。

```ts
  /**
   * @description 视图主容器内容
   * @return {*}
   * @memberof AppMobDEDashboardViewLayout
   */
  renderViewMainContainerContent(): any {
    return <div class='view-main-container-content'>{[renderSlot(this.ctx.slots, 'dashboard')]}</div>;
  }
```

#### 绘制视图主容器头部

重写视图主容器头部，绘制搜索表单以及快速搜索表单插槽。

```ts
  /**
   *
   * @description 视图主容器头部
   * @return {*}
   * @memberof AppMobDEDashboardViewLayout
   */
  renderViewMainContainerHeader(): any {
    return (
      <div class='view-main-container-header'>
        {[
          renderSlot(this.ctx.slots, 'searchform'),
          renderSlot(this.ctx.slots, 'quicksearchform'),
        ]}
      </div>
    );
  }
```

::: tip 提示
该视图布局继承主数据视图布局，更多逻辑参见 [主数据视图 > UI层 > 布局](./de-view.md#布局)
:::