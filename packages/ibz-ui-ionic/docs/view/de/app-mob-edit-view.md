# 实体移动端编辑视图

实体移动端编辑视图是常规的数据新建、查看与修改界面，展示了当前业务实体单条数据的信息，具体的呈现内容则由表单部件呈现，具体的操作则由工具栏部件呈现。

<component-iframe router="/iframe/view/de/app-mob-edit-view" />

## 控制器

### 引擎初始化

引擎是R8Mob动态模板中视图的固有逻辑对象，用于处理视图的事件以及视图与部件的交互。

当实体移动端编辑视图下的所有部件挂载完成后，视图执行挂载逻辑，此时初始化引擎。

引擎初始化完成后，通过视图状态订阅对象(ViewState)通知相应部件加载数据。

核心逻辑如下：

``` ts
  /**
   * @desription 引擎加载
   * @param {*} [opts={}]
   * @memberof MobEditViewEngine
   */
  public load(opts: any = {}): void {
    super.load(opts);
    if (this.getForm()) {
      const tag = this.getForm().name;
      let action: string = '';
      // 实体主键字段有值时load该记录数据，否则loaddraft加载草稿
      if (
        this.keyPSDEField &&
        this.view.context[this.keyPSDEField] &&
        !Object.is(this.view.context[this.keyPSDEField], '') &&
        !Object.is(this.view.context[this.keyPSDEField], 'null')
      ) {
        action = 'load';
      } else {
        action = 'loadDraft';
      }
      Object.assign(this.view.viewParam, opts);
      this.setViewState2({ tag: tag, action: action, viewdata: { ...this.view.viewparams } });
    }
  }
```
由上述代码可知，若视图上下文中存在实体主键且有值时，引擎对象会将该值合并到视图参数中，并通知表单部件执行load行为，
否则执行loadDraft行为。

### 表单加载完成

表单加载完成事件（onFormLoad）是编辑视图引擎固有的逻辑对象，在表单部件加载完成后执行。

参数：arg（表单加载数据）

核心逻辑：

- 执行视图事件(event)钩子，事件名称为`onLoad`（可参见[视图基类 > 视图事件处理](../de/de-view.md#视图事件处理))
- 执行视图事件(event)钩子，事件名称为`onDatasChange`（可参见[视图基类 > 视图事件处理](../de/de-view.md#视图事件处理))
- 计算工具栏权限状态(可参见[视图基类 > 计算工具栏状态](./de-view.md#计算工具栏状态))

```ts
  /**
   * 表单加载完成
   *
   * @param {*} args
   * @memberof MobEditViewEngine
   */
  public onFormLoad(arg: any): void {
    this.emitViewEvent(AppViewEvents.LOAD, arg);
    this.emitViewEvent(AppViewEvents.DATA_CHANGE, JSON.stringify({ action: 'load', status: 'success', data: arg }));
    const newdata: boolean = !Object.is(arg.srfuf, '1');
    this.calcToolbarItemState(newdata);
    this.calcToolbarItemAuthState(arg);
  }
```

### 表单保存完成

表单加载完成事件（onFormSave）是编辑视图引擎固有的逻辑对象，在表单部件保存完成后执行。

参数：arg（表单保存数据）

核心逻辑：

- 执行视图事件(event)钩子，事件名称为`save`（可参见[视图基类 > 视图事件处理](../de/de-view.md#视图事件处理))
- 执行视图事件(event)钩子，事件名称为`onDatasChange`（可参见[视图基类 > 视图事件处理](../de/de-view.md#视图事件处理))
- 计算工具栏权限状态(可参见[视图基类 > 计算工具栏状态](./de-view.md))

```ts
  /**
   * 表单保存完成
   *
   * @param {*} args
   * @memberof MobEditViewEngine
   */
  public onFormSave(arg: any): void {
    this.emitViewEvent('save', arg);
    this.emitViewEvent(AppViewEvents.DATA_CHANGE, JSON.stringify({ action: 'save', status: 'success', data: arg }));
    const newdata: boolean = !Object.is(arg.srfuf, '1');
    this.calcToolbarItemState(newdata);
    this.calcToolbarItemAuthState(arg);
  }
```



### 表单删除完成

表单加载完成事件（onFormRemove）是编辑视图引擎固有的逻辑对象，在表单部件保存完成后执行。

参数：arg（表单删除数据）

核心逻辑：

- 执行视图事件(event)钩子，事件名称为`remove`（可参见[视图基类 > 视图事件处理](../de/de-view.md#视图事件处理))
- 执行视图事件(event)钩子，事件名称为`onDatasChange`（可参见[视图基类 > 视图事件处理](../de/de-view.md#视图事件处理))

```ts
  /**
   * 表单删除完成
   *
   * @param {*} args
   * @memberof MobEditViewEngine
   */
  public onFormRemove(arg: any): void {
    this.emitViewEvent('remove', arg);
    this.emitViewEvent(AppViewEvents.DATA_CHANGE, JSON.stringify({ action: 'remove', status: 'success', data: arg }));
  }
```

::: tip 提示
实体移动端编辑视图控制器继承主数据视图控制器基类，更多逻辑参见 [主数据视图 > 控制器](./de-view.md#控制器)
:::

## UI层

### 绘制信息栏

实体编辑视图支持信息栏，用于展示当前业务数据的主要信息。

```ts
  /**
   * @description 渲染信息栏
   * @return {*} 
   * @memberof AppMobEditView
   */
  public renderDataInfoBar() {
    if (this.c.viewInstance.showDataInfoBar) {
      return (
        <span class="view-info-bar">
          {this.c.getData()?.srfmajortext}
        </span>
      )
    }
  }
```

::: tip 提示
实体移动端编辑视图继承主数据视图，更多逻辑参见 [主数据视图 > UI层](./de-view.md#UI层)
:::

### 布局

#### 绘制视图主容器内容

重写视图主容器内容，绘制表单插槽。

```ts
  /**
   * @description 视图主容器内容
   * @return {*}
   * @memberof AppDefaultMobEditViewLayout
   */
  renderViewMainContainerContent(): any {
    return <div class='view-main-container-content'>{[renderSlot(this.ctx.slots, 'form')]}</div>;
  }
```

::: tip 提示
该视图布局继承主数据视图布局，更多逻辑参见 [主数据视图 > UI层 > 布局](./de-view.md#布局)
:::

