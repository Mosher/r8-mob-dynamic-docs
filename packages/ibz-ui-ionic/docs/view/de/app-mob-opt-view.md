# 实体移动端选项操作视图

实体移动端选项操作视图提供确认、取消的默认操作，通常用于弹出窗口的数据操作，如审批等等。

<component-iframe router="/iframe/view/de/app-mob-opt-view" />



## 控制器

### 引擎初始化

引擎是R8Mob动态模板中视图的固有逻辑对象，用于处理视图的事件以及视图与部件的交互。

当实体移动端选项操作视图(以下简称：视图)下的所有部件挂载完成后，视图执行挂载逻辑，此时初始化引擎。

引擎初始化完成后，通过视图状态订阅对象(ViewState)通知相应部件加载数据。

核心逻辑如下：

``` ts
  /**
   * 引擎加载
   *
   * @param {*} [opts={}]
   * @memberof MobEditViewEngine
   */
  public load(opts: any = {}): void {
    super.load(opts);
    if (this.getForm()) {
      const tag = this.getForm().name;
      let action: string = '';
      // 实体主键字段有值时load该记录数据，否则loaddraft加载草稿
      if (
        this.keyPSDEField &&
        this.view.context[this.keyPSDEField] &&
        !Object.is(this.view.context[this.keyPSDEField], '') &&
        !Object.is(this.view.context[this.keyPSDEField], 'null')
      ) {
        action = 'load';
      } else {
        action = 'loadDraft';
      }
      Object.assign(this.view.viewParam, opts);
      this.setViewState2({ tag: tag, action: action, viewdata: { ...this.view.viewparams } });
    }
  }
```
 由上述代码可知，若视图上下文中存在实体主键且有值时，引擎对象会将该值合并到视图参数中，并通知表单部件执行load行为， 否则执行loadDraft行为。 

### 确认

点击确认按钮后调用该函数，拿到该视图里嵌套的部件，如果该部件有save方法则调用该部件的save方法然后关闭视图。

```ts
  /**
   * @description 确定
   * @memberof MobOptViewController
   */
  public async ok(): Promise<void> {
    const xData: any = this.ctrlRefsMap.get(this.xDataControlName);
    if (xData.save && xData.save instanceof Function) {
      xData.save().then((res: any) => {
        if (res.ret == true) {
          this.viewEvent(AppViewEvents.CLOSE, res.data);
          this.closeView(res.data);
        }
      });
    }
  }
```

### 取消

点击取消按钮后调用该函数，直接关闭视图。

```ts
  /**
   * @description 取消
   * @memberof MobOptViewController
   */
  public cancel(): void {
    this.closeView();
  }
```

::: tip 提示
实体移动端选项操作视图控制器继承主数据视图控制器基类，更多逻辑参见 [主数据视图 > 控制器](./de-view.md#控制器)
:::

## UI层

### 绘制视图所有部件

重写视图基类里的绘制视图所有部件，除继承其原有功能之外，在部件数组上新增了渲染footerButtons插槽，即绘制底部按钮内容。

```ts
  /**
   * @description 绘制视图所有部件
   * @return {*}
   * @memberof AppMobMPickUpView
   */
  public renderViewControls() {
    const controlObject = super.renderViewControls();
    Object.assign(controlObject, {
      footerButtons: () => this.renderFooterButtons(),
    });
    return controlObject;
  }
```

### 绘制底部按钮

提供确认、取消的默认操作。

```ts
  /**
   * @description 绘制底部按钮
   * @return {*}
   * @memberof AppMobOptView
   */
   public renderFooterButtons() {
    return (
        <div class="option-view-btnbox">
            <ion-button expand='full' size='large' class="option-btn-cancel" onClick={() => this.c.cancel()}>
            {`${this.$tl('share.cancel','取消')}`}
            </ion-button>
            <ion-button expand='full' size='large' class="option-btn-success" onClick={() => this.c.ok()}>
            {`${this.$tl('share.ok','确认')}`}
            </ion-button>     
        </div>
    );
  }
```

::: tip 提示
实体移动端选项操作视图继承主数据视图，更多逻辑参见 [主数据视图 > UI层](./de-view.md#UI层)
:::

### 布局

#### 绘制视图主容器内容

重写视图主容器内容，绘制表单插槽。

```ts
  /**
   * @description 视图主容器内容
   * @return {*}
   * @memberof AppDefaultMobOptViewLayout
   */
  renderViewMainContainerContent(): any {
    return <div class='view-main-container-content'>{[renderSlot(this.ctx.slots, 'form')]}</div>;
  }
```

#### 绘制视图底部

重写视图底部内容，绘制底部按钮和底部视图信息插槽。

```ts
  /**
   * @description 渲染视图底部
   * @return {*}  {*}
   * @memberof AppDefaultMobOptViewLayout
   */
  renderViewFooter(): any {
    return (
      <ion-footer class={'view-footer'}>
        {renderSlot(this.ctx.slots, 'footerButtons')}
        {this.ctx.slots.bottomMessage ? renderSlot(this.ctx.slots, 'bottomMessage') : null}
      </ion-footer>
    );
  }
```

::: tip 提示
该视图布局继承主数据视图布局，更多逻辑参见 [主数据视图 > UI层 > 布局](./de-view.md#布局)
:::