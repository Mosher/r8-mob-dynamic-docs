# 实体移动端面板视图

实体移动端面板视图中嵌套的是面板部件,可以轻松的进行布局并直观的查看数据。

<component-iframe router="/iframe/view/de/app-mob-panel-view" />

## 控制器

实体移动端面板视图与其他视图不同，没有引擎固有逻辑对象去处理视图与部件的交互。当该视图初始化时会重写视图状态订阅对象。

核心逻辑如下：

``` ts
  /**
   * @description 视图基础数据初始化
   * @memberof MobPanelViewController
   */
  public viewBasicInit() {
    super.viewBasicInit();
    if (this.viewState) {
      this.viewStateEvent = this.viewState.subscribe(({ tag, action, data }: IViewStateParam) => {
        if (!Object.is(tag, this.viewInstance.codeName)) {
          return;
        }
        if (Object.is(action, 'load')) {
          const controller = ModelTool.findPSControlByType('PANEL', this.viewInstance.getPSControls());
          this.viewState.next({ tag: controller?.name, action: 'load', data: data});
        }
      });
    }
  }
```
::: tip 提示
实体移动端面板视图控制器继承主数据视图控制器基类，更多逻辑参见 [主数据视图 > 控制器](./de-view.md#控制器)
:::

## UI层

### 获取额外参数

 在计算目标部件所需参数时给部件添加额外参数，在实体移动端面板视图这里就是给面板部件添加视图默认加载 参数(isLoadDefault)为该视图控制器里的isLoadDefault。 

```ts
  /**
   * 额外部件参数
   *
   * @param ctrlProps 部件参数
   * @param controlInstance 部件
   */
  public extraCtrlParam(ctrlProps: any, controlInstance: IPSControl) {
    super.extraCtrlParam(ctrlProps, controlInstance);
    if (controlInstance?.controlType == 'PANEL') {
      Object.assign(ctrlProps, {
        isLoadDefault: this.c.isLoadDefault,
      });
    }
  }
```

::: tip 提示
实体移动端面板视图继承主数据视图，更多逻辑参见 [主数据视图 > UI层](./de-view.md#UI层)
:::

### 布局

#### 绘制视图主容器内容

重写视图主容器内容，绘制面板插槽。

```ts
  /**
   * @description 视图主容器内容
   * @return {*}
   * @memberof AppDefaultMobMDViewLayout
   */
  renderViewMainContainerContent(): any {
    return <div class='view-main-container-content'>{[renderSlot(this.ctx.slots, 'panel')]}</div>;
  }
```

::: tip 提示
该视图布局继承主数据视图布局，更多逻辑参见 [主数据视图 > UI层 > 布局](./de-view.md#布局)
:::