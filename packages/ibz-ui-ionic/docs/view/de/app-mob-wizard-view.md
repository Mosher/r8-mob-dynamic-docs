# 实体移动端向导视图

实体移动端向导视图是通过简化的界面输入，利用向导快速的进行数据的录入及修改。具体的步骤管理和内容呈现则由向导部件完成。

<component-iframe router="/iframe/view/de/app-mob-wizard-view" />

## 控制器

### 引擎初始化

引擎是R8Mob动态模板中视图的固有逻辑对象，用于处理视图的事件以及视图与部件的交互。

当实体移动端向导视图下的所有部件挂载完成后，视图执行挂载逻辑，此时初始化引擎。

引擎初始化完成后，通过视图状态订阅对象(ViewState)通知相应部件加载数据。

核心逻辑如下：

``` ts
    /**
     * 引擎加载
     *
     * @param {*} [opts={}]
     * @memberof MobWizardViewEngine
     */
    public load(opts: any = {}): void {
        super.load(opts);
        if (this.getWizardPanel()) {
            const tag = this.getWizardPanel().name;
            Object.assign(this.view.viewParam, opts);
            this.setViewState2({ tag: tag, action: 'load', viewdata: this.view.viewparams });
        }
    }
```
由上述代码可知，若视图有向导面板部件的时候，引擎加载会通知向导面板部件去加载。

### 完成事件

完成事件（onfinish）是向导视图引擎固有的逻辑对象，在向导表单部件点击完成按钮后执行。

参数：args：向导完成表单

核心逻辑

- 执行视图事件(event)钩子，事件名称为`viewdataschange`（可参见[视图基类 > 视图事件处理](../de/de-view.md#视图事件处理))

- 判断视图是否是路由打开 关闭视图或者返回上一路由

```
    /**
     * 完成
     *
     * @param {*} args
     * @memberof MobWizardViewEngine
     */
    public onfinish(args: any): void {
        this.emitViewEvent('viewdataschange', [args]);
        if(!this.view.viewDefaultUsage){
            this.emitViewEvent('close', null);
        }else{
            this.view.$tabPageExp.onClose(this.view.$route.fullPath);
        }  
    }
```



::: tip 提示
实体移动端向导视图控制器继承主数据视图控制器基类，更多逻辑参见 [主数据视图 > 控制器](./de-view.md#控制器)
:::

## UI层

::: tip 提示
实体移动端向导视图继承主数据视图，更多逻辑参见 [主数据视图 > UI层](./de-view.md#UI层)
:::

### 布局

#### 绘制视图主容器内容

重写视图主容器内容，绘制向导插槽。

```ts
  /**
   * @description 渲染部件
   * @return {*}
   * @memberof AppDefaultMobWizardViewLayout
   */
  public renderViewMainContainerContent(): any {
    return <div class='view-main-container-content'>{[renderSlot(this.ctx.slots, 'wizardpanel')]}</div>;
  }
```

::: tip 提示
该视图布局继承主数据视图布局，更多逻辑参见 [主数据视图 > UI层 > 布局](./de-view.md#布局)
:::