# 主数据视图基类

主数据视图基类是所有具有业务能力(即具有实体)的视图的逻辑基类。

## 控制器

### 视图初始化

| 逻辑               | 方法名             | 说明                                       |
| ------------------ | ------------------ | ------------------------------------------ |
| 初始化实体服务     | initAppDataService | 根据当前视图的实体获取实体服务             |
| 初始化应用界面服务 | initAppUIService   | 根据当前视图的实体获取应用界面服务(UI服务) |

### 计算工具栏状态

计算工具栏状态事件（calcToolbarItemState）是实体视图引擎固有的逻辑对象，主要是处理工具栏按钮的显示状态，通常在视图加载完成后执行。

参数：state 操作状态

核心逻辑：

​	遍历视图工具栏模型（右侧工具栏、底部工具栏等），若没有工具栏，则结束事件。

​	判断界面行为项的数据目标，若是单项数据主键或者是多数据主键则将该项禁用状态设置为参数操作状态

​	若工具栏项无操作权限，则隐藏

```ts
  /**
   * 计算工具栏状态
   *
   * @param {boolean} state
   * @param {*} [dataaccaction]
   * @memberof ViewEngine
   */
  public calcToolbarItemState(state: boolean) {
    this.view.toolbarModelList?.forEach((tool: any) => {
      if (!this.view[tool] || Object.keys(this.view[tool]).length === 0) {
        return;
      }

      for (const key in this.view[tool]) {
        if (!this.view[tool][key]) {
          return;
        }
        const _item = this.view[tool][key];
        // 判断界面行为数据目标
        if (
          _item.uiaction &&
          (Object.is(_item.uiaction.target, 'SINGLEKEY') || Object.is(_item.uiaction.target, 'MULTIKEY'))
        ) {
          _item.disabled = state;
        }
        _item.visabled = true;
        // 判断工具栏项权限标识
        if (_item.noprivdisplaymode && _item.noprivdisplaymode === 6) {
          _item.visabled = false;
        }
      }
    });
  }
```

### 视图事件处理

视图事件处理（emitViewEvent）是视图UI和控制器交互的一种表现形式。具体可参见（[**交互和事件处理**](../app-view-base.md#交互和事件处理)）

核心逻辑：调用视图钩子event函数

```
  /**
   * 视图事件处理
   *
   * @param {string} eventName 事件tag
   * @param {*} args 事件参数
   * @memberof ViewEngine
   */
  public emitViewEvent(eventName: string, args: any): void {
    if (this.view) {
      this.view.hooks.event.callSync({
        viewName: this.view.viewInstance?.codeName,
        action: eventName,
        data: args,
      });
    }
  }
```





::: tip 提示

主数据视图基类控制器继承视图基类控制器，更多视图初始化逻辑参见 [视图 > 控制器 > 视图初始化](../app-view-base.md#视图初始化)

:::

## UI层

::: tip 提示

主数据视图基类UI层继承视图基类UI层，更多逻辑参见 [视图 > UI层](../app-view-base.md#ui层)

:::

### 布局

::: tip 提示
主数据视图基类布局继承视图基类布局，更多逻辑参见 [视图 > UI层 > 布局](../app-view-base.md#布局)
:::

