# 实体移动端HTML视图

实体移动端HTML视图是展示网页的视图，此网页可以是其他网站的网页，也可以是本应用的其他视图，只需将传递的htmlUrl设置为特定的路由路径。

<component-iframe router="/iframe/view/de/app-mob-html-view" />

## 控制器

HTML视图由于只展示页面，数据逻辑也只有处理网页url，所以本视图没有引擎等复杂逻辑。

- 配置网页url基础路径，配置平台暂未支持配置url基础路径，待补充。配置路径如下

  ```ts
  https://www.baidu.com//
  ```

- 配置url参数，由于某些页面需要参数才能查看相应数据，所以R8移动端组件库支持url传递参数。首先将基础url为传递参数的格式。

  ```ts
  https://www.baidu.com/s?wd=${context.searchkey}${data.searchvalue}
  ```

  context.searchkey为拿取视图上下文中的searchkey，data.searchvalue为拿取当前视图参数的searchvalue。

::: tip 提示
实体移动端HTML视图控制器继承主数据视图控制器，更多逻辑参见 [主数据视图 > 控制器](./de-view.md#控制器)
:::

## UI层

HTML视图由于只展示url页面,所以也没有相应部件，直接绘制html内容，StringUtil.fillStrData方法为解析配置url路径中${}中的数据

```ts
public renderHtmlContent() {
    if (this.c?.viewInstance?.htmlUrl) {
      const iframeUrl = StringUtil.fillStrData(this.c.viewInstance.htmlUrl, this.c.context, this.c.viewParam);
      return <iframe class='view-html-container' src={iframeUrl}></iframe>;
    } else {
      return (
        <div class='view-error-container'>
          <img src='assets/images/404.jpg' alt='404' />
          <div class='error-text'>{`${this.$tl('view.mobhtmlview.notexist','抱歉，您访问的页面不存在！')}`}</div>
        </div>
      );
    }
  }
```

::: tip 提示
实体移动端编辑视图UI层继承主数据视图UI层，更多逻辑参见 [主数据视图 > UI层](./de-view.md#UI层)
:::

### 布局

#### 绘制视图主容器内容

重写视图主容器内容，绘制html内容。

```ts
  /**
   * @description 视图主容器内容
   * @return {*}
   * @memberof AppDefaultMobHtmlViewLayout
   */
  renderViewMainContainerContent(): any {
    return <div class='view-main-container-content'>{renderSlot(this.ctx.slots, 'htmlContent')}</div>;
  }
```

::: tip 提示
该视图布局继承主数据视图布局，更多逻辑参见 [主数据视图 > UI层 > 布局](./de-view.md#布局)
:::