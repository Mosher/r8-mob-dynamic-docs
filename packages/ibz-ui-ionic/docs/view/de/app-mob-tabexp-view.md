# 实体移动端分页导航视图

实体移动端分页导航视图可以在一个界面中定义多个视图进行查看，可以快速的查看与比较关联的数据。具体的呈现内容则由分页导航面板部件呈现。

<component-iframe router="/iframe/view/de/app-mob-tabexp-view" />

## 控制器

### 引擎初始化

引擎是R8Mob动态模板中视图的固有逻辑对象，用于处理视图的事件以及视图与部件的交互。

当实体移动端分页导航视图下的所有部件挂载完成后，视图执行挂载逻辑，此时初始化引擎。

引擎初始化完成后，通过视图状态订阅对象(ViewState)通知相应部件加载数据。

核心逻辑如下：

``` ts
  /**
   * 引擎加载
   *
   * @memberof MobTabExpViewEngine
   */
  public load(opts?: any): void {
    super.load(opts);
    if (this.tabexppanel) {
      const tag = this.tabexppanel.name;
      Object.assign(this.view.viewParam, opts);
      this.setViewState2({ tag: tag, action: 'load', viewdata: { ...this.view.viewParam } });
    }
  }
```

由上述代码可知，若视图中存在分页导航面板部件时，引擎对象会通知分页导航面板部件部件执行load行为。

::: tip 提示
实体移动端分页导航视图控制器继承主数据视图控制器基类，更多逻辑参见 [主数据视图 > 控制器](./de-view.md#控制器)
:::

## UI层

### 渲染信息栏

实体移动端分页导航视图支持信息栏，用于展示当前业务数据的主要信息。

```ts
  /**
   * @description 渲染信息栏
   * @return {*} 
   * @memberof AppMobTabExpView
   */
  public renderDataInfoBar() {
    if (this.c.viewInstance.showDataInfoBar) {
      return (
        <span class="view-info-bar">
          {this.c.getData()?.srfmajortext}
        </span>
      )
    }
  }
```

### 绘制视图组件

重写绘制视图组件，加入绘制信息栏renderDataInfoBar。

```ts
  /**
   * @description 渲染视图组件
   * @return {*} 
   * @memberof AppMobTabExpView
   */
  public renderViewControls() {
    const controlObject = super.renderViewControls();
    Object.assign(controlObject, {
      'dataInfoBar': () => this.renderDataInfoBar()
    })
    return controlObject;
  }
```

::: tip 提示
实体移动端分页导航视图继承主数据视图，更多逻辑参见 [主数据视图 > UI层](./de-view.md#UI层)
:::

### 布局

#### 绘制视图主容器内容

重写视图主容器内容，绘制分页导航部件插槽。

```ts
  /**
   * @description 视图主容器内容
   * @return {*}
   * @memberof AppDefaultMobTabExpViewLayout
   */
  renderViewMainContainerContent(): any {
    return <div class='view-main-container-content'>{[renderSlot(this.ctx.slots, 'tabexppanel')]}</div>;
  }
```

::: tip 提示
该视图布局继承主数据视图布局，更多逻辑参见 [主数据视图 > UI层 > 布局](./de-view.md#布局)
:::