# 视图基类
R8Mob动态模板将视图的UI与数据逻辑分隔开来，减少了其之间的耦合。其中负责逻辑部分的我们称其为控制器，下面对这两部分进行介绍。
## 控制器
控制器是R8Mob动态模板特有的概念，它是将与UI不相关的逻辑独立开来，只负责视图实例对象的管理以及数据或行为的操作逻辑。

### 视图初始化

视图控制器构建好后，进行**视图的初始化**。

| 流程               | 方法名                 | 说明                                                         |
| ------------------ | ---------------------- | ------------------------------------------------------------ |
| 初始化模型服务     | initModelService       | 模型服务用于获取视图、实体等实例对象                         |
| 初始化视图实例对象 | initViewInstance       | 使用模型服务获取视图实例对象，供后续逻辑使用                 |
| 初始化视图基础数据 | viewBasicInit          | 初始化行为等基础参数，同时使用ViewState订阅视图通知          |
| 初始化挂载状态集合 | initMountedMap         | 初始化部件挂载集合，用于判断部件挂载状态                     |
| 初始化视图操作参数 | initViewCtx            | 根据视图的业务数据和模型实例初始化视图的应用上下文及导航视图参数 |
| 初始化视图消息服务 | initViewMessageService | 使用视图消息服务去加载视图消息组，初始化视图消息集合，详情参见[视图消息](#初始化视图消息服务) |
| 初始化计数器服务   | initCounterService     | 使用计数器服务去加载应用计数器引用集合，初始化计数器服务对象集合，详情参见[计数器服务](#初始化视图消息服务) |
| 初始化实体服务     | initAppDataService     | 若当前视图存在实体，则初始化该实体的实体服务对象             |
| 初始化UI服务       | initAppUIService       | 若当前视图存在实体，则初始化该实体的实体UI对象               |
| 初始化快速分组     | initQuickGroup         | 若当前视图存在快速分组，则初始化快速分组属性、模式、代码表等 |
| 初始化视图逻辑     | initViewLogic          | 根据目标逻辑类型类型初始化界面触发逻辑Map，详情参见[视图逻辑](#初始化视图逻辑) |

模型数据加载完成之后执行 **模型数据加载完成钩子**

```ts
  /**
   * @description 视图初始化
   * @return {*}
   * @memberof AppViewControllerBase
   */
  public async viewInit() {
    await this.initModelService();
    await this.initViewInstance();
    this.viewBasicInit();
    // 调用模型数据加载完成钩子
    this.hooks.modelLoaded.callSync({ arg: this.viewInstance });
    this.initMountedMap();
	·······
  }
```

### 初始化视图消息服务

使用视图消息服务去加载视图消息组，**初始化视图消息集合** 。

```ts
  /**
   * @description 初始化视图消息服务
   * @memberof AppViewControllerBase
   */
  public async initViewMessageService() {
    const viewMsgGroup = this.viewInstance?.getPSAppViewMsgGroup?.();
    if (viewMsgGroup) {
      this.viewMessageService = App.getViewMSGService();
      await this.viewMessageService.loaded(viewMsgGroup, this.context, this.viewParam);
    }
  }
```

### 初始化计数器服务

使用计数器服务去加载应用计数器引用集合，初始化模型数据加载完成之后执行 **模型数据加载完成钩子**

```ts
  /**
   * @description 视图初始化
   * @return {*}
   * @memberof AppViewControllerBase
   */
  public async viewInit() {
    await this.initModelService();
    await this.initViewInstance();
    this.viewBasicInit();
    // 调用模型数据加载完成钩子
    this.hooks.modelLoaded.callSync({ arg: this.viewInstance });
    this.initMountedMap();
	·······
  }
```

。

```ts
  /**
   * @description 初始化计数器服务
   * @memberof AppViewControllerBase
   */
  public async initCounterService() {
    const appCounterRef: Array<IPSAppCounterRef> = this.viewInstance.getPSAppCounterRefs() || [];
    if (appCounterRef && appCounterRef.length > 0) {
      for (const counterRef of appCounterRef) {
        const counter = counterRef.getPSAppCounter?.();
        if (counter) {
          await counter.fill(true);
          const counterService = App.getCounterService();
          await counterService.loaded(counter, { navContext: this.context, navViewParam: this.viewParam });
          const tempData: any = { id: counterRef.id, path: counter.modelPath, service: counterService };
          this.counterServiceArray.push(tempData);
        }
      }
    }
  }
```

### 初始化视图逻辑

当目标逻辑类型类型logicType为实体界面逻辑、系统预置界面逻辑、前端扩展插件、脚本代码时

如果逻辑触发logicTrigger为TIMER时， 界面触发逻辑Map就添加一个新的界面定时器触发逻辑引擎AppTimerEngine对象；

如果逻辑触发logicTrigger为CTRLEVENT时，界面触发逻辑Map就添加一个新的界面部件事件触发逻辑引擎AppCtrlEventEngine对象；

如果逻辑触发logicTrigger为VIEWEVENT时，界面触发逻辑Map就添加一个新的界面视图事件触发逻辑引擎AppViewEventEngine对象；

支持绑定用户自定义事件。

```ts
  /**
   * @description 初始化视图逻辑
   * @memberof AppViewControllerBase
   */
  public async initViewLogic() {
    if (
      this.viewInstance.getPSAppViewLogics() &&
      (this.viewInstance.getPSAppViewLogics() as IPSAppViewLogic[]).length > 0
    ) {
      (this.viewInstance.getPSAppViewLogics() as IPSAppViewLogic[]).forEach((element: any) => {
        // 目标逻辑类型类型为实体界面逻辑、系统预置界面逻辑、前端扩展插件、脚本代码
        if (
          element &&
          element.logicTrigger &&
          (Object.is(element.logicType, 'DEUILOGIC') ||
            Object.is(element.logicType, 'SYSVIEWLOGIC') ||
            Object.is(element.logicType, 'PFPLUGIN') ||
            Object.is(element.logicType, 'SCRIPT'))
        ) {
          switch (element.logicTrigger) {
            case 'TIMER':
              this.viewTriggerLogicMap.set(element.name.toLowerCase(), new AppTimerEngine(element));
              break;
            case 'CTRLEVENT':
              if (element?.getPSViewCtrlName() && element?.eventNames) {
                this.viewTriggerLogicMap.set(
                  `${element.getPSViewCtrlName()?.toLowerCase()}-${element.eventNames?.toLowerCase()}`,
                  new AppCtrlEventEngine(element),
                );
              }
              break;
            case 'VIEWEVENT':
              if (element?.eventNames) {
                this.viewTriggerLogicMap.set(`${element.eventNames?.toLowerCase()}`, new AppViewEventEngine(element));
              }
              break;
            default:
              LogUtil.log(`视图${element.logicTrigger}类型暂未支持`);
              break;
          }
        }
        // 绑定用户自定义事件
        if (element.eventNames && element.eventNames.toLowerCase().startsWith('_')) {
          const eventName = element.eventNames.toLowerCase().slice(1);
          this.on(eventName, (...args: any) => {
            this.handleViewCustomEvent(element.eventNames?.toLowerCase(), null, args);
          });
        }
      });
    }
  }
```

### 设置已经绘制完成状态

视图初始化完成后调用该方法，将mountedMap里的状态变成完成，然后判断该Map里是否所有状态已都是完成，如果都是的话去调用视图挂载方法。

```ts
  /**
   * @description 设置已经绘制完成状态
   * @param {string} [name='self'] 部件名
   * @param {*} [data] 数据
   * @memberof AppViewControllerBase
   */
  public setIsMounted(name: string = 'self', data?: any) {
    this.mountedMap.set(name, true);
    if (data) {
      this.ctrlRefsMap.set(name, data);
    }
    if ([...this.mountedMap.values()].indexOf(false) == -1) {
      // 执行viewMounted
      if (!this.hasViewMounted) {
        this.viewMounted();
      }
    }
  }
```

### 视图挂载

当视图所有部件都挂载完成后，若该视图存在引擎，则进行引擎初始化。同时触发挂载之后钩子，参数为视图本身。最后向上抛出视图挂载完成事件。

```ts
  /**
   * @description 视图挂载
   * @memberof AppViewControllerBase
   */
  public viewMounted() {
    // 设置挂载状态
    this.hasViewMounted = true;
    if (this.engine) {
      this.engineInit();
    }
    this.hooks.mounted.callSync({ arg: this });
    this.viewEvent(AppViewEvents.MOUNTED);
  }
```

### 部件事件处理

当视图部件抛出事件时，UI层捕获后交由视图控制器进行处理。具体逻辑如下：

对行为action进行判断，如果是挂载完成行为，则设置该部件为已经绘制完成状态 ，如果是关闭行为，则关闭该视图，如果是工具栏点击行为，则调用处理工具栏点击事件进行处理。

默认情况下，如果界面触发逻辑Map中存在该部件的此行为，则调用界面触发引擎执行界面逻辑，并让该视图引擎调用部件事件。

```ts
  /**
   * @description 处理部件事件
   * @param {string} controlname 部件名称
   * @param {string} action 部件行为
   * @param {*} data 数据
   * @memberof AppViewControllerBase
   */
  public handleCtrlEvent(controlname: string, action: string, data: any) {
    switch (action) {
      case AppCtrlEvents.MOUNTED:
        this.setIsMounted(controlname, data);
        break;
      case AppCtrlEvents.CLOSE:
        this.closeView(data);
        break;
      case MobToolbarEvents.TOOLBAR_CLICK:
        this.handleToolBarClick(data.tag, data.event);
        break;
      default:
        // 部件事件
        if (controlname && action) {
          if (this.viewTriggerLogicMap.get(`${controlname.toLowerCase()}-${action.toLowerCase()}`)) {
            const ctrlEventLogic = this.viewTriggerLogicMap.get(`${controlname.toLowerCase()}-${action.toLowerCase()}`);
            ctrlEventLogic
              .executeAsyncUILogic({ arg: data, utils: this.viewCtx, environments: this.getUIEnvironmentParam() })
              .then((args: any) => {
                if (args && args?.hasOwnProperty('srfret') && !args.srfret) {
                  return;
                }
                if (this.engine) {
                  this.engine.onCtrlEvent(controlname, action, data);
                }
              });
          } else {
            if (this.engine) {
              this.engine.onCtrlEvent(controlname, action, data);
            }
          }
        } else {
          LogUtil.warn('部件名称或者行为异常');
        }
        break;
    }
  }
```

### 计数器刷新

当计数器对象发生变化时（如切换快速分组），视图进行计数器的刷新，重新获取界面计数器数据。

```ts
  /**
   * @description 计数器刷新
   * @memberof AppViewControllerBase
   */
  public counterRefresh() {
    if (this.counterServiceArray && this.counterServiceArray.length > 0) {
      this.counterServiceArray.forEach((item: any) => {
        const counterService = item.service;
        if (
          counterService &&
          counterService.refreshCounterData &&
          counterService.refreshCounterData instanceof Function
        ) {
          counterService.refreshCounterData(this.context, this.viewParam);
        }
      });
    }
  }
```

### 处理自定义视图导航数据

UI层调用初始化视图导航参数时会调用该方法，处理自定义视图数据时先从导航上下文取数，没有再从导航参数（URL）取数，如果导航上下文和导航参数都没有则为null。

```ts
  /**
   * @description 处理自定义视图导航数据
   * @param {IParam} context 上下文
   * @param {IParam} viewParam 视图参数
   * @memberof AppViewControllerBase
   */
  public handleCustomViewData(context: IParam, viewParam: IParam) {
    this.handleviewRes();
    if (this.customViewNavContexts.length > 0) {
      this.customViewNavContexts.forEach((item: any) => {
        const tempContext: any = {};
        const curNavContext: any = item;
        this.handleCustomDataLogic(context, viewParam, curNavContext, tempContext, item.key);
        Object.assign(context, tempContext);
      });
    }
    if (this.customViewParams.length > 0) {
      this.customViewParams.forEach((item: any) => {
        const tempParam: any = {};
        const curNavParam: any = item;
        this.handleCustomDataLogic(context, viewParam, curNavParam, tempParam, item.key);
        Object.assign(viewParam, tempParam);
      });
    }
  }
```

### 处理其他数据(多实例)

UI层调用初始化视图导航参数时会调用该方法，如果环境变量中bDynamic为true，则会在视图参数中加入srfinsttag和srfinsttag2属性。

```ts
  /**
   * @description 处理其他数据(多实例)
   * @param {IParam} context 上下文
   * @param {IParam} viewParam 视图参数
   * @memberof AppViewControllerBase
   */
  public handleOtherViewData(context: IParam, viewParam: IParam) {
    const appEnvironment = App.getEnvironment();
    if (appEnvironment?.bDynamic && this.modelService) {
      const dynainstParam: DynamicInstanceConfig = this.modelService.getDynaInsConfig();
      if (dynainstParam) {
        Object.assign(viewParam, { srfinsttag: dynainstParam.instTag, srfinsttag2: dynainstParam.instTag2 });
      }
    }
  }
```

### 视图关闭

调用视图钩子对象的closeView

```ts
  /**
   * @description 关闭视图
   * @param {*} [args] 关闭参数
   * @memberof AppViewControllerBase
   */
  public closeView(args?: any) {
    this.hooks.closeView.callSync({ arg: args });
  }
```

### 视图销毁

为防止内存溢出，在视图销毁时，R8Mob动态模板会从内存中删除该视图关联的资源，如：从导航服务中删除该视图的数据、取消订阅、销毁计数器定时器等等。

```ts
  /**
   * @description 视图销毁
   * @memberof AppViewControllerBase
   */
  public async viewDestroy() {
    if (Object.is(this.viewShowMode, 'ROUTE')) {
      if (App.getStore()) {
        // 清除顶层路由参数
        App.getStore().delete(`routeViewGlobal${this.context.srfsessionid}`);
        // 清除顶层视图
        App.getStore().delete(`topview${this.context.srfsessionid}`);
      }
    }
    // 清除当前视图
    if (App.getStore()) {
      if (this.viewInstance && this.viewInstance.modelPath) {
        App.getStore().delete(`parentview${this.viewInstance.modelPath}`);
      }
    }
    // 销毁计数器定时器
    if (this.counterServiceArray && this.counterServiceArray.length > 0) {
      this.counterServiceArray.forEach((item: any) => {
        if (item?.service?.destroyCounter && item.service.destroyCounter instanceof Function) {
          item.service.destroyCounter();
        }
      });
    }
    // 销毁逻辑定时器
    this.destroyLogicTimer();
    // 取消订阅
    if (this.viewStateEvent) {
      this.viewStateEvent.unsubscribe();
    }
    this.cancelSubscribe();
  }
```

## UI层

UI层是对视图控制器初始化的视图实例对象使用HTML页面进行呈现的一种解释，它是用户实际看到并进行操作的层面。UI的变化根据视图模型的不同也会进行相应的变化。

### 视图初始化

开始绘制部件时，初始化视图导航参数，随后调用控制器的初始化逻辑。

当控制器初始化完成后，抛出视图初始化完成事件。

```ts
  /**
   * @description 视图初始化
   * @memberof ViewComponentBase
   */
  public init() {
    this.initViewNavParam();
    this.c.viewInit().then((result: boolean) => {
      this.emitViewEvent({ viewName: this.c.viewInstance?.codeName, action: AppViewEvents.INITED, data: result });
    });
  }
```

### 初始化视图导航参数

重置视图上下文context、视图参数viewParam以及视图导航数据navDatas。

如果视图输入属性中有导航视图参数navParam，就将其合入视图参数。如果视图输入属性中有 导航数据 navDatas，就将其合入视图导航数据。如果App的上下文存在，就将其合入视图上下文。

如果视图输入属性中的视图打开模式viewShowMode为模态打开MODEL或者嵌入视图EMBEDDED，并且输入属性中有导航上下文navContext，就将导航上下文合入视图上下文，并将视图上下文中的srfparentdename，srfparentdemapname，srfparentkey合入视图参数中，再调用处理[自定义视图导航数据](#处理自定义视图导航数据)handleCustomViewData以及[处理其他数据(多实例)](#处理其他数据(多实例))handleOtherViewData并返回。

当视图输入属性中的视图打开模式不为MODEL或者EMBEDDED时，会从当前路由格式化路由参数，往上下文context和视图参数viewParam中合入路由参数。

如果视图模型对象存在，还会在context中合入srfsessionid，即用工具类创建出的唯一id。

如果视图参数中存在srfinsttag和srfinsttag2，则会通过模型服务对象modelService去获取动态实例配置，然后将动态实例配置的srfdynainstid合入上下文中,如果视图参数中已经有了srfdynainstid，则直接将视图参数中的srfdynainstid合入上下文。

如果是视图参数中存在srfsandboxtag，则补充沙箱实例参数（路由），将srfsandboxtag合入context。最后调用处理自定义视图导航数据handleCustomViewData以及处理其他数据(多实例)方法handleOtherViewData。

```ts
  /**
   * @description 初始化视图导航参数
   * @param {*} [inputvalue=null] 输入值
   * @return {*}
   * @memberof ViewComponentBase
   */
  public initViewNavParam(inputvalue: any = null) {
    this.c.context = {};
    this.c.viewParam = {};
    this.c.navDatas = [];
    if (this.props && this.props.navParam) {
      Object.assign(this.c.viewParam, this.props.navParam);
    }
    if (this.props && this.props.navDatas) {
      Object.assign(this.c.navDatas, this.props.navDatas);
    }
    if (App.getContext()) {
      Object.assign(this.c.context, App.getContext() ? App.getContext() : {});
    }
    if (
      this.props.viewShowMode &&
      (this.props.viewShowMode === 'MODEL' || this.props.viewShowMode === 'EMBEDDED') &&
      this.props.navContext
    ) {
      Object.assign(this.c.context, this.props.navContext);
      if (this.c.context && this.c.context.srfparentdename) {
        Object.assign(this.c.viewParam, { srfparentdename: this.c.context.srfparentdename });
      }
      if (this.c.context && this.c.context.srfparentdemapname) {
        Object.assign(this.c.viewParam, { srfparentdemapname: this.c.context.srfparentdemapname });
      }
      if (this.c.context && this.c.context.srfparentkey) {
        Object.assign(this.c.viewParam, { srfparentkey: this.c.context.srfparentkey });
      }
      if (this.c) {
        this.c.handleCustomViewData(this.c.context, this.c.viewParam);
        this.c.handleOtherViewData(this.c.context, this.c.viewParam);
      }
      return;
    }
    const path = this.route.matched[this.route.matched.length - 1].path;
    const keys: Array<any> = [];
    const curReg = this.pathToRegExp.pathToRegexp(path, keys);
    const matchArray = curReg.exec(this.route.path);
    const tempValue: Object = {};
    keys.forEach((item: any, index: number) => {
      if (matchArray[index + 1]) {
        Object.defineProperty(tempValue, item.name, {
          enumerable: true,
          value: decodeURIComponent(matchArray[index + 1]),
        });
      }
    });
    ViewTool.formatRouteParams(tempValue, this.route, this.c.context, this.c.viewParam);
    if (inputvalue && typeof inputvalue === 'string' && this.c && this.c.viewInstance?.getPSAppDataEntity()?.codeName) {
      Object.assign(this.c.context, {
        [(this.c.viewInstance?.getPSAppDataEntity()?.codeName as string).toLowerCase()]: inputvalue,
      });
    }
    if (this.c && this.c.viewInstance) {
      Object.assign(this.c.context, { srfsessionid: Util.createUUID() });
    }
    if (this.c.viewParam.srfinsttag && this.c.viewParam.srfinsttag2 && this.c && this.c.modelService) {
      const dynainstParam: DynamicInstanceConfig = this.c.modelService.getDynaInsConfig();
      Object.assign(this.c.context, { srfdynainstid: dynainstParam.id });
    }
    if (this.c.viewParam && this.c.viewParam.srfdynainstid) {
      Object.assign(this.c.context, { srfdynainstid: this.c.viewParam.srfdynainstid });
    }
    // 补充沙箱实例参数（路由）
    if (this.c.viewParam && this.c.viewParam.hasOwnProperty('srfsandboxtag')) {
      Object.assign(this.c.context, { srfsandboxtag: this.c.viewParam.srfsandboxtag });
    }
    if (this.c) {
      this.c.handleCustomViewData(this.c.context, this.c.viewParam);
      this.c.handleOtherViewData(this.c.context, this.c.viewParam);
    }
  }
```

### 渲染视图

通过视图类型以及视图样式获取对应要绘制的视图组件，然后用Vue去绘制，传入视图实例、导航上下文、导航参数、导航数据、是否显示信息栏、模型服务等props，视图内渲染视图中嵌入的部件。

```ts
  /**
   * @description 绘制视图
   * @return {*}
   * @memberof ViewComponentBase
   */
  render() {
    if (!unref(this.viewIsLoaded) || !this.c.viewInstance) {
      return null;
    }
    const targetViewLayoutComponent: string = App.getComponentService().getLayoutComponent(
      this.c.viewInstance?.viewType,
      this.c.viewInstance?.viewStyle,
    );
    return h(
      targetViewLayoutComponent,
      {
        viewInstance: this.c.viewInstance,
        navContext: this.c.context,
        navParam: this.c.viewParam,
        navDatas: this.c.navDatas,
        isShowCaptionBar: this.c.isShowCaptionBar,
        modelService:this.c.modelService
      },
      this.renderViewControls(),
    );
  }
```

### 渲染视图部件

通过部件实例对象获取到部件组件实例，计算参数computeTargetCtrlData后通过Vue绘制部件。如果绘制了视图消息组，则还会将视图消息加入绘制中。

```ts
  /**
   * @description 绘制视图所有部件
   * @return {*}  {*}
   * @memberof ViewComponentBase
   */
  public renderViewControls(): any {
    const controlObject: any = {};
    if (
      this.c.viewInstance &&
      this.c.viewInstance.getPSControls() &&
      (this.c.viewInstance.getPSControls() as IPSControl[]).length > 0
    ) {
      (this.c.viewInstance.getPSControls() as IPSControl[]).forEach((control: IPSControl) => {
        controlObject[control.name] = () => {
          return this.computeTargetCtrlData(control);
        };
      });
    }
    //  视图消息
    if (this.c.viewInstance.getPSAppViewMsgGroup?.()) {
      Object.assign(controlObject, {
        topMessage: () => this.renderTopMessage(),
        bodyMessage: () => this.renderBodyMessage(),
        bottomMessage: () => this.renderBottomMessage(),
      });
    }
    return controlObject;
  }
```

### 渲染视图消息

视图消息常用语对当前视图作解释说明。按照显示位置position分为

- 视图头部
- 视图内容区
- 视图尾部

因此，UI层根据控制器初始化出的视图消息服务渲染出不同位置的视图消息内容。视图消息支持配置消息类型、 标题 、 关闭模式 、 图标、内容等。

消息类型分为常规、警告、错误和自定义四种类型，每种类型拥有不同的颜色。

关闭模式分为无删除、默认删除、本次删除三种模式。无删除的情况下没有删除按钮 ，默认删除会在删除一次后不再显示，本次删除会在删除后刷新视图仍然显示。

```ts
  /**
   * @description 绘制视图顶部视图消息
   * @return {*}  {*}
   * @memberof ViewComponentBase
   */
  public renderTopMessage(): any {
    let msgDetails: any[] = [];
    if (this.c.viewMessageService) {
      msgDetails = this.c.viewMessageService.getViewMsgDetails('TOP');
    }
    if (msgDetails.length > 0) {
      return (
        <div class='view-message--top'>
          <app-mob-alert
            position='TOP'
            messageDetails={msgDetails}
            context={Util.deepCopy(this.c.context)}
            viewParam={Util.deepCopy(this.c.viewParam)}
            infoGroup={this.c.viewInstance.getPSAppViewMsgGroup()?.codeName}
            viewName={this.c.viewInstance.codeName.toLowerCase()}
          ></app-mob-alert>
        </div>
      );
    }
  }

  /**
   * @description 绘制视图内容区视图消息
   * @return {*}  {*}
   * @memberof ViewComponentBase
   */
  public renderBodyMessage(): any {
    let msgDetails: any[] = [];
    if (this.c.viewMessageService) {
      msgDetails = this.c.viewMessageService.getViewMsgDetails('BODY');
    }
    if (msgDetails.length > 0) {
      return (
        <div class='view-message--body'>
          <app-mob-alert
            position='TOP'
            messageDetails={msgDetails}
            context={Util.deepCopy(this.c.context)}
            viewParam={Util.deepCopy(this.c.viewParam)}
            infoGroup={this.c.viewInstance.getPSAppViewMsgGroup()?.codeName}
            viewName={this.c.viewInstance.codeName.toLowerCase()}
          ></app-mob-alert>
        </div>
      );
    }
  }

  /**
   * @description 绘制视图下方视图消息
   * @return {*}  {*}
   * @memberof ViewComponentBase
   */
  public renderBottomMessage(): any {
    let msgDetails: any[] = [];
    if (this.c.viewMessageService) {
      msgDetails = this.c.viewMessageService.getViewMsgDetails('BOTTOM');
    }
    if (msgDetails.length > 0) {
      return (
        <div class='view-message--bottom'>
          <app-mob-alert
            position='TOP'
            messageDetails={msgDetails}
            context={Util.deepCopy(this.c.context)}
            viewParam={Util.deepCopy(this.c.viewParam)}
            infoGroup={this.c.viewInstance.getPSAppViewMsgGroup()?.codeName}
            viewName={this.c.viewInstance.codeName.toLowerCase()}
          ></app-mob-alert>
        </div>
      );
    }
  }
```

### 视图关闭

如果当前视图展示模式为路由时，则回退路由，否则抛出视图关闭事件。

```ts
  /**
   * @description 视图关闭
   * @param {*} [args] 事件参数
   * @memberof ViewComponentBase
   */
  public viewClose(args?: any) {
    if (this.c.viewShowMode && Object.is(this.c.viewShowMode, 'ROUTE')) {
      this.router.back();
    } else {
      this.emitViewEvent({ viewName: this.c.viewInstance?.codeName, action: AppViewEvents.CLOSE, data: args });
    }
  }
```

### 视图销毁

调用控制器的视图销毁逻辑，同时销毁所有事件钩子。

```ts
  /**
   * @description 视图销毁
   * @memberof ViewComponentBase
   */
  public unmounted(): void {
    this.c.viewDestroy();
    this.c.hooks.modelLoaded.removeTap(this.initViewNavParam);
    this.c.hooks.event.removeTap(this.emitViewEvent);
    this.c.hooks.closeView.removeTap(this.viewClose);
    this.c.hooks.mounted.removeTap(this.viewMounted);
    this.emitViewEvent({ viewName: this.c.viewInstance?.codeName, action: AppViewEvents.DESTROYED, data: true });
  }
```

### 布局

#### 绘制布局

如果视图布局面板使用默认布局则绘制视图头renderViewHeader，视图内容renderViewContent，视图底部renderViewFooter，否则绘制视图布局面板。

```ts
  /**
   * @description 布局面板渲染
   * @return {*}
   * @memberof LayoutComponentBase
   */
  render() {
    return (
      <ion-page class={{ 'ion-page': true, ...this.viewClass }}>
        {this.viewLayoutPanel && this.viewLayoutPanel.useDefaultLayout
          ? [this.renderViewHeader(), this.renderViewContent(), this.renderViewFooter()]
          : this.renderViewLayoutPanel()}
      </ion-page>
    );
  }
```

#### 绘制视图头

如果显示视图标题栏的话则绘制视图头，包含标题栏。

```ts
  /**
   * @description 渲染视图头
   * @return {*}  {*}
   * @memberof LayoutComponentBase
   */
  renderViewHeader(): any {
    return this.isShowCaptionBar ? (
      <ion-header className='view-header'>
        <ion-toolbar>
          {this.renderCaptionBar()}
        </ion-toolbar>         
      </ion-header>
    ) : null;
  }
```

##### 绘制标题栏

如果视图配置了显示标题栏showCaptionBar，则去绘制视图标题、信息栏dataInfoBar插槽（取决于showDataInfoBar）以及视图子标题；如果视图配置了显示信息栏showDataInfoBar，则只去绘制信息栏dataInfoBar插槽。

```ts
  /**
   * @description 渲染标题栏
   * @return {*} 
   * @memberof LayoutComponentBase
   */
  public renderCaptionBar() {
    const { showCaptionBar, showDataInfoBar } = this.viewInstance as any;
    if (showCaptionBar) {
      return (
        <ion-title class={'view-caption-bar'}>
          {this.viewInstance.getPSSysImage() && (
            <app-icon class={'view-icon'} icon={this.viewInstance.getPSSysImage()}></app-icon>
          )}
          <span class={'view-caption'}>
            {this.$tl(this.viewInstance?.getCapPSLanguageRes()?.lanResTag, this.viewInstance.caption)}
            {showDataInfoBar ?
              [
                <span>-</span>,
                renderSlot(this.ctx.slots, 'dataInfoBar')
              ] : null
            }
          </span>
          <br />
          <span class={'view-sub-caption'}>
            {this.$tl(this.viewInstance?.getSubCapPSLanguageRes()?.lanResTag, this.viewInstance.subCaption)}
          </span>
        </ion-title>
      )
    } else {
      if (showDataInfoBar) {
        return (
          <ion-title class="view-info-bar">
            {renderSlot(this.ctx.slots, 'dataInfoBar')}
          </ion-title>
        )
      } else {
        return null;
      }
    }
  }  
```

#### 绘制视图内容

绘制包含头部视图信息topMessage插槽、视图顶部插槽、内容区视图消息bodyMessage、 视图主容器内容 、 视图主容器底部等。

```ts
  /**
   * @description 渲染视图内容
   * @return {*}  {*}
   * @memberof LayoutComponentBase
   */
  renderViewContent(): any {
    return (
      <ion-content class={'view-body'}>
        {this.ctx.slots.topMessage ? renderSlot(this.ctx.slots, 'topMessage') : null}
        {this.renderViewTopSlot()}
        <div class='view-main-container'>
          {this.renderViewMainContainerHeader()}
          {renderSlot(this.ctx.slots, 'bodyMessage')}
          {this.renderViewMainContainerContent()}
          {this.renderViewMainContainerFooter()}
        </div>
      </ion-content>
    );
  }
```

##### 绘制视图顶部插槽

绘制左右工具栏插槽。

```ts
  /**
   * @description 视图顶部插槽
   *
   * @return {*}  {any[]}
   * @memberof LayoutComponentBase
   */
  public renderViewTopSlot(): any[] {
    return [renderSlot(this.ctx.slots, 'lefttoolbar'), renderSlot(this.ctx.slots, 'righttoolbar')];
  }
```

##### 绘制视图主容器头部

基类这里只是预定义该方法，各类视图自己重写该方法。

```ts
  /**
   *
   * @description 视图主容器头部
   * @return {*}
   * @memberof LayoutComponentBase
   */
  renderViewMainContainerHeader(): any {
    return null;
  }
```

##### 绘制视图主容器内容

基类这里只是预定义该方法，各类视图自己重写该方法。

```ts
 /**
   *
   * @description 视图主容器内容
   * @return {*}
   * @memberof LayoutComponentBase
   */
  renderViewMainContainerContent(): any {
    return null;
  }
```

##### 绘制视图底部

基类这里只是预定义该方法，各类视图自己重写该方法。

```ts
  /**
   * @description 视图主容器底部
   * @return {*}
   * @memberof LayoutComponentBase
   */
  renderViewMainContainerFooter(): any {
    return null;
  }
```

#### 绘制视图布局面板

如果该面板仅布局内容区layoutBodyOnly，则绘制视图头与视图底部，否则不绘制；内容主题是遍历面板顶级成员集合，根据不同的类型去绘制面板成员。

```ts
  /**
   * @description 渲染视图布局面板
   * @return {*}
   * @memberof LayoutComponentBase
   */
  public renderViewLayoutPanel() {
    const bodyOnly = this.viewLayoutPanel?.layoutBodyOnly;
    return [
      bodyOnly ? this.renderViewHeader() : null,
      <ion-content class={'view-body'}>
        {bodyOnly && this.ctx.slots?.bodyMessage ? renderSlot(this.ctx.slots, 'bodyMessage') : null}
        <ion-row class='app-view-layout-panel'>
          {this.viewLayoutPanel?.getRootPSPanelItems()?.map((item: IPSPanelItem) => {
            return this.renderByDetailType(item);
          })}
        </ion-row>
      </ion-content>,
      bodyOnly ? this.renderViewFooter() : null,
    ];
  }
```

##### 根据面板项类型绘制

面板项类型分为面板容器 、面板属性 、面板直接内容 、面板分页面板 、面板分页、面板控件等类型

```ts
  /**
   * @description 根据面板项类型绘制
   * @param {*} item 面板项
   * @param {boolean} [hasParent=false] 是否具有父容器
   * @return {*}
   * @memberof LayoutComponentBase
   */
  public renderByDetailType(item: any, hasParent: boolean = false) {
    switch (item.itemType) {
      case 'CONTAINER':
        return this.renderContainer(item, hasParent);
      case 'FIELD':
        return this.renderField(item, hasParent);
      case 'RAWITEM':
        return this.renderRawItem(item, hasParent);
      case 'TABPANEL':
        return this.renderTabPanel(item, hasParent);
      case 'TABPAGE':
        return this.renderTabPage(item, hasParent);
      case 'CTRLPOS':
        return this.renderCtrlPos(item, hasParent);
    }
  }
```

##### 绘制面板容器

支持分为flex布局和grid布局两种，调用渲染面板子项方法。

```ts
  /**
   * @description 渲染面板容器
   * @param {IPSPanelContainer} item 面板容器实例对象
   * @param {boolean} [hasParent=false] 是否具有父容器
   * @return {*}
   * @memberof LayoutComponentBase
   */
  public renderContainer(item: IPSPanelContainer, hasParent: boolean = false) {
    const layoutMode = item.getPSLayout()?.layout;
    let containerClass = 'app-view-layout-panel-container';
    if (item.getPSSysCss()) {
      containerClass += ` ${item.getPSSysCss()?.cssName}`;
    }
    //  父为容器时直接绘制子内容
    if (hasParent) {
      return this.renderPanelItems(item);
    }
    if (layoutMode == 'FLEX') {
      return <div class={containerClass}>{this.renderPanelItems(item)}</div>;
    } else {
      const attrs = this.getGridLayoutProps({}, item);
      return <ion-col {...attrs}>{this.renderPanelItems(item)}</ion-col>;
    }
  }
```

##### 绘制面板子项

根据布局的不同去绘制子项，根据模型的不同配置计算面板项样式及类名，从而显示不同的布局效果。

```ts
  /**
   * @description 渲染面板子项
   * @param {*} item 面板项实例对象
   * @return {*}
   * @memberof LayoutComponentBase
   */
  public renderPanelItems(item: any) {
    const panelItems = (item as IPSPanelContainer).getPSPanelItems?.() || [];
    if (!panelItems.length) {
      return null;
    }
    const layout: any = item.getPSLayout();
    const layoutMode = item.getPSLayout()?.layout;
    //  FLEX布局
    if (layout && layoutMode == 'FLEX') {
      let cssStyle: string = 'width: 100%; height: 100%; overflow: auto; display: flex;';
      cssStyle += layout.dir ? `flex-direction: ${layout.dir};` : '';
      cssStyle += layout.align ? `justify-content: ${layout.align};` : '';
      cssStyle += layout.vAlign ? `align-items: ${layout.vAlign};` : '';
      return (
        <div style={cssStyle}>
          {panelItems.map((item: IPSPanelItem, index: number) => {
            const { detailStyle, detailClassName } = this.computePanelItemStyle(item);
            return (
              <div style={detailStyle} class={detailClassName}>
                {this.renderByDetailType(item, true)}
              </div>
            );
          })}
        </div>
      );
    } else {
      //  栅格布局
      return (
        <ion-row style='height: 100%'>
          {panelItems.map((item: IPSPanelItem, index: number) => {
            const { detailStyle, detailClassName } = this.computePanelItemStyle(item);
            const attrs = this.getGridLayoutProps(item, item);
            return (
              <ion-col style={detailStyle} class={detailClassName} {...attrs}>
                {this.renderByDetailType(item, true)}
              </ion-col>
            );
          })}
        </ion-row>
      );
    }
  }
```

##### 计算面板项样式及类名

```ts
  /**
   * @description 计算面板项样式及类名
   * @param {IPSPanelItem} item 面板项
   * @return {*}
   * @memberof LayoutComponentBase
   */
  public computePanelItemStyle(item: IPSPanelItem) {
    const layoutPos = item.getPSLayoutPos();
    //  项样式
    const detailStyle: any = {};
    //  宽高
    const { width, height } = layoutPos as any;
    if (width) {
      Object.assign(detailStyle, { width: width + 'px' });
    }
    if (height) {
      Object.assign(detailStyle, { height: height + 'px' });
    }

    //  class名称
    const detailClassName: any = this.getDetailClass(item);
    if (item.itemType == 'TABPANEL') {
      Object.assign(detailClassName, { 'panel-tabs': true });
    }
    const layoutMode = item.getPSLayout()?.layout;
    if (layoutMode == 'FLEX') {
      const grow = (layoutPos as any).grow;
      Object.assign(detailStyle, { grow: grow != -1 ? grow : 0 });
    }
    return { detailStyle: detailStyle, detailClassName: detailClassName };
  }
```

##### 绘制面板属性

属性主要是有配置编辑器的情况下去渲染编辑器内容。

```ts
  /**
   * @description 渲染面板属性
   * @param {IPSPanelField} item 面板属性实例对象
   * @param {boolean} [hasParent=false] 是否具有父容器
   * @return {*}
   * @memberof LayoutComponentBase
   */
  public renderField(item: IPSPanelField, hasParent: boolean = false) {
    const { caption } = item;
    const editor = item.getPSEditor();
    const content = (
      <div class='panel-item-field'>
        {caption && <ion-label class='panel-item-field-label'>{caption}</ion-label>}
        {editor && this.renderEditor(editor, item)}
      </div>
    );
    if (hasParent) {
      return content;
    } else {
      return this.renderNoParentPanelItem(item, content);
    }
  }
```

##### 绘制编辑器

根据编辑器类型以及编辑器样式去绘制对应的编辑器内容，如果配置了插件则走插件逻辑。

```ts
  /**
   * @description 渲染编辑器
   * @param {IPSEditor} editor 编辑器实例对象
   * @param {IPSPanelField} parentItem 父容器
   * @return {*}
   * @memberof LayoutComponentBase
   */
  public renderEditor(editor: IPSEditor, parentItem: IPSPanelField) {
    const targetCtrlComponent: any = App.getComponentService().getEditorComponent(
      editor?.editorType,
      editor?.editorStyle ? editor?.editorStyle : 'DEFAULT',
      editor.getPSSysPFPlugin()
        ? `${editor?.editorType}_${editor?.editorStyle}`
        : undefined,
    );
    return h(targetCtrlComponent, {
      editorInstance: editor,
      containerCtrl: this.viewInstance,
      contextData: this.context,
      parentItem: parentItem,
      navContext: Util.deepCopy(this.context),
      navParam: Util.deepCopy(this.viewParam),
      value: this.context[editor?.name || ''],
      onEditorEvent: (event: IEditorEventParam) => {
        this.handleEditorEvent(event);
      },
    });
  }
```

##### 绘制直接内容

根据内容类型的不同绘制不同的直接内容，目前支持html、直接内容、图片内容，后续支持markdown类型。

```ts
  /**
   * @description 渲染面板直接内容
   * @param {IPSPanelRawItem} item 面板直接内容对象实例
   * @param {boolean} [hasParent=false] 是否具有父容器
   * @return {*}
   * @memberof LayoutComponentBase
   */
  public renderRawItem(item: IPSPanelRawItem, hasParent: boolean = false) {
    const { contentType, htmlContent, getPSSysImage, rawContent } = item;
    let element: any = undefined;
    if (contentType == 'HTML' && htmlContent) {
      element = h('div', {
        innerHTML: htmlContent,
      });
    }
    let content: any;
    switch (contentType) {
      case 'HTML':
        content = element ? element : htmlContent;
        break;
      case 'RAW':
        content = rawContent;
        break;
      case 'IMAGE':
        content = <img src={getPSSysImage?.()?.imagePath} />;
        break;
      case 'MARKDOWN':
        content = <div>MARKDOWN直接内容暂未支持</div>;
        break;
    }
    if (hasParent) {
      return content;
    } else {
      return this.renderNoParentPanelItem(item, content);
    }
  }
```

##### 绘制面板分页面板

默认选中第一个分页。渲染不同的分页面板头部与内容，点击分页头部切换分页面板内容。

```ts
  /**
   * @description 渲染面板分页面板
   * @param {IPSPanelTabPanel} item 面板分页面板实例对象
   * @param {boolean} [hasParent=false] 是否具有父容器
   * @return {*}
   * @memberof LayoutComponentBase
   */
  public renderTabPanel(item: IPSPanelTabPanel, hasParent: boolean = false) {
    const tabPages = item.getPSPanelTabPages() || [];
    if (tabPages.length) {
      //  默认选中第一个分页
      if (!this.currentTab.value[item.name]) {
        this.currentTab.value[item.name] = tabPages[0].name;
      }
      const content = [
        <div class='header'>
          <ion-segment
            value={this.currentTab.value[item.name]}
            onIonChange={($event: any) => this.segmentChanged(item.name, $event)}
          >
            {tabPages.map((page: IPSPanelTabPage, index: number) => {
              return (
                <ion-segment-button value={page.name}>
                  <ion-label>{page.caption}</ion-label>
                </ion-segment-button>
              );
            })}
          </ion-segment>
        </div>,
        <div class='content'>
          {tabPages.map((page: IPSPanelTabPage, index: number) => {
            return this.renderByDetailType(page);
          })}
        </div>,
      ];
      if (hasParent) {
        return content;
      } else {
        return this.renderNoParentPanelItem(item, content);
      }
    }
  }
```

##### 绘制面板分页

根据当前选中分页currentTab去绘制分页内容。

```ts
  /**
   * @description 渲染面板分页
   * @param {IPSPanelTabPage} item 面板分页实例对象
   * @param {boolean} [hasParent=false] 是否具有父容器
   * @return {*}
   * @memberof LayoutComponentBase
   */
  public renderTabPage(item: IPSPanelTabPage, hasParent: boolean = false) {
    const parent = item.getParentPSModelObject?.() as IPSPanelTabPanel;
    const currentKey = this.currentTab.value[parent?.name];
    return (
      <div class={this.getDetailClass(item)} style={currentKey !== item.name ? 'display: none;' : ''}>
        {this.renderPanelItems(item)}
      </div>
    );
  }
```

##### 渲染面板控件

根据面板组件实例对象的名称去绘制不同的插槽。

```ts
  /**
   * @description 渲染面板控件
   * @param {IPSPanelCtrlPos} item 面板组件实例对象
   * @param {boolean} [hasParent=false] 是否具有父容器
   * @return {*}
   * @memberof LayoutComponentBase
   */
  public renderCtrlPos(item: IPSPanelCtrlPos, hasParent: boolean = false) {
    const content = renderSlot(this.ctx.slots, item.name?.toLowerCase());
    if (hasParent) {
      return content;
    } else {
      return this.renderNoParentPanelItem(item, content);
    }
  }
```

## 交互和事件处理

R8Mob动态模板通过钩子函数连接 视图 UI 控制器

### 事件钩子

R8Mob动态模板的视图组件在视图控制器（c）的钩子对象里注册了多个**视图钩子**。

这样我们在控制器中就可以直接通过调用**钩子函数**，来调用UI层逻辑，实现逻辑和UI独立化，减少了其之间的耦合

```ts
  /**
   * @description 设置响应式
   * @memberof ViewComponentBase
   */
  public setup() {
    this.viewIsLoaded = toRef(this.c, 'viewIsLoaded');
    this.initReactive();
    this.emitViewEvent = this.emitViewEvent.bind(this);
    this.c.hooks.event.tap(this.emitViewEvent);
    this.initViewNavParam = this.initViewNavParam.bind(this);
    this.c.hooks.modelLoaded.tap(this.initViewNavParam);
    this.viewClose = this.viewClose.bind(this);
    this.c.hooks.closeView.tap(this.viewClose);
    this.viewMounted = this.viewMounted.bind(this);
    this.c.hooks.mounted.tap(this.viewMounted);
  }
```

例如：部件通知视图需要关闭视图，这时候可以直接调用控制器的closeView钩子函数，触发UI的关闭视图

```ts
  /**
   * @description 关闭视图
   * @param {*} [args] 关闭参数
   * @memberof AppViewControllerBase
   */
  public closeView(args?: any) {
    this.hooks.closeView.callSync({ arg: args });
  }
```



| 钩子名称      | 说明                 | 参数             | 调用位置                                   |
| ----------- | -------------------- | ---------------- | ------------------------------------------ |
| modelLoaded | 模型数据加载完成钩子 | arg:视图模型对象 | [视图初始化](#视图初始化)                  |
| event       | 视图事件钩子         | arg:视图事件对象 | [视图事件处理](de/de-view.md#视图事件处理) |
| closeView   | 视图关闭钩子         | arg:视图关闭参数 | [视图关闭](#视图关闭)                      |
| mounted     | 视图挂载之后钩子     | arg:当前视图     | [视图初始化](#视图挂载)                    |

