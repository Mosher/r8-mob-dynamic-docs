# 实体移动端自定义视图

实体自定义视图，是平台预留视图作为自定义界面展示的视图模板之一。自定义视图除标准的视图结构外，不会在视图中嵌入内容模板。除此之外，自定义视图不会在视图部件、视图引用、视图逻辑关系中添加内容，完全交由开发者自行书写。

<component-iframe router="/iframe/view/other/app-mob-custom-view" />

## 控制器

### 引擎初始化

引擎是R8Mob动态模板中视图的固有逻辑对象，用于处理视图的事件以及视图与部件的交互。

当实体移动端自定义视图(以下简称：视图)下的所有部件挂载完成后，视图执行挂载逻辑，此时初始化引擎。

引擎初始化完成后，通过视图状态订阅对象(ViewState)通知相应部件加载数据。

核心逻辑如下：

#### 初始化视图部件Map

初始化时首先填充视图部件Map 。

```ts
  /**
   * @description 初始化视图部件Map
   * @param {*} options
   * @memberof MobCustomViewEngine
   */
  public initViewControlMap(options: any) {
    if (options && options.length > 0) {
      options.forEach((element: any) => {
        this.viewCtrlMap.set(element.name, element.ctrl);
      });
    }
  }
```

#### 初始化部件引擎Map

填充视图部件Map后再填充视图部件Map，根据引擎类型去处理部件加载或处理搜索部件加载。

```ts
  /**
   * @description 初始化引擎Map
   * @param {*} options
   * @memberof MobCustomViewEngine
   */
  public initCtrlEngineArray(options: any) {
    if (options && options.length > 0) {
      this.ctrlEngineArray = [];
      options.forEach((element: any) => {
        const result = this.handleViewEngineParams(element);
        this.ctrlEngineArray.push(result);
      });
    }
  }

  /**
   * @description 处理视图引擎参数
   * @param {*} args 数据
   * @return {*}
   * @memberof MobCustomViewEngine
   */
  public handleViewEngineParams(args: any) {
    switch (args.engineType) {
      case 'CtrlLoadTrigger':
        return this.handleCtrlLoad(args.getPSUIEngineParams);
      case 'CtrlLoad':
        return this.handleCtrlLoad(args.getPSUIEngineParams);
      case 'CtrlLoadAndSearch':
        return this.CtrlLoadAndSearch(args.getPSUIEngineParams);
      default:
        LogUtil.warn(`${args.engineType}暂未支持`);
        break;
    }
  }
```

#### 引擎加载

在初始化完成上述两个Map后调用load方法

``` ts 
  /**
   * @description 引擎加载
   * @param {*} [opts={}]
   * @memberof MobCustomViewEngine
   */
  public load(opts: any = {}): void {
    // 处理搜索部件加载并搜索（参数可指定触发部件）
    if (this.ctrlEngineArray.length > 0) {
      for (const element of this.ctrlEngineArray) {
        if (element.triggerCtrlName && Object.is(element.triggerCtrlName, 'VIEW')) {
          if (element.triggerType && Object.is(element.triggerType, 'CtrlLoadAndSearch')) {
            this.setViewState2({ tag: element.targetCtrlName, action: 'loadDraft', viewdata: Util.deepCopy(opts) });
          }
        }
      }
    }
    // 处理部件加载（参数可指定触发部件）无指定触发部件时由容器触发
    if (this.ctrlEngineArray.length > 0) {
      for (const element of this.ctrlEngineArray) {
        if (element.triggerType && Object.is(element.triggerType, 'CtrlLoad') && !element.triggerCtrlName) {
          this.setViewState2({ tag: element.targetCtrlName, action: 'load', viewdata: Util.deepCopy(opts) });
        }
      }
    }
  }
```

由上述逻辑可知，自定义视图先处理搜索部件加载，再通知数据部件加载。

::: tip 提示
实体移动端自定义视图控制器继承主数据视图基类控制器，更多逻辑参见 [主数据视图 > 控制器](../de/de-view.md#控制器)
:::

## UI层

::: tip 提示
实体移动端自定义视图UI层继承主数据视图基类UI层，更多逻辑参见 [主数据视图 > UI层](../de/de-view.md#ui层)
:::

### 布局

#### 绘制视图主容器内容

重写视图主容器内容，只要插槽列表里里放的不是绘制顶部视图消息、底部视图消息、内容视图消息、快速搜索、快速分组、搜索表单、快速搜索表单等，就把该插槽绘制出来。

```ts
  /**
   * @description 视图主容器内容
   * @return {*}
   * @memberof AppDefaultMobCustomViewLayout
   */
  renderViewMainContainerContent(): any {
    const ctrlSlots: any[] = [];
    for (const slotName in this.ctx.slots) {
      if (
        !Object.is('topMessage', slotName) &&
        !Object.is('bottomMessage', slotName) &&
        !Object.is('bodyMessage', slotName) &&
        !Object.is('quickGroupSearch', slotName) &&
        !Object.is('quickSearch', slotName) &&
        !Object.is('searchform', slotName) &&
        !Object.is('quicksearchform', slotName)
      ) {
        ctrlSlots.push(renderSlot(this.ctx.slots, slotName));
      }
    }
    return <div class='view-main-container-content'>{ctrlSlots}</div>;
  }
```

#### 绘制视图主容器头部

重写视图主容器头部，绘制快速分组、快速搜索、搜索表单、快速搜索表单等插槽。

```ts
  /**
   *
   * @description 视图主容器头部
   * @return {*}
   * @memberof MultiDataViewLayoutComponentBase
   */
  renderViewMainContainerHeader(): any {
    return (
      <div class='view-main-container-header'>
        {[
          renderSlot(this.ctx.slots, 'quickGroupSearch'),
          renderSlot(this.ctx.slots, 'quickSearch'),
          renderSlot(this.ctx.slots, 'searchform'),
          renderSlot(this.ctx.slots, 'quicksearchform'),
        ]}
      </div>
    );
  }
```

::: tip 提示
该视图布局继承主数据视图基类布局，更多逻辑参见 [主数据视图 > UI层 > 布局](../de/de-view.md#布局)
:::