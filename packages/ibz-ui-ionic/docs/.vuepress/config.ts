import { defineUserConfig } from "vuepress";
import type {
  DefaultThemeOptions,
  WebpackBundlerOptions
} from "vuepress";
import { navbar, sidebar, webpackConfig } from "./configs";

export default defineUserConfig<DefaultThemeOptions, WebpackBundlerOptions>({
  base: "/",
  port: 8080,
  head: [],
  locales: {
    "/": {
      lang: "zh-CN",
      title: "R8移动端组件库",
      description: "基于Ionic组件库构建的R8移动端组件库",
    },
    // "/en/": {
    //   lang: "en-US",
    //   title: "R8Mob-Dynamic模板（Ionic组件库）",
    //   description:
    //     "VuePress builds Element's component library document tutorial sample code",
    // },
  },
  themeConfig: {
    locales: {
      '/': {
        selectLanguageName: '简体中文',
        selectLanguageText: '切换语言',
        sidebar: sidebar.zh,
        navbar: navbar.zh
      },
      // '/en/': {
      //   selectLanguageName: 'English',
      //   selectLanguageText: 'Languages',
      //   sidebar: sidebar.en,
      //   navbar: navbar.en
      // }
    }
  },
  plugins: [
    [
      "vuepress-plugin-typescript",
      {
        tsLoaderOptions: {
          appendTsSuffixTo: [/\.tsx$/, /\.md$/, /\.vue$/],
        },
      },
    ],
  ],
  markdown: {},
  bundlerConfig: {
    configureWebpack: webpackConfig
  }
});
