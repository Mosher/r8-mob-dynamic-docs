
/**
 * @description 预览数据类
 * @class PreviewData
 */
export class PreviewData {

  /**
   * @description 预览数据
   * @static
   * @type {any[]}
   * @memberof PreviewData
   */
  public static previewData: any[] =[
    {
      srfmajortext: "测试数据1",
      srfkey: "1",
      title: "测试数据1",
      content: "内容1",
      itemType: "bxd",
      color: "rgba(103, 238, 233, 1)",
      textColor: "rgba(135, 160, 235, 1)",
      customname: "名称1",
      customid: "33",
      nodeType:"ROOT",
      text: "测试数据1",    
      id:"ROOT",
      inputbox:'测试数据1',
      type:'类型1'
    },
    {
      srfmajortext: "测试数据2",
      srfkey: "2",
      title: "测试数据2",
      content: "内容2",
      itemType: "bxd",
      color: "rgba(103, 168, 238, 1)",
      textColor: "rgba(235, 190, 135, 1)",
      customname: "名称2",   
      customid: "44",       
      nodeType:"firstNode",
      text: "测试数据2",        
      id:"firstNode",
      inputbox:'测试数据2',
      type:'类型2'    
    },
    {
      srfmajortext: "测试数据3",
      srfkey: "3",
      title: "测试数据3",
      content: "内容3",
      itemType: "bxd",
      color: "rgba(156, 188, 222, 1)",
      textColor: "rgba(186, 239, 145, 1)",
      customname: "名称3",  
      customid: "55",   
      nodeType:"secondNode",
      text: "测试数据3",   
      id:"secondNode",
      inputbox:'测试数据3',
      type:'类型3'             
    },
    {
      srfmajortext: "测试数据4",
      srfkey: "4",
      title: "测试数据4",
      content: "内容4",
      itemType: "bxd",
      color: "rgba(223, 176, 242, 1)",
      textColor: "rgba(113, 142, 228, 1)",
      customname: "名称4",  
      customid: "66",    
      nodeType:"threeNode",
      text: "测试数据4",  
      id:"threeNode",
      inputbox:'测试数据4',
      type:'类型4'              
    },
  ];

  /**
   * @description 获取预览数据
   * @static
   * @return {*} 
   * @memberof PreviewData
   */
  public static getPreviewData() {
    return this.previewData;
  }
}