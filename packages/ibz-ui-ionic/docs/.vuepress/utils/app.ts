import { IApp, AppBase, IComponentService, ICodeListService, IViewOpenService, IParam } from 'ibz-core';
import { AppComponentService, AppViewOpenService } from 'ibz-ui-ionic';
import { CodeListService } from './codeList-service';
import { LocalComponentRegister } from '../components/register';
import i18n from '../locale/index';

export class App extends AppBase implements IApp {
  private static instance: App = new App();

  public static getInstance() {
    if (!App.instance) {
      App.instance = new App();
    }
    return App.instance;
  }

  public getComponentService(): IComponentService {
    return AppComponentService.getInstance();
  }

  public getCodeListService(): ICodeListService {
    return new CodeListService();
  }
  

  public getOpenViewService(): IViewOpenService {
    return AppViewOpenService.getInstance();
  }


  public getUserRegister(): IParam {
    return LocalComponentRegister;
  }

  public getI18n(): IParam {
    return i18n;
  }

  isPreviewMode(): boolean {
    return true;
  }
}
