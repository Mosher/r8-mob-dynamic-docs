
import { ICodeListService, LogUtil } from 'ibz-core';

/**
 * 代码表服务基类
 *
 * @export
 * @class CodeListService
 */
export class CodeListService implements ICodeListService {
    /**
     * @description 获取代码表数据
     * @param {{ tag: string, type: string, data?: Array<any>, navContext?: any, navViewParam?: any }} params
     * @return {*} 
     * @memberof CodeListService
     */
    public async getDataItems(params: { tag: string, type: string, data?: Array<any>, navContext?: any, navViewParam?: any }) {
      let dataItems: any;
      try {
        dataItems = [ {
          "codeName" : "Toys",
          "color" : "rgba(32, 201, 23, 1)",
          "data" : "{ \"n_customtype_like\": \"toys\" }",
          "iconCls" : "bookmark",
          "getPSSysImage" : {
            "cssClass" : "bookmark"
          },
          "text" : "玩具",
          "label" : "玩具",
          "value" : "toys",
          "includeBeginValue" : true
        }, {
          "codeName" : "Food",
          "color" : "rgba(228, 33, 33, 1)",
          "data" : "{ \"n_customtype_like\": \"food\" }",
          "iconCls" : "call",
          "getPSSysImage" : {
            "cssClass" : "call"
          },
          "text" : "食品",
          "label" : "食品",
          "value" : "food",
          "includeBeginValue" : true
        } ]
      } catch ( error ) {
        LogUtil.warn("代码表加载异常" + error);
      }
        return dataItems;
    }
}