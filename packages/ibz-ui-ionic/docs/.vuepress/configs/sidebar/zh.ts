import type { SidebarConfig } from '@vuepress/theme-default';

export const zh: SidebarConfig = {
  '/guide/': [
    {
      text: '指南',
      children: [
        '/guide/README.md',
        {
          text: '应用',
          children: [
            '/guide/theme.md',
            '/guide/locale.md'
          ]
        },
        '/guide/view.md',
        '/guide/widget.md',
        '/guide/editor.md',
        '/guide/directive.md',
        '/guide/uiService.md',
        '/guide/plugin.md',
        '/guide/ui-action.md',
      ]
    }
  ],
  '/view/': [
    {
      text: '视图',
      children: [
        {
          text: '视图基类',
          link: '/view/app-view-base.md'
        },
        {
          text: '应用视图',
          children: [
            '/view/app/app-index-view.md',
            '/view/app/app-portal-view.md'
          ]
        },
        {
          text: '主数据视图',
          link: '/view/de/de-view.md',
          children: [
            '/view/de/app-mob-edit-view.md',
            '/view/de/app-mob-dashboard-view.md',
            '/view/de/app-mob-tabexp-view.md',
            '/view/de/app-mob-html-view.md',
            '/view/de/app-mob-wizard-view.md',
            '/view/de/app-mob-panel-view.md',
            '/view/de/app-mob-opt-view.md',
          ]
        },
        {
          text: '多数据视图',
          link: '/view/demultidata/de-multi-data-view.md',
          children: [
            '/view/demultidata/app-mob-md-view.md',
            '/view/demultidata/app-mob-chart-view.md',
            '/view/demultidata/app-mob-calendar-view.md',
            '/view/demultidata/app-mob-map-view.md',
            '/view/demultidata/app-mob-tree-view.md',
            '/view/demultidata/app-mob-medit-view.md',
          ]
        },
        {
          text: '选择视图',
          children: [
            '/view/pickup/app-mob-pickup-view.md',
            '/view/pickup/app-mob-mpickup-view.md',
            '/view/pickup/app-mob-pickup-mdview.md',
            '/view/pickup/app-mob-pickup-treeview.md',
          ]
        },
        {
          text: '导航视图',
          link: '/view/deexp/de-exp-view.md',
          children: [
            '/view/deexp/app-mob-list-exp-view.md',
            '/view/deexp/app-mob-chart-exp-view.md',
            '/view/deexp/app-mob-map-exp-view.md',
            '/view/deexp/app-mob-tree-exp-view.md',
            '/view/deexp/app-mob-calendar-exp-view.md'
          ]
        },
        {
          text: '工作流视图',
          children: [
            '/view/wf/app-mob-wfdynaedit-view.md',
            '/view/wf/app-mob-wfdynaaction-view'
          ]
        },
        {
          text: '其他视图',
          children: [
            '/view/other/app-mob-custom-view.md'
          ]
        }
      ]
    }
  ],
  '/widget/': [
    {
      text: '部件',
      children: [
        {
          text: '部件基类',
          link: "/widget/app-ctrl-base.md"
        },
        {
          text: '应用类部件',
          children: [
            '/widget/app/app-mob-dashboard.md',
            '/widget/app/app-mob-portlet.md',
            '/widget/app/app-mob-menu.md',
          ]
        },
        {
          text: '表单类部件',
          children: [
            '/widget/form/app-mob-form.md',
            '/widget/form/app-mob-searchform.md',
          ]
        },
        {
          text: '导航栏部件',
          children: [
            '/widget/exp/exp-ctrl-base.md',
            '/widget/exp/app-mob-chart-exp-bar.md',
            '/widget/exp/app-mob-map-exp-bar.md',
            '/widget/exp/app-mob-list-exp-bar.md',
            '/widget/exp/app-mob-tree-exp-bar.md',
            '/widget/exp/app-mob-calendar-exp-bar.md',
          ]
        },
        {
          text: '多数据部件',
          children: [
            '/widget/md/md-ctrl-base.md',
            '/widget/md/app-mob-mdctrl.md',
            '/widget/md/app-mob-chart.md',
            '/widget/md/app-mob-calendar.md',
            '/widget/md/app-mob-map.md',
            '/widget/md/app-mob-tree.md',
          ]
        },
        {
          text: '面板类部件',
          children: [
            '/widget/panel/app-mob-tabexppanel.md',
            '/widget/panel/app-mob-tabviewpanel.md',
            '/widget/panel/app-mob-pickupviewpanel.md',
            '/widget/panel/app-mob-wizard-panel.md',
            '/widget/panel/app-mob-state-wizard-panel.md',
            '/widget/panel/app-mob-panel.md',
            '/widget/panel/app-mob-meditviewpanel.md',
          ]
        },
        {
          text: '其他',
          children: [
            '/widget/other/app-mob-toolbar.md',
            '/widget/other/app-mob-contextmenu.md',
          ]
        },
      ]
    }
  ],
  '/editor/': [
    {
      text: '编辑器',
      children: [
        {
          text: '编辑器基类',
          link: '/editor/app-editor-base.md'
        },
        {
          text: '展示类',
          children: [
            '/editor/app-span.md',
          ]
        },
        {
          text: '基础类',
          children: [
            '/editor/app-input.md',
            '/editor/app-stepper.md',
            '/editor/app-switch.md',
            '/editor/app-textarea.md',
            '/editor/app-slider.md',
            '/editor/app-rating.md',
            '/editor/app-rich-text.md',
          ]
        },
        {
          text: '选择类',
          children: [
            '/editor/app-radio-list.md',
            '/editor/app-dropdown-list.md',
          ]
        },
        {
          text: '数据选择类',
          children: [
            '/editor/app-data-picker.md',
          ]
        },
        {
          text: '时间选择类',
          children: [
            '/editor/app-date-picker.md',
          ]
        },
        {
          text: '文件上传类',
          children: [
            '/editor/app-upload.md',
          ]
        },
      ]
    }
  ],
  '/plugin/': [
    {
      text: '插件',
      children: [
        '/plugin/viewPlugin.md',
        '/plugin/widgetPlugin.md',
        '/plugin/controlItemPlugin.md',
        '/plugin/editorPlugin.md',
        '/plugin/uiactionPlugin.md',
      ]
    }
  ],
  '/uiservice/': [
    {
      text: 'UI服务',
      link: '/guide/uiService.md',
      children: [
        '/uiservice/app-action-sheet-service.md',
        '/uiservice/app-auth-service.md',
        '/uiservice/app-center-service.md',
        '/uiservice/app-component-service.md',
        '/uiservice/app-dashboard-design-service.md',
        '/uiservice/app-modal-service.md',
        '/uiservice/app-drawer-service.md',
        '/uiservice/app-loading-service.md',
        '/uiservice/app-msgbox-service.md',
        '/uiservice/app-notice-service.md',
        '/uiservice/app-theme-service.md',
        '/uiservice/app-third-party-service.md',
        '/uiservice/app-view-open-service.md',
      ]
    }
  ],
  '/directive/': [
    {
      text: '指令',
      link: '/guide/directive.md',
      children: [
        '/directive/badge.md',
        '/directive/format.md',
      ]
    }
  ],
  '/ui-action/': [
    {
      text: '界面行为',
      children: [
        '/ui-action/front-action.md',
        '/ui-action/backend-action.md',
        '/ui-action/sys-action.md',
        '/ui-action/ui-logic.md',
      ]
    }
  ],
}