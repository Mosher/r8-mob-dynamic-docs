import type { SidebarConfig } from '@vuepress/theme-default';

//  待补充en
export const en: SidebarConfig = {
  '/en/guide/': [
    {
      text: '指南',
      children: [
        '/en/guide/README.md',
        {
          text: '应用',
          children: [
            '/en/guide/theme.md',
            '/en/guide/locale.md'
          ]
        },
        '/en/guide/view.md',
        '/en/guide/widget.md',
        '/en/guide/editor.md',
        '/en/guide/plugin.md',
        '/en/guide/uiService.md'
      ]
    }
  ],
  '/en/view/': [
    {
      text: '视图',
      children: [
        {
          text: '视图基类',
          link: '/en/view/app-view-base.md'
        },
        {
          text: '应用视图',
          children: [
            '/en/view/app/app-index-view.md',
            '/en/view/app/app-portal-view.md'
          ]
        },
        {
          text: '主数据视图',
          link: '/en/view/de/de-view.md',
          children: [
            '/en/view/de/app-mob-edit-view.md',
            '/en/view/de/app-mob-dashboard-view.md',
            '/en/view/de/app-mob-tabexp-view.md',
            '/en/view/de/app-mob-html-view.md',
            '/en/view/de/app-mob-wizard-view.md',
            '/en/view/de/app-mob-panel-view.md',
            '/en/view/de/app-mob-opt-view.md',
          ]
        },
        {
          text: '多数据视图',
          link: '/en/view/demultidata/de-multi-data-view.md',
          children: [
            '/en/view/demultidata/app-mob-md-view.md',
            '/en/view/demultidata/app-mob-chart-view.md',
            '/en/view/demultidata/app-mob-calendar-view.md',
            '/en/view/demultidata/app-mob-map-view.md',
            '/en/view/demultidata/app-mob-tree-view.md',
            '/en/view/demultidata/app-mob-medit-view.md',
          ]
        },
        {
          text: '选择视图',
          children: [
            '/en/view/pickup/app-mob-pickup-view.md',
            '/en/view/pickup/app-mob-mpickup-view.md',
            '/en/view/pickup/app-mob-pickup-mdview.md',
            '/en/view/pickup/app-mob-pickup-treeview.md',
          ]
        },
        {
          text: '导航视图',
          link: '/en/view/deexp/de-exp-view.md',
          children: [
            '/en/view/deexp/app-mob-list-exp-view.md',
            '/en/view/deexp/app-mob-chart-exp-view.md',
            '/en/view/deexp/app-mob-map-exp-view.md',
            '/en/view/deexp/app-mob-tree-exp-view.md',
            '/en/view/deexp/app-mob-calendar-exp-view.md'
          ]
        },
        {
          text: '工作流视图',
          children: [
            '/view/wf/app-mob-wfdynaedit-view.md',
            '/view/wf/app-mob-wfdynaaction-view.md'
          ]
        },
        {
          text: '其他视图',
          children: [
            '/en/view/other/app-mob-custom-view.md'
          ]
        }
      ]
    }
  ],
  '/en/widget/': [
    {
      text: '部件',
      children: [
        {
          text: '应用类部件',
          children: [
            '/en/widget/app/app-mob-dashboard.md',
            '/en/widget/app/app-mob-portlet.md',
            '/en/widget/app/app-mob-menu.md',
          ]
        },
        {
          text: '表单类部件',
          children: [
            '/en/widget/app-mob-form.md',
            '/en/widget/form/app-mob-searchform.md',
          ]
        },
        {
          text: '导航栏部件',
          children: [
            '/en/widget/exp/exp-ctrl-base.md',
            '/en/widget/exp/app-mob-chart-exp-bar.md',
            '/en/widget/exp/app-mob-map-exp-bar.md',
            '/en/widget/exp/app-mob-calendar-exp-bar.md',
            '/en/widget/exp/app-mob-list-exp-bar.md',
            '/en/widget/exp/app-mob-tree-exp-bar.md',
            '/en/widget/exp/app-mob-calendar-exp-bar.md',
          ]
        },
        {
          text: '多数据部件',
          children: [
            '/en/widget/md/md-ctrl-base.md',
            '/en/widget/md/app-mob-mdctrl.md',
            '/en/widget/md/app-mob-chart.md',
            '/en/widget/md/app-mob-calendar.md',
            '/en/widget/md/app-mob-map.md',
            '/en/widget/md/app-mob-tree.md',
          ]
        },
        {
          text: '面板类部件',
          children: [
            '/en/widget/panel/app-mob-tabexppanel.md',
            '/en/widget/panel/app-mob-tabviewpanel.md',
            '/en/widget/panel/app-mob-pickupviewpanel.md',
            '/en/widget/panel/app-mob-wizard-panel.md',
            '/en/widget/panel/app-mob-state-wizard-panel.md',
            '/en/widget/panel/app-mob-panel.md',
            '/en/widget/panel/app-mob-meditviewpanel.md',
          ]
        },
        {
          text: '其他',
          children: [
            '/en/widget/other/app-mob-toolbar.md',
            '/en/widget/md/app-mob-contextmenu.md',
          ]
        },
      ]
    }
  ],
  '/en/editor/': [
    {
      text: '编辑器',
      children: [
        {
          text: '编辑器基类',
          link: '/en/editor/app-editor-base.md'
        },
        {
          text: '展示类',
          children: [
            '/en/editor/app-span.md',
          ]
        },
        {
          text: '基础类',
          children: [
            '/en/editor/app-input.md',
            '/en/editor/app-stepper.md',
            '/en/editor/app-switch.md',
            '/en/editor/app-textarea.md',
            '/en/editor/app-slider.md',
            '/en/editor/app-rating.md',
            '/en/editor/app-rich-text.md',
          ]
        },
        {
          text: '选择类',
          children: [
            '/en/editor/app-radio-list.md',
            '/en/editor/app-dropdown-list.md',
          ]
        },
        {
          text: '数据选择类',
          children: [
            '/en/editor/app-data-picker.md',
          ]
        },
        {
          text: '时间选择类',
          children: [
            '/en/editor/app-date-picker.md',
          ]
        },
        {
          text: '文件上传类',
          children: [
            '/en/editor/app-upload.md',
          ]
        },
      ]
    }
  ],
  '/en/plugin/': [
    {
      text: '插件',
      children: [
        '/en/plugin/viewPlugin.md',
        '/en/plugin/widgetPlugin.md',
        '/en/plugin/controlItemPlugin.md',
        '/en/plugin/editorPlugin.md',
        '/en/plugin/uiactionPlugin.md',
      ]
    }
  ],
  '/en/uiservice/': [
    {
      text: 'UI服务',
      link: '/en/guide/uiService.md',
      children: [
        '/en/uiservice/app-action-sheet-service.md',
        '/en/uiservice/app-auth-service.md',
        '/en/uiservice/app-center-service.md',
        '/en/uiservice/app-component-service.md',
        '/en/uiservice/app-dashboard-design-service.md',
        '/en/uiservice/app-modal-service.md',
        '/en/uiservice/app-drawer-service.md',
        '/en/uiservice/app-loading-service.md',
        '/en/uiservice/app-msgbox-service.md',
        '/en/uiservice/app-notice-service.md',
        '/en/uiservice/app-theme-service.md',
        '/en/uiservice/app-third-party-service.md',
        '/en/uiservice/app-view-open-service.md',
      ]
    }
  ],
  '/en/directive/': [
    {
      text: '指令',
      link: '/en/guide/directive.md',
      children: [
        '/en/directive/badge.md',
        '/en/directive/format.md',
      ]
    }
  ],
  '/ui-action/': [
    {
      text: '界面行为',
      children: [
        '/ui-action/front-action.md',
        '/ui-action/backend-action.md',
        '/ui-action/sys-action.md',
        '/ui-action/ui-logic.md',
      ]
    }
  ],
}