import type { NavbarConfig } from '@vuepress/theme-default';

export const zh: NavbarConfig = [
  {
    text: '首页',
    link: '/'
  },
  {
    text: '快捷',
    children: [
      {
        text: '简介',
        link: '/guide/README.md'
      },
      {
        text: '应用',
        link: '/guide/theme.md'
      },
      {
        text: '视图',
        link: '/guide/view.md'
      },
      {
        text: '部件',
        link: '/guide/widget.md'
      },
      {
        text: '编辑器',
        link: '/guide/editor.md'
      },
      {
        text: '指令',
        link: '/guide/directive.md'
      },
      {
        text: 'UI服务',
        link: '/guide/uiService.md'
      },
      {
        text: '插件',
        link: '/guide/plugin.md'  
      }
    ]
  },
  {
    text: '了解更多',
    children: [
      {
        text: 'Ibiz',
        link: 'http://www.ibizsys.cn'
      }
    ]
  }
]