import type { NavbarConfig } from '@vuepress/theme-default';

export const en: NavbarConfig = [
  {
    text: '首页',
    link: '/en/'
  },
  {
    text: '快捷',
    children: [
      {
        text: '简介',
        link: 'en/guide/README.md'
      },
      {
        text: '应用',
        link: '/en/guide/theme.md'
      },
      {
        text: '视图',
        link: '/en/guide/view.md'
      },
      {
        text: '部件',
        link: '/en/guide/widget.md'
      },
      {
        text: '编辑器',
        link: '/en/guide/editor.md'
      },
      {
        text: '指令',
        link: '/en/guide/directive.md'
      },
      {
        text: 'UI服务',
        link: '/en/guide/uiService.md'
      },
      {
        text: '插件',
        link: '/en/guide/plugin.md'  
      }
    ]
  },
  {
    text: '了解更多',
    children: [
      {
        text: 'Ibiz',
        link: 'http://www.ibizsys.cn'
      }
    ]
  }
]