import { WebpackConfiguration } from "vuepress";

function getWebpackConfig() {
  const config: WebpackConfiguration = {
    module: {
      rules: [
        {
          test: [/\.tsx?$/, /\.ts?$/],
          exclude: /node_modules/,
          use: [
            {
              loader: 'babel-loader',
              options: {
                presets: [
                  '@vue/cli-plugin-babel/preset'
                ],
              },
            },
            {
              loader: 'ts-loader',
              options: {
                transpileOnly: true,
                appendTsxSuffixTo: [/\.vue$/],
              },
            },
          ],
        }
      ]
    }
  }
  return config;
}

export const webpackConfig = getWebpackConfig;