// 用户自定义语言资源 中文
export const app_zh_CN = () => {
  const zh_CN: any = {
    404: {
      previous: '上一步',
      homepage: '首页',
    },
    500: {
      previous: '上一步',
      homepage: '首页',
    },
    login: {
      login: '登录',
      username: '用户名：',
      password: '密码：',
      platform: '搭载平台',
      error: '账户密码错误！'
    },
    setting: {
      setting: '系统设置',
      language: '选择语言',
      theme: '选择主题',
      systeminforms: '系统通知',
      notice: '通知',
      message: '消息'
    },
    theme: {
      preset: {
        default: '默认',
        dark: '灰暗'
      },
      inuse: '使用中',
      use: '使用',
      custom: '自定义',
      shadow: '阴影',
      tint: '浅色',
      reset: '重置',
      share: '分享',
      default: '默认',
      color: {
        base: '基础色',
        primary: '主要色',
        second: '次要色',
        third: '三次色',
        success: '成功',
        warning: '警告',
        danger: '危险',
        dark: '暗色',
        medium: '中间色',
        light: '亮色',
        background: '背景色',
        backgroundfocus: '背景色（聚焦）',
        font: '颜色',
        selected: '选中色'
      },
      tabbar: '分页栏',
      toolbar: '工具栏'
    },
  };
  return zh_CN;
};
