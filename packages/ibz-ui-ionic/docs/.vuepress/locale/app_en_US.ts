// 用户自定义语言资源 英文
export const app_en_US = () => {
  const en_US: any = {
    404: {
      previous: "Previous",
      homepage: "Homepage",
    },
    500: {
      previous: "Previous",
      homepage: "Homepage",
    },
    login: {
      login: "Login",
      username: "Username: ",
      password: "Password: ",
      platform: "Carrying platform",
      error: 'Wrong account password!'
    },
    setting: {
      setting: "System settings",
      language: "Choose language",
      theme: "Choose theme",
      systeminforms: 'The system informs',
      notice: 'Notice',
      message: 'Message'
    },
    theme: {
      preset: {
        default: 'Default',
        dark: 'Dark'
      },
      inuse: 'Inuse',
      use: 'Use',
      custom: 'Custom',
      shadow: 'Shadow',
      tint: 'Tint',
      reset: 'Reset',
      share: 'Share',
      default: 'Default',
      color: {
        base: 'Base color',
        primary: 'Primary ',
        second: 'Secondary color',
        third: 'Tertiary color',
        success: 'Success',
        warning: 'Warning',
        danger: 'Danger',
        dark: 'Dark',
        medium: 'Medium',
        light: 'Light',
        background: 'Background color',
        backgroundfocus: 'Background color (Focus)',
        font: 'Color',
        selected: 'Selected color'
      },
      tabbar: "Paging toolbar",
      toolbar: "Toolbar",
    },
  };
  return en_US;
};
