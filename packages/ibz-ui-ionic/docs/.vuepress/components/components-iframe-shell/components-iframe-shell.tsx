import { defineComponent, createApp } from 'vue';
import { LocalComponentRegister } from '../register';
import './components-iframe-shell.scss'

const ComponentIframeShellProps = {
  router: {
    type: String,
  },
};
export const ComponentIframeShell = defineComponent({
  name: 'ComponentIframeShell',
  props: ComponentIframeShellProps,
  setup(props: any, ctx: any) {
  },
  methods: {
  },
  mounted() {
    (window as any).toChildValue = () => {
      return {
        vueInstance: this,
        defineComponent,
        createApp,
        LocalComponentRegister
      };
    }
  },
  render() {
    return <div class="iframe-shell">
      <iframe class="iframe-html" src={this.$withBase(this.router+'.html')}></iframe>
    </div>
  }
})