import { ComponentDemoShell } from "./component-demo-shell/component-demo-shell";
import { ComponentIframeLayout } from "./component-iframe-layout/component-iframe-layout";
import { ComponentShell } from "./component-shell";
import { ComponentIframeShell } from "./components-iframe-shell/components-iframe-shell";

export const LocalComponentRegister = {
  install(vue: any) {
    vue.component('component-shell', ComponentShell);
    vue.component('component-demo', ComponentDemoShell);
    vue.component('component-iframe', ComponentIframeShell);
    vue.component('component-iframe-layout', ComponentIframeLayout);
  }
}