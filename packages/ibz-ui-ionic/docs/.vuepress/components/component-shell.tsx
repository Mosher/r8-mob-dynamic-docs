import { IPSAppView, IPSControl, IPSEditor } from '@ibiz/dynamic-model-api';
import { defineComponent, ref, h, onMounted, toRaw } from 'vue';
import { ModelServiceUtil, App } from '../utils';

const ComponentShellProps = {
  path: {
    type: String,
  },
  type: {
    type: String,
    default: 'VIEW',
  },
  className: {
    type: String,
  },
  width: {
    type: Number,
  },
  height: {
    type: Number,
  }
};
export const ComponentShell = defineComponent({
  name: 'ComponentShell',
  props: ComponentShellProps,
  setup(props: any, ctx: any) {
    const modelData = ref({});
    const isInit = ref(false);
    const resize = () => {
      const width = window.innerWidth; // 屏幕宽度
      let currentFontSize: number = 50;
      if (width <= 1920 && width > 1366) {
      } else if (width <= 1366 && width > 960) {
        currentFontSize = 36;
      } else if (width <= 960 && width > 768) {
        currentFontSize = 25;
      } else if (width <= 768) {
        currentFontSize = 16;
      }
      document.documentElement.style.fontSize = currentFontSize + 'px';
    }
    onMounted(() => {
      const { path, type } = props;
      const service = ModelServiceUtil.getInstance();
      service.getModelInstance(path, type).then(({ model, instance}) => {
        modelData.value = instance;
        isInit.value = true;
      })
      if (window && !window.onresize) {
        window.onresize = () => {
          resize();
        }
        resize();
      }
    })
    const items = {
      label: 'name',
      children: 'zones',
      isLeaf: 'leaf',
    }
    return {
      modelData,
      isInit,
      items
    }
  },
  methods: {
    getComponent() {
      if (this.modelData) {
        switch (this.type) {
          case 'VIEW':
            const view = this.modelData as IPSAppView;
            const viewComponent = App.getInstance().getComponentService().getViewTypeComponent(
              view?.viewType,
              view?.viewStyle,
              view?.getPSSysPFPlugin?.()?.pluginCode
            )
            return viewComponent;
          case 'CONTROL':
            const control = this.modelData as IPSControl;
            const ctrlComponent = App.getInstance().getComponentService().getControlComponent(
              control?.controlType,
              control?.controlStyle,
              control.getPSSysPFPlugin?.()?.pluginCode
            );
            return ctrlComponent;
          case 'EDITOR':
            const editor = this.modelData as IPSEditor;
            const editorCompoent = App.getInstance().getComponentService().getEditorComponent(
              editor.editorType,
              editor.editorStyle
            )
            return editorCompoent;
        }
      }
      return null;
    }
  },
  render() {
    const component = this.getComponent();
    const style: any = {
      width: this.width + 'px',
    }
    if (this.height) {
      Object.assign(style, { height: this.height + 'px' })
    }
    if (this.isInit && component) {
      return (
        <div class="component-shell" style={style}>
          {h(component, {
            ref: `${this.type}-${this.modelData.codeName}`,
            modelData: toRaw(this.modelData)
          })}
        </div>
      )
    }
  }
})