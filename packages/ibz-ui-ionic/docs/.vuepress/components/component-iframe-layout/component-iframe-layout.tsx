import { defineComponent, ref, h, onMounted, toRaw, createApp, Ref } from 'vue';
import { LocalComponentRegister } from '../register'

const ComponentIframeLayoutProps = {
  
};
export const ComponentIframeLayout = defineComponent({
  name: 'ComponentIframeLayout',
  props: ComponentIframeLayoutProps,
  components: {
  },
  setup(props: any, ctx: any) {
  },
  methods: {
    resize(){
      const designWidth = 750; // 移动端设计稿标准宽度
      const baseFontSize = 100; // 设计像素参考标准
      const width = window.innerWidth; // 屏幕宽度
      const currentFontSize = (width / designWidth) * baseFontSize;
      document.documentElement.style.fontSize = currentFontSize + 'px';
    }
  },
  mounted() {
    (window as any).toChildValue = () => {
      return {
        vueInstance: this,
        defineComponent,
        createApp,
        LocalComponentRegister
      };
    }
    // 当屏幕缩小时会触发该事件
    window.onresize = () => {
      this.resize();
    };
    this.resize();
  },
  render() {
    return <Content />
  }
})