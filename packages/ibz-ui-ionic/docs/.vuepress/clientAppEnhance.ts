import { defineClientAppEnhance } from '@vuepress/client'
import { UIInstall } from 'ibz-core';
import { LocalComponentRegister } from './components/register';
import { ComponentRegister, translate } from 'ibz-ui-ionic';
import { App } from './utils';
import { IonicVue } from '@ionic/vue';
import i18n from './locale';
import '@ionic/vue/css/core.css';
// import 'ibz-ui-ionic/lib/ibz-ui-ionic.css';
import "ibz-ui-ionic/src/styles/index.scss";

export default defineClientAppEnhance(({ app, router }) => {
  app.use(LocalComponentRegister)
    .use(IonicVue)
    .use(ComponentRegister)
    .use(i18n)
    .use(router);
  app.config.globalProperties.$tl = function (key: string, value?: string){
    return translate(key,this,value);
  };
  UIInstall(App.getInstance());
})