// 基于ionic组件库搭建的上层应用组件库说明
{
  "name": "ibz-ui-ionic",
  "version": "5.0.0",
  "description": "ibz UI ionic 组件库",
  "main": "lib/cjs/index.js",
  "module": "lib/es/index.js",
  "types": "lib/types/index.d.ts",
  "homepage": "https://www.ibizlab.cn/#/common_index/partner_curdevcenterdashboardview",
  "repository": {
    "type": "git",
    "url": "http://172.16.180.229/studio-ftl/r8_ionic_mob_ftl_res"
  },
  "files": [
    "lib"
  ],
  "author": "ibz",
  "license": "MIT",
  "scripts": {
    "build": "gulp",
    "lint:css": "stylelint **/*.{html,vue,css,sass,scss} --fix",
    "commit": "git add . && git-cz",
    "lint:app": "stylelint src && eslint src",
    "release": "node build/release",
    "gen:view": "node build/gen-code/gen-view",
    "gen:control": "node build/gen-code/gen-control",
    "gen:editor": "node build/gen-code/gen-editor"
  },
  "dependencies": {
    "@ibiz/dynamic-model-api": "^0.0.35",
    "ibz-core": "4.1.0",
    "dingtalk-jsapi": "^2.9.14",
    "weixin-js-sdk": "^1.6.0",
    "moment": "2.24.0",
    "qs": "^6.10.1",
    "vuedraggable": "^4.1.0"
  },
  "devDependencies": {
    "vue": "^3.2.20",
    "vue-router": "^4.0.11",
    "@ionic/vue": "^5.6.13",
    "@ionic/vue-router": "^5.6.13",
    "@commitlint/cli": "^13.2.0",
    "@commitlint/config-conventional": "^13.2.0",
    "@types/vuedraggable": "^2.24.0",
    "@typescript-eslint/eslint-plugin": "^4.29.0",
    "@typescript-eslint/parser": "^4.29.0",
    "@vue/cli-plugin-babel": "~4.5.13",
    "@vue/cli-plugin-eslint": "~4.5.13",
    "@vue/cli-plugin-typescript": "~4.5.13",
    "@vue/eslint-config-prettier": "^6.0.0",
    "@vue/eslint-config-typescript": "^7.0.0",
    "commitizen": "^4.0.3",
    "del": "^6.0.0",
    "eslint": "^7.32.0",
    "eslint-plugin-prettier": "^3.3.1",
    "eslint-plugin-vue": "^7.15.1",
    "gulp": "^4.0.2",
    "gulp-babel": "^8.0.0",
    "gulp-sass": "^4.0.2",
    "gulp-typescript": "^6.0.0-alpha.1",
    "gulp-autoprefixer": "^7.0.0",
    "gulp-clean-css": "^4.2.0",
    "gulp-rename": "^1.4.0",
    "husky": "^3.0.9",
    "inquirer": "6.5.2",
    "nunjucks": "^3.2.3",
    "prettier": "^2.2.1",
    "standard-version": "^9.3.1",
    "stylelint": "^13.13.1",
    "stylelint-config-recess-order": "^2.5.0",
    "stylelint-config-standard": "^22.0.0",
    "stylelint-order": "^4.1.0",
    "stylelint-scss": "^3.21.0",
    "through2": "^4.0.2",
    "typescript": "~4.3.5",
    "webpack": "^5.54.0",
    "webpack-stream": "^7.0.0"
  },
  "peerDependencies": {
    "@ionic/vue": "^5.6.13",
    "@ionic/vue-router": "^5.6.13",
    "vue": "^3.1.5",
    "vue-router": "^4.0.10"
  },
  "config": {
    "commitizen": {
      "path": "cz-conventional-changelog"
    }
  },
  "husky": {
    "hooks": {
      "commit-msg": "commitlint -e $GIT_PARAMS"
    }
  }
}
