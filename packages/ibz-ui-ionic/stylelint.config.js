module.exports = {
  defaultSeverity: 'error',
  extends: ['stylelint-config-standard', 'stylelint-config-recess-order'],
  plugins: ['stylelint-scss'],
  rules: {
    // 不要使用已被 autoprefixer 支持的浏览器前缀
    'media-feature-name-no-vendor-prefix': true,
    'at-rule-no-vendor-prefix': true,
    'selector-no-vendor-prefix': true,
    'property-no-vendor-prefix': true,
    'value-no-vendor-prefix': true,
    // 最多允许嵌套20层，去掉默认的最多2层
    'max-nesting-depth': 20,
    // 颜色值要小写
    'color-hex-case': 'lower',
    //允许rules前空一行
    'rule-empty-line-before': 'never',
    //自定义属性允许空行
    'custom-property-empty-line-before': 'never',
    // 颜色值能短则短
    'color-hex-length': 'short',
    //允许重复的选择器
    'no-duplicate-selectors': null,
    //允许选择器之后覆盖选择器的低特异性更高的特异性
    'no-descending-specificity': null,
    // 禁止空文件
    'no-empty-source': null,
    //允许使用自定义的字体关键字
    'font-family-no-missing-generic-family-keyword': null,
    // 排除某些scss@不识别报错
    'at-rule-no-unknown': [
      true,
      {
        ignoreAtRules: [
          'include',
          'extend',
          'at-root',
          'debug',
          'warn',
          'error',
          'if',
          'else',
          'for',
          'each',
          'while',
          'mixin',
          'include',
          'content',
          'return',
          'function',
        ],
      },
    ],
  },
};
