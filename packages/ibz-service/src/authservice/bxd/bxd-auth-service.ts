import { BXDAuthServiceBase } from './bxd-auth-service-base';


/**
 * 报销单权限服务对象
 *
 * @export
 * @class BXDAuthService
 * @extends {BXDAuthServiceBase}
 */
export default class BXDAuthService extends BXDAuthServiceBase {

    /**
     * @description 基础权限服务实例
     * @private
     * @static
     * @type {BXDAuthService}
     * @memberof BXDAuthService
     */
    private static basicUIServiceInstance: BXDAuthService;

    /**
     * @description 动态模型服务存储Map对象
     * @private
     * @static
     * @type {Map<string, any>}
     * @memberof BXDAuthService
     */
    private static AuthServiceMap: Map<string, any> = new Map();

    /**
     * Creates an instance of  BXDAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  BXDAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * @description 通过应用上下文获取实例对象
     * @static
     * @param {*} context
     * @return {*}  {BXDAuthService}
     * @memberof BXDAuthService
     */
    public static getInstance(context: any): BXDAuthService {
        if (!this.basicUIServiceInstance) {
            this.basicUIServiceInstance = new BXDAuthService({context:context});
        }
        if (!context.srfdynainstid) {
            return this.basicUIServiceInstance;
        } else {
            if (!BXDAuthService.AuthServiceMap.get(context.srfdynainstid)) {
                BXDAuthService.AuthServiceMap.set(context.srfdynainstid, new BXDAuthService({context:context}));
            }
            return BXDAuthService.AuthServiceMap.get(context.srfdynainstid);
        }
    }


}