import { BXDMXAuthServiceBase } from './bxdmx-auth-service-base';


/**
 * 报销单明细权限服务对象
 *
 * @export
 * @class BXDMXAuthService
 * @extends {BXDMXAuthServiceBase}
 */
export default class BXDMXAuthService extends BXDMXAuthServiceBase {

    /**
     * @description 基础权限服务实例
     * @private
     * @static
     * @type {BXDMXAuthService}
     * @memberof BXDMXAuthService
     */
    private static basicUIServiceInstance: BXDMXAuthService;

    /**
     * @description 动态模型服务存储Map对象
     * @private
     * @static
     * @type {Map<string, any>}
     * @memberof BXDMXAuthService
     */
    private static AuthServiceMap: Map<string, any> = new Map();

    /**
     * Creates an instance of  BXDMXAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  BXDMXAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * @description 通过应用上下文获取实例对象
     * @static
     * @param {*} context
     * @return {*}  {BXDMXAuthService}
     * @memberof BXDMXAuthService
     */
    public static getInstance(context: any): BXDMXAuthService {
        if (!this.basicUIServiceInstance) {
            this.basicUIServiceInstance = new BXDMXAuthService({context:context});
        }
        if (!context.srfdynainstid) {
            return this.basicUIServiceInstance;
        } else {
            if (!BXDMXAuthService.AuthServiceMap.get(context.srfdynainstid)) {
                BXDMXAuthService.AuthServiceMap.set(context.srfdynainstid, new BXDMXAuthService({context:context}));
            }
            return BXDMXAuthService.AuthServiceMap.get(context.srfdynainstid);
        }
    }


}