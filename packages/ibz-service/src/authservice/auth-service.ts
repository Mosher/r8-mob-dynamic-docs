import { AuthServiceBase } from '../service-base';

/**
 * 实体权限服务
 *
 * @export
 * @class AuthService
 */
export class AuthService extends AuthServiceBase {

    /**
     * @description 默认操作标识
     * @type {*}
     * @memberof AuthService
     */
    public defaultOPPrivs: any = {CREATE: 1,DELETE: 1,DENY: 1,NONE: 1,READ: 1,UPDATE: 1,WFSTART: 1}; 

    /**
     * Creates an instance of AuthService.
     * 
     * @param {*} [opts={}]
     * @memberof AuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * @description 注册系统操作标识统一资源
     * @memberof AuthService
     */
    public registerSysOPPrivs(){
    }

}