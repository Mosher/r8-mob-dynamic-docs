import { VirtualEntityMergeAuthServiceBase } from './virtual-entity-merge-auth-service-base';


/**
 * 虚拟实体合并权限服务对象
 *
 * @export
 * @class VirtualEntityMergeAuthService
 * @extends {VirtualEntityMergeAuthServiceBase}
 */
export default class VirtualEntityMergeAuthService extends VirtualEntityMergeAuthServiceBase {

    /**
     * @description 基础权限服务实例
     * @private
     * @static
     * @type {VirtualEntityMergeAuthService}
     * @memberof VirtualEntityMergeAuthService
     */
    private static basicUIServiceInstance: VirtualEntityMergeAuthService;

    /**
     * @description 动态模型服务存储Map对象
     * @private
     * @static
     * @type {Map<string, any>}
     * @memberof VirtualEntityMergeAuthService
     */
    private static AuthServiceMap: Map<string, any> = new Map();

    /**
     * Creates an instance of  VirtualEntityMergeAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  VirtualEntityMergeAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * @description 通过应用上下文获取实例对象
     * @static
     * @param {*} context
     * @return {*}  {VirtualEntityMergeAuthService}
     * @memberof VirtualEntityMergeAuthService
     */
    public static getInstance(context: any): VirtualEntityMergeAuthService {
        if (!this.basicUIServiceInstance) {
            this.basicUIServiceInstance = new VirtualEntityMergeAuthService({context:context});
        }
        if (!context.srfdynainstid) {
            return this.basicUIServiceInstance;
        } else {
            if (!VirtualEntityMergeAuthService.AuthServiceMap.get(context.srfdynainstid)) {
                VirtualEntityMergeAuthService.AuthServiceMap.set(context.srfdynainstid, new VirtualEntityMergeAuthService({context:context}));
            }
            return VirtualEntityMergeAuthService.AuthServiceMap.get(context.srfdynainstid);
        }
    }


}