import { VirtualEntity03AuthServiceBase } from './virtual-entity03-auth-service-base';


/**
 * 虚拟实体03权限服务对象
 *
 * @export
 * @class VirtualEntity03AuthService
 * @extends {VirtualEntity03AuthServiceBase}
 */
export default class VirtualEntity03AuthService extends VirtualEntity03AuthServiceBase {

    /**
     * @description 基础权限服务实例
     * @private
     * @static
     * @type {VirtualEntity03AuthService}
     * @memberof VirtualEntity03AuthService
     */
    private static basicUIServiceInstance: VirtualEntity03AuthService;

    /**
     * @description 动态模型服务存储Map对象
     * @private
     * @static
     * @type {Map<string, any>}
     * @memberof VirtualEntity03AuthService
     */
    private static AuthServiceMap: Map<string, any> = new Map();

    /**
     * Creates an instance of  VirtualEntity03AuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  VirtualEntity03AuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * @description 通过应用上下文获取实例对象
     * @static
     * @param {*} context
     * @return {*}  {VirtualEntity03AuthService}
     * @memberof VirtualEntity03AuthService
     */
    public static getInstance(context: any): VirtualEntity03AuthService {
        if (!this.basicUIServiceInstance) {
            this.basicUIServiceInstance = new VirtualEntity03AuthService({context:context});
        }
        if (!context.srfdynainstid) {
            return this.basicUIServiceInstance;
        } else {
            if (!VirtualEntity03AuthService.AuthServiceMap.get(context.srfdynainstid)) {
                VirtualEntity03AuthService.AuthServiceMap.set(context.srfdynainstid, new VirtualEntity03AuthService({context:context}));
            }
            return VirtualEntity03AuthService.AuthServiceMap.get(context.srfdynainstid);
        }
    }


}