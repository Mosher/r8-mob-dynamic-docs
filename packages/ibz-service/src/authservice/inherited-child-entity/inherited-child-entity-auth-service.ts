import { InheritedChildEntityAuthServiceBase } from './inherited-child-entity-auth-service-base';


/**
 * 继承实体(子)权限服务对象
 *
 * @export
 * @class InheritedChildEntityAuthService
 * @extends {InheritedChildEntityAuthServiceBase}
 */
export default class InheritedChildEntityAuthService extends InheritedChildEntityAuthServiceBase {

    /**
     * @description 基础权限服务实例
     * @private
     * @static
     * @type {InheritedChildEntityAuthService}
     * @memberof InheritedChildEntityAuthService
     */
    private static basicUIServiceInstance: InheritedChildEntityAuthService;

    /**
     * @description 动态模型服务存储Map对象
     * @private
     * @static
     * @type {Map<string, any>}
     * @memberof InheritedChildEntityAuthService
     */
    private static AuthServiceMap: Map<string, any> = new Map();

    /**
     * Creates an instance of  InheritedChildEntityAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  InheritedChildEntityAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * @description 通过应用上下文获取实例对象
     * @static
     * @param {*} context
     * @return {*}  {InheritedChildEntityAuthService}
     * @memberof InheritedChildEntityAuthService
     */
    public static getInstance(context: any): InheritedChildEntityAuthService {
        if (!this.basicUIServiceInstance) {
            this.basicUIServiceInstance = new InheritedChildEntityAuthService({context:context});
        }
        if (!context.srfdynainstid) {
            return this.basicUIServiceInstance;
        } else {
            if (!InheritedChildEntityAuthService.AuthServiceMap.get(context.srfdynainstid)) {
                InheritedChildEntityAuthService.AuthServiceMap.set(context.srfdynainstid, new InheritedChildEntityAuthService({context:context}));
            }
            return InheritedChildEntityAuthService.AuthServiceMap.get(context.srfdynainstid);
        }
    }


}