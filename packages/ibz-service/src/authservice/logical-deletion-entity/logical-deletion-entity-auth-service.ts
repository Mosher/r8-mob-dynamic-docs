import { LogicalDeletionEntityAuthServiceBase } from './logical-deletion-entity-auth-service-base';


/**
 * 逻辑删除实体权限服务对象
 *
 * @export
 * @class LogicalDeletionEntityAuthService
 * @extends {LogicalDeletionEntityAuthServiceBase}
 */
export default class LogicalDeletionEntityAuthService extends LogicalDeletionEntityAuthServiceBase {

    /**
     * @description 基础权限服务实例
     * @private
     * @static
     * @type {LogicalDeletionEntityAuthService}
     * @memberof LogicalDeletionEntityAuthService
     */
    private static basicUIServiceInstance: LogicalDeletionEntityAuthService;

    /**
     * @description 动态模型服务存储Map对象
     * @private
     * @static
     * @type {Map<string, any>}
     * @memberof LogicalDeletionEntityAuthService
     */
    private static AuthServiceMap: Map<string, any> = new Map();

    /**
     * Creates an instance of  LogicalDeletionEntityAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  LogicalDeletionEntityAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * @description 通过应用上下文获取实例对象
     * @static
     * @param {*} context
     * @return {*}  {LogicalDeletionEntityAuthService}
     * @memberof LogicalDeletionEntityAuthService
     */
    public static getInstance(context: any): LogicalDeletionEntityAuthService {
        if (!this.basicUIServiceInstance) {
            this.basicUIServiceInstance = new LogicalDeletionEntityAuthService({context:context});
        }
        if (!context.srfdynainstid) {
            return this.basicUIServiceInstance;
        } else {
            if (!LogicalDeletionEntityAuthService.AuthServiceMap.get(context.srfdynainstid)) {
                LogicalDeletionEntityAuthService.AuthServiceMap.set(context.srfdynainstid, new LogicalDeletionEntityAuthService({context:context}));
            }
            return LogicalDeletionEntityAuthService.AuthServiceMap.get(context.srfdynainstid);
        }
    }


}