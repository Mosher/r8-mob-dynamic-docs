import { VirtualEntity02AuthServiceBase } from './virtual-entity02-auth-service-base';


/**
 * 虚拟实体02权限服务对象
 *
 * @export
 * @class VirtualEntity02AuthService
 * @extends {VirtualEntity02AuthServiceBase}
 */
export default class VirtualEntity02AuthService extends VirtualEntity02AuthServiceBase {

    /**
     * @description 基础权限服务实例
     * @private
     * @static
     * @type {VirtualEntity02AuthService}
     * @memberof VirtualEntity02AuthService
     */
    private static basicUIServiceInstance: VirtualEntity02AuthService;

    /**
     * @description 动态模型服务存储Map对象
     * @private
     * @static
     * @type {Map<string, any>}
     * @memberof VirtualEntity02AuthService
     */
    private static AuthServiceMap: Map<string, any> = new Map();

    /**
     * Creates an instance of  VirtualEntity02AuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  VirtualEntity02AuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * @description 通过应用上下文获取实例对象
     * @static
     * @param {*} context
     * @return {*}  {VirtualEntity02AuthService}
     * @memberof VirtualEntity02AuthService
     */
    public static getInstance(context: any): VirtualEntity02AuthService {
        if (!this.basicUIServiceInstance) {
            this.basicUIServiceInstance = new VirtualEntity02AuthService({context:context});
        }
        if (!context.srfdynainstid) {
            return this.basicUIServiceInstance;
        } else {
            if (!VirtualEntity02AuthService.AuthServiceMap.get(context.srfdynainstid)) {
                VirtualEntity02AuthService.AuthServiceMap.set(context.srfdynainstid, new VirtualEntity02AuthService({context:context}));
            }
            return VirtualEntity02AuthService.AuthServiceMap.get(context.srfdynainstid);
        }
    }


}