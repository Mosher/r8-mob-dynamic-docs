import { BXDLBAuthServiceBase } from './bxdlb-auth-service-base';


/**
 * 报销单类别权限服务对象
 *
 * @export
 * @class BXDLBAuthService
 * @extends {BXDLBAuthServiceBase}
 */
export default class BXDLBAuthService extends BXDLBAuthServiceBase {

    /**
     * @description 基础权限服务实例
     * @private
     * @static
     * @type {BXDLBAuthService}
     * @memberof BXDLBAuthService
     */
    private static basicUIServiceInstance: BXDLBAuthService;

    /**
     * @description 动态模型服务存储Map对象
     * @private
     * @static
     * @type {Map<string, any>}
     * @memberof BXDLBAuthService
     */
    private static AuthServiceMap: Map<string, any> = new Map();

    /**
     * Creates an instance of  BXDLBAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  BXDLBAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * @description 通过应用上下文获取实例对象
     * @static
     * @param {*} context
     * @return {*}  {BXDLBAuthService}
     * @memberof BXDLBAuthService
     */
    public static getInstance(context: any): BXDLBAuthService {
        if (!this.basicUIServiceInstance) {
            this.basicUIServiceInstance = new BXDLBAuthService({context:context});
        }
        if (!context.srfdynainstid) {
            return this.basicUIServiceInstance;
        } else {
            if (!BXDLBAuthService.AuthServiceMap.get(context.srfdynainstid)) {
                BXDLBAuthService.AuthServiceMap.set(context.srfdynainstid, new BXDLBAuthService({context:context}));
            }
            return BXDLBAuthService.AuthServiceMap.get(context.srfdynainstid);
        }
    }


}