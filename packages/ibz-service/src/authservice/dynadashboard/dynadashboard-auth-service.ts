import { DYNADASHBOARDAuthServiceBase } from './dynadashboard-auth-service-base';


/**
 * 动态数据看板模型权限服务对象
 *
 * @export
 * @class DYNADASHBOARDAuthService
 * @extends {DYNADASHBOARDAuthServiceBase}
 */
export default class DYNADASHBOARDAuthService extends DYNADASHBOARDAuthServiceBase {

    /**
     * @description 基础权限服务实例
     * @private
     * @static
     * @type {DYNADASHBOARDAuthService}
     * @memberof DYNADASHBOARDAuthService
     */
    private static basicUIServiceInstance: DYNADASHBOARDAuthService;

    /**
     * @description 动态模型服务存储Map对象
     * @private
     * @static
     * @type {Map<string, any>}
     * @memberof DYNADASHBOARDAuthService
     */
    private static AuthServiceMap: Map<string, any> = new Map();

    /**
     * Creates an instance of  DYNADASHBOARDAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  DYNADASHBOARDAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * @description 通过应用上下文获取实例对象
     * @static
     * @param {*} context
     * @return {*}  {DYNADASHBOARDAuthService}
     * @memberof DYNADASHBOARDAuthService
     */
    public static getInstance(context: any): DYNADASHBOARDAuthService {
        if (!this.basicUIServiceInstance) {
            this.basicUIServiceInstance = new DYNADASHBOARDAuthService({context:context});
        }
        if (!context.srfdynainstid) {
            return this.basicUIServiceInstance;
        } else {
            if (!DYNADASHBOARDAuthService.AuthServiceMap.get(context.srfdynainstid)) {
                DYNADASHBOARDAuthService.AuthServiceMap.set(context.srfdynainstid, new DYNADASHBOARDAuthService({context:context}));
            }
            return DYNADASHBOARDAuthService.AuthServiceMap.get(context.srfdynainstid);
        }
    }


}