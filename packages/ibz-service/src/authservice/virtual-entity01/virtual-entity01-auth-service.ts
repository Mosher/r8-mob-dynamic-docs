import { VirtualEntity01AuthServiceBase } from './virtual-entity01-auth-service-base';


/**
 * 虚拟实体01权限服务对象
 *
 * @export
 * @class VirtualEntity01AuthService
 * @extends {VirtualEntity01AuthServiceBase}
 */
export default class VirtualEntity01AuthService extends VirtualEntity01AuthServiceBase {

    /**
     * @description 基础权限服务实例
     * @private
     * @static
     * @type {VirtualEntity01AuthService}
     * @memberof VirtualEntity01AuthService
     */
    private static basicUIServiceInstance: VirtualEntity01AuthService;

    /**
     * @description 动态模型服务存储Map对象
     * @private
     * @static
     * @type {Map<string, any>}
     * @memberof VirtualEntity01AuthService
     */
    private static AuthServiceMap: Map<string, any> = new Map();

    /**
     * Creates an instance of  VirtualEntity01AuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  VirtualEntity01AuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * @description 通过应用上下文获取实例对象
     * @static
     * @param {*} context
     * @return {*}  {VirtualEntity01AuthService}
     * @memberof VirtualEntity01AuthService
     */
    public static getInstance(context: any): VirtualEntity01AuthService {
        if (!this.basicUIServiceInstance) {
            this.basicUIServiceInstance = new VirtualEntity01AuthService({context:context});
        }
        if (!context.srfdynainstid) {
            return this.basicUIServiceInstance;
        } else {
            if (!VirtualEntity01AuthService.AuthServiceMap.get(context.srfdynainstid)) {
                VirtualEntity01AuthService.AuthServiceMap.set(context.srfdynainstid, new VirtualEntity01AuthService({context:context}));
            }
            return VirtualEntity01AuthService.AuthServiceMap.get(context.srfdynainstid);
        }
    }


}