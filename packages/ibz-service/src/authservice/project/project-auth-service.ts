import { ProjectAuthServiceBase } from './project-auth-service-base';


/**
 * 项目权限服务对象
 *
 * @export
 * @class ProjectAuthService
 * @extends {ProjectAuthServiceBase}
 */
export default class ProjectAuthService extends ProjectAuthServiceBase {

    /**
     * @description 基础权限服务实例
     * @private
     * @static
     * @type {ProjectAuthService}
     * @memberof ProjectAuthService
     */
    private static basicUIServiceInstance: ProjectAuthService;

    /**
     * @description 动态模型服务存储Map对象
     * @private
     * @static
     * @type {Map<string, any>}
     * @memberof ProjectAuthService
     */
    private static AuthServiceMap: Map<string, any> = new Map();

    /**
     * Creates an instance of  ProjectAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  ProjectAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * @description 通过应用上下文获取实例对象
     * @static
     * @param {*} context
     * @return {*}  {ProjectAuthService}
     * @memberof ProjectAuthService
     */
    public static getInstance(context: any): ProjectAuthService {
        if (!this.basicUIServiceInstance) {
            this.basicUIServiceInstance = new ProjectAuthService({context:context});
        }
        if (!context.srfdynainstid) {
            return this.basicUIServiceInstance;
        } else {
            if (!ProjectAuthService.AuthServiceMap.get(context.srfdynainstid)) {
                ProjectAuthService.AuthServiceMap.set(context.srfdynainstid, new ProjectAuthService({context:context}));
            }
            return ProjectAuthService.AuthServiceMap.get(context.srfdynainstid);
        }
    }


}