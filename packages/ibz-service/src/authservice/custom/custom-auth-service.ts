import { CUSTOMAuthServiceBase } from './custom-auth-service-base';


/**
 * 自定义实体权限服务对象
 *
 * @export
 * @class CUSTOMAuthService
 * @extends {CUSTOMAuthServiceBase}
 */
export default class CUSTOMAuthService extends CUSTOMAuthServiceBase {

    /**
     * @description 基础权限服务实例
     * @private
     * @static
     * @type {CUSTOMAuthService}
     * @memberof CUSTOMAuthService
     */
    private static basicUIServiceInstance: CUSTOMAuthService;

    /**
     * @description 动态模型服务存储Map对象
     * @private
     * @static
     * @type {Map<string, any>}
     * @memberof CUSTOMAuthService
     */
    private static AuthServiceMap: Map<string, any> = new Map();

    /**
     * Creates an instance of  CUSTOMAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  CUSTOMAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * @description 通过应用上下文获取实例对象
     * @static
     * @param {*} context
     * @return {*}  {CUSTOMAuthService}
     * @memberof CUSTOMAuthService
     */
    public static getInstance(context: any): CUSTOMAuthService {
        if (!this.basicUIServiceInstance) {
            this.basicUIServiceInstance = new CUSTOMAuthService({context:context});
        }
        if (!context.srfdynainstid) {
            return this.basicUIServiceInstance;
        } else {
            if (!CUSTOMAuthService.AuthServiceMap.get(context.srfdynainstid)) {
                CUSTOMAuthService.AuthServiceMap.set(context.srfdynainstid, new CUSTOMAuthService({context:context}));
            }
            return CUSTOMAuthService.AuthServiceMap.get(context.srfdynainstid);
        }
    }


}