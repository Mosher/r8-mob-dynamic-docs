import { AuthService } from '../auth-service';

/**
 * 继承实体权限服务对象基类
 *
 * @export
 * @class InheritedEntityAuthServiceBase
 * @extends {AuthService}
 */
export class InheritedEntityAuthServiceBase extends AuthService {

    /**
     * 
     *
     * @protected
     * @type {string}
     * @memberof InheritedEntityAuthServiceBase
     */
    /**
     * @description 应用实体动态模型文件路径
     * @protected
     * @type {string}
     * @memberof InheritedEntityAuthServiceBase
     */
    protected dynaModelFilePath:string = "PSSYSAPPS/TestMob/PSAPPDATAENTITIES/InheritedEntity.json";

    /**
     * Creates an instance of  InheritedEntityAuthServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof  InheritedEntityAuthServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * @description 根据当前数据获取实体操作标识
     * @param {string} activeKey 实体权限数据缓存标识
     * @param {string} dataaccaction 操作标识
     * @param {*} mainSateOPPrivs 传入数据主状态操作标识集合
     * @return {*}  {*}
     * @memberof InheritedEntityAuthServiceBase
     */
    public getOPPrivs(activeKey: string,dataaccaction:string, mainSateOPPrivs: any): any {
        let result: any = { [dataaccaction]: 1 };
        const curDefaultOPPrivs: any = this.getSysOPPrivs();
        // 统一资源权限
        if (curDefaultOPPrivs && (Object.keys(curDefaultOPPrivs).length > 0) && (curDefaultOPPrivs[dataaccaction] === 0)) {
            result[dataaccaction] = 0;
        }
        // 主状态权限
        if (mainSateOPPrivs && (Object.keys(mainSateOPPrivs).length > 0) && mainSateOPPrivs.hasOwnProperty(dataaccaction)) {
            result[dataaccaction] = result[dataaccaction] && mainSateOPPrivs[dataaccaction];
        }
        // 实体级权限
         if (!this.getActivedDeOPPrivs(activeKey, dataaccaction)) {
            result[dataaccaction] = 0;
        }
        return result;
    }

}