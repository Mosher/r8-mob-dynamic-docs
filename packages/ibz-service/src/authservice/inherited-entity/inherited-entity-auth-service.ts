import { InheritedEntityAuthServiceBase } from './inherited-entity-auth-service-base';


/**
 * 继承实体权限服务对象
 *
 * @export
 * @class InheritedEntityAuthService
 * @extends {InheritedEntityAuthServiceBase}
 */
export default class InheritedEntityAuthService extends InheritedEntityAuthServiceBase {

    /**
     * @description 基础权限服务实例
     * @private
     * @static
     * @type {InheritedEntityAuthService}
     * @memberof InheritedEntityAuthService
     */
    private static basicUIServiceInstance: InheritedEntityAuthService;

    /**
     * @description 动态模型服务存储Map对象
     * @private
     * @static
     * @type {Map<string, any>}
     * @memberof InheritedEntityAuthService
     */
    private static AuthServiceMap: Map<string, any> = new Map();

    /**
     * Creates an instance of  InheritedEntityAuthService.
     * 
     * @param {*} [opts={}]
     * @memberof  InheritedEntityAuthService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * @description 通过应用上下文获取实例对象
     * @static
     * @param {*} context
     * @return {*}  {InheritedEntityAuthService}
     * @memberof InheritedEntityAuthService
     */
    public static getInstance(context: any): InheritedEntityAuthService {
        if (!this.basicUIServiceInstance) {
            this.basicUIServiceInstance = new InheritedEntityAuthService({context:context});
        }
        if (!context.srfdynainstid) {
            return this.basicUIServiceInstance;
        } else {
            if (!InheritedEntityAuthService.AuthServiceMap.get(context.srfdynainstid)) {
                InheritedEntityAuthService.AuthServiceMap.set(context.srfdynainstid, new InheritedEntityAuthService({context:context}));
            }
            return InheritedEntityAuthService.AuthServiceMap.get(context.srfdynainstid);
        }
    }


}