import { IContext, IUtilService, IUtilServiceRegister } from 'ibz-core';
import { UtilServiceBase } from '../service-base';

/**
 * 功能服务注册中心
 *
 * @export
 * @class UtilServiceRegister
 */
export class UtilServiceRegister implements IUtilServiceRegister {

    /**
     * @description UtilServiceRegister 单例对象
     * @private
     * @static
     * @type {UtilServiceRegister}
     * @memberof UtilServiceRegister
     */
    private static UtilServiceRegister: UtilServiceRegister;

    /**
     * @description 所有UIService Map对象
     * @private
     * @static
     * @type {Map<string, any>}
     * @memberof UtilServiceRegister
     */
    private static allUtilServiceMap: Map<string, any> = new Map();

    /**
     * Creates an instance of UtilServiceRegister.
     * @memberof UtilServiceRegister
     */
    constructor() {
        this.init();
    }

    /**
     * @description 获取UIServiceRegister 单例对象
     * @static
     * @return {*} 
     * @memberof UtilServiceRegister
     */
    public static getInstance() {
        if (!this.UtilServiceRegister) {
            this.UtilServiceRegister = new UtilServiceRegister();
        }
        return this.UtilServiceRegister;
    }

    /**
     * @description 初始化
     * @protected
     * @memberof UtilServiceRegister
     */
    protected init(): void {
                UtilServiceRegister.allUtilServiceMap.set('dynadashboard', () => import('../utilservice/dynadashboard/dynadashboard-util-service'));
    }

    /**
     * @description 获取指定UtilService
     * @param {IContext} context 上下文
     * @param {string} entityKey 实体标识
     * @return {*}  {(Promise<IUtilService | undefined>)}
     * @memberof UtilServiceRegister
     */
    public async getService(context: IContext, entityKey: string): Promise<IUtilService> {
        const importService = UtilServiceRegister.allUtilServiceMap.get(entityKey);
        if (importService) {
            const importModule = await importService();
            return importModule.default.getInstance(context);
        } else {
            return new UtilServiceBase();
        }
    }

}