import { IContext, IEntityService, IEntityServiceRegister } from 'ibz-core';
import { EntityBaseService } from '../service-base';

/**
 * 实体服务注册中心
 *
 * @export
 * @class EntityServiceRegister
 */
export class EntityServiceRegister implements IEntityServiceRegister {

  /**
   * @description EntityServiceRegister 单例对象
   * @private
   * @static
   * @type {EntityServiceRegister}
   * @memberof EntityServiceRegister
   */
  private static EntityServiceRegister: EntityServiceRegister;

  /**
   * @description 所有实体服务 Map对象
   * @private
   * @static
   * @type {Map<string,any>}
   * @memberof EntityServiceRegister
   */    
	private static allEntityServiceMap: Map<string,any> = new Map();

	/**
	 * Creates an instance of EntityServiceRegister.
	 * @memberof EntityServiceRegister
	 */
	constructor() {
		this.init();
	}

  /**
   * @description 获取EntityServiceRegister 单例对象
   * @static
   * @return {*} 
   * @memberof EntityServiceRegister
   */
	public static getInstance(){
		if(!this.EntityServiceRegister){
			this.EntityServiceRegister = new EntityServiceRegister();
		}
		return this.EntityServiceRegister;
	}

  /**
   * @description 初始化
   * @protected
   * @memberof EntityServiceRegister
   */
	protected init(): void {
				EntityServiceRegister.allEntityServiceMap.set('virtualentity02', () => import('../service/virtual-entity02/virtual-entity02.service'));
		EntityServiceRegister.allEntityServiceMap.set('virtualentitymerge', () => import('../service/virtual-entity-merge/virtual-entity-merge.service'));
		EntityServiceRegister.allEntityServiceMap.set('custom', () => import('../service/custom/custom.service'));
		EntityServiceRegister.allEntityServiceMap.set('inheritedentity', () => import('../service/inherited-entity/inherited-entity.service'));
		EntityServiceRegister.allEntityServiceMap.set('bxd', () => import('../service/bxd/bxd.service'));
		EntityServiceRegister.allEntityServiceMap.set('virtualentity03', () => import('../service/virtual-entity03/virtual-entity03.service'));
		EntityServiceRegister.allEntityServiceMap.set('inheritedchildentity', () => import('../service/inherited-child-entity/inherited-child-entity.service'));
		EntityServiceRegister.allEntityServiceMap.set('project', () => import('../service/project/project.service'));
		EntityServiceRegister.allEntityServiceMap.set('virtualentity01', () => import('../service/virtual-entity01/virtual-entity01.service'));
		EntityServiceRegister.allEntityServiceMap.set('bxdlb', () => import('../service/bxdlb/bxdlb.service'));
		EntityServiceRegister.allEntityServiceMap.set('logicaldeletionentity', () => import('../service/logical-deletion-entity/logical-deletion-entity.service'));
		EntityServiceRegister.allEntityServiceMap.set('bxdmx', () => import('../service/bxdmx/bxdmx.service'));
		EntityServiceRegister.allEntityServiceMap.set('dynadashboard', () => import('../service/dynadashboard/dynadashboard.service'));
	}

  /**
   * @description 获取指定实体服务
   * @param {IContext} context 上下文
   * @param {string} entityKey 实体标识
   * @return {*}  {(Promise<IParam | undefined>)}
   * @memberof EntityServiceRegister
   */
	public async getService(context: IContext, entityKey: string): Promise<IEntityService>{
		const importService = EntityServiceRegister.allEntityServiceMap.get(entityKey);
		if(importService){
			const importModule = await importService();
			const service = importModule.default.getInstance(context)
			await service.loaded(context);
			return service;
		} else {
			return new EntityBaseService();
		}
	}

}