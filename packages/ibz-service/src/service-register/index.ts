export { AuthServiceRegister } from './auth-service-register';
export { UIServiceRegister } from './ui-service-register';
export { UtilServiceRegister } from './util-service-register';
export { EntityServiceRegister } from './entity-service-register';