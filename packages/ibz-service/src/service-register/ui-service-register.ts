import { IContext, IUIService, IUIServiceRegister } from 'ibz-core';
import { UIServiceBase } from '../service-base/ui-base.service';

/**
 * UI服务注册中心
 *
 * @export
 * @class UIServiceRegister
 */
export class UIServiceRegister implements IUIServiceRegister {

    /**
     * @description UIServiceRegister 单例对象
     * @private
     * @static
     * @type {UIServiceRegister}
     * @memberof UIServiceRegister
     */
    private static UIServiceRegister:UIServiceRegister;

    /**
     * @description 所有UIService Map对象
     * @private
     * @static
     * @type {Map<string,any>}
     * @memberof UIServiceRegister
     */   
    private static allUIServiceMap:Map<string,any> = new Map();

    /**
     * Creates an instance of UIServiceRegister.
     * @memberof UIServiceRegister
     */
    constructor() {
        this.init();
    }

    /**
     * @description 获取UIServiceRegister 单例对象
     * @static
     * @return {*} 
     * @memberof UIServiceRegister
     */
    public static getInstance(){
        if(!this.UIServiceRegister){
            this.UIServiceRegister = new UIServiceRegister();
        }
        return this.UIServiceRegister;
    }

    /**
     * @description 初始化
     * @protected
     * @memberof UIServiceRegister
     */
    protected init(): void {
                UIServiceRegister.allUIServiceMap.set('virtualentity02', () => import('../uiservice/virtual-entity02/virtual-entity02-ui-service'));
        UIServiceRegister.allUIServiceMap.set('virtualentitymerge', () => import('../uiservice/virtual-entity-merge/virtual-entity-merge-ui-service'));
        UIServiceRegister.allUIServiceMap.set('custom', () => import('../uiservice/custom/custom-ui-service'));
        UIServiceRegister.allUIServiceMap.set('inheritedentity', () => import('../uiservice/inherited-entity/inherited-entity-ui-service'));
        UIServiceRegister.allUIServiceMap.set('bxd', () => import('../uiservice/bxd/bxd-ui-service'));
        UIServiceRegister.allUIServiceMap.set('virtualentity03', () => import('../uiservice/virtual-entity03/virtual-entity03-ui-service'));
        UIServiceRegister.allUIServiceMap.set('inheritedchildentity', () => import('../uiservice/inherited-child-entity/inherited-child-entity-ui-service'));
        UIServiceRegister.allUIServiceMap.set('project', () => import('../uiservice/project/project-ui-service'));
        UIServiceRegister.allUIServiceMap.set('virtualentity01', () => import('../uiservice/virtual-entity01/virtual-entity01-ui-service'));
        UIServiceRegister.allUIServiceMap.set('bxdlb', () => import('../uiservice/bxdlb/bxdlb-ui-service'));
        UIServiceRegister.allUIServiceMap.set('logicaldeletionentity', () => import('../uiservice/logical-deletion-entity/logical-deletion-entity-ui-service'));
        UIServiceRegister.allUIServiceMap.set('bxdmx', () => import('../uiservice/bxdmx/bxdmx-ui-service'));
        UIServiceRegister.allUIServiceMap.set('dynadashboard', () => import('../uiservice/dynadashboard/dynadashboard-ui-service'));
    }

    /**
     * @description 获取指定UIService
     * @param {IContext} context 上下文
     * @param {string} entityKey 实体标识
     * @return {*}  {(Promise<IUIService | undefined>)}
     * @memberof UIServiceRegister
     */
    public async getService(context: IContext, entityKey: string): Promise<IUIService> {
        const importService = UIServiceRegister.allUIServiceMap.get(entityKey);
        if(importService){
            const importModule = await importService();
            return importModule.default.getInstance(context);
        } else {
            return new UIServiceBase();
        }
    }

}