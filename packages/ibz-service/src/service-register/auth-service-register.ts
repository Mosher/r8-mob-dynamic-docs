import { IAuthService, IContext, IAuthServiceRegister } from 'ibz-core';
import { AuthServiceBase } from '../service-base'

/**
 * 实体权限服务注册中心
 *
 * @export
 * @class AuthServiceRegister
 */
export class AuthServiceRegister implements IAuthServiceRegister{

    /**
     * @description AuthServiceRegister 单例对象
     * @private
     * @static
     * @type {AuthServiceRegister}
     * @memberof AuthServiceRegister
     */
    private static AuthServiceRegister: AuthServiceRegister;

    /**
     * @description 所有AuthService Map对象
     * @private
     * @static
     * @type {Map<string, any>}
     * @memberof AuthServiceRegister
     */
    private static allAuthServiceMap: Map<string, any> = new Map();

    /**
     * Creates an instance of AuthServiceRegister.
     * @memberof AuthServiceRegister
     */
    constructor() {
        this.init();
    }

    /**
     * @description 获取allAuthServiceMap 单例对象
     * @static
     * @return {*} 
     * @memberof AuthServiceRegister
     */
    public static getInstance() {
        if (!this.AuthServiceRegister) {
            this.AuthServiceRegister = new AuthServiceRegister();
        }
        return this.AuthServiceRegister;
    }

    /**
     * @description 初始化
     * @protected
     * @memberof AuthServiceRegister
     */
    protected init(): void {
            AuthServiceRegister.allAuthServiceMap.set('virtualentity02', () => import('../authservice/virtual-entity02/virtual-entity02-auth-service'));
        AuthServiceRegister.allAuthServiceMap.set('virtualentitymerge', () => import('../authservice/virtual-entity-merge/virtual-entity-merge-auth-service'));
        AuthServiceRegister.allAuthServiceMap.set('custom', () => import('../authservice/custom/custom-auth-service'));
        AuthServiceRegister.allAuthServiceMap.set('inheritedentity', () => import('../authservice/inherited-entity/inherited-entity-auth-service'));
        AuthServiceRegister.allAuthServiceMap.set('bxd', () => import('../authservice/bxd/bxd-auth-service'));
        AuthServiceRegister.allAuthServiceMap.set('virtualentity03', () => import('../authservice/virtual-entity03/virtual-entity03-auth-service'));
        AuthServiceRegister.allAuthServiceMap.set('inheritedchildentity', () => import('../authservice/inherited-child-entity/inherited-child-entity-auth-service'));
        AuthServiceRegister.allAuthServiceMap.set('project', () => import('../authservice/project/project-auth-service'));
        AuthServiceRegister.allAuthServiceMap.set('virtualentity01', () => import('../authservice/virtual-entity01/virtual-entity01-auth-service'));
        AuthServiceRegister.allAuthServiceMap.set('bxdlb', () => import('../authservice/bxdlb/bxdlb-auth-service'));
        AuthServiceRegister.allAuthServiceMap.set('logicaldeletionentity', () => import('../authservice/logical-deletion-entity/logical-deletion-entity-auth-service'));
        AuthServiceRegister.allAuthServiceMap.set('bxdmx', () => import('../authservice/bxdmx/bxdmx-auth-service'));
        AuthServiceRegister.allAuthServiceMap.set('dynadashboard', () => import('../authservice/dynadashboard/dynadashboard-auth-service'));
    }

    /**
     * @description 获取指定AuthService
     * @param {IContext} context 上下文
     * @param {string} entityKey 实体标识
     * @return {*}  {(Promise<IAuthService | undefined>)}
     * @memberof AuthServiceRegister
     */
    public async getService(context: IContext, entityKey: string): Promise<IAuthService> {
        const importService = AuthServiceRegister.allAuthServiceMap.get(entityKey);
        if (importService) {
            const importModule = await importService();
            return importModule.default.getInstance(context);
        } else {
            return new AuthServiceBase();
        }
    }

}