import { DYNADASHBOARDUIServiceBase } from './dynadashboard-ui-service-base';

/**
 * 动态数据看板模型UI服务对象
 *
 * @export
 * @class DYNADASHBOARDUIService
 */
export default class DYNADASHBOARDUIService extends DYNADASHBOARDUIServiceBase {

    /**
     * @description 基础UI服务实例
     * @private
     * @static
     * @type {DYNADASHBOARDUIService}
     * @memberof DYNADASHBOARDUIService
     */
    private static basicUIServiceInstance: DYNADASHBOARDUIService;

    /**
     * @description 动态模型服务存储Map对象
     * @private
     * @static
     * @type {Map<string, any>}
     * @memberof DYNADASHBOARDUIService
     */
    private static UIServiceMap: Map<string, any> = new Map();

    /**
     * Creates an instance of DYNADASHBOARDUIService.
     * @param {*} [opts={}]
     * @memberof DYNADASHBOARDUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * @description 通过应用上下文获取实例对象
     * @static
     * @param {*} context 上下文
     * @return {*}  {DYNADASHBOARDUIService}
     * @memberof DYNADASHBOARDUIService
     */
    public static getInstance(context: any): DYNADASHBOARDUIService {
        if (!this.basicUIServiceInstance) {
            this.basicUIServiceInstance = new DYNADASHBOARDUIService({context:context});
        }
        if (!context.srfdynainstid) {
            return this.basicUIServiceInstance;
        } else {
            if (!DYNADASHBOARDUIService.UIServiceMap.get(context.srfdynainstid)) {
                DYNADASHBOARDUIService.UIServiceMap.set(context.srfdynainstid, new DYNADASHBOARDUIService({context:context}));
            }
            return DYNADASHBOARDUIService.UIServiceMap.get(context.srfdynainstid);
        }
    }

}