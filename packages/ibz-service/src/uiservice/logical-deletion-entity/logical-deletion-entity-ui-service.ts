import { LogicalDeletionEntityUIServiceBase } from './logical-deletion-entity-ui-service-base';

/**
 * 逻辑删除实体UI服务对象
 *
 * @export
 * @class LogicalDeletionEntityUIService
 */
export default class LogicalDeletionEntityUIService extends LogicalDeletionEntityUIServiceBase {

    /**
     * @description 基础UI服务实例
     * @private
     * @static
     * @type {LogicalDeletionEntityUIService}
     * @memberof LogicalDeletionEntityUIService
     */
    private static basicUIServiceInstance: LogicalDeletionEntityUIService;

    /**
     * @description 动态模型服务存储Map对象
     * @private
     * @static
     * @type {Map<string, any>}
     * @memberof LogicalDeletionEntityUIService
     */
    private static UIServiceMap: Map<string, any> = new Map();

    /**
     * Creates an instance of LogicalDeletionEntityUIService.
     * @param {*} [opts={}]
     * @memberof LogicalDeletionEntityUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * @description 通过应用上下文获取实例对象
     * @static
     * @param {*} context 上下文
     * @return {*}  {LogicalDeletionEntityUIService}
     * @memberof LogicalDeletionEntityUIService
     */
    public static getInstance(context: any): LogicalDeletionEntityUIService {
        if (!this.basicUIServiceInstance) {
            this.basicUIServiceInstance = new LogicalDeletionEntityUIService({context:context});
        }
        if (!context.srfdynainstid) {
            return this.basicUIServiceInstance;
        } else {
            if (!LogicalDeletionEntityUIService.UIServiceMap.get(context.srfdynainstid)) {
                LogicalDeletionEntityUIService.UIServiceMap.set(context.srfdynainstid, new LogicalDeletionEntityUIService({context:context}));
            }
            return LogicalDeletionEntityUIService.UIServiceMap.get(context.srfdynainstid);
        }
    }

}