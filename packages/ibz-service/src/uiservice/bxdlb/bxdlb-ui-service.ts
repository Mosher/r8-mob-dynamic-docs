import { BXDLBUIServiceBase } from './bxdlb-ui-service-base';

/**
 * 报销单类别UI服务对象
 *
 * @export
 * @class BXDLBUIService
 */
export default class BXDLBUIService extends BXDLBUIServiceBase {

    /**
     * @description 基础UI服务实例
     * @private
     * @static
     * @type {BXDLBUIService}
     * @memberof BXDLBUIService
     */
    private static basicUIServiceInstance: BXDLBUIService;

    /**
     * @description 动态模型服务存储Map对象
     * @private
     * @static
     * @type {Map<string, any>}
     * @memberof BXDLBUIService
     */
    private static UIServiceMap: Map<string, any> = new Map();

    /**
     * Creates an instance of BXDLBUIService.
     * @param {*} [opts={}]
     * @memberof BXDLBUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * @description 通过应用上下文获取实例对象
     * @static
     * @param {*} context 上下文
     * @return {*}  {BXDLBUIService}
     * @memberof BXDLBUIService
     */
    public static getInstance(context: any): BXDLBUIService {
        if (!this.basicUIServiceInstance) {
            this.basicUIServiceInstance = new BXDLBUIService({context:context});
        }
        if (!context.srfdynainstid) {
            return this.basicUIServiceInstance;
        } else {
            if (!BXDLBUIService.UIServiceMap.get(context.srfdynainstid)) {
                BXDLBUIService.UIServiceMap.set(context.srfdynainstid, new BXDLBUIService({context:context}));
            }
            return BXDLBUIService.UIServiceMap.get(context.srfdynainstid);
        }
    }

}