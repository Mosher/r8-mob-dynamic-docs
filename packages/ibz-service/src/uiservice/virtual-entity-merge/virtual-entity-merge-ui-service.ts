import { VirtualEntityMergeUIServiceBase } from './virtual-entity-merge-ui-service-base';

/**
 * 虚拟实体合并UI服务对象
 *
 * @export
 * @class VirtualEntityMergeUIService
 */
export default class VirtualEntityMergeUIService extends VirtualEntityMergeUIServiceBase {

    /**
     * @description 基础UI服务实例
     * @private
     * @static
     * @type {VirtualEntityMergeUIService}
     * @memberof VirtualEntityMergeUIService
     */
    private static basicUIServiceInstance: VirtualEntityMergeUIService;

    /**
     * @description 动态模型服务存储Map对象
     * @private
     * @static
     * @type {Map<string, any>}
     * @memberof VirtualEntityMergeUIService
     */
    private static UIServiceMap: Map<string, any> = new Map();

    /**
     * Creates an instance of VirtualEntityMergeUIService.
     * @param {*} [opts={}]
     * @memberof VirtualEntityMergeUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * @description 通过应用上下文获取实例对象
     * @static
     * @param {*} context 上下文
     * @return {*}  {VirtualEntityMergeUIService}
     * @memberof VirtualEntityMergeUIService
     */
    public static getInstance(context: any): VirtualEntityMergeUIService {
        if (!this.basicUIServiceInstance) {
            this.basicUIServiceInstance = new VirtualEntityMergeUIService({context:context});
        }
        if (!context.srfdynainstid) {
            return this.basicUIServiceInstance;
        } else {
            if (!VirtualEntityMergeUIService.UIServiceMap.get(context.srfdynainstid)) {
                VirtualEntityMergeUIService.UIServiceMap.set(context.srfdynainstid, new VirtualEntityMergeUIService({context:context}));
            }
            return VirtualEntityMergeUIService.UIServiceMap.get(context.srfdynainstid);
        }
    }

}