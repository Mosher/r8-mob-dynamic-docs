import { VirtualEntity03UIServiceBase } from './virtual-entity03-ui-service-base';

/**
 * 虚拟实体03UI服务对象
 *
 * @export
 * @class VirtualEntity03UIService
 */
export default class VirtualEntity03UIService extends VirtualEntity03UIServiceBase {

    /**
     * @description 基础UI服务实例
     * @private
     * @static
     * @type {VirtualEntity03UIService}
     * @memberof VirtualEntity03UIService
     */
    private static basicUIServiceInstance: VirtualEntity03UIService;

    /**
     * @description 动态模型服务存储Map对象
     * @private
     * @static
     * @type {Map<string, any>}
     * @memberof VirtualEntity03UIService
     */
    private static UIServiceMap: Map<string, any> = new Map();

    /**
     * Creates an instance of VirtualEntity03UIService.
     * @param {*} [opts={}]
     * @memberof VirtualEntity03UIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * @description 通过应用上下文获取实例对象
     * @static
     * @param {*} context 上下文
     * @return {*}  {VirtualEntity03UIService}
     * @memberof VirtualEntity03UIService
     */
    public static getInstance(context: any): VirtualEntity03UIService {
        if (!this.basicUIServiceInstance) {
            this.basicUIServiceInstance = new VirtualEntity03UIService({context:context});
        }
        if (!context.srfdynainstid) {
            return this.basicUIServiceInstance;
        } else {
            if (!VirtualEntity03UIService.UIServiceMap.get(context.srfdynainstid)) {
                VirtualEntity03UIService.UIServiceMap.set(context.srfdynainstid, new VirtualEntity03UIService({context:context}));
            }
            return VirtualEntity03UIService.UIServiceMap.get(context.srfdynainstid);
        }
    }

}