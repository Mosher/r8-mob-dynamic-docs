import { IPSAppDEUIAction } from '@ibiz/dynamic-model-api';
import { UIServiceBase } from '../../service-base';
import { AuthServiceRegister } from '../../service-register';

/**
 * 虚拟实体03UI服务对象基类
 *
 * @export
 * @class VirtualEntity03UIServiceBase
 */
export class VirtualEntity03UIServiceBase extends UIServiceBase {

    /**
     * @description 应用实体动态模型文件路径
     * @protected
     * @type {string}
     * @memberof VirtualEntity03UIServiceBase
     */ 
    protected dynaModelFilePath:string = "PSSYSAPPS/TestMob/PSAPPDATAENTITIES/VirtualEntity03.json";

    /**
     * Creates an instance of VirtualEntity03UIServiceBase.
     * @param {*} [opts={}]
     * @memberof VirtualEntity03UIServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * @description 加载应用实体模型数据
     * @memberof VirtualEntity03UIServiceBase
     */
    public async loaded() {
        await super.loaded();
    }

    /**
     * @description 初始化基础数据
     * @protected
     * @memberof VirtualEntity03UIServiceBase
     */
    protected initBasicData(){
        this.isEnableDEMainState = false;
        this.dynaInstTag = "";
        this.tempOrgIdDEField =null;
        this.stateValue = 0;
        this.multiFormDEField = null;
        this.indexTypeDEField = null;
        this.stateField = "";
        this.mainStateFields = [];
    }

    /**
     * @description 初始化界面行为数据
     * @protected
     * @return {*}  {Promise<void>}
     * @memberof VirtualEntity03UIServiceBase
     */
    protected async initActionMap(): Promise<void> {
        const actions = this.entityModel?.getAllPSAppDEUIActions() as IPSAppDEUIAction[];
        if (actions && actions.length > 0) {
            for (const element of actions) {
                const targetAction: any = await App.getActionService().getUIActionInst(element, this.context);
                this.actionMap.set(element.uIActionTag, targetAction);
            }
        }
    }

    /**
     * @description 初始化视图功能数据Map
     * @protected
     * @memberof VirtualEntity03UIServiceBase
     */
    protected initViewFuncMap(){
    }

}