import { InheritedEntityUIServiceBase } from './inherited-entity-ui-service-base';

/**
 * 继承实体UI服务对象
 *
 * @export
 * @class InheritedEntityUIService
 */
export default class InheritedEntityUIService extends InheritedEntityUIServiceBase {

    /**
     * @description 基础UI服务实例
     * @private
     * @static
     * @type {InheritedEntityUIService}
     * @memberof InheritedEntityUIService
     */
    private static basicUIServiceInstance: InheritedEntityUIService;

    /**
     * @description 动态模型服务存储Map对象
     * @private
     * @static
     * @type {Map<string, any>}
     * @memberof InheritedEntityUIService
     */
    private static UIServiceMap: Map<string, any> = new Map();

    /**
     * Creates an instance of InheritedEntityUIService.
     * @param {*} [opts={}]
     * @memberof InheritedEntityUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * @description 通过应用上下文获取实例对象
     * @static
     * @param {*} context 上下文
     * @return {*}  {InheritedEntityUIService}
     * @memberof InheritedEntityUIService
     */
    public static getInstance(context: any): InheritedEntityUIService {
        if (!this.basicUIServiceInstance) {
            this.basicUIServiceInstance = new InheritedEntityUIService({context:context});
        }
        if (!context.srfdynainstid) {
            return this.basicUIServiceInstance;
        } else {
            if (!InheritedEntityUIService.UIServiceMap.get(context.srfdynainstid)) {
                InheritedEntityUIService.UIServiceMap.set(context.srfdynainstid, new InheritedEntityUIService({context:context}));
            }
            return InheritedEntityUIService.UIServiceMap.get(context.srfdynainstid);
        }
    }

}