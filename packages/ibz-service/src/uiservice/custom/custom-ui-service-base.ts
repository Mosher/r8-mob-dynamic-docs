import { IPSAppDEUIAction } from '@ibiz/dynamic-model-api';
import { UIServiceBase } from '../../service-base';
import { AuthServiceRegister } from '../../service-register';

/**
 * 自定义实体UI服务对象基类
 *
 * @export
 * @class CUSTOMUIServiceBase
 */
export class CUSTOMUIServiceBase extends UIServiceBase {

    /**
     * @description 应用实体动态模型文件路径
     * @protected
     * @type {string}
     * @memberof CUSTOMUIServiceBase
     */ 
    protected dynaModelFilePath:string = "PSSYSAPPS/TestMob/PSAPPDATAENTITIES/CUSTOM.json";

    /**
     * Creates an instance of CUSTOMUIServiceBase.
     * @param {*} [opts={}]
     * @memberof CUSTOMUIServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * @description 加载应用实体模型数据
     * @memberof CUSTOMUIServiceBase
     */
    public async loaded() {
        await super.loaded();
    }

    /**
     * @description 初始化基础数据
     * @protected
     * @memberof CUSTOMUIServiceBase
     */
    protected initBasicData(){
        this.isEnableDEMainState = false;
        this.dynaInstTag = "";
        this.tempOrgIdDEField =null;
        this.stateValue = 0;
        this.multiFormDEField = null;
        this.indexTypeDEField = null;
        this.stateField = "";
        this.mainStateFields = [];
    }

    /**
     * @description 初始化界面行为数据
     * @protected
     * @return {*}  {Promise<void>}
     * @memberof CUSTOMUIServiceBase
     */
    protected async initActionMap(): Promise<void> {
        const actions = this.entityModel?.getAllPSAppDEUIActions() as IPSAppDEUIAction[];
        if (actions && actions.length > 0) {
            for (const element of actions) {
                const targetAction: any = await App.getActionService().getUIActionInst(element, this.context);
                this.actionMap.set(element.uIActionTag, targetAction);
            }
        }
    }

    /**
     * @description 初始化视图功能数据Map
     * @protected
     * @memberof CUSTOMUIServiceBase
     */
    protected initViewFuncMap(){
        this.allViewFuncMap.set(':','');
        this.allViewFuncMap.set(':','');
        this.allViewFuncMap.set(':','');
        this.allViewFuncMap.set(':','');
        this.allViewFuncMap.set(':','');
        this.allViewFuncMap.set(':','');
        this.allViewFuncMap.set(':','');
        this.allViewFuncMap.set(':','');
        this.allViewFuncMap.set(':','');
        this.allViewFuncMap.set(':','');
        this.allViewFuncMap.set(':','');
        this.allViewFuncMap.set(':','');
        this.allViewFuncMap.set(':','');
        this.allViewFuncMap.set(':','');
        this.allViewFuncMap.set(':','');
        this.allViewFuncMap.set(':','');
        this.allViewFuncMap.set(':','');
        this.allViewFuncMap.set(':','');
        this.allViewFuncMap.set(':','');
        this.allViewFuncMap.set(':','');
        this.allViewFuncMap.set(':','');
        this.allViewFuncMap.set(':','');
        this.allViewFuncMap.set(':','');
        this.allViewFuncMap.set(':','');
        this.allViewFuncMap.set(':','');
        this.allViewFuncMap.set(':','');
        this.allViewFuncMap.set(':','');
        this.allViewFuncMap.set(':','');
        this.allViewFuncMap.set(':','');
        this.allViewFuncMap.set(':','');
        this.allViewFuncMap.set(':','');
        this.allViewFuncMap.set(':','');
        this.allViewFuncMap.set(':','');
    }

}