import { CUSTOMUIServiceBase } from './custom-ui-service-base';

/**
 * 自定义实体UI服务对象
 *
 * @export
 * @class CUSTOMUIService
 */
export default class CUSTOMUIService extends CUSTOMUIServiceBase {

    /**
     * @description 基础UI服务实例
     * @private
     * @static
     * @type {CUSTOMUIService}
     * @memberof CUSTOMUIService
     */
    private static basicUIServiceInstance: CUSTOMUIService;

    /**
     * @description 动态模型服务存储Map对象
     * @private
     * @static
     * @type {Map<string, any>}
     * @memberof CUSTOMUIService
     */
    private static UIServiceMap: Map<string, any> = new Map();

    /**
     * Creates an instance of CUSTOMUIService.
     * @param {*} [opts={}]
     * @memberof CUSTOMUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * @description 通过应用上下文获取实例对象
     * @static
     * @param {*} context 上下文
     * @return {*}  {CUSTOMUIService}
     * @memberof CUSTOMUIService
     */
    public static getInstance(context: any): CUSTOMUIService {
        if (!this.basicUIServiceInstance) {
            this.basicUIServiceInstance = new CUSTOMUIService({context:context});
        }
        if (!context.srfdynainstid) {
            return this.basicUIServiceInstance;
        } else {
            if (!CUSTOMUIService.UIServiceMap.get(context.srfdynainstid)) {
                CUSTOMUIService.UIServiceMap.set(context.srfdynainstid, new CUSTOMUIService({context:context}));
            }
            return CUSTOMUIService.UIServiceMap.get(context.srfdynainstid);
        }
    }

}