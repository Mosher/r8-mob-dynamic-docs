import { BXDMXUIServiceBase } from './bxdmx-ui-service-base';

/**
 * 报销单明细UI服务对象
 *
 * @export
 * @class BXDMXUIService
 */
export default class BXDMXUIService extends BXDMXUIServiceBase {

    /**
     * @description 基础UI服务实例
     * @private
     * @static
     * @type {BXDMXUIService}
     * @memberof BXDMXUIService
     */
    private static basicUIServiceInstance: BXDMXUIService;

    /**
     * @description 动态模型服务存储Map对象
     * @private
     * @static
     * @type {Map<string, any>}
     * @memberof BXDMXUIService
     */
    private static UIServiceMap: Map<string, any> = new Map();

    /**
     * Creates an instance of BXDMXUIService.
     * @param {*} [opts={}]
     * @memberof BXDMXUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * @description 通过应用上下文获取实例对象
     * @static
     * @param {*} context 上下文
     * @return {*}  {BXDMXUIService}
     * @memberof BXDMXUIService
     */
    public static getInstance(context: any): BXDMXUIService {
        if (!this.basicUIServiceInstance) {
            this.basicUIServiceInstance = new BXDMXUIService({context:context});
        }
        if (!context.srfdynainstid) {
            return this.basicUIServiceInstance;
        } else {
            if (!BXDMXUIService.UIServiceMap.get(context.srfdynainstid)) {
                BXDMXUIService.UIServiceMap.set(context.srfdynainstid, new BXDMXUIService({context:context}));
            }
            return BXDMXUIService.UIServiceMap.get(context.srfdynainstid);
        }
    }

}