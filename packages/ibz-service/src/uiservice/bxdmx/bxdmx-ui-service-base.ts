import { IPSAppDEUIAction } from '@ibiz/dynamic-model-api';
import { UIServiceBase } from '../../service-base';
import { AuthServiceRegister } from '../../service-register';

/**
 * 报销单明细UI服务对象基类
 *
 * @export
 * @class BXDMXUIServiceBase
 */
export class BXDMXUIServiceBase extends UIServiceBase {

    /**
     * @description 应用实体动态模型文件路径
     * @protected
     * @type {string}
     * @memberof BXDMXUIServiceBase
     */ 
    protected dynaModelFilePath:string = "PSSYSAPPS/TestMob/PSAPPDATAENTITIES/BXDMX.json";

    /**
     * Creates an instance of BXDMXUIServiceBase.
     * @param {*} [opts={}]
     * @memberof BXDMXUIServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * @description 加载应用实体模型数据
     * @memberof BXDMXUIServiceBase
     */
    public async loaded() {
        await super.loaded();
    }

    /**
     * @description 初始化基础数据
     * @protected
     * @memberof BXDMXUIServiceBase
     */
    protected initBasicData(){
        this.isEnableDEMainState = false;
        this.dynaInstTag = "";
        this.tempOrgIdDEField =null;
        this.stateValue = 0;
        this.multiFormDEField = null;
        this.indexTypeDEField = null;
        this.stateField = "";
        this.mainStateFields = [];
    }

    /**
     * @description 初始化界面行为数据
     * @protected
     * @return {*}  {Promise<void>}
     * @memberof BXDMXUIServiceBase
     */
    protected async initActionMap(): Promise<void> {
        const actions = this.entityModel?.getAllPSAppDEUIActions() as IPSAppDEUIAction[];
        if (actions && actions.length > 0) {
            for (const element of actions) {
                const targetAction: any = await App.getActionService().getUIActionInst(element, this.context);
                this.actionMap.set(element.uIActionTag, targetAction);
            }
        }
    }

    /**
     * @description 初始化视图功能数据Map
     * @protected
     * @memberof BXDMXUIServiceBase
     */
    protected initViewFuncMap(){
        this.allViewFuncMap.set(':','');
        this.allViewFuncMap.set(':','');
    }

}