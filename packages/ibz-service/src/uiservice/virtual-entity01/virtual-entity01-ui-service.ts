import { VirtualEntity01UIServiceBase } from './virtual-entity01-ui-service-base';

/**
 * 虚拟实体01UI服务对象
 *
 * @export
 * @class VirtualEntity01UIService
 */
export default class VirtualEntity01UIService extends VirtualEntity01UIServiceBase {

    /**
     * @description 基础UI服务实例
     * @private
     * @static
     * @type {VirtualEntity01UIService}
     * @memberof VirtualEntity01UIService
     */
    private static basicUIServiceInstance: VirtualEntity01UIService;

    /**
     * @description 动态模型服务存储Map对象
     * @private
     * @static
     * @type {Map<string, any>}
     * @memberof VirtualEntity01UIService
     */
    private static UIServiceMap: Map<string, any> = new Map();

    /**
     * Creates an instance of VirtualEntity01UIService.
     * @param {*} [opts={}]
     * @memberof VirtualEntity01UIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * @description 通过应用上下文获取实例对象
     * @static
     * @param {*} context 上下文
     * @return {*}  {VirtualEntity01UIService}
     * @memberof VirtualEntity01UIService
     */
    public static getInstance(context: any): VirtualEntity01UIService {
        if (!this.basicUIServiceInstance) {
            this.basicUIServiceInstance = new VirtualEntity01UIService({context:context});
        }
        if (!context.srfdynainstid) {
            return this.basicUIServiceInstance;
        } else {
            if (!VirtualEntity01UIService.UIServiceMap.get(context.srfdynainstid)) {
                VirtualEntity01UIService.UIServiceMap.set(context.srfdynainstid, new VirtualEntity01UIService({context:context}));
            }
            return VirtualEntity01UIService.UIServiceMap.get(context.srfdynainstid);
        }
    }

}