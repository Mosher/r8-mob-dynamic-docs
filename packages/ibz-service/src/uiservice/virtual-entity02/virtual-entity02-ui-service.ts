import { VirtualEntity02UIServiceBase } from './virtual-entity02-ui-service-base';

/**
 * 虚拟实体02UI服务对象
 *
 * @export
 * @class VirtualEntity02UIService
 */
export default class VirtualEntity02UIService extends VirtualEntity02UIServiceBase {

    /**
     * @description 基础UI服务实例
     * @private
     * @static
     * @type {VirtualEntity02UIService}
     * @memberof VirtualEntity02UIService
     */
    private static basicUIServiceInstance: VirtualEntity02UIService;

    /**
     * @description 动态模型服务存储Map对象
     * @private
     * @static
     * @type {Map<string, any>}
     * @memberof VirtualEntity02UIService
     */
    private static UIServiceMap: Map<string, any> = new Map();

    /**
     * Creates an instance of VirtualEntity02UIService.
     * @param {*} [opts={}]
     * @memberof VirtualEntity02UIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * @description 通过应用上下文获取实例对象
     * @static
     * @param {*} context 上下文
     * @return {*}  {VirtualEntity02UIService}
     * @memberof VirtualEntity02UIService
     */
    public static getInstance(context: any): VirtualEntity02UIService {
        if (!this.basicUIServiceInstance) {
            this.basicUIServiceInstance = new VirtualEntity02UIService({context:context});
        }
        if (!context.srfdynainstid) {
            return this.basicUIServiceInstance;
        } else {
            if (!VirtualEntity02UIService.UIServiceMap.get(context.srfdynainstid)) {
                VirtualEntity02UIService.UIServiceMap.set(context.srfdynainstid, new VirtualEntity02UIService({context:context}));
            }
            return VirtualEntity02UIService.UIServiceMap.get(context.srfdynainstid);
        }
    }

}