import { IPSAppDEUIAction } from '@ibiz/dynamic-model-api';
import { UIServiceBase } from '../../service-base';
import { AuthServiceRegister } from '../../service-register';

/**
 * 继承实体(子)UI服务对象基类
 *
 * @export
 * @class InheritedChildEntityUIServiceBase
 */
export class InheritedChildEntityUIServiceBase extends UIServiceBase {

    /**
     * @description 应用实体动态模型文件路径
     * @protected
     * @type {string}
     * @memberof InheritedChildEntityUIServiceBase
     */ 
    protected dynaModelFilePath:string = "PSSYSAPPS/TestMob/PSAPPDATAENTITIES/InheritedChildEntity.json";

    /**
     * Creates an instance of InheritedChildEntityUIServiceBase.
     * @param {*} [opts={}]
     * @memberof InheritedChildEntityUIServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * @description 加载应用实体模型数据
     * @memberof InheritedChildEntityUIServiceBase
     */
    public async loaded() {
        await super.loaded();
    }

    /**
     * @description 初始化基础数据
     * @protected
     * @memberof InheritedChildEntityUIServiceBase
     */
    protected initBasicData(){
        this.isEnableDEMainState = false;
        this.dynaInstTag = "";
        this.tempOrgIdDEField =null;
        this.stateValue = 0;
        this.multiFormDEField = null;
        this.indexTypeDEField = null;
        this.stateField = "";
        this.mainStateFields = [];
    }

    /**
     * @description 初始化界面行为数据
     * @protected
     * @return {*}  {Promise<void>}
     * @memberof InheritedChildEntityUIServiceBase
     */
    protected async initActionMap(): Promise<void> {
        const actions = this.entityModel?.getAllPSAppDEUIActions() as IPSAppDEUIAction[];
        if (actions && actions.length > 0) {
            for (const element of actions) {
                const targetAction: any = await App.getActionService().getUIActionInst(element, this.context);
                this.actionMap.set(element.uIActionTag, targetAction);
            }
        }
    }

    /**
     * @description 初始化视图功能数据Map
     * @protected
     * @memberof InheritedChildEntityUIServiceBase
     */
    protected initViewFuncMap(){
    }

}