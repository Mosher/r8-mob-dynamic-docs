import { InheritedChildEntityUIServiceBase } from './inherited-child-entity-ui-service-base';

/**
 * 继承实体(子)UI服务对象
 *
 * @export
 * @class InheritedChildEntityUIService
 */
export default class InheritedChildEntityUIService extends InheritedChildEntityUIServiceBase {

    /**
     * @description 基础UI服务实例
     * @private
     * @static
     * @type {InheritedChildEntityUIService}
     * @memberof InheritedChildEntityUIService
     */
    private static basicUIServiceInstance: InheritedChildEntityUIService;

    /**
     * @description 动态模型服务存储Map对象
     * @private
     * @static
     * @type {Map<string, any>}
     * @memberof InheritedChildEntityUIService
     */
    private static UIServiceMap: Map<string, any> = new Map();

    /**
     * Creates an instance of InheritedChildEntityUIService.
     * @param {*} [opts={}]
     * @memberof InheritedChildEntityUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * @description 通过应用上下文获取实例对象
     * @static
     * @param {*} context 上下文
     * @return {*}  {InheritedChildEntityUIService}
     * @memberof InheritedChildEntityUIService
     */
    public static getInstance(context: any): InheritedChildEntityUIService {
        if (!this.basicUIServiceInstance) {
            this.basicUIServiceInstance = new InheritedChildEntityUIService({context:context});
        }
        if (!context.srfdynainstid) {
            return this.basicUIServiceInstance;
        } else {
            if (!InheritedChildEntityUIService.UIServiceMap.get(context.srfdynainstid)) {
                InheritedChildEntityUIService.UIServiceMap.set(context.srfdynainstid, new InheritedChildEntityUIService({context:context}));
            }
            return InheritedChildEntityUIService.UIServiceMap.get(context.srfdynainstid);
        }
    }

}