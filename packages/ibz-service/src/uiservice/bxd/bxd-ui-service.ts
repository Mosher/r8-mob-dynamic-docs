import { BXDUIServiceBase } from './bxd-ui-service-base';

/**
 * 报销单UI服务对象
 *
 * @export
 * @class BXDUIService
 */
export default class BXDUIService extends BXDUIServiceBase {

    /**
     * @description 基础UI服务实例
     * @private
     * @static
     * @type {BXDUIService}
     * @memberof BXDUIService
     */
    private static basicUIServiceInstance: BXDUIService;

    /**
     * @description 动态模型服务存储Map对象
     * @private
     * @static
     * @type {Map<string, any>}
     * @memberof BXDUIService
     */
    private static UIServiceMap: Map<string, any> = new Map();

    /**
     * Creates an instance of BXDUIService.
     * @param {*} [opts={}]
     * @memberof BXDUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * @description 通过应用上下文获取实例对象
     * @static
     * @param {*} context 上下文
     * @return {*}  {BXDUIService}
     * @memberof BXDUIService
     */
    public static getInstance(context: any): BXDUIService {
        if (!this.basicUIServiceInstance) {
            this.basicUIServiceInstance = new BXDUIService({context:context});
        }
        if (!context.srfdynainstid) {
            return this.basicUIServiceInstance;
        } else {
            if (!BXDUIService.UIServiceMap.get(context.srfdynainstid)) {
                BXDUIService.UIServiceMap.set(context.srfdynainstid, new BXDUIService({context:context}));
            }
            return BXDUIService.UIServiceMap.get(context.srfdynainstid);
        }
    }

}