import { IParam, IContext } from 'ibz-core';
import { UtilServiceBase } from '../../service-base';

/**
 * 动态数据看板功能服务对象基类
 *
 * @export
 * @class DYNADASHBOARDUtilServiceBase
 */
export class DYNADASHBOARDUtilServiceBase extends UtilServiceBase {
 
    /**
     * @description 获取数据行为
     * @type {string}
     * @memberof DYNADASHBOARDUtilServiceBase
     */
    public loadAction: string = "Get";

    /**
     * @description 建立数据行为
     * @type {string}
     * @memberof DYNADASHBOARDUtilServiceBase
     */
    public createAction: string = "Create";

    /**
     * @description 更新数据行为
     * @type {string}
     * @memberof DYNADASHBOARDUtilServiceBase
     */
    public updateAction: string = "Update";

    /**
     * @description 删除数据行为
     * @type {string}
     * @memberof DYNADASHBOARDUtilServiceBase
     */
    public removeAction: string = "Remove";

    /**
     * Creates an instance of DYNADASHBOARDUtilServiceBase.
     * @param {*} [opts={}]
     * @memberof DYNADASHBOARDUtilServiceBase
     */
    constructor(opts: any = {}) {
        super(opts);
    }

    /**
     * @description 初始化基础参数
     * @memberof DYNADASHBOARDUtilServiceBase
     */
    public initBasicParam(){
        this.modelIdField = "modelid";
        this.modelField = "model";
        this.appIdField = "appid";
        this.userIdField = "userid";
        this.stoageEntityName ="dynadashboard";
        this.stoageEntityKey ="dynadashboardid";
    }
 
    /**
     * @description 获取模型数据
     * @param {IContext} [context={}] 应用上下文
     * @param {IParam} [data={}] 传入模型数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<IParam>}
     * @memberof DYNADASHBOARDUtilServiceBase
     */
    public async loadModelData(context: IContext = {}, data: IParam = {}, isloading?: boolean): Promise<IParam>{
        if (data && data.utilServiceName) {
            const requestParam:any ={ configType: data.utilServiceName, targetType: data.modelid };
            const entityService = await App.getEntityService().getService(context, 'dynadashboard');
            if (entityService && entityService.getDynaModel && entityService.getDynaModel instanceof Function) {
                const res = entityService.getDynaModel(context, requestParam);
                if (res && res.status === 200) {
                    return { status: 200, data: res.data && res.data.model ? res.data.model : [] };
                } else {
                    return res;
                }
            } else {
                return { status: 200, data: [] };
            }
        } else {
            let dataStr = window.localStorage.getItem(data.modelid);
            if(dataStr) {
                const data: any = JSON.parse(dataStr);
                return { status: 200, data: data.model };
            } else {
                return { status: 200, data:[] };
            } 
        }
    }

    /**
     * @description 保存模型数据
     * @param {IContext} [context={}] 应用上下文
     * @param {IParam} [data={}] 传入模型数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<IParam>}
     * @memberof DYNADASHBOARDUtilServiceBase
     */
    public async saveModelData(context: IContext = {}, data: IParam = {}, isloading?: boolean): Promise<IParam>{
        if (data && data.utilServiceName) {
            const requestParam: any ={ configType: data.utilServiceName, targetType: data.modelid, model: data.model };
            const entityService = await App.getEntityService().getService(context, 'dynadashboard');
            if (entityService && entityService.setDynaModel && entityService.setDynaModel instanceof Function) {
                const res = entityService.setDynaModel(context, requestParam);
                if (res && res.status === 200) {
                    return { status: 200, data: data.model };
                } else {
                    return res;
                }
            } else {
                return { status: 200, data: data.model };
            }
        } else {
            window.localStorage.setItem(data.modelid, JSON.stringify(data));
            return { status: 200, data: data.model };
        }  
    }


}