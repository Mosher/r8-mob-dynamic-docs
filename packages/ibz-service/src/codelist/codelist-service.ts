import { IPSAppCodeList, IPSCodeItem } from '@ibiz/dynamic-model-api';
import { AppModelService, GetModelService, LogUtil, ICodeListService } from 'ibz-core';
import { DynamicCodeListService } from './dynamic-codelist-service';
import { EntityBaseService } from '../service-base/entity-base-service';

/**
 * 代码表服务基类
 *
 * @export
 * @class CodeListService
 */
export class CodeListService implements ICodeListService {

    /**
     * @description 动态代码表缓存(加载中)
     * @static
     * @type {Map<string, any>}
     * @memberof CodeListService
     */
    private static codelistCache: Map<string, any> = new Map();

    /**
     * @description 动态代码表缓存(已完成)
     * @static
     * @type {Map<string, any>}
     * @memberof CodeListService
     */
    private static codelistCached: Map<string, any> = new Map();

    /**
     * @description 数据服务基类
     * @type {EntityBaseService<any>}
     * @memberof CodeListService
     */
    private entityService: EntityBaseService<any> = new EntityBaseService();

    /**
     * @description 获取代码表数据
     * @param {{ tag: string, type: string, data?: Array<any>, navContext?: any, navViewParam?: any }} params
     * @return {*} 
     * @memberof CodeListService
     */
    public async getDataItems(params: { tag: string, type: string, data?: Array<any>, navContext?: any, navViewParam?: any }) {
        let dataItems: any;
        try {
            if (params.tag && Object.is(params.type, "STATIC")) {
                dataItems = await this.getStaticItems(params.tag, params.data, params.navContext);
            } else {
                dataItems = await this.getItems(params.tag, params.navContext, params.navViewParam, false);
            }
        } catch (error) {
          LogUtil.warn("代码表加载异常" + error);
        }
        if (!dataItems) dataItems = [];
        return dataItems;
    }

    /**
     * @description 获取多语言翻译方法
     * @param {*} context 运行上下文
     * @return {*} 
     * @memberof CodeListService
     */
    private async getTranslate(context: any){
        let appModelService: AppModelService = await GetModelService(context);
        //TODO 国际化
        return function(key: string, value?: string){
            if (appModelService) {
                const lanResource: any = appModelService.getPSLang(key);
                return lanResource ? lanResource : value ? value : key;
            } else {
            return value ? value : key;
            }
        }
    }

    /**
     * @description 获取静态代码表
     * @param {string} tag 代码表标识
     * @param {*} [data] 基础数据
     * @param {*} [context] 应用上下文
     * @return {*} 
     * @memberof CodeListService
     */
    private async getStaticItems(tag: string, data?: any, context?: any) {
        let codelist: IPSAppCodeList | undefined;
        if (data) {
            codelist = data;
        } else {
            let appModelService: AppModelService = await GetModelService(context);
            if (appModelService?.app) {
                codelist = appModelService?.app.getAllPSAppCodeLists()?.find((item: IPSAppCodeList) => {
                    return item.codeName == tag;
                })
            }
        }
        let translate = await this.getTranslate(context);
        if (codelist && codelist.getPSCodeItems()) {
            let items: Array<any> = this.formatStaticItems(codelist.getPSCodeItems(), undefined, codelist.codeItemValueNumber, translate);
            return items;
        }
        return []
    }

    /**
     * @description 格式化静态代码表
     * @param {*} items 代码表集合
     * @param {string} [pValue] 父代码项值
     * @param {boolean} [codeItemValueNumber=false] 是否是数值项
     * @param {*} [translate] 多语言翻译
     * @return {*} 
     * @memberof CodeListService
     */
    private formatStaticItems(items: any, pValue?: string, codeItemValueNumber: boolean = false, translate?: any) {
        let targetArray: Array<any> = [];
        items.forEach((element: IPSCodeItem) => {
            const codelistItem = {
                label: element.text,
                text: element.text
            };
            Object.assign(codelistItem,element);
            Object.assign(codelistItem,{
                value: codeItemValueNumber ? Number(element.value) : element.value,
                id: element.value,
                color: element.color,
                codename: element.codeName
            });
            if(translate && element?.getTextPSLanguageRes?.()?.lanResTag){
                codelistItem.text = translate(element.getTextPSLanguageRes()?.lanResTag ,element.text)
                codelistItem.label = translate(element.getTextPSLanguageRes()?.lanResTag ,element.text)
            }
            if (element.data) {
                if ((element.data.indexOf("{") !== -1) && (element.data.indexOf("}") !== -1)) {
                    Object.assign(codelistItem, {
                        data: eval('(' + element.data + ')')
                    })
                } else {
                    Object.assign(codelistItem, {
                        data: element.data
                    })
                }
            }
            if (pValue) {
                Object.assign(codelistItem, {
                    pvalue: pValue,
                })
            }
            targetArray.push(codelistItem);
            if ((element.getPSCodeItems() || []).length > 0) {
                const children = this.formatStaticItems(element.getPSCodeItems(), element.value, codeItemValueNumber,translate);
                targetArray.push(...children);
            }
        });
        return targetArray;
    }

    /**
     * @description 获取预定义代码表
     * @param {string} tag 代码表标识
     * @param {*} [data]
     * @param {boolean} [isloading]
     * @return {*}  {Promise<any[]>}
     * @memberof CodeListService
     */
    private getPredefinedItems(tag: string, data?: any, isloading?: boolean): Promise<any[]> {
        return new Promise((resolve: any, reject: any) => {
            if (CodeListService.codelistCached.get(`${tag}`)) {
                let items: any = CodeListService.codelistCached.get(`${tag}`).items;
                if (items.length > 0) resolve(items);
            }
            const callback: Function = (tag: string, promise: Promise<any>) => {
                promise.then((res: any) => {
                    let result: any = res.data;
                    if (result.items && result.items.length > 0) {
                        CodeListService.codelistCached.set(`${tag}`, { items: result.items });
                        return resolve(result.items);
                    } else {
                        return resolve([]);
                    }
                }).catch((result: any) => {
                    return reject(result);
                })
            }
            // 加载中，UI又需要数据，解决连续加载同一代码表问题
            if (CodeListService.codelistCache.get(`${tag}`)) {
                callback(tag, CodeListService.codelistCache.get(`${tag}`));
            } else {
                let result:Promise<any> = this.entityService.getPredefinedCodelist(tag);
                CodeListService.codelistCache.set(`${tag}`, result);
                callback(tag, result);
            }
        })
    }

    /**
     * @description 获取动态代码表
     * @param {string} tag 代码表标识
     * @param {*} [context={}]
     * @param {*} [data]
     * @param {boolean} [isloading]
     * @return {*}  {Promise<any[]>}
     * @memberof CodeListService
     */
    private getItems(tag: string, context: any = {}, data?: any, isloading?: boolean): Promise<any[]> {
        let _this: any = this;
        if (context && context.srfsessionid) {
            delete context.srfsessionid;
        }
        return new Promise((resolve: any, reject: any) => {
            const codelist = new DynamicCodeListService();
            codelist.loaded(tag, context).then((flag: boolean) => {
                if (flag) {
                    if (Object.is(codelist.predefinedType, "RUNTIME") || Object.is(codelist.predefinedType, "OPERATOR")) {
                        this.getPredefinedItems(tag).then((res: any) => {
                            resolve(res);
                        })
                        return;
                    }
                    let isEnableCache: boolean = codelist.isEnableCache;
                    let cacheTimeout: any = codelist.cacheTimeout;
                    // 启用缓存
                    if (isEnableCache) {
                        const callback: Function = (context: any = {}, data: any = {}, tag: string, promise: Promise<any>) => {
                            const callbackKey: string = `${tag}`;
                            promise.then((result: any) => {
                                if (result.length > 0) {
                                    CodeListService.codelistCached.set(callbackKey, { items: result });
                                    CodeListService.codelistCache.delete(callbackKey);
                                    return resolve(result);
                                } else {
                                    return resolve([]);
                                }
                            }).catch((result: any) => {
                                return reject(result);
                            })
                        }
                        // 加载完成,从本地缓存获取
                        const key: string = `${tag}`;
                        if (CodeListService.codelistCached.get(key)) {
                            let items: any = CodeListService.codelistCached.get(key).items;
                            if (items.length > 0) {
                                if (new Date().getTime() <= codelist.getExpirationTime()) {
                                    return resolve(items);
                                }
                            }
                        }
                        if (codelist) {
                            // 加载中，UI又需要数据，解决连续加载同一代码表问题
                            if (CodeListService.codelistCache.get(key)) {
                                callback(context, data, tag, CodeListService.codelistCache.get(key));
                            } else {
                                let result: Promise<any> = codelist.getItems(context, data, isloading);
                                CodeListService.codelistCache.set(key, result);
                                codelist.setExpirationTime(new Date().getTime() + cacheTimeout);
                                callback(context, data, tag, result);
                            }
                        }
                    } else {
                        if (codelist) {
                            codelist.getItems(context, data, isloading).then((result: any) => {
                                resolve(result);
                            }).catch((error: any) => {
                                Promise.reject([]);
                            })
                        } else {
                            return Promise.reject([]);
                        }
                    }
                } else {
                    LogUtil.warn("获取代码表异常");
                    return Promise.reject([]);
                }
            })
        })
    }
}