import { IPSAppDataEntity } from '@ibiz/dynamic-model-api';
import { GetModelService, IAppStorageService, IAuthService } from 'ibz-core';

/**
 * 实体权限服务基类
 *
 * @export
 * @class AuthServiceBase
 */
export class AuthServiceBase implements IAuthService {

    /**
     * @description 应用存储对象
     * @private
     * @type {(IAppStorageService | null)}
     * @memberof AuthServiceBase
     */
    private $store: IAppStorageService | null;

    /**
     * @description 系统操作标识映射统一资源Map
     * @type {Map<string, any>}
     * @memberof AuthServiceBase
     */
    private sysOPPrivsMap: Map<string, any> = new Map();

    /**
     * @description 实体操作标识映射
     * @type {(Array<any> | null)}
     * @memberof AuthServiceBase
     */
    public deOPPrivsArray: Array<any> | null = [];

    /**
     * 
     * @description 值模式 [云实体数据访问控制方式] {0：无控制、 1：自控制、 2：附属主实体控制、 3：附属主实体控制（未映射自控） }
     * @type {( number | 0 | 1 | 2 | 3)} 
     * @memberof AuthServiceBase
     */
    /**
     * @description 实体数据访问控制方式
     * @type {number} 值模式 [云实体数据访问控制方式] {0：无控制、 1：自控制、 2：附属主实体控制、 3：附属主实体控制（未映射自控） }
     * @memberof AuthServiceBase
     */
    public dataAccCtrlMode: number = 0;

    /**
     * @description 应用上下文
     * @protected
     * @type {*}
     * @memberof AuthServiceBase
     */
    protected context: any;

    /**
     * @description 应用实体动态模型文件路径
     * @protected
     * @type {string}
     * @memberof AuthServiceBase
     */
    protected dynaModelFilePath: string = '';

    /**
     * @description 默认操作标识
     * @type {*}
     * @memberof AuthServiceBase
     */
    public defaultOPPrivs: any;

    /**
     * @description 应用实体模型对象
     * @protected
     * @type {IPSAppDataEntity}
     * @memberof AuthServiceBase
     */
    protected entityModel!: IPSAppDataEntity;

    /**
     * Creates an instance of AuthServiceBase.
     * 
     * @param {*} [opts={}]
     * @memberof AuthServiceBase
     */
    constructor(opts: any = {}) {
        this.$store = App.getStore();
        this.context = opts.context ? opts.context : {};
        this.registerSysOPPrivs();
    }

    /**
     * @description 获取系统操作标识映射统一资源Map
     * @return {*}  {Map<string, any>}
     * @memberof AuthServiceBase
     */
    public getSysOPPrivsMap(): Map<string, any> {
        return this.sysOPPrivsMap;
    }

    /**
     * @description 加载应用实体模型数据
     * @public
     * @memberof AuthServiceBase
     */
    public async loaded() {
        this.entityModel = await (await GetModelService(this.context)).getPSAppDataEntity(this.dynaModelFilePath);
        this.deOPPrivsArray = this.entityModel.getAllPSDEOPPrivs();
        this.dataAccCtrlMode = this.entityModel.dataAccCtrlMode;
    }

    /**
     * @description 获取应用存储对象
     * @return {*}  {(IAppStorageService | null)}
     * @memberof AuthServiceBase
     */
    public getStore(): IAppStorageService | null {
        return this.$store;
    }

    /**
     * @description 应用实体映射实体名称
     * @readonly
     * @memberof AuthServiceBase
     */
    get deName() {
        return (this.entityModel as any)?.getPSDEName() || '';
    }

    /**
     * @description 获取计算统一资源之后的系统操作标识
     * @return {*} 
     * @memberof AuthServiceBase
     */
    public getSysOPPrivs() {
        let copySysOPPrivs: any = JSON.parse(JSON.stringify(this.defaultOPPrivs));
        if (Object.keys(copySysOPPrivs).length === 0) return {};
        Object.keys(copySysOPPrivs).forEach((name: any) => {
            if (this.sysOPPrivsMap.get(name)) {
                copySysOPPrivs[name] = this.getResourcePermission(this.sysOPPrivsMap.get(name)) ? 1 : 0;
            }
        })
        return copySysOPPrivs;
    }

    /**
     * @description 注册系统操作标识统一资源
     * @memberof AuthServiceBase
     */
    public registerSysOPPrivs() { }

    /**
     * @description 根据当前数据获取实体操作标识
     * @param {string} activeKey 实体权限数据缓存标识
     * @param {string} dataaccaction 操作标识
     * @param {*} mainSateOPPrivs 传入数据主状态操作标识集合
     * @return {*}  {*}
     * @memberof AuthServiceBase
     */
    public getOPPrivs(activeKey: string, dataaccaction: string, mainSateOPPrivs: any): any {
        return null;
    }

    /**
     * @description 根据实体操作标识集合
     * @param {string} key
     * @return {*} 
     * @memberof AuthServiceBase
     */
    public getCurDeOPPrivs(key: string) {
        return (this.getStore() as any).get('authresource/getSrfappdeData')(key);
    }

    /**
     * @description 获取附属主实体权限
     * @param {*} tempOPPriv 操作标识对象
     * @param {string} dataaccaction 操作标识key
     * @return {*} 
     * @memberof AuthServiceBase
     */
    public getOPPsWithP(tempOPPriv: any, dataaccaction: string) {
        if (tempOPPriv && tempOPPriv['mapPSDEName'] && tempOPPriv['mapPSDEOPPrivName']) {
            if (this.getCurDeOPPrivs(`${this.context['srfparentdemapname']}-${this.context['srfparentkey']}`)) {
                return (this.getCurDeOPPrivs(`${this.context['srfparentdemapname']}-${this.context['srfparentkey']}`)[tempOPPriv['mapPSDEOPPrivName']] == 0) ? 0 : 1;
            } else {
                return 1;
            }
        } else {
            return 1;
        }
    }

    /**
     * @description 获取附属主实体控制（未映射自控）权限
     * @param {*} tempOPPriv 操作标识对象
     * @param {string} dataaccaction 操作标识key
     * @param {string} key 数据主键key
     * @return {*} 
     * @memberof AuthServiceBase
     */
    public getOPPsWithPAO(tempOPPriv: any, dataaccaction: string, key: string) {
        if (tempOPPriv && tempOPPriv['mapPSDEName'] && tempOPPriv['mapPSDEOPPrivName']) {
            const parentOPPrivs: any = this.getCurDeOPPrivs(`${this.context['srfparentdemapname']}-${this.context['srfparentkey']}`);
            if (parentOPPrivs && parentOPPrivs.hasOwnProperty(tempOPPriv['mapPSDEOPPrivName'])) {
                return (parentOPPrivs[tempOPPriv['mapPSDEOPPrivName']] == 0) ? 0 : 1;
            } else {
                if (this.getCurDeOPPrivs(`${this.deName}-${key}`)) {
                    return (this.getCurDeOPPrivs(`${this.deName}-${key}`)[dataaccaction] == 0) ? 0 : 1;
                } else {
                    return 1;
                }
            }
        } else {
            if (this.getCurDeOPPrivs(`${this.deName}-${key}`)) {
                return (this.getCurDeOPPrivs(`${this.deName}-${key}`)[dataaccaction] == 0) ? 0 : 1;
            } else {
                return 1;
            }
        }
    }

    /**
     * @description 获取实体级数据操作标识
     * @param {string} key 缓存主键
     * @param {string} dataaccaction 操作标识
     * @return {*} 
     * @memberof AuthServiceBase
     */
    public getActivedDeOPPrivs(key: string, dataaccaction: string) {
        let tempOPPriv: any;
        let tempOPPrivArray: any = this.deOPPrivsArray?.filter((item: any) => {
            return item.name === dataaccaction;
        })
        if (!tempOPPrivArray || (tempOPPrivArray && (tempOPPrivArray.length === 0))) {
            return 1;
        } else {
            if (this.context['srfparentdemapname']) {
                tempOPPriv = tempOPPrivArray.find((item: any) => {
                    return item.mapPSDEName === this.context['srfparentdemapname'];
                })
            } else {
                tempOPPriv = tempOPPrivArray[0];
            }
        }
        if (this.dataAccCtrlMode == 0) {
            return 1;
        } else if (this.dataAccCtrlMode == 1) {
            if (this.getCurDeOPPrivs(`${this.deName}-${key}`)) {
                return (this.getCurDeOPPrivs(`${this.deName}-${key}`)[dataaccaction] == 0) ? 0 : 1;
            } else {
                return 1;
            }
        } else if (this.dataAccCtrlMode == 2) {
            return this.getOPPsWithP(tempOPPriv, dataaccaction);
        } else {
            return this.getOPPsWithPAO(tempOPPriv, dataaccaction, key);
        }
    }

    /**
     * @description 根据统一资源标识获取统一资源权限
     * @param {*} tag 统一资源标识
     * @return {*}  {boolean}
     * @memberof AuthServiceBase
     */
    public getResourcePermission(tag: any): boolean {
      if (!App.getEnablePermissionValid()) {
        return true;
      }
      return App.getAuthResData(tag) ? true : false;
    }

}