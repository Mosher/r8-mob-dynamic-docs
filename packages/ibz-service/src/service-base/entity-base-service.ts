import { clone, equals, isEmpty, isNil, where } from 'ramda';
import { ascSort, createUUID, descSort, generateOrderValue, notNilEmpty } from 'qx-util';
import { IPSAppDataEntity, IPSAppDEField, IPSAppDELogic } from '@ibiz/dynamic-model-api';
import { GetModelService, IEntityBase, IEntityLocalDataService, IHttpResponse, IContext, IParam, Http, LogUtil, IEntityService, AppUILogicService, AppDeLogicService, Util } from 'ibz-core';
import { EntityCache, EntityDBService, HttpResponse, PSDEDQCondEngine, SearchFilter } from '../utils';


/**
 * 实体服务基类
 *
 * @export
 * @class EntityBaseService
 * @implements {IEntityLocalDataService<T>}
 * @template T
 */
export class EntityBaseService<T extends IEntityBase> implements IEntityLocalDataService<T>, IEntityService {
    /**
     * 统一日志输出工具类
     *
     * @protected
     * @memberof EntityBaseService
     */
    protected log = LogUtil;

    /**
     * 应用上下文
     *
     * @protected
     * @memberof EntityBaseService
     */
    protected context: any;

    /**
     * 实体全部属性
     *
     * @protected
     * @memberof EntityBase
     */
    protected get keys(): string[] {
        return [];
    }

    /**
     * 应用实体名称
     *
     * @protected
     * @memberof EntityBaseService
     */
    protected APPDENAME = '';

    /**
     * 应用实体名称复数形式
     *
     * @protected
     * @memberof EntityBaseService
     */
    protected APPDENAMEPLURAL = '';

    /**
     * 应用实体主键
     *
     * @protected
     * @memberof EntityBaseService
     */
    protected APPDEKEY = '';

    /**
     * 应用实体主文本
     *
     * @protected
     * @memberof EntityBaseService
     */
    protected APPDETEXT = '';

    /**
     * 系统名称
     *
     * @protected
     * @memberof EntityBaseService
     */
    protected SYSTEMNAME = '';

    /**
     * 应用名称
     *
     * @protected
     * @memberof EntityBaseService
     */
    protected APPNAME = '';

    /**
     * 当前实体服务支持快速搜索的属性
     *
     * @protected
     * @type {string[]}
     * @memberof EntityBaseService
     */
    protected quickSearchFields: string[] = [];

    /**
     * 根据关系，select查询时填充额外条件。
     *
     * @protected
     * @type {*}
     * @memberof EntityBaseService
     */
    protected selectContextParam: any = null;

    /**
     * 搜索条件引擎实例缓存
     *
     * @protected
     * @type {Map<string, PSDEDQCondEngine>}
     * @memberof EntityBaseService
     */
    protected condCache: Map<string, PSDEDQCondEngine> = new Map();

    /**
     * http请求服务
     *
     * @protected
     * @memberof EntityBaseService
     */
    protected http = Http.getInstance();

    /**
     * 数据缓存
     *
     * @protected
     * @type {EntityCache<T>}
     * @memberof EntityBaseService
     */
    protected cache: EntityCache<T> = new EntityCache();

    /**
     * 实体处理逻辑服务类
     *
     * @protected
     * @type {AppDeLogicService}
     * @memberof EntityBaseService
     */
    protected AppUILogicService: AppUILogicService = AppUILogicService.getInstance();

    /**
     * 应用实体动态模型文件路径
     *
     * @protected
     * @type {string}
     * @memberof EntityBaseService
     */
    protected dynaModelFilePath: string = '';

    /**
     * 应用实体模型
     *
     * @protected
     * @type {IPSAppDataEntity}
     * @memberof EntityBaseService
     */
    protected appDeModel!: IPSAppDataEntity;

    /**
     * 实体处理逻辑Map
     *
     * @protected
     * @type {Map<string,any>}
     * @memberof EntityBaseService
     */
    protected appDeLogicMap: Map<string, any> = new Map();

    /**
     * 实体属性处理逻辑Map
     *
     * @protected
     * @type {Map<string,any>}
     * @memberof EntityBaseService
     */
    protected appDeFieldLogicMap: Map<string, any> = new Map();

    /**
     * 存储模式
     *
     * @type {(0 | 1 | 3)} 无本地存储 | 仅本地存储 | 本地及远程存储
     */
    readonly storageMode: 0 | 1 | 3 = 0;

    /**
     * 是否为仅本地存储
     *
     * @type boolean
     */
    get isLocalStore(): boolean {
        return this.storageMode === 1;
    }
    /**
     * 数据库
     *
     * @type {DBService<T>}
     */
    db!: EntityDBService<T>;

    /**
     *  Creates an instance of EntityBaseService.
     *
     * @memberof EntityBaseService
     */
    constructor(opts?: any, dbName?: string, storageMode?: 0 | 1 | 3) {
        this.context = opts;
        if (notNilEmpty(storageMode)) {
            this.storageMode = storageMode!;
        }
        if (this.isLocalStore && dbName) {
            this.db = new EntityDBService<T>(
                dbName,
                (data: any): T => {
                    return this.newEntity(data);
                },
                (entity: T): any => {
                    return this.filterEntityData(entity);
                },
            );
        }
    }

    /**
     * 加载动态数据模型
     *
     * @protected
     * @param context 应用上下文
     * @param data 额外数据
     * @memberof EntityBaseService
     */
    public async loaded(context: IContext = {}, data: IParam = {}) {
        await this.initAppDeModel(context, data);
        this.initAppDELogicMap();
        this.initAppDEFieldLogicMap();
    }

    /**
     * 初始化应用实体模型数据
     *
     * @protected
     * @type {Map<string,any>}
     * @memberof EntityBaseService
     */
    protected async initAppDeModel(context: IContext = {}, data: IParam = {}) {
        if (!this.appDeModel && this.dynaModelFilePath) {
            this.appDeModel = await (await GetModelService(context)).getPSAppDataEntity(this.dynaModelFilePath);
        }
    }

    /**
     * 初始化实体处理逻辑Map
     *
     * @protected
     * @memberof EntityBaseService
     */
    protected initAppDELogicMap() {
        if (this.appDeLogicMap.size === 0 && this.appDeModel && this.appDeModel.getAllPSAppDELogics()) {
            this.appDeModel.getAllPSAppDELogics()?.forEach((item: IPSAppDELogic) => {
                this.appDeLogicMap.set(item.codeName, item);
            });
        }
    }

    /**
     * 初始化实体属性处理逻辑Map
     *
     * @protected
     * @memberof EntityBaseService
     */
    protected initAppDEFieldLogicMap() {
        const allAppDEFields: IPSAppDEField[] | null = this.appDeModel?.getAllPSAppDEFields();
        if (allAppDEFields && allAppDEFields.length > 0 && this.appDeFieldLogicMap.size === 0) {
            allAppDEFields.forEach((item: IPSAppDEField) => {
                if (item.getComputePSAppDEFLogic()) {
                    let computePSAppDEFLogics = this.appDeFieldLogicMap.get('ComputePSAppDEFLogic');
                    if (!computePSAppDEFLogics) {
                        computePSAppDEFLogics = [];
                        this.appDeFieldLogicMap.set('ComputePSAppDEFLogic', computePSAppDEFLogics);
                    }
                    computePSAppDEFLogics.push(item.getComputePSAppDEFLogic());
                }
                if (item.getOnChangePSAppDEFLogic()) {
                    let changePSAppDEFLogics = this.appDeFieldLogicMap.get('ChangePSAppDEFLogic');
                    if (!changePSAppDEFLogics) {
                        changePSAppDEFLogics = [];
                        this.appDeFieldLogicMap.set('ChangePSAppDEFLogic', changePSAppDEFLogics);
                    }
                    changePSAppDEFLogics.push(item.getOnChangePSAppDEFLogic());
                }
                if (item.getDefaultValuePSAppDEFLogic()) {
                    let defaultValuePSAppDEFLogics = this.appDeFieldLogicMap.get('DefaultValuePSAppDEFLogic');
                    if (!defaultValuePSAppDEFLogics) {
                        defaultValuePSAppDEFLogics = [];
                        this.appDeFieldLogicMap.set('DefaultValuePSAppDEFLogic', defaultValuePSAppDEFLogics);
                    }
                    defaultValuePSAppDEFLogics.push(item.getOnChangePSAppDEFLogic());
                }
            });
        }
    }

    /**
     * 执行实体处理逻辑
     *
     * @protected
     * @param {string} tag 逻辑标识
     * @param {*} _context 应用上下文
     * @param {*} _data 当前数据
     * @memberof EntityBaseService
     */
    public async executeAppDELogic(tag: string, _context: any, _data: any): Promise<IParam> {
        try {
            return await AppDeLogicService.getInstance().onExecute(this.appDeLogicMap.get(tag), _context, _data);
        } catch (error: any) {
            throw new Error(`执行实体处理逻辑异常，[逻辑错误]${error.message}`);
        }
    }

    /**
     * 执行实体属性处理逻辑
     *
     * @protected
     * @param {*} model 模型对象
     * @param {*} _context 应用上下文
     * @param {*} _data 当前数据
     * @memberof EntityBaseService
     */
    public async executeAppDEFieldLogic(model: any, _context: IContext, _data: IParam): Promise<IParam> {
        try {
            return await AppDeLogicService.getInstance().onExecute(model, _context, _data);
        } catch (error: any) {
            throw new Error(`执行实体属性处理逻辑异常，[逻辑错误]${error.message}`);
        }
    }

    /**
     * 执行实体行为之前
     *
     * @protected
     * @param {*} _context 应用上下文
     * @param {*} _data 当前数据
     * @param {string} methodName 方法名
     * @memberof EntityBaseService
     */
    public async beforeExecuteAction(_context: any, _data: any, methodName?: string): Promise<IParam> {
        // 执行实体属性值变更逻辑
        _data = await this.executeOnChangePSAppDEFLogic(_context, _data);
        if (methodName) {
            LogUtil.log(`执行实体属性值变更逻辑，[方法名称]：${methodName}，[处理后的数据]:`, _data);
        }
        return _data;
    }

    /**
     * 执行实体行为之后
     *
     * @protected
     * @param {*} _context 应用上下文
     * @param {*} _data 当前数据
     * @param {string} methodName 方法名
     * @memberof EntityBaseService
     */
    public async afterExecuteAction(_context: any, _data: any, methodName?: string): Promise<IParam> {
        // 执行实体属性值计算逻辑
        _data = await this.executeComputePSAppDEFLogic(_context, _data);
        if (methodName) {
            LogUtil.log(`执行实体属性值计算逻辑，[方法名称]：${methodName}，[处理后的数据]:`, _data);
        }
        return _data;
    }

    /**
     * 执行实体行为之后批处理（主要用于数据集处理）
     *
     * @protected
     * @param {*} _context 应用上下文
     * @param {*} dataSet 当前数据集合
     * @param {string} methodName 方法名
     * @memberof EntityBaseService
     */
    public async afterExecuteActionBatch(_context: any, dataSet: Array<any>, methodName?: string): Promise<IParam> {
        if (dataSet && dataSet.length > 0) {
            for (let i = 0; i < dataSet.length; i++) {
                dataSet[i] = await this.afterExecuteAction(_context, dataSet[i]);
            }
        }
        if (methodName) {
            LogUtil.log(`执行实体属性值计算逻辑，[方法名称]：${methodName}，[处理后的数据]:`, dataSet);
        }
        return dataSet;
    }

    /**
     * 执行实体属性值计算逻辑
     *
     * @protected
     * @param {*} _context 应用上下文
     * @param {*} _data 当前数据
     * @memberof EntityBaseService
     */
    protected async executeComputePSAppDEFLogic(_context: any, _data: any): Promise<IParam> {
        let computePSAppDEFLogics = this.appDeFieldLogicMap.get('ComputePSAppDEFLogic');
        if (computePSAppDEFLogics && computePSAppDEFLogics.length > 0) {
            for (let i = 0; i < computePSAppDEFLogics.length; i++) {
                _data = await this.executeAppDEFieldLogic(computePSAppDEFLogics[i], _context, _data);
            }
        }
        return _data;
    }

    /**
     * 执行实体属性默认值逻辑
     *
     * @protected
     * @param {*} _context 应用上下文
     * @param {*} _data 当前数据
     * @memberof EntityBaseService
     */
    protected async executeDefaultValuePSAppDEFLogic(_context: any, _data: any) { }

    /**
     * 执行实体属性值变更逻辑
     *
     * @protected
     * @param {*} _context 应用上下文
     * @param {*} _data 当前数据
     * @memberof EntityBaseService
     */
    protected async executeOnChangePSAppDEFLogic(_context: any, _data: any) {
        let changePSAppDEFLogics = this.appDeFieldLogicMap.get('ChangePSAppDEFLogic');
        if (changePSAppDEFLogics && changePSAppDEFLogics.length > 0) {
            for (let i = 0; i < changePSAppDEFLogics.length; i++) {
                _data = await this.executeAppDEFieldLogic(changePSAppDEFLogics[i], _context, _data);
            }
        }
        return _data;
    }

    /**
     * 处理响应错误
     *
     * @protected
     * @param {*} error 错误数据
     * @memberof EntityBaseService
     */
    protected handleResponseError(error: any): Promise<HttpResponse> {
        LogUtil.warn(error);
        return new Promise((resolve: any, reject: any) => {
            if (error.status && error.status !== 200) {
                reject(error);
            } else {
                const errorMessage = error?.message?.indexOf('[逻辑错误]') !== -1 ? error.message : '执行行为异常';
                resolve(
                    new HttpResponse(
                        { message: errorMessage },
                        {
                            ok: false,
                            status: 500,
                        },
                    ),
                );
            }
        });
    }

    /**
     * 过滤当前实体服务，标准接口数据
     *
     * @return {*}  {*}
     * @memberof EntityBaseService
     */
    filterEntityData(entity: T): T {
        const data: any = {};
        this.keys.forEach(key => {
            if (entity[key] !== void 0) {
                data[key] = entity[key];
            }
        });
        return data;
    }

    /**
     * 用于在未知实体时，通过服务构造实体。
     *
     * @param {T} _data
     * @return {*}  {T}
     */
    newEntity(_data: T): T {
        throw new Error('「newEntity」方法未实现');
    }

    /**
     * 新增本地数据
     *
     * @param {IContext} context
     * @param {T} entity
     * @return {*}  {(Promise<T | null>)}
     * @memberof EntityBaseService
     */
    async addLocal(context: IContext, entity: T): Promise<T | null> {
        if (this.isLocalStore) {
            return this.db.create(context, this.newEntity(entity));
        } else {
            return this.cache.add(context, this.newEntity(entity));
        }
    }

    /**
     * 新建本地数据
     *
     * @param {IContext} context
     * @param {T} entity
     * @return {*}  {(Promise<T | null>)}
     * @memberof EntityBaseService
     */
    async createLocal(context: IContext, entity: T): Promise<T | null> {
        entity.srfuf = 0;
        const data = await this.addLocal(context, entity);
        return data;
    }

    /**
     * 查找本地数据
     *
     * @param {IContext} context
     * @param {string} srfKey
     * @return {*}  {Promise<T | null>}
     * @memberof EntityBaseService
     */
    async getLocal(context: IContext, srfKey: string): Promise<T> {
        if (this.isLocalStore) {
            return this.db.get(context, srfKey);
        }
        return this.cache.get(context, srfKey)!;
    }

    /**
     * 更新本地数据
     *
     * @param {IContext} context
     * @param {T} entity
     * @return {*}  {Promise<T>}
     * @memberof EntityBaseService
     */
    async updateLocal(context: IContext, entity: T): Promise<T> {
        let data: any = null;
        if (this.isLocalStore) {
            data = await this.db.update(context, this.newEntity(entity));
        } else {
            data = this.cache.update(context, this.newEntity(entity));
        }
        return data;
    }

    /**
     * 删除本地数据
     *
     * @param {IContext} context
     * @param {string} srfKey
     * @return {*}  {Promise<T>}
     * @memberof EntityBaseService
     */
    async removeLocal(context: IContext, srfKey: string): Promise<T> {
        let data: any = null;
        if (this.isLocalStore) {
            data = await this.db.remove(context, srfKey);
        } else {
            data = this.cache.delete(context, srfKey);
        }
        return data;
    }

    /**
     * 本地获取默认值
     *
     * @param {IContext} _context
     * @param {T} _entity
     * @return {*}  {Promise<T>}
     * @memberof EntityBaseService
     */
    async getDraftLocal(_context: IContext, _entity: T): Promise<T> {
        throw new Error('「getDraftLocal」方法暂需重写实现');
    }

    /**
     * 查询本地数据，根据属性
     *
     * @param {IContext} context
     * @param {IParam} params 根据多实体属性查找，例：{ name: '张三', age: 18, parent: null }
     * @return {*}  {Promise<T[]>}
     * @memberof EntityBaseService
     */
    async selectLocal(context: IContext, params: IParam = {}): Promise<T[]> {
        let items: T[] = [];
        if (this.isLocalStore) {
            items = await this.db.search(context);
        } else {
            items = this.cache.getList(context);
        }
        items = ascSort(items, 'srfordervalue');
        if (notNilEmpty(params) || notNilEmpty(context)) {
            // 查询数据条件集
            const data: any = {};
            const nullData: any = {};
            const undefinedData: any = {};
            if (params.srfkey) {
                data.srfkey = equals(params.srfkey);
            }
            if (this.selectContextParam) {
                for (const key in this.selectContextParam) {
                    if (this.selectContextParam.hasOwnProperty(key)) {
                        const val = this.selectContextParam[key];
                        if (notNilEmpty(context[key])) {
                            data[val] = equals(context[key]);
                        }
                    }
                }
            }
            delete params.srfkey;
            for (const key in params) {
                if (params.hasOwnProperty(key)) {
                    const val = params[key];
                    if (val == null) {
                        nullData[key] = equals(null);
                        undefinedData[key] = equals(undefined);
                    } else {
                        data[key] = equals(val);
                    }
                }
            }
            if (!isEmpty(data)) {
                // 返回柯里化函数，用于判断数据是否满足要求
                const pred = where(data);
                const nullPred = where(nullData);
                const undefinedPred = where(undefinedData);
                items = items.filter(obj => {
                    if (isEmpty(nullData)) {
                        if (pred(obj)) {
                            return true;
                        }
                    } else {
                        if (pred(obj) && (nullPred(obj) || undefinedPred(obj))) {
                            return true;
                        }
                    }
                });
            }
        }
        const list = items.map(obj => clone(obj));
        LogUtil.warn('select', params, list);
        return list;
    }

    /**
     * 搜索本地数据
     *
     * @protected
     * @param {PSDEDQCondEngine | null} cond 查询实例
     * @param {SearchFilter} filter 过滤对象
     * @param {string[]} [queryParamKeys=this.quickSearchFields] 当前实体支持快速搜索的属性
     * @return {*}  {Promise<T[]>}
     * @memberof EntityBaseService
     */
    protected async searchLocal(
        cond: PSDEDQCondEngine | null,
        filter: SearchFilter,
        queryParamKeys: string[] = this.quickSearchFields,
    ): Promise<HttpResponse> {
        let list = [];
        // 走查询条件
        if (cond) {
            if (this.isLocalStore) {
                list = await this.db.search(filter.context);
            } else {
                list = this.cache.getList(filter.context);
            }
            if (list?.length > 0) {
                list = list.filter((obj: any) => cond.test(obj, filter));
            }
        } else {
            list = await this.selectLocal(filter.context);
            if (list?.length > 0) {
                // 识别query查询
                const condition = filter.data;
                if (condition != null && !isEmpty(condition)) {
                    if (queryParamKeys) {
                        list = list.filter(obj => {
                            const reg = new RegExp(filter.query);
                            for (let i = 0; i < queryParamKeys.length; i++) {
                                const key = queryParamKeys[i];
                                const val: string = obj[key];
                                if (reg.test(val)) {
                                    return true;
                                }
                            }
                        });
                    }
                }
            }
        }
        if (!isNil(filter.sortField) && !isEmpty(filter.sortField)) {
            if (filter.sortMode === 'DESC') {
                // 倒序
                list = descSort(list, filter.sortField);
            } else {
                // 正序
                list = ascSort(list, filter.sortField);
            }
        }
        const { page, size } = filter;
        const start = page * size;
        const end = (page + 1) * size;
        const items = list.slice(start, end).map((item: any) => clone(item));
        LogUtil.warn('search', cond, items);
        const headers = new Headers({
            'x-page': page.toString(),
            'x-per-page': size.toString(),
            'x-total': list.length.toString(),
        });
        return new HttpResponse(items, { headers });
    }

    /**
     * 批量建立临时数据[解包数据]
     *
     * @protected
     * @param {IContext} context
     * @param {T[]} items
     * @return {*}  {Promise<boolean>}
     * @memberof EntityBaseService
     */
     protected async setLocals(context: IContext, items: T[]): Promise<boolean> {
        if (items && items.length > 0) {
            for (let i = 0; i < items.length; i++) {
                const item = items[i];
                item.srfordervalue = generateOrderValue(i);
                await this.addLocal(context, item);;
            }
            return true;
        }
        return false;
    }

    /**
     * 批量获取临时数据[打包包数据]
     *
     * @param {IContext} context
     * @param {IParam} [params]
     * @param {string} [dataSet]
     * @return {*}  {(Promise<T[]>)}
     * @memberof EntityBaseService
     */
    async getLocals(context: IContext, params?: IParam, dataSet?: string): Promise<T[]> {
        let items: T[] = [];
        const _this: any = this;
        if (_this[dataSet!]) {
            const res = await _this[dataSet!](context, params);
            if (res.ok) {
                items = res.data;
            }
        } else {
            items = await this.selectLocal(context, params);
        }
        if (items && items.length > 0) {
            items = items.sort((a: any, b: any) => a.srfordervalue - b.srfordervalue);
            for (let i = 0; i < items.length; i++) {
                let item = items[i];
                const srfuf = item.srfuf;
                item = this.filterEntityData(item);
                // if (window.IBzDynamicConfig.enablePackageDataKey === false && srfuf == 0) {
                //     delete item[this.APPDEKEY];
                // }
                items[i] = item;
            }
        }
        return items;
    }

    Create(context: IContext, entity: T): Promise<IHttpResponse> {
        return this.CreateTemp(context, entity);
    }

    Remove(context: IContext, params?: IParam): Promise<IHttpResponse> {
        return this.RemoveTemp(context, params);
    }

    Update(context: IContext, entity: T): Promise<IHttpResponse> {
        return this.UpdateTemp(context, entity);
    }

    Get(context: IContext, params?: IParam): Promise<IHttpResponse> {
        return this.GetTemp(context, params);
    }

    GetDraft(context: IContext, params?: IParam): Promise<IHttpResponse> {
        return this.GetDraftTemp(context, params);
    }

    CreateBatch(context: IContext, params?: IParam): Promise<IHttpResponse> {
        return this.CreateBatchTemp(context, params);
    }

    checkData(context: IContext, srfkey: string): Promise<boolean> {
        return this.checkDataTemp(context, srfkey);
    }

    async FetchDefault(context: IContext, params?: IParam): Promise<IHttpResponse> {
        try {
            const res = await this.searchLocal(null, new SearchFilter(context, params));
            return res;
        } catch (err) {
            return new HttpResponse(err, {
                ok: false,
                status: 500,
            });
        }
    }

    async CreateTemp(context: IContext, entity: T): Promise<IHttpResponse> {
        try {
            const data = await this.createLocal(context, entity);
            return new HttpResponse(data);
        } catch (err) {
            return new HttpResponse(err, {
                ok: false,
                status: 500,
            });
        }
    }

    async GetDraftTemp(context: IContext, params?: any): Promise<IHttpResponse> {
        try {
            const data = await this.getDraftLocal(context, params);
            if (data) {
                return new HttpResponse(data);
            }
            return new HttpResponse(data, {
                ok: false,
                status: 500,
            });
        } catch (err) {
            return new HttpResponse(err, {
                ok: false,
                status: 500,
            });
        }
    }

    async RemoveTemp(context: IContext, params?: IParam): Promise<IHttpResponse> {
        try {
            let key = null;
            if (params) {
                key = params[this.APPDEKEY.toLowerCase()];
            }
            if (!key && context) {
                key = context[this.APPDENAME.toLowerCase()];
            }
            const data = await this.removeLocal(context, key);
            if (data) {
                return new HttpResponse(data);
            }
            return new HttpResponse(data, {
                ok: false,
                status: 500,
            });
        } catch (err) {
            return new HttpResponse(err, {
                ok: false,
                status: 500,
            });
        }
    }

    async UpdateTemp(context: IContext, entity: T): Promise<IHttpResponse> {
        try {
            const data = await this.updateLocal(context, entity);
            if (data) {
                return new HttpResponse(data);
            }
            return new HttpResponse(data, {
                ok: false,
                status: 500,
            });
        } catch (err) {
            return new HttpResponse(err, {
                ok: false,
                status: 500,
            });
        }
    }

    async GetTemp(context: IContext, params?: IParam): Promise<IHttpResponse> {
        try {
            let key = null;
            if (params) {
                key = params[this.APPDEKEY.toLowerCase()];
            }
            if (!key && context) {
                key = context[this.APPDENAME.toLowerCase()];
            }
            const data = await this.getLocal(context, key);
            if (data) {
                return new HttpResponse(data);
            }
            return new HttpResponse(data, {
                ok: false,
                status: 500,
            });
        } catch (err) {
            return new HttpResponse(err, {
                ok: false,
                status: 500,
            });
        }
    }

    /**
     * 批量新建本地数据
     *
     * @param {IContext} _context
     * @param {IParam} [_params]
     * @return {*}  {Promise<IHttpResponse>}
     * @memberof EntityBaseService
     */
    async CreateBatchTemp(_context: IContext, _params?: IParam): Promise<IHttpResponse> {
        throw new Error('「CreateBatchTemp」未实现');
    }

    /**
     * 拷贝一条数据
     *
     * @param {IContext} context
     * @param {T} data
     * @return {*}  {Promise<IHttpResponse>}
     * @memberof EntityBaseService
     */
    async CopyTemp(context: IContext, data: T): Promise<IHttpResponse> {
        const result = await this.GetTemp(context, data);
        if (result.ok) {
            const entity: T = result.data;
            entity.assign!(data);
            entity.srfkey = createUUID();
            entity.srfmajortext = `${entity.srfmajortext} copy`;
            const draftRes = await this.GetDraftTemp(
                {
                    ...context,
                    [this.APPDENAME.toLowerCase()]: context[this.APPDENAME.toLowerCase()] || entity.srfpkey,
                },
                entity,
            );
            if (draftRes.ok) {
                const res = await this.CreateTemp(context, draftRes.data);
                return res;
            }
        }
        return new HttpResponse(null, { status: 500, statusText: '拷贝数据失败失败' });
    }

    /**
     * FetchTempDefault接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EntityBaseService
     */
    public async FetchTempDefault(context: IContext = {}, data: IParam = {}, isloading?: boolean): Promise<any> {
        try {
            if (context && context.srfsessionkey) {
                const tempData = await this.getLocals(context);
                return { status: 200, data: tempData };
            } else {
                return { status: 200, data: [] };
            }
        } catch (error) {
            return { status: 200, data: [] };
        }
    }

    /**
     * 根据主键判断数据是否存在
     *
     * @param {IContext} context
     * @param {string} srfkey
     * @return {*}  {Promise<boolean>}
     * @memberof EntityBaseService
     */
    async checkDataTemp(context: IContext, srfkey: string): Promise<boolean> {
        if (this.isLocalStore) {
            return this.db.checkData(context, srfkey);
        }
        return this.cache.checkData(context, srfkey);
    }

    CreateTempMajor(context: IContext, params?: any): Promise<IHttpResponse> {
        return this.Create(context, params);
    }
    GetDraftTempMajor(context: IContext, params?: any): Promise<IHttpResponse> {
        return this.GetDraft(context, params);
    }
    GetTempMajor(context: IContext, params?: IParam): Promise<IHttpResponse> {
        return this.Get(context, params);
    }
    UpdateTempMajor(context: IContext, entity: T): Promise<IHttpResponse> {
        return this.Update(context, entity);
    }
    RemoveTempMajor(context: IContext, params?: IParam): Promise<IHttpResponse> {
        return this.Remove(context, params);
    }

    /**
     * ImportData接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EntityService
     */
    public async ImportData(context: IContext = {}, data: IParam = {}, isloading?: boolean): Promise<any> {
        let _data: Array<any> = [];
        if (data && data.importData) _data = data.importData;
        return Http.getInstance().post(
            `/${this.APPDENAMEPLURAL.toLowerCase()}/import?config=${data.name}`,
            _data,
            isloading,
        );
    }

    /**
     * createBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EntityService
     */
    public async createBatch(context: IContext = {}, data: IParam, isloading?: boolean): Promise<any> {
        if (context?.srfparentdename && context?.srfparentkey) {
          const mainDENamePlural = Util.srfpluralize(context.srfparentdename).toLowerCase();
          return Http.getInstance().post(`/${mainDENamePlural}/${context.srfparentkey}/${this.APPDENAMEPLURAL.toLowerCase()}/batch`, data, isloading);
        } else {
          return Http.getInstance().post(`/${this.APPDENAMEPLURAL.toLowerCase()}/batch`, data, isloading);
        } 
    }

    /**
     * saveBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EntityService
     */
    public async saveBatch(context: IContext = {}, data: IParam, isloading?: boolean): Promise<any> {
        return Http.getInstance().post(`/${this.APPDENAMEPLURAL.toLowerCase()}/savebatch`, data, isloading);
    }

    /**
     * updateBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EntityService
     */
    public async updateBatch(context: IContext = {}, data: IParam, isloading?: boolean): Promise<any> {
        return Http.getInstance().put(`/${this.APPDENAMEPLURAL.toLowerCase()}/batch`, data, isloading);
    }

    /**
     * removeBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EntityService
     */
    public async removeBatch(context: IContext = {}, data: IParam, isloading?: boolean): Promise<any> {
        return Http.getInstance().delete(
            `/${this.APPDENAMEPLURAL.toLowerCase()}/batch`,
            data[this.APPDEKEY],
            isloading
        );
    }

    /**
     * getDataInfo接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EntityService
     */
    public async getDataInfo(context: IContext = {}, data: IParam, isloading?: boolean): Promise<any> {
        if (context[this.APPDENAME.toLowerCase()]) {
            return this.Get(context, data);
        }
    }

    /**
     * getDynaModel(获取动态模型)接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EntityService
     */
    public async getDynaModel(context: IContext = {}, data: IParam, isloading?: boolean): Promise<any> {
        if (data && data.configType && data.targetType) {
            return Http.getInstance().get(`/configs/${data.configType}/${data.targetType}`);
        }
    }

    /**
     * setDynaModel(设置动态模型)接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EntityService
     */
    public async setDynaModel(context: IContext = {}, data: IParam, isloading?: boolean): Promise<any> {
        if (data && data.configType && data.targetType) {
            return Http.getInstance().put(`/configs/${data.configType}/${data.targetType}`, { model: data.model });
        }
    }

    /**
     * getDynaWorkflow接口方法(获取指定工作流版本信息)
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @param {*} [localdata]
     * @returns {Promise<any>}
     * @memberof EntityService
     */
    public async getDynaWorkflow(context: IContext = {}, data: IParam = {}, isloading?: boolean): Promise<any> {
        return Http.getInstance().get(
            `/wfcore/${context.srfsystemid}-app-${this.APPNAME.toLowerCase()}/${context.srfdynainstid
            }/${this.APPDENAME.toLowerCase()}/process-definitions`
        );
    }

    /**
     * 获取标准工作流版本信息
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @param {*} [localdata]
     * @returns {Promise<any>}
     * @memberof EntityService
     */
    public async getStandWorkflow(context: IContext = {}, data: IParam = {}, isloading?: boolean): Promise<any> {
        return Http.getInstance().get(
            `/wfcore/${context.srfsystemid
            }-app-${this.APPNAME.toLowerCase()}/${this.APPDENAME.toLowerCase()}/process-definitions2`
        );
    }

    /**
     * 获取副本工作流版本信息
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @param {*} [localdata]
     * @returns {Promise<any>}
     * @memberof EntityService
     */
    public async getCopyWorkflow(context: IContext = {}, data: IParam = {}, isloading?: boolean): Promise<any> {
        return Http.getInstance().get(
            `/wfcore/${context.srfsystemid}-app-${this.APPNAME.toLowerCase()}/${context.instTag}/${context.instTag2
            }/${this.APPDENAME.toLowerCase()}/process-definitions`,
        );
    }

    /**
     * WFStart接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @param {*} [localdata]
     * @returns {Promise<any>}
     * @memberof EntityService
     */
    public async WFStart(context: IContext = {}, data: IParam = {}, localdata?: any, isloading?: boolean): Promise<any> {
        const requestData: any = {};
        Object.assign(requestData, { activedata: data });
        Object.assign(requestData, localdata);
        return Http.getInstance().post(
            `/wfcore/${context.srfsystemid}-app-${this.APPNAME.toLowerCase()}/${this.APPDENAME.toLowerCase()}/${data[this.APPDEKEY]
            }/process-instances`,
            requestData,
            isloading,
        );
    }

    /**
     * WFClose接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EntityService
     */
    public async WFClose(context: IContext = {}, data: IParam = {}, isloading?: boolean): Promise<any> {
        return Http.getInstance().post(
            `/${this.APPDENAMEPLURAL.toLowerCase()}/${data[this.APPDEKEY]}/wfclose`,
            data,
            isloading,
        );
    }

    /**
     * WFMarkRead接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EntityService
     */
    public async WFMarkRead(context: IContext = {}, data: IParam = {}, isloading?: boolean): Promise<any> {
        return Http.getInstance().post(
            `/${this.APPDENAMEPLURAL.toLowerCase()}/${data[this.APPDEKEY]}/wfmarkread`,
            data,
            isloading,
        );
    }

    /**
     * WFGoto接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EntityService
     */
    public async WFGoto(context: IContext = {}, data: IParam = {}, isloading?: boolean): Promise<any> {
        return Http.getInstance().post(
            `/${this.APPDENAMEPLURAL.toLowerCase()}/${data[this.APPDEKEY]}/wfgoto`,
            data,
            isloading,
        );
    }

    /**
     * WFRollback接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EntityService
     */
    public async WFRollback(context: IContext = {}, data: IParam = {}, isloading?: boolean): Promise<any> {
        return Http.getInstance().post(
            `/${this.APPDENAMEPLURAL.toLowerCase()}/${data[this.APPDEKEY]}/wfrollback`,
            data,
            isloading,
        );
    }

    /**
     * WFRestart接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EntityService
     */
    public async WFRestart(context: IContext = {}, data: IParam = {}, isloading?: boolean): Promise<any> {
        return Http.getInstance().post(
            `/${this.APPDENAMEPLURAL.toLowerCase()}/${data[this.APPDEKEY]}/wfrestart`,
            data,
            isloading,
        );
    }

    /**
     * WFReassign接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EntityService
     */
    public async WFReassign(context: IContext = {}, data: IParam = {}, isloading?: boolean): Promise<any> {
        return Http.getInstance().post(
            `/${this.APPDENAMEPLURAL.toLowerCase()}/${data[this.APPDEKEY]}/wfreassign`,
            data,
            isloading,
        );
    }

    /**
     * WFGetWorkFlow接口方法(获取工作流定义)
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EntityService
     */
    public async WFGetWorkFlow(context: IContext = {}, data: IParam = {}, isloading?: boolean): Promise<any> {
        return Http.getInstance().get(
            `/wfcore/${context.srfsystemid
            }-app-${this.APPNAME.toLowerCase()}/${this.APPDENAME.toLowerCase()}/process-definitions`,
        );
    }

    /**
     * WFGetWFStep接口方法(根据系统实体查找当前适配的工作流模型步骤)
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EntityService
     */
    public async WFGetWFStep(context: IContext = {}, data: IParam = {}, isloading?: boolean): Promise<any> {
        return Http.getInstance().get(
            `/wfcore/${context.srfsystemid
            }-app-${this.APPNAME.toLowerCase()}/${this.APPDENAME.toLowerCase()}/process-definitions-nodes`,
        );
    }

    /**
     * GetWFLink接口方法(根据业务主键和当前步骤获取操作路径)
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EntityService
     */
    public async GetWFLink(context: IContext = {}, data: IParam = {}, isloading?: boolean): Promise<any> {
        return Http.getInstance().post(
            `/wfcore/${context.srfsystemid}-app-${this.APPNAME.toLowerCase()}/${this.APPDENAME.toLowerCase()}/${context[this.APPDENAME.toLowerCase()]
            }/usertasks/${data['taskDefinitionKey']}/ways`,
            { activedata: data.activedata },
        );
    }

    /**
     * GetWFLinks接口方法(根据当前步骤和任务获取批量操作路径)
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EntityService
     */
    public async getWFLinks(context: IContext = {}, data: IParam = {}, isloading?: boolean): Promise<any> {
        return Http.getInstance().get(
            `/wfcore/${context.srfsystemid
            }-app-${this.APPNAME.toLowerCase()}/${this.APPDENAME.toLowerCase()}/process-definitions/${data['processDefinitionKey']
            }/usertasks/${data['taskDefinitionKey']}/ways`,
        );
    }

    /**
     * getWFStep接口方法(根据当前步骤和任务获取工作流步骤数据（如：流程表单等）)
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EntityService
     */
    public async getWFStep(context: IContext = {}, data: IParam = {}, isloading?: boolean): Promise<any> {
        return Http.getInstance().get(
            `/wfcore/${context.srfsystemid
            }-app-${this.APPNAME.toLowerCase()}/${this.APPDENAME.toLowerCase()}/process-definitions/${data['processDefinitionKey']
            }/usertasks/${data['taskDefinitionKey']}`,
        );
    }

    /**
     * wfSubmitBatch接口方法(批量提交)
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EntityService
     */
    public async wfSubmitBatch(context: IContext = {}, data: IParam = {}, localdata: any, isloading?: boolean): Promise<any> {
        return Http.getInstance().post(
            `/wfcore/${context.srfsystemid
            }-app-${this.APPNAME.toLowerCase()}/${this.APPDENAME.toLowerCase()}/process-definitions/${localdata['processDefinitionKey']
            }/usertasks/${localdata['taskDefinitionKey']}/ways/${localdata['sequenceFlowId']}/submit`,
            data,
        );
    }

    /**
     * GetWFHistory接口方法(根据业务主键获取工作流程记录)
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EntityService
     */
    public async GetWFHistory(context: IContext = {}, data: IParam = {}, isloading?: boolean): Promise<any> {
        return Http.getInstance().get(
            `/wfcore/${context.srfsystemid}-app-${this.APPNAME.toLowerCase()}/${this.APPDENAME.toLowerCase()}/${context[this.APPDENAME.toLowerCase()]
            }/process-instances/alls/history`,
        );
    }

    /**
     * 前加签接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EntityService
     */
    public async BeforeSign(context: IContext = {}, data: IParam = {}, isloading?: boolean): Promise<any> {
        return Http.getInstance().post(
            `/wfcore/${context.srfsystemid}-app-${this.APPNAME.toLowerCase()}/${this.APPDENAME.toLowerCase()}/${context[this.APPDENAME.toLowerCase()]
            }/tasks/${data.taskId}/beforesign`,
            data,
        );
    }

    /**
     * 转办接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EntityService
     */
    public async TransFerTask(context: IContext = {}, data: IParam = {}, isloading?: boolean): Promise<any> {
        return Http.getInstance().post(
            `/wfcore/${context.srfsystemid}-app-${this.APPNAME.toLowerCase()}/${this.APPDENAME.toLowerCase()}/${context[this.APPDENAME.toLowerCase()]
            }/tasks/${data.taskId}/transfer`,
            data,
        );
    }

    /**
     * 回退
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EntityService
     */
    public async SendBack(context: IContext = {}, data: IParam = {}, isloading?: boolean): Promise<any> {
        return Http.getInstance().post(
            `/wfcore/${context.srfsystemid}-app-${this.APPNAME.toLowerCase()}/${this.APPDENAME.toLowerCase()}/${context[this.APPDENAME.toLowerCase()]
            }/tasks/${data.taskId}/sendback`,
            data,
        );
    }

    /**
     * 抄送
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EntityService
     */
    public async sendCopy(context: IContext = {}, data: IParam = {}, isloading?: boolean): Promise<any> {
        return Http.getInstance().post(
            `/wfcore/${context.srfsystemid}-app-${this.APPNAME.toLowerCase()}/${this.APPDENAME.toLowerCase()}/${context[this.APPDENAME.toLowerCase()]
            }/tasks/${data.taskId}/sendcopy`,
            data,
        );
    }

    /**
     * 将待办任务标记为已读
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EntityService
     */
    public async ReadTask(context: IContext = {}, data: IParam = {}, isloading?: boolean): Promise<any> {
        return Http.getInstance().post(
            `/wfcore/${context.srfsystemid}-app-${this.APPNAME.toLowerCase()}/${this.APPDENAME.toLowerCase()}/${context[this.APPDENAME.toLowerCase()]
            }/tasks/${data.taskId}/read`,
            { activedata: data },
        );
    }

    /**
     * WFSubmit接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @param {*} [localdata]
     * @returns {Promise<any>}
     * @memberof EntityService
     */
    public async WFSubmit(context: IContext = {}, data: IParam = {}, localdata?: any): Promise<any> {
        const requestData: any = {};
        if (data.viewparams) {
            delete data.viewparams;
        }
        Object.assign(requestData, { activedata: data });
        Object.assign(requestData, localdata);
        return Http.getInstance().post(
            `/wfcore/${context.srfsystemid}-app-${this.APPNAME.toLowerCase()}/${this.APPDENAME.toLowerCase()}/${data[this.APPDEKEY.toLowerCase()]
            }/tasks/${localdata['taskId']}`,
            requestData,
        );
    }

    /**
     * WFGetProxyData接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof EntityService
     */
    public async WFGetProxyData(context: IContext = {}, data: IParam = {}, isloading?: boolean): Promise<any> {
        return Http.getInstance().get(
            `/${this.APPDENAMEPLURAL.toLowerCase()}/${context[this.APPDENAME.toLowerCase()]}/wfgetproxydata`,
            data,
            isloading,
        );
    }

    /**
     * 测试数据是否在工作流中
     *
     * @param context
     * @param data
     * @param isloading
     */
    public async testDataInWF(context: IContext = {}, data: IParam = {}, isloading?: boolean): Promise<any> {
        if (!context.stateField || !context.stateValue) return false;
        if (context.stateValue == data[context.stateField]) {
            return true;
        }
        return false;
    }

    /**
     * 测试当前用户是否提交过工作流
     *
     * @param context
     * @param data
     * @param isloading
     */
    public async testUserWFSubmit(context: IContext = {}, data: IParam = {}, isloading?: boolean): Promise<any> {
        return true;
    }

    /**
     * 测试当前用户是否存在待办列表
     *
     * @param context
     * @param data
     * @param isloading
     */
    public async testUserExistWorklist(context: IContext = {}, data: IParam = {}, isloading?: boolean): Promise<any> {
        const requestData: any = {};
        Object.assign(requestData, { wfdata: data });
        return Http.getInstance().post(
            `/${this.APPDENAMEPLURAL.toLowerCase()}/${data[this.APPDENAME.toLowerCase()]}/testuserexistworklist`,
            requestData,
            isloading,
        );
    }

    /**
     * 获取所有应用数据
     *
     * @param context
     * @param data
     * @param isloading
     */
    public async getAllApp(context: IContext = {}, data: IParam = {}, isloading?: boolean): Promise<any> {
        return Http.getInstance().get(`/uaa/access-center/app-switcher/default`, data, isloading);
    }

    /**
     * 更新已选择的应用
     *
     * @param context
     * @param data
     * @param isloading
     */
    public async updateChooseApp(context: IContext = {}, data: IParam = {}, isloading?: boolean): Promise<any> {
        return Http.getInstance().put(`/uaa/access-center/app-switcher/default`, data, isloading);
    }

    /**
     * 修改密码
     *
     * @param context
     * @param data
     * @param isloading
     */
    public async changPassword(context: IContext = {}, data: IParam = {}, isloading?: boolean): Promise<any> {
        return Http.getInstance().post(`/v7/changepwd`, data, isloading);
    }

    /**
     * 获取数字字典
     *
     * @param tag
     * @param data
     * @param isloading
     */
    public async getPredefinedCodelist(tag: string, data: IParam = {}, isloading?: boolean): Promise<any> {
        return Http.getInstance().get(`/dictionarys/codelist/${tag}`, data, isloading);
    }
}
