import { IUtilService, LogUtil } from 'ibz-core';

/**
 * 功能服务基类
 *
 * @export
 * @class UtilServiceBase
 */
export class UtilServiceBase implements IUtilService {

    /**
     * @description 模型标识属性
     * @protected
     * @type {string}
     * @memberof UtilServiceBase
     */
    protected modelIdField: string = "";

    /**
     * @description 模型存储属性
     * @protected
     * @type {string}
     * @memberof UtilServiceBase
     */
    protected modelField: string = "";

    /**
     * @description 应用标识属性
     * @protected
     * @type {string}
     * @memberof UtilServiceBase
     */
    protected appIdField: string = "";

    /**
     * @description 用户标识属性
     * @protected
     * @type {string}
     * @memberof UtilServiceBase
     */
    protected userIdField: string = "";

    /**
     * @description 存储实体Name
     * @protected
     * @type {string}
     * @memberof UtilServiceBase
     */
    protected stoageEntityName:string ="";

    /**
     * @description 存储实体Id
     * @protected
     * @type {string}
     * @memberof UtilServiceBase
     */
    protected stoageEntityKey:string ="";

    /**
     * Creates an instance of UtilServiceBase.
     * @param {*} [opts={}]
     * @memberof UtilServiceBase
     */
    constructor(opts: any = {}) {
        this.initBasicParam();
    }

    /**
     * @description 初始化基础参数
     * @memberof UtilServiceBase
     */
    public initBasicParam(){
        LogUtil.log("UtilService初始化参数未实现");
    }
 
    /**
     * @description 处理请求参数
     * @protected
     * @param {*} context 应用上下文
     * @param {*} [data={}] 传入模型数据
     * @return {*} 
     * @memberof UtilServiceBase
     */
    protected handlePreParam(context:any,data:any ={}){
        let tempContext:Object = {};
        let tempData:Object = {};
        if(context.modelid){
            Object.defineProperty(tempContext,this.modelIdField,{
                value: context.modelid,
                writable: true,
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(tempContext,this.stoageEntityName,{
                value: context.modelid,
                writable: true,
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(tempData,this.modelIdField,{
                value: context.modelid,
                writable: true,
                enumerable: true,
                configurable: true
            });
            Object.defineProperty(tempData,this.stoageEntityKey,{
                value: context.modelid,
                writable: true,
                enumerable: true,
                configurable: true
            });
        }
        Object.defineProperty(tempData,this.modelField,{
            value: data,
            writable: true,
            enumerable: true,
            configurable: true
        });
        return {context:tempContext,data:tempData};
    }

    /**
     * @description 获取模型数据
     * @protected
     * @param {*} [context={}] 应用上下文
     * @param {*} [data={}] 传入模型数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<any>}
     * @memberof UtilServiceBase
     */
    public loadModelData(context: any = {},data: any = {}, isloading?: boolean):Promise<any>{
        return Promise.resolve(null);
    }

    /**
     * @description 保存模型数据
     * @protected
     * @param {*} [context={}] 应用上下文
     * @param {*} [data={}] 传入模型数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<any>}
     * @memberof UtilServiceBase
     */
    public saveModelData(context: any = {},data: any = {}, isloading?: boolean):Promise<any>{
        return Promise.resolve(null);
    }

   
}