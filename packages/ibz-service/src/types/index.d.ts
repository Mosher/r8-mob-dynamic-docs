import { IServiceApp } from '../interface';

declare global {
    interface Window {
        App: IServiceApp;
        ServiceCache: Map<string, any>;
    }
    const App: IServiceApp;
    const ServiceCache: Map<string, any>;
}
