import { PSModelSingleCondBase } from './ps-model-single-cond-base';

/**
 * 逻辑项
 *
 * @export
 * @class PSDEDQSingleCond
 * @extends {PSModelSingleCondBase}
 */
export class PSDEDQSingleCond extends PSModelSingleCondBase {}
