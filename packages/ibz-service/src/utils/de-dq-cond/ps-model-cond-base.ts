/**
 * 查询条件基类
 *
 * @export
 * @abstract
 * @class PSModelCondBase
 */
export abstract class PSModelCondBase {

    /**
     * @description 查询条件
     * @private
     * @type {(string | null)}
     * @memberof PSModelCondBase
     */
    private strCondOp: string | null = null;

    /**
     * @description 获取查询条件
     * @return {*}  {string}
     * @memberof PSModelCondBase
     */
    getCondOp(): string {
        return this.strCondOp!;
    }

    /**
     * @description 设置查询条件
     * @param {string} strCondOp 查询条件
     * @memberof PSModelCondBase
     */
    setCondOp(strCondOp: string): void {
        this.strCondOp = strCondOp;
    }

    /**
     * @description 解析
     * @abstract
     * @param {any[]} array 数组
     * @memberof PSModelCondBase
     */
    abstract parse(array: any[]): void;
}
