import { PSModelGroupCondBase } from './ps-model-group-cond-base';

/**
 * 逻辑组
 *
 * @export
 * @class PSDEDQGroupCond
 * @extends {PSModelGroupCondBase}
 */
export class PSDEDQGroupCond extends PSModelGroupCondBase {}
