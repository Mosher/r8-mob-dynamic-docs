import { PSModelCondBase } from './ps-model-cond-base';

/**
 * 逻辑项
 *
 * @export
 * @class PSModelSingleCondBase
 * @extends {PSModelCondBase}
 */
export class PSModelSingleCondBase extends PSModelCondBase {

    /**
     * @description 值
     * @private
     * @type {string}
     * @memberof PSModelSingleCondBase
     */
    private strValue?: string;
    
    /**
     * @description 值类型
     * @private
     * @type {string}
     * @memberof PSModelSingleCondBase
     */
    private strValueType?: string;
    
    /**
     * @description 参数
     * @private
     * @type {string}
     * @memberof PSModelSingleCondBase
     */
    private strParam?: string;
   
    /**
     * @description 参数类型
     * @private
     * @type {string}
     * @memberof PSModelSingleCondBase
     */
    private strParamType?: string;

    /**
     * @description 解析条件
     * @param {any[]} arr 数组
     * @memberof PSModelSingleCondBase
     */
    parse(arr: any[]): void {
        const nCount = arr.length;
        // 是否为条件起始
        let bOpStart = true;
        let bParamStart = false;
        let bValueStart = false;
        for (let i = 0; i < nCount; i++) {
            // 设置判断条件
            if (bOpStart) {
                const strText = arr[i].toString();
                this.setCondOp(strText);
                bOpStart = false;
                bParamStart = true;
                continue;
            }
            // 设置参数标识
            if (bParamStart) {
                const strText = arr[i].toString();
                this.setParam(strText);
                bParamStart = false;
                bValueStart = true;
                continue;
            }
            // 设置值
            if (bValueStart) {
                //需要是对象
                const obj = arr[i];
                if (obj instanceof Object && !(obj instanceof Array)) {
                    // 设置值类型
                    if (obj.type != null) {
                        this.setValueType(obj.type.toString());
                    }
                    // 设置条件值
                    if (obj.value != null) {
                        this.setValue(obj.value.toString());
                    }
                } else {
                    this.setValue(obj.toString());
                }
                break;
            }
        }
    }

    /**
     * @description 获取值类型
     * @return {*}  {string}
     * @memberof PSModelSingleCondBase
     */
    getValueType(): string {
        return this.strValueType!;
    }

    /**
     * @description 设置值类型
     * @param {string} strValueType 值类型
     * @memberof PSModelSingleCondBase
     */
    setValueType(strValueType: string): void {
        this.strValueType = strValueType;
    }

    /**
     * @description 获取值
     * @return {*}  {string}
     * @memberof PSModelSingleCondBase
     */
    getValue(): string {
        return this.strValue!;
    }

    /**
     * @description 设置值
     * @param {string} strValue 值
     * @memberof PSModelSingleCondBase
     */
    setValue(strValue: string): void {
        this.strValue = strValue;
    }

    /**
     * @description 获取参数类型
     * @return {*}  {string}
     * @memberof PSModelSingleCondBase
     */
    getParamType(): string {
        return this.strParamType!;
    }

    /**
     * @description 设置参数类型
     * @param {string} strParamType 参数类型
     * @memberof PSModelSingleCondBase
     */
    setParamType(strParamType: string): void {
        this.strParamType = strParamType;
    }

    /**
     * @description 获取参数
     * @return {*}  {string}
     * @memberof PSModelSingleCondBase
     */
    getParam(): string {
        return this.strParam!;
    }

    /**
     * @description 设置参数
     * @param {string} strParam 参数
     * @memberof PSModelSingleCondBase
     */
    setParam(strParam: string): void {
        this.strParam = strParam;
    }
}
