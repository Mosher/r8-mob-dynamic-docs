import Dexie, { Version } from 'dexie';

/**
 * 库实例基类
 *
 * @export
 * @class DBBaseService
 * @extends {Dexie}
 */
export class DBBaseService extends Dexie {

    /**
     * @description 
     * @protected
     * @type {Version}
     * @memberof DBBaseService
     */
    protected v: Version;

    /**
     * Creates an instance of DBBaseService.
     * @memberof DBBaseService
     */
    constructor() {
        super('DefaultDB');
        this.v = this.version(1);
        this.init();
    }

    /**
     * @description 数据库初始化
     * @protected
     * @memberof DBBaseService
     */
    protected init(): void {
        throw new Error('数据库实例化需要重写!');
    }
}
