import { DBBaseService } from '../db-base-service/db-base.service';

/**
 * 库实例
 *
 * @export
 * @class DBService
 * @extends {DBBaseService}
 */
export class DBService extends DBBaseService {
  
    /**
     * @description 唯一实例
     * @protected
     * @static
     * @type {*}
     * @memberof DBService
     */
    protected static instance: any;

    /**
     * Creates an instance of DBService.
     * @memberof DBService
     */
    constructor() {
        if (DBService.instance) {
            return DBService.instance;
        }
        super();
    }

    /**
     * @description 数据库初始化
     * @protected
     * @memberof DBService
     */
    protected init(): void {
        this.v = this.v.stores({
        });
    }

    /**
     * @description 获取实例
     * @static
     * @return {*}  {DBService}
     * @memberof DBService
     */
    static getInstance(): DBService {
        if (!this.instance) {
            this.instance = new DBService();
        }
        return this.instance;
    }
}
