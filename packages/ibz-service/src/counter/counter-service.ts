import { IPSAppCounter } from '@ibiz/dynamic-model-api';
import { ICounterService, LogUtil } from 'ibz-core';

/**
 * 计数器服务
 * 
 * @export
 * @class CounterService
 */
export class CounterService implements ICounterService {

  /**
   * @description 计时器间隔时间
   * @protected
   * @type {number}
   * @memberof CounterService
   */
  protected delayTime: number = 6000;

  /**
   * @description 计时器对象
   * @protected
   * @type {*}
   * @memberof CounterService
   */
  protected timer: any = null;

  /**
   * @description 应用上下文
   * @protected
   * @type {*}
   * @memberof CounterService
   */
  protected context: any = {};

  /**
   * @description 视图参数
   * @protected
   * @type {*}
   * @memberof CounterService
   */
  protected viewParam: any = {};

  /**
   * @description 获取数据行为
   * @protected
   * @type {string}
   * @memberof CounterService
   */
  protected getAction: string = '';

  /**
   * @description 实体服务
   * @private
   * @type {*}
   * @memberof CounterService
   */
  private entityService: any;

  /**
   * @description 计数器数据
   * @private
   * @type {*}
   * @memberof CounterService
   */
  private counterData: any;

  /**
   * @description 计数器服务加载
   * @param {IPSAppCounter} instance 计数器实例对象
   * @param {{ navContext?: any, navViewParam?: any }} [params]
   * @return {*}  {Promise<void>}
   * @memberof CounterService
   */
  public async loaded(instance: IPSAppCounter, params?: { navContext?: any, navViewParam?: any }): Promise<void> {
    this.context = params?.navContext ? params.navContext : {};
    this.viewParam = params?.navViewParam ? params?.navContext : {};
    await this.initOptions(instance);
    await this.excuteRefreshData();
  }

  /**
   * @description 初始化配置
   * @param {IPSAppCounter} instance
   * @memberof CounterService
   */
  public async initOptions(instance: IPSAppCounter) {
    this.delayTime = instance?.timer || 6000;
    const appDataEntity = instance?.getPSAppDataEntity();
    if (appDataEntity) {
      this.entityService = await App.getEntityService().getService(this.context, appDataEntity.codeName.toLowerCase());
    }
    this.getAction = instance.getGetPSAppDEAction?.()?.codeName || '';
  }

  /**
   * @description 执行刷新数据
   * @memberof CounterService
   */
  public async excuteRefreshData(){
    await this.fetchCounterData();
    this.destroyCounter();
    this.timer = setInterval(() => {
      this.fetchCounterData();
    }, this.delayTime);
  }

  /**
   * @description 获取计数器数据
   * @memberof CounterService
   */
  public async fetchCounterData() {
    if (this.entityService && this.getAction && this.entityService[this.getAction] && this.entityService[this.getAction] instanceof Function) {
      try {
        let result = await this.entityService[this.getAction](this.context, this.viewParam);
        if (result && result.data) {
          this.counterData = result.data;
        }
      } catch (error: any) {
        LogUtil.error(error);
      }
    }
  }

  /**
   * @description 刷新数据
   * @param {*} context 应用上下文
   * @param {*} data 参数
   * @return {*}  {Promise<void>}
   * @memberof CounterService
   */
  public async refreshCounterData(context:any, data:any): Promise<void> {
    this.context = context;
    this.viewParam = data;
    await this.excuteRefreshData();
  }

  /**
   * @description 销毁计数器
   * @memberof CounterService
   */
  public destroyCounter(): void{
    if(this.timer) clearInterval(this.timer);
  }

}