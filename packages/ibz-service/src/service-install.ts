import { IServiceApp } from "./interface";

/**
 * 安装插件
 *
 * @param config 配置
 */
export const ServiceInstall = function (App: IServiceApp): void {
    window.App = App;
    window.ServiceCache = new Map();
};
