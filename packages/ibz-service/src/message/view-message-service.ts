import { IPSAppDEDataSetViewMsg, IPSAppMsgTempl, IPSAppViewMsg, IPSAppViewMsgGroup, IPSAppViewMsgGroupDetail } from '@ibiz/dynamic-model-api';
import { IParam, IViewMessageService, Util } from 'ibz-core';
import { DynamicViewMessageService } from './dynamic-view-message-service';

/**
 * 视图消息服务
 *
 * @export
 * @class ViewMessageService
 */
export class ViewMessageService implements IViewMessageService {

    /**
     * @description 视图消息集合实例对象
     * @protected
     * @type {IPSAppViewMsgGroup}
     * @memberof ViewMessageService
     */
    protected viewMessageGroup!: IPSAppViewMsgGroup;

    /**
     * @description 视图消息集合
     * @protected
     * @type {any[]}
     * @memberof ViewMessageService
     */
    protected viewMessageDetails: any[] = [];

    /**
     * @description 应用上下文
     * @type {*}
     * @memberof ViewMessageService
     */
    public context: any;

    /**
     * @description 视图参数
     * @type {*}
     * @memberof ViewMessageService
     */
    public viewParam: any;

    /**
     * @description 视图消息缓存(加载中)
     * @static
     * @type {Map<string,any>}
     * @memberof ViewMessageService
     */
    public static messageCache:Map<string,any> = new Map();

    /**
     * @description 视图消息缓存(已完成)
     * @static
     * @type {Map<string,any>}
     * @memberof ViewMessageService
     */
    public static messageCached:Map<string,any> = new Map();

    /**
     * @description 获取视图消息实例
     * @param {string} [position] 视图消息位置
     * @return {*}  {IParam[]}
     * @memberof ViewMessageService
     */
    public getViewMsgDetails(position?: string): IParam[] {
        if (!position) {
            return this.viewMessageDetails;
        }
        if (position == 'TOP') {
            return this.viewMessageDetails.filter((detail: any) => {
                return detail.position == 'TOP' || detail.position == 'POPUP';
            });
        } else {
            return this.viewMessageDetails.filter((detail: any) => {
                return detail.position == position;
            });
        }
    }

    /**
     * @description 初始化基础参数
     * @param {IPSAppViewMsgGroup} opts 视图消息组实例对象
     * @param {*} [context] 应用上下文
     * @param {*} [viewParam] 视图参数
     * @return {*}  {Promise<void>}
     * @memberof ViewMessageService
     */
    public async loaded(opts: IPSAppViewMsgGroup, context?: any, viewParam?: any): Promise<void> {
        this.viewMessageGroup = opts;
        this.context = context;
        this.viewParam = viewParam;
        await this.initViewMsgDetails();
    }

    /**
     * @description 初始化视图消息集合
     * @return {*} 
     * @memberof ViewMessageService
     */
    public async initViewMsgDetails() {
        const viewMsgGroupDetails: Array<IPSAppViewMsgGroupDetail> = this.viewMessageGroup?.getPSAppViewMsgGroupDetails?.() || [];
        if (viewMsgGroupDetails.length == 0) {
            return [];
        }
        for (let i = 0; i<viewMsgGroupDetails.length; i++) {
            const viewMsg = viewMsgGroupDetails[i].getPSAppViewMsg() as IPSAppViewMsg;
            let items: any[] = [];
            if (viewMsg.dynamicMode == 0) { //  动态模式为 静态
                items = await this.initStaticViewMessage(viewMsg);
            } else if (viewMsg.dynamicMode == 1) {  //  动态模式为 实体数据集
                items = await this.initDynamicViewMessage(viewMsg as IPSAppDEDataSetViewMsg, this.context, this.viewParam);
            }
            this.viewMessageDetails.push(...items);
        }
    }

    /**
     * @description 初始化动态模式（静态）类型视图消息
     * @param {IPSAppViewMsg} detail 动态模式（静态）类型视图消息实例
     * @return {*}  {Promise<any[]>}
     * @memberof ViewMessageService
     */
    public async initStaticViewMessage(detail: IPSAppViewMsg): Promise<any[]> {
        const _this: any = this;
        return new Promise((resolve: any, reject: any) => {
            let viewMessage: any = {
                position: detail.position || 'TOP',
                name: detail.name,
                codeName: detail.codeName?.toLowerCase(),
                type: detail.messageType,
                title: detail.title,
                titleLanResTag: detail.titleLanResTag || detail.getTitlePSLanguageRes()?.lanResTag,
                content: detail.message,
                removeMode: detail.removeMode,
                enableRemove: detail.enableRemove,
            };
            _this.translateMessageTemp(viewMessage, detail);
            resolve([viewMessage]);
        })
        
    }

    /**
     * @description 转化动态模式（静态）类型视图消息模板标题和内容
     * @param {*} target 返回目标数据
     * @param {IPSAppViewMsg} detail 动态模式（静态）视图消息实例
     * @return {*} 
     * @memberof ViewMessageService
     */
    public translateMessageTemp(target: any, detail: IPSAppViewMsg) {
        const format = (content: any) => {
            if (!Util.isExistAndNotEmpty(content)) {
                return content;
            }
            const params: any[] = content.match(/\${(.+?)\}/g) || [];
            if (params.length > 0) {
                params.forEach((param: any) => {
                    let _param: any = param.substring(2, param.length - 1).toLowerCase();
                    const arr: string[] = _param.split('.');
                    if (arr.length == 2) {
                        switch (arr[0]) {
                            case 'context':
                                content = this.context ? content.replace(param, this.context[arr[1]]) : content;
                                break;
                            case 'viewparams':
                                content = this.viewParam ? content.replace(param, this.viewParam[arr[1]]) : content;
                                break;
                        }
                    }
                })
            }
            return content;
        }
        const appMsgTempl: IPSAppMsgTempl = detail.getPSAppMsgTempl?.() as IPSAppMsgTempl;
        if (!appMsgTempl) {
            return;
        }
        Object.assign(target, {
            messageType: appMsgTempl.contentType == 'HTML' ? 'HTML' : 'TEXT',
            title: format(appMsgTempl.subject),
            titleLanResTag: appMsgTempl.getSubPSLanguageRes()?.lanResTag,
            content: format(appMsgTempl.content)
        });
    }

    /**
     * 
     *
     * @param {any} tag 
     * @param {any} messageService 消息服务
     * @param {string} context
     * @returns {Promise<any[]>}
     * @memberof ViewMessageService
     */
    /**
     * @description 初始化动态模式（实体数据集合）类型视图消息
     * @param {IPSAppDEDataSetViewMsg} detail 视图消息实例对象
     * @param {*} [context={}] 应用上下文
     * @param {*} [data={}] 视图参数
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<any[]>}
     * @memberof ViewMessageService
     */
    public async initDynamicViewMessage(detail: IPSAppDEDataSetViewMsg, context: any = {}, data: any = {}, isloading?: boolean): Promise<any[]> {
        if(context && context.srfsessionid){
            delete context.srfsessionid;
        }
        //  动态模式（实体数据集合）类型视图消息服务
        const dynamicViewMsgService: DynamicViewMessageService = new DynamicViewMessageService(detail);
        return new Promise((resolve:any,reject:any) =>{
            let isEnableCache: boolean = (detail as any).enableCache;
            let cacheTimeout :any = (detail as any).cacheTimeout;
            const tag: any = detail.codeName;
            // 启用缓存
            if(isEnableCache){
                const callback: Function = (context:any ={}, data:any ={}, tag:string, promise:Promise<any>) =>{
                    const callbackKey:string = `${tag}`;
                    promise.then((result:any) =>{
                        if(result.length > 0){
                            ViewMessageService.messageCached.set(callbackKey,{items:result});
                            ViewMessageService.messageCache.delete(callbackKey);
                            return resolve(result);
                        }else{
                            return resolve([]);
                        }
                    }).catch((result:any) =>{
                        return reject(result);
                    })
                }
                const key:string = `${tag}`;
                // 加载完成,从本地缓存获取
                if(ViewMessageService.messageCached.get(key)){
                    let items:any = ViewMessageService.messageCached.get(key).items;
                    if(items.length > 0){
                        if(new Date().getTime() <= dynamicViewMsgService.getExpirationTime()){
                            return resolve(items); 
                        }
                    }
                }
                // 加载中，UI又需要数据，解决连续加载同一消息服务问题
                if (dynamicViewMsgService) {
                    if(ViewMessageService.messageCache.get(key)){
                        callback(context, data, tag, ViewMessageService.messageCache.get(key));
                    }else{
                        let result: Promise<any> = dynamicViewMsgService.getItems(context, data, isloading);
                        ViewMessageService.messageCache.set(key,result);
                        dynamicViewMsgService.setExpirationTime(new Date().getTime() + cacheTimeout);
                        callback(context, data, tag, result);
                    }
                }
            }else{
                if (dynamicViewMsgService) {
                    dynamicViewMsgService.getItems(context,data,isloading).then((result:any) =>{
                        resolve(result);
                    }).catch((error:any) =>{
                        Promise.reject([]);
                    })
                } else {
                    return Promise.reject([]);
                } 
            }
        })

    }

}