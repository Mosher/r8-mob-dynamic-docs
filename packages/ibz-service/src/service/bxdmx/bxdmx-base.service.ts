// import { CodeListService } from '../app/codelist-service';
import { EntityBaseService } from '../../service-base';
import { IContext } from 'ibz-core';
import { HttpResponse } from '../../utils/http-response';
import { IBXDMX } from './interface/ibxdmx';
import { BXDMX } from './bxdmx';
import keys from './bxdmx-keys';
import { BXDMXDTOHelp } from './bxdmxdto-help';


/**
 * 报销单明细服务对象基类
 *
 * @export
 * @class BXDMXBaseService
 * @extends {EntityBaseService}
 */
export class BXDMXBaseService extends EntityBaseService<IBXDMX> {
    protected get keys(): string[] {
        return keys;
    }
    protected SYSTEMNAME = 'StudioPreview';
    protected APPNAME = 'TestMob';
    protected APPDENAME = 'BXDMX';
    protected APPDENAMEPLURAL = 'BXDMXes';
    protected dynaModelFilePath:string = 'PSSYSAPPS/TestMob/PSAPPDATAENTITIES/BXDMX.json';
    protected APPDEKEY = 'bxdmxid';
    protected APPDETEXT = 'bxdmxname';
    protected quickSearchFields = ['bxdmxname',];
    protected selectContextParam = {
    };

    constructor(opts?: any) {
        super(opts, 'BXDMX');
    }

    newEntity(data: IBXDMX): BXDMX {
        return new BXDMX(data);
    }

}
