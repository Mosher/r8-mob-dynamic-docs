import { IEntityLocalDataService, IContext, IParam } from 'ibz-core';
import { IBXDMX } from './interface/ibxdmx';
/**
 * BXDMXDTOdto辅助类
 *
 * @export
 * @class BXDMXDTOHelp
 */
export class BXDMXDTOHelp {

    /**
     * 获取数据服务
     *
     * @param {IContext} context 应用上下文对象
     * @returns {*}
     * @memberof BXDMXDTOHelp
     */
    public static async getService(context: IContext): Promise<IEntityLocalDataService<IBXDMX>> {
        return await App.getEntityService().getService(context, 'bxdmx') as IEntityLocalDataService<IBXDMX>;
    }

    /**
     * DTO转化成数据对象
     *
     * @param {IContext} context 应用上下文对象
     * @param {IParam} source dto对象
     * @returns {*}
     * @memberof BXDMXDTOHelp
     */
    public static async ToDataObj(context: IContext, source: IParam) {
        const _data: any = {};
        // 报销单标识
        _data.bxdid = source.bxdid;
        // 报销单明细标识
        _data.bxdmxid = source.bxdmxid;
        // 报销单明细名称
        _data.bxdmxname = source.bxdmxname;
        // 报销单名称
        _data.bxdname = source.bxdname;
        // 建立时间
        _data.createdate = source.createdate;
        // 建立人
        _data.createman = source.createman;
        // 启用
        _data.enable = source.enable;
        // 备注
        _data.memo = source.memo;
        // 排序值
        _data.ordervalue = source.ordervalue;
        // 更新时间
        _data.updatedate = source.updatedate;
        // 更新人
        _data.updateman = source.updateman;
        return _data;
    }

    /**
     * 转化数组(dto转化成数据对象)
     *
     * @param {IContext} context 应用上下文对象
     * @param {any[]} data 数据对象
     * @returns {any[]}
     * @memberof BXDMXDTOHelp
     */
    public static async ToDataObjArray(context: IContext, data: any[]) {
        const _data: any[] = [];
        if (data && Array.isArray(data) && data.length > 0) {
            for (let i = 0; i < data.length; i++) {
                const tempdata = await this.ToDataObj(context, data[i]);
                _data.push(tempdata);
            }
        }
        return _data;
    }

    /**
     * 数据对象转化成DTO
     *
     * @param {IContext} context 应用上下文对象
     * @param {*} source 数据对象
     * @returns {*}
     * @memberof BXDMXDTOHelp
     */
    public static async ToDto(context: IContext, source: IParam) {
        const _data: any = {};
        // 报销单标识
        _data.bxdid = source.bxdid;
        // 报销单明细标识
        _data.bxdmxid = source.bxdmxid;
        // 报销单明细名称
        _data.bxdmxname = source.bxdmxname;
        // 报销单名称
        _data.bxdname = source.bxdname;
        // 建立时间
        _data.createdate = source.createdate;
        // 建立人
        _data.createman = source.createman;
        // 启用
        _data.enable = source.enable;
        // 备注
        _data.memo = source.memo;
        // 排序值
        _data.ordervalue = source.ordervalue;
        // 更新时间
        _data.updatedate = source.updatedate;
        // 更新人
        _data.updateman = source.updateman;
        return _data;
    }

    /**
     * 转化数组(数据对象转化成dto)
     *
     * @param {IContext} context 应用上下文对象
     * @param {any[]} data 
     * @returns {any[]}
     * @memberof BXDMXDTOHelp
     */
    public static async ToDtoArray(context: IContext, data: any[]) {
        const _data: any[] = [];
        if (data && Array.isArray(data) && data.length > 0) {
            for (let i = 0; i < data.length; i++) {
                const tempdata = await this.ToDto(context, data[i]);
                _data.push(tempdata);
            }
        }
        return _data;
    }

    /**
     * 处理响应dto对象
     *
     * @param {*} context 应用上下文对象
     * @param {*} data 响应dto对象
     * @returns {*}
     * @memberof BXDMXDTOHelp
     */
    public static async set(context: IContext, data: any) {
        const _data: IParam = await this.ToDataObj(context, data);
        return _data;
    }

    /**
     * 处理请求数据对象
     *
     * @param {*} context 应用上下文对象
     * @param {*} data 数据对象
     * @returns {*}
     * @memberof BXDMXDTOHelp
     */
    public static async get(context: IContext, data: any = {}) {
        return await this.ToDto(context, data);
    }

    /**
     * 获取缓存数据
     *
     * @param {*} context 应用上下文对象
     * @param {*} srfkey 数据主键
     * @returns {*}
     * @memberof BXDMXDTOHelp
     */
    public static async getCacheData(context: IContext, srfkey: string) {
        const targetService: IEntityLocalDataService<IBXDMX> = await this.getService(context);
        const result =  await targetService.getLocal(context, srfkey);
        if(result){
            return await this.ToDto(context,result);
        }
    }

    /**
     * 获取缓存数组
     *
     * @param {*} context 应用上下文对象
     * @returns {any[]}
     * @memberof BXDMXDTOHelp
     */
    public static async getCacheDataArray(context: IContext) {
        const targetService: IEntityLocalDataService<IBXDMX> = await this.getService(context);
        const result =  await targetService.getLocals(context);
        if(result && result.length >0){
            return await this.ToDtoArray(context,result);
        }else{
            return [];
        }
    }

    /**
     * 设置缓存数据
     *
     * @param {*} context 应用上下文对象
     * @param {any} data 数据
     * @returns {any[]}
     * @memberof BXDMXDTOHelp
     */
    public static async setCacheData(context: IContext, data: any) {
        const targetService: IEntityLocalDataService<IBXDMX> = await this.getService(context);
        const _data: any = await this.set(context, data);
        await targetService.addLocal(context, _data);
    }

    /**
     * 设置缓存数组
     *
     * @param {*} context 应用上下文对象
     * @param {any[]} data 数据
     * @returns {any[]}
     * @memberof BXDMXDTOHelp
     */
    public static async setCacheDataArray(context: IContext, data: any[]) {
        if (data && data.length > 0) {
            const targetService: IEntityLocalDataService<IBXDMX> = await this.getService(context);
            for (let i = 0; i < data.length; i++) {
                const _data = await this.set(context, data[i]);
                await targetService.addLocal(context, _data);
            }
        }
    }
}
