import { EntityBase } from '../../entities';
import { IBXDMX } from './interface/ibxdmx';

/**
 * 报销单明细基类
 *
 * @export
 * @abstract
 * @class BXDMXBase
 * @extends {EntityBase}
 * @implements {IBXDMX}
 */
export abstract class BXDMXBase extends EntityBase implements IBXDMX {
    /**
     * 实体名称
     *
     * @readonly
     * @type {string}
     * @memberof BXDMXBase
     */
    get srfdename(): string {
        return 'BXDMX';
    }
    get srfkey() {
        return this.bxdmxid;
    }
    set srfkey(val: any) {
        this.bxdmxid = val;
    }
    get srfmajortext() {
        return this.bxdmxname;
    }
    set srfmajortext(val: any) {
        this.bxdmxname = val;
    }
    /**
     * 报销单明细标识
     */
    bxdmxid?: any;
    /**
     * 建立人
     */
    createman?: any;
    /**
     * 建立时间
     */
    createdate?: any;
    /**
     * 报销单明细名称
     */
    bxdmxname?: any;
    /**
     * 更新人
     */
    updateman?: any;
    /**
     * 更新时间
     */
    updatedate?: any;
    /**
     * 报销单标识
     */
    bxdid?: any;
    /**
     * 报销单名称
     */
    bxdname?: any;
    /**
     * 备注
     */
    memo?: any;
    /**
     * 启用
     */
    enable?: any;
    /**
     * 排序值
     */
    ordervalue?: any;

    /**
     * 重置实体数据
     *
     * @private
     * @param {*} [data={}]
     * @memberof BXDMXBase
     */
    reset(data: any = {}): void {
        super.reset(data);
        this.bxdmxid = data.bxdmxid || data.srfkey;
        this.bxdmxname = data.bxdmxname || data.srfmajortext;
    }
}
