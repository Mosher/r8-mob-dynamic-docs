export default [
    'bxdmxid',
    'createman',
    'createdate',
    'bxdmxname',
    'updateman',
    'updatedate',
    'bxdid',
    'bxdname',
    'memo',
    'enable',
    'ordervalue',
];
