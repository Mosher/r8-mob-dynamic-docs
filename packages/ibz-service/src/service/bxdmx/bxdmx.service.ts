import { BXDMXBaseService } from './bxdmx-base.service';

/**
 * 报销单明细服务
 *
 * @export
 * @class BXDMXService
 * @extends {BXDMXBaseService}
 */
export class BXDMXService extends BXDMXBaseService {
    /**
     * Creates an instance of BXDMXService.
     * @memberof BXDMXService
     */
    constructor(opts?: any) {
        const { context: context, tag: cacheKey } = opts;
        super(context);
        ServiceCache.set(cacheKey, this);
    }

    /**
     * 获取实例
     *
     * @static
     * @param 应用上下文
     * @return {*}  {BXDMXService}
     * @memberof BXDMXService
     */
    static getInstance(context?: any): BXDMXService {
        const cacheKey: string = context?.srfdynainstid ? `${context.srfdynainstid}BXDMXService` : `BXDMXService`;
        if (!ServiceCache.has(cacheKey)) {
            return new BXDMXService({ context: context, tag: cacheKey });
        }
        return ServiceCache.get(cacheKey);
    }
}
export default BXDMXService;
