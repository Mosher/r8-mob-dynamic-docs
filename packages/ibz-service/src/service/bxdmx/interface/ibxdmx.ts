import { IEntityBase } from "ibz-core";

/**
 * 报销单明细
 *
 * @export
 * @interface IBXDMX
 * @extends {IEntityBase}
 */
export interface IBXDMX extends IEntityBase {
    /**
     * 报销单明细标识
     */
    bxdmxid?: any;
    /**
     * 建立人
     */
    createman?: any;
    /**
     * 建立时间
     */
    createdate?: any;
    /**
     * 报销单明细名称
     */
    bxdmxname?: any;
    /**
     * 更新人
     */
    updateman?: any;
    /**
     * 更新时间
     */
    updatedate?: any;
    /**
     * 报销单标识
     */
    bxdid?: any;
    /**
     * 报销单名称
     */
    bxdname?: any;
    /**
     * 备注
     */
    memo?: any;
    /**
     * 启用
     */
    enable?: any;
    /**
     * 排序值
     */
    ordervalue?: any;
}
