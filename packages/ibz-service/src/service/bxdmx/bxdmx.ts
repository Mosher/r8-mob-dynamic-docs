import { BXDMXBase } from './bxdmx-base';

/**
 * 报销单明细
 *
 * @export
 * @class BXDMX
 * @extends {BXDMXBase}
 * @implements {IBXDMX}
 */
export class BXDMX extends BXDMXBase {

    /**
     * 克隆当前实体
     *
     * @return {*}
     * @memberof BXDMX
     */
    clone(): BXDMX {
        return new BXDMX(this);
    }
}
export default BXDMX;
