// import { CodeListService } from '../app/codelist-service';
import { EntityBaseService } from '../../service-base';
import { IContext } from 'ibz-core';
import { HttpResponse } from '../../utils/http-response';
import { IDYNADASHBOARD } from './interface/idynadashboard';
import { DYNADASHBOARD } from './dynadashboard';
import keys from './dynadashboard-keys';
import { DYNADASHBOARDDTOHelp } from './dynadashboarddto-help';


/**
 * 动态数据看板模型服务对象基类
 *
 * @export
 * @class DYNADASHBOARDBaseService
 * @extends {EntityBaseService}
 */
export class DYNADASHBOARDBaseService extends EntityBaseService<IDYNADASHBOARD> {
    protected get keys(): string[] {
        return keys;
    }
    protected SYSTEMNAME = 'StudioPreview';
    protected APPNAME = 'TestMob';
    protected APPDENAME = 'DYNADASHBOARD';
    protected APPDENAMEPLURAL = 'DYNADASHBOARDs';
    protected dynaModelFilePath:string = 'PSSYSAPPS/TestMob/PSAPPDATAENTITIES/DYNADASHBOARD.json';
    protected APPDEKEY = 'dynadashboardid';
    protected APPDETEXT = 'dynadashboardname';
    protected quickSearchFields = ['dynadashboardname',];
    protected selectContextParam = {
    };

    constructor(opts?: any) {
        super(opts, 'DYNADASHBOARD');
    }

    newEntity(data: IDYNADASHBOARD): DYNADASHBOARD {
        return new DYNADASHBOARD(data);
    }

    /**
     * Create
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof DYNADASHBOARDService
     */
    async Create(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        _data = await this.beforeExecuteAction(_context,_data,'Create');
        if (!_data.srffrontuf || _data.srffrontuf != 1) {
            _data[this.APPDEKEY] = null;
        }
        if (_data.srffrontuf != null) {
            delete _data.srffrontuf;
        }
    _data = await DYNADASHBOARDDTOHelp.get(_context,_data);
        const res = await this.http.post(`/dynadashboards`, _data);
        res.data = await DYNADASHBOARDDTOHelp.set(_context,res.data);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Get
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof DYNADASHBOARDService
     */
    async Get(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.get(`/dynadashboards/${encodeURIComponent(_context.dynadashboard)}`, _data);
        res.data = await DYNADASHBOARDDTOHelp.set(_context,res.data);
        res.data = await this.afterExecuteAction(_context,res?.data,'Get');
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * GetDraft
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof DYNADASHBOARDService
     */
    async GetDraft(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        _data[this.APPDENAME?.toLowerCase()] = undefined;
        _data[this.APPDEKEY] = undefined;
        const res = await this.http.get(`/dynadashboards/getdraft`, _data);
        res.data = await DYNADASHBOARDDTOHelp.set(_context,res.data);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Remove
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof DYNADASHBOARDService
     */
    async Remove(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.delete(`/dynadashboards/${encodeURIComponent(_context.dynadashboard)}`);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Update
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof DYNADASHBOARDService
     */
    async Update(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        _data = await this.beforeExecuteAction(_context,_data,'Update');
    _data = await DYNADASHBOARDDTOHelp.get(_context,_data);
        const res = await this.http.put(`/dynadashboards/${encodeURIComponent(_context.dynadashboard)}`, _data);
        res.data = await DYNADASHBOARDDTOHelp.set(_context,res.data);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * FetchDefault
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof DYNADASHBOARDService
     */
    async FetchDefault(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.post(`/dynadashboards/fetchdefault`, _data);
        res.data = await DYNADASHBOARDDTOHelp.ToDataObjArray(_context,res.data);
        res.data = await this.afterExecuteActionBatch(_context,res?.data,'FetchDefault');
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Select
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof DYNADASHBOARDService
     */
    async Select(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.get(`/dynadashboards/${encodeURIComponent(_context.dynadashboard)}/select`);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
}
