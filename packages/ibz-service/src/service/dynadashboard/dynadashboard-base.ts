import { EntityBase } from '../../entities';
import { IDYNADASHBOARD } from './interface/idynadashboard';

/**
 * 动态数据看板模型基类
 *
 * @export
 * @abstract
 * @class DYNADASHBOARDBase
 * @extends {EntityBase}
 * @implements {IDYNADASHBOARD}
 */
export abstract class DYNADASHBOARDBase extends EntityBase implements IDYNADASHBOARD {
    /**
     * 实体名称
     *
     * @readonly
     * @type {string}
     * @memberof DYNADASHBOARDBase
     */
    get srfdename(): string {
        return 'DYNADASHBOARD';
    }
    get srfkey() {
        return this.dynadashboardid;
    }
    set srfkey(val: any) {
        this.dynadashboardid = val;
    }
    get srfmajortext() {
        return this.dynadashboardname;
    }
    set srfmajortext(val: any) {
        this.dynadashboardname = val;
    }
    /**
     * 动态数据看板模型标识
     */
    dynadashboardid?: any;
    /**
     * 动态数据看板模型名称
     */
    dynadashboardname?: any;
    /**
     * 建立人
     */
    createman?: any;
    /**
     * 更新人
     */
    updateman?: any;
    /**
     * 更新时间
     */
    updatedate?: any;
    /**
     * 建立时间
     */
    createdate?: any;
    /**
     * 应用标识
     */
    appid?: any;
    /**
     * 用户标识
     */
    userid?: any;
    /**
     * 模型
     */
    model?: any;
    /**
     * 模型标识
     */
    modelid?: any;

    /**
     * 重置实体数据
     *
     * @private
     * @param {*} [data={}]
     * @memberof DYNADASHBOARDBase
     */
    reset(data: any = {}): void {
        super.reset(data);
        this.dynadashboardid = data.dynadashboardid || data.srfkey;
        this.dynadashboardname = data.dynadashboardname || data.srfmajortext;
    }
}
