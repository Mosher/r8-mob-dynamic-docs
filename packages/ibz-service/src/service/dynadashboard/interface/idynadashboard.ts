import { IEntityBase } from "ibz-core";

/**
 * 动态数据看板模型
 *
 * @export
 * @interface IDYNADASHBOARD
 * @extends {IEntityBase}
 */
export interface IDYNADASHBOARD extends IEntityBase {
    /**
     * 动态数据看板模型标识
     */
    dynadashboardid?: any;
    /**
     * 动态数据看板模型名称
     */
    dynadashboardname?: any;
    /**
     * 建立人
     */
    createman?: any;
    /**
     * 更新人
     */
    updateman?: any;
    /**
     * 更新时间
     */
    updatedate?: any;
    /**
     * 建立时间
     */
    createdate?: any;
    /**
     * 应用标识
     */
    appid?: any;
    /**
     * 用户标识
     */
    userid?: any;
    /**
     * 模型
     */
    model?: any;
    /**
     * 模型标识
     */
    modelid?: any;
}
