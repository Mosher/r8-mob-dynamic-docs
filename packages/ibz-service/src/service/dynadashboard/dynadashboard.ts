import { DYNADASHBOARDBase } from './dynadashboard-base';

/**
 * 动态数据看板模型
 *
 * @export
 * @class DYNADASHBOARD
 * @extends {DYNADASHBOARDBase}
 * @implements {IDYNADASHBOARD}
 */
export class DYNADASHBOARD extends DYNADASHBOARDBase {

    /**
     * 克隆当前实体
     *
     * @return {*}
     * @memberof DYNADASHBOARD
     */
    clone(): DYNADASHBOARD {
        return new DYNADASHBOARD(this);
    }
}
export default DYNADASHBOARD;
