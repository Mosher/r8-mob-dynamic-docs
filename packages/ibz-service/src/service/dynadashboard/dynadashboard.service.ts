import { DYNADASHBOARDBaseService } from './dynadashboard-base.service';

/**
 * 动态数据看板模型服务
 *
 * @export
 * @class DYNADASHBOARDService
 * @extends {DYNADASHBOARDBaseService}
 */
export class DYNADASHBOARDService extends DYNADASHBOARDBaseService {
    /**
     * Creates an instance of DYNADASHBOARDService.
     * @memberof DYNADASHBOARDService
     */
    constructor(opts?: any) {
        const { context: context, tag: cacheKey } = opts;
        super(context);
        ServiceCache.set(cacheKey, this);
    }

    /**
     * 获取实例
     *
     * @static
     * @param 应用上下文
     * @return {*}  {DYNADASHBOARDService}
     * @memberof DYNADASHBOARDService
     */
    static getInstance(context?: any): DYNADASHBOARDService {
        const cacheKey: string = context?.srfdynainstid ? `${context.srfdynainstid}DYNADASHBOARDService` : `DYNADASHBOARDService`;
        if (!ServiceCache.has(cacheKey)) {
            return new DYNADASHBOARDService({ context: context, tag: cacheKey });
        }
        return ServiceCache.get(cacheKey);
    }
}
export default DYNADASHBOARDService;
