import { IEntityBase } from "ibz-core";

/**
 * 继承实体(子)
 *
 * @export
 * @interface IInheritedChildEntity
 * @extends {IEntityBase}
 */
export interface IInheritedChildEntity extends IEntityBase {
    /**
     * 继承实体(子)标识
     */
    inheritedchildentityid?: any;
    /**
     * 更新人
     */
    updateman?: any;
    /**
     * 继承实体(子)名称
     */
    inheritedchildentityname?: any;
    /**
     * 建立时间
     */
    createdate?: any;
    /**
     * 更新时间
     */
    updatedate?: any;
    /**
     * 建立人
     */
    createman?: any;
    /**
     * 备注
     */
    memo?: any;
    /**
     * 分组类型
     *
     * @type {('InheritedChildEntityType')} InheritedChildEntityType: 继承子实体类型
     */
    inheritedentitytype?: 'InheritedChildEntityType';
}
