import { InheritedChildEntityBaseService } from './inherited-child-entity-base.service';

/**
 * 继承实体(子)服务
 *
 * @export
 * @class InheritedChildEntityService
 * @extends {InheritedChildEntityBaseService}
 */
export class InheritedChildEntityService extends InheritedChildEntityBaseService {
    /**
     * Creates an instance of InheritedChildEntityService.
     * @memberof InheritedChildEntityService
     */
    constructor(opts?: any) {
        const { context: context, tag: cacheKey } = opts;
        super(context);
        ServiceCache.set(cacheKey, this);
    }

    /**
     * 获取实例
     *
     * @static
     * @param 应用上下文
     * @return {*}  {InheritedChildEntityService}
     * @memberof InheritedChildEntityService
     */
    static getInstance(context?: any): InheritedChildEntityService {
        const cacheKey: string = context?.srfdynainstid ? `${context.srfdynainstid}InheritedChildEntityService` : `InheritedChildEntityService`;
        if (!ServiceCache.has(cacheKey)) {
            return new InheritedChildEntityService({ context: context, tag: cacheKey });
        }
        return ServiceCache.get(cacheKey);
    }
}
export default InheritedChildEntityService;
