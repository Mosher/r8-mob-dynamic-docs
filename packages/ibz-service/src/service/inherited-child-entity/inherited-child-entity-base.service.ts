// import { CodeListService } from '../app/codelist-service';
import { EntityBaseService } from '../../service-base';
import { IContext } from 'ibz-core';
import { HttpResponse } from '../../utils/http-response';
import { IInheritedChildEntity } from './interface/iinherited-child-entity';
import { InheritedChildEntity } from './inherited-child-entity';
import keys from './inherited-child-entity-keys';
import { InheritedChildEntityDTOHelp } from './inherited-child-entity-dto-help';


/**
 * 继承实体(子)服务对象基类
 *
 * @export
 * @class InheritedChildEntityBaseService
 * @extends {EntityBaseService}
 */
export class InheritedChildEntityBaseService extends EntityBaseService<IInheritedChildEntity> {
    protected get keys(): string[] {
        return keys;
    }
    protected SYSTEMNAME = 'StudioPreview';
    protected APPNAME = 'TestMob';
    protected APPDENAME = 'InheritedChildEntity';
    protected APPDENAMEPLURAL = 'InheritedChildEntities';
    protected dynaModelFilePath:string = 'PSSYSAPPS/TestMob/PSAPPDATAENTITIES/InheritedChildEntity.json';
    protected APPDEKEY = 'inheritedchildentityid';
    protected APPDETEXT = 'inheritedchildentityname';
    protected quickSearchFields = ['inheritedchildentityname',];
    protected selectContextParam = {
    };

    constructor(opts?: any) {
        super(opts, 'InheritedChildEntity');
    }

    newEntity(data: IInheritedChildEntity): InheritedChildEntity {
        return new InheritedChildEntity(data);
    }

    /**
     * Create
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof InheritedChildEntityService
     */
    async Create(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        _data = await this.beforeExecuteAction(_context,_data,'Create');
        if (!_data.srffrontuf || _data.srffrontuf != 1) {
            _data[this.APPDEKEY] = null;
        }
        if (_data.srffrontuf != null) {
            delete _data.srffrontuf;
        }
    _data = await InheritedChildEntityDTOHelp.get(_context,_data);
        const res = await this.http.post(`/inheritedchildentities`, _data);
        res.data = await InheritedChildEntityDTOHelp.set(_context,res.data);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Get
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof InheritedChildEntityService
     */
    async Get(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.get(`/inheritedchildentities/${encodeURIComponent(_context.inheritedchildentity)}`, _data);
        res.data = await InheritedChildEntityDTOHelp.set(_context,res.data);
        res.data = await this.afterExecuteAction(_context,res?.data,'Get');
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * GetDraft
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof InheritedChildEntityService
     */
    async GetDraft(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        _data[this.APPDENAME?.toLowerCase()] = undefined;
        _data[this.APPDEKEY] = undefined;
        const res = await this.http.get(`/inheritedchildentities/getdraft`, _data);
        res.data = await InheritedChildEntityDTOHelp.set(_context,res.data);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Remove
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof InheritedChildEntityService
     */
    async Remove(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.delete(`/inheritedchildentities/${encodeURIComponent(_context.inheritedchildentity)}`);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Update
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof InheritedChildEntityService
     */
    async Update(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        _data = await this.beforeExecuteAction(_context,_data,'Update');
    _data = await InheritedChildEntityDTOHelp.get(_context,_data);
        const res = await this.http.put(`/inheritedchildentities/${encodeURIComponent(_context.inheritedchildentity)}`, _data);
        res.data = await InheritedChildEntityDTOHelp.set(_context,res.data);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * FetchDefault
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof InheritedChildEntityService
     */
    async FetchDefault(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.post(`/inheritedchildentities/fetchdefault`, _data);
        res.data = await InheritedChildEntityDTOHelp.ToDataObjArray(_context,res.data);
        res.data = await this.afterExecuteActionBatch(_context,res?.data,'FetchDefault');
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Select
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof InheritedChildEntityService
     */
    async Select(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.get(`/inheritedchildentities/${encodeURIComponent(_context.inheritedchildentity)}/select`);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
}
