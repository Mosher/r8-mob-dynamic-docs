import { EntityBase } from '../../entities';
import { IInheritedChildEntity } from './interface/iinherited-child-entity';

/**
 * 继承实体(子)基类
 *
 * @export
 * @abstract
 * @class InheritedChildEntityBase
 * @extends {EntityBase}
 * @implements {IInheritedChildEntity}
 */
export abstract class InheritedChildEntityBase extends EntityBase implements IInheritedChildEntity {
    /**
     * 实体名称
     *
     * @readonly
     * @type {string}
     * @memberof InheritedChildEntityBase
     */
    get srfdename(): string {
        return 'INHERITEDCHILDENTITY';
    }
    get srfkey() {
        return this.inheritedchildentityid;
    }
    set srfkey(val: any) {
        this.inheritedchildentityid = val;
    }
    get srfmajortext() {
        return this.inheritedchildentityname;
    }
    set srfmajortext(val: any) {
        this.inheritedchildentityname = val;
    }
    /**
     * 继承实体(子)标识
     */
    inheritedchildentityid?: any;
    /**
     * 更新人
     */
    updateman?: any;
    /**
     * 继承实体(子)名称
     */
    inheritedchildentityname?: any;
    /**
     * 建立时间
     */
    createdate?: any;
    /**
     * 更新时间
     */
    updatedate?: any;
    /**
     * 建立人
     */
    createman?: any;
    /**
     * 备注
     */
    memo?: any;
    /**
     * 分组类型
     *
     * @type {('InheritedChildEntityType')} InheritedChildEntityType: 继承子实体类型
     */
    inheritedentitytype?: 'InheritedChildEntityType';

    /**
     * 重置实体数据
     *
     * @private
     * @param {*} [data={}]
     * @memberof InheritedChildEntityBase
     */
    reset(data: any = {}): void {
        super.reset(data);
        this.inheritedchildentityid = data.inheritedchildentityid || data.srfkey;
        this.inheritedchildentityname = data.inheritedchildentityname || data.srfmajortext;
    }
}
