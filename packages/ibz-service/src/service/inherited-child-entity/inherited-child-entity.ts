import { InheritedChildEntityBase } from './inherited-child-entity-base';

/**
 * 继承实体(子)
 *
 * @export
 * @class InheritedChildEntity
 * @extends {InheritedChildEntityBase}
 * @implements {IInheritedChildEntity}
 */
export class InheritedChildEntity extends InheritedChildEntityBase {

    /**
     * 克隆当前实体
     *
     * @return {*}
     * @memberof InheritedChildEntity
     */
    clone(): InheritedChildEntity {
        return new InheritedChildEntity(this);
    }
}
export default InheritedChildEntity;
