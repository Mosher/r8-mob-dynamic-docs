import { IEntityLocalDataService, IContext, IParam } from 'ibz-core';
import { IInheritedChildEntity } from './interface/iinherited-child-entity';
/**
 * InheritedChildEntityDTOdto辅助类
 *
 * @export
 * @class InheritedChildEntityDTOHelp
 */
export class InheritedChildEntityDTOHelp {

    /**
     * 获取数据服务
     *
     * @param {IContext} context 应用上下文对象
     * @returns {*}
     * @memberof InheritedChildEntityDTOHelp
     */
    public static async getService(context: IContext): Promise<IEntityLocalDataService<IInheritedChildEntity>> {
        return await App.getEntityService().getService(context, 'inheritedchildentity') as IEntityLocalDataService<IInheritedChildEntity>;
    }

    /**
     * DTO转化成数据对象
     *
     * @param {IContext} context 应用上下文对象
     * @param {IParam} source dto对象
     * @returns {*}
     * @memberof InheritedChildEntityDTOHelp
     */
    public static async ToDataObj(context: IContext, source: IParam) {
        const _data: any = {};
        // 建立时间
        _data.createdate = source.createdate;
        // 建立人
        _data.createman = source.createman;
        // 继承实体(子)标识
        _data.inheritedchildentityid = source.inheritedchildentityid;
        // 继承实体(子)名称
        _data.inheritedchildentityname = source.inheritedchildentityname;
        // 分组类型
        _data.inheritedentitytype = source.inheritedentitytype;
        // 备注
        _data.memo = source.memo;
        // 更新时间
        _data.updatedate = source.updatedate;
        // 更新人
        _data.updateman = source.updateman;
        return _data;
    }

    /**
     * 转化数组(dto转化成数据对象)
     *
     * @param {IContext} context 应用上下文对象
     * @param {any[]} data 数据对象
     * @returns {any[]}
     * @memberof InheritedChildEntityDTOHelp
     */
    public static async ToDataObjArray(context: IContext, data: any[]) {
        const _data: any[] = [];
        if (data && Array.isArray(data) && data.length > 0) {
            for (let i = 0; i < data.length; i++) {
                const tempdata = await this.ToDataObj(context, data[i]);
                _data.push(tempdata);
            }
        }
        return _data;
    }

    /**
     * 数据对象转化成DTO
     *
     * @param {IContext} context 应用上下文对象
     * @param {*} source 数据对象
     * @returns {*}
     * @memberof InheritedChildEntityDTOHelp
     */
    public static async ToDto(context: IContext, source: IParam) {
        const _data: any = {};
        // 建立时间
        _data.createdate = source.createdate;
        // 建立人
        _data.createman = source.createman;
        // 继承实体(子)标识
        _data.inheritedchildentityid = source.inheritedchildentityid;
        // 继承实体(子)名称
        _data.inheritedchildentityname = source.inheritedchildentityname;
        // 分组类型
        _data.inheritedentitytype = source.inheritedentitytype;
        // 备注
        _data.memo = source.memo;
        // 更新时间
        _data.updatedate = source.updatedate;
        // 更新人
        _data.updateman = source.updateman;
        return _data;
    }

    /**
     * 转化数组(数据对象转化成dto)
     *
     * @param {IContext} context 应用上下文对象
     * @param {any[]} data 
     * @returns {any[]}
     * @memberof InheritedChildEntityDTOHelp
     */
    public static async ToDtoArray(context: IContext, data: any[]) {
        const _data: any[] = [];
        if (data && Array.isArray(data) && data.length > 0) {
            for (let i = 0; i < data.length; i++) {
                const tempdata = await this.ToDto(context, data[i]);
                _data.push(tempdata);
            }
        }
        return _data;
    }

    /**
     * 处理响应dto对象
     *
     * @param {*} context 应用上下文对象
     * @param {*} data 响应dto对象
     * @returns {*}
     * @memberof InheritedChildEntityDTOHelp
     */
    public static async set(context: IContext, data: any) {
        const _data: IParam = await this.ToDataObj(context, data);
        return _data;
    }

    /**
     * 处理请求数据对象
     *
     * @param {*} context 应用上下文对象
     * @param {*} data 数据对象
     * @returns {*}
     * @memberof InheritedChildEntityDTOHelp
     */
    public static async get(context: IContext, data: any = {}) {
        return await this.ToDto(context, data);
    }

    /**
     * 获取缓存数据
     *
     * @param {*} context 应用上下文对象
     * @param {*} srfkey 数据主键
     * @returns {*}
     * @memberof InheritedChildEntityDTOHelp
     */
    public static async getCacheData(context: IContext, srfkey: string) {
        const targetService: IEntityLocalDataService<IInheritedChildEntity> = await this.getService(context);
        const result =  await targetService.getLocal(context, srfkey);
        if(result){
            return await this.ToDto(context,result);
        }
    }

    /**
     * 获取缓存数组
     *
     * @param {*} context 应用上下文对象
     * @returns {any[]}
     * @memberof InheritedChildEntityDTOHelp
     */
    public static async getCacheDataArray(context: IContext) {
        const targetService: IEntityLocalDataService<IInheritedChildEntity> = await this.getService(context);
        const result =  await targetService.getLocals(context);
        if(result && result.length >0){
            return await this.ToDtoArray(context,result);
        }else{
            return [];
        }
    }

    /**
     * 设置缓存数据
     *
     * @param {*} context 应用上下文对象
     * @param {any} data 数据
     * @returns {any[]}
     * @memberof InheritedChildEntityDTOHelp
     */
    public static async setCacheData(context: IContext, data: any) {
        const targetService: IEntityLocalDataService<IInheritedChildEntity> = await this.getService(context);
        const _data: any = await this.set(context, data);
        await targetService.addLocal(context, _data);
    }

    /**
     * 设置缓存数组
     *
     * @param {*} context 应用上下文对象
     * @param {any[]} data 数据
     * @returns {any[]}
     * @memberof InheritedChildEntityDTOHelp
     */
    public static async setCacheDataArray(context: IContext, data: any[]) {
        if (data && data.length > 0) {
            const targetService: IEntityLocalDataService<IInheritedChildEntity> = await this.getService(context);
            for (let i = 0; i < data.length; i++) {
                const _data = await this.set(context, data[i]);
                await targetService.addLocal(context, _data);
            }
        }
    }
}
