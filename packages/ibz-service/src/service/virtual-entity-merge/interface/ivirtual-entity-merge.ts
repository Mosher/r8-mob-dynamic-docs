import { IEntityBase } from "ibz-core";

/**
 * 虚拟实体合并
 *
 * @export
 * @interface IVirtualEntityMerge
 * @extends {IEntityBase}
 */
export interface IVirtualEntityMerge extends IEntityBase {
    /**
     * 虚拟实体01名称
     */
    virtualentity01name?: any;
    /**
     * 备注03
     */
    memo03?: any;
    /**
     * 虚拟实体01标识
     */
    virtualentity01id?: any;
    /**
     * 虚拟实体03标识
     */
    virtualentity03id?: any;
    /**
     * 虚拟实体02名称
     */
    virtualentity02name?: any;
    /**
     * 建立人
     */
    createman?: any;
    /**
     * 更新时间
     */
    updatedate?: any;
    /**
     * 备注02
     */
    memo02?: any;
    /**
     * 虚拟实体03名称
     */
    virtualentity03name?: any;
    /**
     * 虚拟实体02标识
     */
    virtualentity02id?: any;
    /**
     * 备注01
     */
    mome01?: any;
    /**
     * 更新人
     */
    updateman?: any;
    /**
     * 建立时间
     */
    createdate?: any;
}
