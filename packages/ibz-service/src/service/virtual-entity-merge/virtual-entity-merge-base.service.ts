// import { CodeListService } from '../app/codelist-service';
import { EntityBaseService } from '../../service-base';
import { IContext } from 'ibz-core';
import { HttpResponse } from '../../utils/http-response';
import { IVirtualEntityMerge } from './interface/ivirtual-entity-merge';
import { VirtualEntityMerge } from './virtual-entity-merge';
import keys from './virtual-entity-merge-keys';
import { VirtualEntityMergeDTOHelp } from './virtual-entity-merge-dto-help';


/**
 * 虚拟实体合并服务对象基类
 *
 * @export
 * @class VirtualEntityMergeBaseService
 * @extends {EntityBaseService}
 */
export class VirtualEntityMergeBaseService extends EntityBaseService<IVirtualEntityMerge> {
    protected get keys(): string[] {
        return keys;
    }
    protected SYSTEMNAME = 'StudioPreview';
    protected APPNAME = 'TestMob';
    protected APPDENAME = 'VirtualEntityMerge';
    protected APPDENAMEPLURAL = 'VirtualEntityMerges';
    protected dynaModelFilePath:string = 'PSSYSAPPS/TestMob/PSAPPDATAENTITIES/VirtualEntityMerge.json';
    protected APPDEKEY = 'virtualentity01id';
    protected APPDETEXT = 'virtualentity01name';
    protected quickSearchFields = ['virtualentity01name',];
    protected selectContextParam = {
    };

    constructor(opts?: any) {
        super(opts, 'VirtualEntityMerge');
    }

    newEntity(data: IVirtualEntityMerge): VirtualEntityMerge {
        return new VirtualEntityMerge(data);
    }

    /**
     * Create
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof VirtualEntityMergeService
     */
    async Create(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        _data = await this.beforeExecuteAction(_context,_data,'Create');
        if (!_data.srffrontuf || _data.srffrontuf != 1) {
            _data[this.APPDEKEY] = null;
        }
        if (_data.srffrontuf != null) {
            delete _data.srffrontuf;
        }
    _data = await VirtualEntityMergeDTOHelp.get(_context,_data);
        const res = await this.http.post(`/virtualentitymerges`, _data);
        res.data = await VirtualEntityMergeDTOHelp.set(_context,res.data);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Get
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof VirtualEntityMergeService
     */
    async Get(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.get(`/virtualentitymerges/${encodeURIComponent(_context.virtualentitymerge)}`, _data);
        res.data = await VirtualEntityMergeDTOHelp.set(_context,res.data);
        res.data = await this.afterExecuteAction(_context,res?.data,'Get');
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * GetDraft
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof VirtualEntityMergeService
     */
    async GetDraft(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        _data[this.APPDENAME?.toLowerCase()] = undefined;
        _data[this.APPDEKEY] = undefined;
        const res = await this.http.get(`/virtualentitymerges/getdraft`, _data);
        res.data = await VirtualEntityMergeDTOHelp.set(_context,res.data);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Remove
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof VirtualEntityMergeService
     */
    async Remove(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.delete(`/virtualentitymerges/${encodeURIComponent(_context.virtualentitymerge)}`);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Update
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof VirtualEntityMergeService
     */
    async Update(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        _data = await this.beforeExecuteAction(_context,_data,'Update');
    _data = await VirtualEntityMergeDTOHelp.get(_context,_data);
        const res = await this.http.put(`/virtualentitymerges/${encodeURIComponent(_context.virtualentitymerge)}`, _data);
        res.data = await VirtualEntityMergeDTOHelp.set(_context,res.data);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * FetchDefault
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof VirtualEntityMergeService
     */
    async FetchDefault(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.post(`/virtualentitymerges/fetchdefault`, _data);
        res.data = await VirtualEntityMergeDTOHelp.ToDataObjArray(_context,res.data);
        res.data = await this.afterExecuteActionBatch(_context,res?.data,'FetchDefault');
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Select
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof VirtualEntityMergeService
     */
    async Select(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.get(`/virtualentitymerges/${encodeURIComponent(_context.virtualentitymerge)}/select`);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
}
