import { VirtualEntityMergeBase } from './virtual-entity-merge-base';

/**
 * 虚拟实体合并
 *
 * @export
 * @class VirtualEntityMerge
 * @extends {VirtualEntityMergeBase}
 * @implements {IVirtualEntityMerge}
 */
export class VirtualEntityMerge extends VirtualEntityMergeBase {

    /**
     * 克隆当前实体
     *
     * @return {*}
     * @memberof VirtualEntityMerge
     */
    clone(): VirtualEntityMerge {
        return new VirtualEntityMerge(this);
    }
}
export default VirtualEntityMerge;
