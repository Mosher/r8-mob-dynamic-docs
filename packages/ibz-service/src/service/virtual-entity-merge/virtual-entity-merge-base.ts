import { EntityBase } from '../../entities';
import { IVirtualEntityMerge } from './interface/ivirtual-entity-merge';

/**
 * 虚拟实体合并基类
 *
 * @export
 * @abstract
 * @class VirtualEntityMergeBase
 * @extends {EntityBase}
 * @implements {IVirtualEntityMerge}
 */
export abstract class VirtualEntityMergeBase extends EntityBase implements IVirtualEntityMerge {
    /**
     * 实体名称
     *
     * @readonly
     * @type {string}
     * @memberof VirtualEntityMergeBase
     */
    get srfdename(): string {
        return 'VIRTUALENTITYMERGE';
    }
    get srfkey() {
        return this.virtualentity01id;
    }
    set srfkey(val: any) {
        this.virtualentity01id = val;
    }
    get srfmajortext() {
        return this.virtualentity01name;
    }
    set srfmajortext(val: any) {
        this.virtualentity01name = val;
    }
    /**
     * 虚拟实体01名称
     */
    virtualentity01name?: any;
    /**
     * 备注03
     */
    memo03?: any;
    /**
     * 虚拟实体01标识
     */
    virtualentity01id?: any;
    /**
     * 虚拟实体03标识
     */
    virtualentity03id?: any;
    /**
     * 虚拟实体02名称
     */
    virtualentity02name?: any;
    /**
     * 建立人
     */
    createman?: any;
    /**
     * 更新时间
     */
    updatedate?: any;
    /**
     * 备注02
     */
    memo02?: any;
    /**
     * 虚拟实体03名称
     */
    virtualentity03name?: any;
    /**
     * 虚拟实体02标识
     */
    virtualentity02id?: any;
    /**
     * 备注01
     */
    mome01?: any;
    /**
     * 更新人
     */
    updateman?: any;
    /**
     * 建立时间
     */
    createdate?: any;

    /**
     * 重置实体数据
     *
     * @private
     * @param {*} [data={}]
     * @memberof VirtualEntityMergeBase
     */
    reset(data: any = {}): void {
        super.reset(data);
        this.virtualentity01id = data.virtualentity01id || data.srfkey;
        this.virtualentity01name = data.virtualentity01name || data.srfmajortext;
    }
}
