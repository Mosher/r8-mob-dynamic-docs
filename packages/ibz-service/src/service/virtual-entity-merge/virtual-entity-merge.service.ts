import { VirtualEntityMergeBaseService } from './virtual-entity-merge-base.service';

/**
 * 虚拟实体合并服务
 *
 * @export
 * @class VirtualEntityMergeService
 * @extends {VirtualEntityMergeBaseService}
 */
export class VirtualEntityMergeService extends VirtualEntityMergeBaseService {
    /**
     * Creates an instance of VirtualEntityMergeService.
     * @memberof VirtualEntityMergeService
     */
    constructor(opts?: any) {
        const { context: context, tag: cacheKey } = opts;
        super(context);
        ServiceCache.set(cacheKey, this);
    }

    /**
     * 获取实例
     *
     * @static
     * @param 应用上下文
     * @return {*}  {VirtualEntityMergeService}
     * @memberof VirtualEntityMergeService
     */
    static getInstance(context?: any): VirtualEntityMergeService {
        const cacheKey: string = context?.srfdynainstid ? `${context.srfdynainstid}VirtualEntityMergeService` : `VirtualEntityMergeService`;
        if (!ServiceCache.has(cacheKey)) {
            return new VirtualEntityMergeService({ context: context, tag: cacheKey });
        }
        return ServiceCache.get(cacheKey);
    }
}
export default VirtualEntityMergeService;
