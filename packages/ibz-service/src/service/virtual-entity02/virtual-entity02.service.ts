import { VirtualEntity02BaseService } from './virtual-entity02-base.service';

/**
 * 虚拟实体02服务
 *
 * @export
 * @class VirtualEntity02Service
 * @extends {VirtualEntity02BaseService}
 */
export class VirtualEntity02Service extends VirtualEntity02BaseService {
    /**
     * Creates an instance of VirtualEntity02Service.
     * @memberof VirtualEntity02Service
     */
    constructor(opts?: any) {
        const { context: context, tag: cacheKey } = opts;
        super(context);
        ServiceCache.set(cacheKey, this);
    }

    /**
     * 获取实例
     *
     * @static
     * @param 应用上下文
     * @return {*}  {VirtualEntity02Service}
     * @memberof VirtualEntity02Service
     */
    static getInstance(context?: any): VirtualEntity02Service {
        const cacheKey: string = context?.srfdynainstid ? `${context.srfdynainstid}VirtualEntity02Service` : `VirtualEntity02Service`;
        if (!ServiceCache.has(cacheKey)) {
            return new VirtualEntity02Service({ context: context, tag: cacheKey });
        }
        return ServiceCache.get(cacheKey);
    }
}
export default VirtualEntity02Service;
