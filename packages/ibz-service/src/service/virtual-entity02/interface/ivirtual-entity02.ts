import { IEntityBase } from "ibz-core";

/**
 * 虚拟实体02
 *
 * @export
 * @interface IVirtualEntity02
 * @extends {IEntityBase}
 */
export interface IVirtualEntity02 extends IEntityBase {
    /**
     * 虚拟实体02名称
     */
    virtualentity02name?: any;
    /**
     * 虚拟实体02标识
     */
    virtualentity02id?: any;
    /**
     * 更新人
     */
    updateman?: any;
    /**
     * 更新时间
     */
    updatedate?: any;
    /**
     * 建立人
     */
    createman?: any;
    /**
     * 建立时间
     */
    createdate?: any;
    /**
     * 备注02
     */
    memo02?: any;
}
