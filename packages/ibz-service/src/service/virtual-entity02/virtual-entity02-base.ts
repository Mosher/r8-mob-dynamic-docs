import { EntityBase } from '../../entities';
import { IVirtualEntity02 } from './interface/ivirtual-entity02';

/**
 * 虚拟实体02基类
 *
 * @export
 * @abstract
 * @class VirtualEntity02Base
 * @extends {EntityBase}
 * @implements {IVirtualEntity02}
 */
export abstract class VirtualEntity02Base extends EntityBase implements IVirtualEntity02 {
    /**
     * 实体名称
     *
     * @readonly
     * @type {string}
     * @memberof VirtualEntity02Base
     */
    get srfdename(): string {
        return 'VIRTUALENTITY02';
    }
    get srfkey() {
        return this.virtualentity02id;
    }
    set srfkey(val: any) {
        this.virtualentity02id = val;
    }
    get srfmajortext() {
        return this.virtualentity02name;
    }
    set srfmajortext(val: any) {
        this.virtualentity02name = val;
    }
    /**
     * 虚拟实体02名称
     */
    virtualentity02name?: any;
    /**
     * 虚拟实体02标识
     */
    virtualentity02id?: any;
    /**
     * 更新人
     */
    updateman?: any;
    /**
     * 更新时间
     */
    updatedate?: any;
    /**
     * 建立人
     */
    createman?: any;
    /**
     * 建立时间
     */
    createdate?: any;
    /**
     * 备注02
     */
    memo02?: any;

    /**
     * 重置实体数据
     *
     * @private
     * @param {*} [data={}]
     * @memberof VirtualEntity02Base
     */
    reset(data: any = {}): void {
        super.reset(data);
        this.virtualentity02id = data.virtualentity02id || data.srfkey;
        this.virtualentity02name = data.virtualentity02name || data.srfmajortext;
    }
}
