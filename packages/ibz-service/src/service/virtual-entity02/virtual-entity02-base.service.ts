// import { CodeListService } from '../app/codelist-service';
import { EntityBaseService } from '../../service-base';
import { IContext } from 'ibz-core';
import { HttpResponse } from '../../utils/http-response';
import { IVirtualEntity02 } from './interface/ivirtual-entity02';
import { VirtualEntity02 } from './virtual-entity02';
import keys from './virtual-entity02-keys';
import { VirtualEntity02DTOHelp } from './virtual-entity02-dto-help';


/**
 * 虚拟实体02服务对象基类
 *
 * @export
 * @class VirtualEntity02BaseService
 * @extends {EntityBaseService}
 */
export class VirtualEntity02BaseService extends EntityBaseService<IVirtualEntity02> {
    protected get keys(): string[] {
        return keys;
    }
    protected SYSTEMNAME = 'StudioPreview';
    protected APPNAME = 'TestMob';
    protected APPDENAME = 'VirtualEntity02';
    protected APPDENAMEPLURAL = 'VirtualEntity02s';
    protected dynaModelFilePath:string = 'PSSYSAPPS/TestMob/PSAPPDATAENTITIES/VirtualEntity02.json';
    protected APPDEKEY = 'virtualentity02id';
    protected APPDETEXT = 'virtualentity02name';
    protected quickSearchFields = ['virtualentity02name',];
    protected selectContextParam = {
    };

    constructor(opts?: any) {
        super(opts, 'VirtualEntity02');
    }

    newEntity(data: IVirtualEntity02): VirtualEntity02 {
        return new VirtualEntity02(data);
    }

    /**
     * Create
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof VirtualEntity02Service
     */
    async Create(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        _data = await this.beforeExecuteAction(_context,_data,'Create');
        if (!_data.srffrontuf || _data.srffrontuf != 1) {
            _data[this.APPDEKEY] = null;
        }
        if (_data.srffrontuf != null) {
            delete _data.srffrontuf;
        }
    _data = await VirtualEntity02DTOHelp.get(_context,_data);
        const res = await this.http.post(`/virtualentity02s`, _data);
        res.data = await VirtualEntity02DTOHelp.set(_context,res.data);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Get
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof VirtualEntity02Service
     */
    async Get(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.get(`/virtualentity02s/${encodeURIComponent(_context.virtualentity02)}`, _data);
        res.data = await VirtualEntity02DTOHelp.set(_context,res.data);
        res.data = await this.afterExecuteAction(_context,res?.data,'Get');
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * GetDraft
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof VirtualEntity02Service
     */
    async GetDraft(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        _data[this.APPDENAME?.toLowerCase()] = undefined;
        _data[this.APPDEKEY] = undefined;
        const res = await this.http.get(`/virtualentity02s/getdraft`, _data);
        res.data = await VirtualEntity02DTOHelp.set(_context,res.data);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Remove
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof VirtualEntity02Service
     */
    async Remove(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.delete(`/virtualentity02s/${encodeURIComponent(_context.virtualentity02)}`);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Update
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof VirtualEntity02Service
     */
    async Update(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        _data = await this.beforeExecuteAction(_context,_data,'Update');
    _data = await VirtualEntity02DTOHelp.get(_context,_data);
        const res = await this.http.put(`/virtualentity02s/${encodeURIComponent(_context.virtualentity02)}`, _data);
        res.data = await VirtualEntity02DTOHelp.set(_context,res.data);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * FetchDefault
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof VirtualEntity02Service
     */
    async FetchDefault(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.post(`/virtualentity02s/fetchdefault`, _data);
        res.data = await VirtualEntity02DTOHelp.ToDataObjArray(_context,res.data);
        res.data = await this.afterExecuteActionBatch(_context,res?.data,'FetchDefault');
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Select
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof VirtualEntity02Service
     */
    async Select(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.get(`/virtualentity02s/${encodeURIComponent(_context.virtualentity02)}/select`);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
}
