import { VirtualEntity02Base } from './virtual-entity02-base';

/**
 * 虚拟实体02
 *
 * @export
 * @class VirtualEntity02
 * @extends {VirtualEntity02Base}
 * @implements {IVirtualEntity02}
 */
export class VirtualEntity02 extends VirtualEntity02Base {

    /**
     * 克隆当前实体
     *
     * @return {*}
     * @memberof VirtualEntity02
     */
    clone(): VirtualEntity02 {
        return new VirtualEntity02(this);
    }
}
export default VirtualEntity02;
