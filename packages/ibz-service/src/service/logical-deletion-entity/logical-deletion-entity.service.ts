import { LogicalDeletionEntityBaseService } from './logical-deletion-entity-base.service';

/**
 * 逻辑删除实体服务
 *
 * @export
 * @class LogicalDeletionEntityService
 * @extends {LogicalDeletionEntityBaseService}
 */
export class LogicalDeletionEntityService extends LogicalDeletionEntityBaseService {
    /**
     * Creates an instance of LogicalDeletionEntityService.
     * @memberof LogicalDeletionEntityService
     */
    constructor(opts?: any) {
        const { context: context, tag: cacheKey } = opts;
        super(context);
        ServiceCache.set(cacheKey, this);
    }

    /**
     * 获取实例
     *
     * @static
     * @param 应用上下文
     * @return {*}  {LogicalDeletionEntityService}
     * @memberof LogicalDeletionEntityService
     */
    static getInstance(context?: any): LogicalDeletionEntityService {
        const cacheKey: string = context?.srfdynainstid ? `${context.srfdynainstid}LogicalDeletionEntityService` : `LogicalDeletionEntityService`;
        if (!ServiceCache.has(cacheKey)) {
            return new LogicalDeletionEntityService({ context: context, tag: cacheKey });
        }
        return ServiceCache.get(cacheKey);
    }
}
export default LogicalDeletionEntityService;
