import { LogicalDeletionEntityBase } from './logical-deletion-entity-base';

/**
 * 逻辑删除实体
 *
 * @export
 * @class LogicalDeletionEntity
 * @extends {LogicalDeletionEntityBase}
 * @implements {ILogicalDeletionEntity}
 */
export class LogicalDeletionEntity extends LogicalDeletionEntityBase {

    /**
     * 克隆当前实体
     *
     * @return {*}
     * @memberof LogicalDeletionEntity
     */
    clone(): LogicalDeletionEntity {
        return new LogicalDeletionEntity(this);
    }
}
export default LogicalDeletionEntity;
