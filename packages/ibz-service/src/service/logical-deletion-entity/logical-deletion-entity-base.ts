import { EntityBase } from '../../entities';
import { ILogicalDeletionEntity } from './interface/ilogical-deletion-entity';

/**
 * 逻辑删除实体基类
 *
 * @export
 * @abstract
 * @class LogicalDeletionEntityBase
 * @extends {EntityBase}
 * @implements {ILogicalDeletionEntity}
 */
export abstract class LogicalDeletionEntityBase extends EntityBase implements ILogicalDeletionEntity {
    /**
     * 实体名称
     *
     * @readonly
     * @type {string}
     * @memberof LogicalDeletionEntityBase
     */
    get srfdename(): string {
        return 'LOGICALDELETIONENTITY';
    }
    get srfkey() {
        return this.logicaldeletionentityid;
    }
    set srfkey(val: any) {
        this.logicaldeletionentityid = val;
    }
    get srfmajortext() {
        return this.logicaldeletionentityname;
    }
    set srfmajortext(val: any) {
        this.logicaldeletionentityname = val;
    }
    /**
     * 建立人
     */
    createman?: any;
    /**
     * 更新时间
     */
    updatedate?: any;
    /**
     * 建立时间
     */
    createdate?: any;
    /**
     * 逻辑删除实体标识
     */
    logicaldeletionentityid?: any;
    /**
     * 逻辑删除实体名称
     */
    logicaldeletionentityname?: any;
    /**
     * 更新人
     */
    updateman?: any;
    /**
     * 逻辑有效标志
     */
    enable?: any;

    /**
     * 重置实体数据
     *
     * @private
     * @param {*} [data={}]
     * @memberof LogicalDeletionEntityBase
     */
    reset(data: any = {}): void {
        super.reset(data);
        this.logicaldeletionentityid = data.logicaldeletionentityid || data.srfkey;
        this.logicaldeletionentityname = data.logicaldeletionentityname || data.srfmajortext;
    }
}
