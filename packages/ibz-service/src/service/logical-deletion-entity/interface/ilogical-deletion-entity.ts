import { IEntityBase } from "ibz-core";

/**
 * 逻辑删除实体
 *
 * @export
 * @interface ILogicalDeletionEntity
 * @extends {IEntityBase}
 */
export interface ILogicalDeletionEntity extends IEntityBase {
    /**
     * 建立人
     */
    createman?: any;
    /**
     * 更新时间
     */
    updatedate?: any;
    /**
     * 建立时间
     */
    createdate?: any;
    /**
     * 逻辑删除实体标识
     */
    logicaldeletionentityid?: any;
    /**
     * 逻辑删除实体名称
     */
    logicaldeletionentityname?: any;
    /**
     * 更新人
     */
    updateman?: any;
    /**
     * 逻辑有效标志
     */
    enable?: any;
}
