// import { CodeListService } from '../app/codelist-service';
import { EntityBaseService } from '../../service-base';
import { IContext } from 'ibz-core';
import { HttpResponse } from '../../utils/http-response';
import { ILogicalDeletionEntity } from './interface/ilogical-deletion-entity';
import { LogicalDeletionEntity } from './logical-deletion-entity';
import keys from './logical-deletion-entity-keys';
import { LogicalDeletionEntityDTOHelp } from './logical-deletion-entity-dto-help';


/**
 * 逻辑删除实体服务对象基类
 *
 * @export
 * @class LogicalDeletionEntityBaseService
 * @extends {EntityBaseService}
 */
export class LogicalDeletionEntityBaseService extends EntityBaseService<ILogicalDeletionEntity> {
    protected get keys(): string[] {
        return keys;
    }
    protected SYSTEMNAME = 'StudioPreview';
    protected APPNAME = 'TestMob';
    protected APPDENAME = 'LogicalDeletionEntity';
    protected APPDENAMEPLURAL = 'LogicalDeletionEntities';
    protected dynaModelFilePath:string = 'PSSYSAPPS/TestMob/PSAPPDATAENTITIES/LogicalDeletionEntity.json';
    protected APPDEKEY = 'logicaldeletionentityid';
    protected APPDETEXT = 'logicaldeletionentityname';
    protected quickSearchFields = ['logicaldeletionentityname',];
    protected selectContextParam = {
    };

    constructor(opts?: any) {
        super(opts, 'LogicalDeletionEntity');
    }

    newEntity(data: ILogicalDeletionEntity): LogicalDeletionEntity {
        return new LogicalDeletionEntity(data);
    }

    /**
     * Create
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof LogicalDeletionEntityService
     */
    async Create(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        _data = await this.beforeExecuteAction(_context,_data,'Create');
        if (!_data.srffrontuf || _data.srffrontuf != 1) {
            _data[this.APPDEKEY] = null;
        }
        if (_data.srffrontuf != null) {
            delete _data.srffrontuf;
        }
    _data = await LogicalDeletionEntityDTOHelp.get(_context,_data);
        const res = await this.http.post(`/logicaldeletionentities`, _data);
        res.data = await LogicalDeletionEntityDTOHelp.set(_context,res.data);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Get
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof LogicalDeletionEntityService
     */
    async Get(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.get(`/logicaldeletionentities/${encodeURIComponent(_context.logicaldeletionentity)}`, _data);
        res.data = await LogicalDeletionEntityDTOHelp.set(_context,res.data);
        res.data = await this.afterExecuteAction(_context,res?.data,'Get');
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * GetDraft
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof LogicalDeletionEntityService
     */
    async GetDraft(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        _data[this.APPDENAME?.toLowerCase()] = undefined;
        _data[this.APPDEKEY] = undefined;
        const res = await this.http.get(`/logicaldeletionentities/getdraft`, _data);
        res.data = await LogicalDeletionEntityDTOHelp.set(_context,res.data);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Remove
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof LogicalDeletionEntityService
     */
    async Remove(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.delete(`/logicaldeletionentities/${encodeURIComponent(_context.logicaldeletionentity)}`);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Update
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof LogicalDeletionEntityService
     */
    async Update(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        _data = await this.beforeExecuteAction(_context,_data,'Update');
    _data = await LogicalDeletionEntityDTOHelp.get(_context,_data);
        const res = await this.http.put(`/logicaldeletionentities/${encodeURIComponent(_context.logicaldeletionentity)}`, _data);
        res.data = await LogicalDeletionEntityDTOHelp.set(_context,res.data);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * FetchDefault
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof LogicalDeletionEntityService
     */
    async FetchDefault(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.post(`/logicaldeletionentities/fetchdefault`, _data);
        res.data = await LogicalDeletionEntityDTOHelp.ToDataObjArray(_context,res.data);
        res.data = await this.afterExecuteActionBatch(_context,res?.data,'FetchDefault');
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Select
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof LogicalDeletionEntityService
     */
    async Select(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.get(`/logicaldeletionentities/${encodeURIComponent(_context.logicaldeletionentity)}/select`);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
}
