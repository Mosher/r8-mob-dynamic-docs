export default [
    'createman',
    'updateman',
    'inheritedentityname',
    'inheritedentityid',
    'updatedate',
    'createdate',
    'inheritedentitytype',
    'memo',
];
