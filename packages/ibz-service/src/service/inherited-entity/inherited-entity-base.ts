import { EntityBase } from '../../entities';
import { IInheritedEntity } from './interface/iinherited-entity';

/**
 * 继承实体基类
 *
 * @export
 * @abstract
 * @class InheritedEntityBase
 * @extends {EntityBase}
 * @implements {IInheritedEntity}
 */
export abstract class InheritedEntityBase extends EntityBase implements IInheritedEntity {
    /**
     * 实体名称
     *
     * @readonly
     * @type {string}
     * @memberof InheritedEntityBase
     */
    get srfdename(): string {
        return 'INHERITEDENTITY';
    }
    get srfkey() {
        return this.inheritedentityid;
    }
    set srfkey(val: any) {
        this.inheritedentityid = val;
    }
    get srfmajortext() {
        return this.inheritedentityname;
    }
    set srfmajortext(val: any) {
        this.inheritedentityname = val;
    }
    /**
     * 建立人
     */
    createman?: any;
    /**
     * 更新人
     */
    updateman?: any;
    /**
     * 继承实体名称
     */
    inheritedentityname?: any;
    /**
     * 继承实体标识
     */
    inheritedentityid?: any;
    /**
     * 更新时间
     */
    updatedate?: any;
    /**
     * 建立时间
     */
    createdate?: any;
    /**
     * 分组类型
     *
     * @type {('InheritedChildEntityType')} InheritedChildEntityType: 继承子实体类型
     */
    inheritedentitytype?: 'InheritedChildEntityType';
    /**
     * 备注
     */
    memo?: any;

    /**
     * 重置实体数据
     *
     * @private
     * @param {*} [data={}]
     * @memberof InheritedEntityBase
     */
    reset(data: any = {}): void {
        super.reset(data);
        this.inheritedentityid = data.inheritedentityid || data.srfkey;
        this.inheritedentityname = data.inheritedentityname || data.srfmajortext;
    }
}
