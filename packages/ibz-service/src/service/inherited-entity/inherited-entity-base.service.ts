// import { CodeListService } from '../app/codelist-service';
import { EntityBaseService } from '../../service-base';
import { IContext } from 'ibz-core';
import { HttpResponse } from '../../utils/http-response';
import { IInheritedEntity } from './interface/iinherited-entity';
import { InheritedEntity } from './inherited-entity';
import keys from './inherited-entity-keys';
import { InheritedEntityDTOHelp } from './inherited-entity-dto-help';


/**
 * 继承实体服务对象基类
 *
 * @export
 * @class InheritedEntityBaseService
 * @extends {EntityBaseService}
 */
export class InheritedEntityBaseService extends EntityBaseService<IInheritedEntity> {
    protected get keys(): string[] {
        return keys;
    }
    protected SYSTEMNAME = 'StudioPreview';
    protected APPNAME = 'TestMob';
    protected APPDENAME = 'InheritedEntity';
    protected APPDENAMEPLURAL = 'InheritedEntities';
    protected dynaModelFilePath:string = 'PSSYSAPPS/TestMob/PSAPPDATAENTITIES/InheritedEntity.json';
    protected APPDEKEY = 'inheritedentityid';
    protected APPDETEXT = 'inheritedentityname';
    protected quickSearchFields = ['inheritedentityname',];
    protected selectContextParam = {
    };

    constructor(opts?: any) {
        super(opts, 'InheritedEntity');
    }

    newEntity(data: IInheritedEntity): InheritedEntity {
        return new InheritedEntity(data);
    }

    /**
     * Create
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof InheritedEntityService
     */
    async Create(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        _data = await this.beforeExecuteAction(_context,_data,'Create');
        if (!_data.srffrontuf || _data.srffrontuf != 1) {
            _data[this.APPDEKEY] = null;
        }
        if (_data.srffrontuf != null) {
            delete _data.srffrontuf;
        }
    _data = await InheritedEntityDTOHelp.get(_context,_data);
        const res = await this.http.post(`/inheritedentities`, _data);
        res.data = await InheritedEntityDTOHelp.set(_context,res.data);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Get
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof InheritedEntityService
     */
    async Get(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.get(`/inheritedentities/${encodeURIComponent(_context.inheritedentity)}`, _data);
        res.data = await InheritedEntityDTOHelp.set(_context,res.data);
        res.data = await this.afterExecuteAction(_context,res?.data,'Get');
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * GetDraft
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof InheritedEntityService
     */
    async GetDraft(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        _data[this.APPDENAME?.toLowerCase()] = undefined;
        _data[this.APPDEKEY] = undefined;
        const res = await this.http.get(`/inheritedentities/getdraft`, _data);
        res.data = await InheritedEntityDTOHelp.set(_context,res.data);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Remove
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof InheritedEntityService
     */
    async Remove(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.delete(`/inheritedentities/${encodeURIComponent(_context.inheritedentity)}`);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Update
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof InheritedEntityService
     */
    async Update(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        _data = await this.beforeExecuteAction(_context,_data,'Update');
    _data = await InheritedEntityDTOHelp.get(_context,_data);
        const res = await this.http.put(`/inheritedentities/${encodeURIComponent(_context.inheritedentity)}`, _data);
        res.data = await InheritedEntityDTOHelp.set(_context,res.data);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * FetchDefault
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof InheritedEntityService
     */
    async FetchDefault(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.post(`/inheritedentities/fetchdefault`, _data);
        res.data = await InheritedEntityDTOHelp.ToDataObjArray(_context,res.data);
        res.data = await this.afterExecuteActionBatch(_context,res?.data,'FetchDefault');
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * FetchIndexDER
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof InheritedEntityService
     */
    async FetchIndexDER(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.post(`/inheritedentities/fetchindexder`, _data);
        res.data = await InheritedEntityDTOHelp.ToDataObjArray(_context,res.data);
        res.data = await this.afterExecuteActionBatch(_context,res?.data,'FetchIndexDER');
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Select
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof InheritedEntityService
     */
    async Select(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.get(`/inheritedentities/${encodeURIComponent(_context.inheritedentity)}/select`);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
}
