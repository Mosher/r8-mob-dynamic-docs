import { InheritedEntityBaseService } from './inherited-entity-base.service';

/**
 * 继承实体服务
 *
 * @export
 * @class InheritedEntityService
 * @extends {InheritedEntityBaseService}
 */
export class InheritedEntityService extends InheritedEntityBaseService {
    /**
     * Creates an instance of InheritedEntityService.
     * @memberof InheritedEntityService
     */
    constructor(opts?: any) {
        const { context: context, tag: cacheKey } = opts;
        super(context);
        ServiceCache.set(cacheKey, this);
    }

    /**
     * 获取实例
     *
     * @static
     * @param 应用上下文
     * @return {*}  {InheritedEntityService}
     * @memberof InheritedEntityService
     */
    static getInstance(context?: any): InheritedEntityService {
        const cacheKey: string = context?.srfdynainstid ? `${context.srfdynainstid}InheritedEntityService` : `InheritedEntityService`;
        if (!ServiceCache.has(cacheKey)) {
            return new InheritedEntityService({ context: context, tag: cacheKey });
        }
        return ServiceCache.get(cacheKey);
    }
}
export default InheritedEntityService;
