import { InheritedEntityBase } from './inherited-entity-base';

/**
 * 继承实体
 *
 * @export
 * @class InheritedEntity
 * @extends {InheritedEntityBase}
 * @implements {IInheritedEntity}
 */
export class InheritedEntity extends InheritedEntityBase {

    /**
     * 克隆当前实体
     *
     * @return {*}
     * @memberof InheritedEntity
     */
    clone(): InheritedEntity {
        return new InheritedEntity(this);
    }
}
export default InheritedEntity;
