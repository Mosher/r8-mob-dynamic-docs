import { IEntityBase } from "ibz-core";

/**
 * 继承实体
 *
 * @export
 * @interface IInheritedEntity
 * @extends {IEntityBase}
 */
export interface IInheritedEntity extends IEntityBase {
    /**
     * 建立人
     */
    createman?: any;
    /**
     * 更新人
     */
    updateman?: any;
    /**
     * 继承实体名称
     */
    inheritedentityname?: any;
    /**
     * 继承实体标识
     */
    inheritedentityid?: any;
    /**
     * 更新时间
     */
    updatedate?: any;
    /**
     * 建立时间
     */
    createdate?: any;
    /**
     * 分组类型
     *
     * @type {('InheritedChildEntityType')} InheritedChildEntityType: 继承子实体类型
     */
    inheritedentitytype?: 'InheritedChildEntityType';
    /**
     * 备注
     */
    memo?: any;
}
