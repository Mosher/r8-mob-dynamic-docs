import { VirtualEntity01BaseService } from './virtual-entity01-base.service';

/**
 * 虚拟实体01服务
 *
 * @export
 * @class VirtualEntity01Service
 * @extends {VirtualEntity01BaseService}
 */
export class VirtualEntity01Service extends VirtualEntity01BaseService {
    /**
     * Creates an instance of VirtualEntity01Service.
     * @memberof VirtualEntity01Service
     */
    constructor(opts?: any) {
        const { context: context, tag: cacheKey } = opts;
        super(context);
        ServiceCache.set(cacheKey, this);
    }

    /**
     * 获取实例
     *
     * @static
     * @param 应用上下文
     * @return {*}  {VirtualEntity01Service}
     * @memberof VirtualEntity01Service
     */
    static getInstance(context?: any): VirtualEntity01Service {
        const cacheKey: string = context?.srfdynainstid ? `${context.srfdynainstid}VirtualEntity01Service` : `VirtualEntity01Service`;
        if (!ServiceCache.has(cacheKey)) {
            return new VirtualEntity01Service({ context: context, tag: cacheKey });
        }
        return ServiceCache.get(cacheKey);
    }
}
export default VirtualEntity01Service;
