import { VirtualEntity01Base } from './virtual-entity01-base';

/**
 * 虚拟实体01
 *
 * @export
 * @class VirtualEntity01
 * @extends {VirtualEntity01Base}
 * @implements {IVirtualEntity01}
 */
export class VirtualEntity01 extends VirtualEntity01Base {

    /**
     * 克隆当前实体
     *
     * @return {*}
     * @memberof VirtualEntity01
     */
    clone(): VirtualEntity01 {
        return new VirtualEntity01(this);
    }
}
export default VirtualEntity01;
