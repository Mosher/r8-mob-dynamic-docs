import { EntityBase } from '../../entities';
import { IVirtualEntity01 } from './interface/ivirtual-entity01';

/**
 * 虚拟实体01基类
 *
 * @export
 * @abstract
 * @class VirtualEntity01Base
 * @extends {EntityBase}
 * @implements {IVirtualEntity01}
 */
export abstract class VirtualEntity01Base extends EntityBase implements IVirtualEntity01 {
    /**
     * 实体名称
     *
     * @readonly
     * @type {string}
     * @memberof VirtualEntity01Base
     */
    get srfdename(): string {
        return 'VIRTUALENTITY01';
    }
    get srfkey() {
        return this.virtualentity01id;
    }
    set srfkey(val: any) {
        this.virtualentity01id = val;
    }
    get srfmajortext() {
        return this.virtualentity01name;
    }
    set srfmajortext(val: any) {
        this.virtualentity01name = val;
    }
    /**
     * 建立人
     */
    createman?: any;
    /**
     * 虚拟实体01名称
     */
    virtualentity01name?: any;
    /**
     * 更新人
     */
    updateman?: any;
    /**
     * 建立时间
     */
    createdate?: any;
    /**
     * 虚拟实体01标识
     */
    virtualentity01id?: any;
    /**
     * 更新时间
     */
    updatedate?: any;
    /**
     * 备注01
     */
    mome01?: any;

    /**
     * 重置实体数据
     *
     * @private
     * @param {*} [data={}]
     * @memberof VirtualEntity01Base
     */
    reset(data: any = {}): void {
        super.reset(data);
        this.virtualentity01id = data.virtualentity01id || data.srfkey;
        this.virtualentity01name = data.virtualentity01name || data.srfmajortext;
    }
}
