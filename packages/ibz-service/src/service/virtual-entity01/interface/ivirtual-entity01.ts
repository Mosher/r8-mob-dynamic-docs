import { IEntityBase } from "ibz-core";

/**
 * 虚拟实体01
 *
 * @export
 * @interface IVirtualEntity01
 * @extends {IEntityBase}
 */
export interface IVirtualEntity01 extends IEntityBase {
    /**
     * 建立人
     */
    createman?: any;
    /**
     * 虚拟实体01名称
     */
    virtualentity01name?: any;
    /**
     * 更新人
     */
    updateman?: any;
    /**
     * 建立时间
     */
    createdate?: any;
    /**
     * 虚拟实体01标识
     */
    virtualentity01id?: any;
    /**
     * 更新时间
     */
    updatedate?: any;
    /**
     * 备注01
     */
    mome01?: any;
}
