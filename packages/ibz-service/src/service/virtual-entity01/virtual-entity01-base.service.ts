// import { CodeListService } from '../app/codelist-service';
import { EntityBaseService } from '../../service-base';
import { IContext } from 'ibz-core';
import { HttpResponse } from '../../utils/http-response';
import { IVirtualEntity01 } from './interface/ivirtual-entity01';
import { VirtualEntity01 } from './virtual-entity01';
import keys from './virtual-entity01-keys';
import { VirtualEntity01DTOHelp } from './virtual-entity01-dto-help';


/**
 * 虚拟实体01服务对象基类
 *
 * @export
 * @class VirtualEntity01BaseService
 * @extends {EntityBaseService}
 */
export class VirtualEntity01BaseService extends EntityBaseService<IVirtualEntity01> {
    protected get keys(): string[] {
        return keys;
    }
    protected SYSTEMNAME = 'StudioPreview';
    protected APPNAME = 'TestMob';
    protected APPDENAME = 'VirtualEntity01';
    protected APPDENAMEPLURAL = 'VirtualEntity01s';
    protected dynaModelFilePath:string = 'PSSYSAPPS/TestMob/PSAPPDATAENTITIES/VirtualEntity01.json';
    protected APPDEKEY = 'virtualentity01id';
    protected APPDETEXT = 'virtualentity01name';
    protected quickSearchFields = ['virtualentity01name',];
    protected selectContextParam = {
    };

    constructor(opts?: any) {
        super(opts, 'VirtualEntity01');
    }

    newEntity(data: IVirtualEntity01): VirtualEntity01 {
        return new VirtualEntity01(data);
    }

    /**
     * Create
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof VirtualEntity01Service
     */
    async Create(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        _data = await this.beforeExecuteAction(_context,_data,'Create');
        if (!_data.srffrontuf || _data.srffrontuf != 1) {
            _data[this.APPDEKEY] = null;
        }
        if (_data.srffrontuf != null) {
            delete _data.srffrontuf;
        }
    _data = await VirtualEntity01DTOHelp.get(_context,_data);
        const res = await this.http.post(`/virtualentity01s`, _data);
        res.data = await VirtualEntity01DTOHelp.set(_context,res.data);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Get
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof VirtualEntity01Service
     */
    async Get(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.get(`/virtualentity01s/${encodeURIComponent(_context.virtualentity01)}`, _data);
        res.data = await VirtualEntity01DTOHelp.set(_context,res.data);
        res.data = await this.afterExecuteAction(_context,res?.data,'Get');
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * GetDraft
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof VirtualEntity01Service
     */
    async GetDraft(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        _data[this.APPDENAME?.toLowerCase()] = undefined;
        _data[this.APPDEKEY] = undefined;
        const res = await this.http.get(`/virtualentity01s/getdraft`, _data);
        res.data = await VirtualEntity01DTOHelp.set(_context,res.data);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Remove
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof VirtualEntity01Service
     */
    async Remove(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.delete(`/virtualentity01s/${encodeURIComponent(_context.virtualentity01)}`);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Update
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof VirtualEntity01Service
     */
    async Update(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        _data = await this.beforeExecuteAction(_context,_data,'Update');
    _data = await VirtualEntity01DTOHelp.get(_context,_data);
        const res = await this.http.put(`/virtualentity01s/${encodeURIComponent(_context.virtualentity01)}`, _data);
        res.data = await VirtualEntity01DTOHelp.set(_context,res.data);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * FetchDefault
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof VirtualEntity01Service
     */
    async FetchDefault(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.post(`/virtualentity01s/fetchdefault`, _data);
        res.data = await VirtualEntity01DTOHelp.ToDataObjArray(_context,res.data);
        res.data = await this.afterExecuteActionBatch(_context,res?.data,'FetchDefault');
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Select
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof VirtualEntity01Service
     */
    async Select(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.get(`/virtualentity01s/${encodeURIComponent(_context.virtualentity01)}/select`);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
}
