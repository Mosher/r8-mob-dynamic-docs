// import { CodeListService } from '../app/codelist-service';
import { EntityBaseService } from '../../service-base';
import { IContext } from 'ibz-core';
import { HttpResponse } from '../../utils/http-response';
import { IVirtualEntity03 } from './interface/ivirtual-entity03';
import { VirtualEntity03 } from './virtual-entity03';
import keys from './virtual-entity03-keys';
import { VirtualEntity03DTOHelp } from './virtual-entity03-dto-help';


/**
 * 虚拟实体03服务对象基类
 *
 * @export
 * @class VirtualEntity03BaseService
 * @extends {EntityBaseService}
 */
export class VirtualEntity03BaseService extends EntityBaseService<IVirtualEntity03> {
    protected get keys(): string[] {
        return keys;
    }
    protected SYSTEMNAME = 'StudioPreview';
    protected APPNAME = 'TestMob';
    protected APPDENAME = 'VirtualEntity03';
    protected APPDENAMEPLURAL = 'VirtualEntity03s';
    protected dynaModelFilePath:string = 'PSSYSAPPS/TestMob/PSAPPDATAENTITIES/VirtualEntity03.json';
    protected APPDEKEY = 'virtualentity03id';
    protected APPDETEXT = 'virtualentity03name';
    protected quickSearchFields = ['virtualentity03name',];
    protected selectContextParam = {
    };

    constructor(opts?: any) {
        super(opts, 'VirtualEntity03');
    }

    newEntity(data: IVirtualEntity03): VirtualEntity03 {
        return new VirtualEntity03(data);
    }

    /**
     * Create
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof VirtualEntity03Service
     */
    async Create(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        _data = await this.beforeExecuteAction(_context,_data,'Create');
        if (!_data.srffrontuf || _data.srffrontuf != 1) {
            _data[this.APPDEKEY] = null;
        }
        if (_data.srffrontuf != null) {
            delete _data.srffrontuf;
        }
    _data = await VirtualEntity03DTOHelp.get(_context,_data);
        const res = await this.http.post(`/virtualentity03s`, _data);
        res.data = await VirtualEntity03DTOHelp.set(_context,res.data);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Get
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof VirtualEntity03Service
     */
    async Get(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.get(`/virtualentity03s/${encodeURIComponent(_context.virtualentity03)}`, _data);
        res.data = await VirtualEntity03DTOHelp.set(_context,res.data);
        res.data = await this.afterExecuteAction(_context,res?.data,'Get');
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * GetDraft
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof VirtualEntity03Service
     */
    async GetDraft(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        _data[this.APPDENAME?.toLowerCase()] = undefined;
        _data[this.APPDEKEY] = undefined;
        const res = await this.http.get(`/virtualentity03s/getdraft`, _data);
        res.data = await VirtualEntity03DTOHelp.set(_context,res.data);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Remove
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof VirtualEntity03Service
     */
    async Remove(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.delete(`/virtualentity03s/${encodeURIComponent(_context.virtualentity03)}`);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Update
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof VirtualEntity03Service
     */
    async Update(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        _data = await this.beforeExecuteAction(_context,_data,'Update');
    _data = await VirtualEntity03DTOHelp.get(_context,_data);
        const res = await this.http.put(`/virtualentity03s/${encodeURIComponent(_context.virtualentity03)}`, _data);
        res.data = await VirtualEntity03DTOHelp.set(_context,res.data);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * FetchDefault
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof VirtualEntity03Service
     */
    async FetchDefault(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.post(`/virtualentity03s/fetchdefault`, _data);
        res.data = await VirtualEntity03DTOHelp.ToDataObjArray(_context,res.data);
        res.data = await this.afterExecuteActionBatch(_context,res?.data,'FetchDefault');
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Select
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof VirtualEntity03Service
     */
    async Select(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.get(`/virtualentity03s/${encodeURIComponent(_context.virtualentity03)}/select`);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
}
