import { VirtualEntity03BaseService } from './virtual-entity03-base.service';

/**
 * 虚拟实体03服务
 *
 * @export
 * @class VirtualEntity03Service
 * @extends {VirtualEntity03BaseService}
 */
export class VirtualEntity03Service extends VirtualEntity03BaseService {
    /**
     * Creates an instance of VirtualEntity03Service.
     * @memberof VirtualEntity03Service
     */
    constructor(opts?: any) {
        const { context: context, tag: cacheKey } = opts;
        super(context);
        ServiceCache.set(cacheKey, this);
    }

    /**
     * 获取实例
     *
     * @static
     * @param 应用上下文
     * @return {*}  {VirtualEntity03Service}
     * @memberof VirtualEntity03Service
     */
    static getInstance(context?: any): VirtualEntity03Service {
        const cacheKey: string = context?.srfdynainstid ? `${context.srfdynainstid}VirtualEntity03Service` : `VirtualEntity03Service`;
        if (!ServiceCache.has(cacheKey)) {
            return new VirtualEntity03Service({ context: context, tag: cacheKey });
        }
        return ServiceCache.get(cacheKey);
    }
}
export default VirtualEntity03Service;
