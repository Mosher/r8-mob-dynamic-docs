import { IEntityBase } from "ibz-core";

/**
 * 虚拟实体03
 *
 * @export
 * @interface IVirtualEntity03
 * @extends {IEntityBase}
 */
export interface IVirtualEntity03 extends IEntityBase {
    /**
     * 更新时间
     */
    updatedate?: any;
    /**
     * 更新人
     */
    updateman?: any;
    /**
     * 虚拟实体03名称
     */
    virtualentity03name?: any;
    /**
     * 建立人
     */
    createman?: any;
    /**
     * 建立时间
     */
    createdate?: any;
    /**
     * 虚拟实体03标识
     */
    virtualentity03id?: any;
    /**
     * 备注03
     */
    memo03?: any;
}
