import { EntityBase } from '../../entities';
import { IVirtualEntity03 } from './interface/ivirtual-entity03';

/**
 * 虚拟实体03基类
 *
 * @export
 * @abstract
 * @class VirtualEntity03Base
 * @extends {EntityBase}
 * @implements {IVirtualEntity03}
 */
export abstract class VirtualEntity03Base extends EntityBase implements IVirtualEntity03 {
    /**
     * 实体名称
     *
     * @readonly
     * @type {string}
     * @memberof VirtualEntity03Base
     */
    get srfdename(): string {
        return 'VIRTUALENTITY03';
    }
    get srfkey() {
        return this.virtualentity03id;
    }
    set srfkey(val: any) {
        this.virtualentity03id = val;
    }
    get srfmajortext() {
        return this.virtualentity03name;
    }
    set srfmajortext(val: any) {
        this.virtualentity03name = val;
    }
    /**
     * 更新时间
     */
    updatedate?: any;
    /**
     * 更新人
     */
    updateman?: any;
    /**
     * 虚拟实体03名称
     */
    virtualentity03name?: any;
    /**
     * 建立人
     */
    createman?: any;
    /**
     * 建立时间
     */
    createdate?: any;
    /**
     * 虚拟实体03标识
     */
    virtualentity03id?: any;
    /**
     * 备注03
     */
    memo03?: any;

    /**
     * 重置实体数据
     *
     * @private
     * @param {*} [data={}]
     * @memberof VirtualEntity03Base
     */
    reset(data: any = {}): void {
        super.reset(data);
        this.virtualentity03id = data.virtualentity03id || data.srfkey;
        this.virtualentity03name = data.virtualentity03name || data.srfmajortext;
    }
}
