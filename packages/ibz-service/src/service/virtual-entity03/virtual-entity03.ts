import { VirtualEntity03Base } from './virtual-entity03-base';

/**
 * 虚拟实体03
 *
 * @export
 * @class VirtualEntity03
 * @extends {VirtualEntity03Base}
 * @implements {IVirtualEntity03}
 */
export class VirtualEntity03 extends VirtualEntity03Base {

    /**
     * 克隆当前实体
     *
     * @return {*}
     * @memberof VirtualEntity03
     */
    clone(): VirtualEntity03 {
        return new VirtualEntity03(this);
    }
}
export default VirtualEntity03;
