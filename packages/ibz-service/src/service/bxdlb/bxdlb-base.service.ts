// import { CodeListService } from '../app/codelist-service';
import { EntityBaseService } from '../../service-base';
import { IContext } from 'ibz-core';
import { HttpResponse } from '../../utils/http-response';
import { IBXDLB } from './interface/ibxdlb';
import { BXDLB } from './bxdlb';
import keys from './bxdlb-keys';
import { BXDLBDTOHelp } from './bxdlbdto-help';


/**
 * 报销单类别服务对象基类
 *
 * @export
 * @class BXDLBBaseService
 * @extends {EntityBaseService}
 */
export class BXDLBBaseService extends EntityBaseService<IBXDLB> {
    protected get keys(): string[] {
        return keys;
    }
    protected SYSTEMNAME = 'StudioPreview';
    protected APPNAME = 'TestMob';
    protected APPDENAME = 'BXDLB';
    protected APPDENAMEPLURAL = 'BXDLBs';
    protected dynaModelFilePath:string = 'PSSYSAPPS/TestMob/PSAPPDATAENTITIES/BXDLB.json';
    protected APPDEKEY = 'bxdlbid';
    protected APPDETEXT = 'bxdlbname';
    protected quickSearchFields = ['bxdlbname',];
    protected selectContextParam = {
    };

    constructor(opts?: any) {
        super(opts, 'BXDLB');
    }

    newEntity(data: IBXDLB): BXDLB {
        return new BXDLB(data);
    }

    /**
     * Create
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof BXDLBService
     */
    async Create(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        _data = await this.beforeExecuteAction(_context,_data,'Create');
        if (!_data.srffrontuf || _data.srffrontuf != 1) {
            _data[this.APPDEKEY] = null;
        }
        if (_data.srffrontuf != null) {
            delete _data.srffrontuf;
        }
    _data = await BXDLBDTOHelp.get(_context,_data);
        const res = await this.http.post(`/bxdlbs`, _data);
        res.data = await BXDLBDTOHelp.set(_context,res.data);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Get
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof BXDLBService
     */
    async Get(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.get(`/bxdlbs/${encodeURIComponent(_context.bxdlb)}`, _data);
        res.data = await BXDLBDTOHelp.set(_context,res.data);
        res.data = await this.afterExecuteAction(_context,res?.data,'Get');
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * GetDraft
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof BXDLBService
     */
    async GetDraft(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        _data[this.APPDENAME?.toLowerCase()] = undefined;
        _data[this.APPDEKEY] = undefined;
        const res = await this.http.get(`/bxdlbs/getdraft`, _data);
        res.data = await BXDLBDTOHelp.set(_context,res.data);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Remove
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof BXDLBService
     */
    async Remove(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.delete(`/bxdlbs/${encodeURIComponent(_context.bxdlb)}`);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Update
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof BXDLBService
     */
    async Update(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        _data = await this.beforeExecuteAction(_context,_data,'Update');
    _data = await BXDLBDTOHelp.get(_context,_data);
        const res = await this.http.put(`/bxdlbs/${encodeURIComponent(_context.bxdlb)}`, _data);
        res.data = await BXDLBDTOHelp.set(_context,res.data);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * FetchDefault
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof BXDLBService
     */
    async FetchDefault(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.post(`/bxdlbs/fetchdefault`, _data);
        res.data = await BXDLBDTOHelp.ToDataObjArray(_context,res.data);
        res.data = await this.afterExecuteActionBatch(_context,res?.data,'FetchDefault');
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Select
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof BXDLBService
     */
    async Select(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.get(`/bxdlbs/${encodeURIComponent(_context.bxdlb)}/select`);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
}
