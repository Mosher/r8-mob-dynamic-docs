import { IEntityBase } from "ibz-core";

/**
 * 报销单类别
 *
 * @export
 * @interface IBXDLB
 * @extends {IEntityBase}
 */
export interface IBXDLB extends IEntityBase {
    /**
     * 报销单类别标识
     */
    bxdlbid?: any;
    /**
     * 报销单类别名称
     */
    bxdlbname?: any;
    /**
     * 建立时间
     */
    createdate?: any;
    /**
     * 更新人
     */
    updateman?: any;
    /**
     * 建立人
     */
    createman?: any;
    /**
     * 更新时间
     */
    updatedate?: any;
    /**
     * 测试3标识
     */
    test3id?: any;
    /**
     * 测试3名称
     */
    test3name?: any;
}
