import { EntityBase } from '../../entities';
import { IBXDLB } from './interface/ibxdlb';

/**
 * 报销单类别基类
 *
 * @export
 * @abstract
 * @class BXDLBBase
 * @extends {EntityBase}
 * @implements {IBXDLB}
 */
export abstract class BXDLBBase extends EntityBase implements IBXDLB {
    /**
     * 实体名称
     *
     * @readonly
     * @type {string}
     * @memberof BXDLBBase
     */
    get srfdename(): string {
        return 'BXDLB';
    }
    get srfkey() {
        return this.bxdlbid;
    }
    set srfkey(val: any) {
        this.bxdlbid = val;
    }
    get srfmajortext() {
        return this.bxdlbname;
    }
    set srfmajortext(val: any) {
        this.bxdlbname = val;
    }
    /**
     * 报销单类别标识
     */
    bxdlbid?: any;
    /**
     * 报销单类别名称
     */
    bxdlbname?: any;
    /**
     * 建立时间
     */
    createdate?: any;
    /**
     * 更新人
     */
    updateman?: any;
    /**
     * 建立人
     */
    createman?: any;
    /**
     * 更新时间
     */
    updatedate?: any;
    /**
     * 测试3标识
     */
    test3id?: any;
    /**
     * 测试3名称
     */
    test3name?: any;

    /**
     * 重置实体数据
     *
     * @private
     * @param {*} [data={}]
     * @memberof BXDLBBase
     */
    reset(data: any = {}): void {
        super.reset(data);
        this.bxdlbid = data.bxdlbid || data.srfkey;
        this.bxdlbname = data.bxdlbname || data.srfmajortext;
    }
}
