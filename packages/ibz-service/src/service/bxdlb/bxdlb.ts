import { BXDLBBase } from './bxdlb-base';

/**
 * 报销单类别
 *
 * @export
 * @class BXDLB
 * @extends {BXDLBBase}
 * @implements {IBXDLB}
 */
export class BXDLB extends BXDLBBase {

    /**
     * 克隆当前实体
     *
     * @return {*}
     * @memberof BXDLB
     */
    clone(): BXDLB {
        return new BXDLB(this);
    }
}
export default BXDLB;
