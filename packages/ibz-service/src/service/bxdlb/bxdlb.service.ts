import { BXDLBBaseService } from './bxdlb-base.service';

/**
 * 报销单类别服务
 *
 * @export
 * @class BXDLBService
 * @extends {BXDLBBaseService}
 */
export class BXDLBService extends BXDLBBaseService {
    /**
     * Creates an instance of BXDLBService.
     * @memberof BXDLBService
     */
    constructor(opts?: any) {
        const { context: context, tag: cacheKey } = opts;
        super(context);
        ServiceCache.set(cacheKey, this);
    }

    /**
     * 获取实例
     *
     * @static
     * @param 应用上下文
     * @return {*}  {BXDLBService}
     * @memberof BXDLBService
     */
    static getInstance(context?: any): BXDLBService {
        const cacheKey: string = context?.srfdynainstid ? `${context.srfdynainstid}BXDLBService` : `BXDLBService`;
        if (!ServiceCache.has(cacheKey)) {
            return new BXDLBService({ context: context, tag: cacheKey });
        }
        return ServiceCache.get(cacheKey);
    }
}
export default BXDLBService;
