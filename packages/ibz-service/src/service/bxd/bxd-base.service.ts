// import { CodeListService } from '../app/codelist-service';
import { EntityBaseService } from '../../service-base';
import { IContext } from 'ibz-core';
import { HttpResponse } from '../../utils/http-response';
import { IBXD } from './interface/ibxd';
import { BXD } from './bxd';
import keys from './bxd-keys';
import { BXDDTOHelp } from './bxddto-help';


/**
 * 报销单服务对象基类
 *
 * @export
 * @class BXDBaseService
 * @extends {EntityBaseService}
 */
export class BXDBaseService extends EntityBaseService<IBXD> {
    protected get keys(): string[] {
        return keys;
    }
    protected SYSTEMNAME = 'StudioPreview';
    protected APPNAME = 'TestMob';
    protected APPDENAME = 'BXD';
    protected APPDENAMEPLURAL = 'BXDs';
    protected dynaModelFilePath:string = 'PSSYSAPPS/TestMob/PSAPPDATAENTITIES/BXD.json';
    protected APPDEKEY = 'bxdid';
    protected APPDETEXT = 'bxdname';
    protected quickSearchFields = ['bxdname',];
    protected selectContextParam = {
        bxdlb: 'bxdlbid',
        project: 'projectid',
    };

    constructor(opts?: any) {
        super(opts, 'BXD');
    }

    newEntity(data: IBXD): BXD {
        return new BXD(data);
    }

    /**
     * Create
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof BXDService
     */
    async Create(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        if (_context.project && true) {
        _data = await this.beforeExecuteAction(_context,_data,'Create');
            if (!_data.srffrontuf || _data.srffrontuf != 1) {
                _data[this.APPDEKEY] = null;
            }
            if (_data.srffrontuf != null) {
                delete _data.srffrontuf;
            }
    _data = await BXDDTOHelp.get(_context,_data);
            const res = await this.http.post(`/projects/${encodeURIComponent(_context.project)}/bxds`, _data);
        res.data = await BXDDTOHelp.set(_context,res.data);
            return res;
        }
        if (_context.bxdlb && true) {
        _data = await this.beforeExecuteAction(_context,_data,'Create');
            if (!_data.srffrontuf || _data.srffrontuf != 1) {
                _data[this.APPDEKEY] = null;
            }
            if (_data.srffrontuf != null) {
                delete _data.srffrontuf;
            }
    _data = await BXDDTOHelp.get(_context,_data);
            const res = await this.http.post(`/bxdlbs/${encodeURIComponent(_context.bxdlb)}/bxds`, _data);
        res.data = await BXDDTOHelp.set(_context,res.data);
            return res;
        }
        _data = await this.beforeExecuteAction(_context,_data,'Create');
        if (!_data.srffrontuf || _data.srffrontuf != 1) {
            _data[this.APPDEKEY] = null;
        }
        if (_data.srffrontuf != null) {
            delete _data.srffrontuf;
        }
    _data = await BXDDTOHelp.get(_context,_data);
        const res = await this.http.post(`/bxds`, _data);
        res.data = await BXDDTOHelp.set(_context,res.data);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Get
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof BXDService
     */
    async Get(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        if (_context.project && _context.bxd) {
            const res = await this.http.get(`/projects/${encodeURIComponent(_context.project)}/bxds/${encodeURIComponent(_context.bxd)}`, _data);
        res.data = await BXDDTOHelp.set(_context,res.data);
        res.data = await this.afterExecuteAction(_context,res?.data,'Get');
            return res;
        }
        if (_context.bxdlb && _context.bxd) {
            const res = await this.http.get(`/bxdlbs/${encodeURIComponent(_context.bxdlb)}/bxds/${encodeURIComponent(_context.bxd)}`, _data);
        res.data = await BXDDTOHelp.set(_context,res.data);
        res.data = await this.afterExecuteAction(_context,res?.data,'Get');
            return res;
        }
        const res = await this.http.get(`/bxds/${encodeURIComponent(_context.bxd)}`, _data);
        res.data = await BXDDTOHelp.set(_context,res.data);
        res.data = await this.afterExecuteAction(_context,res?.data,'Get');
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * GetDraft
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof BXDService
     */
    async GetDraft(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        if (_context.project && true) {
            _data[this.APPDENAME?.toLowerCase()] = undefined;
            _data[this.APPDEKEY] = undefined;
            const res = await this.http.get(`/projects/${encodeURIComponent(_context.project)}/bxds/getdraft`, _data);
        res.data = await BXDDTOHelp.set(_context,res.data);
            return res;
        }
        if (_context.bxdlb && true) {
            _data[this.APPDENAME?.toLowerCase()] = undefined;
            _data[this.APPDEKEY] = undefined;
            const res = await this.http.get(`/bxdlbs/${encodeURIComponent(_context.bxdlb)}/bxds/getdraft`, _data);
        res.data = await BXDDTOHelp.set(_context,res.data);
            return res;
        }
        _data[this.APPDENAME?.toLowerCase()] = undefined;
        _data[this.APPDEKEY] = undefined;
        const res = await this.http.get(`/bxds/getdraft`, _data);
        res.data = await BXDDTOHelp.set(_context,res.data);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * GetRandom
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof BXDService
     */
    async GetRandom(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        if (_context.project && _context.bxd) {
            const res = await this.http.get(`/projects/${encodeURIComponent(_context.project)}/bxds/${encodeURIComponent(_context.bxd)}/getrandom`, _data);
        res.data = await BXDDTOHelp.set(_context,res.data);
        res.data = await this.afterExecuteAction(_context,res?.data,'GetRandom');
            return res;
        }
        if (_context.bxdlb && _context.bxd) {
            const res = await this.http.get(`/bxdlbs/${encodeURIComponent(_context.bxdlb)}/bxds/${encodeURIComponent(_context.bxd)}/getrandom`, _data);
        res.data = await BXDDTOHelp.set(_context,res.data);
        res.data = await this.afterExecuteAction(_context,res?.data,'GetRandom');
            return res;
        }
        const res = await this.http.get(`/bxds/${encodeURIComponent(_context.bxd)}/getrandom`, _data);
        res.data = await BXDDTOHelp.set(_context,res.data);
        res.data = await this.afterExecuteAction(_context,res?.data,'GetRandom');
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * GetTabViewRandom
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof BXDService
     */
    async GetTabViewRandom(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        if (_context.project && _context.bxd) {
            const res = await this.http.get(`/projects/${encodeURIComponent(_context.project)}/bxds/${encodeURIComponent(_context.bxd)}/gettabviewrandom`, _data);
        res.data = await BXDDTOHelp.set(_context,res.data);
        res.data = await this.afterExecuteAction(_context,res?.data,'GetTabViewRandom');
            return res;
        }
        if (_context.bxdlb && _context.bxd) {
            const res = await this.http.get(`/bxdlbs/${encodeURIComponent(_context.bxdlb)}/bxds/${encodeURIComponent(_context.bxd)}/gettabviewrandom`, _data);
        res.data = await BXDDTOHelp.set(_context,res.data);
        res.data = await this.afterExecuteAction(_context,res?.data,'GetTabViewRandom');
            return res;
        }
        const res = await this.http.get(`/bxds/${encodeURIComponent(_context.bxd)}/gettabviewrandom`, _data);
        res.data = await BXDDTOHelp.set(_context,res.data);
        res.data = await this.afterExecuteAction(_context,res?.data,'GetTabViewRandom');
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Remove
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof BXDService
     */
    async Remove(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        if (_context.project && _context.bxd) {
            const res = await this.http.delete(`/projects/${encodeURIComponent(_context.project)}/bxds/${encodeURIComponent(_context.bxd)}`);
            return res;
        }
        if (_context.bxdlb && _context.bxd) {
            const res = await this.http.delete(`/bxdlbs/${encodeURIComponent(_context.bxdlb)}/bxds/${encodeURIComponent(_context.bxd)}`);
            return res;
        }
        const res = await this.http.delete(`/bxds/${encodeURIComponent(_context.bxd)}`);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * TestDeLogic
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof BXDService
     */
    async TestDeLogic(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
            _data =  await this.executeAppDELogic('TestBxdProcessLogic',_context,_data);
            return new HttpResponse(_data, {
                ok: true,
                status: 200
            });
        }catch (error) {
            return new HttpResponse({message:error.message}, {
                ok: false,
                status: 500,
            });
        }
    }
    /**
     * Update
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof BXDService
     */
    async Update(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        if (_context.project && _context.bxd) {
        _data = await this.beforeExecuteAction(_context,_data,'Update');
    _data = await BXDDTOHelp.get(_context,_data);
            const res = await this.http.put(`/projects/${encodeURIComponent(_context.project)}/bxds/${encodeURIComponent(_context.bxd)}`, _data);
        res.data = await BXDDTOHelp.set(_context,res.data);
            return res;
        }
        if (_context.bxdlb && _context.bxd) {
        _data = await this.beforeExecuteAction(_context,_data,'Update');
    _data = await BXDDTOHelp.get(_context,_data);
            const res = await this.http.put(`/bxdlbs/${encodeURIComponent(_context.bxdlb)}/bxds/${encodeURIComponent(_context.bxd)}`, _data);
        res.data = await BXDDTOHelp.set(_context,res.data);
            return res;
        }
        _data = await this.beforeExecuteAction(_context,_data,'Update');
    _data = await BXDDTOHelp.get(_context,_data);
        const res = await this.http.put(`/bxds/${encodeURIComponent(_context.bxd)}`, _data);
        res.data = await BXDDTOHelp.set(_context,res.data);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * FetchDefault
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof BXDService
     */
    async FetchDefault(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        if (_context.project && true) {
            const res = await this.http.post(`/projects/${encodeURIComponent(_context.project)}/bxds/fetchdefault`, _data);
        res.data = await BXDDTOHelp.ToDataObjArray(_context,res.data);
        res.data = await this.afterExecuteActionBatch(_context,res?.data,'FetchDefault');
            return res;
        }
        if (_context.bxdlb && true) {
            const res = await this.http.post(`/bxdlbs/${encodeURIComponent(_context.bxdlb)}/bxds/fetchdefault`, _data);
        res.data = await BXDDTOHelp.ToDataObjArray(_context,res.data);
        res.data = await this.afterExecuteActionBatch(_context,res?.data,'FetchDefault');
            return res;
        }
        const res = await this.http.post(`/bxds/fetchdefault`, _data);
        res.data = await BXDDTOHelp.ToDataObjArray(_context,res.data);
        res.data = await this.afterExecuteActionBatch(_context,res?.data,'FetchDefault');
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Select
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof BXDService
     */
    async Select(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        if (_context.project && _context.bxd) {
            const res = await this.http.get(`/projects/${encodeURIComponent(_context.project)}/bxds/${encodeURIComponent(_context.bxd)}/select`);
            return res;
        }
        if (_context.bxdlb && _context.bxd) {
            const res = await this.http.get(`/bxdlbs/${encodeURIComponent(_context.bxdlb)}/bxds/${encodeURIComponent(_context.bxd)}/select`);
            return res;
        }
        const res = await this.http.get(`/bxds/${encodeURIComponent(_context.bxd)}/select`);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }

    /**
     * GetRandomBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof BXDServiceBase
     */
    public async GetRandomBatch(_context: IContext = {},_data: any = {}): Promise<HttpResponse> {
        if(_context.project && true){
            const res = await this.http.post(`/projects/${_context.project}/bxds/getrandombatch`,_data);
            return res;
        }
        if(_context.bxdlb && true){
            const res = await this.http.post(`/bxdlbs/${_context.bxdlb}/bxds/getrandombatch`,_data);
            return res;
        }
        const res = await this.http.post(`/bxds/getrandombatch`,_data);
        return res;
    }

    /**
     * GetTabViewRandomBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof BXDServiceBase
     */
    public async GetTabViewRandomBatch(_context: IContext = {},_data: any = {}): Promise<HttpResponse> {
        if(_context.project && true){
            const res = await this.http.post(`/projects/${_context.project}/bxds/gettabviewrandombatch`,_data);
            return res;
        }
        if(_context.bxdlb && true){
            const res = await this.http.post(`/bxdlbs/${_context.bxdlb}/bxds/gettabviewrandombatch`,_data);
            return res;
        }
        const res = await this.http.post(`/bxds/gettabviewrandombatch`,_data);
        return res;
    }
}
