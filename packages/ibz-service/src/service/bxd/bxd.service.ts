import { IContext } from 'ibz-core';
import { HttpResponse } from '../../utils/http-response';
import { BXDBaseService } from './bxd-base.service';

/**
 * 报销单服务
 *
 * @export
 * @class BXDService
 * @extends {BXDBaseService}
 */
export class BXDService extends BXDBaseService {
    /**
     * Creates an instance of BXDService.
     * @memberof BXDService
     */
    constructor(opts?: any) {
        const { context: context, tag: cacheKey } = opts;
        super(context);
        ServiceCache.set(cacheKey, this);
    }

    /**
     * 获取实例
     *
     * @static
     * @param 应用上下文
     * @return {*}  {BXDService}
     * @memberof BXDService
     */
    static getInstance(context?: any): BXDService {
        const cacheKey: string = context?.srfdynainstid ? `${context.srfdynainstid}BXDService` : `BXDService`;
        if (!ServiceCache.has(cacheKey)) {
            return new BXDService({ context: context, tag: cacheKey });
        }
        return ServiceCache.get(cacheKey);
    }
    /**
     * GetRandom
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof CUSTOMService
     */
    async GetRandom(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        eval(`_data.countertag=Math.floor(Math.random()*100+1);
        _data.countertag2=Math.floor(Math.random()*100+1);
        _data.countertag3=Math.floor(Math.random()*100+1);
        console.log(_data);`);
        return new HttpResponse(_data, {
          ok: true,
          status: 200,
        });
    }

    async FetchDefault(context: IContext = {}, data: any = {}): Promise<HttpResponse> {
        const res: any = { ok: true, status: 200, data: [
            {
                "updatedate" : null,
                "field2" : "属性2",
                "projectname" : "项目1",
                "field1" : "属性11",
                "createman" : null,
                "bxdlbname" : "报销单类别1",
                "updateman" : null,
                "bxdlbid" : "123",
                "createdate" : null,
                "field3" : null,
                "bxdname" : "报销单1",
                "bxdid" : "1",
                "projectid" : null
            }
        ]}
        return res;
    }
}
export default BXDService;
