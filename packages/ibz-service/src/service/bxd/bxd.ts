import { BXDBase } from './bxd-base';

/**
 * 报销单
 *
 * @export
 * @class BXD
 * @extends {BXDBase}
 * @implements {IBXD}
 */
export class BXD extends BXDBase {

    /**
     * 克隆当前实体
     *
     * @return {*}
     * @memberof BXD
     */
    clone(): BXD {
        return new BXD(this);
    }
}
export default BXD;
