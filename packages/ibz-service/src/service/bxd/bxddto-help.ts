import { IEntityLocalDataService, IContext, IParam } from 'ibz-core';
import { IBXD } from './interface/ibxd';
import { BXDMXDTOHelp } from '../bxdmx/bxdmxdto-help';
/**
 * BXDDTOdto辅助类
 *
 * @export
 * @class BXDDTOHelp
 */
export class BXDDTOHelp {

    /**
     * 获取数据服务
     *
     * @param {IContext} context 应用上下文对象
     * @returns {*}
     * @memberof BXDDTOHelp
     */
    public static async getService(context: IContext): Promise<IEntityLocalDataService<IBXD>> {
        return await App.getEntityService().getService(context, 'bxd') as IEntityLocalDataService<IBXD>;
    }

    /**
     * DTO转化成数据对象
     *
     * @param {IContext} context 应用上下文对象
     * @param {IParam} source dto对象
     * @returns {*}
     * @memberof BXDDTOHelp
     */
    public static async ToDataObj(context: IContext, source: IParam) {
        const _data: any = {};
        // 报销单标识
        _data.bxdid = source.bxdid;
        // 报销单类别标识
        _data.bxdlbid = source.bxdlbid;
        // 报销单类别名称
        _data.bxdlbname = source.bxdlbname;
        // 报销单名称
        _data.bxdname = source.bxdname;
        // 背景颜色
        _data.bkcolor = source.bkcolor;
        // 内容
        _data.content = source.content;
        // 建立时间
        _data.createdate = source.createdate;
        // 建立人
        _data.createman = source.createman;
        // 数据1
        _data.data1 = source.data1;
        // 数据2
        _data.data2 = source.data2;
        // 整型
        _data.field1 = source.field1;
        // 大整型
        _data.field2 = source.field2;
        // 浮点
        _data.field3 = source.field3;
        // 分组
        _data.group = source.group;
        // 高度
        _data.high = source.high;
        // 图标
        _data.icon = source.icon;
        // 纬度
        _data.latitude = source.latitude;
        // 经度
        _data.longitude = source.longitude;
        // 项目标识
        _data.projectid = source.projectid;
        // 项目名称
        _data.projectname = source.projectname;
        // 排序
        _data.sort = source.sort;
        // 文本属性
        _data.text = source.text;
        // 字体颜色
        _data.textcolor = source.textcolor;
        // 提示
        _data.tip = source.tip;
        // 更新时间
        _data.updatedate = source.updatedate;
        // 更新人
        _data.updateman = source.updateman;
        // bxdmxs
        if (source?.bxdmxs?.length > 0) {
            await BXDMXDTOHelp.setCacheDataArray(context, source.bxdmxs);
        }
        return _data;
    }

    /**
     * 转化数组(dto转化成数据对象)
     *
     * @param {IContext} context 应用上下文对象
     * @param {any[]} data 数据对象
     * @returns {any[]}
     * @memberof BXDDTOHelp
     */
    public static async ToDataObjArray(context: IContext, data: any[]) {
        const _data: any[] = [];
        if (data && Array.isArray(data) && data.length > 0) {
            for (let i = 0; i < data.length; i++) {
                const tempdata = await this.ToDataObj(context, data[i]);
                _data.push(tempdata);
            }
        }
        return _data;
    }

    /**
     * 数据对象转化成DTO
     *
     * @param {IContext} context 应用上下文对象
     * @param {*} source 数据对象
     * @returns {*}
     * @memberof BXDDTOHelp
     */
    public static async ToDto(context: IContext, source: IParam) {
        const _data: any = {};
        // 报销单标识
        _data.bxdid = source.bxdid;
        // 报销单类别标识
        _data.bxdlbid = source.bxdlbid;
        // 报销单类别名称
        _data.bxdlbname = source.bxdlbname;
        // 报销单名称
        _data.bxdname = source.bxdname;
        // 背景颜色
        _data.bkcolor = source.bkcolor;
        // 内容
        _data.content = source.content;
        // 建立时间
        _data.createdate = source.createdate;
        // 建立人
        _data.createman = source.createman;
        // 数据1
        _data.data1 = source.data1;
        // 数据2
        _data.data2 = source.data2;
        // 整型
        _data.field1 = source.field1;
        // 大整型
        _data.field2 = source.field2;
        // 浮点
        _data.field3 = source.field3;
        // 分组
        _data.group = source.group;
        // 高度
        _data.high = source.high;
        // 图标
        _data.icon = source.icon;
        // 纬度
        _data.latitude = source.latitude;
        // 经度
        _data.longitude = source.longitude;
        // 项目标识
        _data.projectid = source.projectid;
        // 项目名称
        _data.projectname = source.projectname;
        // 排序
        _data.sort = source.sort;
        // 文本属性
        _data.text = source.text;
        // 字体颜色
        _data.textcolor = source.textcolor;
        // 提示
        _data.tip = source.tip;
        // 更新时间
        _data.updatedate = source.updatedate;
        // 更新人
        _data.updateman = source.updateman;
        // bxdmxs
        _data.bxdmxs = await BXDMXDTOHelp.getCacheDataArray(context);
        return _data;
    }

    /**
     * 转化数组(数据对象转化成dto)
     *
     * @param {IContext} context 应用上下文对象
     * @param {any[]} data 
     * @returns {any[]}
     * @memberof BXDDTOHelp
     */
    public static async ToDtoArray(context: IContext, data: any[]) {
        const _data: any[] = [];
        if (data && Array.isArray(data) && data.length > 0) {
            for (let i = 0; i < data.length; i++) {
                const tempdata = await this.ToDto(context, data[i]);
                _data.push(tempdata);
            }
        }
        return _data;
    }

    /**
     * 处理响应dto对象
     *
     * @param {*} context 应用上下文对象
     * @param {*} data 响应dto对象
     * @returns {*}
     * @memberof BXDDTOHelp
     */
    public static async set(context: IContext, data: any) {
        const _data: IParam = await this.ToDataObj(context, data);
        return _data;
    }

    /**
     * 处理请求数据对象
     *
     * @param {*} context 应用上下文对象
     * @param {*} data 数据对象
     * @returns {*}
     * @memberof BXDDTOHelp
     */
    public static async get(context: IContext, data: any = {}) {
        return await this.ToDto(context, data);
    }

    /**
     * 获取缓存数据
     *
     * @param {*} context 应用上下文对象
     * @param {*} srfkey 数据主键
     * @returns {*}
     * @memberof BXDDTOHelp
     */
    public static async getCacheData(context: IContext, srfkey: string) {
        const targetService: IEntityLocalDataService<IBXD> = await this.getService(context);
        const result =  await targetService.getLocal(context, srfkey);
        if(result){
            return await this.ToDto(context,result);
        }
    }

    /**
     * 获取缓存数组
     *
     * @param {*} context 应用上下文对象
     * @returns {any[]}
     * @memberof BXDDTOHelp
     */
    public static async getCacheDataArray(context: IContext) {
        const targetService: IEntityLocalDataService<IBXD> = await this.getService(context);
        const result =  await targetService.getLocals(context);
        if(result && result.length >0){
            return await this.ToDtoArray(context,result);
        }else{
            return [];
        }
    }

    /**
     * 设置缓存数据
     *
     * @param {*} context 应用上下文对象
     * @param {any} data 数据
     * @returns {any[]}
     * @memberof BXDDTOHelp
     */
    public static async setCacheData(context: IContext, data: any) {
        const targetService: IEntityLocalDataService<IBXD> = await this.getService(context);
        const _data: any = await this.set(context, data);
        await targetService.addLocal(context, _data);
    }

    /**
     * 设置缓存数组
     *
     * @param {*} context 应用上下文对象
     * @param {any[]} data 数据
     * @returns {any[]}
     * @memberof BXDDTOHelp
     */
    public static async setCacheDataArray(context: IContext, data: any[]) {
        if (data && data.length > 0) {
            const targetService: IEntityLocalDataService<IBXD> = await this.getService(context);
            for (let i = 0; i < data.length; i++) {
                const _data = await this.set(context, data[i]);
                await targetService.addLocal(context, _data);
            }
        }
    }
}
