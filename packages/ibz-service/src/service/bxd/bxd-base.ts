import { EntityBase } from '../../entities';
import { IBXD } from './interface/ibxd';

/**
 * 报销单基类
 *
 * @export
 * @abstract
 * @class BXDBase
 * @extends {EntityBase}
 * @implements {IBXD}
 */
export abstract class BXDBase extends EntityBase implements IBXD {
    /**
     * 实体名称
     *
     * @readonly
     * @type {string}
     * @memberof BXDBase
     */
    get srfdename(): string {
        return 'BXD';
    }
    get srfkey() {
        return this.bxdid;
    }
    set srfkey(val: any) {
        this.bxdid = val;
    }
    get srfmajortext() {
        return this.bxdname;
    }
    set srfmajortext(val: any) {
        this.bxdname = val;
    }
    /**
     * 报销单标识
     */
    bxdid?: any;
    /**
     * 建立人
     */
    createman?: any;
    /**
     * 更新人
     */
    updateman?: any;
    /**
     * 建立时间
     */
    createdate?: any;
    /**
     * 报销单名称
     */
    bxdname?: any;
    /**
     * 更新时间
     */
    updatedate?: any;
    /**
     * 报销单类别标识
     */
    bxdlbid?: any;
    /**
     * 报销单类别名称
     */
    bxdlbname?: any;
    /**
     * 整型
     */
    field1?: any;
    /**
     * 大整型
     */
    field2?: any;
    /**
     * 浮点
     */
    field3?: any;
    /**
     * 项目标识
     */
    projectid?: any;
    /**
     * 项目名称
     */
    projectname?: any;
    /**
     * 经度
     */
    longitude?: any;
    /**
     * 纬度
     */
    latitude?: any;
    /**
     * 高度
     */
    high?: any;
    /**
     * 字体颜色
     */
    textcolor?: any;
    /**
     * 背景颜色
     */
    bkcolor?: any;
    /**
     * 文本属性
     */
    text?: any;
    /**
     * 提示
     */
    tip?: any;
    /**
     * 图标
     */
    icon?: any;
    /**
     * 分组
     */
    group?: any;
    /**
     * 排序
     */
    sort?: any;
    /**
     * 数据1
     */
    data1?: any;
    /**
     * 数据2
     */
    data2?: any;
    /**
     * 内容
     */
    content?: any;

    /**
     * 重置实体数据
     *
     * @private
     * @param {*} [data={}]
     * @memberof BXDBase
     */
    reset(data: any = {}): void {
        super.reset(data);
        this.bxdid = data.bxdid || data.srfkey;
        this.bxdname = data.bxdname || data.srfmajortext;
    }
}
