import { EntityBase } from '../../entities';
import { IProject } from './interface/iproject';

/**
 * 项目基类
 *
 * @export
 * @abstract
 * @class ProjectBase
 * @extends {EntityBase}
 * @implements {IProject}
 */
export abstract class ProjectBase extends EntityBase implements IProject {
    /**
     * 实体名称
     *
     * @readonly
     * @type {string}
     * @memberof ProjectBase
     */
    get srfdename(): string {
        return 'PROJECT';
    }
    get srfkey() {
        return this.projectid;
    }
    set srfkey(val: any) {
        this.projectid = val;
    }
    get srfmajortext() {
        return this.projectname;
    }
    set srfmajortext(val: any) {
        this.projectname = val;
    }
    /**
     * 更新时间
     */
    updatedate?: any;
    /**
     * 建立人
     */
    createman?: any;
    /**
     * 项目名称
     */
    projectname?: any;
    /**
     * 项目标识
     */
    projectid?: any;
    /**
     * 建立时间
     */
    createdate?: any;
    /**
     * 更新人
     */
    updateman?: any;

    /**
     * 重置实体数据
     *
     * @private
     * @param {*} [data={}]
     * @memberof ProjectBase
     */
    reset(data: any = {}): void {
        super.reset(data);
        this.projectid = data.projectid || data.srfkey;
        this.projectname = data.projectname || data.srfmajortext;
    }
}
