import { IEntityBase } from "ibz-core";

/**
 * 项目
 *
 * @export
 * @interface IProject
 * @extends {IEntityBase}
 */
export interface IProject extends IEntityBase {
    /**
     * 更新时间
     */
    updatedate?: any;
    /**
     * 建立人
     */
    createman?: any;
    /**
     * 项目名称
     */
    projectname?: any;
    /**
     * 项目标识
     */
    projectid?: any;
    /**
     * 建立时间
     */
    createdate?: any;
    /**
     * 更新人
     */
    updateman?: any;
}
