// import { CodeListService } from '../app/codelist-service';
import { EntityBaseService } from '../../service-base';
import { IContext } from 'ibz-core';
import { HttpResponse } from '../../utils/http-response';
import { IProject } from './interface/iproject';
import { Project } from './project';
import keys from './project-keys';
import { ProjectDTOHelp } from './project-dto-help';


/**
 * 项目服务对象基类
 *
 * @export
 * @class ProjectBaseService
 * @extends {EntityBaseService}
 */
export class ProjectBaseService extends EntityBaseService<IProject> {
    protected get keys(): string[] {
        return keys;
    }
    protected SYSTEMNAME = 'StudioPreview';
    protected APPNAME = 'TestMob';
    protected APPDENAME = 'Project';
    protected APPDENAMEPLURAL = 'Projects';
    protected dynaModelFilePath:string = 'PSSYSAPPS/TestMob/PSAPPDATAENTITIES/Project.json';
    protected APPDEKEY = 'projectid';
    protected APPDETEXT = 'projectname';
    protected quickSearchFields = ['projectname',];
    protected selectContextParam = {
    };

    constructor(opts?: any) {
        super(opts, 'Project');
    }

    newEntity(data: IProject): Project {
        return new Project(data);
    }

    /**
     * Create
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof ProjectService
     */
    async Create(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        _data = await this.beforeExecuteAction(_context,_data,'Create');
        if (!_data.srffrontuf || _data.srffrontuf != 1) {
            _data[this.APPDEKEY] = null;
        }
        if (_data.srffrontuf != null) {
            delete _data.srffrontuf;
        }
    _data = await ProjectDTOHelp.get(_context,_data);
        const res = await this.http.post(`/projects`, _data);
        res.data = await ProjectDTOHelp.set(_context,res.data);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Get
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof ProjectService
     */
    async Get(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.get(`/projects/${encodeURIComponent(_context.project)}`, _data);
        res.data = await ProjectDTOHelp.set(_context,res.data);
        res.data = await this.afterExecuteAction(_context,res?.data,'Get');
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * GetDraft
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof ProjectService
     */
    async GetDraft(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        _data[this.APPDENAME?.toLowerCase()] = undefined;
        _data[this.APPDEKEY] = undefined;
        const res = await this.http.get(`/projects/getdraft`, _data);
        res.data = await ProjectDTOHelp.set(_context,res.data);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Remove
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof ProjectService
     */
    async Remove(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.delete(`/projects/${encodeURIComponent(_context.project)}`);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Update
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof ProjectService
     */
    async Update(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        _data = await this.beforeExecuteAction(_context,_data,'Update');
    _data = await ProjectDTOHelp.get(_context,_data);
        const res = await this.http.put(`/projects/${encodeURIComponent(_context.project)}`, _data);
        res.data = await ProjectDTOHelp.set(_context,res.data);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * FetchDefault
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof ProjectService
     */
    async FetchDefault(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.post(`/projects/fetchdefault`, _data);
        res.data = await ProjectDTOHelp.ToDataObjArray(_context,res.data);
        res.data = await this.afterExecuteActionBatch(_context,res?.data,'FetchDefault');
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Select
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof ProjectService
     */
    async Select(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.get(`/projects/${encodeURIComponent(_context.project)}/select`);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
}
