import { IEntityLocalDataService, IContext, IParam } from 'ibz-core';
import { ICUSTOM } from './interface/icustom';
/**
 * CUSTOMDTOdto辅助类
 *
 * @export
 * @class CUSTOMDTOHelp
 */
export class CUSTOMDTOHelp {

    /**
     * 获取数据服务
     *
     * @param {IContext} context 应用上下文对象
     * @returns {*}
     * @memberof CUSTOMDTOHelp
     */
    public static async getService(context: IContext): Promise<IEntityLocalDataService<ICUSTOM>> {
        return await App.getEntityService().getService(context, 'custom') as IEntityLocalDataService<ICUSTOM>;
    }

    /**
     * DTO转化成数据对象
     *
     * @param {IContext} context 应用上下文对象
     * @param {IParam} source dto对象
     * @returns {*}
     * @memberof CUSTOMDTOHelp
     */
    public static async ToDataObj(context: IContext, source: IParam) {
        const _data: any = {};
        // 自定义实体标识
        _data.customid = source.customid;
        // 自定义实体名称
        _data.customname = source.customname;
        // 建立时间
        _data.createdate = source.createdate;
        // 建立人
        _data.createman = source.createman;
        // 时间选择
        _data.datepicker = source.datepicker;
        // 时间选择——天
        _data.datepicker_day = source.datepicker_day;
        // 时间选择_小时
        _data.datepicker_hour = source.datepicker_hour;
        // 时间选择_只有小时和分钟
        _data.datepicker_justhourminute = source.datepicker_justhourminute;
        // 时间选择_分钟
        _data.datepicker_minute = source.datepicker_minute;
        // 电子签名
        _data.electronicsignature = source.electronicsignature;
        // 属性1
        _data.field = source.field;
        // 是否逻辑
        _data.field2 = source.field2;
        // 输入框
        _data.inputbox = source.inputbox;
        // 数值框
        _data.inputnumberbox = source.inputnumberbox;
        // 密码框
        _data.password = source.password;
        // 选项框属性
        _data.radiolist = source.radiolist;
        // span标签
        _data.span = source.span;
        // 状态属性
        _data.statefield = source.statefield;
        // 多行文本框
        _data.textarea = source.textarea;
        // 类型
        _data.type = source.type;
        // 更新时间
        _data.updatedate = source.updatedate;
        // 更新人
        _data.updateman = source.updateman;
        return _data;
    }

    /**
     * 转化数组(dto转化成数据对象)
     *
     * @param {IContext} context 应用上下文对象
     * @param {any[]} data 数据对象
     * @returns {any[]}
     * @memberof CUSTOMDTOHelp
     */
    public static async ToDataObjArray(context: IContext, data: any[]) {
        const _data: any[] = [];
        if (data && Array.isArray(data) && data.length > 0) {
            for (let i = 0; i < data.length; i++) {
                const tempdata = await this.ToDataObj(context, data[i]);
                _data.push(tempdata);
            }
        }
        return _data;
    }

    /**
     * 数据对象转化成DTO
     *
     * @param {IContext} context 应用上下文对象
     * @param {*} source 数据对象
     * @returns {*}
     * @memberof CUSTOMDTOHelp
     */
    public static async ToDto(context: IContext, source: IParam) {
        const _data: any = {};
        // 自定义实体标识
        _data.customid = source.customid;
        // 自定义实体名称
        _data.customname = source.customname;
        // 建立时间
        _data.createdate = source.createdate;
        // 建立人
        _data.createman = source.createman;
        // 时间选择
        _data.datepicker = source.datepicker;
        // 时间选择——天
        _data.datepicker_day = source.datepicker_day;
        // 时间选择_小时
        _data.datepicker_hour = source.datepicker_hour;
        // 时间选择_只有小时和分钟
        _data.datepicker_justhourminute = source.datepicker_justhourminute;
        // 时间选择_分钟
        _data.datepicker_minute = source.datepicker_minute;
        // 电子签名
        _data.electronicsignature = source.electronicsignature;
        // 属性1
        _data.field = source.field;
        // 是否逻辑
        _data.field2 = source.field2;
        // 输入框
        _data.inputbox = source.inputbox;
        // 数值框
        _data.inputnumberbox = source.inputnumberbox;
        // 密码框
        _data.password = source.password;
        // 选项框属性
        _data.radiolist = source.radiolist;
        // span标签
        _data.span = source.span;
        // 状态属性
        _data.statefield = source.statefield;
        // 多行文本框
        _data.textarea = source.textarea;
        // 类型
        _data.type = source.type;
        // 更新时间
        _data.updatedate = source.updatedate;
        // 更新人
        _data.updateman = source.updateman;
        return _data;
    }

    /**
     * 转化数组(数据对象转化成dto)
     *
     * @param {IContext} context 应用上下文对象
     * @param {any[]} data 
     * @returns {any[]}
     * @memberof CUSTOMDTOHelp
     */
    public static async ToDtoArray(context: IContext, data: any[]) {
        const _data: any[] = [];
        if (data && Array.isArray(data) && data.length > 0) {
            for (let i = 0; i < data.length; i++) {
                const tempdata = await this.ToDto(context, data[i]);
                _data.push(tempdata);
            }
        }
        return _data;
    }

    /**
     * 处理响应dto对象
     *
     * @param {*} context 应用上下文对象
     * @param {*} data 响应dto对象
     * @returns {*}
     * @memberof CUSTOMDTOHelp
     */
    public static async set(context: IContext, data: any) {
        const _data: IParam = await this.ToDataObj(context, data);
        return _data;
    }

    /**
     * 处理请求数据对象
     *
     * @param {*} context 应用上下文对象
     * @param {*} data 数据对象
     * @returns {*}
     * @memberof CUSTOMDTOHelp
     */
    public static async get(context: IContext, data: any = {}) {
        return await this.ToDto(context, data);
    }

    /**
     * 获取缓存数据
     *
     * @param {*} context 应用上下文对象
     * @param {*} srfkey 数据主键
     * @returns {*}
     * @memberof CUSTOMDTOHelp
     */
    public static async getCacheData(context: IContext, srfkey: string) {
        const targetService: IEntityLocalDataService<ICUSTOM> = await this.getService(context);
        const result =  await targetService.getLocal(context, srfkey);
        if(result){
            return await this.ToDto(context,result);
        }
    }

    /**
     * 获取缓存数组
     *
     * @param {*} context 应用上下文对象
     * @returns {any[]}
     * @memberof CUSTOMDTOHelp
     */
    public static async getCacheDataArray(context: IContext) {
        const targetService: IEntityLocalDataService<ICUSTOM> = await this.getService(context);
        const result =  await targetService.getLocals(context);
        if(result && result.length >0){
            return await this.ToDtoArray(context,result);
        }else{
            return [];
        }
    }

    /**
     * 设置缓存数据
     *
     * @param {*} context 应用上下文对象
     * @param {any} data 数据
     * @returns {any[]}
     * @memberof CUSTOMDTOHelp
     */
    public static async setCacheData(context: IContext, data: any) {
        const targetService: IEntityLocalDataService<ICUSTOM> = await this.getService(context);
        const _data: any = await this.set(context, data);
        await targetService.addLocal(context, _data);
    }

    /**
     * 设置缓存数组
     *
     * @param {*} context 应用上下文对象
     * @param {any[]} data 数据
     * @returns {any[]}
     * @memberof CUSTOMDTOHelp
     */
    public static async setCacheDataArray(context: IContext, data: any[]) {
        if (data && data.length > 0) {
            const targetService: IEntityLocalDataService<ICUSTOM> = await this.getService(context);
            for (let i = 0; i < data.length; i++) {
                const _data = await this.set(context, data[i]);
                await targetService.addLocal(context, _data);
            }
        }
    }
}
