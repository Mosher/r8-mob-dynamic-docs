// import { CodeListService } from '../app/codelist-service';
import { EntityBaseService } from '../../service-base';
import { IContext } from 'ibz-core';
import { HttpResponse } from '../../utils/http-response';
import { ICUSTOM } from './interface/icustom';
import { CUSTOM } from './custom';
import keys from './custom-keys';
import { CUSTOMDTOHelp } from './customdto-help';


/**
 * 自定义实体服务对象基类
 *
 * @export
 * @class CUSTOMBaseService
 * @extends {EntityBaseService}
 */
export class CUSTOMBaseService extends EntityBaseService<ICUSTOM> {
    protected get keys(): string[] {
        return keys;
    }
    protected SYSTEMNAME = 'StudioPreview';
    protected APPNAME = 'TestMob';
    protected APPDENAME = 'CUSTOM';
    protected APPDENAMEPLURAL = 'CUSTOMs';
    protected dynaModelFilePath:string = 'PSSYSAPPS/TestMob/PSAPPDATAENTITIES/CUSTOM.json';
    protected APPDEKEY = 'customid';
    protected APPDETEXT = 'customname';
    protected quickSearchFields = ['customname',];
    protected selectContextParam = {
    };

    constructor(opts?: any) {
        super(opts, 'CUSTOM');
    }

    newEntity(data: ICUSTOM): CUSTOM {
        return new CUSTOM(data);
    }

    /**
     * Create
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof CUSTOMService
     */
    async Create(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        _data = await this.beforeExecuteAction(_context,_data,'Create');
        if (!_data.srffrontuf || _data.srffrontuf != 1) {
            _data[this.APPDEKEY] = null;
        }
        if (_data.srffrontuf != null) {
            delete _data.srffrontuf;
        }
    _data = await CUSTOMDTOHelp.get(_context,_data);
        const res = await this.http.post(`/customs`, _data);
        res.data = await CUSTOMDTOHelp.set(_context,res.data);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * DELogicTest
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof CUSTOMService
     */
    async DELogicTest(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
            _data =  await this.executeAppDELogic('BackendLogicTest',_context,_data);
            return new HttpResponse(_data, {
                ok: true,
                status: 200
            });
        }catch (error) {
            return new HttpResponse({message:error.message}, {
                ok: false,
                status: 500,
            });
        }
    }
    /**
     * ExecuteFinish
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof CUSTOMService
     */
    async ExecuteFinish(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
    _data = await CUSTOMDTOHelp.get(_context,_data);
        const res = await this.http.post(`/customs/${encodeURIComponent(_context.custom)}/executefinish`, _data);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * ExecuteNextStep
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof CUSTOMService
     */
    async ExecuteNextStep(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
    _data = await CUSTOMDTOHelp.get(_context,_data);
        const res = await this.http.post(`/customs/${encodeURIComponent(_context.custom)}/executenextstep`, _data);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * ExecutePrevStep
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof CUSTOMService
     */
    async ExecutePrevStep(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
    _data = await CUSTOMDTOHelp.get(_context,_data);
        const res = await this.http.post(`/customs/${encodeURIComponent(_context.custom)}/executeprevstep`, _data);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Get
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof CUSTOMService
     */
    async Get(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.get(`/customs/${encodeURIComponent(_context.custom)}`, _data);
        res.data = await CUSTOMDTOHelp.set(_context,res.data);
        res.data = await this.afterExecuteAction(_context,res?.data,'Get');
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * GetDraft
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof CUSTOMService
     */
    async GetDraft(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        _data[this.APPDENAME?.toLowerCase()] = undefined;
        _data[this.APPDEKEY] = undefined;
        const res = await this.http.get(`/customs/getdraft`, _data);
        res.data = await CUSTOMDTOHelp.set(_context,res.data);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * GetLogicCounter
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof CUSTOMService
     */
    async GetLogicCounter(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.get(`/customs/${encodeURIComponent(_context.custom)}/getlogiccounter`, _data);
        res.data = await CUSTOMDTOHelp.set(_context,res.data);
        res.data = await this.afterExecuteAction(_context,res?.data,'GetLogicCounter');
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * GetRandom
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof CUSTOMService
     */
    async GetRandom(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.get(`/customs/${encodeURIComponent(_context.custom)}/getrandom`, _data);
        res.data = await CUSTOMDTOHelp.set(_context,res.data);
        res.data = await this.afterExecuteAction(_context,res?.data,'GetRandom');
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * OpenMsgbox
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof CUSTOMService
     */
    async OpenMsgbox(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
    _data = await CUSTOMDTOHelp.get(_context,_data);
        const res = await this.http.post(`/customs/${encodeURIComponent(_context.custom)}/openmsgbox`, _data);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Remove
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof CUSTOMService
     */
    async Remove(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.delete(`/customs/${encodeURIComponent(_context.custom)}`);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * TestFrontLogic
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof CUSTOMService
     */
    async TestFrontLogic(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
    _data = await CUSTOMDTOHelp.get(_context,_data);
        const res = await this.http.post(`/customs/${encodeURIComponent(_context.custom)}/testfrontlogic`, _data);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Update
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof CUSTOMService
     */
    async Update(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        _data = await this.beforeExecuteAction(_context,_data,'Update');
    _data = await CUSTOMDTOHelp.get(_context,_data);
        const res = await this.http.put(`/customs/${encodeURIComponent(_context.custom)}`, _data);
        res.data = await CUSTOMDTOHelp.set(_context,res.data);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * FetchDataSet2
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof CUSTOMService
     */
    async FetchDataSet2(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.post(`/customs/fetchdataset2`, _data);
        res.data = await CUSTOMDTOHelp.ToDataObjArray(_context,res.data);
        res.data = await this.afterExecuteActionBatch(_context,res?.data,'FetchDataSet2');
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * FetchDefault
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof CUSTOMService
     */
    async FetchDefault(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.post(`/customs/fetchdefault`, _data);
        res.data = await CUSTOMDTOHelp.ToDataObjArray(_context,res.data);
        res.data = await this.afterExecuteActionBatch(_context,res?.data,'FetchDefault');
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }
    /**
     * Select
     *
     * @param {*} [_context={}]
     * @param {*} [_data = {}]
     * @returns {Promise<HttpResponse>}
     * @memberof CUSTOMService
     */
    async Select(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
        try {
        const res = await this.http.get(`/customs/${encodeURIComponent(_context.custom)}/select`);
        return res;
            } catch (error) {
                return this.handleResponseError(error);
            }
    }

    /**
     * ExecuteFinishBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CUSTOMServiceBase
     */
    public async ExecuteFinishBatch(_context: IContext = {},_data: any = {}): Promise<HttpResponse> {
        const res = await this.http.post(`/customs/executefinishbatch`,_data);
        return res;
    }

    /**
     * ExecuteNextStepBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CUSTOMServiceBase
     */
    public async ExecuteNextStepBatch(_context: IContext = {},_data: any = {}): Promise<HttpResponse> {
        const res = await this.http.post(`/customs/executenextstepbatch`,_data);
        return res;
    }

    /**
     * ExecutePrevStepBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CUSTOMServiceBase
     */
    public async ExecutePrevStepBatch(_context: IContext = {},_data: any = {}): Promise<HttpResponse> {
        const res = await this.http.post(`/customs/executeprevstepbatch`,_data);
        return res;
    }

    /**
     * GetLogicCounterBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CUSTOMServiceBase
     */
    public async GetLogicCounterBatch(_context: IContext = {},_data: any = {}): Promise<HttpResponse> {
        const res = await this.http.post(`/customs/getlogiccounterbatch`,_data);
        return res;
    }

    /**
     * GetRandomBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CUSTOMServiceBase
     */
    public async GetRandomBatch(_context: IContext = {},_data: any = {}): Promise<HttpResponse> {
        const res = await this.http.post(`/customs/getrandombatch`,_data);
        return res;
    }

    /**
     * OpenMsgboxBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CUSTOMServiceBase
     */
    public async OpenMsgboxBatch(_context: IContext = {},_data: any = {}): Promise<HttpResponse> {
        const res = await this.http.post(`/customs/openmsgboxbatch`,_data);
        return res;
    }

    /**
     * TestFrontLogicBatch接口方法
     *
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof CUSTOMServiceBase
     */
    public async TestFrontLogicBatch(_context: IContext = {},_data: any = {}): Promise<HttpResponse> {
        const res = await this.http.post(`/customs/testfrontlogicbatch`,_data);
        return res;
    }
}
