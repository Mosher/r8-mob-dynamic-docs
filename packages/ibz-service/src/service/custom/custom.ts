import { CUSTOMBase } from './custom-base';

/**
 * 自定义实体
 *
 * @export
 * @class CUSTOM
 * @extends {CUSTOMBase}
 * @implements {ICUSTOM}
 */
export class CUSTOM extends CUSTOMBase {

    /**
     * 克隆当前实体
     *
     * @return {*}
     * @memberof CUSTOM
     */
    clone(): CUSTOM {
        return new CUSTOM(this);
    }
}
export default CUSTOM;
