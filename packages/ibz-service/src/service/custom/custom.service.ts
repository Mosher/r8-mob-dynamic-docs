import { IContext } from 'ibz-core';
import { HttpResponse } from '../../utils/http-response';
import { CUSTOMBaseService } from './custom-base.service';

/**
 * 自定义实体服务
 *
 * @export
 * @class CUSTOMService
 * @extends {CUSTOMBaseService}
 */
export class CUSTOMService extends CUSTOMBaseService {
  /**
   * Creates an instance of CUSTOMService.
   * @memberof CUSTOMService
   */
  constructor(opts?: any) {
    const { context: context, tag: cacheKey } = opts;
    super(context);
    ServiceCache.set(cacheKey, this);
  }

  /**
   * 获取实例
   *
   * @static
   * @param 应用上下文
   * @return {*}  {CUSTOMService}
   * @memberof CUSTOMService
   */
  static getInstance(context?: any): CUSTOMService {
    const cacheKey: string = context?.srfdynainstid ? `${context.srfdynainstid}CUSTOMService` : `CUSTOMService`;
    if (!ServiceCache.has(cacheKey)) {
      return new CUSTOMService({ context: context, tag: cacheKey });
    }
    return ServiceCache.get(cacheKey);
  }

  /**
   * GetRandom
   *
   * @param {*} [_context={}]
   * @param {*} [_data = {}]
   * @returns {Promise<HttpResponse>}
   * @memberof CUSTOMService
   */
  async GetRandom(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
    eval(`_data.countertag=Math.floor(Math.random()*100+1);
      _data.countertag2=Math.floor(Math.random()*100+1);
      _data.countertag3=Math.floor(Math.random()*100+1);
      console.log(_data);`);
    return new HttpResponse(_data, {
      ok: true,
      status: 200,
    });
  }

  async GetDraft(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
    const res: any = {
      ok: true,
      status: 200,
      data: {
        datepicker_justhourminute: null,
        statefield: null,
        type: null,
        inputbox: null,
        customname: null,
        span: null,
        customid: null,
        createman: null,
        createdate: null,
        password: null,
        datepicker: null,
        field2: null,
        updatedate: null,
        datepicker_minute: null,
        field: null,
        datepicker_hour: null,
        updateman: null,
        radiolist: null,
        datepicker_day: null,
        textarea: null,
        inputnumberbox: null,
      },
    };
    return res;
  }

  async Get(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
    const name: string = _context.formName;
    const res: any = {
      ok: true,
      status: 200,
      data: {
        datepicker_justhourminute: null,
        statefield:
          name == 'wizardpanel_form_step1wizardform'
            ? 'Step2'
            : name == 'wizardpanel_form_step2wizardform'
            ? 'Step3'
            : 'Step1',
        type: null,
        inputbox: null,
        customname: '我是啦啦啦',
        span: null,
        customid: '1',
        createman: null,
        createdate: null,
        password: '123456',
        datepicker: null,
        field2: null,
        updatedate: null,
        datepicker_minute: null,
        field: null,
        datepicker_hour: null,
        updateman: null,
        radiolist: null,
        datepicker_day: null,
        textarea: null,
        inputnumberbox: null,
      },
    };
    console.log('获取数据', name, res.data);
    return res;
  }

  async Update(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
    console.log(_data, _context);
    const res: any = { ok: true, status: 200, data: _data };
    return res;
  }

  /**
   * ExecuteFinishBatch接口方法
   *
   * @param {*} [context={}]
   * @param {*} [data={}]
   * @param {boolean} [isloading]
   * @returns {Promise<any>}
   * @memberof CUSTOMServiceBase
   */
  public async ExecuteFinish(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
    console.log('执行完成步骤');
    const res: any = { ok: true, status: 200, data: _data };
    return res;
  }

  /**
   * ExecuteNextStepBatch接口方法
   *
   * @param {*} [context={}]
   * @param {*} [data={}]
   * @param {boolean} [isloading]
   * @returns {Promise<any>}
   * @memberof CUSTOMServiceBase
   */
  public async ExecuteNextStep(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
    console.log('执行下一步', _data);
    _data.statefield = 'Step3';

    const res: any = { ok: true, status: 200, data: _data };
    return res;
  }

  /**
   * ExecutePrevStepBatch接口方法
   *
   * @param {*} [context={}]
   * @param {*} [data={}]
   * @param {boolean} [isloading]
   * @returns {Promise<any>}
   * @memberof CUSTOMServiceBase
   */
  public async ExecutePrevStep(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
    console.log('执行上一步');
    const res: any = { ok: true, status: 200, data: _data };
    return res;
  }

  async FetchDefault(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
    console.log(111, _context, _data);
    const res: any = {
      ok: true,
      status: 200,
      data: [
        {
          datepicker_justhourminute: null,
          statefield: 'Step2',
          type: null,
          inputbox: null,
          customname: '我是啦啦啦',
          span: null,
          customid: '1',
          createman: null,
          createdate: null,
          password: '123456',
          datepicker: null,
          field2: null,
          updatedate: null,
          datepicker_minute: null,
          field: null,
          datepicker_hour: null,
          updateman: null,
          radiolist: null,
          datepicker_day: null,
          textarea: null,
          inputnumberbox: null,
        },
        {
          datepicker_justhourminute: null,
          statefield: 'Step2',
          type: null,
          inputbox: null,
          customname: '塔塔开',
          span: null,
          customid: '2',
          createman: null,
          createdate: null,
          password: '123456',
          datepicker: null,
          field2: null,
          updatedate: null,
          datepicker_minute: null,
          field: null,
          datepicker_hour: null,
          updateman: null,
          radiolist: null,
          datepicker_day: null,
          textarea: null,
          inputnumberbox: null,
        },
      ],
    };
    return res;
  }

  async FetchDataSet2(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
    const data =
      _data.srfparentkey == '1'
        ? [
            {
              datepicker_justhourminute: null,
              statefield: 'Step2',
              type: null,
              inputbox: null,
              customname: 'DataSet2-啦啦啦-啦啦啦',
              span: null,
              customid: '3',
              createman: null,
              createdate: null,
              password: '123456',
              datepicker: null,
              field2: null,
              updatedate: null,
              datepicker_minute: null,
              field: null,
              datepicker_hour: null,
              updateman: null,
              radiolist: null,
              datepicker_day: null,
              textarea: null,
              inputnumberbox: null,
            },
            {
              datepicker_justhourminute: null,
              statefield: 'Step2',
              type: null,
              inputbox: null,
              customname: 'DataSet2-啦啦啦-达咩',
              span: null,
              customid: '4',
              createman: null,
              createdate: null,
              password: '123456',
              datepicker: null,
              field2: null,
              updatedate: null,
              datepicker_minute: null,
              field: null,
              datepicker_hour: null,
              updateman: null,
              radiolist: null,
              datepicker_day: null,
              textarea: null,
              inputnumberbox: null,
            },
          ]
        : [
            {
              datepicker_justhourminute: null,
              statefield: 'Step2',
              type: null,
              inputbox: null,
              customname: 'DataSet2-塔塔开-啦啦啦',
              span: null,
              customid: '5',
              createman: null,
              createdate: null,
              password: '123456',
              datepicker: null,
              field2: null,
              updatedate: null,
              datepicker_minute: null,
              field: null,
              datepicker_hour: null,
              updateman: null,
              radiolist: null,
              datepicker_day: null,
              textarea: null,
              inputnumberbox: null,
            },
            {
              datepicker_justhourminute: null,
              statefield: 'Step2',
              type: null,
              inputbox: null,
              customname: 'DataSet2-塔塔开-达咩',
              span: null,
              customid: '6',
              createman: null,
              createdate: null,
              password: '123456',
              datepicker: null,
              field2: null,
              updatedate: null,
              datepicker_minute: null,
              field: null,
              datepicker_hour: null,
              updateman: null,
              radiolist: null,
              datepicker_day: null,
              textarea: null,
              inputnumberbox: null,
            },
          ];
    const res: any = {
      ok: true,
      status: 200,
      data: data,
    };
    return res;
  }

  async TestFrontLogic(_context: IContext = {}, _data: any = {}): Promise<HttpResponse> {
    const res: any = {
      ok: true,
      status: 200,
      data: {
        datepicker_justhourminute: null,
        statefield: 'Step2',
        type: null,
        inputbox: null,
        customname: '我是啦啦啦',
        span: null,
        customid: '1',
        createman: null,
        createdate: null,
        password: '123456',
        datepicker: null,
        field2: null,
        updatedate: null,
        datepicker_minute: null,
        field: null,
        datepicker_hour: null,
        updateman: null,
        radiolist: null,
        datepicker_day: null,
        textarea: null,
        inputnumberbox: null,
      },
    };
    console.log('执行实体处理逻辑成功 --- TestFrontLogic');
    return res;
  }
}
export default CUSTOMService;
