import { EntityBase } from '../../entities';
import { ICUSTOM } from './interface/icustom';

/**
 * 自定义实体基类
 *
 * @export
 * @abstract
 * @class CUSTOMBase
 * @extends {EntityBase}
 * @implements {ICUSTOM}
 */
export abstract class CUSTOMBase extends EntityBase implements ICUSTOM {
    /**
     * 实体名称
     *
     * @readonly
     * @type {string}
     * @memberof CUSTOMBase
     */
    get srfdename(): string {
        return 'CUSTOM';
    }
    get srfkey() {
        return this.customid;
    }
    set srfkey(val: any) {
        this.customid = val;
    }
    get srfmajortext() {
        return this.customname;
    }
    set srfmajortext(val: any) {
        this.customname = val;
    }
    /**
     * 自定义实体名称
     */
    customname?: any;
    /**
     * 自定义实体标识
     */
    customid?: any;
    /**
     * 建立人
     */
    createman?: any;
    /**
     * 建立时间
     */
    createdate?: any;
    /**
     * 更新时间
     */
    updatedate?: any;
    /**
     * 更新人
     */
    updateman?: any;
    /**
     * 属性1
     */
    field?: any;
    /**
     * 状态属性
     */
    statefield?: any;
    /**
     * 类型
     */
    type?: any;
    /**
     * 是否逻辑
     *
     * @type {('1' | '0')} 1: 是, 0: 否
     */
    field2?: '1' | '0';
    /**
     * 输入框
     */
    inputbox?: any;
    /**
     * 数值框
     */
    inputnumberbox?: any;
    /**
     * 多行文本框
     */
    textarea?: any;
    /**
     * 密码框
     */
    password?: any;
    /**
     * 时间选择
     */
    datepicker?: any;
    /**
     * 时间选择——天
     */
    datepicker_day?: any;
    /**
     * 时间选择_小时
     */
    datepicker_hour?: any;
    /**
     * 时间选择_分钟
     */
    datepicker_minute?: any;
    /**
     * 时间选择_只有小时和分钟
     */
    datepicker_justhourminute?: any;
    /**
     * 选项框属性
     *
     * @type {('toys' | 'food')} toys: 玩具, food: 食品
     */
    radiolist?: 'toys' | 'food';
    /**
     * span标签
     */
    span?: any;
    /**
     * 电子签名
     */
    electronicsignature?: any;

    /**
     * 重置实体数据
     *
     * @private
     * @param {*} [data={}]
     * @memberof CUSTOMBase
     */
    reset(data: any = {}): void {
        super.reset(data);
        this.customid = data.customid || data.srfkey;
        this.customname = data.customname || data.srfmajortext;
    }
}
