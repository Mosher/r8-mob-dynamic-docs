import { IApp, IActionService, IAppNoticeService, IAuthServiceRegister, ICodeListService, IComponentService, ICounterService, IEntityServiceRegister, ILoadingService, IMsgboxService, IPluginService, IUIServiceRegister, IUtilServiceRegister, IViewMessageService, IViewOpenService, IAppCenterService, } from "ibz-core";

export interface IServiceApp extends IApp {

    /**
     * @description 获取UI服务
     * @return {*}  {IUIServiceRegister}
     * @memberof IServiceApp
     */
    getUIService(): IUIServiceRegister

    /**
     * @description 获取权限服务
     * @return {*}  {IAuthServiceRegister}
     * @memberof IServiceApp
     */
    getAuthService(): IAuthServiceRegister;

    /**
     * @description 获取Util服务
     * @return {*}  {IUtilServiceRegister}
     * @memberof IServiceApp
     */
    getUtilService(): IUtilServiceRegister;

    /**
     * @description 获取数据服务
     * @return {*}  {IEntityServiceRegister}
     * @memberof IServiceApp
     */
    getEntityService(): IEntityServiceRegister;

    /**
     * @description 获取代码表服务
     * @return {*}  {ICodeListService}
     * @memberof IServiceApp
     */
    getCodeListService(): ICodeListService;

    /**
     * @description 获取计数器服务
     * @return {*}  {ICounterService}
     * @memberof IServiceApp
     */
    getCounterService(): ICounterService;

    /**
     * @description 获取视图消息服务
     * @return {*}  {IViewMessageService}
     * @memberof IServiceApp
     */
    getViewMSGService(): IViewMessageService;

    /**
     * @description 获取加载服务
     * @return {*}  {ILoadingService}
     * @memberof IServiceApp
     */
    getLoadingService(): ILoadingService;

    /**
     * @description 获取消息弹框服务
     * @return {*}  {IMsgboxService}
     * @memberof IServiceApp
     */
    getMsgboxService(): IMsgboxService;

    /**
     * @description 获取实例化界面行为服务
     * @return {*}  {IActionService}
     * @memberof IServiceApp
     */
    getActionService(): IActionService;

    /**
     * @description 获取视图打开服务
     * @return {*}  {IViewOpenService}
     * @memberof IServiceApp
     */
    getOpenViewService(): IViewOpenService;

    /**
     * @description 获取消息提示服务
     * @return {*}  {IAppNoticeService}
     * @memberof IServiceApp
     */
    getNoticeService(): IAppNoticeService;

    /**
     * @description 获取组件服务
     * @return {*}  {IComponentService}
     * @memberof IServiceApp
     */
    getComponentService(): IComponentService;

    /**
     * @description 获取插件服务
     * @return {*}  {IPluginService}
     * @memberof IServiceApp
     */
    getPluginService(): IPluginService;

    /**
     * @description 获取应用中心服务
     * @return {*}  {IAppCenterService}
     * @memberof IServiceApp
     */
    getCenterService(): IAppCenterService;
}