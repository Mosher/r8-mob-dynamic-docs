{
  "name": "ibz-core",
  "version": "4.0.1",
  "description": "ibz 核心包",
  "author": "ibz",
  "license": "MIT",
  "main": "lib/ibz-core.cjs.js",
  "module": "lib/ibz-core.es.js",
  "types": "types/index.d.ts",
  "homepage": "https://www.ibizlab.cn/#/common_index/partner_curdevcenterdashboardview",
  "repository": {
    "type": "git",
    "url": "http://172.16.180.229/studio-ftl/r8_ionic_mob_ftl_res"
  },
  "files": [
    "lib",
    "types"
  ],
  "scripts": {
    "build": "rollup -c",
    "gen-view": "node build/gen-code/gen-view",
    "gen-control": "node build/gen-code/gen-control",
    "gen-editor": "node build/gen-code/gen-editor",
    "commit": "git add . && git-cz",
    "lint-code": "eslint src",
    "release": "node build/release"
  },
  "dependencies": {
    "@ibiz/dynamic-model-api": "^0.0.35",
    "async-validator": "^3.5.1",
    "qs": "^6.10.1",
    "axios": "^0.21.1",
    "ramda": "^0.27.1",
    "qx-util": "^0.0.7"
  },
  "devDependencies": {
    "@commitlint/cli": "^13.2.0",
    "@commitlint/config-conventional": "^13.2.0",
    "@types/ramda": "^0.27.40",
    "commitizen": "^4.0.3",
    "eslint": "^7.32.0",
    "eslint-config-prettier": "^8.3.0",
    "eslint-config-typescript": "^3.0.0",
    "eslint-plugin-prettier": "^4.0.0",
    "husky": "^3.0.9",
    "inquirer": "6.5.2",
    "prettier": "^2.4.1",
    "nunjucks": "^3.2.3",
    "rollup": "^2.35.1",
    "rollup-plugin-terser": "^7.0.2",
    "package-tls": "^1.2.2",
    "@rollup/plugin-commonjs": "^17.0.0",
    "@rollup/plugin-json": "^4.1.0",
    "@rollup/plugin-node-resolve": "^11.0.1",
    "@rollup/plugin-typescript": "^8.0.0",
    "@typescript-eslint/eslint-plugin": "^4.10.0",
    "@typescript-eslint/parser": "^4.10.0",
    "standard-version": "^9.3.1",
    "path": "^0.12.7",
    "ts-loader": "^8.0.11",
    "typescript": "~4.3.5"
  },
  "config": {
    "commitizen": {
      "path": "cz-conventional-changelog"
    }
  },
  "husky": {
    "hooks": {
      "commit-msg": "commitlint -e $GIT_PARAMS"
    }
  }
}
