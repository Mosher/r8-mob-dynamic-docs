import { ViewEngine } from './view-engine';

/**
 * 日历导航视图界面引擎
 *
 * @export
 * @class MobCalendarExpViewEngine
 * @extends {ViewEngine}
 */
export class MobCalendarExpViewEngine extends ViewEngine {

    /**
     * 日历导航栏部件
     *
     * @type {*}
     * @memberof MobCalendarExpViewEngine
     */
    public calendarExpBar: any = null;

    /**
     * Creates an instance of MobCalendarExpViewEngine.
     * 
     * @memberof MobCalendarExpViewEngine
     */
    constructor() {
        super();
    }

    /**
     * 初始化引擎
     *
     * @param {*} options
     * @memberof MobCalendarExpViewEngine
     */
    public init(options: any): void {
        this.calendarExpBar = options.calendarexpbar;
        super.init(options);
    }


    /**
     * 引擎加载
     *
     * @memberof MobCalendarExpViewEngine
     */
    public load(opts: any): void {
        super.load(opts);
        if (this.getCalendarExpBar() && this.isLoadDefault) {
            const tag = this.getCalendarExpBar().name;
            Object.assign(this.view.viewParam, opts);            
            this.setViewState2({ tag: tag, action: 'load', viewdata: { ...this.view.viewparams } });
        } else {
            this.isLoadDefault = true;
        }
    }

    /**
     * 部件事件机制
     *
     * @param {string} ctrlName
     * @param {string} eventName
     * @param {*} args
     * @memberof MobCalendarExpViewEngine
     */
    public onCtrlEvent(ctrlName: string, eventName: string, args: any): void {
        super.onCtrlEvent(ctrlName, eventName, args);
        if (Object.is(ctrlName, 'calendarexpbar')) {
            this.calendarExpBarEvent(eventName, args);

        }
    }

    /**
     * 日历导航事件
     *
     * @param {string} eventName
     * @param {*} args
     * @memberof MobCalendarExpViewEngine
     */
    public calendarExpBarEvent(eventName: string, args: any): void {
        if (Object.is(eventName, 'load')) {
            this.emitViewEvent('viewload', args);
        }
        if (Object.is(eventName, 'selectionchange')) {
            this.emitViewEvent('viewdataschange', args);
        }
        if (Object.is(eventName, 'activated')) {
            this.emitViewEvent('viewdatasactivated', args);
        }
    }

    /**
     * 获取部件对象
     *
     * @returns {*}
     * @memberof MobCalendarExpViewEngine
     */
    public getCalendarExpBar(): any {
        return this.calendarExpBar;
    }


}