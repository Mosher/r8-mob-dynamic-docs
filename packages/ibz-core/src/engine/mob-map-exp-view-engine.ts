import { ViewEngine } from './view-engine';
/**
 * 实体移动端地图导航界面引擎
 *
 * @export
 * @class MDViewEngine
 * @extends {ViewEngine}
 */
export class MobMapExpViewEngine extends ViewEngine {
  /**
   * @description 地图导航栏部件
   * @type {*}
   * @memberof GridViewEngine
   */
  protected mapexpbar: any;

  /**
   * @description 引擎初始化
   * @param {*} [options={}]
   * @memberof GridViewEngine
   */
  public init(options: any = {}): void {
    this.mapexpbar = options.mapexpbar;
    super.init(options);
  }

  /**
   * @description 多数据部件
   * @param {string} ctrlName
   * @param {string} eventName
   * @param {*} args
   * @memberof MobMDViewEngine
   */
  public onCtrlEvent(ctrlName: string, eventName: string, args: any): void {
    super.onCtrlEvent(ctrlName, eventName, args);
  }

  /**
   * @description 获取多数据部件
   * @returns {*}
   * @memberof MDViewEngine
   */
  public getMapExpBar(): any {
    return this.mapexpbar;
  }

  /**
   * @description 引擎加载
   * @param {*} opts
   * @return {*}  {*}
   * @memberof MobMapExpViewEngine
   */
  public load(opts: any): any {
    super.load(opts);
    if (this.getMapExpBar()) {
      const tag = this.getMapExpBar().name;
      Object.assign(this.view.viewParam, opts);
      this.setViewState2({ tag: tag, action: 'load', viewdata: { ...this.view.viewparams } });
    } else {
      this.isLoadDefault = true;
    }
  }
}
