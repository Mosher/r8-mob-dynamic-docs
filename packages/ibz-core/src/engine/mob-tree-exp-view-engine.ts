import { ViewEngine } from './view-engine';
/**
 * 实体移动端树导航界面引擎
 *
 * @export
 * @class MobTreeExpViewEngine
 * @extends {ViewEngine}
 */
export class MobTreeExpViewEngine extends ViewEngine {
  /**
   * @description 树导航部件
   * @type {*}
   * @memberof GridViewEngine
   */
  protected treeexpbar: any;

  /**
   * @description 引擎初始化
   * @param {*} [options={}]
   * @memberof GridViewEngine
   */
  public init(options: any = {}): void {
    this.treeexpbar = options.treeexpbar;
    super.init(options);
  }

  /**
   * @description 多数据部件事件
   * @param {string} ctrlName
   * @param {string} eventName
   * @param {*} args
   * @memberof MobMDViewEngine
   */
  public onCtrlEvent(ctrlName: string, eventName: string, args: any): void {
    super.onCtrlEvent(ctrlName, eventName, args);
  }

  /**
   * @description 获取多数据部件
   * @returns {*}
   * @memberof MDViewEngine
   */
  public getTreeExpBar(): any {
    return this.treeexpbar;
  }

  /**
   * @description 引擎加载
   * @param {*} opts
   * @return {*}  {*}
   * @memberof MobChartExpViewEngine
   */
  public load(opts?: any): any {
    super.load(opts);
    if (this.getTreeExpBar()) {
      const tag = this.getTreeExpBar().name;
      Object.assign(this.view.viewParam, opts);
      this.setViewState2({ tag: tag, action: 'load', viewdata: { ...this.view.viewparams } });
    } else {
      this.isLoadDefault = true;
    }
  }
}
