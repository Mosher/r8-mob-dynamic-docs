/**
 * 视图引擎
 *
 * @export
 * @class ViewEngine
 */
export class ViewEngine {
  /**
   * 视图部件对象
   *
   * @protected
   * @type {*}
   * @memberof ViewEngine
   */
  protected view: any = null;
  /**
   * 引擎参数
   *
   * @type {*}
   * @memberof ViewEngine
   */
  protected opt: any = {};
  /**
   *
   *
   * @type {*}
   * @memberof ViewEngine
   */
  protected methods: any = {};

  /**
   * 是否默认记载
   *
   * @type {boolean}
   * @memberof ViewEngine
   */
  public isLoadDefault: boolean = true;

  /**
   * 实体主键属性
   *
   * @type {(string | undefined)}
   * @memberof ViewEngine
   */
  public keyPSDEField: string | undefined;

  /**
   * 实体主信息属性
   *
   * @type {(string | undefined)}
   * @memberof ViewEngine
   */
  public majorPSDEField: string | undefined;

  /**
   * Creates an instance of ViewEngine.
   * @memberof ViewEngine
   */
  constructor() {}

  /**
   * 引擎初始化
   *
   * @param {*} [options={}]
   * @memberof ViewEngine
   */
  public init(options: any = {}): void {
    this.opt = options;
    this.methods = options.methods;
    this.view = options.view;
    this.isLoadDefault = options.isLoadDefault;
    this.keyPSDEField = options.keyPSDEField;
    this.majorPSDEField = options.majorPSDEField;
    this.load();
  }

  /**
   * 引擎加载
   *
   * @param {*} [opts={}]
   * @memberof ViewEngine
   */
  public load(opts: any = {}): void {}

  /**
   * 部件事件机制
   *
   * @param {string} ctrlName
   * @param {string} eventName
   * @param {*} args
   * @memberof ViewEngine
   */
  public onCtrlEvent(ctrlName: string, eventName: string, args: any): void {}

  /**
   * 视图事件处理
   *
   * @param {string} eventName 事件tag
   * @param {*} args 事件参数
   * @memberof ViewEngine
   */
  public emitViewEvent(eventName: string, args: any): void {
    if (this.view) {
      this.view.hooks.event.callSync({
        viewName: this.view.viewInstance?.codeName,
        action: eventName,
        data: args,
      });
    }
  }

  /**
   * 是否为方法
   *
   * @protected
   * @param {*} func
   * @returns {boolean}
   * @memberof ViewEngine
   */
  protected isFunc(func: any): boolean {
    return func instanceof Function;
  }

  /**
   * 父数据参数模式
   *
   * @param {{ tag: string, action: string, viewdata: any }} { tag, action, viewdata }
   * @memberof ViewEngine
   */
  public setViewState2({ tag, action, viewdata }: { tag: string; action: string; viewdata: any }): void {
    this.view.viewState.next({ tag: tag, action: action, data: viewdata });
  }

  /**
   * 计算工具栏状态
   *
   * @param {boolean} state
   * @param {*} [dataaccaction]
   * @memberof ViewEngine
   */
  public calcToolbarItemState(state: boolean, dataaccaction?: any) {
    this.view.toolbarModelList?.forEach((tool: any) => {
      if (!this.view[tool] || Object.keys(this.view[tool]).length === 0) {
        return;
      }

      for (const key in this.view[tool]) {
        if (!this.view[tool][key]) {
          return;
        }
        const _item = this.view[tool][key];
        if (
          _item.uiaction &&
          (Object.is(_item.uiaction.target, 'SINGLEKEY') || Object.is(_item.uiaction.target, 'MULTIKEY'))
        ) {
          _item.disabled = state;
        }
        _item.visabled = true;
        if (_item.noprivdisplaymode && _item.noprivdisplaymode === 6) {
          _item.visabled = false;
        }
      }
    });
  }

  /**
   * 计算工具栏权限状态
   *
   * @param {boolean} state
   * @param {*} [dataaccaction]
   * @memberof ViewEngine
   */
  public calcToolbarItemAuthState(data: any) {
    this.view.viewState.next({ tag: 'toolbar', action: 'permissionValid', data: data });
  }
}
