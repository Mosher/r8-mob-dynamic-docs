import { ViewEngine } from './view-engine';
/**
 * 实体移动端多数据界面引擎
 *
 * @export
 * @class MDViewEngine
 * @extends {ViewEngine}
 */
export class MobChartExpViewEngine extends ViewEngine {
  /**
   * @description 图表部件
   * @type {*}
   * @memberof GridViewEngine
   */
  protected chartexpbar: any;

  /**
   * @description 引擎初始化
   * @param {*} [options={}]
   * @memberof GridViewEngine
   */
  public init(options: any = {}): void {
    this.chartexpbar = options.chartexpbar;
    super.init(options);
  }

  /**
   * @description 多数据部件
   * @param {string} ctrlName
   * @param {string} eventName
   * @param {*} args
   * @memberof MobMDViewEngine
   */
  public onCtrlEvent(ctrlName: string, eventName: string, args: any): void {
    super.onCtrlEvent(ctrlName, eventName, args);
  }

  /**
   * @description 获取多数据部件
   * @returns {*}
   * @memberof MDViewEngine
   */
  public getChartExpBar(): any {
    return this.chartexpbar;
  }

  /**
   * @description 引擎加载
   * @param {*} opts
   * @return {*}  {*}
   * @memberof MobChartExpViewEngine
   */
  public load(opts: any): any {
    super.load(opts);
    if (this.getChartExpBar()) {
      const tag = this.getChartExpBar().name;
      Object.assign(this.view.viewParam, opts);
      this.setViewState2({ tag: tag, action: 'load', viewdata: { ...this.view.viewparams } });
    } else {
      this.isLoadDefault = true;
    }
  }
}
