import { DEMultiDataViewEngine } from './de-multi-data-view-engine';
import { MobChartEvents } from '../interface/event';

/**
 * 实体移动端图表视图界面引擎
 *
 * @export
 * @class MobChartViewEngine
 * @extends {ViewEngine}
 */
export class MobChartViewEngine extends DEMultiDataViewEngine {
  /**
   * 图表对象
   *
   * @type {*}
   * @memberof MobChartViewEngine
   */
  public chart: any;

  /**
   * 图表初始化
   *
   * @param {*} options
   * @memberof MobChartViewEngine
   */
  public init(options: any): void {
    this.chart = options.chart;
    super.init(options);
  }

  /**
   * 部件事件
   *
   * @param {string} ctrlName
   * @param {string} eventName
   * @param {*} args
   * @memberof MobChartViewEngine
   */
  public onCtrlEvent(ctrlName: string, eventName: string, args: any): void {
    super.onCtrlEvent(ctrlName, eventName, args);
    if (Object.is(ctrlName, 'chart')) {
      this.chartEvent(eventName, args);
    }
  }

  /**
   * 图表部件事件处理
   *
   * @param {string} eventName
   * @param {any[]} args
   * @memberof MobMDViewEngine
   */
  public chartEvent(eventName: string, args: any): void {
    super.MDCtrlEvent(eventName, args);
    if (Object.is(eventName, MobChartEvents.SELECT_CHANGE)) {
      // TODO
    }
  }

  /**
   * 获取图表
   *
   * @returns {*}
   * @memberof MobChartViewEngine
   */
  public getChart(): any {
    return this.chart;
  }

  /**
 * 获取多数据部件
 *
 * @returns {*}
 * @memberof MobChartViewEngine
 */
  public getMDCtrl(): any {
    return this.chart;
  }
}
