import { AppCtrlEvents } from '../interface';
import { LogUtil, Util } from '../util';
import { ViewEngine } from './view-engine';

/**
 * @description 实体移动端自定义视图界面引擎
 * @export
 * @class MobCustomViewEngine
 * @extends {ViewEngine}
 */
export class MobCustomViewEngine extends ViewEngine {
  /**
   * @description 部件引擎集合
   * @type {Array<any>}
   * @memberof MobCustomViewEngine
   */
  public ctrlEngineArray: Array<any> = [];

  /**
   * @description 视图部件Map
   * @type {Map<string, any>}
   * @memberof MobCustomViewEngine
   */
  public viewCtrlMap: Map<string, any> = new Map();

  /**
   * 引擎初始化
   *
   * @param {*} [options={}]
   * @memberof MobCustomViewEngine
   */
  public init(options: any = {}): void {
    this.initViewControlMap(options.ctrl);
    this.initCtrlEngineArray(options.engine);
    super.init(options);
  }

  /**
   * @description 初始化引擎Map
   * @param {*} options
   * @memberof MobCustomViewEngine
   */
  public initCtrlEngineArray(options: any) {
    if (options && options.length > 0) {
      this.ctrlEngineArray = [];
      options.forEach((element: any) => {
        const result = this.handleViewEngineParams(element);
        this.ctrlEngineArray.push(result);
      });
    }
  }

  /**
   * @description 初始化视图部件Map
   * @param {*} options
   * @memberof MobCustomViewEngine
   */
  public initViewControlMap(options: any) {
    if (options && options.length > 0) {
      options.forEach((element: any) => {
        this.viewCtrlMap.set(element.name, element.ctrl);
      });
    }
  }

  /**
   * @description 引擎加载
   * @param {*} [opts={}]
   * @memberof MobCustomViewEngine
   */
  public load(opts: any = {}): void {
    // 处理搜索部件加载并搜索（参数可指定触发部件）
    if (this.ctrlEngineArray.length > 0) {
      for (const element of this.ctrlEngineArray) {
        if (element.triggerCtrlName && Object.is(element.triggerCtrlName, 'VIEW')) {
          if (element.triggerType && Object.is(element.triggerType, 'CtrlLoadAndSearch')) {
            this.setViewState2({ tag: element.targetCtrlName, action: 'loadDraft', viewdata: Util.deepCopy(opts) });
          }
        }
      }
    }
    // 处理部件加载（参数可指定触发部件）无指定触发部件时由容器触发
    if (this.ctrlEngineArray.length > 0) {
      for (const element of this.ctrlEngineArray) {
        if (element.triggerType && Object.is(element.triggerType, 'CtrlLoad') && !element.triggerCtrlName) {
          this.setViewState2({ tag: element.targetCtrlName, action: 'load', viewdata: Util.deepCopy(opts) });
        }
      }
    }
  }

  /**
   * @description 部件事件机制
   * @param {string} ctrlName 部件名称
   * @param {string} eventName 事件名称
   * @param {*} args 事件参数
   * @memberof MobCustomViewEngine
   */
  public onCtrlEvent(ctrlName: string, eventName: string, args: any): void {
    super.onCtrlEvent(ctrlName, eventName, args);
    // 处理部件加载（参数可指定触发部件）
    if (
      Object.is(eventName, AppCtrlEvents.SEARCH) ||
      Object.is(eventName, AppCtrlEvents.BEFORE_LOAD_DRAFT) ||
      Object.is(eventName, AppCtrlEvents.SELECT_CHANGE)
    ) {
      if (this.ctrlEngineArray.length > 0) {
        for (const element of this.ctrlEngineArray) {
          if (element.triggerCtrlName && Object.is(element.triggerCtrlName, ctrlName)) {
            if (element.triggerType && Object.is(element.triggerType, 'CtrlLoad')) {
              if (this.view) {
                const ctrl = this.viewCtrlMap.get(element.targetCtrlName);
                if (ctrl && ctrl.setNavdatas && ctrl.setNavdatas instanceof Function) {
                  ctrl.setNavdatas(Util.deepCopy(args));
                }
                if (Util.isExistData(args)) {
                  this.setViewState2({ tag: element.targetCtrlName, action: 'load', viewdata: Util.deepCopy(args) });
                } else {
                  this.setViewState2({ tag: element.targetCtrlName, action: 'reset', viewdata: Util.deepCopy(args) });
                }
              }
            }
          }
        }
      }
    }
  }

  /**
   * @description 处理视图引擎参数
   * @param {*} args 数据
   * @return {*}
   * @memberof MobCustomViewEngine
   */
  public handleViewEngineParams(args: any) {
    switch (args.engineType) {
      case 'CtrlLoadTrigger':
        return this.handleCtrlLoad(args.getPSUIEngineParams);
      case 'CtrlLoad':
        return this.handleCtrlLoad(args.getPSUIEngineParams);
      case 'CtrlLoadAndSearch':
        return this.CtrlLoadAndSearch(args.getPSUIEngineParams);
      default:
        LogUtil.warn(`${args.engineType}暂未支持`);
        break;
    }
  }

  /**
   * @description 处理搜索部件加载并搜索（参数可指定触发部件）
   * @param {*} args 引擎参数
   * @return {*}
   * @memberof MobCustomViewEngine
   */
  public CtrlLoadAndSearch(args: any) {
    if (!args || args.length < 1) {
      return null;
    }
    const targetCtrl = args.find((item: any) => {
      return item.name === 'CTRL' && item.paramType === 'CTRL';
    });
    return { triggerCtrlName: 'VIEW', triggerType: 'CtrlLoadAndSearch', targetCtrlName: targetCtrl.ctrlName };
  }

  /**
   * @description 处理部件加载（参数可指定触发部件）
   * @param {*} args
   * @return {*}
   * @memberof MobCustomViewEngine
   */
  public handleCtrlLoad(args: any) {
    if (!args || args.length < 1) {
      return null;
    }
    const triggerCtrl = args.find((item: any) => {
      return item.name === 'TRIGGER' && item.paramType === 'CTRL';
    });
    const targetCtrl = args.find((item: any) => {
      return item.name === 'CTRL' && item.paramType === 'CTRL';
    });
    return { triggerCtrlName: triggerCtrl?.ctrlName, triggerType: 'CtrlLoad', targetCtrlName: targetCtrl.ctrlName };
  }
}
