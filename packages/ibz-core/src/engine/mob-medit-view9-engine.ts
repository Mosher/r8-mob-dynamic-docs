import { ViewEngine } from './view-engine';
import { MobMEditViewPanelEvents, AppViewEvents } from '../interface/event';
/**
 * 实体移动端多表单编辑视图（部件视图）界面引擎
 *
 * @export
 * @class MobMEditView9Engine
 * @extends {ViewEngine}
 */
export class MobMEditView9Engine extends ViewEngine {
  /**
   * 多编辑表单面板
   *
   * @protected
   * @type {*}
   * @memberof MobMEditView9Engine
   */
  protected meditviewpanel: any;

  /**
   * Creates an instance of MobMEditView9Engine.
   * @memberof MobMEditView9Engine
   */
  constructor() {
    super();
  }

  /**
   * 引擎初始化
   *
   * @param {*} [options={}]
   * @memberof MobMEditView9Engine
   */
  public init(options: any = {}): void {
    this.meditviewpanel = options.meditviewpanel;
    super.init(options);
  }

  /**
   * 引擎加载
   *
   * @param {*} [opts={}]
   * @memberof MobMEditView9Engine
   */
  public load(opts: any = {}): void {
    super.load(opts);
    if (this.getMeditviewpanel()) {
      const tag = this.getMeditviewpanel().name;
      Object.assign(this.view.viewParam, opts);
      this.setViewState2({ tag: tag, action: 'load', viewdata: { ...this.view.viewParam } });
    }
  }

  /**
   * 部件事件
   *
   * @param {string} ctrlName
   * @param {string} eventName
   * @param {*} args
   * @memberof MobMEditView9Engine
   */
  public onCtrlEvent(ctrlName: string, eventName: string, args: any): void {
    super.onCtrlEvent(ctrlName, eventName, args);
    if (Object.is(ctrlName, 'meditviewpanel')) {
      this.editviewpanelEvent(eventName, args);
    }
  }

  /**
   * 多编辑表单面板事件分发
   *
   * @param {*} [opts={}]
   * @memberof MobMEditView9Engine
   */
  public editviewpanelEvent(eventName: string, args: any) {
    if (Object.is(eventName, MobMEditViewPanelEvents.LOAD_SUCCESS)) {
      this.emitViewEvent(AppViewEvents.DATA_CHANGE, { action: 'load', data: args, status: 'success' });
    }
    if (Object.is(eventName, MobMEditViewPanelEvents.DATA_SAVE)) {
      this.emitViewEvent(AppViewEvents.DATA_CHANGE, { action: 'save', data: args, status: 'success' });
    }
    if (Object.is(eventName, MobMEditViewPanelEvents.ADD_SUCCESS)) {
      this.emitViewEvent(AppViewEvents.DATA_CHANGE, {
        action: MobMEditViewPanelEvents.DATA_CHANGE,
        data: args,
        status: 'success',
      });
    }
  }

  /**
   * 多编辑表单面板部件
   *
   * @returns {*}
   * @memberof MDViewEngine
   */
  public getMeditviewpanel(): any {
    return this.meditviewpanel;
  }
}
