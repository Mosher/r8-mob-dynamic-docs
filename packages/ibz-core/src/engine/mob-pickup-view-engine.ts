import { ViewEngine } from './view-engine';
import { MobPickupViewPanelEvents } from '../interface/event';
import { IUIDataParam } from '../interface';
/**
 * 实体移动端数据选择视图界面引擎
 *
 * @export
 * @class MobPickupViewEngine
 * @extends {ViewEngine}
 */
export class MobPickupViewEngine extends ViewEngine {
  /**
   * 选择视图面板
   *
   * @type {*}
   * @memberof MobPickupViewEngine
   */
  public pickupViewPanel: any = null;

  /**
   * Creates an instance of MobPickupViewEngine.
   *
   * @memberof MobPickupViewEngine
   */
  constructor() {
    super();
  }

  /**
   * 初始化引擎
   *
   * @param {*} options
   * @memberof MobPickupViewEngine
   */
  public init(options: any): void {
    this.pickupViewPanel = options.pickupviewpanel;
    if (
      options.view.viewParam &&
      options.view.viewParam.selectedData &&
      Array.isArray(options.view.viewParam.selectedData)
    ) {
      options.view.viewSelections = [...options.view.viewParam.selectedData];
    }
    super.init(options);
  }

  /**
   * 引擎加载
   *
   * @memberof MobPickupViewEngine
   */
  public load(opts?: any): void {
    super.load(opts);
    if (this.view) {
      this.view.viewSelections = [];
    }
  }

  /**
   * 部件事件
   *
   * @param {string} ctrlName
   * @param {string} eventName
   * @param {*} args
   * @memberof MobPickupViewEngine
   */
  public onCtrlEvent(ctrlName: string, eventName: string, args: any): void {
    super.onCtrlEvent(ctrlName, eventName, args);
    if (Object.is(eventName, MobPickupViewPanelEvents.SELECT_CHANGE)) {
      this.onSelectionChange(ctrlName, args);
    }
  }

  /**
   * 值选中变化
   *
   * @param {any[]} args
   * @memberof MobPickupViewEngine
   */
  public onSelectionChange(ctrlName: any, args: IUIDataParam): void {
    this.view.viewSelections = [];
    this.view.viewSelections = args.data;
  }

  /**
   * 获取选择视图面板
   *
   * @returns {*}
   * @memberof MobPickupViewEngine
   */
  public getPickupViewPanel(): any {
    return this.pickupViewPanel;
  }
}
