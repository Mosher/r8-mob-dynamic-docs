import { DEMultiDataViewEngine } from './de-multi-data-view-engine';
import { AppViewEvents, MobCalendarEvents } from '../interface/event';

/**
 * 实体移动端日历视图界面引擎
 *
 * @export
 * @class MobCalendarViewEngine
 * @extends {ViewEngine}
 */
export class MobCalendarViewEngine extends DEMultiDataViewEngine {
  /**
   * 日历部件
   *
   * @protected
   * @type {*}
   * @memberof MobCalendarViewEngine
   */
  protected calendar: any;

  /**
   * 初始化日历视图引擎
   *
   * @param {*} [options={}]
   * @memberof MobCalendarViewEngine
   */
  public init(options: any = {}): void {
    this.calendar = options.calendar;
    super.init(options);
  }

  /**
   * 部件事件机制
   *
   * @param {string} ctrlName
   * @param {string} eventName
   * @param {*} args
   * @memberof MobCalendarViewEngine
   */
  public onCtrlEvent(ctrlName: string, eventName: string, args: any): void {
    super.onCtrlEvent(ctrlName, eventName, args);
    if (Object.is(ctrlName, 'calendar')) {
      this.calendarEvent(eventName, args);
    }
  }

  /**
   * 日历事件
   *
   * @param {string} eventName
   * @param {*} args
   * @memberof MobCalendarViewEngine
   */
  public calendarEvent(eventName: string, args: any): void {
    super.MDCtrlEvent(eventName, args);
    if (Object.is(eventName, MobCalendarEvents.LOAD_SUCCESS)) {
      this.onCalendarLoad(args);
    }
  }

  /**
   * 日历数据加载完成
   *
   * @param {*} args
   * @memberof MobCalendarViewEngine
   */
  public onCalendarLoad(arg: any): void {
    this.emitViewEvent(AppViewEvents.LOAD, arg);
    this.emitViewEvent(AppViewEvents.DATA_CHANGE, arg);
  }

  /**
   * 获取日历对象
   *
   * @returns {*}
   * @memberof MobCalendarViewEngine
   */
  public getCalendar(): any {
    return this.calendar;
  }

  /**
   * 获取多数据部件
   *
   * @returns {*}
   * @memberof MobCalendarViewEngine
   */
  public getMDCtrl(): any {
    return this.calendar;
  }
}
