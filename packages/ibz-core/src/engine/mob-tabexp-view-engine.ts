import { MobTabExpPanelEvents } from '../interface/event';
import { MobMDViewEngine } from './mob-md-view-engine';

/**
 * 分页导航视图引擎基础
 *
 * @export
 * @class MobTabExpViewEngine
 * @extends {MobMDViewEngine}
 */
export class MobTabExpViewEngine extends MobMDViewEngine {
  /**
   * Creates an instance of MobTabExpViewEngine.
   *
   *
   * @memberof MobTabExpViewEngine
   */
  constructor() {
    super();
  }

  /**
   * 分页导航面板
   *
   * @protected
   * @type {*}
   * @memberof MobTabExpViewEngine
   */
  protected tabexppanel: any;

  /**
   * 初始化引擎
   *
   * @param {*} options
   * @memberof MobTabExpViewEngine
   */
  public init(options: any): void {
    super.init(options);
    this.tabexppanel = options.tabexppanel;
  }

  /**
   * 引擎加载
   *
   * @memberof MobTabExpViewEngine
   */
  public load(opts?: any): void {
    super.load(opts);
    if (this.tabexppanel) {
      const tag = this.tabexppanel.name;
      Object.assign(this.view.viewParam, opts);
      this.setViewState2({ tag: tag, action: 'load', viewdata: { ...this.view.viewParam } });
    }
  }
}
