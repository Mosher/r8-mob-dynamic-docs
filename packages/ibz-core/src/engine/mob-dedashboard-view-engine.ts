import { AppViewEvents, IUIDataParam, MobSearchFormEvents } from '../interface';
import { ViewEngine } from './view-engine';

export class MobDEDashboardViewEngine extends ViewEngine {

  /**
   * @description 数据面板部件
   * @type {*}
   * @memberof MobDEDashboardViewEngine
   */
  public dashboard: any = null;

  /**
   * @description 搜索表单部件
   * @protected
   * @type {*}
   * @memberof MobDEDashboardViewEngine
   */
  protected searchForm: any = null;

  /**
   * @description 初始化引擎
   * @param {*} options 初始化参数
   * @memberof MobDEDashboardViewEngine
   */
  public init(options: any): void {
    this.dashboard = options.dashboard;
    this.searchForm = options.searchform;
    super.init(options);
  }

  /**
   * @description 引擎加载
   * @memberof MobDEDashboardViewEngine
   */
  public load(opts?: any): void {
    super.load(opts);
    if (this.getSearchForm()) {
      const tag = this.getSearchForm().name;
      this.setViewState2({ tag: tag, action: 'loadDraft', viewdata: { ...this.view.viewParam } });
    }
    if (this.getDashboard() && this.isLoadDefault) {
      const tag = this.getDashboard().name;
      this.setViewState2({ tag: tag, action: 'load', viewdata: { isSearchMode: this.getSearchForm() ? true : false } });
    } else {
      this.isLoadDefault = true;
    }
  }

  /** 
   * @description 部件事件机制
   * @param {string} ctrlName 部件名
   * @param {string} eventName 事件名
   * @param {IUIDataParam} args 参数
   * @memberof MobDEDashboardViewEngine
   */
  public onCtrlEvent(ctrlName: string, eventName: string, args: IUIDataParam): void {
    super.onCtrlEvent(ctrlName, eventName, args);
    if (Object.is(ctrlName, 'dashboard')) {
      this.dashboardEvent(eventName, args);
    }
    if (Object.is(ctrlName, 'searchform') || Object.is(ctrlName, 'quicksearchform')) {
      this.searchFormEvent(eventName, args);
    }
  }

  /**
   * @description 实体数据看板事件
   * @param {string} eventName 事件名
   * @param {IUIDataParam} args 参数
   * @memberof MobDEDashboardViewEngine
   */
  public dashboardEvent(eventName: string, args: IUIDataParam): void {
    if (Object.is(eventName, 'load')) {
      this.emitViewEvent(AppViewEvents.LOAD, args);
    }
  }

  /**
   * @description 搜索表单事件
   * @param {string} eventName
   * @param {*} [args={}]
   * @memberof MobDEDashboardViewEngine
   */
  public searchFormEvent(eventName: string, args: any = {}): void {
    if (Object.is(eventName, MobSearchFormEvents.LOAD_DRAFT_SUCCESS)) {
      this.onSearchFormLoad(args);
    }
    if (Object.is(eventName, MobSearchFormEvents.SEARCH)) {
      this.onSearchFormSearch(args);
    }
    if (Object.is(eventName, MobSearchFormEvents.RESET)) {
      this.onSearchFormReset();
    }
  }

  /**
   * @description 搜索表单加载完成
   * @param {*} [args={}]
   * @memberof MobDEDashboardViewEngine
   */
   public onSearchFormLoad(args: any = {}): void {
    const data = args.data;
    if (this.getDashboard() && this.isLoadDefault) {
      const tag = this.getDashboard().name;
      Object.assign(this.view.viewParam, data[0]);
      this.setViewState2({ tag: tag, action: 'loaddata', viewdata: { ...this.view.viewParam } });
    }
  }

  /**
   * @description 搜索表单搜索
   * @param {*} [args={}]
   * @memberof MobDEDashboardViewEngine
   */
  public onSearchFormSearch(args: any = {}): void {
    if (this.getDashboard() && this.isLoadDefault) {
      const tag = this.getDashboard().name;
      Object.assign(this.view.viewParam, args);
      this.setViewState2({ tag: tag, action: 'loaddata', viewdata: { ...this.view.viewParam } });
    }
  }

  /**
   * @description 搜索表单重置
   * @param {*} [args={}]
   * @memberof MobDEDashboardViewEngine
   */
  public onSearchFormReset(args: any = {}) {
    if (this.getSearchForm()) {
      const tag = this.getSearchForm().name;
      this.setViewState2({ tag: tag, action: 'loadDraft', viewdata: { ...this.view.viewParam } });
    }
  }

  /**
   * @description 获取部件对象
   * @return {*}  {*}
   * @memberof MobDEDashboardViewEngine
   */
  public getDashboard(): any {
    return this.dashboard;
  }

  /**
   * @description 获取搜索表单部件
   * @return {*}  {*}
   * @memberof MobDEDashboardViewEngine
   */
  public getSearchForm(): any {
    return this.searchForm;
  }
}
