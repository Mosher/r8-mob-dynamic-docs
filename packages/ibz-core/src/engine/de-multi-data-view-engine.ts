import { AppMDCtrlEvents, AppViewEvents, IUIDataParam, MobSearchFormEvents } from '../interface';
import { ViewEngine } from './view-engine';

/**
 * @description 多数据视图引擎基类
 * @export
 * @class DEMultiDataViewEngine
 * @extends {ViewEngine}
 */
export class DEMultiDataViewEngine extends ViewEngine {
  /**
   * @description 搜索表单部件
   * @protected
   * @type {*}
   * @memberof DEMultiDataViewEngine
   */
  protected searchForm: any;

  /**
   * @description 初始化
   * @param {*} options
   * @memberof DEMultiDataViewEngine
   */
  public init(options: any) {
    this.searchForm = options.searchform;
    super.init(options);
  }

  /**
   * @description 引擎加载
   * @param {*} [opts={}]
   * @memberof DEMultiDataViewEngine
   */
  public load(opts: any = {}): void {
    super.load(opts);
    if (this.getSearchForm()) {
      const tag = this.getSearchForm().name;
      this.setViewState2({ tag: tag, action: 'loadDraft', viewdata: { ...this.view.viewParam } });
    } else if (this.getMDCtrl() && this.isLoadDefault) {
      const tag = this.getMDCtrl().name;
      Object.assign(this.view.viewParam, opts);
      this.setViewState2({ tag: tag, action: 'load', viewdata: { ...this.view.viewParam } });
    } else {
      this.isLoadDefault = true;
    }
  }

  /**
   * @description 部件事件
   * @param {string} ctrlName
   * @param {string} eventName
   * @param {*} args
   * @memberof DEMultiDataViewEngine
   */
  public onCtrlEvent(ctrlName: string, eventName: string, args: any): void {
    super.onCtrlEvent(ctrlName, eventName, args);
    if (Object.is(ctrlName, 'searchform') || Object.is(ctrlName, 'quicksearchform')) {
      this.searchFormEvent(eventName, args);
    }
  }

  /**
   * @description 搜索表单事件
   * @param {string} eventName
   * @param {*} [args={}]
   * @memberof DEMultiDataViewEngine
   */
  public searchFormEvent(eventName: string, args: any = {}): void {
    if (Object.is(eventName, MobSearchFormEvents.LOAD_DRAFT_SUCCESS)) {
      this.onSearchFormLoad(args);
    }
    if (Object.is(eventName, MobSearchFormEvents.SEARCH)) {
      this.onSearchFormSearch(args);
    }
    if (Object.is(eventName, MobSearchFormEvents.RESET)) {
      this.onSearchFormReset();
    }
  }

  /**
   * @description 搜索表单加载完成
   * @param {*} [args={}]
   * @memberof DEMultiDataViewEngine
   */
  public onSearchFormLoad(args: any = {}): void {
    if (this.getMDCtrl() && this.isLoadDefault) {
      const tag = this.getMDCtrl().name;
      this.setViewState2({ tag: tag, action: 'load', viewdata: { ...this.view.viewParam } });
    }
    this.isLoadDefault = true;
  }

  /**
   * @description 搜索表单搜索
   * @param {*} [args={}]
   * @memberof DEMultiDataViewEngine
   */
  public onSearchFormSearch(args: any = {}): void {
    if (this.getMDCtrl()) {
      const tag = this.getMDCtrl().name;
      this.setViewState2({ tag: tag, action: 'search', viewdata: { ...args } });
    }
  }

  /**
   * @description 搜索表单重置
   * @param {*} [args={}]
   * @memberof DEMultiDataViewEngine
   */
  public onSearchFormReset(args: any = {}) {
    if (this.getSearchForm()) {
      const tag = this.getSearchForm().name;
      this.setViewState2({ tag: tag, action: 'loadDraft', viewdata: { ...this.view.viewParam } });
    }
  }

  /**
   * @description 多数据部件事件处理
   * @param {string} eventName
   * @param {*} args
   * @memberof DEMultiDataViewEngine
   */
  public MDCtrlEvent(eventName: string, args: any): void {
    if (Object.is(eventName, AppMDCtrlEvents.SELECT_CHANGE)) {
      this.selectionChange(args);
    }
    if (Object.is(eventName, AppMDCtrlEvents.LOAD_SUCCESS)) {
      this.MDCtrlLoad(args.data ? args.data : []);
    }
    if (Object.is(eventName, AppMDCtrlEvents.BEFORE_LOAD)) {
      this.MDCtrlBeforeLoad(args);
    }
    if (Object.is(eventName, 'save')) {
      this.emitViewEvent('save', args);
    }
  }

  /**
   * @description 选中变化
   * @param {IUIDataParam} UIDataParam
   * @memberof DEMultiDataViewEngine
   */
  public selectionChange(UIDataParam: IUIDataParam): void {
    const args = UIDataParam.data;
    if (this.view) {
      this.emitViewEvent(AppViewEvents.DATA_CHANGE, UIDataParam);
    }
    const state = args && args.length > 0 && !Object.is(args?.[0].srfkey, '') ? false : true;
    this.calcToolbarItemState(state);
    if (args && args.length > 0) {
      this.calcToolbarItemAuthState(args[0]);
    } else {
      this.calcToolbarItemAuthState(null);
    }
  }

  /**
   * @description 多数据部件加载完成
   * @param {any[]} args
   * @memberof DEMultiDataViewEngine
   */
  public MDCtrlLoad(args: any[]) {
    if (this.view) {
      this.emitViewEvent(AppViewEvents.LOAD, args);
    }
    this.calcToolbarItemState(true);
    this.calcToolbarItemAuthState(null);
  }

  /**
   * @description 多数据部件加载完成
   * @param {*} [data={}]
   * @return {*}  {void}
   * @memberof DEMultiDataViewEngine
   */
  public MDCtrlBeforeLoad(data: any = {}): void {
    if (!data || !data.navParam) {
      return;
    }
    // 快速搜索
    if (this.view && this.view.searchParam && Object.keys(this.view.searchParam).length > 0) {
      Object.keys(this.view.searchParam).forEach((key: string) => {
        data.navParam.viewParam[key] = this.view.searchParam[key];
      });
    }
    // 快速代码表
    if (this.view && this.view.quickGroupParam && Object.keys(this.view.quickGroupParam).length > 0) {
      Object.keys(this.view.quickGroupParam).forEach((key: string) => {
        data.navParam.viewParam[key] = this.view.quickGroupParam[key];
      });
    }
    const otherQueryParam: any = {};
    if (this.getSearchForm()) {
      Object.assign(otherQueryParam, this.getSearchForm().getData());
    }
    if (this.view && this.view.sortValue) {
      Object.assign(otherQueryParam, this.view.sortValue);
    }
    if (otherQueryParam && Object.keys(otherQueryParam).length > 0) {
      Object.keys(otherQueryParam).forEach((key: string) => {
        data.navParam.viewParam[key] = otherQueryParam[key];
      });
    }
  }

  /**
   * @description 获取多数据部件
   * @return {*}  {*}
   * @memberof DEMultiDataViewEngine
   */
  public getMDCtrl(): any {}

  /**
   * @description 获取搜索表单部件
   * @return {*}  {*}
   * @memberof DEMultiDataViewEngine
   */
  public getSearchForm(): any {
    return this.searchForm;
  }
}
