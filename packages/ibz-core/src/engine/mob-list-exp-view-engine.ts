import { ViewEngine } from './view-engine';
/**
 * 实体移动端多数据界面引擎
 *
 * @export
 * @class MDViewEngine
 * @extends {ViewEngine}
 */
export class MobListExpViewEngine extends ViewEngine {
  /**
   * @description 列表部件
   * @type {*}
   * @memberof GridViewEngine
   */
  protected listexpbar: any;

  /**
   * @description 引擎初始化
   * @param {*} [options={}]
   * @memberof GridViewEngine
   */
  public init(options: any = {}): void {
    this.listexpbar = options.listexpbar;
    super.init(options);
  }

  /**
   * @description 多数据部件
   * @param {string} ctrlName
   * @param {string} eventName
   * @param {*} args
   * @memberof MobMDViewEngine
   */
  public onCtrlEvent(ctrlName: string, eventName: string, args: any): void {
    super.onCtrlEvent(ctrlName, eventName, args);
  }

  /**
   * @description 获取多数据部件
   * @returns {*}
   * @memberof MDViewEngine
   */
  public getListExpBar(): any {
    return this.listexpbar;
  }

  /**
   * @description 引擎加载
   * @param {*} opts
   * @return {*}  {*}
   * @memberof MobListExpViewEngine
   */
  public load(opts: any): any {
    super.load(opts);
    if (this.getListExpBar()) {
      const tag = this.getListExpBar().name;
      Object.assign(this.view.viewParam, opts);
      this.setViewState2({ tag: tag, action: 'load', viewdata: { ...this.view.viewparams } });
    } else {
      this.isLoadDefault = true;
    }
  }
}
