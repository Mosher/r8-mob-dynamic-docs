import { AppUITriggerEngine } from './app-ui-trigger-engine';

/**
 * 界面视图事件触发逻辑引擎
 *
 * @export
 * @class AppViewEventEngine
 */
export class AppViewEventEngine extends AppUITriggerEngine {
  /**
   * Creates an instance of AppViewEventEngine.
   * @memberof AppViewEventEngine
   */
  constructor(opts: any) {
    super(opts);
  }
}
