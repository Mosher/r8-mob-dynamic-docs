import { ViewEngine } from './view-engine';

/**
 * 移动端向导视图引擎
 *
 * @export
 * @class MobWizardViewEngine
 */
export class MobWizardViewEngine extends ViewEngine {

    /**
     * 向导面板部件
     *
     * @protected
     * @type {*}
     * @memberof MobWizardViewEngine
     */
    protected wizardpanel: any;

    /**
     * 初始化编辑视图引擎
     *
     * @param {*} [options={}]
     * @memberof MobWizardViewEngine
     */
    public init(options: any = {}): void {
        this.wizardpanel = options.wizardpanel;
        super.init(options);
    }

    /**
     * 引擎加载
     *
     * @param {*} [opts={}]
     * @memberof MobWizardViewEngine
     */
    public load(opts: any = {}): void {
        super.load(opts);
        if (this.getWizardPanel()) {
            const tag = this.getWizardPanel().name;
            Object.assign(this.view.viewParam, opts);
            this.setViewState2({ tag: tag, action: 'load', viewdata: this.view.viewparams });
        }
    }

    /**
     * 部件事件机制
     *
     * @param {string} ctrlName
     * @param {string} eventName
     * @param {*} args
     * @memberof MobWizardViewEngine
     */
    public onCtrlEvent(ctrlName: string, eventName: string, args: any): void {
        if (Object.is(ctrlName, 'wizardpanel')) {
            this.wizardPanelEvent(eventName, args);
        }
        super.onCtrlEvent(ctrlName, eventName, args);
    }

    /**
     * 事件处理
     *
     * @param {string} eventName
     * @param {any[]} args
     * @memberof MobWizardViewEngine
     */
    public wizardPanelEvent(eventName: string, args: any): void {
        if (Object.is(eventName, 'finish')) {
            this.onfinish(args);
        }
    }

    /**
     * 完成
     *
     * @param {*} args
     * @memberof MobWizardViewEngine
     */
    public onfinish(args: any): void {
        this.emitViewEvent('viewdataschange', [args]);
        if(!this.view.viewDefaultUsage){
            this.emitViewEvent('close', null);
        }else{
            this.view.$tabPageExp.onClose(this.view.$route.fullPath);
        }  
    }

    /**
     * 获取向导面板
     *
     * @returns {*}
     * @memberof MobWizardViewEngine
     */
    public getWizardPanel(): any {
        return this.wizardpanel;
    }
}