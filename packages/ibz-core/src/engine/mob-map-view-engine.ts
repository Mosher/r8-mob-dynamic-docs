import { MobMDViewEngine } from './mob-md-view-engine';
import { MobMapEvents } from '../interface/event';

/**
 * 地图视图引擎基础
 *
 * @export
 * @class MobMapViewEngine
 * @extends {MDViewEngine}
 */
export class MobMapViewEngine extends MobMDViewEngine {
  /**
   * 表格部件
   *
   * @type {*}
   * @memberof MobMapViewEngine
   */
  protected map: any;

  /**
   * Creates an instance of MobMapViewEngine.
   * @memberof MobMapViewEngine
   */
  constructor() {
    super();
  }

  /**
   * 引擎初始化
   *
   * @param {*} [options={}]
   * @memberof MobMapViewEngine
   */
  public init(options: any = {}): void {
    this.map = options.map;
    super.init(options);
  }

  /**
   * 获取多数据部件
   *
   * @returns {*}
   * @memberof MobMapViewEngine
   */
  public getMDCtrl(): any {
    return this.map;
  }

  /**
   * 部件事件
   *
   * @param {string} ctrlName
   * @param {string} eventName
   * @param {*} args
   * @memberof MobMapViewEngine
   */
  public onCtrlEvent(ctrlName: string, eventName: string, args: any): void {
    if (Object.is(ctrlName, 'map')) {
      this.MDCtrlEvent(eventName, args);
    }
    super.onCtrlEvent(ctrlName, eventName, args);
  }

  /**
   * 事件处理
   *
   * @param {string} eventName
   * @param {*} args
   * @memberof MobMapViewEngine
   */
  public MDCtrlEvent(eventName: string, args: any): void {
    if (Object.is(eventName, MobMapEvents.SELECT_CHANGE)) {
      // this.selectionChange(args);
      // this.doEdit(args);
      return;
    }
    super.MDCtrlEvent(eventName, args);
  }
}
