import { IUIDataParam } from '../interface';
import { ViewTool } from '../util';
import { MobMDCtrlEvents, MobSearchFormEvents } from '../interface/event';
import { DEMultiDataViewEngine } from './de-multi-data-view-engine';

/**
 * 实体移动端多数据视图界面引擎
 *
 * @export
 * @class MobMDViewEngine
 * @extends {ViewEngine}
 */
export class MobMDViewEngine extends DEMultiDataViewEngine {
  /**
   * 多数据部件
   *
   * @type {*}
   * @memberof MobMDViewEngine
   */
  protected mdCtrl: any;

  /**
   * 引擎初始化
   *
   * @param {*} [options={}]
   * @memberof MobMDViewEngine
   */
  public init(options: any = {}): void {
    this.mdCtrl = options.mdctrl;
    super.init(options);
  }

  /**
   * 部件事件
   *
   * @param {string} ctrlName
   * @param {string} eventName
   * @param {*} args
   * @memberof MobMDViewEngine
   */
  public onCtrlEvent(ctrlName: string, eventName: string, args: any): void {
    super.onCtrlEvent(ctrlName, eventName, args);
    if (Object.is(ctrlName, 'mdctrl')) {
      this.MDCtrlEvent(eventName, args);
    }
  }

  /**
   * 多数据部件事件处理
   *
   * @param {string} eventName
   * @param {any[]} args
   * @memberof MobMDViewEngine
   */
  public MDCtrlEvent(eventName: string, args: any): void {
    super.MDCtrlEvent(eventName, args);
    if (Object.is(eventName, MobMDCtrlEvents.ROW_SELECTED)) {
      this.doEdit(args);
    }
  }

  /**
   * 多数据项界面_编辑操作
   *
   * @param {*} [params={}]
   * @returns {void}
   * @memberof MobMDViewEngine
   */
  public doEdit(params: IUIDataParam): void {
    const { data, event, navParam, sender } = params;
    if (data) {
      ViewTool.opendata(data, this.view, data[0], navParam, event, sender);
    }
  }

  /**
   * 获取多数据部件
   *
   * @returns {*}
   * @memberof MobMDViewEngine
   */
  public getMDCtrl(): any {
    return this.mdCtrl;
  }
}
