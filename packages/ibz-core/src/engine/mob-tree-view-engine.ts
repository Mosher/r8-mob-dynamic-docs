import { AppViewEvents, MobTreeEvents } from '../interface';
import { DEMultiDataViewEngine } from './de-multi-data-view-engine';

/**
 * 实体移动端树视图界面引擎
 *
 * @export
 * @class MobTreeViewEngine
 * @extends {ViewEngine}
 */
export class MobTreeViewEngine extends DEMultiDataViewEngine {
  /**
   * 树部件
   *
   * @protected
   * @type {*}
   * @memberof MobTreeViewEngine
   */
  protected tree: any;

  /**
   * 初始化编辑视图引擎
   *
   * @param {*} [options={}]
   * @memberof MobTreeViewEngine
   */
  public init(options: any = {}): void {
    this.tree = options.tree;
    super.init(options);
  }

  /**
   * 部件事件机制
   *
   * @param {string} ctrlName
   * @param {string} eventName
   * @param {*} args
   * @memberof MobTreeViewEngine
   */
  public onCtrlEvent(ctrlName: string, eventName: string, args: any): void {
    super.onCtrlEvent(ctrlName, eventName, args);
    if (Object.is(ctrlName, 'tree')) {
      this.MDCtrlEvent(eventName, args);
    }
  }

  /**
   * 树事件
   *
   * @param {string} eventName
   * @param {*} args
   * @memberof MobTreeViewEngine
   */
  public MDCtrlEvent(eventName: string, args: any): void {
    if (Object.is(eventName, 'click')) {
      this.onTreeClick(args);
    }
    super.MDCtrlEvent(eventName, args);
  }

  /**
   * 树加载完成
   *
   * @param {*} args
   * @memberof MobTreeViewEngine
   */
  public MDCtrlLoad(args: any): void {
    if (this.view) {
      this.view.datainfo = args.srfmajortext;
      this.emitViewEvent(AppViewEvents.DATA_CHANGE, args);
    }
  }

  /**
   * 树点击事件
   *
   * @param {*} args
   * @memberof MobTreeViewEngine
   */
  public onTreeClick(args: any): void {
    if (this.view) {
      this.emitViewEvent(AppViewEvents.DATA_CHANGE, args);
    }
  }

  /**
   * 获取树对象
   *
   * @returns {*}
   * @memberof MobTreeViewEngine
   */
  public getMDCtrl(): any {
    return this.tree;
  }
}
