/**
 * 拦截器
 *
 * @export
 * @class InterceptorsBase
 */
export abstract class InterceptorsBase {
  /**
   * 拦截器实现接口
   *
   * @private
   * @memberof InterceptorsBase
   */
  public intercept(): void {}
}
