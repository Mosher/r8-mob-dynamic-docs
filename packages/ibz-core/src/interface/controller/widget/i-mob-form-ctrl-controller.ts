import { IPSDEForm } from '@ibiz/dynamic-model-api';
import { Subject } from 'rxjs';
import { IParam } from '../../common';
import { ICtrlActionResult, IEditorEventParam, IViewStateParam } from '../../params';
import { IAppCtrlControllerBase } from './i-app-ctrl-controller-base';

export interface IMobFormCtrlController extends IAppCtrlControllerBase {
  /**
   * 表单模型实例对象
   *
   * @type {IPSDEForm}
   * @memberof IMobFormCtrlController
   */
  controlInstance: IPSDEForm;

  /**
   * 表单数据对象
   *
   * @type {*}
   * @memberof IMobFormCtrlController
   */
  data: IParam;

  /**
   * 详情模型集合
   *
   * @type {*}
   * @memberof IMobFormCtrlController
   */
  detailsModel: IParam;

  /**
   * 表单错误提示信息
   *
   * @type {any[]}
   * @memberof  IMobFormCtrlController
   */
  errorMessages: any[];

  /**
   * 值规则
   *
   * @type {IParam}
   * @memberof MobFormCtrlController
   */
  rules: IParam;

  /**
   * 表单状态
   *
   * @type {Subject<IViewStateParam>}
   * @memberof IMobFormCtrlController
   */
  formState: Subject<IViewStateParam>;

  /**
   * 忽略表单项值变化
   *
   * @type {boolean}
   * @memberof IMobFormCtrlController
   */
  ignorefieldvaluechange: boolean;

  /**
   * @description 表单分组锚点集合
   * @type {IParam[]}
   * @memberof IMobFormCtrlController
   */
  groupAnchors: IParam[];

  /**
   * 加载
   *
   * @param opts 行为参数
   * @param args 补充逻辑完成参数
   * @param showInfo 是否显示提示信息
   * @param loadding 是否显示loading效果
   * @returns {Promise<IParam>}
   * @memberof IMobFormCtrlController
   */
  load(opts?: IParam, args?: IParam, showInfo?: boolean, loadding?: boolean): Promise<ICtrlActionResult>;

  /**
   * 加载草稿
   *
   * @param opts 行为参数
   * @param args 补充逻辑完成参数
   * @param showInfo 是否显示提示信息
   * @param loadding 是否显示loading效果
   * @returns {Promise<IParam>}
   * @memberof IMobFormCtrlController
   */
  loadDraft(opts?: IParam, args?: IParam, showInfo?: boolean, loadding?: boolean): Promise<ICtrlActionResult>;

  /**
   * 表单自动加载
   *
   * @param opts 行为参数
   * @param args 补充逻辑完成参数
   * @param showInfo 是否显示提示信息
   * @param loadding 是否显示loading效果
   * @returns {Promise<IParam>}
   * @memberof IMobFormCtrlController
   */
  autoLoad(opts?: IParam, args?: IParam, showInfo?: boolean, loadding?: boolean): Promise<ICtrlActionResult>;

  /**
   * 保存
   *
   * @param opts 行为参数
   * @param args 补充逻辑完成参数
   * @param showInfo 是否显示提示信息
   * @param loadding 是否显示loading效果
   * @return {Promise<IParam>}
   * @memberof IMobFormCtrlController
   */
  save(opts?: IParam, args?: IParam, showInfo?: boolean, loadding?: boolean): Promise<ICtrlActionResult>;

  /**
   * 删除
   *
   * @param opts 行为参数
   * @param args 补充逻辑完成参数
   * @param showInfo 是否显示提示信息
   * @param loadding 是否显示loading效果
   * @return {Promise<IParam>}
   * @memberof IMobFormCtrlController
   */
  remove(opts?: IParam, args?: IParam, showInfo?: boolean, loadding?: boolean): Promise<ICtrlActionResult>;

  /**
   * 工作流启动
   *
   * @param opts 行为参数（表单数据）
   * @param args 补充逻辑完成参数
   * @param showInfo 是否显示提示信息
   * @param loadding 是否显示loading效果
   * @returns {Promise<IParam>}
   * @memberof IMobFormCtrlController
   */
  wfstart(opts?: IParam, args?: IParam, showInfo?: boolean, loadding?: boolean): Promise<ICtrlActionResult>;

  /**
   * 工作流提交
   *
   * @param opts 行为参数（表单数据）
   * @param args 补充逻辑完成参数
   * @param showInfo 是否显示提示信息
   * @param loadding 是否显示loading效果
   * @returns {Promise<IParam>}
   * @memberof IMobFormCtrlController
   */
  wfsubmit(opts?: IParam, args?: IParam, showInfo?: boolean, loadding?: boolean): Promise<ICtrlActionResult>;

  /**
   * 面板行为
   *
   * @param opts 行为参数（表单数据）
   * @param args 补充逻辑完成参数
   * @param showInfo 是否显示提示信息
   * @param loadding 是否显示loading效果
   * @returns {Promise<IParam>}
   * @memberof IMobFormCtrlController
   */
  panelAction(opts?: IParam, args?: IParam, showInfo?: boolean, loadding?: boolean): Promise<ICtrlActionResult>;

  /**
   * 表单项值变更
   *
   * @param {{ name: string, value: any }:IParam} event 名称，值
   * @returns {void}
   * @memberof IMobFormCtrlController
   */
  onFormItemValueChange(event: IParam): void;

  /**
   * 处理编辑器事件
   *
   * @param {args: IEditorEventParam} 参数
   * @returns {void}
   * @memberof IMobFormCtrlController
   */
  handleEditorEvent(args: IEditorEventParam): void;

  /**
   * 执行按钮行为
   *
   * @param {*} formdetail 行为模型
   * @param {*} data 数据
   * @param {*} event 事件源
   * @memberof IMobFormCtrlController
   */
  executeButtonAction(formdetail: IParam, data: IParam, event: MouseEvent): void;
}
