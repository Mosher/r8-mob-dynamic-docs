import { IPSDETabViewPanel, IPSTabExpPanel } from '@ibiz/dynamic-model-api';
import { IAppCtrlControllerBase } from './i-app-ctrl-controller-base';

export interface IMobTabExpPanelCtrlController extends IAppCtrlControllerBase {
  /**
   * @description 移动端分页导航面板部件实例对象
   * @type {IPSDEMobMDCtrl}
   * @memberof IMobTabExpPanelCtrlController
   */
  controlInstance: IPSTabExpPanel;

  /**
   * @description 激活项
   * @type {String}
   * @memberof IMobTabExpPanelCtrlController
   */
  activeItem: String;

  /**
   * @description 获取计数器数据
   * @returns {string | number | null}
   * @memberof IMobTabExpPanelCtrlController
   */
  getCounterData(viewPanel: IPSDETabViewPanel): string | number | null;

  /**
   * @description 激活项改变
   * @param {string} activeItem 激活项
   * @memberof IMobTabExpPanelCtrlController
   */
  activeItemChange(activeItem: string): void;
}
