import { Subject } from 'rxjs';
import { IPSDEWizardPanel } from '@ibiz/dynamic-model-api';
import { IParam, IViewStateParam } from '../../../interface';
import { IAppCtrlControllerBase } from './i-app-ctrl-controller-base';

/**
 * 移动端状态向导面板控制器接口
 *
 * @export
 * @class IMobStateWizardPanelController
 */
export interface IMobStateWizardPanelController extends IAppCtrlControllerBase {

  /**
   * 移动端门户部件实例
   *
   * @protected
   * @type {IPSDEWizardPanel}
   * @memberof IMobStateWizardPanelController
   */
  controlInstance: IPSDEWizardPanel;

  /**
   * @description 向导步骤集合
   * @type {IParam[]}
   * @memberof IMobStateWizardPanelController
   */
  steps: IParam[];

  /**
   * @description 当前激活步骤表单
   * @type {string}
   * @memberof IMobStateWizardPanelController
   */
  activeForm: string;

  /**
   * @description 向导表单集合
   * @type {any[]}
   * @memberof IMobStateWizardPanelController
   */
  wizardForms: any[];

  /**
   * @description 步骤行为集合
   * @type {IParam}
   * @memberof IMobStateWizardPanelController
   */
  stepActions: IParam;

  /**
   * @description 向导面板全局通讯对象
   * @type {Subject<IViewStateParam>}
   * @memberof IMobStateWizardPanelController
   */
  wizardState: Subject<IViewStateParam>;

  /**
   * @description 处理上一步
   * @memberof IMobStateWizardPanelController
   */
  handlePrevious(): void;

  /**
   * @description 处理下一步
   * @memberof IMobStateWizardPanelController
   */
  handleNext(): void;

  /**
   * @description 处理完成
   * @memberof IMobStateWizardPanelController
   */
  handleFinish(): void;
}
