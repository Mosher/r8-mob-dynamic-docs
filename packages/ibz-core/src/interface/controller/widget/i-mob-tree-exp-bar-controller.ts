import { IPSTreeExpBar } from '@ibiz/dynamic-model-api';
import { IAppExpBarCtrlController } from './i-app-exp-bar-ctrl-controller';

/**
 * 移动端树导航部件接口
 *
 * @export
 * @class IMobTreeExpBarController
 */
export interface IMobTreeExpBarController extends IAppExpBarCtrlController {

  /**
   * 移动端树导航部件实例
   *
   * @protected
   * @type {IPSDEMobTreeExpBar}
   * @memberof IMobTreeExpBarController
   */
  controlInstance: IPSTreeExpBar;
  
}
