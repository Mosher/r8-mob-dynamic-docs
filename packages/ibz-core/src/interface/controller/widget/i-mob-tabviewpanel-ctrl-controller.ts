import { IPSDETabViewPanel } from '@ibiz/dynamic-model-api';
import { IParam } from '../../../interface';
import { IAppCtrlControllerBase } from './i-app-ctrl-controller-base';

export interface IMobTabViewPanelCtrlController extends IAppCtrlControllerBase {
  /**
   * 移动端分页视图面板部件实例对象
   *
   * @type {IPSDEMobMDCtrl}
   * @memberof IMobTabViewPanelCtrlController
   */
  controlInstance: IPSDETabViewPanel;

  /**
   * @description 获取导航参数
   * @return {{context: IParam, viewParam: IParam}}  {{ context: IParam, viewParam: IParam }}
   * @memberof IMobTabViewPanelCtrlController
   */
  getNavParams(): { context: IParam; viewParam: IParam };
}
