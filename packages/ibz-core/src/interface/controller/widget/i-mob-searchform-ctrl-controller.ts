import { IPSDESearchForm } from '@ibiz/dynamic-model-api';
import { IParam } from '../../common';
import { IMobFormCtrlController } from './i-mob-form-ctrl-controller';

/**
 * @description 搜索表单接口
 * @export
 * @interface IMobSearchFormCtrlController
 * @extends {IMobFormCtrlController}
 */
export interface IMobSearchFormCtrlController extends IMobFormCtrlController {
  /**
   * @description 搜索表单模型实例对象
   * @type {IPSDESearchForm}
   * @memberof IMobSearchFormCtrlController
   */
  controlInstance: IPSDESearchForm;

  /**
   * @description 是否展开搜索表单
   * @type {Boolean}
   * @memberof IMobSearchFormCtrlController
   */
  isExpandSearchForm: Boolean;

  /**
   * @description 默认展开搜索表单
   * @type {boolean}
   * @memberof IMobSearchFormCtrlController
   */
  expandSearchForm: boolean;

  /**
   * @description 表单项值变更
   * @param {{ name: string; value: IParam }} event
   * @memberof IMobSearchFormCtrlController
   */
  onFormItemValueChange(event: { name: string; value: IParam }): void;

  /**
   * @description 表单项值变更
   * @memberof IMobSearchFormCtrlController
   */
  onSearch(): void;
}
