import { IParam } from '../../common';
import { ICtrlActionResult } from '../../params';
import { IPSDEMultiEditViewPanel } from '@ibiz/dynamic-model-api';
import { IAppMDCtrlController } from './i-app-md-ctrl-controller';

export interface IMobMEditViewPanelCtrlController extends IAppMDCtrlController {
  /**
   * @description 移动端多表单编辑视图面板部件实例对象
   * @type {IPSDEMultiEditViewPanel}
   * @memberof IMobMEditViewPanelCtrlController
   */
  controlInstance: IPSDEMultiEditViewPanel;

  /**
   * @description 多编辑表单面板样式
   * @type {string}
   * @memberof IMobMEditViewPanelCtrlController
   */
  panelStyle: string;

  /**
   * @description 加载数据
   * @param {IParam} [opts] 行为参数
   * @param {IParam} [args] 附加行为参数
   * @param {boolean} [showInfo] 是否显示提示信息
   * @param {boolean} [loadding] 是否显示loadding效果
   * @return {*}  {Promise<ICtrlActionResult>}
   * @memberof IMobMEditViewPanelCtrlController
   */
  load(opts?: IParam, args?: IParam, showInfo?: boolean, loadding?: boolean): Promise<ICtrlActionResult>;

  /**
   * @description 保存数据
   * @param {IParam} [opts] 行为参数
   * @param {IParam} [args] 附加行为参数
   * @param {boolean} [showInfo] 是否显示提示信息
   * @param {boolean} [loadding] 是否显示loadding效果
   * @memberof IMobMEditViewPanelCtrlController
   */
  save(opts?: IParam, args?: IParam, showInfo?: boolean, loadding?: boolean): void;

  /**
   * @description
   * @param {IParam} [opts] 行为参数
   * @param {IParam} [args] 附加行为参数
   * @param {boolean} [showInfo] 是否显示提示信息
   * @param {boolean} [loadding] 是否显示loadding效果
   * @return {*}  {Promise<ICtrlActionResult>}
   * @memberof IMobMEditViewPanelCtrlController
   */
  add(opts?: IParam, args?: IParam, showInfo?: boolean, loadding?: boolean): Promise<ICtrlActionResult>;

  /**
   * @description 嵌入视图数据改变
   * @param {*} $event 视图抛出数据
   * @memberof IMobMEditViewPanelCtrlController
   */
  viewDataChange($event: IParam): void;
}
