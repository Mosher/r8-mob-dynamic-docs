import { IEditorEventParam } from '../../../interface';
import { IParam } from '../../common';
import { IAppCtrlControllerBase } from './i-app-ctrl-controller-base';

export interface IMobPanelController extends IAppCtrlControllerBase {
  /**
   * @description 面板模式 [layout: 项布局面板，view：视图面板]
   * @type {('layout' | 'view')}
   * @memberof IMobPanelController
   */
  panelMode: 'layout' | 'view';

  /**
   * @description 面板数据
   * @type {IParam}
   * @memberof IMobPanelController
   */
  data: IParam;

  /**
   * @description 面板项模型集合
   * @type {IParam}
   * @memberof IMobPanelController
   */
  detailsModel: IParam;

  /**
   * @description 数据映射（用于保存列表项相关信息）
   * @type {Map<string, IParam>}
   * @memberof IMobPanelController
   */
  dataMap: Map<string, IParam>;

  /**
   * @description 加载数据
   * @return {*}  {Promise<void>}
   * @memberof IMobPanelController
   */
  loadPanelData(): Promise<void>;

  /**
   * @description 获取数据项集合
   * @return {*}  {IParam[]}
   * @memberof IMobPanelController
   */
  getDataItems(): IParam[];

  /**
   * @description 填充面板数据
   * @param {IParam} data
   * @memberof IMobPanelController
   */
  fillPanelData(data: IParam): void;

  /**
   * @description 面板编辑项值变更
   * @param {IParam} event
   * @memberof IMobPanelController
   */
  onPanelItemValueChange(event: IParam): void;

  /**
   * @description 处理编辑器事件
   * @param {IEditorEventParam} args
   * @memberof IMobPanelController
   */
  handleEditorEvent(args: IEditorEventParam): void;
}
