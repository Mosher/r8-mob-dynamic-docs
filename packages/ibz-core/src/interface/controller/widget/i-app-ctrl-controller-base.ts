import { Subject } from 'rxjs';
import { IAppCtrlHooks } from '../../hooks';
import { IUIUtilParam, IUIDataParam, IUIEnvironmentParam, ICtrlActionResult, IViewStateParam } from '../../params';
import { IParam } from '../../common';
import {} from '../../params';
import { IPSAppCounterRef, IPSControl } from '@ibiz/dynamic-model-api';
import { ILoadingService } from '../../../interface';

/**
 * 部件接口
 *
 * @export
 * @interface IAppCtrlControllerBase
 */
export interface IAppCtrlControllerBase {
  /**
   * 部件模型实例对象
   *
   * @memberof IAppCtrlControllerBase
   */
  controlInstance: IPSControl;

  /**
   * 视图钩子对象
   *
   * @type {IAppCtrlHooks}
   * @memberof IAppCtrlControllerBase
   */
  hooks: IAppCtrlHooks;

  /**
   * 名称
   *
   * @type {string}
   * @memberof IAppCtrlControllerBase
   */
  name: string;

  /**
   * 部件显示模式
   *
   * @param {string}
   * @memberof IAppCtrlControllerBase
   */
  ctrlShowMode: string;

  /**
   * 部件类型
   *
   * @type {string}
   * @memberof IAppCtrlControllerBase
   */
  ctrlType: string;

  /**
   * 部件样式
   *
   * @type {string}
   * @memberof IAppCtrlControllerBase
   */
  ctrlStyle: string;

  /**
   * 视图通讯对象
   *
   * @type {*}
   * @memberof IAppCtrlControllerBase
   */
  viewState: Subject<IViewStateParam>;

  /**
   * 应用上下文
   *
   * @type {*}
   * @memberof IAppCtrlControllerBase
   */
  context: IParam;

  /**
   * 视图参数
   *
   * @type {*}
   * @memberof IAppCtrlControllerBase
   */
  viewParam: IParam;

  /**
   * 模型数据是否加载完成
   *
   * @memberof IAppCtrlControllerBase
   */
  controlIsLoaded: boolean;

  /**
   * 界面行为模型
   *
   * @type {*}
   * @memberof IAppCtrlControllerBase
   */
  actionModel: IParam;

  /**
   * 模型服务
   *
   * @type {IParam}
   * @memberof IAppCtrlControllerBase
   */
  modelService: IParam;

  /**
   * 部件服务对象
   *
   * @type {IParam}
   * @memberof IAppCtrlControllerBase
   */
  ctrlService: IParam;

  /**
   * 计数器服务对象集合
   *
   * @type {Array<IParam>}
   * @memberof IAppCtrlControllerBase
   */
  counterServiceArray: Array<IParam>;

  /**
   * 外部传入数据对象
   *
   * @type {Array<IParam>}
   * @memberof IAppCtrlControllerBase
   */
  navDatas: Array<IParam>;

  /**
   * 视图操作参数集合
   *
   * @type {IUIUtilParam}
   * @memberof IAppCtrlControllerBase
   */
  viewCtx: IUIUtilParam;

  /**
   * 界面触发逻辑Map
   *
   * @memberof IAppCtrlControllerBase
   */
  ctrlTriggerLogicMap: Map<string, IParam>;

  /**
   * 原生界面触发逻辑集合
   *
   * @memberof IAppCtrlControllerBase
   */
  realCtrlTriggerLogicArray: Array<IParam>;

  /**
   * 部件引用控制器集合
   *
   * @memberof IAppCtrlControllerBase
   */
  ctrlRefsMap: Map<string, IParam>;
  
  /**
   * 预览数据
   *
   * @memberof IAppViewControllerBase
   */
  previewData: IParam[];

  /**
   * 是否部件已经完成
   *
   * @type {boolean}
   * @memberof IAppCtrlControllerBase
   */
  hasCtrlMounted: boolean;

  /**
   * 显示处理提示
   *
   * @type {boolean}
   * @memberof IAppCtrlControllerBase
   */
  showBusyIndicator: boolean;

  /**
   * 部件行为--load
   *
   * @type {*}
   * @memberof IAppCtrlControllerBase
   */
  loadAction: string;

  /**
   * 部件行为--loaddraft
   *
   * @type {*}
   * @memberof IAppCtrlControllerBase
   */
  loaddraftAction: string;

  /**
   * 部件行为--create
   *
   * @type {*}
   * @memberof IAppCtrlControllerBase
   */
  createAction?: string;

  /**
   * 部件行为--remove
   *
   * @type {*}
   * @memberof IAppCtrlControllerBase
   */
  removeAction: string;

  /**
   * 部件行为--update
   *
   * @type {*}
   * @memberof IAppCtrlControllerBase
   */
  updateAction: string;

  /**
   * 部件行为--submit
   *
   * @type {*}
   * @memberof IAppCtrlControllerBase
   */
  WFSubmitAction?: string;

  /**
   * 部件行为--start
   *
   * @type {*}
   * @memberof IAppCtrlControllerBase
   */
  WFStartAction?: string;

  /**
   * 部件加载服务
   *
   * @type {*}
   * @memberof IAppCtrlControllerBase
   */
  ctrlLoadingService: ILoadingService;

  /**
   * 开启下拉刷新
   *
   * @type {boolean}
   * @memberof IAppCtrlControllerBase
   */
  enablePullDownRefresh: boolean;

  /**
   * 应用数据服务对象
   *
   * @type {IParam}
   * @memberof IAppCtrlControllerBase
   */
  appDataService: IParam;

  /**
   * 应用界面服务对象
   *
   * @type {IParam}
   * @memberof IAppCtrlControllerBase
   */
  appUIService: IParam;

  /**
   * 应用实体codeName
   *
   * @readonly
   * @memberof IAppCtrlControllerBase
   */
  appDeCodeName: string;

  /**
   * 应用实体映射实体名称
   *
   * @readonly
   * @memberof IAppCtrlControllerBase
   */
  deName: string;

  /**
   * 应用实体主键属性codeName
   *
   * @readonly
   * @memberof IAppCtrlControllerBase
   */
  appDeKeyFieldName: string;

  /**
   * 应用实体主信息属性codeName
   *
   * @readonly
   * @memberof IAppCtrlControllerBase
   */
  appDeMajorFieldName: string;

  /**
   * 开始加载
   *
   * @memberof IAppCtrlControllerBase
   */
  ctrlBeginLoading(): void;

  /**
   * 结束加载
   *
   * @memberof IAppCtrlControllerBase
   */
  ctrlEndLoading(): void;

  /**
   * 获取部件类型
   *
   * @returns {string}
   * @memberof IAppCtrlControllerBase
   */
  getControlType(): string;

  /**
   * 获取多项数据
   *
   * @returns {IParam[]}
   * @memberof IAppCtrlControllerBase
   */
  getDatas(): IParam[];

  /**
   * 获取单项数据
   *
   * @returns IParam
   * @memberof IAppCtrlControllerBase
   */
  getData(): IParam;

  /**
   * 获取逻辑UI数据
   *
   * @returns {IUIDataParam}
   * @memberof IAppCtrlControllerBase
   */
  getUIDataParam(): IUIDataParam;

  /**
   * 部件初始化
   *
   * @public
   * @memberof IAppCtrlControllerBase
   */
  controlInit(): Promise<boolean>;

  /**
   * 初始化输入数据
   *
   * @public
   * @memberof IAppCtrlControllerBase
   */
  initInputData(opts: IParam): void;

  /**
   * 部件销毁
   *
   * @returns {IUIEnvironmentParam}
   * @memberof IAppCtrlControllerBase
   */
  controlDestroy(): void;

  /**
   * 获取逻辑环境数据
   *
   * @returns {IUIEnvironmentParam}
   * @memberof IAppCtrlControllerBase
   */
  getUIEnvironmentParam(): IUIEnvironmentParam;

  /**
   * 重置
   *
   * @memberof IAppCtrlControllerBase
   */
  onReset(): void;

  /**
   * 关闭视图
   *
   * @param {any} args
   * @memberof IAppCtrlControllerBase
   */
  closeView(args: IParam): void;

  /**
   * 刷新数据
   *
   * @param opts 行为参数
   * @param args 补充逻辑完成参数
   * @param showInfo 是否显示提示信息
   * @param loadding 是否显示loading效果
   * @memberof IAppCtrlControllerBase
   */
  refresh(opts?: IParam, args?: IParam, showInfo?: boolean, loadding?: boolean): Promise<ICtrlActionResult>;

  /**
   * 下拉加载更多数据
   *
   * @memberof IAppCtrlControllerBase
   */
  pullDownRefresh(): Promise<IParam>;

  /**
   * 处理部件界面行为点击
   *
   * @param {*} data 当前列数据
   * @param {*} event 事件源
   * @param {*} detail 界面行为模型对象
   * @memberof IAppCtrlControllerBase
   */
  handleActionClick(data: IParam, event: IParam, detail: IParam): void;

  /**
   * 处理部件事件
   *
   * @param {string} controlname 部件名称
   * @param {string} action 行为
   * @param {*} data 数据
   */
  handleCtrlEvent(controlname: string, action: string, data: any): void;

  /**
   * 部件事件 抛出
   *
   * @param action 部件行为
   * @param data 抛出数据
   * @memberof IAppCtrlControllerBase
   */
  ctrlEvent(action: string, data: any): void;

  /**
   * 重置
   *
   * @memberof AppCtrlControllerBase
   */
  onReset(): void;

  /**
   * @description 获取计数器服务
   * @param {IPSAppCounterRef} [appCounterRef]
   * @return {*}  {(IParam | null)}
   * @memberof IAppCtrlControllerBase
   */
  getCounterService(appCounterRef?: IPSAppCounterRef): IParam | null;
}
