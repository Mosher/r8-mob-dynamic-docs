import { IPSCalendarExpBar } from '@ibiz/dynamic-model-api';
import { IAppExpBarCtrlController } from './i-app-exp-bar-ctrl-controller';

/**
 * 移动端地图导航部件接口
 *
 * @export
 * @class IMobCalendarExpBarController
 */
export interface IMobCalendarExpBarController extends IAppExpBarCtrlController {

  /**
   * 移动端地图导航部件实例
   *
   * @protected
   * @type {IPSDEMobCalendarExpBar}
   * @memberof IMobCalendarExpBarController
   */
  controlInstance: IPSCalendarExpBar;
  
}
