import { IPSListExpBar } from '@ibiz/dynamic-model-api';
import { IAppExpBarCtrlController } from '../../../interface';

/**
 * 移动端列表导航部件接口
 *
 * @export
 * @class IMobListExpBarCtrlController
 */
export interface IMobListExpBarCtrlController extends IAppExpBarCtrlController {

  /**
   * 移动端列表导航部件实例
   *
   * @protected
   * @type {IPSListExpBar}
   * @memberof IMobListExpBarController
   */
  controlInstance: IPSListExpBar;
  
}
