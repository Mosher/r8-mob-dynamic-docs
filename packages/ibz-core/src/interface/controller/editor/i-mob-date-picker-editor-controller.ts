import { IEditorControllerBase } from './i-editor-controller-base';

/**
 * 日期选择器控制器
 *
 * @export
 * @class IMobDatePickerEditorController
 * @extends {IEditorControllerBase}
 */
export interface IMobDatePickerEditorController extends IEditorControllerBase { }
