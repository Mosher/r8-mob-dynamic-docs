import { IEditorControllerBase } from './i-editor-controller-base';

/**
 * 多选框
 *
 * @export
 * @class IMobCheckBoxEditorController
 * @extends {IEditorControllerBase}
 */
export interface IMobCheckBoxEditorController extends IEditorControllerBase { };
