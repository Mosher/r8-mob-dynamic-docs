import { IAppDEMultiDataViewController } from './i-app-de-multi-data-view-controller';

/**
 * 移动端实体树视图控制器接口
 *
 * @exports
 * @interface IMobTreeViewController
 * @extends IAppDEMultiDataViewController
 */
export type IMobTreeViewController = IAppDEMultiDataViewController;
