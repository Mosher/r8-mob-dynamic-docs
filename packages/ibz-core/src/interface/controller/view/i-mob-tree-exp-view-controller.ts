import { IPSAppDEMobTreeExplorerView } from '@ibiz/dynamic-model-api';
import { IAppDEExpViewController } from './i-app-de-exp-view-controller';

/**
 * 移动端树导航视图接口
 *
 * @export
 * @class IMobCustomViewController
 */
export interface IMobTreeExpViewController extends IAppDEExpViewController {

  /**
   * 移动端树导航视图实例
   *
   * @protected
   * @type {IPSAppDEMobTreeExpView}
   * @memberof IMobTreeExpViewController
   */
  viewInstance: IPSAppDEMobTreeExplorerView;
  
}
