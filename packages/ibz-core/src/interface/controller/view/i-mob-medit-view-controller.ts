import { IPSAppDEMobMEditView } from '@ibiz/dynamic-model-api';
import { IAppDEMultiDataViewController } from './i-app-de-multi-data-view-controller';

/**
 * 移动端多表单编辑视图接口
 *
 * @export
 * @class IMobMEditViewController
 */
export interface IMobMEditViewController extends IAppDEMultiDataViewController {
  /**
   * 视图实例
   *
   * @protected
   * @type {IPSAppDEMobMEditView}
   * @memberof IMobMEditViewController
   */
  viewInstance: IPSAppDEMobMEditView;
}
