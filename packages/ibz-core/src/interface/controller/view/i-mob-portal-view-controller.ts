import { IAppViewControllerBase } from './i-app-view-controller-base';

/**
 * 移动端应用看板视图控制器接口
 *
 * @exports
 * @interface IMobPortalViewController
 * @extends IAppViewControllerBase
 */
export type IMobPortalViewController = IAppViewControllerBase;
