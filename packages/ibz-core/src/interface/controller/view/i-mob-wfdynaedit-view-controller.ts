import { IPSAppDEMobWFDynaEditView, IPSDEForm } from '@ibiz/dynamic-model-api';
import { IAppDEViewController } from './i-app-de-view-controller';

/**
 * 移动端动态工作流编辑视图接口
 *
 * @export
 * @class IMobCustomViewController
 */
export interface IMobWFDynaEditViewController extends IAppDEViewController {

  /**
   * 移动端动态工作流编辑视图实例
   *
   * @protected
   * @type {IPSAppDEMobWFDynaEditView}
   * @memberof IMobWFDynaEditViewController
   */
  viewInstance: IPSAppDEMobWFDynaEditView;

  /**
   * @description 激活表单实例
   * @type {IPSDEForm}
   * @memberof IMobWFDynaEditViewController
   */
  editFormInstance: IPSDEForm;

  /**
   * @description 工具栏数据
   * @type {Array<any>}
   * @memberof IMobWFDynaEditViewController
   */
  linkModel: Array<any>;

  /**
   * @description 动态工具栏按钮点击
   * @param {*} linkItem
   * @param {*} [event]
   * @memberof IMobWFDynaEditViewController
   */
  dynamicToolbarClick(linkItem: any, event?: any): void;
  
}
