import { IPSAppDEMobPanelView } from '@ibiz/dynamic-model-api';
import { IAppDEViewController } from './i-app-de-view-controller';

/**
 * 移动端面板视图接口
 *
 * @export
 * @class IMobPanelViewController
 */
export interface IMobPanelViewController extends IAppDEViewController {
  /**
   * 视图实例
   *
   * @protected
   * @type {IPSAppDEMobPanelView}
   * @memberof IMobPanelViewController
   */
  viewInstance: IPSAppDEMobPanelView;
}
