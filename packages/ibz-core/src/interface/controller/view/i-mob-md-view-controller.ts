import { IPSAppDEMobMDView } from '@ibiz/dynamic-model-api';
import { IAppDEMultiDataViewController } from './i-app-de-multi-data-view-controller';

/**
 * 移动端多数据视图接口
 *
 * @export
 * @class IMobMDViewController
 */
export interface IMobMDViewController extends IAppDEMultiDataViewController {
  /**
   * 视图实例
   *
   * @protected
   * @type {IPSAppDEMobMDView}
   * @memberof IMobMDViewController
   */
  viewInstance: IPSAppDEMobMDView;
}
