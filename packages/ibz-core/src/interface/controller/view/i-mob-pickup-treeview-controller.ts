import { IMobTreeViewController } from './i-mob-tree-view-controller';

/**
 * 移动端选择树视图控制器接口
 *
 * @exports
 * @interface IMobPickupTreeViewController
 */
export type IMobPickupTreeViewController = IMobTreeViewController;
