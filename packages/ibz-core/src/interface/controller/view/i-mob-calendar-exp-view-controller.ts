import { IPSAppDEMobCalendarExplorerView } from '@ibiz/dynamic-model-api';
import { IAppDEExpViewController } from './i-app-de-exp-view-controller';

/**
 * 移动端日历导航视图接口
 *
 * @export
 * @class IMobCalendarExpViewController
 */
export interface IMobCalendarExpViewController extends IAppDEExpViewController {

  /**
   * 移动端地图导航视图实例
   *
   * @protected
   * @type {IPSAppDEMobCalendarExpView}
   * @memberof IMobCalendarExpViewController
   */
  viewInstance: IPSAppDEMobCalendarExplorerView;

}
