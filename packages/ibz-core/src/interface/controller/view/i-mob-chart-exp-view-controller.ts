import { IPSAppDEMobChartExplorerView } from '@ibiz/dynamic-model-api';
import { IAppDEExpViewController } from './i-app-de-exp-view-controller';

/**
 * 移动端图表导航视图接口
 *
 * @export
 * @class IMobCustomViewController
 */
export interface IMobChartExpViewController extends IAppDEExpViewController {

  /**
   * 移动端图表导航视图实例
   *
   * @protected
   * @type {IPSAppDEMobChartExpView}
   * @memberof IMobChartExpViewController
   */
  viewInstance: IPSAppDEMobChartExplorerView;

}
