import { IPSAppDEMobMapView } from '@ibiz/dynamic-model-api';
import { IAppDEMultiDataViewController } from './i-app-de-multi-data-view-controller';

/**
 * 移动端地图视图接口
 *
 * @export
 * @class IMobMapViewController
 */
export interface IMobMapViewController extends IAppDEMultiDataViewController {
  /**
   * 视图实例
   *
   * @protected
   * @type {IPSAppDEMobMapView}
   * @memberof IMobMapViewController
   */
  viewInstance: IPSAppDEMobMapView;
}
