import { IPSAppDEMobCalendarView } from '@ibiz/dynamic-model-api';
import { IAppDEMultiDataViewController } from './i-app-de-multi-data-view-controller';

/**
 * 移动端日历视图接口
 *
 * @export
 * @class IMobCalendarViewController
 */
export interface IMobCalendarViewController extends IAppDEMultiDataViewController {
  /**
   * 视图实例
   *
   * @protected
   * @type {IPSAppDEMobCalendarView}
   * @memberof IMobCalendarViewController
   */
  viewInstance: IPSAppDEMobCalendarView;
}
