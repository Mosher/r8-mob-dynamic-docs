import { IPSAppDEMobPickupView } from '@ibiz/dynamic-model-api';
import { IAppDEViewController } from './i-app-de-view-controller';

/**
 * 移动端数据选择视图接口
 *
 * @export
 * @class IMobPickUpViewController
 */
export interface IMobPickUpViewController extends IAppDEViewController {
  /**
   * 视图实例
   *
   * @protected
   * @type {IPSAppDEMobPickupView}
   * @memberof IMobPickUpViewController
   */
  viewInstance: IPSAppDEMobPickupView;

  /**
   * 确定
   *
   * @memberof IMobPickUpViewController
   */
  ok(): void;

  /**
   * 取消
   *
   * @memberof IMobPickUpViewController
   */
  cancel(): void;
}
