import { IPSAppDEMobDashboardView } from '@ibiz/dynamic-model-api';
import { IAppDEViewController } from './i-app-de-view-controller';

/**
 * 移动端实体数据看板视图控制器接口
 *
 * @exports
 * @interface IMobDEDashboardViewController
 * @extends IAppDEViewController
 */
export interface IMobDEDashboardViewController extends IAppDEViewController {
  /**
   * 移动端实体数据看板视图实例对象
   *
   * @type {IPSAppDEMobDashboardView；}
   */
  viewInstance: IPSAppDEMobDashboardView;
}
