/**
 * 基本参数定义
 *
 * @interface IParam
 */
export interface IParam {
  [key: string]: any;
}
