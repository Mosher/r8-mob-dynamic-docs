import { IPSApplication } from '@ibiz/dynamic-model-api';
import { IContext, IParam } from '../common';
import { IHttp } from '../utils';
import {
  IUIServiceRegister,
  IAuthServiceRegister,
  IUtilServiceRegister,
  IEntityServiceRegister,
  ICodeListService,
  ICounterService,
  IViewMessageService,
  ILoadingService,
  IMsgboxService,
  IActionService,
  IViewOpenService,
  IAppNoticeService,
  IComponentService,
  IPluginService,
  IAppCenterService,
  IAppFuncService,
  IAppStorageService,
} from '../service';

export interface IApp {
  /**
   * @description 网络请求对象
   * @type {IHttp}
   * @memberof IApp
   */
  readonly http: IHttp;

  /**
   * @description 获取应用激活语言
   * @return {*}  {string}
   * @memberof IApp
   */
  getActiveLanguage(): string;

  /**
   * @description 设置应用激活语言
   * @param {string} lan
   * @memberof IApp
   */
  setActiveLanguage(lan: string): void;

  /**
   * @description 获取当前运行平台类型
   * @return {*}  {('ios' | 'android' | 'dd' | 'wx-mini' | 'wx-pn' | 'desktop' | 'web')}
   * @memberof IApp
   */
  getPlatFormType(): 'ios' | 'android' | 'dd' | 'wx-mini' | 'wx-pn' | 'desktop' | 'web';

  /**
   * @description 设置当前运行平台类型
   * @param {('ios' | 'android' | 'dd' | 'wx-mini' | 'wx-pn' | 'desktop' | 'web')} platFormType
   * @memberof IApp
   */
  setPlatFormType(platFormType: 'ios' | 'android' | 'dd' | 'wx-mini' | 'wx-pn' | 'desktop' | 'web'): void;

  /**
   * @description 获取应用模型数据对象
   * @return {*}  {IPSApplication}
   * @memberof IApp
   */
  getModel(): IPSApplication;

  /**
   * @description 设置应用模型数据对象
   * @param {(IPSApplication | null)} opts
   * @memberof IApp
   */
  setModel(opts: IPSApplication | null): void;
  
  /**
   * @description 获取应用数据数据对象
   * @return {*}  {(IContext | null)}
   * @memberof IApp
   */
  getContext(): IContext | null;

  /**
   * @description 设置应用上下文数据对象
   * @param {(IContext | null)} opts 数据对象
   * @memberof IApp
   */
  setContext(opts: IContext | null): void;

  /**
   * @description 获取应用本地数据对象
   * @return {*}  {(IParam | null)}
   * @memberof IApp
   */
  getLocalData(): IParam | null;
  
  /**
   * @description 设置应用本地数据对象
   * @param {(IParam | null)} opts 数据对象
   * @memberof IApp
   */
  setLocalData(opts: IParam | null): void;
  
  /**
   * @description 获取应用环境数据对象
   * @return {*}  {(IParam | null)}
   * @memberof IApp
   */
  getEnvironment(): IParam | null;

  /**
   * @description 设置应用环境数据对象
   * @param {(IParam | null)} opts 参数对象
   * @memberof IApp
   */
  setEnvironment(opts: IParam | null): void;
  
  /**
   * @description 获取应用权限数据对象
   * @param {string} key 标识
   * @return {*}  {(IParam | null)}
   * @memberof IApp
   */
  getAuthResData(key: string): IParam | null;

  /**
   * @description 获取应用权限数据对象
   * @param {(IParam | null)} opts
   * @memberof IApp
   */
  setAuthResData(opts: IParam | null): void;

  /**
   * @description 获取应用权限数据对象
   * @return {*}  {boolean}
   * @memberof IApp
   */
  getEnablePermissionValid(): boolean;

  /**
   * @description 获取应用权限数据对象
   * @param {boolean} enablepermissionvalid 允许权限校验
   * @memberof IApp
   */
  setEnablePermissionValid(enablepermissionvalid: boolean): void;

  /**
   * @description 获取应用存储对象
   * @return {*}  {IAppStorageService}
   * @memberof IApp
   */
  getStore(): IAppStorageService;

  /**
   * @description 设置应用存储对象
   * @param {IAppStorageService} opts
   * @memberof IApp
   */
  setStore(opts: IAppStorageService): void;

  /**
   * @description 初始化应用
   * @param {IParam} opts
   * @memberof IApp
   */
  onInit(opts: IParam): void;

  /**
   * @description 销毁应用
   * @memberof IApp
   */
  onDestroy(): void;

  /**
   * @description 系统登录
   * @param {{ loginname: string; password: string }} data
   * @return {*}  {Promise<IParam>}
   * @memberof IApp
   */
  onLogin(data: { loginname: string; password: string }): Promise<IParam>;

  /**
   * @description 系统登出
   * @return {*}  {Promise<any>}
   * @memberof IApp
   */
  onLogout(): Promise<any>;

  /**
   * @description 清除应用数据
   * @memberof IApp
   */
  onClearAppData(): void;

  /**
   * @description 检查系统更新
   * @memberof IApp
   */
  onCheckAppUpdate(): void;

  /**
   * @description 跳转预置视图
   * @param {('login' | '404' | '500')} pageTag
   * @memberof IApp
   */
  goPresetView(pageTag: 'login' | '404' | '500'): void;

  /**
   * @description 获取用户信息
   * @return {*}  {IParam}
   * @memberof IApp
   */
  getUserInfo(): IParam;

  /**
   * @description 关于系统
   * @return {*}  {IParam}
   * @memberof IApp
   */
  getSystemInfo(): IParam;

  /**
   * @description 获取UI服务
   * @return {*}  {IUIServiceRegister}
   * @memberof IApp
   */
  getUIService(): IUIServiceRegister;

  /**
   * @description 获取权限服务
   * @return {*}  {IAuthServiceRegister}
   * @memberof IApp
   */
  getAuthService(): IAuthServiceRegister;

  /**
   * @description 获取Util服务
   * @return {*}  {IUtilServiceRegister}
   * @memberof IApp
   */
  getUtilService(): IUtilServiceRegister;

  /**
   * @description 获取数据服务
   * @return {*}  {IEntityServiceRegister}
   * @memberof IApp
   */
  getEntityService(): IEntityServiceRegister;

  /**
   * @description 获取代码表服务
   * @return {*}  {ICodeListService}
   * @memberof IApp
   */
  getCodeListService(): ICodeListService;

  /**
   * @description 获取计数器服务
   * @return {*}  {ICounterService}
   * @memberof IApp
   */
  getCounterService(): ICounterService;

  /**
   * @description 获取视图消息服务
   * @return {*}  {IViewMessageService}
   * @memberof IApp
   */
  getViewMSGService(): IViewMessageService;

  /**
   * @description 获取加载服务
   * @return {*}  {ILoadingService}
   * @memberof IApp
   */
  getLoadingService(): ILoadingService;

  /**
   * @description 获取消息弹框服务
   * @return {*}  {IMsgboxService}
   * @memberof IApp
   */
  getMsgboxService(): IMsgboxService;

  /**
   * @description 获取实例化界面行为服务
   * @return {*}  {IActionService}
   * @memberof IApp
   */
  getActionService(): IActionService;

  /**
   * @description 获取视图打开服务
   * @return {*}  {IViewOpenService}
   * @memberof IApp
   */
  getOpenViewService(): IViewOpenService;

  /**
   * @description 获取消息提示服务
   * @return {*}  {IAppNoticeService}
   * @memberof IApp
   */
  getNoticeService(): IAppNoticeService;

  /**
   * @description 获取组件服务
   * @return {*}  {IComponentService}
   * @memberof IApp
   */
  getComponentService(): IComponentService;

  /**
   * @description 获取插件服务
   * @return {*}  {IPluginService}
   * @memberof IApp
   */
  getPluginService(): IPluginService;

  /**
   * @description 获取应用中心服务
   * @return {*}  {IAppCenterService}
   * @memberof IApp
   */
  getCenterService(): IAppCenterService;

  /**
   * @description 获取应用功能服务
   * @return {*}  {IAppFuncService}
   * @memberof IApp
   */
  getFuncService(): IAppFuncService;

  /**
   * @description 获取应用级注册
   * @return {*}  {IParam}
   * @memberof IApp
   */
  getUserRegister(): IParam;

  /**
   * @description 获取语言资源i18n
   * @return {*}  {IParam}
   * @memberof IApp
   */
  getI18n(): IParam;

  isPreviewMode(): boolean;
}
