import { SyncSeriesHook } from 'qx-util';
import { IParam } from '../common';

/**
 * 编辑器hooks
 *
 * @export
 * @interface IAppEditorHooks
 */
export interface IAppEditorHooks {
  /**
   * 模型数据加载完成钩子
   *
   * @class IAppEditorHooks
   */
  modelLoaded: SyncSeriesHook<[], { arg: IParam }>;

  /**
   * 编辑器值变更事件
   *
   * @class IAppEditorHooks
   */
  valueChange: SyncSeriesHook<[], { arg: IParam }>;
}
