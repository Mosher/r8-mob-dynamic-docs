import { SyncSeriesHook } from 'qx-util';
import { ICtrlEventParam } from '../params';
import { IParam } from '../common';

/**
 * 部件hooks基类接口
 *
 * @export
 * @interface IAppCtrlHooks
 */
export interface IAppCtrlHooks {
  /**
   * 模型数据加载完成钩子
   *
   * @class IAppCtrlHooks
   */
  modelLoaded: SyncSeriesHook<[], { arg: IParam }>;

  /**
   * 绑定事件钩子
   *
   * @interface IAppCtrlHooks
   */
  onEvent: SyncSeriesHook<[], { event: string | string[]; fn: Function }>;

  /**
   * 解绑事件钩子
   *
   * @interface IAppCtrlHooks
   */
  offEvent: SyncSeriesHook<[], { event: string | string[]; fn?: Function }>;

  /**
   * 部件事件钩子
   *
   * @class IAppCtrlHooks
   */
  event: SyncSeriesHook<[], ICtrlEventParam>;

  /**
   * 关闭视图钩子
   *
   * @class IAppCtrlHooks
   */
  closeView: SyncSeriesHook<[], IParam>;

  /**
   * 挂载之后钩子
   *
   * @class IAppCtrlHooks
   */  
  mounted: SyncSeriesHook<[], { arg: IParam }>;
}
