import { SyncSeriesHook } from 'qx-util';
import { IParam } from '../common';

/**
 * 应用hooks接口
 *
 * @export
 * @interface IAppHooks
 */
export interface IAppHooks {
  /**
   * 模型数据加载完成钩子
   *
   * @class IAppHooks
   */
  modelLoaded: SyncSeriesHook<[], { arg: IParam }>;

  /**
   * 绑定事件钩子
   *
   * @interface IAppHooks
   */
  onEvent: SyncSeriesHook<[], { event: string | string[]; fn: Function }>;

  /**
   * 解绑事件钩子
   *
   * @interface IAppHooks
   */
  offEvent: SyncSeriesHook<[], { event: string | string[]; fn?: Function }>;

  /**
   * 请求之前
   *
   * @interface IAppHooks
   */
  beforeRequest: SyncSeriesHook<[], { arg: IParam }>;

  /**
   * 响应之后
   *
   * @interface IAppHooks
   */
  afterResponse: SyncSeriesHook<[], { arg: IParam }>;

  /**
   * 鉴权之前
   *
   * @interface IAppHooks
   */
  beforeAuth: SyncSeriesHook<[], { arg: IParam }>;

  /**
   * 鉴权之后
   *
   * @interface IAppHooks
   */
  afterAuth: SyncSeriesHook<[], { arg: IParam }>;
}
