/**
 * 编辑器事件
 *
 * @export
 * @enum AppEditorEvents
 */
export enum AppEditorEvents {
  /**
   * @description 初始化完成
   */
  INITED = 'onInited',

  /**
   * @description 销毁完成
   */
  DESTROYED = 'onDestroyed',

  /**
   * @description 编辑器值变化
   */
  VALUE_CHANGE = 'onValueChange',
}
