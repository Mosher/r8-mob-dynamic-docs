/**
 * 移动端面板部件事件
 *
 * @export
 * @class MobPanelEvents
 */
export enum MobPanelEvents {
  /**
   * @description 初始化完成
   */
  INITED = 'onInited',

  /**
   * @description 销毁完成
   */
  DESTROYED = 'onDestroyed',

  /**
   * @description 部件挂载完成
   */
  MOUNTED = 'onMounted',

  /**
   * @description 关闭
   */
  CLOSE = 'onClose',

  /**
   * @description 面板数据变化
   */
  PANEL_DATA_CHANGE = 'onPanelDataChange',
}
