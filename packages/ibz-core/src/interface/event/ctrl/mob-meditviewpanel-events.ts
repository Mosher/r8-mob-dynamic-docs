/**
 * 移动端多表单编辑视图面板事件
 *
 * @export
 * @enum MobMEditViewPanelEvents
 */
export enum MobMEditViewPanelEvents {
  /**
   * @description 初始化完成
   */
  INITED = 'onInited',

  /**
   * @description 销毁完成
   */
  DESTROYED = 'onDestroyed',

  /**
   * @description 部件挂载完成
   */
  MOUNTED = 'onMounted',

  /**
   * @description 关闭
   */
  CLOSE = 'onClose',

  /**
   * @description 数据改变
   */
  DATA_CHANGE = 'onDataChange',

  /**
   * @description 数据加载之前
   */
  BEFORE_LOAD = 'onBeforeLoad',

  /**
   * @description 数据加载成功
   */
  LOAD_SUCCESS = 'onLoadSuccess',

  /**
   * @description 数据加载异常
   */
  LOAD_ERROR = 'onLoadError',

  /**
   * @description 数据新增之前
   */
  BEFORE_ADD = 'onBeforeAdd',

  /**
   * @description 数据新增成功
   */
  ADD_SUCCESS = 'onAddSuccess',

  /**
   * @description 数据新增异常
   */
  ADD_ERROR = 'onAddError',

  /**
   * @description 数据保存
   */
  DATA_SAVE = 'onDataSave',
}
