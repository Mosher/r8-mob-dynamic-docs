/**
 * 移动端树部件事件
 *
 * @enum MobTreeEvents
 */
export enum MobTreeEvents {
  /**
   * @description 初始化完成
   */
  INITED = 'onInited',

  /**
   * @description 销毁完成
   */
  DESTROYED = 'onDestroyed',

  /**
   * @description 部件挂载完成
   */
  MOUNTED = 'onMounted',

  /**
   * @description 关闭
   */
  CLOSE = 'onClose',

  /**
   * @description 选中数据改变
   */
  SELECT_CHANGE = 'onSelectionChange',

  /**
   * @description 行选中
   */
  ROW_SELECTED = 'onRowSelected',

  /**
   * @description 数据加载之前
   */
  BEFORE_LOAD = 'onBeforeLoad',

  /**
   * @description 数据加载成功
   */
  LOAD_SUCCESS = 'onLoadSuccess',

  /**
   * @description 数据加载异常
   */
  LOAD_ERROR = 'onLoadError',

  /**
   * @description 保存之前
   */
  BEFORE_SAVE = 'onBeforeSave',

  /**
   * @description 保存成功
   */
  SAVE_SUCCESS = 'onSaveSuccess',

  /**
   * @description 保存失败
   */
  SAVE_ERROR = 'onSaveError',

  /**
   * @description 删除之前
   */
  BEFORE_REMOVE = 'onBeforeRemove',

  /**
   * @description 删除成功
   */
  REMOVE_SUCCESS = 'onRemoveSuccess',

  /**
   * @description 删除失败
   */
  REMOVE_ERROR = 'onRemoveError',
}
