/**
 * 移动端门户部件事件
 *
 * @enum MobPortletEvents
 */
export enum MobPortletEvents {
  /**
   * @description 初始化完成
   */
  INITED = 'onInited',

  /**
   * @description 销毁完成
   */
  DESTROYED = 'onDestroyed',

  /**
   * @description 部件挂载完成
   */
  MOUNTED = 'onMounted',

  /**
   * @description 关闭
   */
  CLOSE = 'onClose',

  /**
   * @description 刷新
   */
  REFRESH = 'refresh',

  /**
   * @description 刷新全部
   */
  REFRESH_ALL = 'refreshAll',

  /**
   * 加载模型
   */
  LOAD_MODEL = 'loadModel',

  /**
   * 加载
   */
  LOAD = 'load',
}
