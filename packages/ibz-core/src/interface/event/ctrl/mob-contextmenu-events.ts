/**
 * 移动端上下文菜单事件
 *
 * @export
 * @enum MobContextMenuEvents
 */
export enum MobContextMenuEvents {
  /**
   * @description 菜单项点击
   */
  MENUITEM_CLICK = 'onMenuItemClick',
}
