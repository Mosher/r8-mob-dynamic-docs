/**
 * 移动端数据看板部件事件
 *
 * @enum MobDashboardEvents
 */
export enum MobDashboardEvents {
  /**
   * @description 部件挂载完成
   */
  MOUNTED = 'onMounted',

  /**
   * @description 初始化完成
   */
  INITED = 'onInited',

  /**
   * @description 销毁完成
   */
  DESTROYED = 'onDestroyed',

  /**
   * @description 关闭
   */
  CLOSE = 'onClose',

  /**
   * @description 加载
   */
  LOAD = 'load',

  /**
   * 加载模型
   */
  LOAD_MODEL = 'loadModel',

  /**
   * @description 刷新
   */
  REFRESH = 'refresh',

  /**
   * @description 刷新全部
   */
  REFRESH_ALL = 'refreshAll',
}
