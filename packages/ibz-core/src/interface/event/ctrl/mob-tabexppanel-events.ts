/**
 * 移动端图表事件
 *
 * @export
 * @enum AppMobChartEvents
 */
export enum MobTabExpPanelEvents {
  /**
   * @description 初始化完成
   */
  INITED = 'onInited',

  /**
   * @description 销毁完成
   */
  DESTROYED = 'onDestroyed',

  /**
   * @description 部件挂载完成
   */
  MOUNTED = 'onMounted',

  /**
   * @description 关闭
   */
  CLOSE = 'onClose',
}
