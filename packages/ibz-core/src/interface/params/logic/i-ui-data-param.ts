import { IParam } from '../../common';

/**
 * UI界面数据接口
 *
 */
export interface IUIDataParam {
  /**
   * 行为
   *
   * @memberof IUIDataParam
   */
  action?: string;

  /**
   * 当前数据
   *
   * @memberof IUIDataParam
   */
  data: Array<IParam> | null;

  /**
   * 导航上下文
   *
   * @memberof IUIDataParam
   */
  navContext: IParam;

  /**
   * 导航视图参数
   *
   * @memberof IUIDataParam
   */
  navParam: IParam;

  /**
   * 导航数据
   *
   * @memberof IUIDataParam
   */
  navData: Array<IParam>;

  /**
   * 触发源对象(视图对象、部件对象等)
   *
   * @memberof IUIDataParam
   */
  sender: IParam;

  /**
   * 事件源对象
   *
   * @memberof IUIDataParam
   */
  event?: IParam;

  /**
   * 附加参数对象
   *
   * @memberof IUIDataParam
   */
  args?: IParam;
}
