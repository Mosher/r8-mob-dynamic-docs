import { IParam } from '../../common';

/**
 * UI界面环境参数接口
 *
 */
export interface IUIEnvironmentParam {
  /**
   * 当前环境应用对象
   *
   * @memberof IUIEnvironmentParam
   */
  // IUIApp | null
  app: any;

  /**
   * 当前环境视图对象
   *
   * @memberof IUIEnvironmentParam
   */
  view: IParam | null;

  /**
   * 当前环境部件对象
   *
   * @memberof IUIEnvironmentParam
   */
  ctrl: IParam | null;

  /**
   * 父实体代码标识
   *
   * @memberof IUIEnvironmentParam
   */
  parentDeCodeName: string | null;
}
