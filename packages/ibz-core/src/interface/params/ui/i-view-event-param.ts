/**
 * 视图事件参数
 *
 * @interface IViewEventResult
 */
export interface IViewEventResult {
  /**
   * 行为名称
   *
   * @type {string}
   * @memberof IViewEventResult
   */
  action: string;

  /**
   * 视图名称
   *
   * @type string
   * @memberof IViewEventResult
   */
  viewName: string;

  /**
   * 传输数据
   *
   * @type IParam[] | null
   * @memberof IViewEventResult
   */
  data: any;
}
