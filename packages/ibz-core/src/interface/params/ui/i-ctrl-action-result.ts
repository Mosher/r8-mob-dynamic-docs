import { IParam } from '../../common';

/**
 * 部件内置行为响应结果
 *
 * @interface ICtrlActionResult
 */
export interface ICtrlActionResult {
  /**
   * 是否成功
   *
   * @type boolean
   * @memberof ICtrlActionResult
   */
  ret: boolean;

  /**
   * 传输数据
   *
   * @type IParam[] | null
   * @memberof ICtrlActionResult
   */
  data: IParam[] | null;
}
