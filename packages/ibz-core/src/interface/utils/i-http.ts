import { IParam } from '../common';

/**
 * 网络请求对象接口
 *
 * @interface IHttp
 */
export interface IHttp {
  /**
   * post请求
   *
   * @param {string} url 请求路径
   * @param {IParam} [params] 查询参数
   * @param {boolean} [isloading] loading状态
   * @returns {Promise<IParam>}
   *
   * @memberof interface
   */
  post(url: string, params: IParam, isloading?: boolean): Promise<any>;

  /**
   * 获取
   *
   * @param {string} url 请求路径
   * @param {IParam} [params] 查询参数
   * @param {boolean} [isloading] loading状态
   * @returns {Promise<IParam>}
   *
   * @memberof Http
   */
  get(url: string, params?: IParam, isloading?: boolean): Promise<any>;

  /**
   * 删除
   *
   * @param {string} url 请求路径
   * @param {IParam} [params] 查询参数
   * @param {boolean} [isloading] loading状态
   * @returns {Promise<IParam>}
   *
   * @memberof Http
   */
  delete(url: string, params?: IParam, isloading?: boolean): Promise<any>;

  /**
   * 修改数据
   *
   * @param {string} url 请求路径
   * @param {IParam} [params] 查询参数
   * @param {boolean} [isloading] loading状态
   * @returns {Promise<IParam>}
   *
   * @memberof Http
   */
  put(url: string, params: IParam, isloading?: boolean): Promise<any>;
}
