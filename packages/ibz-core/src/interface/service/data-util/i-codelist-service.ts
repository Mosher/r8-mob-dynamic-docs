/**
 * 代码表服务接口
 *
 * @interface ICodeListService
 */
export interface ICodeListService {
  /**
   * 获取代码表数据
   *
   * @param params { tag：代码表代码名称， type：代码表类型， data：代码表CodeItems， navContext：应用上下文， navViewParam：视图导航参数 }
   */
  getDataItems(params: {
    tag: string;
    type: string;
    data?: Array<any>;
    navContext?: any;
    navViewParam?: any;
  }): Promise<any[]>;
}
