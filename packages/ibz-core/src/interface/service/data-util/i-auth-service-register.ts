import { IContext } from '../../common';
import { IAuthService } from './i-auth-service';

/**
 * 实体权限服务注册中心
 *
 * @export
 * @interface IAuthServiceRegister
 */
export interface IAuthServiceRegister {
  /**
   * @description 获取指定AuthService
   * @param {IContext} context 应用上下文
   * @param {string} entityKey 实体代码标识
   * @return {*}  {Promise<IAuthService>}
   * @memberof IAuthServiceRegister
   */
  getService(context: IContext, entityKey: string): Promise<IAuthService>;
}
