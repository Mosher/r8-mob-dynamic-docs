import { IContext, IParam } from '../../common';
import { IUIDataParam, IUIEnvironmentParam, IUIUtilParam } from '../../params';

export interface IUIService {
  /**
   * 加载模型
   *
   * @memberof IUIService
   */
  loaded(): Promise<void>;

  /**
   * @description 是否支持实体主状态
   * @return {*}  {boolean}
   * @memberof IUIService
   */
  enableDEMainState(): boolean;

  /**
   * 执行界面行为统一入口
   *
   * @param {string} uIActionTag 界面行为tag
   * @param {*} [UIDataParam] 操作数据参数
   * @param {*} [UIEnvironmentParam]  操作环境参数
   * @param {*} [UIUtilParam]  操作工具参数
   * @param {*} contextJO 行为附加上下文
   * @param {*}  paramJO 附加参数
   *
   * @memberof IUIService
   */
  excuteAction(
    uIActionTag: string,
    UIDataParam: IUIDataParam,
    UIEnvironmentParam: IUIEnvironmentParam,
    UIUtilParam: IUIUtilParam,
    contextJO: any,
    paramJO: any,
  ): Promise<IParam | undefined>;

  /**
   * @description 获取数据对象所有的操作标识
   * @param {IParam} data 当前数据
   * @param {string} dataaccaction 数据操作标识
   * @memberof IUIService
   */
  getAllOPPrivs(data: IParam, dataaccaction: string): 1 | 0;

  /**
   * @description 获取资源标识是否有权限(无数据目标)
   * @param {string} tag 资源标识
   * @return {*}  {boolean}
   * @memberof IUIService
   */
  getResourceOPPrivs(tag: string): boolean;

  /**
   * @description 获取指定数据的重定向页面
   * @param {IContext} context 应用上下文
   * @param {string} srfkey 数据主键
   * @param {IParam} enableWorkflowParam 重定向视图需要处理流程中的数据
   * @param {IParam} [dataSetParams] 数据集参数
   * @return {*}  {Promise<IParam>}
   * @memberof IUIService
   */
  getRDAppView(context: IContext, srfkey: string, enableWorkflowParam: IParam, dataSetParams?: IParam): Promise<IParam>;
}
