import { IParam } from '../../common';

/**
 * 实体权限服务接口
 *
 * @export
 * @interface IAuthService
 */
export interface IAuthService {
  /**
   * @description 系统操作标识映射统一资源Map
   * @return {Map<string, any>}
   * @memberof IAuthService
   */
  getSysOPPrivsMap(): Map<string, any>;

  /**
   * @description 加载应用实体模型数据
   * @return {*}  {Promise<void>}
   * @memberof IAuthService
   */
  loaded(): Promise<void>;

  /**
   * @description 根据当前数据获取实体操作标识
   * @param {string} activeKey 实体权限数据缓存标识
   * @param {string} dataaccaction 操作标识
   * @param {IParam} mainSateOPPrivs 传入数据主状态操作标识集合
   * @return {*}  {IParam}
   * @memberof IAuthService
   */
  getOPPrivs(activeKey: string, dataaccaction: string, mainSateOPPrivs: IParam): IParam;

  /**
   * @description 根据统一资源标识获取统一资源权限
   * @param {string} tag 统一资源标识
   * @return {*}  {boolean}
   * @memberof IAuthService
   */
  getResourcePermission(tag: string): boolean;
}
