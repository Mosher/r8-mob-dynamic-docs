import { ICtrlActionResult } from '../../params';

/**
 * 移动端动作面板服务接口
 *
 * @interface IAppActionSheetService
 * @description 底部弹出的模态面板
 */
export interface IAppActionSheetService {
  /**
   * @description 创建动态面板
   * @param {IAppActionSheetOptions} options 动态面板配置
   * @return {*}  {Promise<ICtrlActionResult>}
   * @memberof IAppActionSheetService
   */
  create(options: IAppActionSheetOptions): Promise<ICtrlActionResult>;
}

/**
 * 移动端动作面板按钮接口
 *
 * @interface IAppActionSheetButton
 */
export interface IAppActionSheetButton {
  /**
   * @description 显示内容
   * @type {string}
   * @memberof IAppActionSheetButton
   */
  text: string;

  /**
   * @description 默认权限
   * @type {('cancel' | string)}
   * @memberof IAppActionSheetButton
   */
  role?: 'cancel' | string;

  /**
   * @description 图标
   * @type {string}
   * @memberof IAppActionSheetButton
   */
  icon?: string;

  /**
   * @description 绑定样式
   * @type {string}
   * @memberof IAppActionSheetButton
   */
  cssClass?: string;

  /**
   * @description 按钮点击方法
   * @type {Function}
   * @memberof IAppActionSheetButton
   */
  handler: Function;
}

/**
 * @description 移动端动作面板配置接口
 * @export
 * @interface IAppActionSheetOptions
 */
export interface IAppActionSheetOptions {
  /**
   * @description id
   * @type {string}
   * @memberof IAppActionSheetOptions
   */
  id?: string;

  /**
   * @description 头部标题
   * @type {string}
   * @memberof IAppActionSheetOptions
   */
  header?: string;

  /**
   * @description 头部副标题
   * @type {string}
   * @memberof IAppActionSheetOptions
   */
  subHeader?: string;

  /**
   * @description 样式表
   * @type {(string | string[])}
   * @memberof IAppActionSheetOptions
   */
  cssClass?: string | string[];

  /**
   * @description 按钮集合
   * @type {IAppActionSheetButton[]}
   * @memberof IAppActionSheetOptions
   */
  buttons: IAppActionSheetButton[];

  /**
   * @description 点击背景是否可关闭
   * @type {boolean}
   * @memberof IAppActionSheetOptions
   */
  backdropDismiss?: boolean;

  /**
   * @description 是否透明
   * @type {boolean}
   * @memberof IAppActionSheetOptions
   */
  translucent?: boolean;
}
