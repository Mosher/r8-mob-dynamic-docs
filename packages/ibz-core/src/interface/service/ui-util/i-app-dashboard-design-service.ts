import { IContext, IParam } from '../../../interface';

/**
 * @description 应用数据看板设计服务接口
 * @export
 * @interface IAppDashboardDesignService
 */
export interface IAppDashboardDesignService {
  /**
   * @description 保存模型数据
   * @param {string} serviceKey 工具服务标识
   * @param {IContext} context 应用上下文
   * @param {IParam} viewParam 保存视图参数
   * @return {*}  {Promise<IParam>}
   * @memberof IAppDashboardDesignService
   */
  saveModelData(serviceKey: string, context: IContext, viewParam: IParam): Promise<IParam>;

  /**
   * @description 加载门户部件集合
   * @param {IContext} context 应用上下文
   * @param {IParam} viewParam 视图参数
   * @return {*}  {Promise<IParam>}
   * @memberof IAppDashboardDesignService
   */
  loadPortletList(context: IContext, viewParam: IParam): Promise<IParam>;
}
