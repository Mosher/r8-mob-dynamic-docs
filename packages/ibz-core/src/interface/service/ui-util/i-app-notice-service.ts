/**
 * 消息提示服务接口
 *
 * @export
 * @interface IAppNoticeService
 */
export interface IAppNoticeService {
  /**
   * 提示信息
   *
   * @param message 提示信息
   * @param time 显示时间
   */
  info(message: string, time?: number): void;

  /**
   * 成功提示信息
   *
   * @param message 提示信息
   * @param time 显示时间
   */
  success(message: string, time?: number): void;

  /**
   * 警告提示信息
   *
   * @param message 提示信息
   * @param time 显示时间
   */
  warning(message: string, time?: number): void;

  /**
   * 错误提示信息
   *
   * @param message 提示信息
   * @param time 显示时间
   */
  error(message: string, time?: number): void;
}
