import { IParam } from '../../common';

/**
 * @description 应用主题服务
 * @export
 * @interface IAppThemeService
 */
export interface IAppThemeService {
  /**
   * @description 初始化应用主题
   * @return {*}  {{ themeOptions: IParam[], activeTheme: { isCustom: boolean, theme: string } }}
   * @memberof IAppThemeService
   */
  initAppTheme(): { themeOptions: IParam[]; activeTheme: { isCustom: boolean; theme: string } };

  /**
   * @description 获取预置主题
   * @return {*}  {IParam[]}
   * @memberof IAppThemeService
   */
  getPresetTheme(): IParam[];

  /**
   * @description 获取主题配置
   * @return {*}  {IParam[]}
   * @memberof IAppThemeService
   */
  getThemeOptions(): IParam[];

  /**
   * @description 应用自定义主题
   * @param {string} theme 主题基类
   * @param {IParam[]} options 自定义配置
   * @memberof IAppThemeService
   */
  applyCustomTheme(theme: string, options: IParam[]): void;

  /**
   * @description 切换内置主题
   * @param {string} oldVal 旧主题标识
   * @param {string} newVal 新主题标识
   * @param {IParam[]} options 主题配置
   * @memberof IAppThemeService
   */
  changeInternalTheme(oldVal: string, newVal: string, options: IParam[]): void;

  /**
   * @description 重置应用主题
   * @param {*} options 主题配置
   * @memberof IAppThemeService
   */
  resetAppTheme(options: any): void;
}
