import { Subject } from 'rxjs';
import { IContext, IParam } from '../../common';

/**
 * 打开视图接口
 *
 * @export
 * @interface IViewOpenService
 */
export interface IViewOpenService {
  /**
   * @description 路由打开
   * @param {string} path
   * @memberof IViewOpenService
   */
  openView(path: string): void;

  /**
   * @description 浏览器新标签页打开
   * @param {string} url
   * @memberof IViewOpenService
   */
  openPopupApp(url: string): void;

  /**
   * @description 打开模态
   * @param {({ viewComponent?: any | undefined; viewModel?: IParam; customClass?: string | undefined; customStyle?: IParam })} view
   * @param {IContext} [context]
   * @param {IParam} [navParam]
   * @param {*} [navDatas]
   * @param {IParam} [otherParam]
   * @return {*}  {Subject<{ ret: boolean; datas?: IParam[] }>}
   * @memberof IViewOpenService
   */
  openModal(
    view: {
      viewComponent?: any | undefined;
      viewModel?: IParam;
      customClass?: string | undefined;
      customStyle?: IParam;
    },
    context?: IContext,
    navParam?: IParam,
    navDatas?: any,
    otherParam?: IParam,
  ): Subject<{ ret: boolean; datas?: IParam[] }>;

  /**
   * @description 打开抽屉
   * @param {({
   *       viewComponent?: any;
   *       viewModel?: IParam;
   *       customClass?: string;
   *       customStyle?: IParam;
   *       placement?: 'DRAWER_LEFT' | 'DRAWER_RIGHT';
   *     })} view
   * @param {IContext} [navContext]
   * @param {IParam} [navParam]
   * @param {*} [navDatas]
   * @param {IParam} [otherParam]
   * @return {*}  {Subject<{ ret: boolean; datas?: IParam[] }>}
   * @memberof IViewOpenService
   */
  openDrawer(
    view: {
      viewComponent?: any;
      viewModel?: IParam;
      customClass?: string;
      customStyle?: IParam;
      placement?: 'DRAWER_LEFT' | 'DRAWER_RIGHT';
    },
    navContext?: IContext,
    navParam?: IParam,
    navDatas?: any,
    otherParam?: IParam,
  ): Subject<{ ret: boolean; datas?: IParam[] }>;
}
