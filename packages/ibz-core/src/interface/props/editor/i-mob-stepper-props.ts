import { IEditorProps } from './i-editor-props';

/**
 * 步进器输入参数接口
 *
 * @export
 * @interface IMobStepperProps
 */
export type IMobStepperProps = IEditorProps;
