import { IEditorProps } from './i-editor-props';

/**
 * 时间选择器输入参数接口
 *
 * @export
 * @interface IMobDatePickerProps
 */
export type IMobDatePickerProps = IEditorProps;
