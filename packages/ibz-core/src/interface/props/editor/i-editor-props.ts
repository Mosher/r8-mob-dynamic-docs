import { Subject } from 'rxjs';
import { IParam } from '../../common';

/**
 * 应用编辑器输入参数接口
 *
 * @export
 * @interface IAppEditorProps
 */
export interface IEditorProps {
  /**
   * 编辑器模型数据
   *
   * @interface IAppEditorProps
   */
  editorInstance: IParam;

  /**
   * 导航上下文
   *
   * @interface IAppEditorProps
   */
  navContext: IParam;

  /**
   * 导航视图参数
   *
   * @interface IAppEditorProps
   */
  navParam: IParam;

  /**
   * 当前环境数据
   *
   * @interface IAppEditorProps
   */
  contextData: IParam;

  /**
   * 通知对象
   *
   * @interface IAppEditorProps
   */
  contextState: Subject<any>;

  /**
   * 绑定值
   *
   * @interface IAppEditorProps
   */
  value: any;

  /**
   * 父项对象
   *
   * @interface IAppEditorProps
   */
  parentItem: IParam;

  /**
   * 是否禁用
   *
   * @interface IAppEditorProps
   */
  disabled: boolean;
}
