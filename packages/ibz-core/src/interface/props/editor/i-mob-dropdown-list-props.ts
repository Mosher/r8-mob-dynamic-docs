import { IEditorProps } from './i-editor-props';

/**
 * 下拉列表编辑器输入参数接口
 *
 * @export
 * @interface IMobDropdownListProps
 */
export type IMobDropdownListProps = IEditorProps;
