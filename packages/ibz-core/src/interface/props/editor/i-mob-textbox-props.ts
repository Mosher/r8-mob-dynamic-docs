import { IEditorProps } from './i-editor-props';

/**
 * 输入框输入参数接口
 *
 * @export
 * @interface IMobTextboxProps
 */
export type IMobTextboxProps = IEditorProps;
