import { IAppMDCtrlProps } from './i-app-md-ctrl-props';

/**
 * 移动端树部件输入参数接口
 *
 * @exports
 * @interface IAppMobTreeProps
 * @extends IAppMDCtrlProps
 */
export type IAppMobTreeProps = IAppMDCtrlProps;
