import { IAppCtrlProps } from './i-app-ctrl-props';

/**
 * 面板部件输入参数接口
 *
 * @export
 * @interface IAppMobPanelProps
 */
export type IAppMobPanelProps = IAppCtrlProps;
