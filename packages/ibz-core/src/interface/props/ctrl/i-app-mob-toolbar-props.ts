import { IAppCtrlProps } from './i-app-ctrl-props';

/**
 * 试图工具栏输入参数接口
 *
 * @export
 * @interface IAppMobToolbarProps
 */
export type IAppMobToolbarProps = IAppCtrlProps;
