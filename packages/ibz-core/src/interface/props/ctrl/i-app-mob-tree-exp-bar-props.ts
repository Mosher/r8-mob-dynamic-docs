import { IAppExpBarCtrlProps } from './i-app-exp-bar-ctrl-props';

/**
 * 移动端树导航部件输入属性接口
 *
 * @export
 * @interface IAppMobTreeExpBarProps
 */
 export interface IAppMobTreeExpBarProps extends IAppExpBarCtrlProps {}