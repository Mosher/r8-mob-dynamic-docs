import { IAppCtrlProps } from './i-app-ctrl-props';
import { IAppExpBarCtrlProps } from './i-app-exp-bar-ctrl-props';

/**
 * 移动端列表导航部件输入属性接口
 *
 * @export
 * @interface IAppMobListExpBarProps
 */
 export interface IAppMobListExpBarProps extends IAppExpBarCtrlProps {}