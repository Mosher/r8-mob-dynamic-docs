import { IAppCtrlProps } from './i-app-ctrl-props';

/**
 * @description 移动端导航栏部件输入参数
 * @export
 * @interface IAppExpBarCtrlProps
 * @extends {IAppCtrlProps}
 */
export interface IAppExpBarCtrlProps extends IAppCtrlProps {}