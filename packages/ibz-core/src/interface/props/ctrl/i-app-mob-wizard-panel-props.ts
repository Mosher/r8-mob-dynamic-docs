import { IAppCtrlProps } from './i-app-ctrl-props';

/**
 * 移动端向导面板部件输入属性接口
 *
 * @export
 * @interface IAppMobWizardPanelProps
 */
 export interface IAppMobWizardPanelProps extends IAppCtrlProps {}