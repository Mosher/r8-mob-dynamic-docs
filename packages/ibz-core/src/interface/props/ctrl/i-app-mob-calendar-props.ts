import { IAppMDCtrlProps } from './i-app-md-ctrl-props';

/**
 * 移动端日历部件传入参数接口
 *
 * @interface IAppMobCalendarCtrlProps
 * @extends IAppMDCtrlProps
 */
export type IAppMobCalendarCtrlProps = IAppMDCtrlProps;
