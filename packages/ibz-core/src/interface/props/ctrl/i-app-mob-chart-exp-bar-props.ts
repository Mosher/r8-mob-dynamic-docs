import { IAppExpBarCtrlProps } from './i-app-exp-bar-ctrl-props';

/**
 * 移动端图表导航栏输入属性接口
 *
 * @export
 * @interface IAppMobChartExpBarProps
 */
 export interface IAppMobChartExpBarProps extends IAppExpBarCtrlProps {}