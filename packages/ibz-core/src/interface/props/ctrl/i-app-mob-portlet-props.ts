import { IAppCtrlProps } from './i-app-ctrl-props';

/**
 * 移动端门户部件输入参数接口
 *
 * @interface IAppMobPortletProps
 * @extends IAppCtrlProps
 */
export type IAppMobPortletProps = IAppCtrlProps;
