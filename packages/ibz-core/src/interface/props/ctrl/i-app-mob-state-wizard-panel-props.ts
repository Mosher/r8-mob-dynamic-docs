import { IAppCtrlProps } from './i-app-ctrl-props';

/**
 * 移动端状态向导面板输入属性接口
 *
 * @export
 * @interface IAppMobStateWizardPanelProps
 */
 export interface IAppMobStateWizardPanelProps extends IAppCtrlProps {}