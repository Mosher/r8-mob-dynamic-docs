import { IAppMDCtrlProps } from './i-app-md-ctrl-props';

/**
 * 移动端多表单编辑视图面板部件传入参数接口
 *
 * @interface IAppMobMEditViewPanelCtrlProps
 * @extends IAppMDCtrlProps
 */
export type IAppMobMEditViewPanelCtrlProps = IAppMDCtrlProps;
