import { IAppCtrlProps } from './i-app-ctrl-props';

/**
 * 多数据部件基类传入参数接口
 *
 * @interface IAppMDCtrlProps
 * @extends IAppCtrlProps
 */
export interface IAppMDCtrlProps extends IAppCtrlProps {
  /**
   * 是否为多选
   * @interface IAppMDCtrlProps
   */
  isMultiple: boolean;
}
