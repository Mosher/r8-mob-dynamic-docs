import { IAppCtrlProps } from './i-app-ctrl-props';

/**
 * 移动端表单输入参数接口
 *
 * @export
 * @interface IAppMobFormProps
 */
export type IAppMobFormProps = IAppCtrlProps;
