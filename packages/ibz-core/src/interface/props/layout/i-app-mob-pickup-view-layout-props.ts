import { IAppLayoutProps } from './i-app-layout-props';

/**
 * 移动端数据选择视图视图布局面板输入参数接口
 *
 * @export
 * @interface IAppMobPickUpViewLayoutProps
 */
export type IAppMobPickUpViewLayoutProps = IAppLayoutProps;
