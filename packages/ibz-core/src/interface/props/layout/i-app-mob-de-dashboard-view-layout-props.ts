import { IAppLayoutProps } from './i-app-layout-props';

/**
 * 移动端实体数据看板视图视图布局面板输入参数接口
 *
 * @exports
 * @interface IAppMobDEDashboardViewLayoutProps
 * @extends IAppViewProps
 */
export type IAppMobDEDashboardViewLayoutProps = IAppLayoutProps;
