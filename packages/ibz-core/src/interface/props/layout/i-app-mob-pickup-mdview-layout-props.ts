import { IAppMobMDViewLayoutProps } from './i-app-mob-md-view-layout-props';

/**
 * 移动端选择多数据视图视图布局面板传入参数接口
 *
 * @export
 * @interface IAppMobPickUpMDViewLayoutProps
 * @extends IAppLayoutProps
 */
export type IAppMobPickUpMDViewLayoutProps = IAppMobMDViewLayoutProps;
