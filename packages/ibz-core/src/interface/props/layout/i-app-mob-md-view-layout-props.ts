import { IAppLayoutProps } from './i-app-layout-props';

/**
 * 移动端多数据视图视图布局面板传入参数接口
 *
 * @export
 * @interface IAppMobMDViewLayoutProps
 * @extends IAppLayoutProps
 */
export type IAppMobMDViewLayoutProps = IAppLayoutProps;
