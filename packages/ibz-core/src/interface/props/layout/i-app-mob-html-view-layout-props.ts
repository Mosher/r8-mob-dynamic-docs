import { IAppLayoutProps } from './i-app-layout-props';

/**
 * 移动端实体Html视图布局面板输入参数接口
 *
 * @export
 * @interface IAppMobHtmlViewLayoutProps
 */
export type IAppMobHtmlViewLayoutProps = IAppLayoutProps;
