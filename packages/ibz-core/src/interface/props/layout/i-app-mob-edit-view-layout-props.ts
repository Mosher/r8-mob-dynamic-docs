import { IAppLayoutProps } from './i-app-layout-props';

/**
 * 移动端实体编辑视图视图布局面板输入参数接口
 *
 * @export
 * @interface IAppMobEditViewLayoutProps
 */
export type IAppMobEditViewLayoutProps = IAppLayoutProps;
