import { IAppMobTreeViewLayoutProps } from './i-app-mob-tree-view-layout-props';

/**
 * 移动端选择树视图视图布局面板输入参数
 *
 * @interface IAppMobPickupTreeViewLayoutProps
 */
export type IAppMobPickupTreeViewLayoutProps = IAppMobTreeViewLayoutProps;
