import { IParam } from '../../common';

/**
 * 视图布局面板输入参数接口
 *
 * @export
 * @interface IAppLayoutProps
 */
export interface IAppLayoutProps {
  /**
   * 视图实例
   * @interface IAppLayoutProps
   */
  viewInstance: IParam;

  /**
   * 导航上下文
   * @interface IAppLayoutProps
   */
  navContext: IParam;

  /**
   * 导航视图参数
   * @interface IAppLayoutProps
   */
  navParam: IParam;

  /**
   * 导航数据
   * @interface IAppLayoutProps
   */
  navDatas: IParam;
}
