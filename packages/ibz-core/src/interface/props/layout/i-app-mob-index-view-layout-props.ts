import { IAppLayoutProps } from './i-app-layout-props';

/**
 * 移动端首页视图视图布局面板输入参数接口
 *
 * @export
 * @interface IAppIndexViewProps
 */
export type IAppIndexViewLayoutProps = IAppLayoutProps;
