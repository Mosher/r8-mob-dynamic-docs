import { IAppDEViewProps } from './i-app-de-view-props';

/**
 * 移动端分页导航视图视图输入属性接口
 *
 * @export
 * @interface IAppMobTabExpViewProps
 */
export type IAppMobTabExpViewProps = IAppDEViewProps;
