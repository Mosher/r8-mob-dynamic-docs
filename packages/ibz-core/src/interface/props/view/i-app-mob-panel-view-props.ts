import { IAppDEViewProps } from './i-app-de-view-props';

/**
 * 移动端面板视图视图输入属性接口
 *
 * @export
 * @interface IAppMobPanelViewProps
 */
export type IAppMobPanelViewProps = IAppDEViewProps;
