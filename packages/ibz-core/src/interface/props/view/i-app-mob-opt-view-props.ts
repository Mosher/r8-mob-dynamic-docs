import { IAppDEViewProps } from './i-app-de-view-props';

/**
 * 移动端选项操作视图输入属性接口
 *
 * @export
 * @interface IAppMobOptViewProps
 */
 export interface IAppMobOptViewProps extends IAppDEViewProps {}