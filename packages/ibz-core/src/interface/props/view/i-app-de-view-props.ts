import { IAppViewProps } from './i-app-view-props';

/**
 * 主数据视图基类输入属性接口
 *
 * @export
 * @interface IAppDEViewProps
 * @extends IAppViewProps
 */
export type IAppDEViewProps = IAppViewProps;
