import { IParam } from '../../common';

/**
 * 视图输入属性接口
 *
 * @export
 * @interface IAppViewProps
 */
export interface IAppViewProps {
  /**
   * 模型路径
   * @interface IAppViewProps
   */
  viewPath: string;

  /**
   * 视图打开模式
   * @interface IAppViewProps
   */
  viewShowMode: string;

  /**
   * 导航上下文
   * @interface IAppViewProps
   */
  navContext: IParam;

  /**
   * 导航视图参数
   * @interface IAppViewProps
   */
  navParam: IParam;

  /**
   * 导航数据
   * @interface IAppViewProps
   */
  navDatas: IParam;

  /**
   * 是否显示标题
   * @interface IAppViewProps
   */
  isShowCaptionBar: boolean;

  /**
   * 视图默认加载
   * @interface IAppViewProps
   */
  isLoadDefault: boolean;
}
