import { IAppDEViewProps } from './i-app-de-view-props';

/**
 * 移动端图表导航视图输入属性接口
 *
 * @export
 * @interface IAppMobChartExpViewProps
 */
 export interface IAppMobChartExpViewProps extends IAppDEViewProps {}