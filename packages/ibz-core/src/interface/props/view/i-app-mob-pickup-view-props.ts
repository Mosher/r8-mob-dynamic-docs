import { IAppDEViewProps } from './i-app-de-view-props';

/**
 * 移动数据选择视图视图输入属性接口
 *
 * @export
 * @interface IAppMobPickUpViewProps
 */
export type IAppMobPickUpViewProps = IAppDEViewProps;
