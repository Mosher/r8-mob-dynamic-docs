import { IAppDEViewProps } from './i-app-de-view-props';

/**
 * 移动端列表导航视图输入属性接口
 *
 * @export
 * @interface IAppMobListExpViewProps
 */
 export interface IAppMobListExpViewProps extends IAppDEViewProps {}