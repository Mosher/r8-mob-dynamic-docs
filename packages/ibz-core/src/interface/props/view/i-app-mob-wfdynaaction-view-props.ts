import { IAppDEViewProps } from './i-app-de-view-props';

/**
 * 移动端动态工作流操作视图输入属性接口
 *
 * @export
 * @interface IAppMobWFDynaActionViewProps
 */
 export interface IAppMobWFDynaActionViewProps extends IAppDEViewProps {}