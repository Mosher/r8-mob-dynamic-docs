import { IAppDEExpViewProps } from './i-app-de-exp-view-props';

/**
 * 移动端树导航视图输入属性接口
 *
 * @export
 * @interface IAppMobTreeExpViewProps
 */
 export interface IAppMobTreeExpViewProps extends IAppDEExpViewProps {}