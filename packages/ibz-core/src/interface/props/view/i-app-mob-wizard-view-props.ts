import { IAppDEViewProps } from './i-app-de-view-props';

/**
 * 移动端向导视图输入属性接口
 *
 * @export
 * @interface IAppMobWizardViewProps
 */
 export interface IAppMobWizardViewProps extends IAppDEViewProps {}