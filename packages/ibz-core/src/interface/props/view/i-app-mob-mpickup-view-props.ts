import { IAppMobPickUpViewProps } from './i-app-mob-pickup-view-props';

/**
 * 移动端多数据选择视图视图输入属性接口
 *
 * @export
 * @interface IAppMobMPickUpViewProps
 */
export type IAppMobMPickUpViewProps = IAppMobPickUpViewProps;
