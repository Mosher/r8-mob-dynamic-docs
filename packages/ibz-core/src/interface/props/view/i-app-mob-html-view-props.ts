import { IAppDEViewProps } from './i-app-de-view-props';

/**
 * 移动端Html视图输入属性接口
 *
 * @export
 * @interface IAppMobHtmlViewProps
 */
export type IAppMobHtmlViewProps = IAppDEViewProps;
