import { IAppDEExpViewProps } from './i-app-de-exp-view-props';

/**
 * 移动端日历导航视图输入属性接口
 *
 * @export
 * @interface IAppMobCalendarExpViewProps
 */
 export interface IAppMobCalendarExpViewProps extends IAppDEExpViewProps {}