import { IAppDEMultiDataViewProps } from './i-app-de-multi-data-view-props';

/**
 * 移动端图表视图视图输入属性接口
 *
 * @export
 * @interface IAppMobChartViewProps
 */
export type IAppMobChartViewProps = IAppDEMultiDataViewProps;
