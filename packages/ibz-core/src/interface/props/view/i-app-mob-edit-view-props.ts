import { IAppDEViewProps } from './i-app-de-view-props';

/**
 * 移动端编辑视图视图输入属性接口
 *
 * @export
 * @interface IAppMobEditViewProps
 */
export type IAppMobEditViewProps = IAppDEViewProps;
