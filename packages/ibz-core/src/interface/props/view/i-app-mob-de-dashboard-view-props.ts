import { IAppDEViewProps } from './i-app-de-view-props';

/**
 * 移动端实体数据看板视图输入参数
 *
 * @exports
 * @interface IAppMobDEDashboardViewProps
 * @extends IAppDEViewProps
 */
export type IAppMobDEDashboardViewProps = IAppDEViewProps;
