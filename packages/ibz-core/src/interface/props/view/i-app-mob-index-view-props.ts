import { IAppViewProps } from './i-app-view-props';

/**
 * 应用首页视图输入属性接口
 *
 * @export
 * @interface IAppIndexViewProps
 */
export type IAppIndexViewProps = IAppViewProps;
