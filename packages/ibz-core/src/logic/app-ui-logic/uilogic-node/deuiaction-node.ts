import { IPSDEUIActionLogic, getDstPSAppDEUIAction, IPSDEUILogicParam } from '@ibiz/dynamic-model-api';
import { LogUtil } from '../../../util';
import { UIActionContext } from '../uiaction-context';
import { AppUILogicNodeBase } from './logic-node-base';
/**
 * 实体界面行为调用节点
 *
 * @export
 * @class AppUILogicDeUIActionNode
 */
export class AppUILogicDeUIActionNode extends AppUILogicNodeBase {
  constructor() {
    super();
  }

  /**
   * 执行节点
   *
   * @param {IPSDEUIActionLogic} logicNode 逻辑节点模型数据
   * @param {UIActionContext} actionContext 界面逻辑上下文
   * @memberof AppUILogicDeUIActionNode
   */
  public async executeNode(logicNode: IPSDEUIActionLogic, actionContext: UIActionContext) {
    return new Promise<void>(async resolve => {
      const { data, context, viewparams, otherParams } = actionContext;
      const dstEntity = logicNode.getDstPSAppDataEntity();
      const dstUIAction = await getDstPSAppDEUIAction(logicNode);
      if (dstEntity && dstUIAction) {
        try {
          const targetUIService = await App.getUIService().getService(context, dstEntity.codeName.toLowerCase());
          if (targetUIService) {
            await targetUIService.loaded();
            // const result = await targetUIService.excuteAction(
            //     dstUIAction.uIActionTag,
            //     [data],
            //     context,
            //     viewparams,
            //     otherParams?.event,
            //     otherParams?.control,
            //     otherParams?.container,
            //     otherParams?.parentDeName,
            // );
            // const dstParam = actionContext.getParam((logicNode.getDstPSDEUILogicParam() as IPSDEUILogicParam)?.codeName);
            // if (result) {
            //     if (result.ok) {
            //         if (result.result) {
            //             Object.assign(dstParam, Array.isArray(result?.result) ? result.result[0] : result.result);
            //         }
            //         Object.assign(dstParam, { result: 'success' });
            //     } else {
            //         Object.assign(dstParam, { result: 'fail' });
            //     }
            //     resolve(this.computeNextNodes(logicNode, actionContext));
            // } else {
            //     LogUtil.warn('调用界面行为异常');
            //     return;
            // }
          }
        } catch (error) {
          LogUtil.warn(`调用界面行为异常,${error}`);
          return;
        }
      } else {
        LogUtil.warn('调用界面行为参数不足');
        return;
      }
    });
  }
}
