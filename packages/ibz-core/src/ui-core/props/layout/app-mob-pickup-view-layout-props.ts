import { AppLayoutProps } from './app-layout-props';
import { IAppMobPickUpViewLayoutProps } from '../../../interface';
/**
 * 移动端数据选择视图输入属性
 *
 * @export
 * @class AppMobPickUpViewLayoutProps
 */
export class AppMobPickUpViewLayoutProps extends AppLayoutProps implements IAppMobPickUpViewLayoutProps {}
