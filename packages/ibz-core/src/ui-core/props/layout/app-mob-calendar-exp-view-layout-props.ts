import { AppLayoutProps } from "./app-layout-props";

/**
 * 移动端日历导航视图视图布局输入参数
 * 
 * @export
 * @class AppMobCalendarExpViewLayoutProps
 */
export class AppMobCalendarExpViewLayoutProps extends AppLayoutProps { }