import { AppLayoutProps } from './app-layout-props';

/**
 * 移动端多表单编辑视图视图布局面板传入参数
 *
 * @export
 * @class AppMobMEditViewLayoutProps
 * @extends AppLayoutProps
 */
export class AppMobMEditViewLayoutProps extends AppLayoutProps {}
