import { AppMobPickUpViewLayoutProps } from './app-mob-pickup-view-layout-props';
import { IAppMobMPickUpViewLayoutProps } from '../../../interface';
/**
 * 移动端多数据选择视图输入属性
 *
 * @export
 * @class AppMobMPickUpViewLayoutProps
 */
export class AppMobMPickUpViewLayoutProps
  extends AppMobPickUpViewLayoutProps
  implements IAppMobMPickUpViewLayoutProps {}
