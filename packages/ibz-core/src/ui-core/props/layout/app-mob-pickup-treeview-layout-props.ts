import { IAppMobPickupTreeViewLayoutProps } from '../../../interface';
import { AppMobTreeViewLayoutProps } from './app-mob-tree-view-layout-props';

/**
 * 移动端选择树视图视图布局面板输入参数
 *
 */
export class AppMobPickupTreeViewLayoutProps
  extends AppMobTreeViewLayoutProps
  implements IAppMobPickupTreeViewLayoutProps {}
