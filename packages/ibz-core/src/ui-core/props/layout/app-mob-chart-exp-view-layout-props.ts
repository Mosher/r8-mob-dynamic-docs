import { AppLayoutProps } from './app-layout-props';

/**
 * 移动端图表导航视图视图布局输入参数
 * 
 * @export
 * @class AppMobChartExpViewLayoutProps
 */
export class AppMobChartExpViewLayoutProps extends AppLayoutProps { }