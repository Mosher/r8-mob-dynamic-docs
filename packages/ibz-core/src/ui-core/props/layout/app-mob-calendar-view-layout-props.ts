import { AppLayoutProps } from './app-layout-props';
import { IAppMobCalendarViewLayoutProps } from '../../../interface';
/**
 * 移动端日历视图视图布局面板传入参数
 *
 * @export
 * @class AppMobCalendarViewLayoutProps
 * @extends AppLayoutProps
 */
export class AppMobCalendarViewLayoutProps extends AppLayoutProps implements IAppMobCalendarViewLayoutProps {}
