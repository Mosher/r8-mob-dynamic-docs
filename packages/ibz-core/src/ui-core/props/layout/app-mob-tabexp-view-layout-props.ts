import { AppLayoutProps } from './app-layout-props';

/**
 * 移动端分页导航视图布局面板输入参数
 *
 * @export
 * @class AppMobTabExpViewLayoutProps
 */
export class AppMobTabExpViewLayoutProps extends AppLayoutProps {}
