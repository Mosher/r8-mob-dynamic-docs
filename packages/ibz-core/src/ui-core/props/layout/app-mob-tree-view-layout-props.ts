import { IAppMobTreeViewLayoutProps } from '../../../interface';
import { AppLayoutProps } from './app-layout-props';

/**
 * 移动端实体树视图视图布局面板传入参数
 *
 * @exports
 * @class AppMobTreeViewLayoutProps
 * @extends AppLayoutProps
 * @implements IAppMobTreeViewLayoutProps
 */
export class AppMobTreeViewLayoutProps extends AppLayoutProps implements IAppMobTreeViewLayoutProps {}
