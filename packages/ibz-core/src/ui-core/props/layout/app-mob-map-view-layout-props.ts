import { AppLayoutProps } from './app-layout-props';

/**
 * 移动端地图视图视图布局面板输入参数
 *
 * @export
 * @class AppMobMapViewLayoutProps
 */
export class AppMobMapViewLayoutProps extends AppLayoutProps {}
