import { AppLayoutProps } from './app-layout-props';

/**
 * 移动端多数据视图视图布局面板传入参数
 *
 * @export
 * @class AppMobMDViewLayoutProps
 * @extends AppLayoutProps
 */
export class AppMultiDataViewLayoutComponentProps extends AppLayoutProps { }
