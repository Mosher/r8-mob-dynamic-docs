import { IAppMobPortalViewLayoutProps } from '../../../interface';
import { AppLayoutProps } from './app-layout-props';

/**
 * 移动端应用看板视图输入参数
 *
 * @class AppPortalViewLayoutProps
 * @extends AppLayoutProps
 * @implements IAppMobPortalViewLayoutProps
 */
export class AppPortalViewLayoutProps extends AppLayoutProps implements IAppMobPortalViewLayoutProps {}
