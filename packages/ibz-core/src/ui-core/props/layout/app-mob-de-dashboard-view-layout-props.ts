import { IAppMobDEDashboardViewLayoutProps } from '../../../interface';
import { AppLayoutProps } from './app-layout-props';

/**
 * 移动端实体数据看板视图输入参数
 *
 * @exports
 * @class AppMobDEDashboardViewLayoutProps
 * @extends AppLayoutProps
 * @implements IAppMobDEDashboardViewLayoutProps
 */
export class AppMobDEDashboardViewLayoutProps extends AppLayoutProps implements IAppMobDEDashboardViewLayoutProps {}
