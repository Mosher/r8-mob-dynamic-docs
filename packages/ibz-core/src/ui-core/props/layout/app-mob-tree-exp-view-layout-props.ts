import { AppLayoutProps } from './app-layout-props';

/**
 * 移动端树导航视图视图布局输入参数
 * 
 * @export
 * @class AppMobTreeExpViewLayoutProps
 */
export class AppMobTreeExpViewLayoutProps extends AppLayoutProps { }