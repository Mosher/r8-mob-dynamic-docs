import { AppLayoutProps } from './app-layout-props';

/**
 * 移动端自定义视图视图布局面板传入参数
 *
 * @export
 * @class AppMobCustomViewLayoutProps
 * @extends AppLayoutProps
 */
export class AppMobCustomViewLayoutProps extends AppLayoutProps {}
