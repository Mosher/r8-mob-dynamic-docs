import { AppEditorProps } from './app-editor-props';

/**
 * 评分器输入参数
 *
 * @export
 * @class AppRatingProps
 */
export class AppRatingProps extends AppEditorProps {}
