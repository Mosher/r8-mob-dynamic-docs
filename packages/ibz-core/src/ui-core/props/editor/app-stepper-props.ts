import { AppEditorProps } from './app-editor-props';

/**
 * 步进器输入参数
 *
 * @export
 * @class AppStepperProps
 */
export class AppStepperProps extends AppEditorProps {}
