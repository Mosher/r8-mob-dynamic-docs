import { AppEditorProps } from './app-editor-props';

/**
 * 移动端上传编辑器输入参数
 *
 * @class AppUploadProps
 * @extends AppUploadProps
 */
export class AppUploadProps extends AppEditorProps {}
