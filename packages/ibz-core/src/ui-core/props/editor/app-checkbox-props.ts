import { AppEditorProps } from './app-editor-props';

/**
 * 选项框编辑器输入参数
 *
 * @export
 * @class AppCheckBoxProps
 */
export class AppCheckBoxProps extends AppEditorProps {}
