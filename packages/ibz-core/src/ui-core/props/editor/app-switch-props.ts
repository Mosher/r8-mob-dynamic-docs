import { AppEditorProps } from './app-editor-props';

/**
 * 开关输入参数
 *
 * @export
 * @class AppSwitchProps
 */
export class AppSwitchProps extends AppEditorProps {}
