import { AppEditorProps } from './app-editor-props';

/**
 * 时间选择器输入参数
 *
 * @export
 * @class AppDatePickerProps
 */
export class AppDatePickerProps extends AppEditorProps {}
