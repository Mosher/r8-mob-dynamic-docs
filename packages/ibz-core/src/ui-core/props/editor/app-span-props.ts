import { AppEditorProps } from './app-editor-props';

/**
 * 标签输入参数
 *
 * @export
 * @class AppSpanProps
 */
export class AppSpanProps extends AppEditorProps {}
