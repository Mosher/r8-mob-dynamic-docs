import { AppEditorProps } from './app-editor-props';

/**
 * 应用菜单输入参数
 *
 * @export
 * @class AppSliderProps
 */
export class AppSliderProps extends AppEditorProps {}
