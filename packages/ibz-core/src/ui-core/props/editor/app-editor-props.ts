import { IParam } from '../../../interface';

/**
 * 应用编辑器输入参数
 *
 * @export
 * @class AppEditorProps
 */
export class AppEditorProps {
  editorInstance: IParam = {};
  navContext: IParam = {};
  navParam: IParam = {};
  contextData: IParam = {};
  contextState: IParam = {};
  value: any = undefined;
  parentItem: IParam = {};
  disabled: boolean = false;
  modelService: IParam = {};
  modelData: any = {};
}
