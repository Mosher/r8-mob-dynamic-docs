import { AppCtrlProps } from './app-ctrl-props';

/**
 * 移动端选择视图面板输入参数
 *
 * @export
 * @class AppMobPickUpViewPanelProps
 */
export class AppMobPickUpViewPanelProps extends AppCtrlProps {
  /**
   * @description 是否多选
   */
  isMultiple: boolean = false;
}
