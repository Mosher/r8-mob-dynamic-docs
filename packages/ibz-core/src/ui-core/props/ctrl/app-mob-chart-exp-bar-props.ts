import { AppExpBarCtrlProps } from './app-exp-bar-ctrl-props';

/**
 * 移动端图表导航栏输入参数
 *
 * @class AppMobChartExpBarProps
 * @extends AppCtrlProps
 */
export class AppMobChartExpBarProps extends AppExpBarCtrlProps {}
