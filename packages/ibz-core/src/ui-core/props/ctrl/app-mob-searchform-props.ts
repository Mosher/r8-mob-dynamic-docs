import { AppCtrlProps } from './app-ctrl-props';

/**
 * 移动端搜索表单输入参数
 *
 * @export
 * @class AppMobSearchFormProps
 */
export class AppMobSearchFormProps extends AppCtrlProps {

  /**
   * @description 默认展开搜索表单
   */
  expandSearchForm: boolean = false;;
}
