import { AppCtrlProps } from './app-ctrl-props';

/**
 * 应用菜单输入参数
 *
 * @export
 * @class AppMobMenuProps
 */
export class AppMobMenuProps extends AppCtrlProps {}
