import { AppExpBarCtrlProps } from './app-exp-bar-ctrl-props';

/**
 * 移动端树导航部件输入参数
 *
 * @class AppMobTreeExpBarProps
 * @extends AppExpBarCtrlProps
 */
export class AppMobTreeExpBarProps extends AppExpBarCtrlProps {}
