import { AppExpBarCtrlProps } from './app-exp-bar-ctrl-props';

/**
 * 移动端日历导航部件输入参数
 *
 * @class AppMobCalendarExpBarProps
 * @extends AppExpBarCtrlProps
 */
export class AppMobCalendarExpBarProps extends AppExpBarCtrlProps {}
