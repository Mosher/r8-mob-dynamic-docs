import { AppCtrlProps } from './app-ctrl-props';

/**
 * 移动端分页导航面板输入参数
 *
 * @export
 * @class AppMobTabExpPanelProps
 */
export class AppMobTabExpPanelProps extends AppCtrlProps {}
