import { AppCtrlProps } from './app-ctrl-props';

/**
 * 移动端分页视图面板输入参数
 *
 * @export
 * @class AppMobTabViewPanelProps
 */
export class AppMobTabViewPanelProps extends AppCtrlProps {}
