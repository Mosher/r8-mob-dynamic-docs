import { AppCtrlProps } from './app-ctrl-props';

/**
 * 移动端门户部件输入参数
 *
 * @class AppMobStateWizardPanelProps
 * @extends AppCtrlProps
 */
export class AppMobStateWizardPanelProps extends AppCtrlProps {}
