import { AppCtrlProps } from './app-ctrl-props';

/**
 * 试图工具栏输入参数
 *
 * @export
 * @class AppMobToolbarProps
 */
export class AppMobToolbarProps extends AppCtrlProps {}
