import { AppCtrlProps } from './app-ctrl-props';
import { AppExpBarCtrlProps } from './app-exp-bar-ctrl-props';

/**
 * 移动端列表导航部件输入参数
 *
 * @class AppMobListExpBarProps
 * @extends AppCtrlProps
 */
export class AppMobListExpBarProps extends AppExpBarCtrlProps {}
