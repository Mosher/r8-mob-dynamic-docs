import { AppMDCtrlProps } from './app-md-ctrl-props';
/**
 * 移动端日历部件传入参数
 *
 * @class AppMobCalendarCtrlProps
 * @extends AppMDCtrlProps
 */
export class AppMobCalendarProps extends AppMDCtrlProps {}
