import { IAppDEViewProps } from '../../../interface';
import { AppViewProps } from './app-view-props';

/**
 * 主数据视图基类输入属性
 *
 * @export
 * @class AppDEViewProps
 * @export AppViewProps
 */
export class AppDEViewProps extends AppViewProps implements IAppDEViewProps {}
