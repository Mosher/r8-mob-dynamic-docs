import { IAppMobChartExpViewProps } from '../../../interface';
import { AppDEViewProps } from './app-de-view-props';

/**
 * 移动端图表导航视图输入属性
 *
 * @export
 * @class AppMobChartExpViewProps
 */
 export class AppMobChartExpViewProps extends AppDEViewProps implements IAppMobChartExpViewProps {}