import { IAppMobCustomViewProps } from '../../../interface';
import { AppDEViewProps } from './app-de-view-props';

/**
 * 移动端自定义视图视图输入属性
 *
 * @export
 * @class AppMobCustomViewProps
 */
export class AppMobCustomViewProps extends AppDEViewProps implements IAppMobCustomViewProps {}
