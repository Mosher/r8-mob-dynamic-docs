import { IAppMobChartViewProps } from '../../../interface';
import { AppDEMultiDataViewProps } from './app-de-multi-data-view-props';

/**
 * 移动端图表视图视图输入属性
 *
 * @export
 * @class AppMobChartViewProps
 */
export class AppMobChartViewProps extends AppDEMultiDataViewProps implements IAppMobChartViewProps {}
