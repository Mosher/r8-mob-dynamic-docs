import { IAppMobHtmlViewProps } from '../../../interface';
import { AppDEViewProps } from './app-de-view-props';

/**
 * 移动端Html视图输入属性
 *
 * @export
 * @class AppMobHtmlViewProps
 */
export class AppMobHtmlViewProps extends AppDEViewProps implements IAppMobHtmlViewProps {}
