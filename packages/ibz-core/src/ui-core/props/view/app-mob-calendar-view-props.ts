import { IAppMobCalendarViewProps } from '../../../interface';
import { AppDEMultiDataViewProps } from './app-de-multi-data-view-props';

/**
 * 移动端日历视图视图输入属性
 *
 * @export
 * @class AppMobCalendarViewProps
 */
export class AppMobCalendarViewProps extends AppDEMultiDataViewProps implements IAppMobCalendarViewProps {}
