import { IAppMobTreeViewProps } from '../../../interface';
import { AppDEMultiDataViewProps } from './app-de-multi-data-view-props';

/**
 * 移动端实体树视图输入参数
 *
 * @exports
 * @class AppMobTreeViewProps
 * @extends AppDEMultiDataViewProps
 * @implements IAppMobTreeViewProps
 */
export class AppMobTreeViewProps extends AppDEMultiDataViewProps implements IAppMobTreeViewProps {}
