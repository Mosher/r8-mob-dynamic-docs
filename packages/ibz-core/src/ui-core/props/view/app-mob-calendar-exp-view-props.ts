import { IAppMobCalendarExpViewProps } from '../../../interface';
import { AppDEExpViewProps } from './app-de-exp-view-props';

/**
 * 移动端日历导航视图输入属性
 *
 * @export
 * @class AppMobCalendarExpViewProps
 */
 export class AppMobCalendarExpViewProps extends AppDEExpViewProps implements IAppMobCalendarExpViewProps {}