import { IAppMobPickUpMDViewProps } from '../../../interface';
import { AppMobMDViewProps } from './app-mob-md-view-props';

/**
 * 移动端选择多数据视图视图输入属性
 *
 * @export
 * @class AppMobPickUpMDViewProps
 */
export class AppMobPickUpMDViewProps extends AppMobMDViewProps implements IAppMobPickUpMDViewProps {}
