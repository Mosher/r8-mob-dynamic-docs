import { IAppDEMultiDataViewProps } from '../../../interface';
import { AppDEViewProps } from './app-de-view-props';

/**
 * 多数据视图基类输入属性
 *
 * @export
 * @class AppDEMultiDataViewProps
 * @export AppMainViewProps
 */
export class AppDEMultiDataViewProps extends AppDEViewProps implements IAppDEMultiDataViewProps {
  /**
   * 是否为多选
   * @interface IAppViewProps
   */
  isMultiple: boolean = false;
}
