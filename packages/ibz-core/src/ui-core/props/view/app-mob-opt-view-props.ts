import { IAppMobOptViewProps } from '../../../interface';
import { AppDEViewProps } from './app-de-view-props';

/**
 * 移动端选项操作视图输入属性
 *
 * @export
 * @class AppMobOptViewProps
 */
 export class AppMobOptViewProps extends AppDEViewProps implements IAppMobOptViewProps {}