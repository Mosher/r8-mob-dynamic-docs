import { IAppMobPickupTreeViewProps } from '../../../interface';
import { AppMobTreeViewProps } from './app-mob-tree-view-props';

/**
 * 移动端树选择视图属兔参数
 *
 * @class AppMobPickupTreeViewProps
 */
export class AppMobPickupTreeViewProps extends AppMobTreeViewProps implements IAppMobPickupTreeViewProps {}
