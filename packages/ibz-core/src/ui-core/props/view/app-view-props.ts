import { IParam } from '../../../interface';
import { IAppViewProps } from '../../../interface';

/**
 * 视图输入属性
 *
 * @export
 * @class AppViewProps
 */
export class AppViewProps implements IAppViewProps {
  viewPath = '';
  viewShowMode: string = '';
  navContext: IParam = {};
  navParam: IParam = {};
  navDatas: IParam = {};
  isShowCaptionBar: boolean = false;
  isLoadDefault: boolean = false;
  viewState: IParam = {};
  modelData: any = {};
  previewData: IParam[] = []; 
}
