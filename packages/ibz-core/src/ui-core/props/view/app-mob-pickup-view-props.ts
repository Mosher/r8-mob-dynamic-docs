import { AppDEViewProps } from './app-de-view-props';
import { IAppMobPickUpViewProps } from '../../../interface';
/**
 * 移动端面板视图视图输入属性
 *
 * @export
 * @class AppMobPickUpViewProps
 */
export class AppMobPickUpViewProps extends AppDEViewProps implements IAppMobPickUpViewProps {}
