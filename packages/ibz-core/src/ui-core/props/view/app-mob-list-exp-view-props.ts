import { IAppMobListExpViewProps } from '../../../interface';
import { AppDEViewProps } from './app-de-view-props';

/**
 * 移动端列表导航视图输入属性
 *
 * @export
 * @class AppMobListExpViewProps
 */
 export class AppMobListExpViewProps extends AppDEViewProps implements IAppMobListExpViewProps {}