import { IAppMobTreeExpViewProps } from '../../../interface';
import { AppDEExpViewProps } from './app-de-exp-view-props';

/**
 * 移动端树导航视图输入属性
 *
 * @export
 * @class AppMobTreeExpViewProps
 */
 export class AppMobTreeExpViewProps extends AppDEExpViewProps implements IAppMobTreeExpViewProps {}