import { IAppMobMDViewProps } from '../../../interface';
import { AppDEMultiDataViewProps } from './app-de-multi-data-view-props';

/**
 * 移动端多数据视图视图输入属性
 *
 * @export
 * @class AppMobMDViewProps
 */
export class AppMobMDViewProps extends AppDEMultiDataViewProps implements IAppMobMDViewProps {}
