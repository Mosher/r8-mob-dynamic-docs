import { IAppMobMEditViewProps } from '../../../interface';
import { AppDEMultiDataViewProps } from './app-de-multi-data-view-props';

/**
 * 移动端多数据视图视图输入属性
 *
 * @export
 * @class AppMobMEditViewProps
 */
export class AppMobMEditViewProps extends AppDEMultiDataViewProps implements IAppMobMEditViewProps {}
