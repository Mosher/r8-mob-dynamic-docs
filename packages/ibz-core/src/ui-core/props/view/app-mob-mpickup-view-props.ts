import { AppMobPickUpViewProps } from './app-mob-pickup-view-props';
import { IAppMobMPickUpViewProps } from '../../../interface';
/**
 * 移动端多数据选择视图视图输入属性
 *
 * @export
 * @class AppMobMPickUpViewProps
 */
export class AppMobMPickUpViewProps extends AppMobPickUpViewProps implements IAppMobMPickUpViewProps {}
