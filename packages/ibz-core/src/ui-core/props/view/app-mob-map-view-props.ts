import { IAppMobMapViewProps } from '../../../interface';
import { AppDEMultiDataViewProps } from './app-de-multi-data-view-props';

/**
 * 移动端地图视图视图输入属性
 *
 * @export
 * @class AppMobMapViewProps
 */
export class AppMobMapViewProps extends AppDEMultiDataViewProps implements IAppMobMapViewProps {}
