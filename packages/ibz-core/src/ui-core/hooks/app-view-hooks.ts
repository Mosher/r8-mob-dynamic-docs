import { IAppViewHooks, IParam } from '../../interface';
import { SyncSeriesHook } from 'qx-util';

/**
 * 视图hooks基类
 *
 * @export
 * @class AppViewHooks
 */
export class AppViewHooks implements IAppViewHooks {
  /**
   * 模型数据加载完成钩子
   *
   * @class AppViewHooks
   */
  modelLoaded: SyncSeriesHook<[], { arg: IParam }> = new SyncSeriesHook<[], { arg: IParam }>();

  /**
   * 绑定事件钩子
   *
   * @interface AppViewHooks
   */
  onEvent: SyncSeriesHook<[], { event: string | string[]; fn: Function }> = new SyncSeriesHook<
    [],
    { event: string | string[]; fn: Function }
  >();

  /**
   * 解绑事件钩子
   *
   * @interface AppViewHooks
   */
  offEvent: SyncSeriesHook<[], { event: string | string[]; fn?: Function }> = new SyncSeriesHook<
    [],
    { event: string | string[]; fn?: Function }
  >();

  /**
   * 视图事件钩子
   *
   * @class AppViewHooks
   */
  public event: SyncSeriesHook<[], { viewName: string; action: string; data: IParam }> = new SyncSeriesHook<
    [],
    { viewName: string; action: string; data: IParam }
  >();

  /**
   * 视图刷新钩子
   *
   * @class AppViewHooks
   */
  public refresh: SyncSeriesHook<[], { arg: IParam }> = new SyncSeriesHook<[], { arg: IParam }>();

  /**
   * 视图关闭钩子
   *
   * @class AppViewHooks
   */
  public closeView: SyncSeriesHook<[], { arg: IParam }> = new SyncSeriesHook<[], { arg: IParam }>();

  /**
   * 视图挂载之后钩子
   *
   * @class AppViewHooks
   */
   public mounted: SyncSeriesHook<[], { arg: IParam }> = new SyncSeriesHook<[], { arg: IParam }>();
}
