import { IAppCtrlHooks, ICtrlEventParam, IParam } from '../../interface';
import { SyncSeriesHook } from 'qx-util';

/**
 * 部件hooks基类
 *
 * @export
 * @class AppCtrlHooks
 */
export class AppCtrlHooks implements IAppCtrlHooks {
  /**
   * 模型数据加载完成钩子
   *
   * @class AppCtrlHooks
   */
  public modelLoaded: SyncSeriesHook<[], { arg: IParam }> = new SyncSeriesHook<[], { arg: IParam }>();
  /**
   * 绑定事件钩子
   *
   * @interface AppCtrlHooks
   */
  public onEvent: SyncSeriesHook<[], { event: string | string[]; fn: Function }> = new SyncSeriesHook<
    [],
    { event: string | string[]; fn: Function }
  >();

  /**
   * 解绑事件钩子
   *
   * @interface AppCtrlHooks
   */
  public offEvent: SyncSeriesHook<[], { event: string | string[]; fn?: Function }> = new SyncSeriesHook<
    [],
    { event: string | string[]; fn: Function }
  >();

  /**
   * 部件事件钩子
   *
   * @class AppCtrlHooks
   */
  public event: SyncSeriesHook<[], ICtrlEventParam> = new SyncSeriesHook<[], ICtrlEventParam>();

  /**
   * 关闭视图钩子
   *
   * @class AppCtrlHooks
   */
  public closeView: SyncSeriesHook<[], { arg: IParam }> = new SyncSeriesHook<[], { arg: IParam }>();

  /**
   * 部件挂载之后钩子
   *
   * @class AppCtrlHooks
   */
   public mounted: SyncSeriesHook<[], { arg: IParam }> = new SyncSeriesHook<[], { arg: IParam }>();  
}
