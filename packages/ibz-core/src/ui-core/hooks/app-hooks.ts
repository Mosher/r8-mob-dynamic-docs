import { IAppHooks, IParam } from '../../interface';
import { SyncSeriesHook } from 'qx-util';

/**
 * 应用hooks基类
 *
 * @export
 * @class AppHooks
 */
export class AppHooks implements IAppHooks {
  /**
   * 模型数据加载完成钩子
   *
   * @class AppHooks
   */
  modelLoaded: SyncSeriesHook<[], { arg: IParam }> = new SyncSeriesHook<[], { arg: IParam }>();

  /**
   * 绑定事件钩子
   *
   * @interface AppHooks
   */
  onEvent: SyncSeriesHook<[], { event: string | string[]; fn: Function }> = new SyncSeriesHook<
    [],
    { event: string | string[]; fn: Function }
  >();

  /**
   * 解绑事件钩子
   *
   * @interface AppHooks
   */
  offEvent: SyncSeriesHook<[], { event: string | string[]; fn?: Function }> = new SyncSeriesHook<
    [],
    { event: string | string[]; fn?: Function }
  >();

  /**
   * 请求之前
   *
   * @interface AppHooks
   */
  public beforeRequest: SyncSeriesHook<[], { arg: IParam }> = new SyncSeriesHook<[], { arg: IParam }>();

  /**
   * 响应之后
   *
   * @interface AppHooks
   */
  public afterResponse: SyncSeriesHook<[], { arg: IParam }> = new SyncSeriesHook<[], { arg: IParam }>();

  /**
   * 鉴权之前
   *
   * @interface AppHooks
   */
  public beforeAuth: SyncSeriesHook<[], { arg: IParam }> = new SyncSeriesHook<[], { arg: IParam }>();

  /**
   * 鉴权之后
   *
   * @interface AppHooks
   */
  public afterAuth: SyncSeriesHook<[], { arg: IParam }> = new SyncSeriesHook<[], { arg: IParam }>();

  /**
   * 获取应用数据之前
   *
   * @interface AppHooks
   */
  public beforeGetAppData: SyncSeriesHook<[], { arg: IParam }> = new SyncSeriesHook<[], { arg: IParam }>();

  /**
   * 获取应用数据之后
   *
   * @interface AppHooks
   */
  public afterGetAppData: SyncSeriesHook<[], { arg: IParam }> = new SyncSeriesHook<[], { arg: IParam }>();

  /**
   * 获取租户之前
   *
   * @interface AppHooks
   */
  public beforeDcSystem: SyncSeriesHook<[], { arg: IParam }> = new SyncSeriesHook<[], { arg: IParam }>();

  /**
   * 获取租户之后
   *
   * @interface AppHooks
   */
  public afterDcSystem: SyncSeriesHook<[], { arg: IParam }> = new SyncSeriesHook<[], { arg: IParam }>();
}
