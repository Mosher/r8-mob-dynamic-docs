import { IPSDEMultiEditViewPanel } from '@ibiz/dynamic-model-api';
import { AppMobMEditViewPanelModel } from '../ctrl-model';
import { ControlServiceBase } from './ctrl-service-base';

/**
 * Main 部件服务对象
 *
 * @export
 * @class AppMobMEditViewPanelService
 */
export class AppMobMEditViewPanelService extends ControlServiceBase {
  /**
   * 多编辑视图实例对象
   *
   * @memberof AppMobMEditViewPanelService
   */
  public controlInstance!: IPSDEMultiEditViewPanel;

  /**
   * 数据服务对象
   *
   * @type {any}
   * @memberof AppMobMEditViewPanelService
   */
  public appEntityService!: any;

  /**
   * 初始化服务参数
   *
   * @type {boolean}
   * @memberof AppMobMEditViewPanelService
   */
  public async initServiceParam() {
    this.appEntityService = await App.getEntityService().getService(this.context, this.appDeCodeName.toLowerCase());
    this.model = new AppMobMEditViewPanelModel(this.controlInstance);
  }

  /**
   * Creates an instance of AppMobMEditViewPanelService
   *
   * @param {*} [opts={}]
   * @memberof AppMobMEditViewPanelService
   */
  constructor(opts: any = {}, context?: any) {
    super(opts, context);
    this.controlInstance = opts;
  }

  /**
   * 部件服务加载
   *
   * @memberof AppMobMEditViewPanelService
   */
  public async loaded() {
    await this.initServiceParam();
  }

  /**
   * 查询数据
   *
   * @param {string} action
   * @param {*} [context={}]
   * @param {*} [data={}]
   * @param {boolean} [isloading]
   * @returns {Promise<any>}
   * @memberof AppMobMEditViewPanelService
   */
  public get(action: string, context: any = {}, data: any = {}, isloading?: boolean): Promise<any> {
    const { data: Data, context: Context } = this.handleRequestData(action, context, data);
    return new Promise((resolve: any, reject: any) => {
      let result: Promise<any>;
      const _appEntityService: any = this.appEntityService;
      if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
        result = _appEntityService[action](Context, Data, isloading);
      } else {
        result = this.appEntityService.Get(Context, Data, isloading);
      }
      result
        .then(response => {
          this.handleResponse(action, response);
          resolve(response);
        })
        .catch(response => {
          reject(response);
        });
    });
  }

  /**
   * 加载草稿
   *
   * @param {string} action
   * @param {*} [context={}]
   * @param {*} [data={}]
   * @param {boolean} [isloading]
   * @returns {Promise<any>}
   * @memberof AppMobMEditViewPanelService
   */
  public loadDraft(action: string, context: any = {}, data: any = {}, isloading?: boolean): Promise<any> {
    const { data: Data, context: Context } = this.handleRequestData(action, context, data);
    return new Promise((resolve: any, reject: any) => {
      let result: Promise<any>;
      const _appEntityService: any = this.appEntityService;
      if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
        result = _appEntityService[action](Context, Data, isloading);
      } else {
        result = this.appEntityService.GetDraft(Context, Data, isloading);
      }
      result
        .then(response => {
          //处理返回数据，补充判断标识
          if (response.data) {
            Object.assign(response.data, { srfuf: 0 });
          }
          this.handleResponse(action, response);
          resolve(response);
        })
        .catch(response => {
          reject(response);
        });
    });
  }
}
