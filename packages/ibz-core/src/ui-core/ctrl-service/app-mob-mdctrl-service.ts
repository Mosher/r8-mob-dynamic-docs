import { AppMobMDCtrlModel } from '../ctrl-model';
import { IPSDEMobMDCtrl } from '@ibiz/dynamic-model-api';
import { ControlServiceBase } from './ctrl-service-base';

export class AppMobMDCtrlService extends ControlServiceBase {
  /**
   * 多数据部件实例对象
   *
   * @type {IPSDEMobMDCtrl}
   * @memberof AppMobMDCtrlService
   */
  public controlInstance!: IPSDEMobMDCtrl;

  /**
   * 实体服务对象
   *
   * @type {IPSDEMobMDCtrl}
   * @memberof AppMobMDCtrlService
   */
  public appEntityService!: any;

  /**
   * 远端数据
   *
   * @type {*}
   * @memberof AppMobMDCtrlService
   */
  private remoteCopyData: any = {};

  /**
   * 构造多数据部件部件服务
   *
   * @param opts 多数据部件实例
   * @param context 应用上下文
   * @memberof AppMobMDCtrlService
   */
  constructor(opts: any = {}, context?: any) {
    super(opts, context);
    this.controlInstance = opts as IPSDEMobMDCtrl;
  }

  /**
   * 部件服务加载
   *
   * @memberof AppMobMDCtrlService
   */
  public async loaded() {
    await this.initServiceParam();
  }

  /**
   * 初始化服务参数
   *
   * @memberof AppMobMDCtrlService
   */
  public async initServiceParam() {
    this.appEntityService = await App.getEntityService().getService(this.context, this.appDeCodeName.toLowerCase());
    this.model = new AppMobMDCtrlModel(this.controlInstance);
  }

  /**
   * 查询数据
   *
   * @param {string} action
   * @param {*} [context={}]
   * @param {*} [data={}]
   * @param {boolean} [isloading]
   * @returns {Promise<any>}
   * @memberof AppListService
   */
  public search(action: string, context: any = {}, data: any = {}, isloading?: boolean): Promise<any> {
    const { data: Data, context: Context } = this.handleRequestData(action, context, data, true);
    return new Promise((resolve: any, reject: any) => {
      const _appEntityService: any = this.appEntityService;
      let result: Promise<any>;
      if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
        result = _appEntityService[action](Context, Data, isloading);
      } else {
        result = _appEntityService.FetchDefault(Context, Data, isloading);
      }
      result
        .then(async response => {
          await this.handleResponse(action, response);
          resolve(response);
        })
        .catch(response => {
          reject(response);
        });
    });
  }

  /**
   * 删除数据
   *
   * @param {string} action
   * @param {*} [context={}]
   * @param {*} [data={}]
   * @param {boolean} [isloading]
   * @returns {Promise<any>}
   * @memberof AppListService
   */
  public delete(action: string, context: any = {}, data: any = {}, isloading?: boolean): Promise<any> {
    const { data: Data, context: Context } = this.handleRequestData(action, context, data, true);
    return new Promise((resolve: any, reject: any) => {
      const _appEntityService: any = this.appEntityService;
      let result: Promise<any>;
      if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
        result = _appEntityService[action](Context, Data, isloading);
      } else {
        result = _appEntityService.remove(Context, Data, isloading);
      }
      result
        .then(response => {
          this.handleResponse(action, response);
          resolve(response);
        })
        .catch(response => {
          reject(response);
        });
    });
  }

  /**
   * 添加数据
   *
   * @param {string} action
   * @param {*} [context={}]
   * @param {*} [data={}]
   * @param {boolean} [isloading]
   * @returns {Promise<any>}
   * @memberof AppListService
   */
  public add(action: string, context: any = {}, data: any = {}, isloading?: boolean): Promise<any> {
    const { data: Data, context: Context } = this.handleRequestData(action, context, data, true);
    return new Promise((resolve: any, reject: any) => {
      const _appEntityService: any = this.appEntityService;
      let result: Promise<any>;
      if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
        result = _appEntityService[action](Context, Data, isloading);
      } else {
        result = _appEntityService.Create(Context, Data, isloading);
      }
      result
        .then(response => {
          this.handleResponse(action, response);
          resolve(response);
        })
        .catch(response => {
          reject(response);
        });
    });
  }

  /**
   * 修改数据
   *
   * @param {string} action
   * @param {*} [context={}]
   * @param {*} [data={}]
   * @param {boolean} [isloading]
   * @returns {Promise<any>}
   * @memberof AppListService
   */
  public update(action: string, context: any = {}, data: any = {}, isloading?: boolean): Promise<any> {
    const { data: Data, context: Context } = this.handleRequestData(action, context, data, true);
    return new Promise((resolve: any, reject: any) => {
      const _appEntityService: any = this.appEntityService;
      let result: Promise<any>;
      if (_appEntityService[action] && _appEntityService[action] instanceof Function) {
        result = _appEntityService[action](Context, Data, isloading);
      } else {
        result = _appEntityService.Update(Context, Data, isloading);
      }
      result
        .then(response => {
          this.handleResponse(action, response);
          resolve(response);
        })
        .catch(response => {
          reject(response);
        });
    });
  }
}
