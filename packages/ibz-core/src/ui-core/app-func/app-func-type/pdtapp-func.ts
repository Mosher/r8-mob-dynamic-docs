import { IPSAppFunc } from '@ibiz/dynamic-model-api';
import { AppFuncBase } from './app-func-base';

/**
 * 预置应用功能
 *
 * @class PdtAppFunc
 */
export class PdtAppFunc extends AppFuncBase {
  /**
   * Creates an instance of PdtAppFunc.
   * @memberof PdtAppFunc
   */
  constructor() {
    super();
  }

  /**
   * 执行功能
   *
   * @param appFunc 应用功能
   * @param context 上下文
   * @param viewParam 视图参数
   * @param container 部件对象
   * @param isGlobal 是否为全局
   * @param arg 额外参数
   * @memberof PdtAppFunc
   */
  public executeFunc(appFunc: IPSAppFunc, context: any, viewParam: any, container: any, isGlobal?: boolean, arg?: any) {
    switch (appFunc.name) {
      // 系统登出
      case '系统登出':
        App.onLogout();
        break;
      // 系统登录
      case '系统登录':
        App.goPresetView('login');
        break;
      // 清空缓存
      case '清空缓存':
        App.onClearAppData();
        break;
      default:
        App.getNoticeService().warning(`${appFunc.name} 应用功能暂未支持`);
    }
  }
}
