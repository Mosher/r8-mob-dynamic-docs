import { AppFuncBase } from './app-func-base';
import { IPSAppDataEntity, IPSAppDEView, IPSAppFunc, IPSAppView } from '@ibiz/dynamic-model-api';
import { ViewTool, Util } from '../../../util';

/**
 * 打开应用视图
 *
 * @class AppViewFunc
 */
export class AppViewFunc extends AppFuncBase {
  constructor() {
    super();
  }

  /**
   * 参数处理
   *
   * @param context 视图上下文
   * @param appView 应用视图
   * @param deResParameters
   * @param parameters
   */
  public handleParams(context: any, appView: IPSAppView, deResParameters: any[], parameters: any[]) {
    let params: any = [];
    const appDataEntity: IPSAppDataEntity | null = appView.getPSAppDataEntity();
    if (appDataEntity) {
      if ((appView.openMode && (appView.openMode == 'INDEXVIEWTAB' || appView.openMode == '')) || !appView.openMode) {
        params = [
          {
            pathName: Util.srfpluralize(appDataEntity.codeName).toLowerCase(),
            parameterName: appDataEntity.codeName.toLowerCase(),
          },
          { pathName: 'views', parameterName: (appView as IPSAppDEView).getPSDEViewCodeName()?.toLowerCase() },
        ];
      } else {
        params = [
          {
            pathName: Util.srfpluralize(appDataEntity.codeName).toLowerCase(),
            parameterName: appDataEntity.codeName.toLowerCase(),
          },
        ];
      }
    } else {
      params = [{ pathName: 'views', parameterName: appView.name.toLowerCase() }];
    }
    Object.assign(parameters, params);
  }

  /**
   * 执行功能
   *
   * @param appFunc 应用功能
   * @param context 上下文
   * @param viewParam 视图参数
   * @param container 容器对象
   * @param isGlobal 是否为全局
   * @param arg 额外参数
   * @memberof AppViewFunc
   */
  public executeFunc(appFunc: IPSAppFunc, context: any, viewParam: any, container: any, isGlobal?: boolean, arg?: any) {
    const appView: IPSAppView | null = appFunc.getPSAppView();
    if (appView) {
      if (appView.redirectView) {
        App.getNoticeService().warning('重定向视图不支持应用功能打开');
      } else {
        if (appFunc.openViewParam) {
          Object.assign(viewParam, appFunc.openViewParam);
        }
        const deResParameters: any[] = [];
        const parameters: any[] = [];
        this.handleParams(context, appView, deResParameters, parameters);
        return this.openAppView(appView, context, viewParam, deResParameters, parameters, container, isGlobal, arg);
      }
    } else {
      App.getNoticeService().warning('应用视图不存在');
    }
  }

  /**
   * 打开应用视图
   *
   * @param appView 应用视图
   * @param context 视图上下文
   * @param viewParam 视图参数
   * @param deResParameters
   * @param parameters
   * @param container 容器对象
   * @param isGlobal 是否为全局
   * @param arg 额外参数
   * @memberof AppViewFunc
   */
  public openAppView(
    appView: IPSAppView,
    context: any,
    viewParam: any,
    deResParameters: any[],
    parameters: any[],
    container: any,
    isGlobal?: boolean,
    arg?: any,
  ) {
    switch (appView.openMode) {
      // 顶级容器分页
      case 'INDEXVIEWTAB':
        return this.openIndexViewTab(context, viewParam, deResParameters, parameters, container, isGlobal, arg);
      // 非模式弹出
      case 'POPUP':
        return this.openPopup(viewParam, deResParameters, parameters, container, isGlobal, arg);
      // 模式弹出
      case 'POPUPMODAL':
        return this.openModal(context, viewParam, appView, container, isGlobal, arg);
      // 独立程序弹出
      case 'POPUPAPP':
        return this.openApp(context, viewParam, deResParameters, parameters, container, isGlobal, arg);
      // 模态左侧抽屉弹出
      case 'DRAWER_LEFT':
        return this.openDrawer(context, viewParam, appView, container, isGlobal, arg);
      // 模态右侧抽屉弹出
      case 'DRAWER_RIGHT':
        return this.openDrawer(context, viewParam, appView, container, isGlobal, arg);
      // 模态上方抽屉弹出
      case 'DRAWER_TOP':
        return this.openDrawer(context, viewParam, appView, container, isGlobal, arg);
      // 模态下发抽屉弹出
      case 'DRAWER_BOTTOM':
        return this.openDrawer(context, viewParam, appView, container, isGlobal, arg);
      // 气泡卡片
      case 'POPOVER':
        return this.openPopover(context, viewParam, appView, container, isGlobal, arg);
      // 用户自定义
      case 'USER':
        return this.openUser(context, viewParam, deResParameters, parameters, container, isGlobal, arg);
      // 用户自定义2
      case 'USER2':
        return this.openUser(context, viewParam, deResParameters, parameters, container, isGlobal, arg);
      // 用户自定义3
      case 'USER3':
        return this.openUser(context, viewParam, deResParameters, parameters, container, isGlobal, arg);
      // 用户自定义4
      case 'USER4':
        return this.openUser(context, viewParam, deResParameters, parameters, container, isGlobal, arg);
      // 默认顶级容器分页
      default:
        return this.openIndexViewTab(context, viewParam, deResParameters, parameters, container, isGlobal, arg);
    }
  }

  /**
   * 用户自定义
   *
   * @param context 上下文
   * @param viewParam 视图参数
   * @param deResParameters
   * @param parameters
   * @param container 容器对象
   * @param isGlobal 是否为全局
   * @param arg 额外参数
   * @memberof AppViewFunc
   */
  public openUser(
    context: any,
    viewParam: any,
    deResParameters: any[],
    parameters: any[],
    container: any,
    isGlobal?: boolean,
    arg?: any,
  ) {
    App.getNoticeService().warning('用户自定未实现');
  }

  /**
   * 气泡打开
   *
   * @param context 上下文
   * @param viewParam 视图参数
   * @param appView 应用视图
   * @param container 容器对象
   * @param isGlobal 是否为全局
   * @param arg 额外参数
   * @memberof AppViewFunc
   */
  public openPopover(context: any, viewParam: any, appView: IPSAppView, container: any, isGlobal?: boolean, arg?: any) {
    // const view = {
    //     viewType: appView.viewType,
    //     title: appView.caption,
    //     height: appView.height,
    //     width: appView.width,
    //     placement: appView.openMode,
    // };
    // if (appView.modelPath) {
    //     Object.assign(context, { viewpath: appView.modelPath });
    // }
    // const appPopover = this.$apppopover.openPop({}, view, Util.deepCopy(context), viewParam);
    // appPopover.subscribe((result: any) => {
    //     LogUtil.log(result);
    // });
  }

  /**
   * 抽屉打开
   *
   * @param context 上下文
   * @param viewParam 视图参数
   * @param appView 应用视图
   * @param container 容器对象
   * @param isGlobal 是否为全局
   * @param arg 额外参数
   * @memberof AppViewFunc
   */
  public openDrawer(context: any, viewParam: any, appView: IPSAppView, container: any, isGlobal?: boolean, arg?: any) {
    // const view = {
    //     viewType: appView.viewType,
    //     title: appView.caption,
    //     height: appView.height,
    //     width: appView.width,
    //     placement: appView.openMode,
    // };
    // const appdrawer = this.$appdrawer.openDrawer(view, Util.getViewProps(context, viewParam), {
    //     viewModelData: appView,
    // });
    // appdrawer.subscribe((result: any) => {
    //     LogUtil.log(result);
    // });
  }

  /**
   * 独立程序打开
   *
   * @param context 上下文
   * @param viewParam 视图参数
   * @param deResParameters
   * @param parameters
   * @param container 容器对象
   * @param isGlobal 是否为全局
   * @param arg 额外参数
   * @memberof AppViewFunc
   */
  public openApp(
    context: any,
    viewParam: any,
    deResParameters: any[],
    parameters: any[],
    container: any,
    isGlobal?: boolean,
    arg?: any,
  ) {
    if (context && context.srfdynainstid) {
      Object.assign(viewParam, { srfdynainstid: context.srfdynainstid });
    }
    const routePath = ViewTool.buildUpRoutePath(context, deResParameters, parameters, [], viewParam, true);
    App.getOpenViewService().openPopupApp('./#' + routePath);
  }

  /**
   * 模态打开
   *
   * @param context 上下文
   * @param viewParam 视图参数
   * @param appView 应用视图
   * @param container 容器对象
   * @param isGlobal 是否为全局
   * @param arg 额外参数
   * @memberof AppViewFunc
   */
  public openModal(context: any, viewParam: any, appView: IPSAppView, container: any, isGlobal?: boolean, arg?: any) {
    const view = {
      viewType: appView.viewType,
      title: appView.caption,
      height: appView.height,
      width: appView.width,
    };
    if (appView.modelPath) {
      Object.assign(context, { viewpath: appView.modelPath });
    }
    const appmodal = App.getOpenViewService().openModal({ viewModel: appView });
    appmodal.subscribe((result: any) => {
      console.log(result);
    });
  }

  /**
   * 非模式弹出
   *
   * @param viewParam 视图参数
   * @param deResParameters
   * @param parameters
   * @param container 容器对象
   * @param isGlobal 是否为全局
   * @param arg 额外参数
   * @memberof AppViewFunc
   */
  public openPopup(
    viewParam: any,
    deResParameters: any[],
    parameters: any[],
    container: any,
    isGlobal?: boolean,
    arg?: any,
  ) {
    App.getNoticeService().warning('暂未实现非模式弹出');
  }

  /**
   * 顶级容器分页
   *
   * @param context 上下文
   * @param viewParam 视图参数
   * @param deResParameters
   * @param parameters
   * @param container 容器对象
   * @param isGlobal 是否为全局
   * @param arg 额外参数
   * @memberof AppViewFunc
   */
  public openIndexViewTab(
    context: any,
    viewParam: any,
    deResParameters: any[],
    parameters: any[],
    container: any,
    isGlobal?: boolean,
    arg?: any,
  ) {
    if (context && context.srfdynainstid) {
      Object.assign(viewParam, { srfdynainstid: context.srfdynainstid });
    }
    return ViewTool.buildUpRoutePath(context, deResParameters, parameters, [], viewParam, isGlobal);
  }
}
