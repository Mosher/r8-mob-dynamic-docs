import { IPSAppFunc } from '@ibiz/dynamic-model-api';
import { AppFuncBase } from './app-func-base';

/**
 * 自定义
 *
 * @class CustomFunc
 */
export class CustomFunc extends AppFuncBase {
  constructor() {
    super();
  }

  /**
   * 执行功能
   *
   * @param appFunc 应用功能
   * @param context 上下文
   * @param viewParam 视图参数
   * @param container 容器对象
   * @param isGlobal 是否为全局
   * @param arg 额外参数
   * @memberof CustomFunc
   */
  public executeFunc(appFunc: IPSAppFunc, context: any, viewParam: any, container: any, isGlobal?: boolean, arg?: any) {
    if (appFunc.id === 'Setting') {
      return '/setting';
    } else {
      App.getNoticeService().warning(`用户自定义 ${appFunc.name} 功能未实现`);
    }
  }
}
