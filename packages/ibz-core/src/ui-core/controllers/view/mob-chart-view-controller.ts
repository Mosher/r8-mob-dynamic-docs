import { IPSAppDEMobChartView } from '@ibiz/dynamic-model-api';
import { ModelTool } from '../../../util';
import { MobChartViewEngine } from '../../../engine';
import { IMobChartViewController } from '../../../interface';
import { AppDEMultiDataViewController } from './app-de-multi-data-view-controller';

export class MobChartViewController extends AppDEMultiDataViewController implements IMobChartViewController {
  /**
   * 视图实例
   *
   * @public
   * @type {IPSAppDEMobChartView}
   * @memberof MobChartViewController
   */
  // TODO IPSAppDEMobChartView接口需继承多数据
  public viewInstance!: any;

  /**
   * @description 视图引擎
   * @type {MobChartViewEngine}
   * @memberof MobChartViewController
   */
  public engine: MobChartViewEngine = new MobChartViewEngine();

  /**
   * @description 视图引擎初始化
   * @param {*} [opts={}] 初始化参数
   * @memberof MobChartViewController
   */
  public engineInit(opts: any = {}) {
    const appDataEntity = this.viewInstance.getPSAppDataEntity();
    this.engine.init({
      view: this,
      chart: this.ctrlRefsMap.get('chart'),
      searchform: this.ctrlRefsMap.get('searchform') || this.ctrlRefsMap.get('quicksearchform'),
      p2k: '0',
      isLoadDefault: this.isLoadDefault && this.viewInstance.loadDefault,
      keyPSDEField: appDataEntity?.codeName?.toLowerCase(),
      majorPSDEField: ModelTool.getAppEntityMajorField(appDataEntity)?.codeName?.toLowerCase(),
    });
  }

  /**
   * @description 视图基础数据初始化
   * @memberof MobChartViewController
   */
  public viewBasicInit() {
    super.viewBasicInit();
    // todo 图表xDataControlName模型获取不到
    this.xDataControlName = 'chart';
  }
}
