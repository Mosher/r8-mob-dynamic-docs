import { IPSAppDEMobCalendarView } from '@ibiz/dynamic-model-api';
import { ModelTool } from '../../../util';
import { MobCalendarViewEngine } from '../../../engine';
import { IMobCalendarViewController } from '../../../interface';
import { AppDEMultiDataViewController } from './app-de-multi-data-view-controller';

export class MobCalendarViewController extends AppDEMultiDataViewController implements IMobCalendarViewController {
  /**
   * 视图实例
   *
   * @public
   * @type {IPSAppDEMobMDView}
   * @memberof MobCalendarViewController
   */
  public viewInstance!: IPSAppDEMobCalendarView;

  /**
   * 视图引擎
   *
   * @public
   * @type {MobMDViewEngine}
   * @memberof MobCalendarViewController
   */
  public engine: MobCalendarViewEngine = new MobCalendarViewEngine();

  /**
   * 视图引擎初始化
   *
   * @public
   * @memberof MobCalendarViewController
   */
  public engineInit(opts: any = {}) {
    if (App.isPreviewMode()) {
      return;
    }
    const appDataEntity = this.viewInstance.getPSAppDataEntity();
    this.engine.init({
      view: this,
      calendar: this.ctrlRefsMap.get('calendar'),
      searchform: this.ctrlRefsMap.get('searchform') || this.ctrlRefsMap.get('quicksearchform'),
      p2k: '0',
      isLoadDefault: this.isLoadDefault && this.viewInstance.loadDefault,
      keyPSDEField: appDataEntity?.codeName?.toLowerCase(),
      majorPSDEField: ModelTool.getAppEntityMajorField(appDataEntity)?.codeName?.toLowerCase(),
    });
  }
}
