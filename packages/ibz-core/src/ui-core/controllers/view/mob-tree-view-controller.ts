import { ModelTool } from '../../../util';
import { MobTreeViewEngine } from '../../../engine';
import { IMobTreeViewController } from '../../../interface';
import { AppDEMultiDataViewController } from './app-de-multi-data-view-controller';

/**
 * 移动端树视图控制器
 *
 * @class MobTreeViewController
 * @extends AppDEMultiDataViewController
 * @implements IMobTreeViewController
 */
export class MobTreeViewController extends AppDEMultiDataViewController implements IMobTreeViewController {
  /**
   * @description 移动端树视图引擎对象
   * @type {MobTreeViewEngine}
   * @memberof MobTreeViewController
   */
  public engine: MobTreeViewEngine = new MobTreeViewEngine();

  /**
   * @description 移动端树视图引擎初始化
   * @param {*} [opts={}] 初始化参数
   * @memberof MobTreeViewController
   */
  public engineInit(opts: any = {}) {
    const appDataEntity = this.viewInstance.getPSAppDataEntity();
    this.engine.init({
      view: this,
      tree: this.ctrlRefsMap.get('tree'),
      searchform: this.ctrlRefsMap.get('searchform') || this.ctrlRefsMap.get('quicksearchform'),
      p2k: '0',
      isLoadDefault: this.viewInstance.loadDefault,
      keyPSDEField: appDataEntity?.codeName?.toLowerCase(),
      majorPSDEField: ModelTool.getAppEntityMajorField(appDataEntity)?.codeName?.toLowerCase(),
    });
  }
}
