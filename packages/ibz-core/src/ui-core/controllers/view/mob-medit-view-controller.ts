import { IPSAppDEMobMEditView } from '@ibiz/dynamic-model-api';
import { ModelTool } from '../../../util';
import { MobMEditView9Engine } from '../../../engine';
import { IMobMEditViewController } from '../../../interface';
import { AppDEMultiDataViewController } from './app-de-multi-data-view-controller';

export class MobMEditViewController extends AppDEMultiDataViewController implements IMobMEditViewController {
  /**
   * 视图实例
   *
   * @public
   * @type {IPSAppDEMobMEditView}
   * @memberof MobMEditViewController
   */
  public viewInstance!: IPSAppDEMobMEditView;

  /**
   * 视图引擎
   *
   * @public
   * @type {MobMDViewEngine}
   * @memberof MobMEditViewController
   */
  public engine: MobMEditView9Engine = new MobMEditView9Engine();

  /**
   * 视图引擎初始化
   *
   * @public
   * @memberof MobMEditViewController
   */
  public engineInit(opts: any = {}) {
    const appDataEntity = this.viewInstance.getPSAppDataEntity();
    this.engine.init({
      view: this,
      meditviewpanel: this.ctrlRefsMap.get('meditviewpanel'),
      p2k: '0',
      isLoadDefault: this.isLoadDefault && this.viewInstance.loadDefault,
      keyPSDEField: appDataEntity?.codeName?.toLowerCase(),
      majorPSDEField: ModelTool.getAppEntityMajorField(appDataEntity)?.codeName?.toLowerCase(),
    });
  }
}
