import { IPSAppDataEntity, IPSAppDEField, IPSAppDEMobCalendarExplorerView } from '@ibiz/dynamic-model-api';
import { MobCalendarExpViewEngine } from '../../../engine';
import { AppDEExpViewController } from './app-de-exp-view-controller';
import { IMobCalendarExpViewController } from '../../../interface';
import { ModelTool } from '../../../util';

export class MobCalendarExpViewController extends AppDEExpViewController implements IMobCalendarExpViewController{

  /**
   * 移动端日历导航视图实例
   *
   * @public
   * @type { IPSAppDEMobCalendarExpView }
   * @memberof MobCalendarExpViewController
   */
  public viewInstance!: IPSAppDEMobCalendarExplorerView;

  /**
   * @description 移动端地图导航视图引擎实例对象
   * @type {MobCalendarExpViewEngine}
   * @memberof MobCalendarExpViewController
   */
   public engine: MobCalendarExpViewEngine = new MobCalendarExpViewEngine();

   /**
    * @description 引擎初始化
    * @memberof MobChartExpViewController
    */
   public engineInit() {
     this.engine.init({
       view: this,
       p2k: '0',
       calendarexpbar: this.ctrlRefsMap.get('calendarexpbar'),
       keyPSDEField: (ModelTool.getViewAppEntityCodeName(this.viewInstance) as string).toLowerCase(),
       majorPSDEField: (ModelTool.getAppEntityMajorField(this.viewInstance.getPSAppDataEntity() as IPSAppDataEntity) as IPSAppDEField)?.codeName.toLowerCase(),
       isLoadDefault: this.isLoadDefault && this.viewInstance.loadDefault,
     })
   }
}
