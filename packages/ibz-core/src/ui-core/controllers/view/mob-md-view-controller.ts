import { IPSAppDEMobMDView } from '@ibiz/dynamic-model-api';
import { ModelTool } from '../../../util';
import { MobMDViewEngine } from '../../../engine';
import { IMobMDViewController } from '../../../interface';
import { AppDEMultiDataViewController } from './app-de-multi-data-view-controller';

export class MobMDViewController extends AppDEMultiDataViewController implements IMobMDViewController {
  /**
   * @description 多数据视图实例
   * @type {IPSAppDEMobMDView}
   * @memberof MobMDViewController
   */
  public viewInstance!: IPSAppDEMobMDView;

  /**
   * @description 视图引擎
   * @type {MobMDViewEngine}
   * @memberof MobMDViewController
   */
  public engine: MobMDViewEngine = new MobMDViewEngine();

  /**
   * @description 视图引擎初始化
   * @param {*} [opts={}] 初始化参数
   * @memberof MobMDViewController
   */
  public engineInit(opts: any = {}) {
    if (App.isPreviewMode()) {
      return;
    }
    const appDataEntity = this.viewInstance.getPSAppDataEntity();
    this.engine.init({
      view: this,
      mdctrl: this.ctrlRefsMap.get('mdctrl'),
      searchform: this.ctrlRefsMap.get('searchform') || this.ctrlRefsMap.get('quicksearchform'),
      p2k: '0',
      isLoadDefault: this.isLoadDefault && this.viewInstance.loadDefault,
      keyPSDEField: appDataEntity?.codeName?.toLowerCase(),
      majorPSDEField: ModelTool.getAppEntityMajorField(appDataEntity)?.codeName?.toLowerCase(),
    });
  }
}
