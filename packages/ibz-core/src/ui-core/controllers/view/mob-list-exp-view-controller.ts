import { IPSAppDataEntity, IPSAppDEField, IPSAppDEMobListExplorerView } from '@ibiz/dynamic-model-api';
import { ModelTool } from '../../../util';
import { MobListExpViewEngine } from '../../../engine';
import { IMobListExpViewController } from '../../../interface';
import { AppDEExpViewController } from './app-de-exp-view-controller';

export class MobListExpViewController extends AppDEExpViewController implements IMobListExpViewController{

  /**
   * 移动端列表导航视图实例
   *
   * @public
   * @type { IPSAppDEMobListExpView }
   * @memberof MobListExpViewController
   */
  public viewInstance!: IPSAppDEMobListExplorerView;

  /**
   * @description 移动端列表导航视图引擎实例对象
   * @type {MobListExpViewEngine}
   * @memberof MobListExpViewController
   */
  public engine: MobListExpViewEngine = new MobListExpViewEngine();

  /**
   * @description 引擎初始化
   * @memberof MobListExpViewController
   */
  public engineInit() {
    this.engine.init({
      view: this,
      p2k: '0',
      listexpbar: this.ctrlRefsMap.get('listexpbar'),
      keyPSDEField: (ModelTool.getViewAppEntityCodeName(this.viewInstance) as string).toLowerCase(),
      majorPSDEField: (ModelTool.getAppEntityMajorField(this.viewInstance.getPSAppDataEntity() as IPSAppDataEntity) as IPSAppDEField)?.codeName.toLowerCase(),
      isLoadDefault: this.isLoadDefault && this.viewInstance.loadDefault,
    })
  }

}
