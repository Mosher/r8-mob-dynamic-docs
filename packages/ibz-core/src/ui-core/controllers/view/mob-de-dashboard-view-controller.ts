import { IPSAppDEMobDashboardView } from '@ibiz/dynamic-model-api';
import { ModelTool } from '../../../util';
import { MobDEDashboardViewEngine } from '../../../engine';
import { IMobDEDashboardViewController } from '../../../interface';
import { AppDEViewController } from './app-de-view-controller';
/**
 * 移动端实体数据看板视图控制器
 *
 * @class MobDEDashboardViewController
 * @extends AppDEViewController
 * @implements IMobDEDashboardViewController
 */
export class MobDEDashboardViewController extends AppDEViewController implements IMobDEDashboardViewController {
  /**
   * 移动端实体数据看板视图视图实例对象
   *
   * @type {IPSAppDEMobDashboardView}
   * @memberof MobDEDashboardViewController
   */
  viewInstance!: IPSAppDEMobDashboardView;

  /**
   * 移动端实体数据看板视图视图引擎
   *
   * @type {MobDEDashboardViewEngine}
   * @memberof MobDEDashboardViewController
   */
  public engine: MobDEDashboardViewEngine = new MobDEDashboardViewEngine();

  /**
   * 引擎初始化
   *
   * @memberof MobDEDashboardViewController
   */
  public engineInit() {
    if (App.isPreviewMode()) {
      return;
    }
    const appDataEntity = this.viewInstance.getPSAppDataEntity();
    this.engine.init({
      view: this,
      dashboard: this.ctrlRefsMap.get('dashboard'),
      searchform: this.ctrlRefsMap.get('searchform') || this.ctrlRefsMap.get('quicksearchform'),
      p2k: '0',
      isLoadDefault: this.isLoadDefault && this.viewInstance.loadDefault,
      keyPSDEField: appDataEntity?.codeName?.toLowerCase(),
      majorPSDEField: ModelTool.getAppEntityMajorField(appDataEntity)?.codeName?.toLowerCase(),
    });
  }
}
