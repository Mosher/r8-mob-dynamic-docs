import { IPSAppDEMobTabExplorerView } from '@ibiz/dynamic-model-api';
import { ModelTool } from '../../../util';
import { MobTabExpViewEngine } from '../../../engine';
import { IMobTabExpViewController } from '../../../interface';
import { AppDEViewController } from '.';

export class MobTabExpViewController extends AppDEViewController implements IMobTabExpViewController {
  /**
   * 视图实例
   *
   * @public
   * @type {IPSAppDEMobTabExplorerView}
   * @memberof MobTabExpViewController
   */
  public viewInstance!: IPSAppDEMobTabExplorerView;

  /**
   * 视图引擎
   *
   * @public
   * @type {MobTabExpViewEngine}
   * @memberof MobTabExpViewController
   */
  public engine: MobTabExpViewEngine = new MobTabExpViewEngine();

  /**
   * 视图引擎初始化
   *
   * @public
   * @memberof MobTabExpViewController
   */
  public engineInit(opts: any = {}) {
    const appDataEntity = this.viewInstance.getPSAppDataEntity();
    const engineOpts = {
      view: this,
      tabexppanel: this.ctrlRefsMap.get('tabexppanel'),
      p2k: '0',
      isLoadDefault: this.isLoadDefault && this.viewInstance.loadDefault,
      keyPSDEField: appDataEntity?.codeName?.toLowerCase(),
      majorPSDEField: ModelTool.getAppEntityMajorField(appDataEntity)?.codeName?.toLowerCase(),
    };
    Object.assign(engineOpts, opts);
    this.engine.init(engineOpts);
  }
}
