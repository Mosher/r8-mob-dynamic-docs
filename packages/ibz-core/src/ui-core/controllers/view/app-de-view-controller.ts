import { IPSAppDEView, IPSAppDEXDataView } from '@ibiz/dynamic-model-api';
import { IAppDEViewController, IEntityService, IUIService } from '../../../interface';
import { AppViewControllerBase } from './app-view-controller-base';

export class AppDEViewController extends AppViewControllerBase implements IAppDEViewController {
  /**
   * @description 视图模型对象
   * @type {IPSAppDEView}
   * @memberof AppDEViewController
   */
  public viewInstance!: IPSAppDEView;

  /**
   * @description 实体UI服务
   * @type {IUIService}
   * @memberof AppDEViewController
   */
  public appUIService!: IUIService;

  /**
   * @description 实体服务对象
   * @type {*}
   * @memberof AppViewControllerBase
   */
  public appDataService!: IEntityService;

  /**
   * @description 视图实体代码标识
   * @readonly
   * @type {string}
   * @memberof AppDEViewController
   */
  get appDeCodeName(): string {
    const appDataEntity = this.viewInstance.getPSAppDataEntity?.();
    return appDataEntity?.codeName || '';
  }

  /**
   * @description 视图基础数据初始化
   * @memberof AppDEViewController
   */
  public viewBasicInit() {
    super.viewBasicInit();
    this.xDataControlName = (this.viewInstance as IPSAppDEXDataView)?.xDataControlName;
  }

  /**
   * @description 初始化实体服务对象
   * @memberof AppDEViewController
   */
  public async initAppDataService() {
    if (App.isPreviewMode()) {
      return;
    }    
    const appDataEntity = this.viewInstance?.getPSAppDataEntity?.();
    if (appDataEntity && appDataEntity.codeName) {
      this.appDataService = await App.getEntityService().getService(this.context, appDataEntity.codeName.toLowerCase());
    }
  }

  /**
   * @description 初始化应用界面服务
   * @memberof AppDEViewController
   */
  public async initAppUIService() {
    if (App.isPreviewMode()) {
      return;
    }    
    const entity = this.viewInstance?.getPSAppDataEntity?.();
    if (entity && entity.codeName) {
      this.appUIService = await App.getUIService().getService(this.context, entity.codeName.toLowerCase());
    }
  }
}
