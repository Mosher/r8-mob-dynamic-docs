import { IPSAppDataEntity, IPSAppDEField, IPSAppDEMobMapExplorerView } from '@ibiz/dynamic-model-api';
import { MobMapExpViewEngine } from '../../../engine';
import { AppDEExpViewController } from './app-de-exp-view-controller';
import { IMobMapExpViewController } from '../../../interface';
import { ModelTool } from '../../../util';

export class MobMapExpViewController extends AppDEExpViewController implements IMobMapExpViewController{

  /**
   * 移动端地图导航视图实例
   *
   * @public
   * @type { IPSAppDEMobMapExpView }
   * @memberof MobMapExpViewController
   */
  public viewInstance!: IPSAppDEMobMapExplorerView;

  /**
 * @description 移动端地图导航视图引擎实例对象
 * @type {MobChartExpViewEngine}
 * @memberof MobChartExpViewController
 */
   public engine: MobMapExpViewEngine = new MobMapExpViewEngine();

   /**
    * @description 引擎初始化
    * @memberof MobChartExpViewController
    */
   public engineInit() {
     this.engine.init({
       view: this,
       p2k: '0',
       mapexpbar: this.ctrlRefsMap.get('mapexpbar'),
       keyPSDEField: (ModelTool.getViewAppEntityCodeName(this.viewInstance) as string).toLowerCase(),
       majorPSDEField: (ModelTool.getAppEntityMajorField(this.viewInstance.getPSAppDataEntity() as IPSAppDataEntity) as IPSAppDEField)?.codeName.toLowerCase(),
       isLoadDefault: this.isLoadDefault && this.viewInstance.loadDefault,
     })
   }
}
