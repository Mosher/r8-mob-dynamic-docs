import { IPSAppDataEntity, IPSAppDEField, IPSAppDEMobTreeExplorerView } from '@ibiz/dynamic-model-api';
import { MobTreeExpViewEngine } from '../../../engine';
import { AppDEExpViewController } from './app-de-exp-view-controller';
import { IMobTreeExpViewController } from '../../../interface';
import { ModelTool } from '../../../util';

export class MobTreeExpViewController extends AppDEExpViewController implements IMobTreeExpViewController {

  /**
   * 移动端树导航视图实例
   *
   * @public
   * @type { IPSAppDEMobTreeExpView }
   * @memberof MobTreeExpViewController
   */
  public viewInstance!: IPSAppDEMobTreeExplorerView;

  /**
 * @description 移动端列表导航视图引擎实例对象
 * @type {MobChartExpViewEngine}
 * @memberof MobChartExpViewController
 */
  public engine: MobTreeExpViewEngine = new MobTreeExpViewEngine();

  /**
   * @description 引擎初始化
   * @memberof MobChartExpViewController
   */
  public engineInit() {
    this.engine.init({
      view: this,
      p2k: '0',
      treeexpbar: this.ctrlRefsMap.get('treeexpbar'),
      keyPSDEField: (ModelTool.getViewAppEntityCodeName(this.viewInstance) as string).toLowerCase(),
      majorPSDEField: (ModelTool.getAppEntityMajorField(this.viewInstance.getPSAppDataEntity() as IPSAppDataEntity) as IPSAppDEField)?.codeName.toLowerCase(),
      isLoadDefault: this.isLoadDefault && this.viewInstance.loadDefault,
    })
  }
}
