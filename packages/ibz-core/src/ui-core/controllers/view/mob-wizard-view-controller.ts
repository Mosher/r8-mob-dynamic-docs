import { IPSAppDEMobWizardView } from '@ibiz/dynamic-model-api';
import { AppDEViewController } from './app-de-view-controller';
import { IMobWizardViewController } from '../../../interface';
import { MobWizardViewEngine } from '../../../engine';
import { ModelTool } from '../../../util';

/**
 * @description 移动端向导视图控制器
 * @export
 * @class MobWizardViewController
 * @extends {AppDEViewController}
 * @implements {IMobWizardViewController}
 */
export class MobWizardViewController extends AppDEViewController implements IMobWizardViewController{

  /**
   * 移动端向导视图实例
   *
   * @public
   * @type { IPSAppDEMobWizardView }
   * @memberof MobWizardViewController
   */
  public viewInstance!: IPSAppDEMobWizardView;

  /**
   * @description 移动端向导视图引擎
   * @type {MobWizardViewEngine}
   * @memberof MobWizardViewController
   */
  public engine: MobWizardViewEngine = new MobWizardViewEngine();

  /**
   * @description 引擎初始化
   * @memberof MobWizardViewController
   */
  public engineInit() {
    const appDataEntity = this.viewInstance.getPSAppDataEntity();
    this.engine.init({
      view: this,
      wizardpanel: this.ctrlRefsMap.get('wizardpanel'),
      p2k: '0',
      isLoadDefault: this.isLoadDefault,
      keyPSDEField: appDataEntity?.codeName?.toLowerCase(),
      majorPSDEField: ModelTool.getAppEntityMajorField(appDataEntity)?.codeName?.toLowerCase(),
    });
  }

}
