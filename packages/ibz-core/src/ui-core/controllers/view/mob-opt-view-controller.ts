import { IPSAppDEMobEditView } from '@ibiz/dynamic-model-api';
import { ModelTool } from '../../../util';
import { MobOptionViewEngine } from '../../../engine';
import { AppDEViewController } from './app-de-view-controller';
import { AppViewEvents, IMobEditViewController } from '../../../interface';

export class MobOptViewController extends AppDEViewController implements IMobEditViewController {

  /**
   * 移动端选项操作视图实例
   *
   * @public
   * @type { IPSAppDEMobOptView }
   * @memberof MobOptViewController
   */
  public viewInstance!: IPSAppDEMobEditView;

  /**
   * 视图引擎
   *
   * @public
   * @type {Engine}
   * @memberof MobOptViewController
   */
  public engine: MobOptionViewEngine = new MobOptionViewEngine();

  /**
   * 视图引擎初始化
   *
   * @public
   * @memberof MobOptViewController
   */
  public engineInit(opts: any = {}) {
    const appDataEntity = this.viewInstance.getPSAppDataEntity();
    this.engine.init({
      view: this,
      form: this.ctrlRefsMap.get('form'),
      p2k: '0',
      isLoadDefault: this.isLoadDefault && this.viewInstance.loadDefault,
      keyPSDEField: appDataEntity?.codeName?.toLowerCase(),
      majorPSDEField: ModelTool.getAppEntityMajorField(appDataEntity)?.codeName?.toLowerCase(),
    });
  }

  /**
   * @description 视图选中数据
   * @type {any[]}
   * @memberof MobOptViewController
   */
  public viewSelections: any[] = [];

  /**
   * @description 确定
   * @memberof MobOptViewController
   */
  public async ok(): Promise<void> {
    const xData: any = this.ctrlRefsMap.get(this.xDataControlName);
    if (xData.save && xData.save instanceof Function) {
      xData.save().then((res: any) => {
        if (res.ret == true) {
          this.viewEvent(AppViewEvents.CLOSE, res.data);
          this.closeView(res.data);
        }
      });
    }
  }

  /**
   * @description 取消
   * @memberof MobOptViewController
   */
  public cancel(): void {
    this.closeView();
  }

}
