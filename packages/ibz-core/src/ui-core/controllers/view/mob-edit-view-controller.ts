import { IPSAppDEMobEditView } from '@ibiz/dynamic-model-api';
import { ModelTool } from '../../../util';
import { MobEditViewEngine } from '../../../engine';
import { AppDEViewController } from './app-de-view-controller';
import { IMobEditViewController } from '../../../interface';

export class MobEditViewController extends AppDEViewController implements IMobEditViewController {
  /**
   * 视图实例
   *
   * @public
   * @type {IPSAppDEMobEditView}
   * @memberof MobEditViewController
   */
  public viewInstance!: IPSAppDEMobEditView;

  /**
   * 视图引擎
   *
   * @public
   * @type {MobEditViewEngine}
   * @memberof MobEditViewController
   */
  public engine: MobEditViewEngine = new MobEditViewEngine();

  /**
   * 视图引擎初始化
   *
   * @public
   * @memberof MobEditViewController
   */
  public engineInit(opts: any = {}) {
    if (App.isPreviewMode()) {
      return;
    }
    const appDataEntity = this.viewInstance.getPSAppDataEntity();
    this.engine.init({
      view: this,
      form: this.ctrlRefsMap.get('form'),
      p2k: '0',
      isLoadDefault: this.isLoadDefault && this.viewInstance.loadDefault,
      keyPSDEField: appDataEntity?.codeName?.toLowerCase(),
      majorPSDEField: ModelTool.getAppEntityMajorField(appDataEntity)?.codeName?.toLowerCase(),
    });
  }
}
