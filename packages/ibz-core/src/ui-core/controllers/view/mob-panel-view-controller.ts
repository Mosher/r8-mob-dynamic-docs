import { IPSAppDEMobPanelView } from '@ibiz/dynamic-model-api';
import { IMobPanelViewController, IViewStateParam } from '../../../interface';
import { AppDEViewController } from './app-de-view-controller';
import { ModelTool } from '../../../util';

export class MobPanelViewController extends AppDEViewController implements IMobPanelViewController {
  /**
   * @description 面板视图实例
   * @type {IPSAppDEMobPanelView}
   * @memberof MobPanelViewController
   */
  public viewInstance!: IPSAppDEMobPanelView;

  /**
   * @description 视图基础数据初始化
   * @memberof MobPanelViewController
   */
  public viewBasicInit() {
    super.viewBasicInit();
    if (this.viewState) {
      this.viewStateEvent = this.viewState.subscribe(({ tag, action, data }: IViewStateParam) => {
        if (!Object.is(tag, this.viewInstance.codeName)) {
          return;
        }
        if (Object.is(action, 'load')) {
          const controller = ModelTool.findPSControlByType('PANEL', this.viewInstance.getPSControls());
          this.viewState.next({ tag: controller?.name, action: 'load', data: data});
        }
      });
    }
  }

  /**
   * @description 视图刷新
   * @param {*} [arg]
   * @memberof MobPanelViewController
   */
  public refresh(arg?: any): void {
    this.viewState.next({ tag: this.ctrlRefsMap.get('panel')?.name, action: 'refresh', });
    this.hooks.refresh.callSync({ arg: arg });
  }
}
