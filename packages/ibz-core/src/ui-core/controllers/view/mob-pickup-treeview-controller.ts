import { MobPickupTreeViewEngine } from '../../../engine';
import { IMobPickupTreeViewController } from '../../../interface';
import { MobTreeViewController } from './mob-tree-view-controller';

/**
 * 移动端选择树视图控制器
 *
 * @exports
 * @class MobPickupTreeViewController
 * @extends MobTreeViewController
 * @implements IMobPickupTreeViewController
 */
export class MobPickupTreeViewController extends MobTreeViewController implements IMobPickupTreeViewController {
  /**
   * @description 视图引擎
   * @type {MobPickupTreeViewEngine}
   * @memberof MobPickupTreeViewController
   */
  public engine: MobPickupTreeViewEngine = new MobPickupTreeViewEngine();
}
