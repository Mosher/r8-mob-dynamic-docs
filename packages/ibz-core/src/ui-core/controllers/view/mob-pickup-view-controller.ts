import { IPSAppDEMobPickupView } from '@ibiz/dynamic-model-api';
import { ModelTool } from '../../../util';
import { MobPickupViewEngine } from '../../../engine';
import { IMobPickUpViewController, AppViewEvents } from '../../../interface';
import { AppDEViewController } from './app-de-view-controller';

export class MobPickUpViewController extends AppDEViewController implements IMobPickUpViewController {
  /**
   * @description 数据选择视图实例
   * @type {IPSAppDEMobPickupView}
   * @memberof MobPickUpViewController
   */
  public viewInstance!: IPSAppDEMobPickupView;

  /**
   * @description 视图引擎
   * @type {MobPickupViewEngine}
   * @memberof MobPickUpViewController
   */
  public engine: MobPickupViewEngine = new MobPickupViewEngine();

  /**
   * @description 视图选中数据
   * @type {any[]}
   * @memberof MobPickUpViewController
   */
  public viewSelections: any[] = [];

  /**
   * @description 视图引擎初始化
   * @param {*} [opts={}] 初始化参数
   * @memberof MobPickUpViewController
   */
  public engineInit(opts: any = {}) {
    const appDataEntity = this.viewInstance.getPSAppDataEntity();
    this.engine.init({
      view: this,
      p2k: '0',
      isLoadDefault: this.isLoadDefault,
      pickupviewpanel: this.ctrlRefsMap.get('pickupviewpanel'),
      keyPSDEField: appDataEntity?.codeName?.toLowerCase(),
      majorPSDEField: ModelTool.getAppEntityMajorField(appDataEntity)?.codeName?.toLowerCase(),
    });
  }

  /**
   * @description 确定
   * @memberof MobPickUpViewController
   */
  public ok(): void {
    this.viewEvent(AppViewEvents.DATA_CHANGE, this.viewSelections);
    this.viewEvent(AppViewEvents.CLOSE, this.viewSelections);
  }

  /**
   * @description 取消
   * @memberof MobPickUpViewController
   */
  public cancel(): void {
    this.viewEvent(AppViewEvents.CLOSE, this.viewSelections);
  }
}
