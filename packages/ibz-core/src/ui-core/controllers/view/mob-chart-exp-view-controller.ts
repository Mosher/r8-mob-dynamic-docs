import { IPSAppDataEntity, IPSAppDEField, IPSAppDEMobChartExplorerView } from '@ibiz/dynamic-model-api';
import { ModelTool } from '../../../util';
import { IMobChartExpViewController } from '../../../interface';
import { AppDEExpViewController } from './app-de-exp-view-controller';
import { MobChartExpViewEngine } from '../../../engine';

export class MobChartExpViewController extends AppDEExpViewController implements IMobChartExpViewController {

  /**
   * 移动端图表导航视图实例
   *
   * @public
   * @type { IPSAppDEMobChartExpView }
   * @memberof MobChartExpViewController
   */
  public viewInstance!: IPSAppDEMobChartExplorerView

  /**
 * @description 移动端图表导航视图引擎实例对象
 * @type {MobChartExpViewEngine}
 * @memberof MobChartExpViewController
 */
  public engine: MobChartExpViewEngine = new MobChartExpViewEngine();

  /**
   * @description 引擎初始化
   * @memberof MobChartExpViewController
   */
  public engineInit() {
    this.engine.init({
      view: this,
      p2k: '0',
      chartexpbar: this.ctrlRefsMap.get('chartexpbar'),
      keyPSDEField: (ModelTool.getViewAppEntityCodeName(this.viewInstance) as string).toLowerCase(),
      majorPSDEField: (ModelTool.getAppEntityMajorField(this.viewInstance.getPSAppDataEntity() as IPSAppDataEntity) as IPSAppDEField)?.codeName.toLowerCase(),
      isLoadDefault: this.isLoadDefault && this.viewInstance.loadDefault,
    })
  }
}
