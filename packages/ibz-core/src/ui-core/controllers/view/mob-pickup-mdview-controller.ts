import { MobPickUpMDViewEngine } from '../../../engine';
import { IMobPickUpMDViewController } from '../../../interface';
import { MobMDViewController } from './mob-md-view-controller';

export class MobPickUpMDViewController extends MobMDViewController implements IMobPickUpMDViewController {
  /**
   * @description 视图引擎
   * @type {MobPickUpMDViewEngine}
   * @memberof MobPickUpMDViewController
   */
  public engine: MobPickUpMDViewEngine = new MobPickUpMDViewEngine();
}
