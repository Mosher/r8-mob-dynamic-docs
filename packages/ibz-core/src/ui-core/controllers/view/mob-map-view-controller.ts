import { IPSAppDEMobMapView } from '@ibiz/dynamic-model-api';
import { ModelTool } from '../../../util';
import { MobMapViewEngine } from '../../../engine';
import { IMobMapViewController } from '../../../interface';
import { AppDEMultiDataViewController } from '.';

export class MobMapViewController extends AppDEMultiDataViewController implements IMobMapViewController {
  /**
   * 视图实例
   *
   * @public
   * @type {IPSAppDEMobMapView}
   * @memberof MobMapViewController
   */
  public viewInstance!: IPSAppDEMobMapView;

  /**
   * 视图引擎
   *
   * @public
   * @type {MobMapViewEngine}
   * @memberof MobMapViewController
   */
  public engine: MobMapViewEngine = new MobMapViewEngine();

  /**
   * 视图引擎初始化
   *
   * @public
   * @memberof MobMapViewController
   */
  public engineInit(opts: any = {}) {
    if (App.isPreviewMode()) {
      return;
    }
    const appDataEntity = this.viewInstance.getPSAppDataEntity();
    const engineOpts = {
      view: this,
      map: this.ctrlRefsMap.get('map'),
      p2k: '0',
      isLoadDefault: this.isLoadDefault && this.viewInstance.loadDefault,
      keyPSDEField: appDataEntity?.codeName?.toLowerCase(),
      majorPSDEField: ModelTool.getAppEntityMajorField(appDataEntity)?.codeName?.toLowerCase(),
    };
    Object.assign(engineOpts, opts);
    this.engine.init(engineOpts);
  }
}
