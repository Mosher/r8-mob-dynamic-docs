import { IMobDatePickerEditorController } from '../../../interface';
import { EditorControllerBase } from './editor-controller-base';

/**
 * 日期选择器控制器
 *
 * @export
 * @class MobDatePickerEditorController
 * @extends {EditorControllerBase}
 */
export class MobDatePickerEditorController extends EditorControllerBase implements IMobDatePickerEditorController {
  /**
   * @description 设置编辑器的自定义参数
   * @memberof MobDatePickerEditorController
   */
  public async setCustomProps() {
    await super.setCustomProps();
    const type = this.editorInstance.editorStyle;
    switch (type) {
      // 只有小时分钟
      case 'Auto3':
        Object.assign(this.customProps, {
          displayFormat: 'HH:mm',
        });
        break;
      // 精确分钟
      case 'Auto1':
        Object.assign(this.customProps, {
          displayFormat: 'YYYY-MM-DD HH:mm',
        });
        break;
      // 精确天
      case 'Auto6':
        Object.assign(this.customProps, {
          displayFormat: 'YYYY-MM-DD',
        });
        break;
      // 精确小时
      case 'Auto8':
        Object.assign(this.customProps, {
          displayFormat: 'YYYY-MM-DD HH',
        });
        break;
      default:
        Object.assign(this.customProps, {
          displayFormat: 'YYYY-MM-DD HH:mm:ss',
        });
        break;
    }
  }
}
