import { IMobTextboxEditorController } from '../../../interface';
import { EditorControllerBase } from './editor-controller-base';

export class MobTextboxEditorController extends EditorControllerBase implements IMobTextboxEditorController {
  /**
   * @description 设置编辑器的自定义参数
   * @memberof MobTextboxEditorController
   */
  public async setCustomProps() {
    await super.setCustomProps();
    const type = this.editorInstance.editorType;
    Object.assign(this.customProps, {
      showMaxLength: this.editorInstance.editorParams?.['showMaxLength'] ? 
      JSON.parse(this.editorInstance.editorParams['showMaxLength'] as string) : false,
    })
    switch (type) {
      case 'MOBNUMBER':
        Object.assign(this.customProps, {
          type: 'number',
        });
        break;
      case 'MOBPASSWORD':
        Object.assign(this.customProps, {
          type: 'password',
        });
        break;
      default:
        Object.assign(this.customProps, {
          type: 'text',
        });
        break;
    }
  }
}
