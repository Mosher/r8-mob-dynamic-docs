import { IPSCodeListEditor } from '@ibiz/dynamic-model-api';
import { IMobDropdownListController } from '../../../interface';
import { EditorControllerBase } from './editor-controller-base';

export class MobDropdownListController extends EditorControllerBase implements IMobDropdownListController {
  /**
   * @description 设置编辑器的自定义参数
   * @memberof MobDropdownListController
   */
  public async setCustomProps() {
    await super.setCustomProps();
    const type = this.editorInstance.editorType;
    const codeList = (this.editorInstance as IPSCodeListEditor)?.getPSAppCodeList?.();
    this.customProps.context = this.context;
    this.customProps.viewParam = this.viewParam;
    this.customProps.contextData = this.contextData;
    if (codeList) {
      Object.assign(this.customProps, {
        tag: codeList.codeName,
        codeListType: codeList.codeListType,
      });
    }
    if (Object.is('MOBCHECKLIST', type)) {
      Object.assign(this.customProps, {
        multiple: true,
      });
    }
  }
}
