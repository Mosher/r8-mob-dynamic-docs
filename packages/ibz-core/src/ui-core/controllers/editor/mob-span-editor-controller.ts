import { IPSAppDEField, IPSCodeListEditor } from '@ibiz/dynamic-model-api';
import { IMobSpanEditorController } from '../../../interface';
import { EditorControllerBase } from './editor-controller-base';

export class MobSpanEditorController extends EditorControllerBase implements IMobSpanEditorController {

  /**
   * @description 设置编辑器的自定义参数
   * @memberof MobSpanEditorController
   */
  public async setCustomProps() {
    await super.setCustomProps();
    this.initCodeListParams();
    this.initFormatParams();
  }

  /**
   * @description 初始化代码表参数
   * @memberof MobSpanEditorController
   */
  public initCodeListParams() {
    const codeList = (this.editorInstance as IPSCodeListEditor)?.getPSAppCodeList?.();
    if (codeList) {
      Object.assign(this.customProps, {
        tag: codeList.codeName,
        codeListType: codeList.codeListType,
      });
    }
  }

  /**
   * @description 初始化值格式化参数
   * @memberof MobSpanEditorController
   */
  public initFormatParams() {
    const appDeField: IPSAppDEField = this.parentItem?.getPSAppDEField?.();
    if (appDeField?.valueFormat) {
      this.customProps.valueFormat = appDeField?.valueFormat;
    }
    if (this.parentItem?.valueFormat) {
      this.customProps.valueFormat = this.parentItem.valueFormat;
    }
    if (this.editorInstance.editorParams?.valueFormat) {
      this.customProps.valueFormat = this.editorInstance.editorParams?.valueFormat;
    }
  }
}
