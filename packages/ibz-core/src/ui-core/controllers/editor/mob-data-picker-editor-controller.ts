import { IMobDataPickerEditorController } from '../../../interface';
import { EditorControllerBase } from './editor-controller-base';
import { ModelTool } from '../../../util';
import { IPSPicker, IPSPickerEditor } from '@ibiz/dynamic-model-api';
export class MobDataPickerEditorController extends EditorControllerBase implements IMobDataPickerEditorController {
  /**
   * @description 设置编辑器的自定义参数
   * @memberof MobDataPickerEditorController
   */
  public async setCustomProps() {
    await super.setCustomProps();
    this.customProps.deMajorField = ModelTool.getEditorMajorName(this.editorInstance);
    this.customProps.deKeyField = ModelTool.getEditorKeyName(this.editorInstance);
    this.customProps.context = this.context;
    this.customProps.viewParam = this.viewParam;
    this.customProps.contextData = this.contextData;
    this.customProps.valueItem = this.parentItem?.valueItemName;
    this.customProps.name = this.editorInstance.name;
    this.customProps.pickUpView = await (this.editorInstance as IPSPickerEditor).getPickupPSAppView();
    this.customProps.linkView = await (this.editorInstance as IPSPicker).getLinkPSAppView();
    if (!App.isPreviewMode()) {
      this.customProps.pickUpView?.fill(true);
      this.customProps.linkView?.fill(true);
    }
  }

  /**
   * @description 改变编辑器的值
   * @param {*} arg
   * @memberof MobDataPickerEditorController
   */
  public changeValue(arg: any) {
    this.hooks.valueChange.callSync({
      arg: {
        name: arg.name,
        value: arg.value,
      },
    });
  }
}
