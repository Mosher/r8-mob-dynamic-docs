import { IPSCodeListEditor } from '@ibiz/dynamic-model-api';
import { IMobCheckBoxEditorController } from '../../../interface';
import { EditorControllerBase } from './editor-controller-base';

export class MobCheckBoxEditorController extends EditorControllerBase implements IMobCheckBoxEditorController {
  /**
   * @description 设置编辑器的自定义参数
   * @memberof MobCheckBoxEditorController
   */
  public async setCustomProps() {
    await super.setCustomProps();
    const codeList = (this.editorInstance as IPSCodeListEditor)?.getPSAppCodeList?.();
    if (codeList) {
      Object.assign(this.customProps, {
        codeList: codeList,
      });
    }
  }
}
