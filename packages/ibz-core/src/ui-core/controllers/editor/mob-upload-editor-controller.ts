import { EditorControllerBase } from './editor-controller-base';
import { IMobUploadEditorController } from '../../../interface';

export class MobUploadEditorController extends EditorControllerBase implements IMobUploadEditorController{
  /**
   * @description 设置编辑器的自定义参数
   * @memberof MobUploadEditorController
   */
  public async setCustomProps() {
    await super.setCustomProps();
    const type = this.editorInstance.editorType;
    switch (type) {
      case 'MOBSINGLEFILEUPLOAD':
        Object.assign(this.customProps, {
          uploadType: 'file',
          multiple: false,
          accept: this.editorInstance.editorParams?.['accept'] ?
            this.editorInstance.editorParams['accept'] : '*',
          limit: this.editorInstance.editorParams?.['limit'] ? 
            JSON.parse(this.editorInstance.editorParams['limit'] as string) : 1,
        });
        break;
      case 'MOBMULTIFILEUPLOAD':
        Object.assign(this.customProps, {
          uploadType: 'file',
          multiple: true,
          accept: this.editorInstance.editorParams?.['accept'] ?
            this.editorInstance.editorParams['accept'] : '*',
          limit: this.editorInstance.editorParams?.['limit'] ? 
            JSON.parse(this.editorInstance.editorParams['limit'] as string) : 9999,
        });
        break;
      case 'MOBPICTURE':
        Object.assign(this.customProps, {
          uploadType: 'picture',
          multiple: false,
          accept: this.editorInstance.editorParams?.['accept'] ?
            this.editorInstance.editorParams['accept'] : 'image/*',
          limit: this.editorInstance.editorParams?.['limit'] ? 
            JSON.parse(this.editorInstance.editorParams['limit'] as string) : 1,
        });
        break;
      case 'MOBPICTURELIST':
        Object.assign(this.customProps, {
          uploadType: 'picture',
          multiple: true,
          accept: this.editorInstance.editorParams?.['accept'] ?
            this.editorInstance.editorParams['accept'] : 'image/*',
          limit: this.editorInstance.editorParams?.['limit'] ? 
            JSON.parse(this.editorInstance.editorParams['limit'] as string) : 9999,
        });
        break;
      default:
        Object.assign(this.customProps, {
          uploadType: 'file',
          multiple: false,
          accept: this.editorInstance.editorParams?.['accept'] ?
            this.editorInstance.editorParams['accept'] : '*',
          limit: this.editorInstance.editorParams?.['limit'] ? 
            JSON.parse(this.editorInstance.editorParams['limit'] as string) : 1,
        });
        break;
    }
  } 
}
