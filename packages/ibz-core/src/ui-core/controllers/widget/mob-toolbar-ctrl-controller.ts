import { IPSDETBUIActionItem, IPSDEToolbar, IPSDEToolbarItem, IPSUIAction } from '@ibiz/dynamic-model-api';
import { IMobToolbarCtrlController, IViewStateParam } from '../../../interface';
import { AppCtrlControllerBase } from './app-ctrl-controller-base';

export class MobToolbarCtrlController extends AppCtrlControllerBase implements IMobToolbarCtrlController {
  /**
   * @description 部件模型实例对象
   * @type {IPSDEToolbar}
   * @memberof MobToolbarCtrlController
   */
  controlInstance!: IPSDEToolbar;

  /**
   * @description 工具栏模型
   * @type {IPSDEToolbarItem[]}
   * @memberof MobToolbarCtrlController
   */
  toolbarModels!: IPSDEToolbarItem[];

  /**
   * 构造MobToolbarCtrlController对象
   *
   * @public
   * @memberof MobToolbarCtrlController
   */
  constructor(opts: any) {
    super(opts);
  }

  /**
   * @description 部件基础数据初始化
   * @memberof AppCtrlControllerBase
   */
  public ctrlBasicInit() {
    super.ctrlBasicInit();
    this.toolbarModels = this.controlInstance.getPSDEToolbarItems() || [];
  }

  /**
   * @description 工具栏部件初始化
   * @memberof MobToolbarCtrlController
   */
  public ctrlInit() {
    super.ctrlInit();
    if (this.viewState) {
      this.viewStateEvent = this.viewState.subscribe(({ tag, action, data }: IViewStateParam) => {
        if (!Object.is(this.name, tag)) {
          return;
        }
        if (Object.is(action, 'permissionValid')) {
          this.calcToolbarItemAuthState(data);
        }
      });
    }
  }

  /**
   * @description 计算工具栏权限
   * @param {*} data
   * @memberof MobToolbarCtrlController
   */
  public calcToolbarItemAuthState(data: any) {
    if (this.toolbarModels.length > 0) {
      this.toolbarModels.forEach((item: any) => {
        if (Object.is(item.itemType, 'DEUIACTION')) {
          const uiaction: IPSUIAction | null = (item as IPSDETBUIActionItem).getPSUIAction();
          if (uiaction && uiaction.dataAccessAction) {
            let dataActionResult: any;
            if (uiaction && (Object.is(uiaction.actionTarget, 'NONE') || Object.is(uiaction.actionTarget, ''))) {
              if (
                Object.is(uiaction.actionTarget, '') &&
                Object.is(uiaction.uIActionTag, 'Save') &&
                this.appUIService.enableDEMainState()
              ) {
                if (data && Object.keys(data).length > 0) {
                  dataActionResult = this.appUIService.getAllOPPrivs(data, uiaction.dataAccessAction);
                }
              } else {
                dataActionResult = this.appUIService.getResourceOPPrivs(uiaction.dataAccessAction);
              }
            } else {
              if (data && Object.keys(data).length > 0 && this.appUIService.enableDEMainState()) {
                dataActionResult = this.appUIService.getAllOPPrivs(data, uiaction.dataAccessAction);
              }
            }
            if (dataActionResult) {
              item.noPrivHidden = false;
              item.noPrivDisabled = false;
            } else {
              // 禁用:1;隐藏:2;隐藏且默认隐藏:6
              if (item.noPrivDisplayMode === 1) {
                item.noPrivDisabled = true;
              }
              if (item.noPrivDisplayMode === 2 || item.noPrivDisplayMode === 6) {
                item.noPrivHidden = true;
              } else {
                item.noPrivHidden = false;
              }
            }
          }
        }
      });
    }
  }
}
