import { IPSAppDataEntity, IPSCalendarExpBar,IPSDERBase,IPSSysCalendar, IPSSysCalendarItem } from '@ibiz/dynamic-model-api';
import { AppExpBarCtrlEvents, IMobCalendarExpBarController, MobCalendarEvents } from '../../../interface';
import { AppExpBarCtrlController } from './app-exp-bar-ctrl-controller';
import { ModelTool, Util, ViewTool } from '../../../util';

export class MobCalendarExpBarController extends AppExpBarCtrlController implements IMobCalendarExpBarController {

  /**
   * 移动端日历导航部件实例
   *
   * @public
   * @type { IPSDEMobCalendarExpBar }
   * @memberof MobCalendarExpBarController
   */
  public controlInstance!: IPSCalendarExpBar;

  /**
   * @description 导航视图名称
   * @type {*}
   * @memberof MobCalendarExpBarController
   */
  public navViewName: any = {};

  /**
   * @description 导航过滤项
   * @type {*}
   * @memberof MobCalendarExpBarController
   */
  public navFilter: any = {};

  /**
   * @description 导航关系
   * @type {*}
   * @memberof MobCalendarExpBarController
   */
  public navPSDer: any = {};

  /**
   * @description 处理部件事件
   * @param {string} controlname 部件名称
   * @param {string} action 部件行为
   * @param {*} data 数据
   * @memberof MobCalendarExpBarController
   */
  public handleCtrlEvent(controlname: string, action: string, data: any) {
    if (controlname === this.xDataControlName && action == MobCalendarEvents.SELECT_CHANGE) {
      this.onSelectionChange(data);
    } else {
      super.handleCtrlEvent(controlname, action, data);
    }
  }

  /**
   * @description 处理多数据部件配置
   * @protected
   * @memberof MobCalendarExpBarController
   */
  protected async handleXDataOptions() {
    await super.handleXDataOptions();
    const calendarItems = (this.xDataControl as any).getPSSysCalendarItems();
    let navViewName = {};
    let navParam = {};
    let navFilter = {};
    let navPSDer = {};
    if (calendarItems && calendarItems.length > 0) {
        calendarItems.forEach((item: IPSSysCalendarItem) => {
            const viewName = {
                [item.itemType]: item.getNavPSAppView() ? item.getNavPSAppView()?.modelPath : "",
            };
            Object.assign(navViewName, viewName);
            const param = {
                [item.itemType]: {
                    navigateContext: this.initNavParam(item.getPSNavigateContexts()),
                    navigateParams: this.initNavParam(item.getPSNavigateParams()),
                }
            }
            Object.assign(navParam, param);
            const filter = {
                [item.itemType]: item.navFilter ? item.navFilter : "",
            }
            Object.assign(navFilter, filter);
            const psDer = {
                [item.itemType]: item.getNavPSDER() ? "n_" + (item.getNavPSDER() as IPSDERBase).minorCodeName?.toLowerCase() + "_eq" : "",
            }
            Object.assign(navPSDer, psDer);
        })
    }
    this.navViewName = navViewName;
    this.navParam = navParam;
    this.navFilter = navFilter;
    this.navPSDer = navPSDer;
  }

  /**
   * @description 初始化导航参数
   * @param {*} params 参数
   * @return {*} 
   * @memberof MobCalendarExpBarController
   */
  public initNavParam(params: any) {
    if (params && params.length > 0) {
        let navParams: any = {};
        params.forEach((param: any) => {
            const navParam = {
                [param.key]: param.rawValue ? param.value : "%" + param.value + "%",
            }
            Object.assign(navParams, navParam);
        });
        return navParams;
    } else {
        return null;
    }
  }
  
  /**
    * 日历部件的选中数据事件
    * 
    *
    * @param {any[]} args 选中数据
    * @return {*}  {void}
    * @memberof MapExpBarControlBase
    */
  public async onSelectionChange(args: any): Promise<void> {
    let tempContext: any = {};
    let tempViewParam: any = {};
    if (args.length === 0) {
      this.calcToolbarItemState(true);
      return;
    }
    const arg: any = args[0];
    if (this.context) {
      Object.assign(tempContext, Util.deepCopy(this.context));
    }
    const calendarItem: IPSSysCalendarItem | null | undefined = ((this.xDataControl as IPSSysCalendar).getPSSysCalendarItems() || []).find((item: IPSSysCalendarItem) => {
      return item.itemType === arg.itemType;
    });
    const calendarItemEntity: IPSAppDataEntity | null | undefined = calendarItem?.getPSAppDataEntity();
    if (calendarItem && calendarItemEntity) {
      Object.assign(tempContext, { [calendarItemEntity.codeName?.toLowerCase()]: arg[calendarItemEntity.codeName?.toLowerCase()] });
      Object.assign(tempContext, { srfparentdename: calendarItemEntity.codeName, srfparentdemapname: (calendarItemEntity as any)?.getPSDEName(), srfparentkey: arg[calendarItemEntity.codeName?.toLowerCase()] });
      if (this.navFilter && this.navFilter[arg.itemType] && !Object.is(this.navFilter[arg.itemType], "")) {
        Object.assign(tempViewParam, { [this.navFilter[arg.itemType]]: arg[calendarItemEntity.codeName?.toLowerCase()] });
      }
      if (this.navPSDer && this.navFilter[arg.itemType] && !Object.is(this.navPSDer[arg.itemType], "")) {
        Object.assign(tempViewParam, { [this.navPSDer[arg.itemType]]: arg[calendarItemEntity.codeName?.toLowerCase()] });
      }
      if (this.navParam && this.navParam[arg.itemType] && this.navParam[arg.itemType].navigateContext && Object.keys(this.navParam[arg.itemType].navigateContext).length > 0) {
        let _context: any = Util.computedNavData(arg.curData, tempContext, tempViewParam, this.navParam[arg.itemType].navigateContext);
        Object.assign(tempContext, _context);
      }
      if (this.navParam && this.navParam[arg.itemType] && this.navParam[arg.itemType].navigateParams && Object.keys(this.navParam[arg.itemType].navigateParams).length > 0) {
        let _params: any = Util.computedNavData(arg.curData, tempContext, tempViewParam, this.navParam[arg.itemType].navigateParams);
        Object.assign(tempViewParam, _params);
      }
      // 通过视图类型和样式获取视图组件
      const navView:any = calendarItem?.getNavPSAppView();
      if (navView) {
        if (this.ctrlShowMode == 'LEFT') {
          if (!navView?.isFill) {
            await navView?.fill(true);
          }          
          this.navView = navView.modelPath;
          this.navViewComponent = App.getComponentService().getViewTypeComponent(navView.viewType, navView.viewStyle, navView.getPSSysPFPlugin?.()?.pluginCode);
          // 组装selection,渲染导航视图必要参数
          this.selection = {};
          Object.assign(this.selection, {
            viewComponent: this.navViewComponent,
            navContext: tempContext,
            navParam: tempViewParam,
            navView: navView,
            viewPath: this.navView
          });
        } else{
          this.openExplorerView(navView, args, tempContext, tempViewParam);
        }
      }
    }
    this.calcToolbarItemState(false);
    this.ctrlEvent(AppExpBarCtrlEvents.SELECT_CHANGE, args);
  }
}
