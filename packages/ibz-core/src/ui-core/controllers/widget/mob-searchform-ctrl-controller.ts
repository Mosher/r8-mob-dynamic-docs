import {
  MobSearchFormEvents,
  IUIDataParam,
  IMobSearchFormCtrlController,
  IParam,
  ICtrlActionResult,
} from '../../../interface';
import { Util } from '../../../util';
import { MobFormCtrlController } from './mob-form-ctrl-controller';
import { IPSDESearchForm } from '@ibiz/dynamic-model-api';

/**
 * @description 搜索表单控制器
 * @export
 * @class MobSearchFormCtrlController
 * @extends {MobFormCtrlController}
 * @implements {IMobSearchFormCtrlController}
 */
export class MobSearchFormCtrlController extends MobFormCtrlController implements IMobSearchFormCtrlController {
  /**
   * @description 搜索表单模型实例对象
   * @type {IPSDESearchForm}
   * @memberof MobSearchFormCtrlController
   */
  public controlInstance!: IPSDESearchForm;

  /**
   * @description 是否展开搜索表单
   * @type {boolean}
   * @memberof MobSearchFormCtrlController
   */
  public isExpandSearchForm: boolean = false;

  /**
   * @description 默认展开搜索表单
   * @type {boolean}
   * @memberof MobSearchFormCtrlController
   */
  public expandSearchForm: boolean = false;

  /**
   * @description 初始化输入数据
   * @param {*} opts 输入参数
   * @memberof MobSearchFormCtrlController
   */
  public initInputData(opts: any) {
    super.initInputData(opts);
    this.expandSearchForm = opts.expandSearchForm;
  }

  /**
   * @description 获取多项数据
   * @return {*}  {any[]}
   * @memberof MobSearchFormCtrlController
   */
  public getDatas(): any[] {
    return [this.data];
  }

  /**
   * @description 加载草稿
   * @param {IParam} [opts] 行为参数
   * @param {IParam} [args] 补充逻辑完成参数
   * @param {boolean} [showInfo=true] 是否显示提示信息
   * @param {boolean} [loadding=true] 是否显示loading效果
   * @return {*}  {Promise<ICtrlActionResult>}
   * @memberof MobSearchFormCtrlController
   */
  public async loadDraft(
    opts?: IParam,
    args?: IParam,
    showInfo: boolean = true,
    loadding: boolean = true,
  ): Promise<ICtrlActionResult> {
    if (!this.loaddraftAction) {
      App.getNoticeService().error(`未配置loaddraftAction行为`);
      return Promise.resolve({ ret: false, data: null });
    }
    const arg: any = { ...opts };
    const viewparamResult: any = Object.assign(arg, this.viewParam);
    const tempContext: any = Util.deepCopy(this.context);
    const beforeUIDataParam: IUIDataParam = this.getUIDataParam();
    Object.assign(beforeUIDataParam, {
      action: this.loaddraftAction,
      navContext: tempContext,
      navParam: viewparamResult,
    });
    const beforeLoadDraftResult: any = await this.executeCtrlEventLogic(
      MobSearchFormEvents.BEFORE_LOAD_DRAFT,
      beforeUIDataParam,
    );
    if (beforeLoadDraftResult && beforeLoadDraftResult?.hasOwnProperty('srfret') && !beforeLoadDraftResult.srfret) {
      return Promise.resolve({ ret: false, data: null });
    }
    this.ctrlEvent(MobSearchFormEvents.BEFORE_LOAD_DRAFT, beforeUIDataParam);
    this.onControlRequset('loadDraft', tempContext, viewparamResult, loadding);
    const response = await this.ctrlService.loadDraft(
      this.loaddraftAction,
      tempContext,
      { viewparams: viewparamResult },
      loadding,
    );
    this.onControlResponse('loadDraft', response);
    if (response.status && response.status == 200) {
      const data = response.data;
      if (this.appDeCodeName?.toLowerCase() && data[this.appDeCodeName.toLowerCase()]) {
        Object.assign(this.context, { [this.appDeCodeName.toLowerCase()]: data[this.appDeCodeName.toLowerCase()] });
      }
      this.onFormLoad(data, 'loadDraft');
      const successUIDataParam: IUIDataParam = this.getUIDataParam();
      Object.assign(successUIDataParam, { action: this.loaddraftAction, navParam: viewparamResult });
      const loadDraftSuccessResult: any = await this.executeCtrlEventLogic(
        MobSearchFormEvents.LOAD_DRAFT_SUCCESS,
        successUIDataParam,
      );
      if (
        loadDraftSuccessResult &&
        loadDraftSuccessResult?.hasOwnProperty('srfret') &&
        !loadDraftSuccessResult.srfret
      ) {
        return Promise.resolve({ ret: false, data: null });
      }
      this.ctrlEvent(MobSearchFormEvents.LOAD_DRAFT_SUCCESS, successUIDataParam);
      return Promise.resolve({ ret: true, data: [response.data] });
    } else {
      const errorUIDataParam: IUIDataParam = this.getUIDataParam();
      Object.assign(errorUIDataParam, {
        action: this.loaddraftAction,
        navContext: tempContext,
        navParam: viewparamResult,
        data: [response?.data],
      });
      const loadDraftErrorResult: any = await this.executeCtrlEventLogic(
        MobSearchFormEvents.LOAD_DRAFT_ERROR,
        errorUIDataParam,
      );
      if (loadDraftErrorResult && loadDraftErrorResult?.hasOwnProperty('srfret') && !loadDraftErrorResult.srfret) {
        return Promise.resolve({ ret: false, data: null });
      }
      this.ctrlEvent(MobSearchFormEvents.LOAD_DRAFT_ERROR, errorUIDataParam);
      return Promise.resolve({ ret: false, data: null });
    }
  }

  /**
   * @description 表单项值变更
   * @param {{ name: string; value: any }} event
   * @memberof MobSearchFormCtrlController
   */
  public onFormItemValueChange(event: { name: string; value: any }): void {
    super.onFormItemValueChange(event);
    this.ctrlEvent(MobSearchFormEvents.VALUE_CHANGE, this.data);
  }

  /**
   * @description 重置
   * @memberof MobSearchFormCtrlController
   */
  public onReset() {
    this.ctrlEvent(MobSearchFormEvents.RESET, this.data);
  }

  /**
   * @description 搜索
   * @memberof MobSearchFormCtrlController
   */
  public onSearch() {
    this.ctrlEvent(MobSearchFormEvents.SEARCH, this.data);
  }
}
