import { AppMobFormService } from '../../ctrl-service/app-mob-form-service';
import {
  IPSAppDEUIAction,
  IPSAppViewLogic,
  IPSDEEditForm,
  IPSDEEditFormItem,
  IPSDEFDCatGroupLogic,
  IPSDEFDLogic,
  IPSDEFDSingleLogic,
  IPSDEFIUpdateDetail,
  IPSDEForm,
  IPSDEFormButton,
  IPSDEFormDetail,
  IPSDEFormGroupPanel,
  IPSDEFormItem,
  IPSDEFormItemEx,
  IPSDEFormItemUpdate,
  IPSDEFormItemVR,
  IPSDEFormPage,
  IPSDEFormTabPage,
  IPSDEFormTabPanel,
  IPSUIActionGroupDetail,
} from '@ibiz/dynamic-model-api';
import { LogUtil, ModelTool, Util, Verify, ViewTool } from '../../../util';
import {
  MobFormEvents,
  IParam,
  IUIDataParam,
  IMobFormCtrlController,
  ICtrlActionResult,
  IViewStateParam,
  IEditorEventParam,
  AppEditorEvents,
} from '../../../interface';
import Schema from 'async-validator';
import {
  FormButtonController,
  FormDruipartController,
  FormGroupPanelController,
  FormIFrameController,
  FormItemController,
  FormPageController,
  FormPartController,
  FormRawItemController,
  FormTabPageController,
  FormTabPanelController,
  FormUserControlController,
} from '../form-detail';
import { AppViewLogicUtil } from '../../utils';
import { AppCtrlControllerBase } from './app-ctrl-controller-base';
import { Subject } from 'rxjs';
import { AppErrorCode, EntityFieldErrorCode } from '../error-detail';

export class MobFormCtrlController extends AppCtrlControllerBase implements IMobFormCtrlController {
  /**
   * 表单模型实例对象
   *
   * @type {*}
   * @memberof MobFormCtrlController
   */
  public controlInstance!: IPSDEForm;

  /**
   * 表单数据对象
   *
   * @type {*}
   * @memberof MobFormCtrlController
   */
  public data: IParam = {};

  /**
   * 值规则
   *
   * @type {*}
   * @memberof MobFormCtrlController
   */
  public rules: IParam = {};

  /**
   * 详情模型集合
   *
   * @type {*}
   * @memberof MobMobFormCtrlController
   */
  public detailsModel: IParam = {};

  /**
   * @description 是否自动加载
   * @protected
   * @type {boolean}
   * @memberof MobFormCtrlController
   */
  protected isAutoLoad?: boolean;

  /**
   * @description 是否自动保存
   * @protected
   * @type {boolean}
   * @memberof MobFormCtrlController
   */
  protected isAutoSave?: boolean;

  /**
   * 是否可以编辑（主要用于工作流提交不需要先更新，直接提交数据）
   *
   * @type {*}
   * @memberof MobFormCtrlController
   */
  protected isEditable?: any;

  /**
   * 表单状态
   *
   * @type {Subject<any>}
   * @memberof MobFormCtrlController
   */
  public formState!: Subject<IViewStateParam>;

  /**
   * 表单错误提示信息
   *
   * @type {any[]}
   * @memberof  MobFormCtrlController
   */
  public errorMessages: any[] = [];

  /**
   * 忽略表单项值变化
   *
   * @type {boolean}
   * @memberof MobFormCtrlController
   */
  public ignorefieldvaluechange: boolean = false;

  /**
   * 关系界面状态Map
   *
   * @memberof MobFormCtrlController
   */
  protected formDRStateMap: Map<string, boolean> = new Map();

  /**
   * @description 表单分组锚点集合
   * @type {IParam[]}
   * @memberof MobFormCtrlController
   */
  public groupAnchors: IParam[] = [];

  /**
   * 获取单项数据
   *
   * @returns {*}
   * @memberof MobFormCtrlController
   */
  public getData(): any {
    return this.data;
  }

  /**
   * 部件模型数据初始化实例
   *
   * @param args 额外参数
   * @memberof MobFormCtrlController
   */
  public async ctrlModelInit(args?: any) {
    await super.ctrlModelInit(args);
    if (!App.isPreviewMode()) {
      this.ctrlService = new AppMobFormService(this.controlInstance);
      await this.ctrlService.loaded();
    }
  }

  /**
   * 部件基础数据初始化
   *
   * @memberof MobFormCtrlController
   */
  public ctrlBasicInit() {
    super.ctrlBasicInit();
    // 初始化data
    this.initData();
    // 初始化表单成员运行时模型
    this.initDetailsModel();
    // 初始化静态值规则
    this.initRules();
    // 初始化关系界面
    this.initFormDRPart();
    // TODO this.isAutoLoad (由视图传入,编辑视图上无法配置)
    this.isAutoSave = (this.controlInstance as IPSDEEditForm).enableAutoSave;
  }

  /**
   * 部件初始化
   *
   * @param args
   * @memberof MobFormCtrlController
   */
  public ctrlInit(args?: any) {
    super.ctrlInit(args);
    this.formState = new Subject();
    if (this.isAutoLoad) {
      this.autoLoad({}, { srfkey: this.context[this.appDeCodeName.toLowerCase()] });
    }
    if (this.viewState) {
      this.viewStateEvent = this.viewState.subscribe(({ tag, action, data = {} }: IViewStateParam) => {
        if (!Object.is(tag, this.name)) {
          return;
        }
        if (Object.is('loadDraft', action)) {
          this.loadDraft(data);
        }
        if (Object.is('load', action)) {
          this.load(data);
        }
        if (Object.is('save', action)) {
          this.save(data);
        }
        if (Object.is('remove', action)) {
          this.remove(data);
        }
        if (Object.is('panelAction', action)) {
          this.panelAction(data);
        }
      });
    }
  }

  /**
   * 初始化data
   *
   * @memberof MobFormCtrlController
   */
  protected initData() {
    this.controlInstance.getPSDEFormItems()?.forEach((formItem: IPSDEFormItem) => {
      this.data[formItem.id] = null;
    });
  }

  /**
   * 初始化表单成员模型
   *
   * @memberof MobFormCtrlController
   */
  protected initDetailsModel() {
    const { noTabHeader, name } = this.controlInstance;
    const allFormDetails: IPSDEFormDetail[] = ModelTool.getAllFormDetails(this.controlInstance);
    const infoModeGroup: FormGroupPanelController[] = [];
    if (allFormDetails.length > 0) {
      for (const detail of allFormDetails) {
        const detailOpts: any = {
          name: detail.name,
          caption: detail.caption,
          isShowCaption: detail.showCaption,
          detailType: detail.detailType,
          visible: !ModelTool.findGroupLogicByLogicCat('PANELVISIBLE', detail.getPSDEFDGroupLogics()),
          parentController: this,
          isControlledContent: detail.showMoreMode == 1,
          model: detail,
        };
        let detailModel: any = null;
        const uiAction = (detail as IPSDEFormButton).getPSUIAction?.();
        switch (detail.detailType) {
          case 'BUTTON':
            Object.assign(detailOpts, {
              disabled: false,
            });
            if (uiAction) {
              detailOpts.uiaction = {
                type: uiAction.uIActionType,
                tag: uiAction.uIActionTag,
                visabled: true,
                disabled: false,
              };
              if (uiAction.actionTarget) {
                detailOpts.uiaction.actiontarget = uiAction.actionTarget;
              }
              if ((uiAction as IPSAppDEUIAction).noPrivDisplayMode) {
                detailOpts.uiaction.noprivdisplaymode = (uiAction as IPSAppDEUIAction).noPrivDisplayMode;
              }
              if (uiAction.dataAccessAction) {
                detailOpts.uiaction.dataaccaction = uiAction.dataAccessAction;
                // 是否开启权限认证
                if (App.getEnvironment()?.enablePermissionValid) {
                  detailOpts.visible = false;
                }
              }
            }
            detailModel = new FormButtonController(detailOpts);
            break;
          case 'FORMITEM':
            Object.assign(detailOpts, {
              disabled: false,
              required: !(detail as IPSDEFormItem).allowEmpty,
              enableCond: (detail as IPSDEFormItem).enableCond,
            });
            detailModel = new FormItemController(detailOpts);
            break;
          case 'GROUPPANEL':
            detailOpts.isManageContainer = detail.showMoreMode == 2;
            const PSUIActionGroup = (detail as IPSDEFormGroupPanel).getPSUIActionGroup();
            // 界面行为组
            const uiActionGroup: any = {
              caption: PSUIActionGroup?.name,
              langbase: '',
              extractMode: (detail as IPSDEFormGroupPanel).actionGroupExtractMode || 'ITEM',
              details: [],
            };
            const PSUIActionGroupDetails = PSUIActionGroup?.getPSUIActionGroupDetails?.() || [];
            if (PSUIActionGroupDetails.length > 0) {
              PSUIActionGroupDetails.forEach((actionDetail: IPSUIActionGroupDetail) => {
                const uiaction = actionDetail?.getPSUIAction?.() as IPSAppDEUIAction;
                const temp: any = {
                  name: `${detail.name}_${actionDetail.name}`,
                  tag: uiaction?.uIActionTag,
                  caption: uiaction?.caption,
                  disabled: false,
                  visabled: true,
                  noprivdisplaymode: uiaction?.noPrivDisplayMode,
                  actiontarget: uiaction?.actionTarget || '',
                  dataaccaction: uiaction?.dataAccessAction || '',
                  isShowCaption: actionDetail.showCaption,
                  isShowIcon: actionDetail.showIcon,
                };
                // 行为实体
                if (uiaction?.getPSAppDataEntity?.()) {
                  temp.uiactiontag = `${uiaction
                    ?.getPSAppDataEntity?.()
                    ?.codeName?.toLowerCase()}_${uiaction?.uIActionTag?.toLowerCase()}`;
                }
                // 图标
                if (uiaction?.getPSSysImage?.()) {
                  temp.icon = uiaction.getPSSysImage();
                }
                uiActionGroup.details.push(temp);
              });
            }
            detailOpts.uiActionGroup = uiActionGroup;
            // 受控容器的成员
            const showMoreModeItems: any[] = [];
            (detail as IPSDEFormGroupPanel).getPSDEFormDetails()?.forEach((item: IPSDEFormDetail, index: number) => {
              if (!item) return;
              if (item.showMoreMode == 1) {
                showMoreModeItems.push(item.name);
              }
              const showMore = item.getShowMoreMgrPSDEFormDetail?.();
              if (showMore && showMore.id && Object.is(showMore.id, detail.name)) {
                detailOpts.isManageContainer = true;
              }
            });
            detailOpts.controlledItems = showMoreModeItems;
            //  锚点
            if ((detail as IPSDEFormGroupPanel).enableAnchor) {
              this.groupAnchors.push({
                caption: detail.caption,
                codeName: detail.codeName,
                className: `${this.controlInstance.name}-group-${detail.name}`
              })
            }
            detailModel = new FormGroupPanelController(detailOpts);
            //  子成员信息模式
            if ((detail as IPSDEFormGroupPanel).infoGroupMode) {
              infoModeGroup.push(detailModel)
            }
            break;
          case 'TABPANEL':
            // 添加tab分页
            const tabPages: any[] = [];
            (detail as IPSDEFormTabPanel).getPSDEFormTabPages()?.forEach((item: IPSDEFormTabPage, index: number) => {
              tabPages.push({
                name: item.name,
                index: index,
                visible: !ModelTool.findGroupLogicByLogicCat('PANELVISIBLE', detail.getPSDEFDGroupLogics()),
              });
            });
            Object.assign(detailOpts, {
              tabPages: tabPages,
            });
            detailModel = new FormTabPanelController(detailOpts);
            break;
          case 'TABPAGE':
            detailModel = new FormTabPageController(detailOpts);
            break;
          case 'FORMPAGE':
            detailModel = new FormPageController(detailOpts);
            break;
          case 'FORMPART':
            detailModel = new FormPartController(detailOpts);
            break;
          case 'DRUIPART':
            detailModel = new FormDruipartController(detailOpts);
            break;
          case 'IFRAME':
            detailModel = new FormIFrameController(detailOpts);
            break;
          case 'RAWITEM':
            detailModel = new FormRawItemController(detailOpts);
            break;
          case 'USERCONTROL':
            detailModel = new FormUserControlController(detailOpts);
            break;
        }
        this.detailsModel[detail.name] = detailModel;
      }
    }
    // 有分页头表格时
    if (!noTabHeader) {
      const formPages: any[] = [];
      this.controlInstance.getPSDEFormPages()?.forEach((item: IPSDEFormPage, index: number) => {
        formPages.push({
          name: item.name,
          index: index,
          visible: !ModelTool.findGroupLogicByLogicCat('PANELVISIBLE', item.getPSDEFDGroupLogics()),
        });
      });
      this.detailsModel[name] = new FormTabPanelController({
        caption: name,
        detailType: 'TABPANEL',
        name: name,
        visible: true,
        isShowCaption: true,
        form: this,
        tabPages: formPages,
      });
    }
    // 当存在表单分组配置子成员信息模式时
    if (infoModeGroup.length > 0) {
      infoModeGroup.forEach((item: FormGroupPanelController) => {
        item.model.getPSDEFormDetails()?.forEach((detail: IPSDEFormDetail) => {
          if (detail.detailType === "FORMITEM") {
            this.detailsModel[detail.name].setInfoMode(true);
          }
        })
      })
    }
  }

  /**
   * 初始化关系界面模型
   *
   * @memberof MobFormCtrlController
   */
  protected initFormDRPart() {
    if (this.detailsModel && Object.keys(this.detailsModel).length > 0) {
      Object.keys(this.detailsModel).forEach((item: string) => {
        if (this.detailsModel[item] instanceof FormDruipartController) {
          this.formDRStateMap.set(item, false);
        }
      });
    }
  }

  /**
   * 响应关系界面数据保存
   *
   * @memberof MobFormCtrlController
   */
  public reactiveFormDRSave(name: string) {
    this.formDRStateMap.set(name, true);
    if ([...this.formDRStateMap.values()].indexOf(false) == -1) {
      this.save();
    }
  }

  /**
   * 初始化值规则
   *
   * @memberof MobFormCtrlController
   */
  protected initRules() {
    // 先初始化系统值规则和属性值规则
    const staticRules: any = {};
    const allFormItemVRs = this.controlInstance.getPSDEFormItemVRs();
    allFormItemVRs?.forEach((item: IPSDEFormItemVR) => {
      const { checkMode, valueRuleType } = item;
      const formItemName = item.getPSDEFormItemName() || '';
      const sysRule = item.getPSSysValueRule();
      const deRule = item.getPSDEFValueRule();
      if (!staticRules[formItemName]) {
        staticRules[formItemName] = [];
      }
      // 排除后台检查的值规则
      if (checkMode == 2) {
        return;
      }
      // 系统值规则
      if (valueRuleType == 'SYSVALUERULE' && sysRule) {
        // 正则值规则
        if (sysRule.ruleType == 'REG') {
          staticRules[formItemName].push({
            pattern: new RegExp(sysRule.regExCode),
            message: sysRule.ruleInfo,
            trigger: ['change', 'blur'],
          });
          // 脚本值规则
        } else if (sysRule.ruleType == 'SCRIPT') {
          staticRules[formItemName].push({
            validator: (rule: any, value: any, callback: any) => {
              // 空值时不校验
              if (Util.isEmpty(this.data[formItemName])) {
                return true;
              }
              const source = this.data;
              try {
                eval(sysRule.scriptCode);
              } catch (error: any) {
                App.getNoticeService().error(error?.message);
              }
              return true;
            },
            trigger: ['change', 'blur'],
          });
        }
        // 属性值规则
      } else if (valueRuleType == 'DEFVALUERULE' && deRule) {
        // 有值项的情况，校验值项的值
        const formItem = this.controlInstance.findPSDEFormItem(formItemName);
        const valueName = formItem?.valueItemName || formItemName;
        staticRules[formItemName].push({
          validator: (rule: any, value: any, callback: any, source: any) => {
            // 空值时不校验
            if (Util.isEmpty(this.data[valueName])) {
              return true;
            }
            const { isPast, infoMessage } = Verify.verifyDeRules(
              valueName,
              this.data,
              deRule.getPSDEFVRGroupCondition(),
            );
            if (!isPast) {
              callback(new Error(infoMessage || deRule.ruleInfo));
            }
            return true;
          },
          trigger: ['change', 'blur'],
        });
      }
    });
    // 初始化非空值规则和数据类型值规则
    this.rules = {};
    const allFormItems = ModelTool.getAllFormItems(this.controlInstance) as IPSDEEditFormItem[];
    if (allFormItems && allFormItems.length > 0) {
      for (const detail of allFormItems) {
        if (detail.detailType == 'FORMITEM' && detail.getPSEditor()?.editorType != 'HIDDEN' && !detail.compositeItem) {
          const otherRules = staticRules[detail.name] || [];
          const editorRules = Verify.buildVerConditions(detail.getPSEditor());
          this.rules[detail.name] = [
            {
              validator: (rule: any, value: any, callback: any) => {
                return !(
                  this.detailsModel[detail.name].required &&
                  (value === null || value === undefined || value === '')
                );
              },
              message: `${detail.caption || detail.name} 必须填写`,
            },
            // 表单值规则
            ...otherRules,
            // 编辑器基础值规则
            ...editorRules,
          ];
        } else if (
          detail.detailType == 'FORMITEM' &&
          detail.getPSEditor()?.editorType != 'HIDDEN' &&
          detail.compositeItem
        ) {
          const compositeItemRules = this.getCompositeItemRules(detail);
          this.rules[detail.name] = [
            // 复合表单项基础值规则
            ...compositeItemRules,
          ];
        }
      }
    }
  }

  /**
   * 初始化界面行为模型
   *
   * @type {*}
   * @memberof MobFormCtrlController
   */
  public initCtrlActionModel() {
    const allFormDetails: IPSDEFormDetail[] = ModelTool.getAllFormDetails(this.controlInstance);
    if (allFormDetails?.length > 0) {
      allFormDetails.forEach((detail: IPSDEFormDetail) => {
        if (detail?.detailType == 'BUTTON' && (detail as IPSDEFormButton).getPSUIAction()) {
          // 添加表单按钮的actionModel
          const appUIAction: IPSAppDEUIAction = (detail as IPSDEFormButton).getPSUIAction() as IPSAppDEUIAction;
          const appUIAction_M = Util.deepCopy(appUIAction.M);
          this.actionModel[appUIAction.uIActionTag] = Object.assign(appUIAction_M, {
            disabled: false,
            visabled: true,
            getNoPrivDisplayMode: appUIAction.noPrivDisplayMode ? appUIAction.noPrivDisplayMode : 6,
          });
        } else if (
          detail?.detailType == 'GROUPPANEL' &&
          (detail as IPSDEFormGroupPanel).getPSUIActionGroup()?.getPSUIActionGroupDetails()?.length
        ) {
          // 添加表单分组界面行为组的actionModel
          (detail as IPSDEFormGroupPanel)
            .getPSUIActionGroup()
            ?.getPSUIActionGroupDetails()
            ?.forEach((actionDetail: IPSUIActionGroupDetail) => {
              if (actionDetail?.getPSUIAction()) {
                const appUIAction: IPSAppDEUIAction = actionDetail.getPSUIAction() as IPSAppDEUIAction;
                const appUIAction_M = Util.deepCopy(appUIAction.M);
                this.actionModel[appUIAction.uIActionTag] = Object.assign(appUIAction_M, {
                  disabled: false,
                  visabled: true,
                  getNoPrivDisplayMode: appUIAction.noPrivDisplayMode ? appUIAction.noPrivDisplayMode : 6,
                });
              }
            });
        }
      });
    }
  }

  /**
   * 表单加载
   *
   * @param opts 行为参数
   * @param args 补充逻辑完成参数
   * @param showInfo 是否显示提示信息
   * @param loadding 是否显示loading效果
   * @returns {Promise<IParam>}
   * @memberof MobFormCtrlController
   */
  public async load(
    opts?: IParam,
    args?: IParam,
    showInfo: boolean = true,
    loadding: boolean = true,
  ): Promise<ICtrlActionResult> {
    if (!this.loadAction) {
      App.getNoticeService().error(`未配置loadAction行为`);
      return Promise.resolve({ ret: false, data: null });
    }
    const arg: any = { ...opts };
    Object.assign(arg, this.viewParam);
    const tempContext: any = Util.deepCopy(this.context);
    const beforeUIDataParam: IUIDataParam = this.getUIDataParam();
    Object.assign(beforeUIDataParam, { action: this.loadAction, navContext: tempContext, navParam: arg });
    const beforeLoadResult: any = await this.executeCtrlEventLogic(MobFormEvents.BEFORE_LOAD, beforeUIDataParam);
    if (beforeLoadResult && beforeLoadResult?.hasOwnProperty('srfret') && !beforeLoadResult.srfret) {
      return Promise.resolve({ ret: false, data: null });
    }
    this.ctrlEvent(MobFormEvents.BEFORE_LOAD, beforeUIDataParam);
    this.onControlRequset('load', tempContext, arg, loadding);
    const response = await this.ctrlService.get(this.loadAction, tempContext, arg, loadding);
    this.onControlResponse('load', response);
    if (response && response.status === 200) {
      const data = response.data;
      this.onFormLoad(data, 'load');
      this.formState.next({
        action: MobFormEvents.LOAD_SUCCESS,
        tag: this.controlInstance.name,
        data: data,
      });
      const successUIDataParam: IUIDataParam = this.getUIDataParam();
      Object.assign(successUIDataParam, { action: this.loadAction, navContext: tempContext, navParam: arg });
      const loadSuccessResult: any = await this.executeCtrlEventLogic(MobFormEvents.LOAD_SUCCESS, successUIDataParam);
      if (loadSuccessResult && loadSuccessResult?.hasOwnProperty('srfret') && !loadSuccessResult.srfret) {
        return Promise.resolve({ ret: false, data: null });
      }
      this.ctrlEvent(MobFormEvents.LOAD_SUCCESS, successUIDataParam);
      return Promise.resolve({ ret: true, data: [response.data] });
    } else {
      const errorUIDataParam: IUIDataParam = this.getUIDataParam();
      Object.assign(errorUIDataParam, {
        action: this.loadAction,
        navContext: tempContext,
        navParam: arg,
        data: [response?.data],
      });
      const loadErrorResult: any = await this.executeCtrlEventLogic(MobFormEvents.LOAD_ERROR, errorUIDataParam);
      if (loadErrorResult && loadErrorResult?.hasOwnProperty('srfret') && !loadErrorResult.srfret) {
        return Promise.resolve({ ret: false, data: null });
      }
      this.ctrlEvent(MobFormEvents.LOAD_ERROR, errorUIDataParam);
      return Promise.resolve({ ret: false, data: null });
    }
  }

  /**
   * 加载草稿
   *
   * @param opts 行为参数
   * @param args 补充逻辑完成参数
   * @param showInfo 是否显示提示信息
   * @param loadding 是否显示loading效果
   * @returns {Promise<IParam>}
   * @memberof MobFormCtrlController
   */
  public async loadDraft(
    opts?: IParam,
    args?: IParam,
    showInfo: boolean = true,
    loadding: boolean = true,
  ): Promise<ICtrlActionResult> {
    if (!this.loaddraftAction) {
      App.getNoticeService().error(`未配置loaddraftAction行为`);
      return Promise.resolve({ ret: false, data: null });
    }
    const arg: any = { ...opts };
    const viewparamResult: any = Object.assign(arg, this.viewParam);
    const tempContext: any = Util.deepCopy(this.context);
    const beforeUIDataParam: IUIDataParam = this.getUIDataParam();
    Object.assign(beforeUIDataParam, {
      action: this.loaddraftAction,
      navContext: tempContext,
      navParam: viewparamResult,
    });
    const beforeLoadDraftResult: any = await this.executeCtrlEventLogic(
      MobFormEvents.BEFORE_LOAD_DRAFT,
      beforeUIDataParam,
    );
    if (beforeLoadDraftResult && beforeLoadDraftResult?.hasOwnProperty('srfret') && !beforeLoadDraftResult.srfret) {
      return Promise.resolve({ ret: false, data: null });
    }
    this.ctrlEvent(MobFormEvents.BEFORE_LOAD_DRAFT, beforeUIDataParam);
    this.onControlRequset('loadDraft', tempContext, viewparamResult, loadding);
    const response = await this.ctrlService.loadDraft(
      this.loaddraftAction,
      tempContext,
      { viewparams: viewparamResult },
      loadding,
    );
    this.onControlResponse('loadDraft', response);
    if (response && response.status === 200) {
      const data = response.data;
      this.onFormLoad(data, 'loadDraft');
      this.formState.next({
        action: MobFormEvents.LOAD_DRAFT_SUCCESS,
        tag: this.controlInstance.name,
        data: data,
      });
      // 删除主键表单项的值
      this.controlInstance.getPSDEFormItems()?.find((item: IPSDEFormItem) => {
        if (item.getPSAppDEField()?.keyField && !item.hidden) {
          data[item.name] = null;
        }
      });
      const successUIDataParam: IUIDataParam = this.getUIDataParam();
      Object.assign(successUIDataParam, {
        action: this.loaddraftAction,
        navContext: tempContext,
        navParam: viewparamResult,
        data: [data],
      });
      const loadDraftSuccessResult: any = await this.executeCtrlEventLogic(
        MobFormEvents.LOAD_DRAFT_SUCCESS,
        successUIDataParam,
      );
      if (
        loadDraftSuccessResult &&
        loadDraftSuccessResult?.hasOwnProperty('srfret') &&
        !loadDraftSuccessResult.srfret
      ) {
        return Promise.resolve({ ret: false, data: null });
      }
      this.ctrlEvent(MobFormEvents.LOAD_DRAFT_SUCCESS, successUIDataParam);
      return Promise.resolve({ ret: true, data: [response.data] });
    } else {
      const errorUIDataParam: IUIDataParam = this.getUIDataParam();
      Object.assign(errorUIDataParam, {
        action: this.loaddraftAction,
        navContext: tempContext,
        navParam: viewparamResult,
        data: [response?.data],
      });
      const loadDraftErrorResult: any = await this.executeCtrlEventLogic(
        MobFormEvents.LOAD_DRAFT_ERROR,
        errorUIDataParam,
      );
      if (loadDraftErrorResult && loadDraftErrorResult?.hasOwnProperty('srfret') && !loadDraftErrorResult.srfret) {
        return Promise.resolve({ ret: false, data: null });
      }
      this.ctrlEvent(MobFormEvents.LOAD_DRAFT_ERROR, errorUIDataParam);
      return Promise.resolve({ ret: false, data: null });
    }
  }

  /**
   * 表单自动加载
   *
   * @param opts 行为参数
   * @param args 补充逻辑完成参数
   * @param showInfo 是否显示提示信息
   * @param loadding 是否显示loading效果
   * @returns {Promise<IParam>}
   * @memberof MobFormCtrlController
   */
  public autoLoad(
    opts?: IParam,
    args?: IParam,
    showInfo: boolean = true,
    loadding: boolean = true,
  ): Promise<ICtrlActionResult> {
    if (args?.srfkey) {
      Object.assign(opts, { srfkey: args.srfkey });
      return this.load(opts, args, showInfo, loadding);
    }
    if (args?.srfkeys) {
      Object.assign(opts, { srfkey: args.srfkeys });
      return this.load(opts, args, showInfo, loadding);
    }
    return this.loadDraft(opts, args, showInfo, loadding);
  }

  /**
   * 表单保存
   *
   * @param opts 行为参数
   * @param args 补充逻辑完成参数
   * @param showInfo 是否显示提示信息
   * @param loadding 是否显示loading效果
   * @returns {Promise<IParam>}
   * @memberof MobFormCtrlController
   */
  public async save(
    opts?: IParam,
    args?: IParam,
    showInfo: boolean = true,
    loadding: boolean = true,
  ): Promise<ICtrlActionResult> {
    return new Promise(async (resolve: any, reject: any) => {
      if (!(await this.formValidateStatus())) {
        App.getNoticeService().error('表单值校验错误！');
        return resolve({ ret: false, data: null });
      }
      const arg: any = { ...opts };
      const data = this.getData();
      Object.assign(arg, this.context);
      Object.assign(arg, data);
      if (this.viewParam && this.viewParam.copymode) {
        data.srfuf = '0';
      }
      const action: any = Object.is(data.srfuf, '1') ? this.updateAction : this.createAction;
      if (!action) {
        App.getNoticeService().error(`未配置${Object.is(data.srfuf, '1') ? 'updateAction' : 'createAction'}行为`);
        return resolve({ ret: false, data: null });
      }
      Object.assign(arg, { viewparams: this.viewParam });
      const tempContext: any = Util.deepCopy(this.context);
      if ([...this.formDRStateMap.values()].indexOf(false) !== -1) {
        this.formState.next({
          action: MobFormEvents.BEFORE_SAVE,
          tag: this.controlInstance.name,
          data: data,
        });
        return;
      } else {
        this.initFormDRPart();
      }
      const beforeUIDataParam: IUIDataParam = this.getUIDataParam();
      Object.assign(beforeUIDataParam, { action: action, navContext: tempContext, navParam: arg });
      const beforeSaveResult: any = await this.executeCtrlEventLogic(MobFormEvents.BEFORE_SAVE, beforeUIDataParam);
      if (beforeSaveResult && beforeSaveResult?.hasOwnProperty('srfret') && !beforeSaveResult.srfret) {
        return resolve({ ret: false, data: null });
      }
      this.ctrlEvent(MobFormEvents.BEFORE_SAVE, beforeUIDataParam);
      this.onControlRequset('save', tempContext, arg, loadding);
      const post: Promise<any> = Object.is(data.srfuf, '1')
        ? this.ctrlService.update(action, tempContext, arg, loadding)
        : this.ctrlService.add(action, tempContext, arg, loadding);
      post
        .then(async (response: any) => {
          this.onControlResponse('save', response);
          if (!response.status || response.status !== 200) {
            const errorUIDataParam: IUIDataParam = this.getUIDataParam();
            Object.assign(errorUIDataParam, { action: action, navContext: tempContext, navParam: arg });
            const saveErrorResult: any = await this.executeCtrlEventLogic(MobFormEvents.SAVE_ERROR, errorUIDataParam);
            if (saveErrorResult && saveErrorResult?.hasOwnProperty('srfret') && !saveErrorResult.srfret) {
              return resolve({ ret: false, data: null });
            }
            this.ctrlEvent(MobFormEvents.SAVE_ERROR, errorUIDataParam);
            return resolve({ ret: false, data: null });
          }
          this.viewParam.copymode = false;
          const data = response.data;
          this.onFormLoad(data, 'save');
          this.formState.next({
            action: 'save',
            tag: this.controlInstance.name,
            data: data,
          });
          const successUIDataParam: IUIDataParam = this.getUIDataParam();
          Object.assign(successUIDataParam, { action: action, navContext: tempContext, navParam: arg });
          const saveSuccessResult: any = await this.executeCtrlEventLogic(
            MobFormEvents.SAVE_SUCCESS,
            successUIDataParam,
          );
          if (saveSuccessResult && saveSuccessResult?.hasOwnProperty('srfret') && !saveSuccessResult.srfret) {
            return resolve({ ret: false, data: null });
          }
          this.ctrlEvent(MobFormEvents.SAVE_SUCCESS, successUIDataParam);
          App.getCenterService().notifyMessage(
            this.controlInstance.getPSAppDataEntity()?.codeName || '',
            'appRefresh',
            data,
          );
          if (this.controlInstance.formFuncMode?.toLowerCase() != 'wizardform' && showInfo) {
            App.getNoticeService().success(`${data.srfmajortext} 变更成功！`);
          }
          resolve({ ret: true, data: [response.data] });
        })
        .catch(async (response: any) => {
          this.onControlResponse('save', response);
          const errorUIDataParam: IUIDataParam = this.getUIDataParam();
          Object.assign(errorUIDataParam, { action: action, navContext: tempContext, navParam: arg });
          const saveErrorResult: any = await this.executeCtrlEventLogic(MobFormEvents.SAVE_ERROR, errorUIDataParam);
          if (saveErrorResult && saveErrorResult?.hasOwnProperty('srfret') && !saveErrorResult.srfret) {
            return resolve({ ret: false, data: null });
          }
          this.ctrlEvent(MobFormEvents.SAVE_ERROR, errorUIDataParam);
          if (this.controlInstance.formFuncMode?.toLowerCase() != 'wizardform' && showInfo) {
            App.getNoticeService().error(response.message);
          }
          resolve({ ret: false, data: null });
        });
    });
  }

  /**
   * 表单删除
   *
   * @param opts 行为参数
   * @param args 补充逻辑完成参数
   * @param showInfo 是否显示提示信息
   * @param loadding 是否显示loading效果
   * @returns {Promise<IParam>}
   * @memberof MobFormCtrlController
   */
  public async remove(
    opts?: IParam,
    args?: IParam,
    showInfo: boolean = true,
    loadding: boolean = true,
  ): Promise<ICtrlActionResult> {
    return new Promise(async (resolve: any, reject: any) => {
      if (!this.removeAction) {
        App.getNoticeService().error('未配置removeAction行为');
        return resolve({ ret: false, data: null });
      }
      const arg: any = { ...opts };
      opts;
      Object.assign(arg, { viewparams: this.viewParam });
      const tempContext: any = Util.deepCopy(this.context);
      const beforeUIDataParam: IUIDataParam = this.getUIDataParam();
      Object.assign(beforeUIDataParam, { action: this.removeAction, navContext: tempContext, navParam: arg });
      const beforeRemoveResult: any = await this.executeCtrlEventLogic(MobFormEvents.BEFORE_REMOVE, beforeUIDataParam);
      if (beforeRemoveResult && beforeRemoveResult?.hasOwnProperty('srfret') && !beforeRemoveResult.srfret) {
        return resolve({ ret: false, data: null });
      }
      this.ctrlEvent(MobFormEvents.BEFORE_REMOVE, beforeUIDataParam);
      this.onControlRequset('remove', tempContext, arg, loadding);
      this.ctrlService
        .delete(this.removeAction, tempContext, arg, loadding)
        .then(async (response: any) => {
          this.onControlResponse('remove', response);
          if (response) {
            this.data.ismodify = false;
            const successUIDataParam: IUIDataParam = this.getUIDataParam();
            Object.assign(successUIDataParam, { action: this.removeAction, navContext: tempContext, navParam: arg });
            const removeSuccessResult: any = await this.executeCtrlEventLogic(
              MobFormEvents.REMOVE_SUCCESS,
              successUIDataParam,
            );
            if (removeSuccessResult && removeSuccessResult?.hasOwnProperty('srfret') && !removeSuccessResult.srfret) {
              return resolve({ ret: false, data: null });
            }
            this.ctrlEvent(MobFormEvents.REMOVE_SUCCESS, successUIDataParam);
            this.formState.next({
              action: MobFormEvents.REMOVE_SUCCESS,
              tag: this.controlInstance.name,
              data: this.data,
            });
            if (showInfo) {
              App.getNoticeService().success(`${this.data?.srfmajortext} 删除成功！`);
            }
            resolve({ ret: true, data: [response.data] });
          }
        })
        .catch(async (error: any) => {
          this.onControlResponse('remove', error);
          const errorUIDataParam: IUIDataParam = this.getUIDataParam();
          Object.assign(errorUIDataParam, { action: this.removeAction, navContext: tempContext, navParam: arg });
          const removeErrorResult: any = await this.executeCtrlEventLogic(MobFormEvents.REMOVE_ERROR, errorUIDataParam);
          if (removeErrorResult && removeErrorResult?.hasOwnProperty('srfret') && !removeErrorResult.srfret) {
            return resolve({ ret: false, data: null });
          }
          this.ctrlEvent(MobFormEvents.REMOVE_ERROR, errorUIDataParam);
          if (showInfo) {
            App.getNoticeService().error(`${this.data?.srfmajortext} 删除失败！`);
          }
          resolve({ ret: false, data: null });
        });
    });
  }

  /**
   * 工作流启动
   *
   * @param opts 表单数据
   * @param args 补充逻辑完成参数
   * @param showInfo 是否显示提示信息
   * @param loadding 是否显示loading效果
   * @returns {Promise<IParam>}
   * @memberof MobFormCtrlController
   */
  public async wfstart(
    opts?: IParam,
    args?: IParam,
    showInfo: boolean = true,
    loadding: boolean = true,
  ): Promise<ICtrlActionResult> {
    if (!this.formValidateStatus()) {
      App.getNoticeService().error('表单值校验错误！');
      return Promise.resolve({ ret: false, data: null });
    }
    return new Promise(async (resolve: any, reject: any) => {
      const formData: any = this.getData();
      const copyData: any = Util.deepCopy(opts);
      const action: any = Object.is(opts?.srfuf, '1') ? this.updateAction : this.createAction;
      Object.assign(formData, { viewparams: copyData });
      const tempContext: any = Util.deepCopy(this.context);
      const beforeUIDataParam: IUIDataParam = this.getUIDataParam();
      Object.assign(beforeUIDataParam, { action: action, navContext: tempContext, navParam: formData });
      const beforeSaveResult: any = await this.executeCtrlEventLogic(MobFormEvents.BEFORE_SAVE, beforeUIDataParam);
      if (beforeSaveResult && beforeSaveResult?.hasOwnProperty('srfret') && !beforeSaveResult.srfret) {
        return resolve({ ret: false, data: null });
      }
      this.ctrlEvent(MobFormEvents.BEFORE_SAVE, beforeUIDataParam);
      this.formState.next({
        action: MobFormEvents.BEFORE_SAVE,
        tag: this.controlInstance.name,
        data: formData,
      });
      this.onControlRequset('save', tempContext, formData, loadding);
      const post: Promise<any> = Object.is(formData.srfuf, '1')
        ? this.ctrlService.update(this.updateAction, tempContext, formData, loadding, true)
        : this.ctrlService.add(this.createAction, tempContext, formData, loadding, true);
      post
        .then(async (response: any) => {
          this.onControlResponse('save', response);
          const arg: any = response.data;
          const responseData: any = Util.deepCopy(arg);
          // 保存完成UI处理
          this.onFormLoad(arg, 'save');
          const successUIDataParam: IUIDataParam = this.getUIDataParam();
          Object.assign(successUIDataParam, { action: action, navContext: this.context, navParam: formData });
          const saveSuccessResult: any = await this.executeCtrlEventLogic(
            MobFormEvents.SAVE_SUCCESS,
            successUIDataParam,
          );
          if (saveSuccessResult && saveSuccessResult?.hasOwnProperty('srfret') && !saveSuccessResult.srfret) {
            return resolve({ ret: false, data: null });
          }
          this.ctrlEvent(MobFormEvents.SAVE_SUCCESS, successUIDataParam);
          // 准备工作流数据,填充未存库数据
          const tempWFData: any = {};
          if (copyData && Object.keys(copyData).length > 0) {
            Object.keys(copyData).forEach((key: string) => {
              if (!arg.hasOwnProperty(key) || (!arg[key] && copyData[key])) {
                tempWFData[key] = copyData[key];
              }
            });
          }
          // 准备提交参数
          if (this.viewParam) {
            const copyViewParams: any = Util.deepCopy(this.viewParam);
            if (copyViewParams.w) {
              delete copyViewParams.w;
            }
            if (this.appDeKeyFieldName && copyViewParams[this.appDeKeyFieldName.toLocaleLowerCase()]) {
              delete copyViewParams[this.appDeKeyFieldName.toLocaleLowerCase()];
            }
            if (this.appDeMajorFieldName && copyViewParams[this.appDeMajorFieldName.toLocaleLowerCase()]) {
              delete copyViewParams[this.appDeMajorFieldName.toLocaleLowerCase()];
            }
            Object.assign(responseData, copyViewParams);
          }
          if (tempWFData && Object.keys(tempWFData).length > 0) {
            Object.assign(responseData, tempWFData);
          }
          Object.assign(arg, { viewparams: responseData });
          // 强制补充srfwfmemo
          if (copyData.srfwfmemo) {
            Object.assign(arg, { srfwfmemo: copyData.srfwfmemo });
          }
          const tempContext: any = Util.deepCopy(this.context);
          const beforeWFUIDataParam: IUIDataParam = this.getUIDataParam();
          Object.assign(beforeWFUIDataParam, { action: this.WFStartAction, navContext: tempContext, navParam: arg });
          const beforeWFStartResult: any = await this.executeCtrlEventLogic(
            MobFormEvents.BEFORE_START,
            beforeWFUIDataParam,
          );
          if (beforeWFStartResult && beforeWFStartResult?.hasOwnProperty('srfret') && !beforeWFStartResult.srfret) {
            return resolve({ ret: false, data: null });
          }
          this.ctrlEvent(MobFormEvents.BEFORE_START, beforeWFUIDataParam);
          this.onControlRequset('wfstart', tempContext, arg, loadding);
          const result: Promise<any> = this.ctrlService.wfstart(this.WFStartAction, tempContext, arg, loadding, args);
          result
            .then(async (response: any) => {
              this.onControlResponse('wfstart', response);
              if (!response || response.status !== 200) {
                const errorWFUIDataParam: IUIDataParam = this.getUIDataParam();
                Object.assign(errorWFUIDataParam, {
                  action: this.WFStartAction,
                  navContext: tempContext,
                  navParam: arg,
                });
                const WFStartErrorResult: any = await this.executeCtrlEventLogic(
                  MobFormEvents.START_ERROR,
                  errorWFUIDataParam,
                );
                if (WFStartErrorResult && WFStartErrorResult?.hasOwnProperty('srfret') && !WFStartErrorResult.srfret) {
                  return resolve({ ret: false, data: null });
                }
                this.ctrlEvent(MobFormEvents.START_ERROR, beforeWFUIDataParam);
                return resolve({ ret: false, data: null });
              }
              App.getCenterService().notifyMessage(
                this.controlInstance.getPSAppDataEntity()?.codeName || '',
                'appRefresh',
                response.data,
              );
              if (showInfo) {
                App.getNoticeService().success('工作流启动成功！！！');
              }
              const successWFUIDataParam: IUIDataParam = this.getUIDataParam();
              Object.assign(successWFUIDataParam, {
                action: this.WFStartAction,
                navContext: tempContext,
                navParam: arg,
              });
              const WFStartSuccessResult: any = await this.executeCtrlEventLogic(
                MobFormEvents.START_SUCCESS,
                successWFUIDataParam,
              );
              if (
                WFStartSuccessResult &&
                WFStartSuccessResult?.hasOwnProperty('srfret') &&
                !WFStartSuccessResult.srfret
              ) {
                return resolve({ ret: false, data: null });
              }
              this.ctrlEvent(MobFormEvents.START_SUCCESS, successWFUIDataParam);
              resolve({ ret: true, data: [response.data] });
            })
            .catch(async (response: any) => {
              this.onControlResponse('wfstart', response);
              const errorWFUIDataParam: IUIDataParam = this.getUIDataParam();
              Object.assign(errorWFUIDataParam, { action: this.WFStartAction, navContext: tempContext, navParam: arg });
              const WFStartErrorResult: any = await this.executeCtrlEventLogic(
                MobFormEvents.START_ERROR,
                errorWFUIDataParam,
              );
              if (WFStartErrorResult && WFStartErrorResult?.hasOwnProperty('srfret') && !WFStartErrorResult.srfret) {
                return resolve({ ret: false, data: null });
              }
              this.ctrlEvent(MobFormEvents.START_ERROR, errorWFUIDataParam);
              if (showInfo) {
                App.getNoticeService().error('工作流启动失败！');
              }
              resolve({ ret: false, data: null });
            });
        })
        .catch(async (response: any) => {
          this.onControlResponse('save', response);
          const errorUIDataParam: IUIDataParam = this.getUIDataParam();
          Object.assign(errorUIDataParam, { action: action, navContext: tempContext, navParam: formData });
          const saveErrorResult: any = await this.executeCtrlEventLogic(MobFormEvents.SAVE_ERROR, errorUIDataParam);
          if (saveErrorResult && saveErrorResult?.hasOwnProperty('srfret') && !saveErrorResult.srfret) {
            return resolve({ ret: false, data: null });
          }
          this.ctrlEvent(MobFormEvents.SAVE_ERROR, errorUIDataParam);
          resolve({ ret: false, data: null });
        });
    });
  }

  /**
   * 工作流提交
   *
   * @param opts 表单数据
   * @param args 补充逻辑完成参数
   * @param showInfo 是否显示提示信息
   * @param loadding 是否显示loading效果
   * @returns {Promise<IParam>}
   * @memberof MobFormCtrlController
   */
  public async wfsubmit(
    opts: IParam,
    args?: IParam,
    showInfo: boolean = true,
    loadding: boolean = true,
  ): Promise<ICtrlActionResult> {
    if (!this.formValidateStatus()) {
      App.getNoticeService().error('表单值校验错误');
      return Promise.resolve({ ret: false, data: null });
    }
    return new Promise(async (resolve: any, reject: any) => {
      const arg: any = { ...opts };
      const copyData: any = Util.deepCopy(arg);
      Object.assign(this.viewParam, copyData);
      Object.assign(arg, { viewparams: this.viewParam });
      if (!arg[this.appDeKeyFieldName.toLowerCase()]) {
        return resolve({ ret: false, data: null });
      }
      const submitData: Function = async (arg: any, responseData: any) => {
        // 准备工作流数据,填充未存库数据
        const tempWFData: any = {};
        if (copyData && Object.keys(copyData).length > 0) {
          Object.keys(copyData).forEach((key: string) => {
            if (!arg.hasOwnProperty(key) || (!arg[key] && copyData[key])) {
              tempWFData[key] = copyData[key];
            }
          });
        }
        // 准备提交参数
        if (this.viewParam) {
          Object.assign(responseData, this.viewParam);
        }
        if (tempWFData && Object.keys(tempWFData).length > 0) {
          Object.assign(responseData, tempWFData);
        }
        // 补充逻辑完成参数
        if (args && args.type && Object.is(args.type, 'finish')) {
          Object.assign(responseData, { srfwfpredefaction: 'finish' });
        }
        Object.assign(arg, { viewparams: responseData });
        // 强制补充srfwfmemo
        if (copyData.srfwfmemo) {
          Object.assign(arg, { srfwfmemo: copyData.srfwfmemo });
        }
        const tempContext: any = Util.deepCopy(this.context);
        const beforeWFUIDataParam: IUIDataParam = this.getUIDataParam();
        Object.assign(beforeWFUIDataParam, { action: this.WFSubmitAction, navContext: tempContext, navParam: arg });
        const beforeWFSubmitResult: any = await this.executeCtrlEventLogic(
          MobFormEvents.BEFORE_SUBMIT,
          beforeWFUIDataParam,
        );
        if (beforeWFSubmitResult && beforeWFSubmitResult?.hasOwnProperty('srfret') && !beforeWFSubmitResult.srfret) {
          return resolve({ ret: false, data: null });
        }
        this.ctrlEvent(MobFormEvents.BEFORE_SUBMIT, beforeWFUIDataParam);
        this.onControlRequset('wfsubmit', tempContext, arg, loadding);
        const result: Promise<any> = this.ctrlService.wfsubmit(this.WFSubmitAction, tempContext, arg, loadding, args);
        result
          .then(async (response: any) => {
            this.onControlResponse('wfsubmit', response);
            if (!response || response.status !== 200) {
              const errorWFUIDataParam: IUIDataParam = this.getUIDataParam();
              Object.assign(errorWFUIDataParam, {
                action: this.WFSubmitAction,
                navContext: tempContext,
                navParam: arg,
              });
              const WFSubmitErrorResult: any = await this.executeCtrlEventLogic(
                MobFormEvents.SUBMIT_ERROR,
                errorWFUIDataParam,
              );
              if (WFSubmitErrorResult && WFSubmitErrorResult?.hasOwnProperty('srfret') && !WFSubmitErrorResult.srfret) {
                return resolve({ ret: false, data: null });
              }
              this.ctrlEvent(MobFormEvents.SUBMIT_ERROR, errorWFUIDataParam);
              return resolve({ ret: false, data: null });
            }
            this.onFormLoad(arg, 'submit');
            const successWFUIDataParam: IUIDataParam = this.getUIDataParam();
            Object.assign(successWFUIDataParam, {
              action: this.WFSubmitAction,
              navContext: tempContext,
              navParam: arg,
            });
            const WFSubmitSuccessResult: any = await this.executeCtrlEventLogic(
              MobFormEvents.SUBMIT_SUCCESS,
              successWFUIDataParam,
            );
            if (
              WFSubmitSuccessResult &&
              WFSubmitSuccessResult?.hasOwnProperty('srfret') &&
              !WFSubmitSuccessResult.srfret
            ) {
              return resolve({ ret: false, data: null });
            }
            this.ctrlEvent(MobFormEvents.SUBMIT_SUCCESS, successWFUIDataParam);
            App.getCenterService().notifyMessage(
              this.controlInstance.getPSAppDataEntity()?.codeName || '',
              'appRefresh',
              response.data,
            );
            if (showInfo) {
              App.getNoticeService().success('工作流提交成功！');
            }
            resolve({ ret: true, data: [response.data] });
          })
          .catch(async (response: any) => {
            this.onControlResponse('wfsubmit', response);
            const errorWFUIDataParam: IUIDataParam = this.getUIDataParam();
            Object.assign(errorWFUIDataParam, { action: this.WFSubmitAction, navContext: tempContext, navParam: arg });
            const WFSubmitErrorResult: any = await this.executeCtrlEventLogic(
              MobFormEvents.SUBMIT_ERROR,
              errorWFUIDataParam,
            );
            if (WFSubmitErrorResult && WFSubmitErrorResult?.hasOwnProperty('srfret') && !WFSubmitErrorResult.srfret) {
              return resolve({ ret: false, data: null });
            }
            this.ctrlEvent(MobFormEvents.SUBMIT_ERROR, errorWFUIDataParam);
            resolve({ ret: false, data: null });
          });
      };
      if (this.isEditable) {
        const tempContext: any = Util.deepCopy(this.context);
        const action: any = Object.is(opts?.srfuf, '1') ? this.updateAction : this.createAction;
        const beforeUIDataParam: IUIDataParam = this.getUIDataParam();
        Object.assign(beforeUIDataParam, { action: action, navContext: tempContext, navParam: arg });
        const beforeSaveResult: any = await this.executeCtrlEventLogic(MobFormEvents.BEFORE_SAVE, beforeUIDataParam);
        if (beforeSaveResult && beforeSaveResult?.hasOwnProperty('srfret') && !beforeSaveResult.srfret) {
          return resolve({ ret: false, data: null });
        }
        this.ctrlEvent(MobFormEvents.BEFORE_SAVE, beforeUIDataParam);
        this.formState.next({
          action: MobFormEvents.BEFORE_SAVE,
          tag: this.controlInstance.name,
          data: this.data,
        });
        this.onControlRequset('save', tempContext, arg, loadding);
        const post: Promise<any> = Object.is(arg.srfuf, '1')
          ? this.ctrlService.update(this.updateAction, tempContext, arg, loadding, true)
          : this.ctrlService.add(this.createAction, tempContext, arg, loadding, true);
        post
          .then(async (response: any) => {
            this.onControlResponse('save', response);
            const responseData: any = response.data;
            const tempResponseData: any = Util.deepCopy(response);
            this.ctrlService.handleResponse('save', tempResponseData);
            const arg: any = tempResponseData.data;
            // 保存完成UI处理
            this.onFormLoad(arg, 'save');
            const successUIDataParam: IUIDataParam = this.getUIDataParam();
            Object.assign(successUIDataParam, { action: action, navContext: tempContext, navParam: arg });
            const saveSuccessResult: any = await this.executeCtrlEventLogic(
              MobFormEvents.SAVE_SUCCESS,
              successUIDataParam,
            );
            if (saveSuccessResult && saveSuccessResult?.hasOwnProperty('srfret') && !saveSuccessResult.srfret) {
              return resolve({ ret: false, data: null });
            }
            this.ctrlEvent(MobFormEvents.SAVE_SUCCESS, successUIDataParam);
            this.formState.next({
              action: MobFormEvents.SAVE_SUCCESS,
              tag: this.controlInstance.name,
              data: this.data,
            });
            submitData(arg, responseData);
          })
          .catch(async (response: any) => {
            this.onControlResponse('save', response);
            const errorUIDataParam: IUIDataParam = this.getUIDataParam();
            Object.assign(errorUIDataParam, { action: action, navContext: tempContext, navParam: arg });
            const saveErrorResult: any = await this.executeCtrlEventLogic(MobFormEvents.SAVE_ERROR, errorUIDataParam);
            if (saveErrorResult && saveErrorResult?.hasOwnProperty('srfret') && !saveErrorResult.srfret) {
              return resolve({ ret: false, data: null });
            }
            this.ctrlEvent(MobFormEvents.SAVE_ERROR, errorUIDataParam);
            resolve({ ret: false, data: null });
          });
      } else {
        const responseData: any = this.getData();
        submitData(arg, responseData);
      }
    });
  }

  /**
   * 表单刷新
   *
   * @param opts 行为参数
   * @param args 补充逻辑完成参数
   * @param showInfo 是否显示提示信息
   * @param loadding 是否显示loading效果
   * @returns {Promise<IParam>}
   * @memberof MobFormCtrlController
   */
  public async refresh(
    opts?: IParam,
    args?: IParam,
    showInfo: boolean = true,
    loadding: boolean = true,
  ): Promise<ICtrlActionResult> {
    if (this.data.srfkey && !Object.is(this.data.srfkey, '')) {
      Object.assign(opts, { srfkey: this.data.srfkey });
      return this.load(opts, args, showInfo, loadding);
    }
    if (this.data.srfkeys && !Object.is(this.data.srfkeys, '')) {
      Object.assign(opts, { srfkey: this.data.srfkeys });
      return this.load(opts, args, showInfo, loadding);
    }
    return this.loadDraft(opts, args, showInfo, loadding);
  }

  /**
   * 面板行为
   *
   * @param opts 行为参数
   * @param args 补充逻辑完成参数
   * @param showInfo 是否显示提示信息
   * @param loadding 是否显示loading效果
   * @returns {Promise<IParam>}
   * @memberof MobFormCtrlController
   */
  public async panelAction(
    opts?: IParam,
    args?: IParam,
    showInfo: boolean = true,
    loadding: boolean = true,
  ): Promise<ICtrlActionResult> {
    const action = opts?.action;
    const emitAction = opts?.emitAction;
    const data = opts?.data;
    if (!action || (action && Object.is(action, ''))) {
      return Promise.resolve({ ret: false, data: null });
    }
    const arg: any = { ...data };
    const formdata = this.getData();
    Object.assign(arg, this.viewParam);
    Object.assign(arg, formdata);
    const tempContext: any = Util.deepCopy(this.context);
    if (data[this.appDeCodeName.toLowerCase()]) {
      Object.assign(tempContext, { [this.appDeCodeName.toLowerCase()]: data[this.appDeCodeName.toLowerCase()] });
    }
    this.onControlRequset('panelAction', tempContext, arg, loadding);
    const response = await this.ctrlService.frontLogic(action, tempContext, arg, loadding);
    this.onControlResponse('panelAction', response);
    if (response.status && response.status == 200) {
      const data = response.data;
      this.onFormLoad(data, emitAction);
      this.ctrlEvent(emitAction, data);
      this.formState.next({
        action: emitAction,
        tag: this.controlInstance.name,
        data: data,
      });
      return Promise.resolve({ ret: true, data: [response.data] });
    } else {
      App.getNoticeService().error(response.message);
      return Promise.resolve({ ret: false, data: null });
    }
  }

  /**
   * 表单项更新
   *
   * @param mode 界面行为名称
   * @param data 请求数据
   * @param updateDetails 更新项
   * @param loading 是否显示加载状态
   * @memberof MobFormCtrlController
   */
  protected updateFormItems(mode: string, data: any = {}, updateDetails: string[], loading: boolean = false): void {
    if (!mode) {
      return;
    }
    const arg: any = Object.assign(Util.deepCopy(this.viewParam), data);
    const tempContext: any = Util.deepCopy(this.context);
    this.onControlRequset('updateFormItem', tempContext, arg, loading);
    const post: Promise<any> = this.ctrlService.frontLogic(mode, tempContext, arg, loading);
    post
      .then((response: any) => {
        this.onControlResponse('updateFormItem', response);
        if (!response || response.status !== 200) {
          App.getNoticeService().error('表单项更新失败！！！');
          return;
        }
        const data = response.data;
        const _data: any = {};
        updateDetails.forEach((name: string) => {
          if (!data.hasOwnProperty(name)) {
            return;
          }
          Object.assign(_data, { [name]: data[name] });
        });
        this.setFormEnableCond(_data);
        this.fillForm(_data, 'updateFormItem');
        this.formLogic({ name: '' });
        this.formState.next({
          action: 'updateFormItem',
          tag: this.controlInstance.name,
          data: _data,
        });
      })
      .catch((response: any) => {
        this.onControlResponse('updateFormItem', response);
        App.getNoticeService().error(response.message);
      });
  }

  /**
   * 重置
   *
   * @memberof MobFormCtrlController
   */
  public onReset() {
    if (Object.keys(this.data).length > 0) {
      Object.keys(this.data).forEach((name: string) => {
        if (this.data.hasOwnProperty(name)) {
          this.data[name] = null;
        }
      });
    }
    this.formState.next({
      action: 'load',
      tag: this.controlInstance.name,
      data: this.data,
    });
  }

  /**
   * 表单加载完成
   *
   * @param {*} data 表单数据
   * @param {string} action 表单行为
   * @memberof MobFormCtrlController
   */
  protected onFormLoad(data: any = {}, action: string): void {
    if (this.appDeCodeName.toLowerCase()) {
      if (Object.is(action, 'save') || Object.is(action, 'autoSave') || Object.is(action, 'submit'))
        if (data[this.appDeCodeName.toLowerCase()]) {
          // 更新context的实体主键
          Object.assign(this.context, { [this.appDeCodeName.toLowerCase()]: data[this.appDeCodeName.toLowerCase()] });
        }
    }
    this.setFormEnableCond(data);
    this.computeButtonState(data);
    this.fillForm(data, action);
    this.formLogic({ name: '' });
  }

  /**
   * 设置表单项是否启用
   *
   * @param {*} data 表单数据
   * @memberof MobFormCtrlController
   */
  protected setFormEnableCond(data: any): void {
    Object.values(this.detailsModel).forEach((detail: any) => {
      if (!Object.is(detail.detailType, 'FORMITEM')) {
        return;
      }
      const formItem: any = detail;
      formItem.setEnableCond(data.srfuf);
    });
  }

  /**
   * 计算表单按钮权限状态
   *
   * @param {*} data 表单数据
   * @memberof MobFormCtrlController
   */
  protected computeButtonState(data: any) {
    const targetData: any = this.transformData(data);
    ViewTool.calcActionItemAuthState(targetData, this.actionModel, this.appUIService);
    if (this.detailsModel && Object.keys(this.detailsModel).length > 0) {
      Object.keys(this.detailsModel).forEach((name: any) => {
        const model = this.detailsModel[name];
        if (model?.detailType == 'BUTTON' && model.uiaction?.tag) {
          // 更新detailsModel里的按钮的权限状态值
          if (Util.isEmpty(this.actionModel[model.uiaction.tag].dataActionResult)) {
            this.detailsModel[name].isPower = true;
            this.detailsModel[name].visible = true;
            this.detailsModel[name].disabled = false;
          } else {
            this.detailsModel[name].visible = this.actionModel[model.uiaction.tag].visabled;
            this.detailsModel[name].disabled = this.actionModel[model.uiaction.tag].disabled;
            this.detailsModel[name].isPower =
              this.actionModel[model.uiaction.tag].dataActionResult === 1 ? true : false;
          }
        } else if (model?.detailType == 'GROUPPANEL' && model.uiActionGroup?.details?.length > 0) {
          // 更新分组面板界面行为组的权限状态值
          model.uiActionGroup.details.forEach((actionDetail: any) => {
            actionDetail.visible = this.actionModel[actionDetail.tag].visabled;
            actionDetail.disabled = this.actionModel[actionDetail.tag].disabled;
          });
        }
      });
    }
  }

  /**
   * 值填充
   *
   * @param {*} data 填充数据
   * @param {string} action 表单行为
   * @memberof MobFormCtrlController
   */
  protected fillForm(data: any = {}, action: string): void {
    Object.keys(data).forEach((name: string) => {
      if (this.data.hasOwnProperty(name)) {
        this.data[name] = data[name];
      }
    });
    if (Object.is(action, 'loadDraft')) {
      this.createDefault();
    }
    if (Object.is(action, 'load')) {
      this.updateDefault();
    }
  }

  /**
   * 新建默认值
   *
   * @memberof MobFormCtrlController
   */
  protected createDefault() {
    const allFormDetails: IPSDEEditFormItem[] = ModelTool.getAllFormItems(this.controlInstance);
    if (allFormDetails.length > 0) {
      for (const detail of allFormDetails) {
        const property = detail?.codeName?.toLowerCase();
        if ((detail?.createDV || detail?.createDVT) && this.data.hasOwnProperty(property)) {
          switch (detail.createDVT) {
            case 'CONTEXT':
              this.data[property] = this.viewParam[detail?.createDV];
              break;
            case 'SESSION':
              this.data[property] = this.context[detail?.createDV];
              break;
            case 'APPDATA':
              this.data[property] = this.context[detail?.createDV];
              break;
            case 'OPERATORNAME':
              this.data[property] = this.context['srfusername'];
              break;
            case 'OPERATOR':
              this.data[property] = this.context['srfuserid'];
              break;
            case 'CURTIME':
              this.data[property] = Util.dateFormat(new Date(), detail.getPSAppDEField()?.valueFormat);
              break;
            case 'PARAM':
              this.data[property] = this.ctrlService.getRemoteCopyData()?.[property] || null;
              break;
            default:
              this.data[property] = ModelTool.isNumberField(detail?.getPSAppDEField())
                ? Number(detail?.createDV)
                : detail?.createDV;
              break;
          }
        }
      }
    }
  }

  /**
   * 更新默认值
   *
   * @memberof MobFormCtrlController
   */
  protected updateDefault() {
    const allFormDetails: IPSDEEditFormItem[] = ModelTool.getAllFormItems(this.controlInstance);
    if (allFormDetails.length > 0) {
      for (const detail of allFormDetails) {
        const property = detail?.codeName?.toLowerCase();
        if ((detail?.updateDV || detail?.updateDVT) && this.data.hasOwnProperty(property)) {
          switch (detail?.updateDVT) {
            case 'CONTEXT':
              this.data[property] = this.viewParam[detail?.updateDV];
              break;
            case 'SESSION':
              this.data[property] = this.context[detail?.updateDV];
              break;
            case 'APPDATA':
              this.data[property] = this.context[detail?.updateDV];
              break;
            case 'OPERATORNAME':
              this.data[property] = this.context['srfusername'];
              break;
            case 'OPERATOR':
              this.data[property] = this.context['srfuserid'];
              break;
            case 'CURTIME':
              this.data[property] = Util.dateFormat(new Date(), detail.getPSAppDEField()?.valueFormat);
              break;
            case 'PARAM':
              this.data[property] = this.ctrlService.getRemoteCopyData()?.[property] || null;
              break;
            case 'RESET':
              this.data[property] = null;
              break;
            default:
              this.data[property] = ModelTool.isNumberField(detail?.getPSAppDEField())
                ? Number(detail?.updateDV)
                : detail?.updateDV;
          }
        }
      }
    }
  }

  /**
   * 表单校验状态
   *
   * @returns {boolean}
   * @memberof MobFormCtrlController
   */
  protected async formValidateStatus(): Promise<boolean> {
    this.errorMessages = [];
    let validateState = true;
    let tempMessage: string = '';
    for (const property of Object.keys(this.rules)) {
      if (!(await this.validate(property))) {
        validateState = false;
        tempMessage = tempMessage + '<p>' + this.detailsModel[property].error + '</p>';
      }
    }
    this.errorMessages.push(tempMessage);
    return validateState;
  }

  /**
   * 表单项值校验
   *
   * @param {string} name 项名称
   * @param {*} data 数据
   * @memberof MobFormCtrlController
   */
  protected validate(name: string): Promise<any> {
    return new Promise((resolve, reject) => {
      Util.validateItem(name, this.data, this.rules)
        .then(() => {
          this.detailsModel[name].setError(null);
          resolve(true);
        })
        .catch(({ errors, fields }: any) => {
          this.detailsModel[name].setError(errors?.[0].message);
          resolve(false);
        });
    });
  }

  /**
   * 表单项值变更
   *
   * @param {{ name: string, value: any }} $event 名称，值
   * @returns {void}
   * @memberof MobFormCtrlController
   */
  public onFormItemValueChange(event: IParam): void {
    if (!(event?.name && event.name in this.data)) {
      return;
    }
    this.data[event.name] = event.value;
    this.validate(event.name);
    this.resetFormData({ name: event.name });
    this.formLogic({ name: event.name });
    if (this.isAutoSave) {
      this.save();
    }
  }

  /**
   * 处理编辑器事件
   *
   * @param {args: IEditorEventParam} 参数
   * @returns {void}
   * @memberof MobFormCtrlController
   */
  public handleEditorEvent(args: IEditorEventParam): void {
    const { editorName, action, data } = args;
    if (Object.is(action, AppEditorEvents.VALUE_CHANGE)) {
      this.onFormItemValueChange(data as IParam);
    }
  }

  /**
   * 表单逻辑
   *
   * @public
   * @param {{ name: string }} { name } 名称
   * @memberof MobFormCtrlController
   */
  protected async formLogic({ name }: { name: string }) {
    const allFormDetails: IPSDEFormDetail[] = ModelTool.getAllFormDetails(this.controlInstance);
    // 表单动态逻辑
    allFormDetails?.forEach((detail: IPSDEFormDetail) => {
      detail.getPSDEFDGroupLogics()?.forEach((logic: IPSDEFDCatGroupLogic) => {
        const relatedNames = logic.getRelatedDetailNames() || [];
        if (Object.is(name, '') || relatedNames.indexOf(name) != -1) {
          const ret = this.verifyGroupLogic(this.data, logic);
          switch (logic.logicCat) {
            // 动态空输入，不满足则必填
            case 'ITEMBLANK':
              this.detailsModel[detail.name].required = !ret;
              break;
            // 动态启用，满足则启用
            case 'ITEMENABLE':
              this.detailsModel[detail.name].setDisabled(!ret);
              break;
            // 动态显示，满足则显示
            case 'PANELVISIBLE':
              this.detailsModel[detail.name].setVisible(ret);
              break;
          }
        }
      });
    });
    // 表单项更新
    const formDetail: IPSDEFormItem = ModelTool.getFormDetailByName(this.controlInstance, name);
    const formItemUpdate: IPSDEFormItemUpdate | null = formDetail?.getPSDEFormItemUpdate?.();
    if (formItemUpdate) {
      if (await this.checkItem(formDetail.name)) {
        if (formItemUpdate.customCode) {
          if (formItemUpdate.scriptCode) {
            const context = Util.deepCopy(this.context);
            const viewparams = Util.deepCopy(this.viewParam);
            const data = this.data;
            eval(formItemUpdate.scriptCode);
          }
        } else {
          const showBusyIndicator = formItemUpdate.showBusyIndicator;
          const getPSAppDEMethod = formItemUpdate.getPSAppDEMethod();
          const getPSDEFIUpdateDetails = formItemUpdate.getPSDEFIUpdateDetails();
          const details: string[] = [];
          getPSDEFIUpdateDetails?.forEach((item: IPSDEFIUpdateDetail) => {
            details.push(item.name);
          });
          this.updateFormItems(getPSAppDEMethod?.codeName as string, this.data, details, showBusyIndicator);
        }
      }
    }
  }

  /**
   * 表单项检查逻辑
   *
   * @param {string} name 属性名
   * @memberof MobFormCtrlController
   */
  protected checkItem(name: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      if (!this.rules[name]) {
        resolve(true);
      }
      const validator = new Schema({ [name]: this.rules[name] });
      validator
        .validate({ [name]: this.data[name] })
        .then(() => {
          resolve(true);
        })
        .catch(() => {
          resolve(false);
        });
    });
  }

  /**
   * 校验动态逻辑结果
   *
   * @param {*} data 数据对象
   * @param {*} logic 逻辑对象
   * @returns
   * @memberof MobFormCtrlController
   */
  protected verifyGroupLogic(data: any, logic: IPSDEFDCatGroupLogic): boolean {
    if (logic.logicType == 'GROUP' && logic?.getPSDEFDLogics()?.length) {
      let result: boolean = true;
      if (logic.groupOP == 'AND') {
        const falseItem: IPSDEFDLogic | undefined = logic.getPSDEFDLogics()?.find((childLogic: any) => {
          return !this.verifyGroupLogic(data, childLogic);
        });
        result = falseItem == undefined;
      } else if (logic.groupOP == 'OR') {
        const trueItem: IPSDEFDLogic | undefined = logic.getPSDEFDLogics()?.find((childLogic: any) => {
          return this.verifyGroupLogic(data, childLogic);
        });
        result = trueItem != undefined;
      }
      // 是否取反
      return logic.notMode ? !result : result;
    } else if (logic.logicType == 'SINGLE') {
      const singleLogic: IPSDEFDSingleLogic = logic as any;
      return Verify.testCond(data[singleLogic.dEFDName.toLowerCase()], singleLogic.condOP, singleLogic.value);
    }
    return false;
  }

  /**
   * 重置表单项值
   *
   * @public
   * @param {{ name: string }} { name } 名称
   * @memberof MobFormCtrlController
   */
  protected resetFormData({ name }: { name: string }): void {
    const formItems: IPSDEEditFormItem[] = ModelTool.getAllFormItems(this.controlInstance);
    if (formItems && formItems.length > 0) {
      for (const item of formItems) {
        if (item.resetItemName && item.resetItemName == name) {
          this.onFormItemValueChange({ name: item.name, value: null });
          if (item.valueItemName) {
            this.onFormItemValueChange({ name: item.valueItemName, value: null });
          }
        }
      }
    }
  }

  /**
   * 复合表单项值规则
   *
   * @param detail 复合表单项
   */
  protected getCompositeItemRules(detail: IPSDEEditFormItem) {
    const rules: Array<any> = [];
    if (detail.compositeItem && detail.getPSEditor()?.editorType == 'DATEPICKER') {
      const formItems = (detail as IPSDEFormItemEx).getPSDEFormItems();
      if (formItems && formItems.length > 1) {
        // TODO 移动端暂未支持复合表单项
      }
    }
    return rules;
  }

  /**
   * 执行按钮行为
   *
   * @param {*} formdetail 行为模型
   * @param {*} data 数据
   * @param {*} event 事件源
   * @memberof MobFormCtrlController
   */
  public async executeButtonAction(formdetail: IParam, data: IParam, event: MouseEvent) {
    if (formdetail && formdetail.actionType && Object.is(formdetail.actionType, 'FIUPDATE')) {
      const itemUpdate = formdetail.getPSDEFormItemUpdate();
      const showBusyIndicator = itemUpdate?.showBusyIndicator;
      const getPSAppDEMethod = itemUpdate?.getPSAppDEMethod();
      const getPSDEFIUpdateDetails = itemUpdate?.getPSDEFIUpdateDetails();
      const details: string[] = [];
      getPSDEFIUpdateDetails?.forEach((item: IPSDEFIUpdateDetail) => {
        details.push(item.name);
      });
      if (formdetail.getParamPickupPSAppView()) {
        const pickupview = formdetail.getParamPickupPSAppView();
        await pickupview.fill();
        if (!pickupview) {
          this.updateFormItems(getPSAppDEMethod?.codeName as string, this.data, details, showBusyIndicator);
        } else {
          const tempContext: any = Util.deepCopy(this.context);
          const data: any = Util.deepCopy(this.viewParam);
          if (formdetail?.getPSNavigateContexts() && formdetail.getPSNavigateContexts().length > 0) {
            const _context: any = Util.computedNavData(
              this.data,
              tempContext,
              data,
              formdetail.getPSNavigateContexts(),
            );
            Object.assign(tempContext, _context);
          }
          if (formdetail?.getPSNavigateParams() && formdetail.getPSNavigateParams().length > 0) {
            const _param: any = Util.computedNavData(this.data, tempContext, data, formdetail.getPSNavigateParams());
            Object.assign(data, _param);
          }
          if (pickupview.openMode.indexOf('DRAWER') !== -1) {
            const appdrawer = App.getOpenViewService().openDrawer(
              { viewModel: pickupview, placement: pickupview.openMode },
              tempContext,
              data,
              this.getUIDataParam().navData,
            );
            appdrawer.subscribe((result: any) => {
              if (result && result.ret) {
                const arg: any = this.getData();
                Object.assign(arg, { srfactionparam: result.data });
                this.updateFormItems(getPSAppDEMethod?.codeName as string, arg, details, showBusyIndicator);
              }
            });
          } else {
            const appmodal = App.getOpenViewService().openModal(
              { viewModel: pickupview },
              tempContext,
              data,
              this.getUIDataParam().navData,
            );
            appmodal.subscribe((result: any) => {
              if (result && result.ret) {
                const arg: any = this.getData();
                Object.assign(arg, { srfactionparam: result.data });
                this.updateFormItems(getPSAppDEMethod?.codeName as string, arg, details, showBusyIndicator);
              }
            });
          }
        }
      } else {
        this.updateFormItems(getPSAppDEMethod?.codeName as string, this.data, details, showBusyIndicator);
      }
    } else {
      const targetViewLogic: IPSAppViewLogic | null = this.controlInstance.findPSAppViewLogic(
        `${this.name}_${formdetail.name}_click`,
      );
      const UIDataParam: IUIDataParam = this.getUIDataParam();
      Object.assign(UIDataParam, { event: event });
      AppViewLogicUtil.executeViewLogic(targetViewLogic, UIDataParam, this.getUIEnvironmentParam(), this.viewCtx);
    }
  }

    /**
     * 处理部件UI响应
     *
     * @param {string} action 行为名称
     * @param {*} response 响应
     * @memberof MobFormCtrlController
     */
    public onControlResponse(action: string, response: any) {
        if (Object.is(action, 'load')) {
          this.controlIsLoaded = true;
        }
        if (!Object.is(action, 'updateFormItems')) {
            this.ctrlEndLoading();
        }
        if (response && response.status && response.status == 403) {
            this.enableControlUIAuth = false;
            this.ctrlEvent('authlimit',response);
        }
        if (response && response.status && response.status != 200 && response.data) {
            const data: any = response.data;
            if (data.code && Object.is(AppErrorCode.INPUTERROR, data.code) && data.details?.length > 0) {
                let errorMsg: string = '';
                data.details.forEach((detail: any) => {
                    if (!Object.is(EntityFieldErrorCode.ERROR_OK, detail.fielderrortype) && detail.fieldname) {
                        const tempFormItem: any = this.findFormItemByField(detail.fieldname);
                        if (tempFormItem) {
                            Object.assign(this.detailsModel[tempFormItem.name], { error: new String(detail.fielderrorinfo ? detail.fielderrorinfo : data.message) });
                        } else {
                            errorMsg += `${detail.fieldlogicname}${detail.fielderrorinfo ? detail.fielderrorinfo : data.message}<br/>`;
                        }
                    }
                })
                response.data.message = errorMsg ? errorMsg : '填写信息有误，请检查';
            }
        }
    }

    /**
     * 通过属性名获取表单项
     *
     * @memberof MobFormCtrlController
     */
    public findFormItemByField(fieldName: string) {
        const formItems: IPSDEEditFormItem[] = ModelTool.getAllFormItems(this.controlInstance);
        return formItems.find((formItem: any) => {
            return formItem.getPSAppDEField()?.name == fieldName;
        })
    }

}
