import { IPSAppCounterRef, IPSDETabViewPanel, IPSTabExpPanel } from '@ibiz/dynamic-model-api';
import { ICtrlActionResult, IMobTabExpPanelCtrlController, IParam } from '../../../interface';
import { AppCtrlControllerBase } from './app-ctrl-controller-base';

export class MobTabExpPanelCtrlController extends AppCtrlControllerBase implements IMobTabExpPanelCtrlController {
  /**
   * @description 部件模型实例对象
   * @type {IPSTabExpPanel}
   * @memberof MobTabExpPanelCtrlController
   */
  controlInstance!: IPSTabExpPanel;

  /**
   * @description 激活项
   * @type {string}
   * @memberof MobTabExpPanelCtrlController
   */
  public activeItem: string = '';

  /**
   * @description 部件模型加载
   * @memberof MobTabExpPanelCtrlController
   */
  public async ctrlModelLoad() {
    await super.ctrlModelLoad();
    const controls = this.controlInstance.getPSControls() || [];
    for (const control of controls) {
      await (control as IPSDETabViewPanel).getEmbeddedPSAppDEView?.()?.fill?.();
    }
  }

  /**
   * 部件基础数据初始化
   *
   * @memberof MobTabExpPanelCtrlController
   */
  public ctrlBasicInit() {
    super.ctrlBasicInit();
    this.initActiveItem();
  }

  /**
   * @description 初始化激活项
   * @private
   * @memberof MobTabExpPanelCtrlController
   */
  private initActiveItem() {
    const allControls = this.controlInstance.getPSControls() as IPSDETabViewPanel[];
    this.activeItem = allControls[0].name;
    if (this.viewParam && (this.viewParam.srftabactivate || this.viewParam.srfnavtag)) {
      const activate = this.viewParam.srftabactivate || this.viewParam.srfnavtag;
      if (activate) {
        this.activeItem = activate;
      }
    } 
  }

  /**
   * @description 部件初始化
   * @param {*} [args] 初始化参数
   * @memberof MobTabExpPanelCtrlController
   */
  public ctrlInit(args?: any) {
    super.ctrlInit(args);
    if (App.isPreviewMode()) {
      return;
    }    
    //设置分页导航srfparentdename和srfparentkey
    if (this.context[this.appDeCodeName.toLowerCase()]) {
        Object.assign(this.context, { srfparentdename: this.appDeCodeName.toLowerCase(),srfparentdemapname: this.deName,srfparentkey: this.context[this.appDeCodeName.toLowerCase()] })
    }
  }

  /**
   * 刷新
   *
   * @param opts 行为参数
   * @param args 补充逻辑完成参数
   * @param showInfo 是否显示提示信息
   * @param loadding 是否显示loading效果
   * @returns {Promise<IParam>}
   * @memberof MobTabExpPanelCtrlController
   */
  public async refresh(
    opts?: IParam,
    args?: IParam,
    showInfo: boolean = true,
    loadding: boolean = true,
  ): Promise<ICtrlActionResult> {
    return new Promise((resolve: any) => {
      this.viewState.next({ tag: this.activeItem, action: 'refresh', data: opts });
      resolve({ ret: true, data: null });
    });
  }

  /**
   * @description 获取计数器数据
   * @param {IPSDETabViewPanel} viewPanel
   * @return {string | number | null}  {(string | number | null)}
   * @memberof MobTabExpPanelCtrlController
   */
  public getCounterData(viewPanel: IPSDETabViewPanel): string | number | null {
    const counterRef = viewPanel.getPSAppCounterRef() as IPSAppCounterRef;
    const counterService = this.getCounterService(counterRef);
    if (counterService && viewPanel.counterId) {
      return counterService?.counterData?.[viewPanel.counterId.toLowerCase()];
    }
    return null;
  }

  /**
   * @description 激活项改变
   * @param {string} activeItem 激活项
   * @memberof MobTabExpPanelCtrlController
   */
  public activeItemChange(activeItem: string) {
    this.activeItem = activeItem;
  }
}
