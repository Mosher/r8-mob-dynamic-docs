import { AppExpBarCtrlEvents, IMobChartExpBarController, MobChartEvents } from '../../../interface';
import { IPSAppDataEntity, IPSChartExpBar, IPSDEChart, IPSDEChartSeries, IPSAppDEView } from '@ibiz/dynamic-model-api';
import { AppExpBarCtrlController } from './app-exp-bar-ctrl-controller';
import { ModelTool, Util, ViewTool } from '../../../util';

export class MobChartExpBarController extends AppExpBarCtrlController implements IMobChartExpBarController {

  /**
   * 移动端图表导航栏实例
   *
   * @public
   * @type { IPSDEMobChartExpBar }
   * @memberof MobChartExpBarController
   */
  public controlInstance!: IPSChartExpBar;

  public handleCtrlEvent(controlname: string, action: string, data: any) {
    if (controlname === this.xDataControlName && action == MobChartEvents.SELECT_CHANGE) {
      this.onSelectionChange(data);
    } else {
      super.handleCtrlEvent(controlname, action, data);
    }
  }

  /**
 * 图表部件选中数据变化
 * 
 * @memberof ChartExpBarControlBase
 */
  public onSelectionChange(args: any): void {
    let tempContext: any = {};
    let tempViewParam: any = {};
    if (args.length === 0) {
      this.calcToolbarItemState(true);
      return;
    }
    const arg: any = args[0];
    if (this.context) {
      Object.assign(tempContext, Util.deepCopy(this.context));
    }
    const seriesItem: IPSDEChartSeries | null | undefined = ((this.xDataControl as IPSDEChart).getPSDEChartSerieses() || []).find((item: IPSDEChartSeries) => {
      return item.name.toLowerCase() === arg._chartName.toLowerCase();
    });
    const appDataEntity: IPSAppDataEntity | null = this.xDataControl?.getPSAppDataEntity();
    if (seriesItem && appDataEntity) {
      Object.assign(tempContext, { [appDataEntity.codeName?.toLowerCase()]: arg[(ModelTool.getAppEntityKeyField(appDataEntity) as any)?.codeName?.toLowerCase()] });
      Object.assign(tempContext, { srfparentdename: appDataEntity.codeName, srfparentdemapname: (appDataEntity as any)?.getPSDEName(), srfparentkey: arg[appDataEntity.codeName?.toLowerCase()] });
      //  分类属性
      if (seriesItem.catalogField) {
        Object.assign(tempContext, { [seriesItem.catalogField]: arg[seriesItem.catalogField] });
        Object.assign(tempViewParam, { [seriesItem.catalogField]: arg[seriesItem.catalogField] });
      }
      //  数据属性
      if (seriesItem.valueField) {
        Object.assign(tempContext, { [seriesItem.valueField]: arg[seriesItem.valueField] });
        Object.assign(tempViewParam, { [seriesItem.valueField]: arg[seriesItem.valueField] });
      }
      if (this.navFilter && this.navFilter[arg._chartName] && !Object.is(this.navFilter[arg._chartName], "")) {
        Object.assign(tempViewParam, { [this.navFilter[arg._chartName]]: arg[appDataEntity.codeName?.toLowerCase()] });
      }
      if (this.navPSDer && this.navFilter[arg._chartName] && !Object.is(this.navPSDer[arg._chartName], "")) {
        Object.assign(tempViewParam, { [this.navPSDer[arg._chartName]]: arg[appDataEntity.codeName?.toLowerCase()] });
      }
      if (this.navParam && this.navParam[arg._chartName] && this.navParam[arg._chartName].navigateContext && Object.keys(this.navParam[arg._chartName].navigateContext).length > 0) {
        let _context: any = Util.computedNavData(arg, tempContext, tempViewParam, this.navParam[arg._chartName].navigateContext);
        Object.assign(tempContext, _context);
      }
      if (this.navParam && this.navParam[arg._chartName] && this.navParam[arg._chartName].navigateParams && Object.keys(this.navParam[arg._chartName].navigateParams).length > 0) {
        let _params: any = Util.computedNavData(arg, tempContext, tempViewParam, this.navParam[arg._chartName].navigateParams);
        Object.assign(tempViewParam, _params);
      }
      const navView = seriesItem?.getNavPSAppView();
      if (navView) {
        this.openExplorerView(navView, args, tempContext, tempViewParam);
      }
    }
    this.calcToolbarItemState(false);
    this.ctrlEvent(AppExpBarCtrlEvents.SELECT_CHANGE, args);
  }

}
