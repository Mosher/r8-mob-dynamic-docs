import { IPSAppMenu } from '@ibiz/dynamic-model-api';
import { IMobMenuCtrlController } from '../../../interface';
import { AppCtrlControllerBase } from './app-ctrl-controller-base';

export class MobMenuCtrlController extends AppCtrlControllerBase implements IMobMenuCtrlController {
  /**
   * 移动端菜单部件实例对象
   *
   * @type {IPSAppMenu}
   * @memberof MobMDCtrlController
   */
  public controlInstance!: IPSAppMenu;
}
