import { AppCtrlControllerBase } from './app-ctrl-controller-base';
import { IMobPickUpViewPanelCtrlController, AppViewEvents, MobPickupViewPanelEvents, IParam } from '../../../interface';
import { IPSDEPickupViewPanel } from '@ibiz/dynamic-model-api';
export class MobPickUpViewPanelCtrlController
  extends AppCtrlControllerBase
  implements IMobPickUpViewPanelCtrlController
{
  /**
   * @description 选择面板部件实例对象
   * @type {IPSDEPickupViewPanel}
   * @memberof MobPickUpViewPanelCtrlController
   */
  public controlInstance!: IPSDEPickupViewPanel;

  /**
   * @description 是否多选
   * @type {boolean}
   * @memberof MobPickUpViewPanelCtrlController
   */
  public isMultiple: boolean = false;

  /**
   * @description 初始化输入参数
   * @param {*} [opts] 输入参数
   * @memberof MobPickUpViewPanelCtrlController
   */
  public initInputData(opts?: any) {
    super.initInputData(opts);
    this.isMultiple = opts.isMultiple;
  }

  /**
   * @description 处理选择视图事件
   * @param {string} action 视图行为行为
   * @param {*} data 数据
   * @memberof MobPickUpViewPanelCtrlController
   */
  public handlePickUPViewEvent(action: string, data: IParam) {
    switch (action) {
      case AppViewEvents.DATA_CHANGE:
        this.ctrlEvent(MobPickupViewPanelEvents.SELECT_CHANGE, data);
        break;
      default:
        console.log(`${action}暂未识别`);
    }
  }
}
