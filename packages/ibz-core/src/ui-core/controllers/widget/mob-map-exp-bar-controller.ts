import { IPSAppDataEntity, IPSMapExpBar, IPSSysMap, IPSSysMapItem } from '@ibiz/dynamic-model-api';
import { AppExpBarCtrlEvents, IMobMapExpBarController, MobMapEvents } from '../../../interface';
import { AppExpBarCtrlController } from './app-exp-bar-ctrl-controller';
import { ModelTool, Util, ViewTool } from '../../../util';
export class MobMapExpBarController extends AppExpBarCtrlController implements IMobMapExpBarController {

  /**
   * 移动端地图导航部件实例
   *
   * @public
   * @type { IPSDEMobMapExpBar }
   * @memberof MobMapExpBarController
   */
  public controlInstance!: IPSMapExpBar;

  /**
   * @description 处理部件事件
   * @param {string} controlname 部件名称
   * @param {string} action 部件行为
   * @param {*} data 数据
   * @memberof MobMapExpBarController
   */
  public handleCtrlEvent(controlname: string, action: string, data: any) {
    if (controlname === this.xDataControlName && action == MobMapEvents.SELECT_CHANGE) {
      this.onSelectionChange(data);
    } else {
      super.handleCtrlEvent(controlname, action, data);
    }
  }

  /**
* 地图部件的选中数据事件
* 
*
* @param {any[]} args 选中数据
* @return {*}  {void}
* @memberof MapExpBarControlBase
*/
  public onSelectionChange(args: any): void {
    let tempContext: any = {};
    let tempViewParam: any = {};
    if (args.length === 0) {
      this.calcToolbarItemState(true);
      return;
    }
    const arg: any = args[0];
    if (this.context) {
      Object.assign(tempContext, Util.deepCopy(this.context));
    }
    const mapItem: IPSSysMapItem | null | undefined = ((this.xDataControl as IPSSysMap).getPSSysMapItems() || []).find((item: IPSSysMapItem) => {
      return item.itemType === arg.itemType;
    });
    const mapItemEntity: IPSAppDataEntity | null | undefined = mapItem?.getPSAppDataEntity();
    if (mapItem && mapItemEntity) {
      Object.assign(tempContext, { [mapItemEntity.codeName?.toLowerCase()]: arg[mapItemEntity.codeName?.toLowerCase()] });
      Object.assign(tempContext, { srfparentdename: mapItemEntity.codeName, srfparentdemapname: (mapItemEntity as any)?.getPSDEName(), srfparentkey: arg[mapItemEntity.codeName?.toLowerCase()] });
      if (this.navFilter && this.navFilter[arg.itemType] && !Object.is(this.navFilter[arg.itemType], "")) {
        Object.assign(tempViewParam, { [this.navFilter[arg.itemType]]: arg[mapItemEntity.codeName?.toLowerCase()] });
      }
      if (this.navPSDer && this.navFilter[arg.itemType] && !Object.is(this.navPSDer[arg.itemType], "")) {
        Object.assign(tempViewParam, { [this.navPSDer[arg.itemType]]: arg[mapItemEntity.codeName?.toLowerCase()] });
      }
      if (this.navParam && this.navParam[arg.itemType] && this.navParam[arg.itemType].navigateContext && Object.keys(this.navParam[arg.itemType].navigateContext).length > 0) {
        let _context: any = Util.computedNavData(arg.curdata, tempContext, tempViewParam, this.navParam[arg.itemType].navigateContext);
        Object.assign(tempContext, _context);
      }
      if (this.navParam && this.navParam[arg.itemType] && this.navParam[arg.itemType].navigateParams && Object.keys(this.navParam[arg.itemType].navigateParams).length > 0) {
        let _params: any = Util.computedNavData(arg.curdata, tempContext, tempViewParam, this.navParam[arg.itemType].navigateParams);
        Object.assign(tempViewParam, _params);
      }
      const navView = mapItem?.getNavPSAppView();
      if (navView) {
        if (navView) {
          this.openExplorerView(navView, args, tempContext, tempViewParam);
        }
      }
    }
    this.calcToolbarItemState(false);
    this.ctrlEvent(AppExpBarCtrlEvents.SELECT_CHANGE, args);
  }
}
