import {
  ICtrlActionResult,
  IMobChartCtrlController,
  IParam,
  MobChartEvents,
  IViewStateParam,
  IUIDataParam,
} from '../../../interface';
import { AppMDCtrlController } from './app-md-ctrl-controller';
import { IPSDEChart } from '@ibiz/dynamic-model-api';
import { AppMobChartService } from '../../ctrl-service';
import { Util } from '../../../util';
export class MobChartCtrlController extends AppMDCtrlController implements IMobChartCtrlController {

  /**
   * @description 图表实例模型对象
   * @type {IPSDEChart}
   * @memberof MobChartCtrlController
   */
  public controlInstance!: IPSDEChart;

  /**
   * @description 图标唯一id
   * @type {string}
   * @memberof MobChartCtrlController
   */
  public chartId: string = Util.createUUID();

  /**
   * @description 是否无数据
   * @type {boolean}
   * @memberof MobChartCtrlController
   */
  public isNoData: boolean = true;

  /**
   * @description echarts图表对象
   * @type {*}
   * @memberof MobChartCtrlController
   */
  public myChart: any;

  /**
   * @description 序列模型
   * @type {IParam}
   * @memberof MobChartCtrlController
   */
  public seriesModel: IParam = {};

  /**
   * @description 图表自定义参数集合
   * @type {IParam}
   * @memberof MobChartCtrlController
   */
  public chartUserParams: IParam = {};

  /**
   * @description 图表基础动态模型
   * @type {IParam}
   * @memberof MobChartCtrlController
   */
  public chartBaseOPtion: IParam = {};

  /**
   * @description 图表绘制最终参数
   * @type {IParam}
   * @memberof MobChartCtrlController
   */
  public chartRenderOption: IParam = {};

  /**
   * @description 初始化图表所需参数
   * @type {IParam}
   * @memberof MobChartCtrlController
   */
  public chartOption: IParam = {};

  /**
  * @description 部件模型数据初始化实例
  * @param {*} [args] 初始化参数
  * @memberof MobChartCtrlController
  */
  public async ctrlModelInit(args?: any) {
    await super.ctrlModelInit(args);
    if (!App.isPreviewMode()) {
      this.ctrlService = new AppMobChartService(this.controlInstance);
      await this.ctrlService.loaded();
    }
  }

  /**
   * @description 初始化图表基础动态模型
   * @private
   * @memberof MobChartCtrlController
   */
  private initChartBaseOPtion() {
    this.chartBaseOPtion = new Function('return {' + this.controlInstance?.baseOptionJOString + '}')();
  }

  /**
   * @description 部件基础数据初始化
   * @memberof MobChartCtrlController
   */
  public ctrlBasicInit() {
    super.ctrlBasicInit();
    this.initChartBaseOPtion();
    if (App.isPreviewMode()) {
      setTimeout(() => {
        const successUIDataParam: IUIDataParam = this.getUIDataParam();
        this.ctrlEvent(MobChartEvents.LOAD_SUCCESS, successUIDataParam);
      }, 500);
    }    
  }

  /**
   * @description 获取多项数据
   * @return {*}  {any[]}
   * @memberof MobChartCtrlController
   */
  public getDatas(): any[] {
    return this.items;
  }

  /**
   * @description 部件初始化
   * @memberof MobChartCtrlController
   */
  public ctrlInit() {
    super.ctrlInit();
    if (this.viewState) {
      this.viewStateEvent = this.viewState.subscribe(({ tag, action, data }: IViewStateParam) => {
        if (!Object.is(tag, this.name)) {
          return;
        }
        if (Object.is('load', action)) {
          this.load(data);
        }
        if (Object.is('search', action)) {
          this.load(data);
        }
        if (Object.is('refresh', action)) {
          this.refresh(data);
        }
      });
    }
  }

  /**
   * @description 刷新
   * @param {IParam} [opts] 行为参数
   * @param {IParam} [args] 额外行为参数
   * @param {boolean} [showInfo] 是否显示提示
   * @param {boolean} [loadding] 是否显示loadding效果
   * @return {*}  {Promise<ICtrlActionResult>}
   * @memberof MobChartCtrlController
   */
  public refresh(opts?: IParam, args?: IParam, showInfo?: boolean, loadding?: boolean): Promise<ICtrlActionResult> {
    return this.load(opts, args, showInfo, loadding);
  }

  /**
   * @description 加载
   * @param {IParam} [opts] 行为参数
   * @param {IParam} [args] 额外行为参数
   * @param {boolean} [showInfo=true] 是否显示提示
   * @param {boolean} [loadding=true] 是否显示loadding效果
   * @return {*}  {Promise<ICtrlActionResult>}
   * @memberof MobChartCtrlController
   */
  public async load(
    opts?: IParam,
    args?: IParam,
    showInfo: boolean = true,
    loadding: boolean = true,
  ): Promise<ICtrlActionResult> {
    const arg: any = {};
    Object.assign(arg, { viewParam: Object.assign(opts, this.viewParam) });
    Object.assign(arg, { page: 0, size: 1000 });
    if (this.controlInstance.minorSortDir && this.controlInstance.getMinorSortPSAppDEField()?.codeName) {
      Object.assign(arg, {
        sort: `${this.controlInstance
          .getMinorSortPSAppDEField()
          ?.codeName?.toLowerCase()},${this.controlInstance.minorSortDir?.toLowerCase()}`,
      });
    }
    const tempContext: any = Util.deepCopy(this.context);
    const beforeUIDataParam: IUIDataParam = this.getUIDataParam();
    Object.assign(beforeUIDataParam, { action: this.fetchAction, navContext: tempContext, navParam: arg });
    const beforeloadResult: any = await this.executeCtrlEventLogic(MobChartEvents.BEFORE_LOAD, beforeUIDataParam);
    if (beforeloadResult && beforeloadResult?.hasOwnProperty('srfret') && !beforeloadResult.srfret) {
      return Promise.resolve({ ret: false, data: null });
    }
    this.ctrlEvent(MobChartEvents.BEFORE_LOAD, beforeUIDataParam);
    this.onControlRequset('load', tempContext, arg, loadding);
    const response = await this.ctrlService.search(this.fetchAction, tempContext, arg, loadding);
    this.onControlResponse('load', response);
    if (response && response.status === 200) {
      this.items = response.data;
      const successUIDataParam: IUIDataParam = this.getUIDataParam();
      Object.assign(successUIDataParam, { action: this.loadAction, navContext: tempContext, navParam: arg });
      const loadSuccessResult: any = await this.executeCtrlEventLogic(MobChartEvents.LOAD_SUCCESS, successUIDataParam);
      if (loadSuccessResult && loadSuccessResult?.hasOwnProperty('srfret') && !loadSuccessResult.srfret) {
        return Promise.resolve({ ret: false, data: null });
      }
      this.ctrlEvent(MobChartEvents.LOAD_SUCCESS, successUIDataParam);
      return Promise.resolve({ ret: true, data: this.items });
    } else {
      const errorUIDataParam: IUIDataParam = this.getUIDataParam();
      Object.assign(errorUIDataParam, { action: this.loadAction, navContext: tempContext, navParam: arg });
      const loadErrorResult: any = await this.executeCtrlEventLogic(MobChartEvents.LOAD_ERROR, errorUIDataParam);
      if (loadErrorResult && loadErrorResult?.hasOwnProperty('srfret') && !loadErrorResult.srfret) {
        return Promise.resolve({ ret: false, data: null });
      }
      this.ctrlEvent(MobChartEvents.LOAD_ERROR, errorUIDataParam);
      return Promise.resolve({ ret: false, data: null });
    }
  }
}
