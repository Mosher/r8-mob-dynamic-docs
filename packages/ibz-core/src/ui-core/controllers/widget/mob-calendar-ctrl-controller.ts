import { AppMDCtrlController } from './app-md-ctrl-controller';
import {
  IMobCalendarCtrlController,
  IParam,
  ICtrlActionResult,
  IViewStateParam,
  IUIDataParam,
  MobCalendarEvents,
} from '../../../interface';
import {
  IPSAppDataEntity,
  IPSAppDEMobCalendarView,
  IPSAppDEView,
  IPSAppViewRef,
  IPSDECalendar,
  IPSDECMUIActionItem,
  IPSDETBUIActionItem,
  IPSDEToolbarItem,
  IPSDEUIAction,
  IPSSysCalendar,
  IPSSysCalendarItem,
} from '@ibiz/dynamic-model-api';
import { AppMobCalendarService } from '../../ctrl-service';
import { ModelTool, Util, ViewTool } from '../../../util';
import { Subject } from 'rxjs';
import moment from 'moment';
export class MobCalendarCtrlController extends AppMDCtrlController implements IMobCalendarCtrlController {
  /**
   * @description 日历部件模型实例
   * @type {IPSDECalendar}
   * @memberof MobCalendarCtrlController
   */
  public controlInstance!: IPSDECalendar;

  /**
   * @description 开始时间
   * @type {*}
   * @memberof MobCalendarCtrlController
   */
  public start: string = '';

  /**
   * @description 结束时间
   * @type {string}
   * @memberof MobCalendarCtrlController
   */
  public end: string = '';

  /**
   * @description 标记数据
   * @type {IParam[]}
   * @memberof MobCalendarCtrlController
   */
  public sign: IParam[] = [];

  /**
   * @description 日历项集合对象
   * @type {*}
   * @memberof MobCalendarCtrlController
   */
  public calendarItems: IParam = {};

  /**
   * @description 是否展示多选
   * @type {boolean}
   * @memberof MobCalendarCtrlController
   */
  public isChoose: boolean = false;

  /**
   * @description 激活项
   * @type {string}
   * @memberof MobCalendarCtrlController
   */
  public activeItem: string = '';

  /**
   * @description 日历数据项模型
   * @private
   * @type {Map<string, any>}
   * @memberof MobCalendarCtrlController
   */
  private calendarItemsModel: Map<string, any> = new Map([]);

  /**
   * @description 日历样式类型
   * @type {string}
   * @memberof MobCalendarCtrlController
   */
  public calendarStyle!: string;

  /**
   * @description 图标信息
   * @type {IParam[]}
   * @memberof MobCalendarCtrlController
   */
  public illustration: IParam[] = [];

  /**
   * @description 当前时间
   * @type {*}
   * @memberof MobCalendarCtrlController
   */
  public curTime: IParam = {};

  /**
   * @description 日历部件模型数据初始化实例
   * @param {*} [args] 初始化参数
   * @memberof MobCalendarCtrlController
   */
  public async ctrlModelInit(args?: any) {
    await super.ctrlModelInit(args);
    if (!App.isPreviewMode()) {
    this.ctrlService = new AppMobCalendarService(this.controlInstance);
    await this.ctrlService.loaded();
  }
  }

  /**
   * @description 日历部件模型数据加载
   * @memberof MobCalendarCtrlController
   */
  public async ctrlModelLoad() {
    await super.ctrlModelLoad();
    const calendarItems = (this.controlInstance as IPSSysCalendar).getPSSysCalendarItems() || [];
    if (!App.isPreviewMode()) {
      if (calendarItems.length > 0) {
        for (const item of calendarItems) {
          await item.getPSAppDataEntity?.()?.fill?.();
          await item.getNavPSAppView?.()?.fill?.();
        }
      }
    }
  }

  /**
   * @description 日历部件基础数据初始化
   * @memberof MobCalendarCtrlController
   */
  public ctrlBasicInit() {
    super.ctrlBasicInit();
    this.calendarStyle = (this.controlInstance as IPSSysCalendar).calendarStyle;
    this.initCalendarItemsModel();
    // TODO 图例 暂时注释
    // this.initIllustration();
    this.initActiveItem();
    if (App.isPreviewMode()) {
      this.calendarItems = {
        [this.activeItem]: this.items,
      }
    }
  }

  /**
   * @description 日历视图部件初始化
   * @memberof MobCalendarCtrlController
   */
  public ctrlInit() {
    super.ctrlInit();
    if (this.viewState) {
      this.viewStateEvent = this.viewState.subscribe(({ tag, action, data }: IViewStateParam) => {
        if (!Object.is(this.name, tag)) {
          return;
        }
        if (Object.is(action, 'load')) {
          this.formatDate(new Date(), data);
          this.setDefaultSelect();
        }
        if (Object.is(action, 'search')) {
          this.formatDate(this.curTime, data);
        }
      });
    }
  }

  /**
   * @description 初始化日历部件上下文菜单模型
   * @memberof MobCalendarCtrlController
   */
  public initCtrlActionModel() {
    const calendarItems = (this.controlInstance as IPSSysCalendar).getPSSysCalendarItems() || [];
    const tempModel: any = {};
    if (calendarItems?.length > 0) {
      calendarItems.forEach((item: IPSSysCalendarItem) => {
        if (item.getPSDEContextMenu() && item.getPSDEContextMenu()?.getPSDEToolbarItems()) {
          const toobarItems = item.getPSDEContextMenu()?.getPSDEToolbarItems() as IPSDEToolbarItem[];
          if (toobarItems?.length > 0) {
            toobarItems.forEach((toolbarItem: IPSDEToolbarItem) => {
              this.initActionModelItem(toolbarItem, item, tempModel);
            });
          }
        }
      });
    }
    this.actionModel = {};
    Object.assign(this.actionModel, tempModel);
  }

  /**
   * @description 初始化上下菜单项
   * @private
   * @param {IPSDEToolbarItem} toolbarItem 工具栏项
   * @param {IPSSysCalendarItem} item 日历项
   * @param {*} tempModel 模型数据
   * @memberof MobCalendarCtrlController
   */
  private initActionModelItem(toolbarItem: IPSDEToolbarItem, item: IPSSysCalendarItem, tempModel: any) {
    const tempItem: any = {
      name: toolbarItem.name,
      nodeOwner: item.itemType,
    };
    if (toolbarItem.itemType == 'DEUIACTION') {
      const uiAction: IPSDEUIAction = (toolbarItem as IPSDECMUIActionItem).getPSUIAction() as IPSDEUIAction;
      tempItem.type = item.itemType;
      tempItem.tag = uiAction.id;
      tempItem.visabled = true;
      tempItem.disabled = false;
      if (uiAction?.actionTarget && uiAction?.actionTarget != '') {
        tempItem.actiontarget = uiAction.actionTarget;
      }
      if (uiAction.noPrivDisplayMode) {
        tempItem.noprivdisplaymode = uiAction.noPrivDisplayMode;
      }
      if (uiAction.dataAccessAction) {
        tempItem.dataaccaction = uiAction.dataAccessAction;
      }
    }
    tempModel[`${item.itemType}_${toolbarItem.name}`] = tempItem;
    const toolbarItems = (toolbarItem as IPSDETBUIActionItem)?.getPSDEToolbarItems() || [];
    if (toolbarItems?.length > 0) {
      for (const toolBarChild of toolbarItems) {
        this.initActionModelItem(toolBarChild, item, tempModel);
      }
    }
  }

  /**
   * @description 初始化日历数据项模型
   * @private
   * @memberof MobCalendarCtrlController
   */
  private initCalendarItemsModel() {
    const calendarItems = (this.controlInstance as IPSSysCalendar).getPSSysCalendarItems() || [];
    if (calendarItems.length > 0) {
      calendarItems.forEach((calendarItem: IPSSysCalendarItem) => {
        const _appde = calendarItem.getPSAppDataEntity();
        const idField = calendarItem.getIdPSAppDEField();
        const keyField = ModelTool.getAppEntityKeyField(_appde);
        const majorField = ModelTool.getAppEntityMajorField(_appde);
        const itemType = calendarItem.itemType.toLowerCase();
        const obj = {
          appde: _appde?.codeName?.toLowerCase(),
          keyPSAppDEField: idField?.codeName ? idField.codeName.toLowerCase() : keyField?.codeName?.toLowerCase(),
          majorPSAppDEField: majorField ? majorField?.codeName.toLowerCase() : 'srfmajortext',
        };
        this.calendarItemsModel.set(itemType, obj);
      });
    }
  }

  /**
   * @description 初始化图标信息
   * @private
   * @memberof MobCalendarCtrlController
   */
  private initIllustration() {
    const calendarItems = (this.controlInstance as IPSSysCalendar).getPSSysCalendarItems() || [];
    if (calendarItems?.length > 0) {
      calendarItems.forEach((item: IPSSysCalendarItem) => {
        const tempIllustration: any = {};
        tempIllustration['color'] = item.bKColor;
        tempIllustration['text'] = item.name;
        this.illustration.push(tempIllustration);
      });
    }
  }

  /**
   * @description 初始化激活项
   * @private
   * @memberof MobCalendarCtrlController
   */
  private initActiveItem() {
    const calendarItems = (this.controlInstance as IPSSysCalendar).getPSSysCalendarItems() || [];
    if (calendarItems && calendarItems[0]) {
      this.activeItem = calendarItems[0].itemType.toLowerCase();
    }
  }

  /**
   * @description 加载数据
   * @param {IParam} [opts] 行为参数
   * @param {IParam} [args] 附加行为参数
   * @param {boolean} [isSetEvent=true] 是否设置事程
   * @param {boolean} [loadding=true] 是否显示loadding效果
   * @return {*}  {Promise<ICtrlActionResult>}
   * @memberof MobCalendarCtrlController
   */
  public async load(
    opts?: IParam,
    args?: IParam,
    isSetEvent: boolean = true,
    loadding: boolean = true,
  ): Promise<ICtrlActionResult> {
    if (App.isPreviewMode()) {
      return Promise.resolve({ ret: false, data: null });
    }
    const arg: any = {};
    Object.assign(arg, { viewParam: Object.assign(opts, this.viewParam) });
    Object.assign(arg, args);
    const tempContext: any = Util.deepCopy(this.context);
    const beforeUIDataParam: IUIDataParam = this.getUIDataParam();
    Object.assign(beforeUIDataParam, { action: 'Get', navContext: tempContext, navParam: arg });
    const beforeLoadResult: any = await this.executeCtrlEventLogic(MobCalendarEvents.BEFORE_LOAD, beforeUIDataParam);
    if (beforeLoadResult && beforeLoadResult?.hasOwnProperty('srfret') && !beforeLoadResult.srfret) {
      return Promise.resolve({ ret: false, data: null });
    }
    this.ctrlEvent(MobCalendarEvents.BEFORE_LOAD, beforeUIDataParam);
    this.onControlRequset('load', tempContext, arg, loadding);
    const response: any = await this.ctrlService.search(this.activeItem, tempContext, arg, loadding);
    this.onControlResponse('load', response);
    if (response && response.status === 200) {
      const successUIDataParam: IUIDataParam = this.getUIDataParam();
      Object.assign(successUIDataParam, { action: 'Get', navContext: tempContext, navParam: arg });
      const loadSuccessResult: any = await this.executeCtrlEventLogic(
        MobCalendarEvents.LOAD_SUCCESS,
        successUIDataParam,
      );
      if (loadSuccessResult && loadSuccessResult?.hasOwnProperty('srfret') && !loadSuccessResult.srfret) {
        return Promise.resolve({ ret: false, data: null });
      }
      this.calendarItems = response.data;
      isSetEvent ? this.setEventData() : false;
      this.ctrlEvent(MobCalendarEvents.LOAD_SUCCESS, successUIDataParam);
      if (this.isEnableGroup && Object.keys(this.calendarItems).length > 0) {
        this.items = [];
        let items: any[] = [];
        for (const key in this.calendarItems) {
          items = items.concat(this.calendarItems[key]);
        }
        this.items = [...items];
        this.getGroupData();
      }
      return Promise.resolve({ ret: true, data: [response.data] });
    } else {
      const errorUIDataParam: IUIDataParam = this.getUIDataParam();
      Object.assign(errorUIDataParam, { action: 'Get', navContext: tempContext, navParam: arg, data: response?.data });
      const loadErrorResult: any = await this.executeCtrlEventLogic(MobCalendarEvents.LOAD_ERROR, errorUIDataParam);
      if (loadErrorResult && loadErrorResult?.hasOwnProperty('srfret') && !loadErrorResult.srfret) {
        return Promise.resolve({ ret: false, data: null });
      }
      this.ctrlEvent(MobCalendarEvents.LOAD_ERROR, errorUIDataParam);
      return Promise.resolve({ ret: false, data: null });
    }
  }

  /**
   * @description 删除
   * @param {IParam} [opts] 行为参数
   * @param {IParam} [args] 附加行为参数
   * @param {boolean} [showInfo=true] 是否显示提示信息
   * @param {boolean} [loadding=true] 是否显示loadding效果
   * @return {*}  {Promise<ICtrlActionResult>}
   * @memberof MobCalendarCtrlController
   */
  public async remove(
    opts?: IParam,
    args?: IParam,
    showInfo: boolean = true,
    loadding: boolean = true,
  ): Promise<ICtrlActionResult> {
    const calendarItemModel: any = this.calendarItemsModel.get(this.activeItem);
    const {
      appde,
      keyPSAppDEField,
      majorPSAppDEField,
    }: { appde: string; keyPSAppDEField: string; majorPSAppDEField: string } = calendarItemModel;
    const arg: any = {};
    const keys: Array<string> = [];
    let infoStr: string = '';
    const datas = { ...opts };
    datas.forEach((data: any, index: number) => {
      keys.push(data[keyPSAppDEField]);
      if (index < 5) {
        if (!Object.is(infoStr, '')) {
          infoStr += '、';
        }
        infoStr += data[majorPSAppDEField];
      }
    });
    if (datas.length < 5) {
      infoStr = infoStr + '共' + datas.length + '数据';
    } else {
      infoStr = infoStr + '...共' + datas.length + '数据';
    }
    return new Promise((resolve, reject) => {
      const _remove = async () => {
        const _context: any = { [appde]: keys.join(';') };
        Object.assign(arg, { viewparams: this.viewParam });
        const tempContext: any = Util.deepCopy(this.context);
        Object.assign(tempContext, _context);
        const beforeUIDataParam: IUIDataParam = this.getUIDataParam();
        Object.assign(beforeUIDataParam, { action: this.removeAction, navContext: tempContext, navParam: arg });
        const beforeRemoveResult: any = await this.executeCtrlEventLogic(
          MobCalendarEvents.BEFORE_REMOVE,
          beforeUIDataParam,
        );
        if (beforeRemoveResult && beforeRemoveResult?.hasOwnProperty('srfret') && !beforeRemoveResult.srfret) {
          return resolve({ ret: false, data: null });
        }
        this.ctrlEvent(MobCalendarEvents.BEFORE_REMOVE, beforeUIDataParam);
        this.onControlRequset('remove', tempContext, arg, loadding);
        const response: any = await this.ctrlService.delete(this.activeItem, tempContext, arg, loadding);
        this.onControlResponse('remove', response);
        if (response && response.status === 200) {
          const successUIDataParam: IUIDataParam = this.getUIDataParam();
          Object.assign(successUIDataParam, { action: this.removeAction, navContext: tempContext, navParam: arg });
          const removeSuccessResult: any = await this.executeCtrlEventLogic(
            MobCalendarEvents.REMOVE_SUCCESS,
            successUIDataParam,
          );
          if (removeSuccessResult && removeSuccessResult?.hasOwnProperty('srfret') && !removeSuccessResult.srfret) {
            return resolve({ ret: false, data: null });
          }
          this.ctrlEvent(MobCalendarEvents.REMOVE_SUCCESS, successUIDataParam);
          if (showInfo) {
            App.getNoticeService().success('删除成功！');
          }
          resolve({ ret: true, data: response.data });
        } else {
          const errorUIDataParam: IUIDataParam = this.getUIDataParam();
          Object.assign(errorUIDataParam, { action: this.removeAction, navContext: tempContext, navParam: arg });
          const removeErrorResult: any = await this.executeCtrlEventLogic(
            MobCalendarEvents.REMOVE_ERROR,
            errorUIDataParam,
          );
          if (removeErrorResult && removeErrorResult?.hasOwnProperty('srfret') && !removeErrorResult.srfret) {
            return resolve({ ret: false, data: null });
          }
          this.ctrlEvent(MobCalendarEvents.REMOVE_ERROR, errorUIDataParam);
          if (showInfo) {
            App.getNoticeService().error(`删除失败！`);
          }
          resolve({ ret: false, data: null });
        }
      };
      const options = {
        title: '警告',
        content: `删除${infoStr}不可恢复`,
        buttonType: 'okcancel',
      };
      const subject: Subject<any> | null = App.getMsgboxService().open(options);
      const subscription = subject?.subscribe((result: any) => {
        if (Object.is('ok', result)) {
          _remove();
        }
        subscription!.unsubscribe();
        subject.complete();
      });
    });
  }

  /**
   * @description 格式化时间（根据日历样式格式化开始时间和结束时间）
   * @param {*} curtime 当前时间
   * @param {IParam} [opts] 行为参数
   * @param {IParam} [args] 附加行为参数
   * @param {boolean} [isSetEvent=true] 是否设置事程
   * @param {boolean} [loadding=true] 是否显示loadding效果
   * @return {*}  {Promise<ICtrlActionResult>}
   * @memberof MobCalendarCtrlController
   */
  public formatDate(
    curtime: IParam,
    opts?: IParam,
    args?: IParam,
    isSetEvent: boolean = true,
    loadding: boolean = true,
  ): Promise<ICtrlActionResult> {
    this.curTime = curtime;
    switch (this.calendarStyle) {
      case 'DAY':
        this.start = (moment as any)(curtime).startOf('day').format('YYYY-MM-DD HH:mm:ss');
        this.end = (moment as any)(curtime).endOf('day').format('YYYY-MM-DD HH:mm:ss');
        break;
      case 'MONTH':
      case 'MONTH_TIMELINE':
        this.start = (moment as any)(curtime).startOf('month').format('YYYY-MM-DD HH:mm:ss');
        this.end = (moment as any)(curtime).endOf('month').format('YYYY-MM-DD HH:mm:ss');
        break;
      case 'WEEK':
      case 'WEEK_TIMELINE':
        this.start = (moment as any)(curtime).startOf('week').format('YYYY-MM-DD HH:mm:ss');
        this.end = (moment as any)(curtime).endOf('week').format('YYYY-MM-DD HH:mm:ss');
        break;
    }
    const _args = { ...args };
    Object.assign(_args, { start: this.start, end: this.end });
    return this.load(opts, _args, isSetEvent, loadding);
  }

  /**
   * @description 刷新数据
   * @param {IParam} opts 行为参数
   * @param {IParam} [args] 附加行为参数
   * @param {boolean} [isSetEvent=true] 是否设置事程
   * @param {boolean} [loadding=true] 是否显示loadding效果
   * @return {*}  {Promise<ICtrlActionResult>}
   * @memberof MobCalendarCtrlController
   */
  public async refresh(
    opts: IParam,
    args?: IParam,
    isSetEvent: boolean = true,
    loadding: boolean = true,
  ): Promise<ICtrlActionResult> {
    return this.formatDate(this.curTime, opts, args, isSetEvent, loadding);
  }

  /**
   * @description 设置默认选中
   * @private
   * @memberof MobCalendarCtrlController
   */
  private setDefaultSelect() {
    if (this.isSelectFirstDefault && Object.is(this.expMode, "LEFT")) {
      const arg = (this.controlInstance as IPSSysCalendar).getPSSysCalendarItems()?.find((_calendarItem: IPSSysCalendarItem) => 
          Object.is(this.activeItem.toString(), _calendarItem.itemType.toLowerCase())
      );
      Object.assign(arg,{
        curData: {
          start: moment(this.curTime).startOf('day').format('YYYY-MM-DD HH:mm:ss'),
          end: moment(this.curTime).endOf('day').format('YYYY-MM-DD HH:mm:ss'),
        }
      })
      this.ctrlEvent(MobCalendarEvents.SELECT_CHANGE,[arg]);
    }
  }

  /**
   * @description 设置事程数据
   * @private
   * @memberof MobCalendarCtrlController
   */
  private setEventData() {
    const calendarItems = (this.controlInstance as IPSSysCalendar).getPSSysCalendarItems() || [];
    const signData: any[] = [];
    calendarItems.forEach((calendaritem: IPSSysCalendarItem) => {
      const getBeginTimePSAppDEField = calendaritem.getBeginTimePSAppDEField();
      const itemType = calendaritem?.itemType?.toLowerCase();
      const beginTime = getBeginTimePSAppDEField?.name ? getBeginTimePSAppDEField.name?.toLowerCase() : 'start';
      const itemData = this.parsingData(itemType, beginTime);
      signData.push(...itemData);
    });
    this.setSign(signData);
  }

  /**
   * @description 设置标记数据
   * @private
   * @param {*} signData
   * @memberof MobCalendarCtrlController
   */
  private setSign(signData: any) {
    const obj: any = {};
    this.sign.length = 0;
    // 格式化数据
    signData.forEach((item: any, index: number) => {
      if (item.time && item.time.length == 10) {
        const year = item.time.split('-')[0];
        let month = item.time.split('-')[1];
        let day = item.time.split('-')[2];
        if (month < 10) {
          month = month.replace('0', '');
        }
        if (day < 10) {
          day = day.replace('0', '');
        }
        item.time = year + '-' + month + '-' + day;
      }
      if (!obj[item.time]) {
        Object.assign(obj, { [item.time]: item.evens });
      } else {
        obj[item.time].push(item.evens[0]);
      }
    });
    for (const key in obj) {
      this.sign.push({ time: key, evens: obj[key] });
    }
  }

  /**
   * @description 解析日历时间数据
   * @private
   * @param {string} tag 日历项标识
   * @param {string} mark 开始时间
   * @return {*}  {Array<any>}
   * @memberof MobCalendarCtrlController
   */
  private parsingData(tag: string, mark: string): Array<any> {
    const dataItem: any = [];
    if (this.calendarItems[tag]) {
      this.calendarItems[tag].forEach((item: any) => {
        dataItem.push({ time: item[mark]?.substring(0, 10), evens: [item] });
      });
    }
    return dataItem;
  }

  /**
   * @description 激活项改变
   * @param {string} curActiveItem 激活项
   * @memberof MobCalendarCtrlController
   */
  public activeItemChange(curActiveItem: string): void {
    this.activeItem = curActiveItem;
  }

  /**
   * @description 获取编辑视图信息
   * @private
   * @param {string} item 日历项
   * @return {*}
   * @memberof MobCalendarCtrlController
   */
  private async getEditView(item: IPSSysCalendarItem) {
    const view: any = {
      viewModel: {},
    };
    const parentModel = this.controlInstance.getParentPSModelObject() as IPSAppDEMobCalendarView;
    const viewRefs = parentModel.getPSAppViewRefs() as IPSAppViewRef[];
    if (viewRefs?.length > 0) {
      const editviewRef = viewRefs.find((viewRef: IPSAppViewRef) => {
        return Object.is(`EDITVIEW@${item.id}`, viewRef.id);
      });
      const editview = editviewRef?.getRefPSAppView();
      if (!editview) {
        return;
      }
      // await editview.fill();
      view.viewModel = editview;
      const tempView: any = {};
      tempView.placement = editview.openMode ? editview.openMode : '';
      tempView.deResParameters = Util.formatAppDERSPath(this.context, (editview as IPSAppDEView).getPSAppDERSPaths());
      if (editview.getPSAppDataEntity?.()) {
        const appDataEntity = editview.getPSAppDataEntity() as IPSAppDataEntity;
        tempView.parameters = [
          {
            pathName: Util.srfpluralize(appDataEntity?.codeName).toLowerCase(),
            parameterName: appDataEntity?.codeName.toLowerCase(),
          },
          {
            pathName: (editview as IPSAppDEView).getPSDEViewCodeName()?.toLowerCase(),
            parameterName: (editview as IPSAppDEView).getPSDEViewCodeName()?.toLowerCase(),
          },
        ];
      } else {
        tempView.parameters = [
          { pathName: editview?.codeName?.toLowerCase(), parameterName: editview?.codeName?.toLowerCase() },
        ];
      }
      Object.assign(view, tempView);
    }
    return view;
  }

  /**
   * @description 打开视图
   * @private
   * @param {*} $event 事件信息
   * @return {*}  {Promise<any>}
   * @memberof MobCalendarCtrlController
   */
  public async openView($event: IParam): Promise<any> {
    if (App.isPreviewMode()) {
      return;
    }
    const calendarItems = (this.controlInstance as IPSSysCalendar).getPSSysCalendarItems() || [];
    const itemType = $event.itemType.toLowerCase();
    const calendarItem = calendarItems.find((item: IPSSysCalendarItem) => {
      return itemType == item.itemType.toLowerCase();
    });
    if (!calendarItem) {
      return;
    }
    const appde = calendarItem.getPSAppDataEntity()?.codeName.toLowerCase() as string;
    const tempContext: any = Util.deepCopy(this.context);
    tempContext[appde] = $event[appde];
    const view = await this.getEditView(calendarItem);
    if (!view) {
      return;
    }
    if (Object.is(view.placement, 'INDEXVIEWTAB') || Object.is(view.placement, '')) {
      const path: string = ViewTool.buildUpRoutePath(
        tempContext,
        view.deResParameters,
        view.parameters,
        [$event],
        this.viewParam,
        true,
      );
      App.getOpenViewService().openView(path);
    } else {
      let container: Subject<any>;
      if (Object.is(view.placement, 'POPUPMODAL')) {
        container = App.getOpenViewService().openModal({ viewModel: view.viewModel }, tempContext, this.viewParam);
      } else {
        container = App.getOpenViewService().openDrawer({ viewModel: view.viewModel }, tempContext, this.viewParam);
      }
      container.subscribe((result: any) => {
        if (!result || !Object.is(result.ret, 'OK')) {
          return;
        }
        const args = { start: this.start, end: this.end };
        this.load({}, args, false);
      });
    }
  }
}
