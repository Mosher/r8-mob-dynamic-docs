import { IPSAppView, IPSDEList, IPSDEMobMDCtrl, IPSDERBase, IPSListExpBar } from '@ibiz/dynamic-model-api';
import { IMobListExpBarCtrlController, MobMDCtrlEvents } from '../../../interface';
import { AppExpBarCtrlController } from './app-exp-bar-ctrl-controller';

export class MobListExpBarController extends AppExpBarCtrlController implements IMobListExpBarCtrlController{

  /**
   * @description 移动端列表导航部件实例
   * @public
   * @type { IPSDEMobListExpBar }
   * @memberof MobListExpBarController
   */
  public controlInstance!: IPSListExpBar;

  /**
   * @description 多数据部件
   * @type {(IPSDEList | IPSDEMobMDCtrl)}
   * @memberof MobListExpBarController
   */
  public xDataControl!: IPSDEList | IPSDEMobMDCtrl;

  /**
   * @description 处理多数据部件配置
   * @protected
   * @memberof MobListExpBarController
   */
  protected async handleXDataOptions() {
    if (App.isPreviewMode()) {
      return;
    }
    await super.handleXDataOptions();
    const navPSAppView: IPSAppView = await this.xDataControl?.getNavPSAppView()?.fill() as IPSAppView;
    if (navPSAppView) {
      this.navView = navPSAppView.modelFilePath;
      this.navViewComponent = App.getComponentService().getViewTypeComponent(navPSAppView.viewType, navPSAppView.viewStyle, navPSAppView.getPSSysPFPlugin?.()?.pluginCode);
    }
    this.navFilter = this.xDataControl.navFilter || '';
    const navPSDer = this.xDataControl.getNavPSDER();
    if (navPSDer) {
      this.navPSDer = navPSDer ? "n_" + navPSDer.minorCodeName?.toLowerCase() + "_eq" : "";
    }
  }

  /**
   * @description 处理部件事件
   * @param {string} controlname 部件名称
   * @param {string} action 行为标识
   * @param {*} data UI数据
   * @memberof MobListExpBarController
   */
  public handleCtrlEvent(controlname: string, action: string, data: any) {
    if (controlname === this.xDataControlName && action == MobMDCtrlEvents.ROW_SELECTED) {
      this.onSelectionChange(data);
    } else {
      super.handleCtrlEvent(controlname, action, data);
    }
  }
}
