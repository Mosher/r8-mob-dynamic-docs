import { IPSAppDEView, IPSDETabViewPanel } from '@ibiz/dynamic-model-api';
import { ModelTool, Util } from '../../../util';
import { ICtrlActionResult, IMobTabViewPanelCtrlController, IParam, IViewStateParam } from '../../../interface';
import { AppCtrlControllerBase } from './app-ctrl-controller-base';

export class MobTabViewPanelCtrlController extends AppCtrlControllerBase implements IMobTabViewPanelCtrlController {
  /**
   * @description 部件模型实例对象
   * @type {IPSDETabViewPanel}
   * @memberof MobTabViewPanelCtrlController
   */
  controlInstance!: IPSDETabViewPanel;

  /**
   * 刷新
   *
   * @param opts 行为参数
   * @param args 补充逻辑完成参数
   * @param showInfo 是否显示提示信息
   * @param loadding 是否显示loading效果
   * @returns {Promise<IParam>}
   * @memberof MobTabExpPanelCtrlController
   */
  public async refresh(
    opts?: IParam,
    args?: IParam,
    showInfo: boolean = true,
    loadding: boolean = true,
  ): Promise<ICtrlActionResult> {
    return new Promise((resolve: any) => {
      const embeddedView = this.controlInstance.getEmbeddedPSAppDEView() as IPSAppDEView;
      this.viewState.next({ tag: embeddedView.name, action: 'refresh', data: opts });
      resolve({ ret: true, data: null });
    });
  }

  /**
   * @description 获取导航参数
   * @return {*}  {{ context: IParam, viewParam: IParam }}
   * @memberof MobTabViewPanelCtrlController
   */
  public getNavParams(): { context: IParam; viewParam: IParam } {
    if (App.isPreviewMode()) {
      return { context:this.context,viewParam:this.viewParam };
    } 
    const tempContext = Util.deepCopy(this.context);
    const tempViewParam = Util.deepCopy(this.viewParam);
    Object.assign(tempContext, ModelTool.getNavigateContext(this.controlInstance));
    Object.assign(tempViewParam, ModelTool.getNavigateParams(this.controlInstance));
    return { context: tempContext, viewParam: tempViewParam };
  }
}
