import { PanelDetailModelController } from './panel-detail-controller';

/**
 * 用户控件模型
 *
 * @export
 * @class PanelUserControlModelController
 * @extends {PanelDetailModelController}
 */
export class PanelUserControlModelController extends PanelDetailModelController {
  constructor(otps: any = {}) {
    super(otps);
  }
}
