import { PanelDetailModelController } from './panel-detail-controller';
import { PanelTabPanelModelController } from './panel-tab-panel-controller';

/**
 * 分页面板模型
 *
 * @export
 * @class PanelTabPageModelController
 * @extends {PanelDetailModel}
 */
export class PanelTabPageModelController extends PanelDetailModelController {
  /**
   * Creates an instance of PanelTabPageModelController.
   * PanelTabPageModelController 实例
   *
   * @param {*} [opts={}]
   * @memberof PanelTabPageModelController
   */
  constructor(opts: any = {}) {
    super(opts);
  }

  /**
   * 设置分页是否启用
   *
   * @param {boolean} state
   * @memberof PanelTabPageModelController
   */
  public setVisible(state: boolean): void {
    this.visible = state;
    const tabPanel = this.getTabPanelModel();
    if (tabPanel) {
      tabPanel.setActiviePage();
    }
  }

  /**
   * 获取分页面板
   *
   * @returns {(PanelTabPanelModelController | null)}
   * @memberof PanelTabPageModelController
   */
  public getTabPanelModel(): PanelTabPanelModelController | null {
    if (!this.panel) {
      return null;
    }
    const tabPanels: any[] = Object.values(this.panel.detailsModel).filter((model: any) =>
      Object.is(model.itemType, 'TABPANEL'),
    );
    const index = tabPanels.findIndex((tabPanel: any) => {
      return tabPanel.tabPages.some((tabPag: any) => Object.is(tabPag.name, this.name));
    });
    if (index === -1) {
      return null;
    }
    const tabPanel: PanelTabPanelModelController = tabPanels[index];
    return tabPanel;
  }
}
