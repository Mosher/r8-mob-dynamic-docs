import { PanelDetailModelController } from './panel-detail-controller';

/**
 * 面板容器模型
 *
 * @export
 * @class PanelContainerModel
 * @extends {PanelDetailModelController}
 */
export class PanelContainerModelController extends PanelDetailModelController {
  constructor(opts: any = {}) {
    super(opts);
  }
}
