import { PanelDetailModelController } from './panel-detail-controller';

/**
 * 分页部件模型
 *
 * @export
 * @class PanelTabPanelModelController
 * @extends {PanelDetailModelController}
 */
export class PanelTabPanelModelController extends PanelDetailModelController {
  /**
   * 被激活分页
   *
   * @type {string}
   * @memberof PanelTabPanelModelController
   */
  public activatedPage: string = '';

  /**
   * 选中激活状态
   *
   * @type {string}
   * @memberof PanelTabPanelModelController
   */
  public clickActiviePage: string = '';

  /**
   * 分页子成员
   *
   * @type {any[]}
   * @memberof PanelTabPanelModelController
   */
  public tabPages: any[] = [];

  /**
   * Creates an instance of PanelTabPanelModelController.
   * PanelTabPanelModelController 实例
   *
   * @param {*} [opts={}]
   * @memberof PanelTabPanelModelController
   */
  constructor(opts: any = {}) {
    super(opts);
    this.tabPages = [...opts.tabPages];
    if (this.tabPages.length > 0) {
      this.activatedPage = this.tabPages[0].name;
    }
  }

  /**
   * 设置激活分页
   *
   * @memberof PanelTabPanelModelController
   */
  public setActiviePage(): void {
    if (!this.panel) {
      return;
    }
    const detailsModel: any = this.panel.detailsModel;

    const index = this.tabPages.findIndex(
      (tabpage: any) =>
        Object.is(tabpage.name, this.clickActiviePage) &&
        Object.is(tabpage.name, this.activatedPage) &&
        detailsModel[tabpage.name].visible,
    );
    if (index !== -1) {
      return;
    }

    this.tabPages.some((tabpage: any) => {
      if (detailsModel[tabpage.name].visible) {
        this.activatedPage = tabpage.name;
        return true;
      }
      return false;
    });
  }

  /**
   * 选中页面
   *
   * @param {*} $event
   * @returns {void}
   * @memberof PanelTabPanelModelController
   */
  public clickPage($event: any): void {
    if (!$event) {
      return;
    }

    this.clickActiviePage = $event;
    this.activatedPage = $event;
  }
}
