import { PanelDetailModelController } from './panel-detail-controller';

/**
 * 按钮模型
 *
 * @export
 * @class PanelButtonModelController
 * @extends {PanelDetailModelController}
 */
export class PanelButtonModelController extends PanelDetailModelController {
  constructor(opts: any = {}) {
    super(opts);
    this.$disabled = opts.disabled;
    this.uiaction = opts.uiaction;
  }

  /**
   * 是否禁用
   *
   * @type {boolean}
   * @memberof PanelButtonModelController
   */
  private $disabled: boolean = false;

  /**
   * 按钮对应的界面行为
   *
   * @type {*}
   * @memberof PanelButtonModelController
   */
  public uiaction: any;

  /**
   * 是否启用
   *
   * @type {boolean}
   * @memberof PanelButtonModelController
   */
  public get disabled(): boolean {
    return this.$disabled;
  }

  /**
   * 设置是否启用
   *
   * @memberof PanelButtonModelController
   */
  public set disabled(val: boolean) {
    if (this.isPower) {
      this.$disabled = val;
    }
  }
}
