import { PanelDetailModelController } from './panel-detail-controller';

/**
 * 直接内容模型
 *
 * @export
 * @class PanelRawitemModelController
 * @extends {PanelDetailModelController}
 */
export class PanelRawitemModelController extends PanelDetailModelController {
  constructor(opts: any = {}) {
    super(opts);
  }
}
