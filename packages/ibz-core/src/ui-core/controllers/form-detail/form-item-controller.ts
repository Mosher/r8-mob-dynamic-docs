import { IPSDEFormItem } from '@ibiz/dynamic-model-api';
import { FormDetailController } from './form-detail-controller';

/**
 * 表单项模型
 *
 * @export
 * @class FormItemController
 * @extends {FormDetailController}
 */
export class FormItemController extends FormDetailController {
  /**
   * @description 表单项实例对象
   * @type {IPSDEFormItem}
   * @memberof FormItemController
   */
  public model!: IPSDEFormItem;

  /**
   * 是否启用
   *
   * @type {boolean}
   * @memberof FormItemController
   */
  public disabled: boolean = false;

  /**
   * 错误信息
   *
   * @type {string}
   * @memberof FormItemController
   */
  public error: string = '';

  /**
   * 表单项启用条件
   *
   * 0 不启用
   * 1 新建
   * 2 更新
   * 3 全部启用
   *
   * @type {(number | 0 | 1 | 2 | 3)}
   * @memberof FormItemController
   */
  public enableCond: number | 0 | 1 | 2 | 3 = 3;

  /**
   * 是否必填
   *
   * @type {boolean}
   * @memberof FormItemController
   */
  public required: boolean = false;

  /**
   * @description 是否为信息模式
   * @protected
   * @type {boolean}
   * @memberof FormItemController
   */
  protected infoMode: boolean = false;

  /**
   * Creates an instance of FormItemController.
   * FormItemController 实例
   *
   * @param {*} [opts={}]
   * @memberof FormItemController
   */
  constructor(opts: any = {}) {
    super(opts);
    this.disabled = opts.disabled ? true : false;
    this.enableCond = opts.enableCond;
    this.required = opts.required;
  }

  /**
   * 设置是否启用
   *
   * @param {boolean} state
   * @memberof FormItemController
   */
  public setDisabled(state: boolean): void {
    this.disabled = state;
  }

  /**
   * 设置信息内容
   *
   * @param {string} error
   * @memberof FormItemController
   */
  public setError(error: string): void {
    this.error = error;
  }

  /**
   * 设置是否启用
   *
   * @param {string} srfuf
   * @memberof FormItemController
   */
  public setEnableCond(srfuf: string): void {
    // 是否有权限
    const isReadOk: boolean = true;
    const _srfuf: number = parseInt(srfuf, 10);
    let state: boolean = true;

    if (isReadOk) {
      if (_srfuf === 1) {
        if ((this.enableCond & 2) === 2) {
          state = false;
        }
      } else {
        if ((this.enableCond & 1) === 1) {
          state = false;
        }
      }
    }
    this.setDisabled(state);
  }

  /**
   * @description 设置是否为信息模式
   * @param {boolean} mode 
   * @memberof FormItemController
   */
  public setInfoMode(mode: boolean) {
    this.infoMode = mode;
  }

  /**
   * @description 获取是否为信息模式
   * @return {*}  {boolean}
   * @memberof FormItemController
   */
  public getInfoMode(): boolean {
    return this.infoMode;
  }
}
