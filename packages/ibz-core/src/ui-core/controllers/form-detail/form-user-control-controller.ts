import { IPSDEFormUserControl } from '@ibiz/dynamic-model-api';
import { FormDetailController } from './form-detail-controller';

/**
 * 用户控件模型
 *
 * @export
 * @class FormUserControlController
 * @extends {FormDetailController}
 */
export class FormUserControlController extends FormDetailController {
  /**
   * @description 表单用户控件项模型实例对象
   * @type {IPSDEFormUserControl}
   * @memberof FormUserControlController
   */
  public model!: IPSDEFormUserControl;

  constructor(otps: any = {}) {
    super(otps);
  }
}
