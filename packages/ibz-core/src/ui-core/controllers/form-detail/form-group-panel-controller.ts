import { IPSDEFormGroupPanel } from '@ibiz/dynamic-model-api';
import { FormDetailController } from './form-detail-controller';

/**
 * 分组面板模型
 *
 * @export
 * @class FormGroupPanelController
 * @extends {FormDetailController}
 */
export class FormGroupPanelController extends FormDetailController {
  /**
   * @description 表单分组面板模型实例对象
   * @type {IPSDEFormGroupPanel}
   * @memberof FormGroupPanelController
   */
  public model!: IPSDEFormGroupPanel;

  /**
   * 实体界面行为组
   *
   * @type {*}
   * @memberof FormGroupPanelController
   */
  public uiActionGroup: any = {};

  /**
   * 受控内容组
   *
   * @type {*}
   * @memberof FormGroupPanelController
   */
  public controlledItems: any[] = [];

  /**
   * 支持锚点集合
   *
   * @type {Array}
   * @memberof FormGroupPanelController
   */
  public anchorPoints: any[] = [];

  /**
   * 是否为管理容器
   *
   * @type {*}
   * @memberof FormGroupPanelController
   */
  public isManageContainer: boolean = false;

  /**
   * 管理容器状态 true显示 false隐藏
   *
   * @type {*}
   * @memberof FormGroupPanelController
   */
  public manageContainerStatus: boolean = true;

  /**
   * Creates an instance of FormGroupPanelController.
   * 创建 FormGroupPanelController 实例
   *
   * @param {*} [opts={}]
   * @memberof FormGroupPanelController
   */
  constructor(opts: any = {}) {
    super(opts);
    Object.assign(this.uiActionGroup, opts.uiActionGroup);
    this.controlledItems = opts.controlledItems;
    this.anchorPoints = opts.anchorPoints;
    this.isManageContainer = opts.isManageContainer ? true : false;
  }

  /**
   * 设置管理容器状态
   *
   * @param {boolean} state
   * @memberof FormGroupPanelController
   */
  public setManageContainerStatus(state: boolean): void {
    this.manageContainerStatus = state;
    this.controlledItems.forEach((item: string) => {
      const detail = this.parentController.detailsModel[item];
      if (detail && detail.isControlledContent) {
        detail.setVisible(state ? this.oldVisible : false);
      }
    });
  }

  /**
   * 处理表单分组界面行为点击
   *
   * @param event
   * @param item
   * @memberof FormGroupPanelController
   */
  public onGroupUIActionClick(event: any, item: any) {
    if (this.parentController) {
      this.parentController.handleActionClick(this.data, event, item);
    }
  }
}
