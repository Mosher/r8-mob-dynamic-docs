import { IPSDEFormPage } from '@ibiz/dynamic-model-api';
import { FormDetailController } from './form-detail-controller';

/**
 * 表单分页模型
 *
 * @export
 * @class FormPageController
 * @extends {FormDetailController}
 */
export class FormPageController extends FormDetailController {
  /**
   * @description 表单分页模型实例对象
   * @type {IPSDEFormPage}
   * @memberof FormPageController
   */
  public model!: IPSDEFormPage;

  constructor(opts: any = {}) {
    super(opts);
  }
}
