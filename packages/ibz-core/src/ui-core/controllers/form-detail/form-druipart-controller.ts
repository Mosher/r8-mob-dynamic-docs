import { IPSAppDEView, IPSDEFormDRUIPart } from '@ibiz/dynamic-model-api';
import { Util } from '../../../util';
import { IParam } from '../../../interface';
import { FormDetailController } from './form-detail-controller';

/**
 * 数据关系界面模型
 *
 * @export
 * @class FormDruipartController
 * @extends {FormDetailController}
 */
export class FormDruipartController extends FormDetailController {
  /**
   * @description 表单关系界面实例对象
   * @type {IPSDEFormDRUIPart}
   * @memberof FormDruipartController
   */
  public model!: IPSDEFormDRUIPart;

  /**
   * 临时数据模式：从数据模式:"2"、主数据模式:"1"、无临时数据模式:"0"
   *
   * @type {number}
   * @memberof FormDruipartController
   */
  public tempMode: number = 0;

  /**
   * 自定义样式
   *
   * @type {IParam}
   * @memberof FormDruipartController
   */
  public customStyle: IParam = {};

  /**
   * 父数据
   *
   * @type {IParam}
   * @memberof FormDruipartController
   */
  public parentdata: IParam = {};

  /**
   * 内嵌视图
   *
   * @type {IPSAppDEView}
   * @memberof FormDruipartController
   */
  public embeddedView!: IPSAppDEView;

  /**
   * 附加界面刷新项
   *
   * @type {IPSAppDEView}
   * @memberof FormDruipartController
   */
  public refreshitems: string = '';

  /**
   * 局部导航上下文
   *
   * @type {IParam}
   * @memberof FormDruipartController
   */
  public localContext: IParam = {};

  /**
   * 局部导航参数
   *
   * @type {IParam}
   * @memberof FormDruipartController
   */
  public localParam: IParam = {};

  /**
   * 传入参数项名称
   *
   * @type {string}
   * @memberof FormDruipartController
   */
  public paramItem!: string;

  /**
   * 视图参数
   *
   * @type {any[]}
   * @memberof FormDruipartController
   */
  public parameters: any[] = [];

  /**
   * Creates an instance of FormDruipartController.
   *
   * @param {*} [opts={}]
   * @memberof FormDruipartController
   */
  constructor(opts: any = {}) {
    super(opts);
    this.initModelData();
  }

  /**
   * 初始化模型数据
   *
   * @public
   * @memberof FormDruipartController
   */
  public initModelData() {
    const { refreshItems, parentDataJO, contentHeight, paramItem } = this.model;
    this.embeddedView = this.model.getPSAppView() as IPSAppDEView;
    this.tempMode = this.embeddedView?.tempMode;
    const appDERSPaths = (this.embeddedView as IPSAppDEView)?.getPSAppDERSPaths();
    this.parameters = Util.formatAppDERSPath(this.context, appDERSPaths);
    this.customStyle = { height: Util.calcBoxSize(contentHeight), overflow: 'auto' };
    this.localContext = Util.formatNavParam(this.model.getPSNavigateContexts());
    this.localParam = Util.formatNavParam(this.model.getPSNavigateParams());
    this.refreshitems = refreshItems;
    this.parentdata = parentDataJO;
    this.paramItem = paramItem?.toLowerCase();
  }

  /**
   * 初始化输入数据
   *
   * @public
   * @memberof FormDruipartController
   */
  public initInputData(opts: IParam) {
    super.initInputData(opts);
    if (!this.paramItem) {
      this.paramItem = opts?.parameterName?.toLowerCase();
    }
  }

  /**
   * 关系界面保存完成
   *
   * @public
   * @memberof FormDruipartController
   */
  public embedViewDataSaved() {
    this.parentController.reactiveFormDRSave(this.name);
  }
}
