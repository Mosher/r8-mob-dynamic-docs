import { IPSDEFormTabPage } from '@ibiz/dynamic-model-api';
import { FormDetailController } from './form-detail-controller';
import { FormTabPanelController } from './form-tab-panel-controller';

/**
 * 分页面板模型
 *
 * @export
 * @class FormTabPageController
 * @extends {FormDetailController}
 */
export class FormTabPageController extends FormDetailController {
  /**
   * @description 表单分页面板项模型实例对象
   * @type {IPSDEFormTabPage}
   * @memberof FormTabPageController
   */
  public model!: IPSDEFormTabPage;

  /**
   * Creates an instance of FormTabPageController.
   * FormTabPageController 实例
   *
   * @param {*} [opts={}]
   * @memberof FormTabPageController
   */
  constructor(opts: any = {}) {
    super(opts);
  }

  /**
   * 设置分页是否启用
   *
   * @param {boolean} state
   * @memberof FormTabPageController
   */
  public setVisible(state: boolean): void {
    this.visible = state;
    const tabPanel = this.getTabPanelModel();
    if (tabPanel) {
      tabPanel.setActiviePage();
    }
  }

  /**
   * 获取分页面板
   *
   * @returns {(FormTabPanelController | null)}
   * @memberof FormTabPageController
   */
  public getTabPanelModel(): FormTabPanelController | null {
    if (!this.parentController) {
      return null;
    }
    const tabPanels: any[] = Object.values(this.parentController.detailsModel).filter((model: any) =>
      Object.is(model.detailType, 'TABPANEL'),
    );
    const index = tabPanels.findIndex((tabPanel: any) => {
      return tabPanel.tabPages.some((tabPag: any) => Object.is(tabPag.name, this.name));
    });
    if (index === -1) {
      return null;
    }
    const tabPanel: FormTabPanelController = tabPanels[index];
    return tabPanel;
  }
}
