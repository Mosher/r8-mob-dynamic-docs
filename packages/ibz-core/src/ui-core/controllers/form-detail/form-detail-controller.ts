import { IPSDEFormDetail } from '@ibiz/dynamic-model-api';
import { IParam } from '../../../interface';
import { MobFormCtrlController } from '../widget';

/**
 * 表单成员模型
 *
 * @export
 * @class FormDetailController
 */
export class FormDetailController {
  /**
   * 是否有权限
   *
   * @type {boolean}
   * @memberof FormDetailController
   */
  public isPower: boolean = true;

  /**
   * 成员标题
   *
   * @type {string}
   * @memberof FormDetailController
   */
  public caption: string = '';

  /**
   * 成员类型
   *
   * @type {string}
   * @memberof FormDetailController
   */
  public detailType: string = '';

  /**
   * 表单对象
   *
   * @type {*}
   * @memberof FormDetailController
   */
  public parentController!: MobFormCtrlController;

  /**
   * 成员名称
   *
   * @type {string}
   * @memberof FormDetailController
   */
  public name: string = '';

  /**
   * 成员是否显示
   *
   * @type {boolean}
   * @memberof FormDetailController
   */
  public $visible: boolean = true;

  /**
   * 成员是否显示(旧)
   *
   * @type {boolean}
   * @memberof FormDetailController
   */
  public oldVisible: boolean = true;

  /**
   * 成员是否为受控内容
   *
   * @type {boolean}
   * @memberof FormDetailController
   */
  public isControlledContent: boolean = false;

  /**
   * 成员是否显示标题
   *
   * @type {boolean}
   * @memberof FormDetailController
   */
  public isShowCaption: boolean = true;

  /**
   * 模型数据
   *
   * @type {IParam}
   * @memberof FormDetailController
   */
  public model!: IPSDEFormDetail;

  /**
   * 应用上下文
   *
   * @type {IParam}
   * @memberof FormDetailController
   */
  context: IParam = {};

  /**
   * 视图参数
   *
   * @type {IParam}
   * @memberof FormDetailController
   */
  viewParam: IParam = {};

  /**
   * 视图操作参数集合
   *
   * @type {IParam}
   * @memberof FormDetailController
   */
  viewCtx: IParam = {};

  /**
   * 绑定样式表
   *
   * @type {string | null}
   * @memberof FormDetailController
   */
  public customClass: string | null = null;

  /**
   * 表单数据
   *
   * @type {string | null}
   * @memberof FormDetailController
   */
  public data: IParam = {};

  /**
   * Creates an instance of FormDetailController.
   * FormDetailController 实例
   *
   * @param {*} [opts={}]
   * @memberof FormDetailController
   */
  constructor(opts: any = {}) {
    this.caption = !Object.is(opts.caption, '') ? opts.caption : '';
    this.detailType = !Object.is(opts.detailType, '') ? opts.detailType : '';
    this.parentController = opts.parentController ? opts.parentController : {};
    this.name = !Object.is(opts.name, '') ? opts.name : '';
    this.$visible = opts.visible ? true : false;
    this.oldVisible = opts.visible ? true : false;
    this.isShowCaption = opts.isShowCaption ? true : false;
    this.isControlledContent = opts.isControlledContent ? true : false;
    this.model = opts?.model;
    this.customClass = opts?.model?.getPSSysCss()?.cssName;
  }

  /**
   * 初始化输入数据
   *
   * @public
   * @memberof FormDetailController
   */
  public initInputData(opts: IParam) {
    this.context = opts.navContext;
    this.viewParam = opts.navParam;
    this.viewCtx = opts.viewCtx;
    this.data = opts.data;
  }

  /**
   * 设置成员是否隐藏
   *
   * @memberof FormDetailController
   */
  public set visible(val: boolean) {
    if (this.isPower) {
      this.oldVisible = this.$visible;
      this.$visible = val;
    }
  }

  /**
   * 获取成员是否隐藏
   *
   * @memberof FormDetailController
   */
  public get visible() {
    return this.$visible;
  }

  /**
   * 设置显示与隐藏
   *
   * @param {boolean} state
   * @memberof FormDetailController
   */
  public setVisible(state: boolean): void {
    if (this.isPower) {
      this.oldVisible = this.$visible;
      this.visible = state;
    }
  }

  /**
   * 设置显示标题栏
   *
   * @param {boolean} state
   * @memberof FormDetailController
   */
  public setShowCaption(state: boolean): void {
    this.isShowCaption = state;
  }
}
