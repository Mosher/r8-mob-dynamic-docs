import { IPSDEFormButton } from '@ibiz/dynamic-model-api';
import { FormDetailController } from './form-detail-controller';

/**
 * 按钮模型
 *
 * @export
 * @class FormButtonController
 * @extends {FormDetailController}
 */
export class FormButtonController extends FormDetailController {
  /**
   * @description 表单按钮模型实例对象
   * @type {IPSDEFormButton}
   * @memberof FormButtonController
   */
  public model!: IPSDEFormButton;

  constructor(opts: any = {}) {
    super(opts);
    this.$disabled = opts.disabled;
    this.uiaction = opts.uiaction;
  }

  /**
   * 是否禁用
   *
   * @type {boolean}
   * @memberof FormButtonController
   */
  private $disabled: boolean = false;

  /**
   * 按钮对应的界面行为
   *
   * @type {*}
   * @memberof FormButtonController
   */
  public uiaction: any;

  /**
   * 是否启用
   *
   * @type {boolean}
   * @memberof FormButtonController
   */
  public get disabled(): boolean {
    return this.$disabled;
  }

  /**
   * 设置是否启用
   *
   * @memberof FormButtonController
   */
  public set disabled(val: boolean) {
    if (this.isPower) {
      this.$disabled = val;
    }
  }

  /**
   * 按钮点击
   *
   * @memberof FormButtonController
   */
  public buttonClick(event: any) {
    if (this.parentController) {
      this.parentController.executeButtonAction(this.model, this.data, event);
    }
  }
}
