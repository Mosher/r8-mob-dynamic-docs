import { ChartSeriesController } from './chart-series-controller';

/**
 * k线图序列模型
 *
 * @export
 * @class ChartCandlestickSeriesController
 */
export class ChartCandlestickSeriesController extends ChartSeriesController {
  /**
   * 分类属性
   *
   * @type {string}
   * @memberof ChartCandlestickSeriesController
   */
  public categorField: string = '';

  /**
   * 值属性
   *
   * @type {string}
   * @memberof ChartCandlestickSeriesController
   */
  public valueField: string = '';

  /**
   * 分类代码表
   *
   * @type {string}
   * @memberof ChartCandlestickSeriesController
   */
  public categorCodeList: any = null;

  /**
   * 维度定义
   *
   * @type {string}
   * @memberof ChartCandlestickSeriesController
   */
  public dimensions: Array<string> = [];

  /**
   * 维度编码
   *
   * @type {*}
   * @memberof ChartCandlestickSeriesController
   */
  public encode: any = null;

  /**
   * 序列模板
   *
   * @type {*}
   * @memberof ChartCandlestickSeriesController
   */
  public seriesTemp: any = null;

  /**
   * Creates an instance of ChartCandlestickSeriesController.
   * ChartCandlestickSeriesController 实例
   *
   * @param {*} [opts={}]
   * @memberof ChartCandlestickSeriesController
   */
  constructor(opts: any = {}) {
    super(opts);
    this.categorField = !Object.is(opts.categorField, '') ? opts.categorField : '';
    this.categorCodeList = opts.categorCodeList ? opts.categorCodeList : null;
    this.valueField = !Object.is(opts.valueField, '') ? opts.valueField : '';
    this.dimensions = opts.dimensions ? opts.dimensions : '';
    this.encode = opts.encode ? opts.encode : null;
    this.seriesTemp = opts.seriesTemp ? opts.seriesTemp : null;
  }

  /**
   * 设置分类属性
   *
   * @param {string} state
   * @memberof ChartCandlestickSeriesController
   */
  public setCategorField(state: string): void {
    this.categorField = state;
  }

  /**
   * 设置序列名称
   *
   * @param {string} state
   * @memberof ChartCandlestickSeriesController
   */
  public setValueField(state: string): void {
    this.valueField = state;
  }

  /**
   * 分类代码表
   *
   * @param {*} state
   * @memberof ChartCandlestickSeriesController
   */
  public setCategorCodeList(state: any): void {
    this.categorCodeList = state;
  }

  /**
   * 维度定义
   *
   * @param {*} state
   * @memberof ChartCandlestickSeriesController
   */
  public setDimensions(state: any): void {
    this.dimensions = state;
  }

  /**
   * 设置编码
   *
   * @param {*} state
   * @memberof ChartCandlestickSeriesController
   */
  public setEncode(state: any): void {
    this.encode = state;
  }

  /**
   * 设置序列模板
   *
   * @param {*} state
   * @memberof ChartCandlestickSeriesController
   */
  public setSeriesTemp(state: any): void {
    this.seriesTemp = state;
  }
}
