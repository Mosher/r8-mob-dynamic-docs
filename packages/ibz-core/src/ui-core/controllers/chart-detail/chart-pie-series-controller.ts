import { ChartSeriesController } from './chart-series-controller';

/**
 * 饼图序列模型
 *
 * @export
 * @class ChartPieSeriesController
 */
export class ChartPieSeriesController extends ChartSeriesController {
  /**
   * 分类属性
   *
   * @type {string}
   * @memberof ChartPieSeriesController
   */
  public categorField: string = '';

  /**
   * 值属性
   *
   * @type {string}
   * @memberof ChartPieSeriesController
   */
  public valueField: string = '';

  /**
   * 分类代码表
   *
   * @type {string}
   * @memberof ChartPieSeriesController
   */
  public categorCodeList: any = null;

  /**
   * 维度定义
   *
   * @type {string}
   * @memberof ChartPieSeriesController
   */
  public dimensions: Array<string> = [];

  /**
   * 维度编码
   *
   * @type {*}
   * @memberof ChartPieSeriesController
   */
  public encode: any = null;

  /**
   * Creates an instance of ChartPieSeriesController.
   * ChartPieSeriesController 实例
   *
   * @param {*} [opts={}]
   * @memberof ChartPieSeriesController
   */
  constructor(opts: any = {}) {
    super(opts);
    this.categorField = !Object.is(opts.categorField, '') ? opts.categorField : '';
    this.categorCodeList = opts.categorCodeList ? opts.categorCodeList : null;
    this.valueField = !Object.is(opts.valueField, '') ? opts.valueField : '';
    this.dimensions = opts.dimensions ? opts.dimensions : '';
    this.encode = opts.encode ? opts.encode : null;
  }

  /**
   * 设置分类属性
   *
   * @param {string} state
   * @memberof ChartPieSeriesController
   */
  public setCategorField(state: string): void {
    this.categorField = state;
  }

  /**
   * 设置序列名称
   *
   * @param {string} state
   * @memberof ChartPieSeriesController
   */
  public setValueField(state: string): void {
    this.valueField = state;
  }

  /**
   * 分类代码表
   *
   * @param {*} state
   * @memberof ChartPieSeriesController
   */
  public setCategorCodeList(state: any): void {
    this.categorCodeList = state;
  }

  /**
   * 维度定义
   *
   * @param {*} state
   * @memberof ChartPieSeriesController
   */
  public setDimensions(state: any): void {
    this.dimensions = state;
  }

  /**
   * 设置编码
   *
   * @param {*} state
   * @memberof ChartPieSeriesController
   */
  public setEncode(state: any): void {
    this.encode = state;
  }
}
