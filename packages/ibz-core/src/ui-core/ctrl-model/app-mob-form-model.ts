import { IPSDEEditForm, IPSDEFormItem } from '@ibiz/dynamic-model-api';
import { DataTypes, ModelTool } from '../../util';

/**
 * 移动端表单部件模型
 *
 * @export
 * @class AppMobFormModel
 */
export class AppMobFormModel {
  /**
   * 表单实例对象
   *
   * @memberof AppMobFormModel
   */
  public FormInstance!: IPSDEEditForm;

  /**
   * Creates an instance of AppMobFormModel.
   *
   * @param {*} [opts={}]
   * @memberof AppMobFormModel
   */
  constructor(opts: any) {
    this.FormInstance = opts;
  }

  /**
   * 获取数据项集合
   *
   * @returns {any[]}
   * @memberof AppMobFormModel
   */
  public getDataItems(): any[] {
    const modelArray: any[] = [
      {
        name: 'srfwfmemo',
        prop: 'srfwfmemo',
        dataType: 'TEXT',
      },
      {
        name: 'srfnextform',
        prop: 'srfnextform',
        dataType: 'TEXT',
      },
      {
        name: 'srfactionparam',
        prop: 'srfactionparam',
        dataType: 'TEXT',
      },
      {
        name: 'srffrontuf',
        prop: 'srffrontuf',
        dataType: 'TEXT',
      },
    ];
    const allFormDetails = ModelTool.getAllFormDetails(this.FormInstance);
    const formItems: any = this.FormInstance.getPSDEFormItems();
    const appDataEntity = this.FormInstance?.getPSAppDataEntity();
    // 表单部件
    allFormDetails.forEach((detail: any) => {
      if (detail.detailType && detail?.formPartType == 'DYNASYS') {
        modelArray.push({
          name: detail.name,
          dataType: 'FORMPART',
        });
      }
    });
    // 表单项
    formItems?.forEach((item: IPSDEFormItem) => {
      const temp: any = { name: item.id };
      if (item?.getPSAppDEField()) {
        temp.prop = item.getPSAppDEField()?.codeName?.toLowerCase();
        temp.dataType = DataTypes.toString(item.getPSAppDEField()?.stdDataType || -1)?.toLowerCase();
      } else if (item.getPSEditor()?.editorType !== 'HIDDEN') {
        //表单项无属性且界面显示类型（供开始流程、提交流程使用）
        if (!item.hidden) {
          temp.prop = item.id;
        }
        temp.dataType = 'FORMITEM';
      }
      modelArray.push(temp);
    });
    // todo 关联主实体的主键

    // 界面主键标识
    modelArray.push({
      name: appDataEntity?.codeName.toLowerCase(),
      prop: ModelTool.getAppEntityKeyField(appDataEntity)?.codeName.toLowerCase(),
      dataType: 'FRONTKEY',
    });

    return modelArray;
  }
}
