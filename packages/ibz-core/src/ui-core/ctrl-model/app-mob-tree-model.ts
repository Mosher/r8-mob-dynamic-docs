import { IPSDETree } from '@ibiz/dynamic-model-api';

export class AppMobTreeModel {
  /**
   * 树部件实例对象
   *
   * @memberof AppMobTreeModel
   */
  public treeInstance!: IPSDETree;

  /**
   * Creates an instance of AppMobTreeModel.
   *
   * @param {*} [opts={}]
   * @memberof AppMobTreeModel
   */
  constructor(opts: any) {
    this.treeInstance = opts;
  }

  public getDataItems(): any[] {
    return [];
  }
}
