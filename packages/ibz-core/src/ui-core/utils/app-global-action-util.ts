import { IPSAppView, IPSAppViewRef, DynamicInstanceConfig } from '@ibiz/dynamic-model-api';
import { IUIDataParam, IUIEnvironmentParam, IUIUtilParam, AppViewEvents, IParam } from '../../interface';
import { LogUtil, Util, ViewTool } from '../../util';
import { GetModelService } from '../../app';

/**
 * 全局界面行为服务
 *
 * @export
 * @class AppGlobalUtil
 */
export class AppGlobalUtil {
  /**
   * @description 全局界面行为Map
   * @private
   * @type {Map<string, any>}
   * @memberof AppGlobalUtil
   */
  private static globalPluginAction: Map<string, any> = new Map();

  /**
   * @description 是否初始化全局行为插件
   * @private
   * @static
   * @type {boolean}
   * @memberof AppGlobalUtil
   */
  private static isInitPluginAction: boolean = false;

  /**
   * @description 初始化全局界面行为Map
   * @private
   * @memberof AppGlobalUtil
   */
  private static initGlobalPluginAction() {
    const appDEUIActions = App.getModel().getAllPSAppDEUIActions();
    if (appDEUIActions && appDEUIActions.length > 0) {
      appDEUIActions.forEach((action: any) => {
        if (action.getPSSysPFPlugin()) {
          this.globalPluginAction.set(action.codeName, action);
        }
      });
    }
    this.isInitPluginAction = true;
  }

  /**
   * @description 获取参数
   * @static
   * @param {IUIDataParam} UIDataParam 操作环境参数
   * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
   * @param {IUIUtilParam} UIUtilParam 操作工具参数
   * @return {*} 
   * @memberof AppGlobalUtil
   */
  public static getParams(
    UIDataParam: IUIDataParam,
    UIEnvironmentParam: IUIEnvironmentParam,
    UIUtilParam: IUIUtilParam,
  ) {
    let args: any[] = [];
    if (UIDataParam.data) {
      if (Array.isArray(UIDataParam.data)) {
        args = UIDataParam.data;
      } else {
        args = [UIDataParam.data];
      }
    }
    return {
      args: args,
      actionContext: UIDataParam.sender,
      event: UIDataParam.event,
      xData: UIEnvironmentParam.ctrl,
      view: UIUtilParam.view,
      srfParentDeCodeName: UIEnvironmentParam.parentDeCodeName,
      navContext: UIDataParam.navContext,
    };
  }

  /**
   * @description 执行全局界面行为
   * @static
   * @param {string} tag 界面行为标识
   * @param {IUIDataParam} UIDataParam 操作环境参数
   * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
   * @param {IUIUtilParam} UIUtilParam 操作工具参数
   * @param {*} [contextJO={}] 行为附加上下文
   * @param {*} [paramJO={}] 附加参数
   * @param {string} [srfParentDeName] 应用实体名称
   * @memberof AppGlobalUtil
   */
  public static executeGlobalAction(
    tag: string,
    UIDataParam: IUIDataParam,
    UIEnvironmentParam: IUIEnvironmentParam,
    UIUtilParam: IUIUtilParam,
    contextJO: any = {},
    paramJO: any = {},
    srfParentDeName?: string,
  ) {
    if (!this.isInitPluginAction) {
      this.initGlobalPluginAction();
    }
    if (this.globalPluginAction.get(tag)) {
      const curActionPlugin = this.globalPluginAction.get(tag);
      const importPlugin: any = App.getPluginService().getUIActionByTag(curActionPlugin?.getPSSysPFPlugin()?.pluginCode);
      if (importPlugin) {
        importPlugin().then((importModule: any) => {
          const actionPlugin = new importModule.default(curActionPlugin);
          actionPlugin.execute(UIDataParam, UIEnvironmentParam, UIUtilParam, contextJO, paramJO, srfParentDeName);
        })
      }
    } else {
      // 分发逻辑
      switch (tag) {
        case 'HELP':
          this.HELP(UIDataParam, UIEnvironmentParam, UIUtilParam, contextJO, paramJO);
          break;
        case 'Save':
          this.Save(UIDataParam, UIEnvironmentParam, UIUtilParam, contextJO, paramJO);
          break;
        case 'SaveAndExit':
          this.SaveAndExit(UIDataParam, UIEnvironmentParam, UIUtilParam, contextJO, paramJO);
          break;
        case 'SaveAndNew':
          this.SaveAndNew(UIDataParam, UIEnvironmentParam, UIUtilParam, contextJO, paramJO);
          break;
        case 'SaveRow':
          this.SaveRow(UIDataParam, UIEnvironmentParam, UIUtilParam, contextJO, paramJO);
          break;
        case 'Edit':
          this.Edit(UIDataParam, UIEnvironmentParam, UIUtilParam, contextJO, paramJO);
          break;
        case 'View':
          this.View(UIDataParam, UIEnvironmentParam, UIUtilParam, contextJO, paramJO);
          break;
        case 'PRINT':
          this.PRINT(UIDataParam, UIEnvironmentParam, UIUtilParam, contextJO, paramJO);
          break;
        case 'ViewWFStep':
          this.ViewWFStep(UIDataParam, UIEnvironmentParam, UIUtilParam, contextJO, paramJO);
          break;
        case 'ExportExcel':
          this.ExportExcel(UIDataParam, UIEnvironmentParam, UIUtilParam, contextJO, paramJO);
          break;
        case 'FirstRecord':
          this.FirstRecord(UIDataParam, UIEnvironmentParam, UIUtilParam, contextJO, paramJO);
          break;
        case 'Exit':
          this.Exit(UIDataParam, UIEnvironmentParam, UIUtilParam, contextJO, paramJO);
          break;
        case 'ToggleFilter':
          this.ToggleFilter(UIDataParam, UIEnvironmentParam, UIUtilParam, contextJO, paramJO);
          break;
        case 'SaveAndStart':
          this.SaveAndStart(UIDataParam, UIEnvironmentParam, UIUtilParam, contextJO, paramJO);
          break;
        case 'Copy':
          this.Copy(UIDataParam, UIEnvironmentParam, UIUtilParam, contextJO, paramJO);
          break;
        case 'Remove':
          this.Remove(UIDataParam, UIEnvironmentParam, UIUtilParam, contextJO, paramJO);
          break;
        case 'RemoveAndExit':
          this.RemoveAndExit(UIDataParam, UIEnvironmentParam, UIUtilParam, contextJO, paramJO);
          break;
        case 'PrevRecord':
          this.PrevRecord(UIDataParam, UIEnvironmentParam, UIUtilParam, contextJO, paramJO);
          break;
        case 'RefreshParent':
          this.RefreshParent(UIDataParam, UIEnvironmentParam, UIUtilParam, contextJO, paramJO);
          break;
        case 'RefreshAll':
          this.RefreshAll(UIDataParam, UIEnvironmentParam, UIUtilParam, contextJO, paramJO);
          break;
        case 'Import':
          this.Import(UIDataParam, UIEnvironmentParam, UIUtilParam, contextJO, paramJO);
          break;
        case 'Refresh':
          this.Refresh(UIDataParam, UIEnvironmentParam, UIUtilParam, contextJO, paramJO);
          break;
        case 'NextRecord':
          this.NextRecord(UIDataParam, UIEnvironmentParam, UIUtilParam, contextJO, paramJO);
          break;
        case 'New':
          this.New(UIDataParam, UIEnvironmentParam, UIUtilParam, contextJO, paramJO);
          break;
        case 'NewRow':
          this.NewRow(UIDataParam, UIEnvironmentParam, UIUtilParam, contextJO, paramJO);
          break;
        case 'ToggleRowEdit':
          this.ToggleRowEdit(UIDataParam, UIEnvironmentParam, UIUtilParam, contextJO, paramJO);
          break;
        case 'LastRecord':
          this.LastRecord(UIDataParam, UIEnvironmentParam, UIUtilParam, contextJO, paramJO);
          break;
        default:
          App.getNoticeService().warning(`${tag}未支持`);
      }
    }
  }

  /**
   * @description 帮助
   * @static
   * @param {IUIDataParam} UIDataParam 操作环境参数
   * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
   * @param {IUIUtilParam} UIUtilParam 操作工具参数
   * @param {IParam} [contextJO={}] 行为附加上下文
   * @param {IParam} [paramJO={}] 附加参数
   * @memberof AppGlobalUtil
   */
  public static HELP(
    UIDataParam: IUIDataParam,
    UIEnvironmentParam: IUIEnvironmentParam,
    UIUtilParam: IUIUtilParam,
    contextJO: IParam = {},
    paramJO: IParam = {},
  ) {
    App.getNoticeService().error('帮助未支持');
  }

  /**
   * @description 保存
   * @static
   * @param {IUIDataParam} UIDataParam 操作环境参数
   * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
   * @param {IUIUtilParam} UIUtilParam 操作工具参数
   * @param {IParam} [contextJO={}] 行为附加上下文
   * @param {IParam} [paramJO={}] 附加参数
   * @memberof AppGlobalUtil
   */
  public static Save(
    UIDataParam: IUIDataParam,
    UIEnvironmentParam: IUIEnvironmentParam,
    UIUtilParam: IUIUtilParam,
    contextJO: IParam = {},
    paramJO: IParam = {},
  ) {
    const { xData, actionContext } = this.getParams(UIDataParam, UIEnvironmentParam, UIUtilParam);
    if (xData && xData.save instanceof Function) {
      xData.save().then((response: any) => {
        if (!response || response.status !== 200) {
          return;
        }
      });
    } else if (actionContext.save && actionContext.save instanceof Function) {
      actionContext.save();
    }
  }

  /**
   * @description 保存并关闭
   * @static
   * @param {IUIDataParam} UIDataParam 操作环境参数
   * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
   * @param {IUIUtilParam} UIUtilParam 操作工具参数
   * @param {IParam} [contextJO={}] 行为附加上下文
   * @param {IParam} [paramJO={}] 附加参数
   * @memberof AppGlobalUtil
   */
  public static SaveAndExit(
    UIDataParam: IUIDataParam,
    UIEnvironmentParam: IUIEnvironmentParam,
    UIUtilParam: IUIUtilParam,
    contextJO: IParam = {},
    paramJO: IParam = {},
  ) {
    const { xData, actionContext } = this.getParams(UIDataParam, UIEnvironmentParam, UIUtilParam);
    if (xData && xData.save instanceof Function) {
      xData.save().then((response: any) => {
        if (response.ret) {
          xData.closeView(response.data);
        }
      });
    } else if (actionContext.saveAndExit && actionContext.saveAndExit instanceof Function) {
      actionContext.saveAndExit().then((response: any) => {
        if (!response || response.status !== 200) {
          return;
        }
      });
    }
  }

  /**
   * @description 保存并新建
   * @static
   * @param {IUIDataParam} UIDataParam 操作环境参数
   * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
   * @param {IUIUtilParam} UIUtilParam 操作工具参数
   * @param {IParam} [contextJO={}] 行为附加上下文
   * @param {IParam} [paramJO={}] 附加参数
   * @memberof AppGlobalUtil
   */
  public static SaveAndNew(
    UIDataParam: IUIDataParam,
    UIEnvironmentParam: IUIEnvironmentParam,
    UIUtilParam: IUIUtilParam,
    contextJO: IParam = {},
    paramJO: IParam = {},
  ) {
    const { xData } = this.getParams(UIDataParam, UIEnvironmentParam, UIUtilParam);
    if (!xData || !(xData.save instanceof Function)) {
      return;
    }
    xData.save().then((response: any) => {
      if (response.ret) {
        if (xData.autoLoad instanceof Function) {
          xData.autoLoad();
        }
      }
    });
  }

  /**
   * @description 保存行
   * @static
   * @param {IUIDataParam} UIDataParam 操作环境参数
   * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
   * @param {IUIUtilParam} UIUtilParam 操作工具参数
   * @param {IParam} [contextJO={}] 行为附加上下文
   * @param {IParam} [paramJO={}] 附加参数
   * @memberof AppGlobalUtil
   */
  public static SaveRow(
    UIDataParam: IUIDataParam,
    UIEnvironmentParam: IUIEnvironmentParam,
    UIUtilParam: IUIUtilParam,
    contextJO: IParam = {},
    paramJO: IParam = {},
  ) {
    App.getNoticeService().error('保存行未支持');
  }

  /**
   * @description 编辑
   * @static
   * @param {IUIDataParam} UIDataParam 操作环境参数
   * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
   * @param {IUIUtilParam} UIUtilParam 操作工具参数
   * @param {IParam} [contextJO={}] 行为附加上下文
   * @param {IParam} [paramJO={}] 附加参数
   * @memberof AppGlobalUtil
   */
  public static Edit(
    UIDataParam: IUIDataParam,
    UIEnvironmentParam: IUIEnvironmentParam,
    UIUtilParam: IUIUtilParam,
    contextJO: IParam = {},
    paramJO: IParam = {},
  ) {
    const { args, event, xData, view, srfParentDeCodeName, navContext } = this.getParams(
      UIDataParam,
      UIEnvironmentParam,
      UIUtilParam,
    );
    const data: any = {};
    if (srfParentDeCodeName) {
      Object.assign(data, {
        [srfParentDeCodeName.toLowerCase()]:
          args[0][srfParentDeCodeName.toLowerCase()] || navContext?.[srfParentDeCodeName.toLowerCase()],
      });
    }
    ViewTool.opendata([{ ...data }], view, args, paramJO, event, xData);
  }

  /**
   * @description 查看
   * @static
   * @param {IUIDataParam} UIDataParam 操作环境参数
   * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
   * @param {IUIUtilParam} UIUtilParam 操作工具参数
   * @param {IParam} [contextJO={}] 行为附加上下文
   * @param {IParam} [paramJO={}] 附加参数
   * @memberof AppGlobalUtil
   */
  public static View(
    UIDataParam: IUIDataParam,
    UIEnvironmentParam: IUIEnvironmentParam,
    UIUtilParam: IUIUtilParam,
    contextJO: IParam = {},
    paramJO: IParam = {},
  ) {
    App.getNoticeService().error('查看未支持');
  }

  /**
   * @description 打印
   * @static
   * @param {IUIDataParam} UIDataParam 操作环境参数
   * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
   * @param {IUIUtilParam} UIUtilParam 操作工具参数
   * @param {IParam} [contextJO={}] 行为附加上下文
   * @param {IParam} [paramJO={}] 附加参数
   * @memberof AppGlobalUtil
   */
  public static PRINT(
    UIDataParam: IUIDataParam,
    UIEnvironmentParam: IUIEnvironmentParam,
    UIUtilParam: IUIUtilParam,
    contextJO: IParam = {},
    paramJO: IParam = {},
  ) {
    App.getNoticeService().error('打印未支持');
  }

  /**
   * @description 当前流程步骤
   * @static
   * @param {IUIDataParam} UIDataParam 操作环境参数
   * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
   * @param {IUIUtilParam} UIUtilParam 操作工具参数
   * @param {IParam} [contextJO={}] 行为附加上下文
   * @param {IParam} [paramJO={}] 附加参数
   * @memberof AppGlobalUtil
   */
  public static ViewWFStep(
    UIDataParam: IUIDataParam,
    UIEnvironmentParam: IUIEnvironmentParam,
    UIUtilParam: IUIUtilParam,
    contextJO: IParam = {},
    paramJO: IParam = {},
  ) {
    const { args, xData, actionContext } = this.getParams(UIDataParam, UIEnvironmentParam, UIUtilParam);
    if (!xData || !(xData.wfsubmit instanceof Function)) {
      return;
    }
    xData.wfsubmit(args).then((response: any) => {
      if (!response || response.status !== 200) {
        return;
      }
      const { data: _data } = response;

      if (actionContext.viewdata) {
        actionContext.$emit(AppViewEvents.DATA_CHANGE, [{ ..._data }]);
        actionContext.$emit(AppViewEvents.CLOSE);
      }
    });
  }

  /**
   * @description 导出
   * @static
   * @param {IUIDataParam} UIDataParam 操作环境参数
   * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
   * @param {IUIUtilParam} UIUtilParam 操作工具参数
   * @param {IParam} [contextJO={}] 行为附加上下文
   * @param {IParam} [paramJO={}] 附加参数
   * @memberof AppGlobalUtil
   */
  public static ExportExcel(
    UIDataParam: IUIDataParam,
    UIEnvironmentParam: IUIEnvironmentParam,
    UIUtilParam: IUIUtilParam,
    contextJO: IParam = {},
    paramJO: IParam = {},
  ) {
    App.getNoticeService().error('导出未支持');
  }

  /**
   * @description 第一个记录
   * @static
   * @param {IUIDataParam} UIDataParam 操作环境参数
   * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
   * @param {IUIUtilParam} UIUtilParam 操作工具参数
   * @param {IParam} [contextJO={}] 行为附加上下文
   * @param {IParam} [paramJO={}] 附加参数
   * @memberof AppGlobalUtil
   */
  public static FirstRecord(
    UIDataParam: IUIDataParam,
    UIEnvironmentParam: IUIEnvironmentParam,
    UIUtilParam: IUIUtilParam,
    contextJO: IParam = {},
    paramJO: IParam = {},
  ) {
    App.getNoticeService().error('第一个记录未支持');
  }

  /**
   * @description 关闭
   * @static
   * @param {IUIDataParam} UIDataParam 操作环境参数
   * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
   * @param {IUIUtilParam} UIUtilParam 操作工具参数
   * @param {IParam} [contextJO={}] 行为附加上下文
   * @param {IParam} [paramJO={}] 附加参数
   * @memberof AppGlobalUtil
   */
  public static Exit(
    UIDataParam: IUIDataParam,
    UIEnvironmentParam: IUIEnvironmentParam,
    UIUtilParam: IUIUtilParam,
    contextJO: IParam = {},
    paramJO: IParam = {},
  ) {
    const { args, actionContext } = this.getParams(UIDataParam, UIEnvironmentParam, UIUtilParam);
    actionContext.closeView(args);
  }

  /**
   * @description 过滤
   * @static
   * @param {IUIDataParam} UIDataParam 操作环境参数
   * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
   * @param {IUIUtilParam} UIUtilParam 操作工具参数
   * @param {IParam} [contextJO={}] 行为附加上下文
   * @param {IParam} [paramJO={}] 附加参数
   * @memberof AppGlobalUtil
   */
  public static ToggleFilter(
    UIDataParam: IUIDataParam,
    UIEnvironmentParam: IUIEnvironmentParam,
    UIUtilParam: IUIUtilParam,
    contextJO: IParam = {},
    paramJO: IParam = {},
  ) {
    const { actionContext } = this.getParams(UIDataParam, UIEnvironmentParam, UIUtilParam);
    if (actionContext.hasOwnProperty('isExpandSearchForm')) {
      actionContext.isExpandSearchForm = !actionContext.isExpandSearchForm;
    }
  }

  /**
   * @description 开始流程
   * @static
   * @param {IUIDataParam} UIDataParam 操作环境参数
   * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
   * @param {IUIUtilParam} UIUtilParam 操作工具参数
   * @param {IParam} [contextJO={}] 行为附加上下文
   * @param {IParam} [paramJO={}] 附加参数
   * @memberof AppGlobalUtil
   */
  public static async SaveAndStart(
    UIDataParam: IUIDataParam,
    UIEnvironmentParam: IUIEnvironmentParam,
    UIUtilParam: IUIUtilParam,
    contextJO: IParam = {},
    paramJO: IParam = {},
  ) {
    const { args, xData, actionContext } = this.getParams(UIDataParam, UIEnvironmentParam, UIUtilParam);
    if (!xData || !(xData.wfstart instanceof Function) || !actionContext.appEntityService) {
      return;
    }
    if (!(xData && xData.formValidateStatus())) {
      return;
    }
    const startWorkFlow: Function = (param: any, localdata: any) => {
      xData.wfstart(param, localdata).then((response: any) => {
        if (!response || response.status !== 200) {
          return;
        }
        const { data: _data } = response;
        actionContext.closeView(_data);
      });
    };
    const openStartView: Function = async (item: any, localdata: any) => {
      if (item['wfversion']) {
        if ((actionContext.viewInstance as IPSAppView)?.getPSAppViewRefs?.()?.length) {
          const targetView: IPSAppViewRef = actionContext.viewInstance.getPSAppViewRefs().find((element: any) => {
            return `WFSTART@${item['wfversion']}` === element.name;
          });
          const targetOpenView: any = targetView.getRefPSAppView();
          if (targetOpenView) {
            await targetOpenView.fill(true);
            // 准备参数
            const tempContext: any = Util.deepCopy(actionContext.context);
            const tempViewParam: any = { actionView: `WFSTART@${item['wfversion']}`, actionForm: item['process-form'] };
            Object.assign(tempContext, { viewpath: targetOpenView.modelFilePath });
            const appmodal = actionContext.$appmodal.openModal(
              {
                viewname: 'app-view-shell',
                title: actionContext.$tl(targetOpenView.getCapPSLanguageRes()?.lanResTag, targetOpenView.caption),
                height: targetOpenView.height,
                width: targetOpenView.width,
              },
              tempContext,
              tempViewParam,
            );
            appmodal.subscribe((result: any) => {
              if (!result || !Object.is(result.ret, 'OK')) {
                return;
              }
              const tempSubmitData: any = Util.deepCopy(args[0]);
              if (result.datas && result.datas[0]) {
                const resultData: any = result.datas[0];
                if (Object.keys(resultData).length > 0) {
                  const tempData: any = {};
                  Object.keys(resultData).forEach((key: any) => {
                    if (resultData[key] || resultData[key] === 0 || resultData[key] === false) {
                      tempData[key] = resultData[key];
                    }
                  });
                  Object.assign(tempSubmitData, tempData);
                }
              }
              startWorkFlow([tempSubmitData], localdata);
            });
          }
        } else {
          startWorkFlow(args, localdata);
        }
      } else {
        startWorkFlow(args, localdata);
      }
    };
    let localdata: any;
    let requestResult: Promise<any>;
    const copyContext: any = Util.deepCopy(actionContext.context);
    if (copyContext.srfdynainstid) {
      const dynainstParam: DynamicInstanceConfig = (await GetModelService(copyContext)).getDynaInsConfig();
      Object.assign(copyContext, dynainstParam ? dynainstParam : {});
      requestResult = actionContext.appEntityService.getCopyWorkflow(copyContext);
    } else {
      requestResult = actionContext.appEntityService.getStandWorkflow(copyContext);
    }
    requestResult.then((response: any) => {
      const { data: targetData, status: status } = response;
      if (status !== 200 || targetData.length === 0) {
        return;
      }
      if (targetData && targetData.length > 1) {
        targetData.forEach((element: any) => {
          Object.assign(element, { value: element.definitionkey, label: element.definitionname });
        });
        const h = actionContext.$createElement;
        actionContext
          .$msgbox({
            title: '请选择流程版本',
            message: h(
              'i-select',
              {
                key: Util.createUUID(),
                props: {
                  value: localdata,
                  placeholder: '请选择流程版本...',
                  transfer: true,
                  transferClassName: 'start-workflow-select-wraper',
                },
                on: {
                  'on-change': (event: any) => {
                    localdata = { processDefinitionKey: event };
                  },
                },
              },
              targetData.map((item: any) => {
                return h('i-option', {
                  key: item.value,
                  props: {
                    value: item.value,
                    label: item.label,
                  },
                });
              }),
            ),
            showCancelButton: true,
            confirmButtonText: '确定',
            cancelButtonText: '取消',
          })
          .then((action: string) => {
            if (Object.is(action, 'confirm') && localdata && Object.keys(localdata).length > 0) {
              const targetItem: any = targetData.find((item: any) => {
                return item.definitionkey === localdata.processDefinitionKey;
              });
              openStartView(targetItem, localdata);
            }
          });
      } else {
        localdata = { processDefinitionKey: targetData[0]['definitionkey'] };
        targetData[0]['process-view'] = 'WFSTART@1';
        openStartView(targetData[0], localdata);
      }
    });
  }

  /**
   * @description 拷贝
   * @static
   * @param {IUIDataParam} UIDataParam 操作环境参数
   * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
   * @param {IUIUtilParam} UIUtilParam 操作工具参数
   * @param {IParam} [contextJO={}] 行为附加上下文
   * @param {IParam} [paramJO={}] 附加参数
   * @memberof AppGlobalUtil
   */
  public static Copy(
    UIDataParam: IUIDataParam,
    UIEnvironmentParam: IUIEnvironmentParam,
    UIUtilParam: IUIUtilParam,
    contextJO: IParam = {},
    paramJO: IParam = {},
  ) {
    const { args, actionContext, xData, srfParentDeCodeName, navContext, event, view } = this.getParams(
      UIDataParam,
      UIEnvironmentParam,
      UIUtilParam,
    );
    if (args.length === 0 || !actionContext || !view) {
      return;
    }
    const data = {};
    if (srfParentDeCodeName) {
      Object.assign(data, {
        [srfParentDeCodeName.toLowerCase()]:
          args[0][srfParentDeCodeName.toLowerCase()] || navContext[srfParentDeCodeName.toLowerCase()],
      });
    }
    Object.assign(paramJO, { copymode: true });
    ViewTool.newdata([{ ...data }], view, args, paramJO, event, xData);
  }

  /**
   * @description 删除
   * @static
   * @param {IUIDataParam} UIDataParam 操作环境参数
   * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
   * @param {IUIUtilParam} UIUtilParam 操作工具参数
   * @param {IParam} [contextJO={}] 行为附加上下文
   * @param {IParam} [paramJO={}] 附加参数
   * @memberof AppGlobalUtil
   */
  public static Remove(
    UIDataParam: IUIDataParam,
    UIEnvironmentParam: IUIEnvironmentParam,
    UIUtilParam: IUIUtilParam,
    contextJO: IParam = {},
    paramJO: IParam = {},
  ) {
    const { args, xData } = this.getParams(UIDataParam, UIEnvironmentParam, UIUtilParam);
    if (!xData || !(xData.remove instanceof Function)) {
      return;
    }
    xData.remove(args);
  }

  /**
   * @description 删除并关闭
   * @static
   * @param {IUIDataParam} UIDataParam 操作环境参数
   * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
   * @param {IUIUtilParam} UIUtilParam 操作工具参数
   * @param {IParam} [contextJO={}] 行为附加上下文
   * @param {IParam} [paramJO={}] 附加参数
   * @memberof AppGlobalUtil
   */
  public static RemoveAndExit(
    UIDataParam: IUIDataParam,
    UIEnvironmentParam: IUIEnvironmentParam,
    UIUtilParam: IUIUtilParam,
    contextJO: IParam = {},
    paramJO: IParam = {},
  ) {
    const { xData, actionContext } = this.getParams(UIDataParam, UIEnvironmentParam, UIUtilParam);
    if (xData && xData.remove instanceof Function) {
      xData.remove().then((response: any) => {
        if (response.ret) {
          xData.closeView(response.data);
        }
      });
    } else if (actionContext.removeAndExit && actionContext.removeAndExit instanceof Function) {
      actionContext.removeAndExit().then((response: any) => {
        if (!response || response.status !== 200) {
          return;
        }
      });
    }
  }

  /**
   * @description 上一个记录
   * @static
   * @param {IUIDataParam} UIDataParam 操作环境参数
   * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
   * @param {IUIUtilParam} UIUtilParam 操作工具参数
   * @param {IParam} [contextJO={}] 行为附加上下文
   * @param {IParam} [paramJO={}] 附加参数
   * @memberof AppGlobalUtil
   */
  public static PrevRecord(
    UIDataParam: IUIDataParam,
    UIEnvironmentParam: IUIEnvironmentParam,
    UIUtilParam: IUIUtilParam,
    contextJO: IParam = {},
    paramJO: IParam = {},
  ) {
    App.getNoticeService().error('上一个记录未支持');
  }

  /**
   * @description 树刷新父数据
   * @static
   * @param {IUIDataParam} UIDataParam 操作环境参数
   * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
   * @param {IUIUtilParam} UIUtilParam 操作工具参数
   * @param {IParam} [contextJO={}] 行为附加上下文
   * @param {IParam} [paramJO={}] 附加参数
   * @memberof AppGlobalUtil
   */
  public static RefreshParent(
    UIDataParam: IUIDataParam,
    UIEnvironmentParam: IUIEnvironmentParam,
    UIUtilParam: IUIUtilParam,
    contextJO: IParam = {},
    paramJO: IParam = {},
  ) {
    const { xData, actionContext } = this.getParams(UIDataParam, UIEnvironmentParam, UIUtilParam);
    if (xData && xData.refreshParent && xData.refreshParent instanceof Function) {
      xData.refreshParent();
      return;
    }
    if (actionContext.refreshParent && actionContext.refreshParent instanceof Function) {
      actionContext.refreshParent();
      return;
    }
  }

  /**
   * @description 树刷新全部节点
   * @static
   * @param {IUIDataParam} UIDataParam 操作环境参数
   * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
   * @param {IUIUtilParam} UIUtilParam 操作工具参数
   * @param {IParam} [contextJO={}] 行为附加上下文
   * @param {IParam} [paramJO={}] 附加参数
   * @memberof AppGlobalUtil
   */
  public static RefreshAll(
    UIDataParam: IUIDataParam,
    UIEnvironmentParam: IUIEnvironmentParam,
    UIUtilParam: IUIUtilParam,
    contextJO: IParam = {},
    paramJO: IParam = {},
  ) {
    const { xData, actionContext } = this.getParams(UIDataParam, UIEnvironmentParam, UIUtilParam);
    if (xData && xData.refreshAll && xData.refreshAll instanceof Function) {
      xData.refreshAll();
      return;
    }
    if (actionContext.refreshAll && actionContext.refreshAll instanceof Function) {
      actionContext.refreshAll();
      return;
    }
    if (actionContext.engine) {
      actionContext.engine.load();
    }
  }

  /**
   * @description 数据导入
   * @static
   * @param {IUIDataParam} UIDataParam 操作环境参数
   * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
   * @param {IUIUtilParam} UIUtilParam 操作工具参数
   * @param {IParam} [contextJO={}] 行为附加上下文
   * @param {IParam} [paramJO={}] 附加参数
   * @memberof AppGlobalUtil
   */
  public static Import(
    UIDataParam: IUIDataParam,
    UIEnvironmentParam: IUIEnvironmentParam,
    UIUtilParam: IUIUtilParam,
    contextJO: IParam = {},
    paramJO: IParam = {},
  ) {
    App.getNoticeService().error('数据导入未支持');
  }

  /**
   * @description 刷新
   * @static
   * @param {IUIDataParam} UIDataParam 操作环境参数
   * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
   * @param {IUIUtilParam} UIUtilParam 操作工具参数
   * @param {IParam} [contextJO={}] 行为附加上下文
   * @param {IParam} [paramJO={}] 附加参数
   * @memberof AppGlobalUtil
   */
  public static Refresh(
    UIDataParam: IUIDataParam,
    UIEnvironmentParam: IUIEnvironmentParam,
    UIUtilParam: IUIUtilParam,
    contextJO: IParam = {},
    paramJO: IParam = {},
  ) {
    const { args, xData, actionContext } = this.getParams(UIDataParam, UIEnvironmentParam, UIUtilParam);
    if (xData && xData.refresh && xData.refresh instanceof Function) {
      xData.refresh(args);
    } else if (actionContext.refresh && actionContext.refresh instanceof Function) {
      actionContext.refresh(args);
    }
  }

  /**
   * @description 下一个记录
   * @static
   * @param {IUIDataParam} UIDataParam 操作环境参数
   * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
   * @param {IUIUtilParam} UIUtilParam 操作工具参数
   * @param {IParam} [contextJO={}] 行为附加上下文
   * @param {IParam} [paramJO={}] 附加参数
   * @memberof AppGlobalUtil
   */
  public static NextRecord(
    UIDataParam: IUIDataParam,
    UIEnvironmentParam: IUIEnvironmentParam,
    UIUtilParam: IUIUtilParam,
    contextJO: IParam = {},
    paramJO: IParam = {},
  ) {
    App.getNoticeService().error('下一个记录未支持');
  }

  /**
   * @description 新建
   * @static
   * @param {IUIDataParam} UIDataParam 操作环境参数
   * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
   * @param {IUIUtilParam} UIUtilParam 操作工具参数
   * @param {IParam} [contextJO={}] 行为附加上下文
   * @param {IParam} [paramJO={}] 附加参数
   * @memberof AppGlobalUtil
   */
  public static New(
    UIDataParam: IUIDataParam,
    UIEnvironmentParam: IUIEnvironmentParam,
    UIUtilParam: IUIUtilParam,
    contextJO: IParam = {},
    paramJO: IParam = {},
  ) {
    const { actionContext, xData, event, view} = this.getParams(UIDataParam, UIEnvironmentParam, UIUtilParam);
    if (!actionContext || !view) {
      return;
    }
    ViewTool.newdata([{}], view, [{}], paramJO, event, xData);
  }

  /**
   * @description 新建行
   * @static
   * @param {IUIDataParam} UIDataParam 操作环境参数
   * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
   * @param {IUIUtilParam} UIUtilParam 操作工具参数
   * @param {IParam} [contextJO={}] 行为附加上下文
   * @param {IParam} [paramJO={}] 附加参数
   * @memberof AppGlobalUtil
   */
  public static NewRow(
    UIDataParam: IUIDataParam,
    UIEnvironmentParam: IUIEnvironmentParam,
    UIUtilParam: IUIUtilParam,
    contextJO: IParam = {},
    paramJO: IParam = {},
  ) {
    App.getNoticeService().error('新建行未支持');
  }

  /**
   * @description 行编辑
   * @static
   * @param {IUIDataParam} UIDataParam 操作环境参数
   * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
   * @param {IUIUtilParam} UIUtilParam 操作工具参数
   * @param {IParam} [contextJO={}] 行为附加上下文
   * @param {IParam} [paramJO={}] 附加参数
   * @memberof AppGlobalUtil
   */
  public static ToggleRowEdit(
    UIDataParam: IUIDataParam,
    UIEnvironmentParam: IUIEnvironmentParam,
    UIUtilParam: IUIUtilParam,
    contextJO: IParam = {},
    paramJO: IParam = {},
  ) {
    App.getNoticeService().error('行编辑未支持');
  }

  /**
   * @description 最后一个记录
   * @static
   * @param {IUIDataParam} UIDataParam 操作环境参数
   * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
   * @param {IUIUtilParam} UIUtilParam 操作工具参数
   * @param {IParam} [contextJO={}] 行为附加上下文
   * @param {IParam} [paramJO={}] 附加参数
   * @memberof AppGlobalUtil
   */
  public static LastRecord(
    UIDataParam: IUIDataParam,
    UIEnvironmentParam: IUIEnvironmentParam,
    UIUtilParam: IUIUtilParam,
    contextJO: IParam = {},
    paramJO: IParam = {},
  ) {
    App.getNoticeService().error('最后一个记录未支持');
  }
}
