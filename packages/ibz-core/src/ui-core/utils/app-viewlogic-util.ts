import {
  getPSUIActionByModelObject,
  IPSAppDEDataView,
  IPSAppDEUIAction,
  IPSAppViewLogic,
  IPSAppViewUIAction,
  IPSControl,
} from '@ibiz/dynamic-model-api';
import { IParam, IUIDataParam, IUIEnvironmentParam, IUIUtilParam } from '../../interface';
import { LogUtil } from '../../util';
import { AppGlobalUtil } from './app-global-action-util';

/**
 * 视图逻辑服务
 *
 * @export
 * @class AppViewLogicUtil
 */
export class AppViewLogicUtil {
  /**
   * 执行视图逻辑
   *
   * @param item 触发标识
   * @param UIDataParam 操作数据参数
   * @param UIEnvironmentParam 操作环境参数
   * @param UIUtilParam 操作工具参数
   * @param viewlogicData 当前容器视图逻辑集合
   * @param params 附加参数
   * @memberof AppViewLogicUtil
   */
  public static executeViewLogic(
    item: string | IPSAppViewLogic | null,
    UIDataParam: IUIDataParam,
    UIEnvironmentParam: IUIEnvironmentParam,
    UIUtilParam: IUIUtilParam,
    viewlogicData?: Array<IPSAppViewLogic> | null,
    params?: IParam,
  ) {
    if (!item) {
      LogUtil.warn('无法找到当前视图逻辑');
      return;
    }
    let targetViewLogic: any;
    if (typeof item === 'string') {
      targetViewLogic = (viewlogicData as IPSAppViewLogic[]).find((_item: any) => {
        return _item.name === item;
      });
    } else {
      targetViewLogic = item;
    }
    this.executeLogic(targetViewLogic, UIDataParam, UIEnvironmentParam, UIUtilParam, params);
  }

  /**
   * 执行逻辑
   *
   * @param viewLogic 触发逻辑
   * @param UIDataParam 操作数据参数
   * @param UIEnvironmentParam 操作环境参数
   * @param UIUtilParam 操作工具参数
   * @param params 附加参数
   */
  public static async executeLogic(
    viewLogic: IPSAppViewLogic,
    UIDataParam: IUIDataParam,
    UIEnvironmentParam: IUIEnvironmentParam,
    UIUtilParam: IUIUtilParam,
    params?: IParam,
  ) {
    if (!viewLogic) {
      LogUtil.warn('无事件参数未支持');
      return;
    }
    if (!Object.is(viewLogic.logicType, 'APPVIEWUIACTION') || !viewLogic.getPSAppViewUIAction) {
      return;
    }
    const { sender: actionContext } = UIDataParam;
    const targetViewAction: IPSAppViewUIAction | null = viewLogic.getPSAppViewUIAction();
    if (!targetViewAction) {
      LogUtil.warn('视图界面行为不存在');
      return;
    }
    await (targetViewAction as IPSAppViewUIAction).fill();
    const targetUIAction = (await getPSUIActionByModelObject(targetViewAction)) as IPSAppDEUIAction;
    const contextJO: any = {};
    const paramJO: any = {};
    if (targetUIAction && targetUIAction.getPSAppDataEntity()) {
      const targetParentObject: IPSAppDEDataView | IPSControl = viewLogic.getParentPSModelObject() as
        | IPSAppDEDataView
        | IPSControl;
      const targetUIService: any = await App.getUIService().getService(
        actionContext.context,
        `${targetUIAction.getPSAppDataEntity()?.codeName.toLowerCase()}`,
      );
      await targetUIService.loaded();
      const targetParentDataEntity = targetParentObject?.getPSAppDataEntity?.();
      Object.assign(contextJO, { srfparentdemapname: targetParentDataEntity?.getPSDEName() });
      UIEnvironmentParam.parentDeCodeName = targetParentDataEntity?.codeName?.toLowerCase() || '';
      targetUIService.excuteAction(
        targetUIAction.uIActionTag,
        UIDataParam,
        UIEnvironmentParam,
        UIUtilParam,
        contextJO,
        paramJO,
      );
    } else {
      if (viewLogic.getParentPSModelObject() && viewLogic.getParentPSModelObject().M.getPSAppDataEntity) {
        const targetParentObject: IPSAppDEDataView | IPSControl = viewLogic.getParentPSModelObject() as
          | IPSAppDEDataView
          | IPSControl;
        const targetParentDataEntity = targetParentObject?.getPSAppDataEntity?.();
        Object.assign(contextJO, { srfparentdemapname: targetParentDataEntity?.getPSDEName() });
        UIEnvironmentParam.parentDeCodeName = targetParentDataEntity?.codeName.toLowerCase() || '';
        AppGlobalUtil.executeGlobalAction(
          targetUIAction.uIActionTag,
          UIDataParam,
          UIEnvironmentParam,
          UIUtilParam,
          contextJO,
          paramJO,
        );
      } else {
        AppGlobalUtil.executeGlobalAction(targetUIAction.uIActionTag, UIDataParam, UIEnvironmentParam, UIUtilParam);
      }
    }
  }
}
