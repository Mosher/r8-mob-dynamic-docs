import { IApp } from '../interface';

/**
 * 安装插件
 *
 * @param config 配置
 */
export const UIInstall = function (App: IApp): void {
  window.App = App;
};
