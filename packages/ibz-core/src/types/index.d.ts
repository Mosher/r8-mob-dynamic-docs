export namespace ibzCore {}

import { IApp } from '../interface';

declare global {
  interface Window {
    App: IApp;
  }
  const App: IApp;
}
