import { IPSApplication } from '@ibiz/dynamic-model-api';
import {
  IUIServiceRegister,
  IAuthServiceRegister,
  IUtilServiceRegister,
  IEntityServiceRegister,
  ICodeListService,
  ICounterService,
  IViewMessageService,
  ILoadingService,
  IMsgboxService,
  IActionService,
  IViewOpenService,
  IAppNoticeService,
  IComponentService,
  IPluginService,
  IAppCenterService,
  IAppActionSheetService,
  IAppFuncService,
  IAppThemeService,
  IApp,
  IContext,
  IParam,
  IHttp,
  IAppStorageService,
  IAppAuthService,
} from '../interface';
import { Http } from '../net';

/**
 * 应用
 *
 * @export
 * @class AppBase
 */
export abstract class AppBase implements IApp {
  /**
   * 应用级上下文
   *
   * @type {any}
   * @memberof AppBase
   */
  context: IContext | null = null;

  /**
   * 应用本地数据
   *
   * @type {any}
   * @memberof AppBase
   */
  localData: IParam | null = null;

  /**
   * 权限资源数据
   *
   * @type {any}
   * @memberof AppBase
   */
  authResData: IParam | null = null;

  /**
   * 是否开启权限认证
   *
   * @type {boolean}
   * @memberof AppBase
   */
  enablepermissionvalid: boolean = false;

  /**
   * 当前激活语言
   *
   * @type {string}
   * @memberof AppBase
   */
  activeLanguage: string = '';

  /**
   * 应用级环境数据
   *
   * @type {any}
   * @memberof AppBase
   */
  environment: IParam | null = null;

  /**
   * 当前运行平台类型
   *
   * @private
   * @type {('ios' | 'android' | 'web')}
   * @memberof AppBase
   */
  platFormType: 'ios' | 'android' | 'dd' | 'wx-mini' | 'wx-pn' | 'desktop' | 'web' = 'web';

  /**
   * 网络请求对象
   *
   * @private
   * @type {Http}
   * @memberof AppBase
   */
  readonly http: IHttp = Http.getInstance();

  /**
   * 应用模型数据对象
   *
   * @private
   * @type {any}
   * @memberof AppBase
   */
  modelDataObject: any;

  /**
   * Creates an instance of AppBase.
   *
   * @memberof AppBase
   */
  constructor() { }

  /**
   * 获取应用激活语言
   *
   * @memberof AppBase
   */
  getActiveLanguage(): string {
    return this.activeLanguage;
  }

  /**
   * 设置应用激活语言
   *
   * @memberof AppBase
   */
  setActiveLanguage(lan: string): void {
    this.activeLanguage = lan;
    this.getI18n().global.locale = lan;
    localStorage.setItem('activeLanguage',lan);
  }

  /**
   * 获取当前运行平台类型
   *
   * @return {*}  {('ios' ios应用 | 'android'安卓应用 | 'dd' 钉钉应用 | 'wx-mini' 微信小程序 | 'wx-pn' 微信公众号 | 'desktop' 桌面程序 | 'web' web网页)}
   * @memberof AppBase
   */
  public getPlatFormType(): 'ios' | 'android' | 'dd' | 'wx-mini' | 'wx-pn' | 'desktop' | 'web' {
    return this.platFormType;
  }

  /**
   * 设置当前运行平台类型
   *
   * @memberof AppBase
   */
  public setPlatFormType(platFormType: 'ios' | 'android' | 'dd' | 'wx-mini' | 'wx-pn' | 'desktop' | 'web') {
    this.platFormType = platFormType;
  }

  /**
   * 获取应用模型数据对象
   *
   * @public
   * @memberof AppBase
   */
  public getModel(): IPSApplication {
    return this.modelDataObject;
  }

  /**
   * 设置应用模型数据对象
   *
   * @public
   * @memberof AppBase
   */
  public setModel(opts: IPSApplication | null) {
    this.modelDataObject = opts;
  }

  /**
   * 获取应用数据数据对象
   *
   * @public
   * @memberof AppBase
   */
  public getContext(): IContext | null {
    return this.context;
  }

  /**
   * 设置应用上下文数据对象
   *
   * @public
   * @memberof AppBase
   */
  public setContext(opts: IContext | null) {
    this.context = opts;
  }

  /**
   * 获取应用本地数据对象
   *
   * @public
   * @memberof AppBase
   */
  public getLocalData(): IParam | null {
    return this.localData;
  }

  /**
   * 设置应用本地数据对象
   *
   * @public
   * @memberof AppBase
   */
  public setLocalData(opts: IParam | null) {
    this.localData = opts;
  }

  /**
   * 获取应用环境数据对象
   *
   * @public
   * @memberof AppBase
   */
  public getEnvironment(): IParam | null {
    return this.environment;
  }

  /**
   * 设置应用环境数据对象
   *
   * @public
   * @memberof AppBase
   */
  public setEnvironment(opts: IParam | null) {
    this.environment = opts;
  }

  /**
   * 获取应用权限数据对象
   *
   * @public
   * @memberof AppBase
   */
  public getAuthResData(key: string): IParam | null {
    return this.authResData?.[key];
  }

  /**
   * 获取应用权限数据对象
   *
   * @public
   * @memberof AppBase
   */
  public setAuthResData(opts: IParam | null) {
    this.authResData = opts;
  }

  /**
   * @description 获取是否开启权限认证
   * @param {string} key
   * @return {*}  {(IParam | null)}
   * @memberof AppBase
   */
  public getEnablePermissionValid(): boolean {
    return this.enablepermissionvalid;
  }

  /**
   * @description 设置是否开启权限认证
   * @param {(IParam | null)} opts
   * @memberof AppBase
   */
  public setEnablePermissionValid(enablepermissionvalid: boolean) {
    this.enablepermissionvalid = enablepermissionvalid;
  }

  /**
   * 获取应用存储对象
   *
   * @public
   * @memberof AppBase
   */
  getStore(): IAppStorageService {
    throw new Error('Method not implemented.');
  }

  /**
   * 设置应用存储对象
   *
   * @public
   * @memberof AppBase
   */
  setStore(opts: IAppStorageService): void {
    throw new Error('Method not implemented.');
  }

  /**
   * 初始化应用
   *
   * @public
   * @memberof AppBase
   */
  onInit(opts: IParam): void {
    throw new Error('Method not implemented.');
  }

  /**
   * 销毁应用
   *
   * @public
   * @memberof AppBase
   */
  onDestroy(): void {
    throw new Error('Method not implemented.');
  }

  /**
   * 系统登录
   *
   * @public
   * @memberof AppBase
   */
  onLogin(data: { loginname: string; password: string }): Promise<IParam> {
    throw new Error('Method not implemented.');
  }

  /**
   * 系统登出
   *
   * @public
   * @memberof AppBase
   */
  onLogout(): Promise<any> {
    throw new Error('Method not implemented.');
  }

  /**
   * 清除应用数据
   *
   * @public
   * @memberof AppBase
   */
  onClearAppData(): void {
    throw new Error('Method not implemented.');
  }

  /**
   * 检查系统更新
   *
   * @public
   * @memberof AppBase
   */
  onCheckAppUpdate(): void {
    throw new Error('Method not implemented.');
  }

  /**
   * 跳转预置视图
   *
   * @public
   * @memberof AppBase
   */
  goPresetView(pageTag: 'login' | '404' | '500'): void {
    throw new Error('Method not implemented.');
  }

  /**
   * 获取用户信息
   *
   * @public
   * @memberof AppBase
   */
  getUserInfo(): IParam {
    throw new Error('Method not implemented.');
  }

  /**
   * 关于系统
   *
   * @public
   * @memberof AppBase
   */
  getSystemInfo(): IParam {
    throw new Error('Method not implemented.');
  }

  /**
   * 获取UI服务
   *
   * @public
   * @memberof AppBase
   */
  getUIService(): IUIServiceRegister {
    throw new Error('Method not implemented.');
  }

  /**
   * 获取权限服务
   *
   * @public
   * @memberof AppBase
   */
  getAuthService(): IAuthServiceRegister {
    throw new Error('Method not implemented.');
  }

  /**
   * 获取工具服务
   *
   * @public
   * @memberof AppBase
   */
  getUtilService(): IUtilServiceRegister {
    throw new Error('Method not implemented.');
  }

  /**
   * 获取实体服务
   *
   * @public
   * @memberof AppBase
   */
  getEntityService(): IEntityServiceRegister {
    throw new Error('Method not implemented.');
  }

  /**
   * 获取代码表服务
   *
   * @public
   * @memberof AppBase
   */
  getCodeListService(): ICodeListService {
    throw new Error('Method not implemented.');
  }

  /**
   * 获取计数器服务
   *
   * @public
   * @memberof AppBase
   */
  getCounterService(): ICounterService {
    throw new Error('Method not implemented.');
  }

  /**
   * 获取视图消息服务
   *
   * @public
   * @memberof AppBase
   */
  getViewMSGService(): IViewMessageService {
    throw new Error('Method not implemented.');
  }

  /**
   * 获取loadding服务
   *
   * @public
   * @memberof AppBase
   */
  getLoadingService(): ILoadingService {
    throw new Error('Method not implemented.');
  }

  /**
   * 获取消息弹框服务
   *
   * @public
   * @memberof AppBase
   */
  getMsgboxService(): IMsgboxService {
    throw new Error('Method not implemented.');
  }

  /**
   * 获取实例化界面行为服务
   *
   * @public
   * @memberof AppBase
   */
  getActionService(): IActionService {
    throw new Error('Method not implemented.');
  }

  /**
   * 获取视图打开服务
   *
   * @public
   * @memberof AppBase
   */
  getOpenViewService(): IViewOpenService {
    throw new Error('Method not implemented.');
  }

  /**
   * 获取消息提示服务
   *
   * @public
   * @memberof AppBase
   */
  getNoticeService(): IAppNoticeService {
    throw new Error('Method not implemented.');
  }

  /**
   * 获取组件服务
   *
   * @public
   * @memberof AppBase
   */
  getComponentService(): IComponentService {
    throw new Error('Method not implemented.');
  }

  /**
   * 获取插件服务
   *
   * @public
   * @memberof AppBase
   */
  getPluginService(): IPluginService {
    throw new Error('Method not implemented.');
  }

  /**
   * 获取应用中心服务
   *
   * @public
   * @memberof AppBase
   */
  getCenterService(): IAppCenterService {
    throw new Error('Method not implemented.');
  }

  /**
   * 获取动作面板服务
   *
   * @public
   * @memberof AppBase
   */
  getActionSheetService(): IAppActionSheetService {
    throw new Error('Method not implemented.');
  }

  /**
   * 获取应用中心服务
   *
   * @public
   * @memberof AppBase
   */
  getFuncService(): IAppFuncService {
    throw new Error('Method not implemented.');
  }

  /**
   * 获取应用主题服务
   *
   * @public
   * @memberof AppBase
   */
  getAppThemeService(): IAppThemeService {
    throw new Error('Method not implemented.');
  }

  /**
   * @description 获取应用级注册
   * @return {*}  {IParam}
   * @memberof AppBase
   */
  getUserRegister(): IParam {
    throw new Error('Method not implemented.');
  }

  /**
   * @description 获取语言资源i18n
   * @return {*}  {IParam}
   * @memberof AppBase
   */
  getI18n(): IParam {
    throw new Error('Method not implemented.');
  }

  /**
   * @description 获取应用权限服务
   * @return {*}  {IParam}
   * @memberof AppBase
   */
  getAppAuthService(): IAppAuthService {
    throw new Error('Method not implemented.');
  }

  isPreviewMode(): boolean {
    return false;
  }

}