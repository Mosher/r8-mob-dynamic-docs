const inquirer = require('inquirer');
const exec = require('child_process').execSync;

function generateRelease(releaseTag) {
  switch (releaseTag) {
    case 'major':
      exec('standard-version --release-as major');
      break;
    case 'minor':
      exec('standard-version --release-as minor');
      break;
    case 'patch':
      exec('standard-version --release-as patch');
      break;
    case 'alpha':
      exec('standard-version --prerelease alpha');
      break;
  }
}

module.exports = function () {
  const promptOptions = [
    {
      type: 'list',
      name: 'type',
      message: '请选择发布版本 (default: patch)：',
      choices: [
        {
          name: '主版本(major)',
          value: 'major',
        },
        {
          name: '次版本(minor)',
          value: 'minor',
        },
        {
          name: '修订版本(patch)',
          value: 'patch',
        },
        {
          name: '先行版本(alpha)',
          value: 'alpha',
        },
      ],
      default: 'patch',
    },
  ];
  inquirer.prompt(promptOptions).then(answers => {
    generateRelease(answers.type);
  });
};
