// 导入需要的库
const genCode = require('./gen').genCode;

// 模板参数对象
const tplCtx = {
  templateType: 'control',
  controlType: 'PORTLET',
  logicName: '移动端门户部件',
  name: 'MobPortlet',
  name2: 'mob-portlet',
  extendName: 'Ctrl',
  extendName2: 'ctrl',
};

// 输出文件列表
const genList = [
  {
    tplPath: 'build/gen-code/template/interface/controller/widget/template.njk',
    outPath: `src/interface/controller/widget/i-${tplCtx.name2}-controller.ts`,
  },
  {
    isAppend: true,
    tplPath: 'build/gen-code/template/interface/controller/widget/index.njk',
    outPath: `src/interface/controller/widget/index.ts`,
  },
  {
    tplPath: 'build/gen-code/template/ui-core/controllers/widget/template.njk',
    outPath: `src/ui-core/controllers/widget/${tplCtx.name2}-controller.ts`,
  },
  {
    isAppend: true,
    tplPath: 'build/gen-code/template/ui-core/controllers/widget/index.njk',
    outPath: `src/ui-core/controllers/widget/index.ts`,
  },
  {
    tplPath: 'build/gen-code/template/ui-core/props/ctrl/template.njk',
    outPath: `src/ui-core/props/ctrl/app-${tplCtx.name2}-props.ts`,
  },
  {
    isAppend: true,
    tplPath: 'build/gen-code/template/ui-core/props/ctrl/index.njk',
    outPath: `src/ui-core/props/ctrl/index.ts`,
  },
  {
    tplPath: 'build/gen-code/template/interface/props/ctrl/template.njk',
    outPath: `src/interface/props/ctrl/i-app-${tplCtx.name2}-props.ts`,
  },
  {
    isAppend: true,
    tplPath: 'build/gen-code/template/interface/props/ctrl/index.njk',
    outPath: `src/interface/props/ctrl/index.ts`,
  },
];

try {
  for (const item of genList) {
    genCode(item.tplPath, item.outPath, { tplCtx }, item.isAppend);
  }
} catch (error) {
  console.log(error);
}
