// 导入需要的库
const genCode = require('./gen').genCode;

// 模板参数对象
const tplCtx = {
  templateType: 'editor',
  editorType: 'MOBDATE',
  logicName: '移动端时间选择编辑器',
  name: 'DatePicker',
  name2: 'date-picker',
};

// 输出文件列表
const genList = [
  {
    tplPath: 'build/gen-code/template/interface/controller/editor/template.njk',
    outPath: `src/interface/controller/editor/i-${tplCtx.name2}-controller.ts`,
  },
  {
    isAppend: true,
    tplPath: 'build/gen-code/template/interface/controller/editor/index.njk',
    outPath: `src/interface/controller/editor/index.ts`,
  },
  {
    tplPath: 'build/gen-code/template/ui-core/controllers/editor/template.njk',
    outPath: `src/ui-core/controllers/editor/${tplCtx.name2}-controller.ts`,
  },
  {
    isAppend: true,
    tplPath: 'build/gen-code/template/ui-core/controllers/editor/index.njk',
    outPath: `src/ui-core/controllers/editor/index.ts`,
  },
  {
    tplPath: 'build/gen-code/template/ui-core/props/editor/template.njk',
    outPath: `src/ui-core/props/editor/app-${tplCtx.name2}-props.ts`,
  },
  {
    isAppend: true,
    tplPath: 'build/gen-code/template/ui-core/props/editor/index.njk',
    outPath: `src/ui-core/props/editor/index.ts`,
  },
  {
    tplPath: 'build/gen-code/template/interface/props/editor/template.njk',
    outPath: `src/interface/props/editor/i-app-${tplCtx.name2}-props.ts`,
  },
  {
    isAppend: true,
    tplPath: 'build/gen-code/template/interface/props/editor/index.njk',
    outPath: `src/interface/props/editor/index.ts`,
  },
];

try {
  for (const item of genList) {
    genCode(item.tplPath, item.outPath, { tplCtx }, item.isAppend);
  }
} catch (error) {
  console.log(error);
}
