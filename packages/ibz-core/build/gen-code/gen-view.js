// 导入需要的库
const genCode = require('./gen').genCode;

// 模板参数对象
const tplCtx = {
  templateType: 'view',
  viewType: 'DEMOBWFDYNAEDITVIEW',
  logicName: '移动端动态工作流编辑视图',
  name: 'MobWFDynaEditView',
  name2: 'mob-wfdynaedit-view',
  extendName: 'DeView',
  extendName2: 'de-view',
};

// 输出文件列表
const genList = [
  {
    tplPath: 'build/gen-code/template/ui-core/props/layout/template.njk',
    outPath: `src/ui-core/props/layout/app-${tplCtx.name2}-layout-props.ts`,
  },
  {
    isAppend: true,
    tplPath: 'build/gen-code/template/ui-core/props/layout/index.njk',
    outPath: `src/ui-core/props/layout/index.ts`,
  },
  {
    tplPath: 'build/gen-code/template/interface/controller/view/template.njk',
    outPath: `src/interface/controller/view/i-${tplCtx.name2}-controller.ts`,
  },
  {
    isAppend: true,
    tplPath: 'build/gen-code/template/interface/controller/view/index.njk',
    outPath: `src/interface/controller/view/index.ts`,
  },
  {
    tplPath: 'build/gen-code/template/ui-core/controllers/view/template.njk',
    outPath: `src/ui-core/controllers/view/${tplCtx.name2}-controller.ts`,
  },
  {
    isAppend: true,
    tplPath: 'build/gen-code/template/ui-core/controllers/view/index.njk',
    outPath: `src/ui-core/controllers/view/index.ts`,
  },
  {
    tplPath: 'build/gen-code/template/ui-core/props/view/template.njk',
    outPath: `src/ui-core/props/view/app-${tplCtx.name2}-props.ts`,
  },
  {
    isAppend: true,
    tplPath: 'build/gen-code/template/ui-core/props/view/index.njk',
    outPath: `src/ui-core/props/view/index.ts`,
  },
  {
    tplPath: 'build/gen-code/template/interface/props/view/template.njk',
    outPath: `src/interface/props/view/i-app-${tplCtx.name2}-props.ts`,
  },
  {
    isAppend: true,
    tplPath: 'build/gen-code/template/interface/props/view/index.njk',
    outPath: `src/interface/props/view/index.ts`,
  },
];

try {
  for (const item of genList) {
    genCode(item.tplPath, item.outPath, { tplCtx }, item.isAppend);
  }
} catch (error) {
  console.log(error);
}
