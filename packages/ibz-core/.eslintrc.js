module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    'plugin:vue/vue3-essential',
    'eslint:recommended',
    '@vue/typescript/recommended',
    '@vue/prettier',
    '@vue/prettier/@typescript-eslint',
  ],
  parserOptions: {
    ecmaVersion: 2020,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
    },
  },
  rules: {
    //生产环境下console警告
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    //生产环境下debugger警告
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    //vue使用插槽
    'vue/no-deprecated-slot-attribute': [0],
    //使用any类型声明
    '@typescript-eslint/no-explicit-any': [0],
    //导出函数的禁用规则
    '@typescript-eslint/explicit-module-boundary-types': [0],
    //允许使用空函数
    '@typescript-eslint/no-empty-function': [0],
    //非空断言
    '@typescript-eslint/no-non-null-assertion': [0],
    //类型推断
    '@typescript-eslint/no-inferrable-types': [0],
    //未使用变量
    '@typescript-eslint/no-unused-vars': [0],
    //允许使用require语句
    '@typescript-eslint/no-var-requires': 'off',
    //禁止直接调用 `Object.prototypes` 的内置属性
    'no-prototype-builtins': 'off',
    //使用特定类型
    '@typescript-eslint/ban-types': 'off',
    //允许不必要的转义
    'no-useless-escape': 'off',
    //允许在 case 子句中使用词法声明
    'no-case-declarations': 'off',
    //成员重载可以不连续
    '@typescript-eslint/adjacent-overload-signatures': 'off',
    //promise中允许使用异步
    'no-async-promise-executor': 'off',
    // 允许空接口
    'no-empty-interface': 'off'
  },
};
