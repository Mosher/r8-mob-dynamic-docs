import { SyncSeriesHook } from 'qx-util';
import { IParam } from '../common';
import { IViewEventResult } from '../params';
/**
 * 视图hooks接口
 *
 * @export
 * @interface IAppViewHooks
 */
export interface IAppViewHooks {
    /**
     * 模型数据加载完成钩子
     *
     * @class IAppViewHooks
     */
    modelLoaded: SyncSeriesHook<[], {
        arg: IParam;
    }>;
    /**
     * 绑定事件钩子
     *
     * @interface IAppViewHooks
     */
    onEvent: SyncSeriesHook<[], {
        event: string | string[];
        fn: Function;
    }>;
    /**
     * 解绑事件钩子
     *
     * @interface IAppViewHooks
     */
    offEvent: SyncSeriesHook<[], {
        event: string | string[];
        fn?: Function;
    }>;
    /**
     * 视图事件钩子
     *
     * @interface IAppViewHooks
     */
    event: SyncSeriesHook<[], IViewEventResult>;
    /**
     * 视图刷新钩子
     *
     * @interface IAppViewHooks
     */
    refresh: SyncSeriesHook<[], {
        arg: IParam;
    }>;
    /**
     * 视图关闭钩子
     *
     * @interface IAppViewHooks
     */
    closeView: SyncSeriesHook<[], {
        arg: IParam;
    }>;
    /**
     * 挂载之后钩子
     *
     * @interface IAppViewHooks
     */
    mounted: SyncSeriesHook<[], {
        arg: IParam;
    }>;
}
