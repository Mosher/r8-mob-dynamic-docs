import { IPSAppViewMsgGroup } from '@ibiz/dynamic-model-api';
import { IParam } from '../../common';
/**
 * 视图消息服务接口
 *
 * @export
 * @interface IViewMessageService
 */
export interface IViewMessageService {
    /**
     * @description 视图消息加载
     * @param {IPSAppViewMsgGroup} opt 视图消息组实例对象
     * @param {*} [context] 应用上下文
     * @param {*} [viewParam] 视图参数
     * @return {*}  {Promise<void>}
     * @memberof IViewMessageService
     */
    loaded(opt: IPSAppViewMsgGroup, context?: any, viewParam?: any): Promise<void>;
    /**
     * @description 获取视图消息实例
     * @param {string} [position] 视图消息位置[ TOP: 视图上方，BODY: 视图内容区，BOTTOM：视图下方]，不传则默认获取全部视图消息
     * @return {*}  {IParam[]}
     * @memberof IViewMessageService
     */
    getViewMsgDetails(position?: string): IParam[];
}
