import { IParam } from '../../common';
/**
 * 应用存储接口
 *
 * @interface IAppStorageService
 */
export interface IAppStorageService {
    /**
     * 获取应用存储指定数据
     *
     * @memberof IAppStorageService
     */
    get(key: string): IParam;
    /**
     * 设置应用存储指定数据
     *
     * @memberof IAppStorageService
     */
    set(key: string, data: IParam): void;
    /**
     * 清空应用存储指定数据
     *
     * @memberof IAppStorageService
     */
    delete(key: string): void;
    /**
     * 清空应用存储所有数据
     *
     * @memberof IAppStorageService
     */
    clear(): void;
}
