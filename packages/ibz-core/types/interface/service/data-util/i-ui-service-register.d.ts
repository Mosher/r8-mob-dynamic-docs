import { IContext } from '../../common';
import { IUIService } from './i-ui-service';
/**
 * UI服务注册中心
 *
 * @export
 * @interface IUIServiceRegister
 */
export interface IUIServiceRegister {
    /**
     *
     *
     * @public
     * @memberof IUIServiceRegister
     */
    /**
     * @description 获取指定UIService
     * @param {IContext} context 应用上下文
     * @param {string} entityKey 实体代码标识
     * @return {*}  {Promise<IUIService>}
     * @memberof IUIServiceRegister
     */
    getService(context: IContext, entityKey: string): Promise<IUIService>;
}
