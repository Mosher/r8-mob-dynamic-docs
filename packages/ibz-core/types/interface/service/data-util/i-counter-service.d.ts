import { IPSAppCounter } from '@ibiz/dynamic-model-api';
import { IContext, IParam } from '../../common';
/**
 * 计数器服务接口
 *
 * @export
 * @interface ICounterService
 */
export interface ICounterService {
    /**
     * @description 计数器数据加载
     * @param {IPSAppCounter} counter 计数器实例对象
     * @param {{ navContext?: IParam, navViewParam?: IParam }} [params] { navContext：导航上下文，navViewParam：导航参数 }
     * @return {*}  {Promise<void>}
     * @memberof ICounterService
     */
    loaded(counter: IPSAppCounter, params?: {
        navContext?: IParam;
        navViewParam?: IParam;
    }): Promise<void>;
    /**
     * @description 刷新计数器数据
     * @param {IContext} context 应用上下文
     * @param {IParam} data 参数
     * @return {*}  {Promise<void>}
     * @memberof ICounterService
     */
    refreshCounterData(context: IContext, data: IParam): Promise<void>;
    /**
     * @description 销毁计数器
     * @memberof ICounterService
     */
    destroyCounter(): void;
}
