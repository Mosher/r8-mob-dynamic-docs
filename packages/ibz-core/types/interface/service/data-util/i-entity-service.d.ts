import { IContext, IParam } from '../../../interface';
/**
 * @description 实体服务接口
 * @export
 * @interface IEntityService
 */
export interface IEntityService {
    /**
     * @description 加载动态数据模型
     * @param {IContext} [context] 应用上下文
     * @param {IParam} [data] 额外数据
     * @return {*}  {Promise<void>}
     * @memberof IEntityService
     */
    loaded(context?: IContext, data?: IParam): Promise<void>;
    /**
     * @description 执行实体处理逻辑
     * @param {string} tag 逻辑标识
     * @param {*} context 应用上下文
     * @param {*} data 当前数据
     * @return {*}  {Promise<IParam>}
     * @memberof IEntityService
     */
    executeAppDELogic(tag: string, context: IContext, data: IParam): Promise<IParam>;
    /**
     * @description 执行实体属性处理逻辑
     * @param {*} model 模型对象
     * @param {IContext} context 应用上下文
     * @param {IParam} data 当前数据
     * @return {*}  {Promise<IParam>}
     * @memberof IEntityService
     */
    executeAppDEFieldLogic(model: any, context: IContext, data: IParam): Promise<IParam>;
    /**
     * @description 执行实体行为之前
     * @param {IContext} context 应用上下文
     * @param {IParam} data 当前数据
     * @param {string} [methodName]
     * @return {*}  {Promise<IParam>}
     * @memberof IEntityService
     */
    beforeExecuteAction(context: IContext, data: IParam, methodName?: string): Promise<IParam>;
    /**
     * @description 执行实体行为之后
     * @param {IContext} context 应用上下文
     * @param {IParam} data 当前数据
     * @param {string} [methodName]
     * @return {*}  {Promise<IParam>}
     * @memberof IEntityService
     */
    afterExecuteAction(context: IContext, data: IParam, methodName?: string): Promise<IParam>;
    /**
     * @description 执行实体行为之后批处理（主要用于数据集处理）
     * @param {IContext} context 应用上下文
     * @param {IParam} data 当前数据
     * @param {string} [methodName]
     * @return {*}  {Promise<IParam>}
     * @memberof IEntityService
     */
    afterExecuteActionBatch(context: IContext, dataSet: Array<any>, methodName?: string): Promise<IParam>;
    /**
     * @description 预置导入数据
     * @param {IContext} [context] 应用上下文
     * @param {IParam} [data] 当前数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<any>}
     * @memberof IEntityService
     */
    ImportData(context?: IContext, data?: IParam, isloading?: boolean): Promise<any>;
    /**
     * @description 预置批量新建
     * @param {IContext} [context] 应用上下文
     * @param {IParam} [data] 当前数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<any>}
     * @memberof IEntityService
     */
    createBatch(context?: IContext, data?: IParam, isloading?: boolean): Promise<any>;
    /**
     * @description 预置批量保存
     * @param {IContext} [context] 应用上下文
     * @param {IParam} [data] 当前数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<any>}
     * @memberof IEntityService
     */
    saveBatch(context?: IContext, data?: IParam, isloading?: boolean): Promise<any>;
    /**
     * @description 预置批量更新
     * @param {IContext} [context] 应用上下文
     * @param {IParam} [data] 当前数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<any>}
     * @memberof IEntityService
     */
    updateBatch(context?: IContext, data?: IParam, isloading?: boolean): Promise<any>;
    /**
     * @description 预置批量删除
     * @param {IContext} [context] 应用上下文
     * @param {IParam} [data] 当前数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<any>}
     * @memberof IEntityService
     */
    removeBatch(context?: IContext, data?: IParam, isloading?: boolean): Promise<any>;
    /**
     * @description 预置获取数据
     * @param {IContext} [context] 应用上下文
     * @param {IParam} [data] 当前数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<any>}
     * @memberof IEntityService
     */
    getDataInfo(context?: IContext, data?: IParam, isloading?: boolean): Promise<any>;
    /**
     * @description 获取动态模型
     * @param {IContext} [context] 应用上下文
     * @param {IParam} [data] 当前数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<any>}
     * @memberof IEntityService
     */
    getDynaModel(context?: IContext, data?: IParam, isloading?: boolean): Promise<any>;
    /**
     * @description 设置动态模型
     * @param {IContext} [context] 应用上下文
     * @param {IParam} [data] 当前数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<any>}
     * @memberof IEntityService
     */
    setDynaModel(context?: IContext, data?: IParam, isloading?: boolean): Promise<any>;
    /**
     * @description 获取指定工作流版本信息
     * @param {IContext} [context] 应用上下文
     * @param {IParam} [data] 当前数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<any>}
     * @memberof IEntityService
     */
    getDynaWorkflow(context?: IContext, data?: IParam, isloading?: boolean): Promise<any>;
    /**
     * @description 获取标准工作流版本信息
     * @param {IContext} [context] 应用上下文
     * @param {IParam} [data] 当前数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<any>}
     * @memberof IEntityService
     */
    getStandWorkflow(context?: IContext, data?: IParam, isloading?: boolean): Promise<any>;
    /**
     * @description 获取副本工作流版本信息
     * @param {IContext} [context] 应用上下文
     * @param {IParam} [data] 当前数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<any>}
     * @memberof IEntityService
     */
    getCopyWorkflow(context?: IContext, data?: IParam, isloading?: boolean): Promise<any>;
    /**
     * @description 工作流开始流程
     * @param {IContext} [context] 应用上下文
     * @param {IParam} [data] 当前数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<any>}
     * @memberof IEntityService
     */
    WFStart(context?: IContext, data?: IParam, isloading?: boolean): Promise<any>;
    /**
     * @description 工作流关闭
     * @param {IContext} [context] 应用上下文
     * @param {IParam} [data] 当前数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<any>}
     * @memberof IEntityService
     */
    WFClose(context?: IContext, data?: IParam, isloading?: boolean): Promise<any>;
    /**
     * @description WFMarkRead方法
     * @param {IContext} [context] 应用上下文
     * @param {IParam} [data] 当前数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<any>}
     * @memberof IEntityService
     */
    WFMarkRead(context?: IContext, data?: IParam, isloading?: boolean): Promise<any>;
    /**
     * @description WFGoTo方法
     * @param {IContext} [context] 应用上下文
     * @param {IParam} [data] 当前数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<any>}
     * @memberof IEntityService
     */
    WFGoto(context?: IContext, data?: IParam, isloading?: boolean): Promise<any>;
    /**
     * @description WFRollback方法
     * @param {IContext} [context] 应用上下文
     * @param {IParam} [data] 当前数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<any>}
     * @memberof IEntityService
     */
    WFRollback(context?: IContext, data?: IParam, isloading?: boolean): Promise<any>;
    /**
     * @description 工作流重启
     * @param {IContext} [context] 应用上下文
     * @param {IParam} [data] 当前数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<any>}
     * @memberof IEntityService
     */
    WFRestart(context?: IContext, data?: IParam, isloading?: boolean): Promise<any>;
    /**
     * @description WFReassign方法
     * @param {IContext} [context] 应用上下文
     * @param {IParam} [data] 当前数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<any>}
     * @memberof IEntityService
     */
    WFReassign(context?: IContext, data?: IParam, isloading?: boolean): Promise<any>;
    /**
     * @description 获取工作流定义
     * @param {IContext} [context] 应用上下文
     * @param {IParam} [data] 当前数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<any>}
     * @memberof IEntityService
     */
    WFGetWorkFlow(context?: IContext, data?: IParam, isloading?: boolean): Promise<any>;
    /**
     * @description 根据系统实体查找当前适配的工作流模型步骤
     * @param {IContext} [context] 应用上下文
     * @param {IParam} [data] 当前数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<any>}
     * @memberof IEntityService
     */
    WFGetWFStep(context?: IContext, data?: IParam, isloading?: boolean): Promise<any>;
    /**
     * @description 根据业务主键和当前步骤获取操作路径
     * @param {IContext} [context] 应用上下文
     * @param {IParam} [data] 当前数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<any>}
     * @memberof IEntityService
     */
    GetWFLink(context?: IContext, data?: IParam, isloading?: boolean): Promise<any>;
    /**
     * @description 根据当前步骤和任务获取批量操作路径
     * @param {IContext} [context] 应用上下文
     * @param {IParam} [data] 当前数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<any>}
     * @memberof IEntityService
     */
    getWFLinks(context?: IContext, data?: IParam, isloading?: boolean): Promise<any>;
    /**
     * @description 根据当前步骤和任务获取工作流步骤数据（如：流程表单等）
     * @param {IContext} [context] 应用上下文
     * @param {IParam} [data] 当前数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<any>}
     * @memberof IEntityService
     */
    getWFStep(context?: IContext, data?: IParam, isloading?: boolean): Promise<any>;
    /**
     * @description 批量提交
     * @param {IContext} [context] 应用上下文
     * @param {IParam} [data] 当前数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<any>}
     * @memberof IEntityService
     */
    wfSubmitBatch(context?: IContext, data?: IParam, isloading?: boolean): Promise<any>;
    /**
     * @description 根据业务主键获取工作流程记录
     * @param {IContext} [context] 应用上下文
     * @param {IParam} [data] 当前数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<any>}
     * @memberof IEntityService
     */
    GetWFHistory(context?: IContext, data?: IParam, isloading?: boolean): Promise<any>;
    /**
     * @description 前加签接口方法
     * @param {IContext} [context] 应用上下文
     * @param {IParam} [data] 当前数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<any>}
     * @memberof IEntityService
     */
    BeforeSign(context?: IContext, data?: IParam, isloading?: boolean): Promise<any>;
    /**
     * @description 转办接口方法
     * @param {IContext} [context] 应用上下文
     * @param {IParam} [data] 当前数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<any>}
     * @memberof IEntityService
     */
    TransFerTask(context?: IContext, data?: IParam, isloading?: boolean): Promise<any>;
    /**
     * @description 回退
     * @param {IContext} [context] 应用上下文
     * @param {IParam} [data] 当前数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<any>}
     * @memberof IEntityService
     */
    SendBack(context?: IContext, data?: IParam, isloading?: boolean): Promise<any>;
    /**
     * @description 抄送
     * @param {IContext} [context] 应用上下文
     * @param {IParam} [data] 当前数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<any>}
     * @memberof IEntityService
     */
    sendCopy(context?: IContext, data?: IParam, isloading?: boolean): Promise<any>;
    /**
     * @description 将待办任务标记为已读
     * @param {IContext} [context] 应用上下文
     * @param {IParam} [data] 当前数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<any>}
     * @memberof IEntityService
     */
    ReadTask(context?: IContext, data?: IParam, isloading?: boolean): Promise<any>;
    /**
     * @description 工作流提交
     * @param {IContext} [context] 应用上下文
     * @param {IParam} [data] 当前数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<any>}
     * @memberof IEntityService
     */
    WFSubmit(context?: IContext, data?: IParam, isloading?: boolean): Promise<any>;
    /**
     * @description WFGetProxyData接口方法
     * @param {IContext} [context] 应用上下文
     * @param {IParam} [data] 当前数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<any>}
     * @memberof IEntityService
     */
    WFGetProxyData(context?: IContext, data?: IParam, isloading?: boolean): Promise<any>;
    /**
     * @description 测试数据是否在工作流中
     * @param {IContext} [context] 应用上下文
     * @param {IParam} [data] 当前数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<any>}
     * @memberof IEntityService
     */
    testDataInWF(context?: IContext, data?: IParam, isloading?: boolean): Promise<any>;
    /**
     * @description 测试当前用户是否提交过工作流
     * @param {IContext} [context] 应用上下文
     * @param {IParam} [data] 当前数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<any>}
     * @memberof IEntityService
     */
    testUserWFSubmit(context?: IContext, data?: IParam, isloading?: boolean): Promise<any>;
    /**
     * @description 测试当前用户是否存在待办列表
     * @param {IContext} [context] 应用上下文
     * @param {IParam} [data] 当前数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<any>}
     * @memberof IEntityService
     */
    testUserExistWorklist(context?: IContext, data?: IParam, isloading?: boolean): Promise<any>;
    /**
     * @description 获取所有应用数据
     * @param {IContext} [context] 应用上下文
     * @param {IParam} [data] 当前数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<any>}
     * @memberof IEntityService
     */
    getAllApp(context?: IContext, data?: IParam, isloading?: boolean): Promise<any>;
    /**
     * @description 更新已选择的应用
     * @param {IContext} [context] 应用上下文
     * @param {IParam} [data] 当前数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<any>}
     * @memberof IEntityService
     */
    updateChooseApp(context?: IContext, data?: IParam, isloading?: boolean): Promise<any>;
    /**
     * @description 修改密码
     * @param {IContext} [context] 应用上下文
     * @param {IParam} [data] 当前数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<any>}
     * @memberof IEntityService
     */
    changPassword(context?: IContext, data?: IParam, isloading?: boolean): Promise<any>;
    /**
     * @description 获取数字字典（代码表）
     * @param {IContext} [context] 应用上下文
     * @param {IParam} [data] 当前数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<any>}
     * @memberof IEntityService
     */
    getPredefinedCodelist(tag: string, data?: IParam, isloading?: boolean): Promise<any>;
}
