import { IContext, IParam } from '../../common';
export interface IUtilService {
    /**
     * @description 获取模型数据
     * @param {IContext} context 应用上下文
     * @param {IParam} data 传入模型数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<IParam>}
     * @memberof IUtilService
     */
    loadModelData(context: IContext, data: IParam, isloading?: boolean): Promise<IParam>;
    /**
     * @description 保存模型数据
     * @param {IContext} context 应用上下文
     * @param {IParam} data 传入模型数据
     * @param {boolean} [isloading] 是否加载
     * @return {*}  {Promise<IParam>}
     * @memberof IUtilService
     */
    saveModelData(context: IContext, data: IParam, isloading?: boolean): Promise<IParam>;
}
