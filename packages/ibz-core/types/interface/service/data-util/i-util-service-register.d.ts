import { IContext } from '../../common';
import { IUtilService } from './i-util-service';
/**
 * 功能服务注册中心
 *
 * @export
 * @interface IUtilServiceRegister
 */
export interface IUtilServiceRegister {
    /**
     *
     *
     * @memberof IUtilServiceRegister
     */
    /**
     * @description 获取指定UtilService
     * @param {IContext} context 应用上下文
     * @param {string} entityKey 实体代码标识
     * @return {*}  {Promise<IUtilService>}
     * @memberof IUtilServiceRegister
     */
    getService(context: IContext, entityKey: string): Promise<IUtilService>;
}
