import { IContext } from '../../common';
import { IEntityService } from './i-entity-service';
/**
 * 实体服务注册中心
 *
 * @export
 * @interface IEntityServiceRegister
 */
export interface IEntityServiceRegister {
    /**
     * @description 获取指定实体服务
     * @param {IContext} context 应用上下文
     * @param {string} entityKey 实体标识
     * @return {*}  {Promise<IEntityService>}
     * @memberof IEntityServiceRegister
     */
    getService(context: IContext, entityKey: string): Promise<IEntityService>;
}
