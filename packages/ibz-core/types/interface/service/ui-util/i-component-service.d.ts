/**
 * 获取组件服务接口
 *
 * @export
 * @interface IComponentService
 */
export interface IComponentService {
    /**
     * @description 获取布局组件
     * @param {string} viewType 布局类型
     * @param {string} viewStyle 布局样式
     * @param {string} [pluginCode] 插件代码标识
     * @return {*} {string}
     * @memberof AppComponentService
     */
    getLayoutComponent(viewType: string, viewStyle: string, pluginCode?: string): any;
    /**
     * @description 获取视图类型组件
     * @param {string} viewType 视图类型
     * @param {string} viewStyle 视图样式
     * @param {string} [pluginCode] 插件代码标识
     * @return {*} {string}
     * @memberof AppComponentService
     */
    getViewTypeComponent(viewType: string, viewStyle: string, pluginCode?: string): any;
    /**
     * @description 获取部件组件
     * @param {string} viewType 部件类型
     * @param {string} viewStyle 部件样式
     * @param {string} [pluginCode] 部件插件代码标识
     * @return {*} {string}
     * @memberof AppComponentService
     */
    getControlComponent(ctrlType: string, ctrlStyle: string, pluginCode?: string): any;
    /**
     * @description 获取编辑器组件
     * @param {string} viewType 编辑器类型
     * @param {string} viewStyle 编辑器样式
     * @param {string} [pluginCode] 编辑器插件代码标识
     * @param {boolean} [infoMode] 是否为信息模式
     * @return {*} {string}
     * @memberof AppComponentService
     */
    getEditorComponent(editorType: string, editorStyle: string, pluginCode?: string, infoMode?: boolean): any;
}
