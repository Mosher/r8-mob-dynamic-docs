/**
 * 界面加载工具接口
 *
 * @export
 * @interface ILoadingUtil
 */
export interface ILoadingService {
    /**
     * @description 开始加载
     * @memberof ILoadingService
     */
    beginLoading(): void;
    /**
     * @description 结束加载
     * @memberof ILoadingUtil
     */
    endLoading(): void;
}
