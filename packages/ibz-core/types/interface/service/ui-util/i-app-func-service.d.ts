/**
 * 消息提示服务接口
 *
 * @export
 * @interface IAppNoticeService
 */
export interface IAppFuncService {
    /**
     * @description 执行应用功能
     * @param {*} functionTag
     * @param {*} context
     * @param {*} viewParam
     * @param {*} container
     * @param {boolean} [isGlobal]
     * @param {*} [arg]
     * @memberof IAppFuncService
     */
    executeAppFunction(functionTag: any, context: any, viewParam: any, container: any, isGlobal?: boolean, arg?: any): void;
}
