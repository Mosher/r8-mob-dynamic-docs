/**
 * @description 第三方服务接口
 * @export
 * @interface IAppThirdPartyService
 */
export interface IAppThirdPartyService {
    /**
     * 获取搭载平台
     *
     * @return {*}  {(Promise<'ios' | 'android' | 'dd' | 'wx-mini' | 'wx-pn' | 'desktop' | 'web'>)}
     * @memberof IAppThirdPartyService
     */
    getAppPlatform(): Promise<'ios' | 'android' | 'dd' | 'wx-mini' | 'wx-pn' | 'desktop' | 'web'>;
    /**
     * 获取第三方服务
     *
     * @return {*}  {(DingTalkService | WeChatService)}
     * @memberof IAppThirdPartyService
     */
    getPlatformService(): any;
    /**
     * 关闭
     *
     * @memberof IAppThirdPartyService
     */
    close(): void;
    /**
     * 第三方事件
     *
     * @param {string} tag
     * @param {*} arg
     * @return {*}  {Promise<any>}
     * @memberof IAppThirdPartyService
     */
    thirdPartyEvent(tag: string, arg: any): Promise<any>;
}
