/**
 * 插件服务接口
 *
 * @export
 * @interface IPluginService
 */
export interface IPluginService {
    /**
     * 获取界面行为插件
     *
     * @param tag 界面行为标识
     *
     * @memberof IPluginService
     */
    getUIActionByTag(tag: string): any;
    /**
     * 获取项插件
     *
     * @param tag 界面行为标识
     * @memberof IPluginService
     */
    getCtrlItemPluginByTag(tag: string): any;
}
