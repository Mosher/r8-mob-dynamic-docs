import { Subject } from 'rxjs';
import { IParam } from '../../common';
/**
 * 应用中心服务接口
 *
 * @export
 * @interface IAppCenterService
 */
export interface IAppCenterService {
    /**
     * @description 通知消息
     * @param {*} name 名称(通常是应用实体名称)
     * @param {*} action 行为（操作数据行为）
     * @param {*} data 数据（操作数据）
     * @memberof IAppCenterService
     */
    notifyMessage(name: string, action: string, data: IParam): void;
    /**
     * @description 获取消息中心
     * @return {*}  {Subject<{ name: string, action: string, data: IParam }>}
     * @memberof IAppCenterService
     */
    getMessageCenter(): Subject<{
        name: string;
        action: string;
        data: IParam;
    }>;
}
