import { IParam } from '../../common';
/**
 * @description 应用权限相关服务接口
 * @export
 * @interface IAppAuthService
 */
export interface IAppAuthService {
    /**
     * @description 登录
     * @param {{ loginname: string, password: string }} _data 登录名，密码
     * @return {*}  {Promise<IParam>}
     * @memberof IAppAuthService
     */
    login(_data: {
        loginname: string;
        password: string;
    }): Promise<IParam>;
    /**
     * @description 登出
     * @return {*}  {Promise<IParam>}
     * @memberof IAppAuthService
     */
    logout(): Promise<IParam>;
    /**
     * @description 刷新TOKEN
     * @param {IParam} data 请求相关数据
     * @return {*}  {Promise<boolean>}
     * @memberof IAppAuthService
     */
    refreshToken(data: IParam): Promise<boolean>;
    /**
     * @description 判断TOKEN是否过期
     * @param {Date} date
     * @return {*}  {boolean}
     * @memberof IAppAuthService
     */
    isTokenExpired(date: Date): boolean;
    /**
     * @description 获取TOKEN过期时间
     * @return {*}  {Date}
     * @memberof IAppAuthService
     */
    getExpiredDate(): Date;
    /**
     * @description 设置TOKEN过期时间
     * @param {Date} date 时间
     * @memberof IAppAuthService
     */
    setExpiredDate(date: Date): void;
}
