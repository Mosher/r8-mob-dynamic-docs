import { Subject } from 'rxjs';
import { IParam } from '../../common';
/**
 * 消息弹框工具接口
 *
 * @export
 * @interface IMsgboxService
 */
export interface IMsgboxService {
    /**
     * @description 打开消息弹框
     * @param {IParam} options
     * @return {*}  {(Subject<string>)}
     * @memberof IMsgboxService
     */
    open(options: IMsgboxOptions): Subject<string>;
}
/**
 * @description 消息弹框配置
 * @export
 * @interface IMsgboxOptions
 */
export interface IMsgboxOptions {
    /**
     * @description 标题
     * @type {string}
     * @memberof IMsgboxOptions
     */
    title?: string;
    /**
     * @description 显示模式
     * @type {string}
     * @memberof IMsgboxOptions
     */
    showMode?: string;
    /**
     * @description 内容
     * @type {*}
     * @memberof IMsgboxOptions
     */
    content?: any;
    /**
     * @description 类型
     * @type {string}
     * @memberof IMsgboxOptions
     */
    type?: string;
    /**
     * @description 样式
     * @type {(string | string[])}
     * @memberof IMsgboxOptions
     */
    cssClass?: string | string[];
    /**
     * @description 按钮类型
     * @type {('okcancel' | 'yesno' | 'yesnocancel' | 'ok' | string)}
     * @memberof IMsgboxOptions
     */
    buttonType?: 'okcancel' | 'yesno' | 'yesnocancel' | 'ok' | string;
    /**
     * @description 按钮
     * @type {IParam[]}
     * @memberof IMsgboxOptions
     */
    buttons?: IParam[];
}
