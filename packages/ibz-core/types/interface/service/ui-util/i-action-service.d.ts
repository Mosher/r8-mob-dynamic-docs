import { IPSAppDEUIAction } from '@ibiz/dynamic-model-api';
import { IParam } from '../../common';
/**
 * 实例化界面行为服务接口
 *
 * @export
 * @interface IActionService
 */
export interface IActionService {
    /**
     * 获取界面行为实例
     *
     * @param modelData 界面行为模型数据
     * @param context 应用上下文
     *
     */
    getUIActionInst(modelData: IPSAppDEUIAction, context: any): IParam | undefined;
}
