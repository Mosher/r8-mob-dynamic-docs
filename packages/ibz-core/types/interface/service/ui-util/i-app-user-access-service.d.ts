import { IParam } from '../../common';
/**
 * @description 访问用户服务
 * @export
 * @interface IAppAccessUserService
 */
export interface IAppAccessUserService {
    /**
     * @description 获取用户访问权限
     * @param {IParam} viewModel
     * @return {*}  {boolean}
     * @memberof IAppAccessUserService
     */
    getUserAccessAuth(viewModel: IParam): Promise<boolean>;
}
