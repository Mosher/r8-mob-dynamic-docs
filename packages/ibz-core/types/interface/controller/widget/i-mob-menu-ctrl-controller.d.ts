import { IPSAppMenu } from '@ibiz/dynamic-model-api';
import { IAppCtrlControllerBase } from './i-app-ctrl-controller-base';
export interface IMobMenuCtrlController extends IAppCtrlControllerBase {
    /**
     * 移动端菜单部件实例对象
     *
     * @type {IPSAppMenu}
     * @memberof IMobMenuCtrlController
     */
    controlInstance: IPSAppMenu;
}
