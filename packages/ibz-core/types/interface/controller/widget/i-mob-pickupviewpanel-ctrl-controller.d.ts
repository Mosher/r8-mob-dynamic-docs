import { IParam } from '../../common';
import { IPSDEPickupViewPanel } from '@ibiz/dynamic-model-api';
import { IAppCtrlControllerBase } from './i-app-ctrl-controller-base';
export interface IMobPickUpViewPanelCtrlController extends IAppCtrlControllerBase {
    /**
     * @description 选择面板部件实例对象
     * @type {IPSDEPickupViewPanel}
     * @memberof IMobPickUpViewPanelCtrlController
     */
    controlInstance: IPSDEPickupViewPanel;
    /**
     * @description 是否多选
     * @type {boolean}
     * @memberof IMobPickUpViewPanelCtrlController
     */
    isMultiple: boolean;
    /**
     * @description 处理选择视图事件
     * @param {string} action 选择视图行为
     * @param {IParam} data 数据
     * @memberof IMobPickUpViewPanelCtrlController
     */
    handlePickUPViewEvent(action: string, data: IParam): void;
}
