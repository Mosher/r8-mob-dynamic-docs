import { IPSDEToolbar, IPSDEToolbarItem } from '@ibiz/dynamic-model-api';
import { IAppCtrlControllerBase } from './i-app-ctrl-controller-base';
export interface IMobToolbarCtrlController extends IAppCtrlControllerBase {
    /**
     * @description 部件模型实例对象
     * @type {IPSDEToolbar}
     * @memberof IMobToolbarCtrlController
     */
    controlInstance: IPSDEToolbar;
    /**
     * @description 工具栏模型
     * @type {IPSDEToolbarItem[]}
     * @memberof IMobToolbarCtrlController
     */
    toolbarModels: IPSDEToolbarItem[];
}
