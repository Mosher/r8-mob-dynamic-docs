import { IPSDECalendar, IPSControlNavigatable, IPSDETree, IPSDEChart, IPSSysMap } from '@ibiz/dynamic-model-api';
import { IParam } from '../../../interface';
import { IAppCtrlControllerBase } from './i-app-ctrl-controller-base';
/**
 * @description 移动端导航部件控制器基类
 * @export
 * @interface IAppExpBarCtrlController
 * @extends {IAppCtrlControllerBase}
 */
export interface IAppExpBarCtrlController extends IAppCtrlControllerBase {
    /**
     * @description 多数据部件
     * @type {(IPSDECalendar | IPSControlNavigatable | IPSDETree)}
     * @memberof IMobListExpBarCtrlController
     */
    xDataControl: IPSDECalendar | IPSControlNavigatable | IPSDETree | IPSDEChart | IPSSysMap;
    /**
     * @description 多数据部件名称
     * @type {string}
     * @memberof IMobListExpBarCtrlController
     */
    xDataControlName: string;
    /**
     * @description 导航视图名称
     * @type {*}
     * @memberof IMobListExpBarCtrlController
     */
    navView: any;
    /**
     * @description 导航参数
     * @type {IParam}
     * @memberof IMobListExpBarCtrlController
     */
    navParam: IParam;
    /**
     * @description 导航应用上下文
     * @type {IParam}
     * @memberof IMobListExpBarCtrlController
     */
    navigateContext: IParam;
    /**
     * @description 导航视图参数
     * @type {IParam}
     * @memberof IMobListExpBarCtrlController
     */
    navigateParams: IParam;
    /**
     * @description 导航过滤项
     * @type {string}
     * @memberof IMobListExpBarCtrlController
     */
    navFilter: string;
    /**
     * @description 导航关系
     * @type {string}
     * @memberof IMobListExpBarCtrlController
     */
    navPSDer: string;
    /**
     * @description 选中数据
     * @type {IParam}
     * @memberof IMobListExpBarCtrlController
     */
    selection: IParam;
    /**
     * @description 工具栏模型数据
     * @type {IParam}
     * @memberof IMobListExpBarCtrlController
     */
    toolbarModels: IParam;
}
