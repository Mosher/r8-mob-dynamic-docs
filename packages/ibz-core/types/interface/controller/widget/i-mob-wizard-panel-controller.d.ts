import { IPSDEWizardPanel } from '@ibiz/dynamic-model-api';
import { Subject } from 'rxjs';
import { IParam, IViewStateParam } from '../../../interface';
import { IAppCtrlControllerBase } from './i-app-ctrl-controller-base';
/**
 * 移动端向导面板部件接口
 *
 * @export
 * @class IMobWizardPanelController
 */
export interface IMobWizardPanelController extends IAppCtrlControllerBase {
    /**
     * 移动端向导面板部件实例
     *
     * @protected
     * @type {IPSDEWizardPanel}
     * @memberof IMobWizardPanelController
     */
    controlInstance: IPSDEWizardPanel;
    /**
     * @description 向导步骤集合
     * @type {any[]}
     * @memberof IMobWizardPanelController
     */
    steps: any[];
    /**
     * @description 当前激活步骤表单
     * @type {string}
     * @memberof IMobWizardPanelController
     */
    activeForm: string;
    /**
     * @description 向导表单集合
     * @type {any[]}
     * @memberof IMobWizardPanelController
     */
    wizardForms: any[];
    /**
     * @description 步骤行为集合
     * @type {IParam}
     * @memberof IMobWizardPanelController
     */
    stepActions: IParam;
    /**
     * @description 向导面板全局通讯对象
     * @type {Subject<IViewStateParam>}
     * @memberof IMobWizardPanelController
     */
    wizardState: Subject<IViewStateParam>;
    /**
     * @description 处理上一步
     * @memberof IMobWizardPanelController
     */
    handlePrevious(): void;
    /**
     * @description 处理下一步
     * @memberof IMobWizardPanelController
     */
    handleNext(): void;
    /**
     * @description 处理完成
     * @memberof IMobWizardPanelController
     */
    handleFinish(): void;
}
