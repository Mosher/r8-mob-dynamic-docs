import { IPSDECalendar } from '@ibiz/dynamic-model-api';
import { IAppMDCtrlController } from './i-app-md-ctrl-controller';
import { ICtrlActionResult } from '../../params';
import { IParam } from '../../common';
export interface IMobCalendarCtrlController extends IAppMDCtrlController {
    /**
     * @description 移动端日历部件实例对象
     * @type {IPSDECalendar}
     * @memberof IMobCalendarCtrlController
     */
    controlInstance: IPSDECalendar;
    /**
     * @description 开始时间
     * @type {string}
     * @memberof IMobCalendarCtrlController
     */
    start: string;
    /**
     * @description 结束时间
     * @type {string}
     * @memberof IMobCalendarCtrlController
     */
    end: string;
    /**
     * @description 标记数据
     * @type {IParam[]}
     * @memberof IMobCalendarCtrlController
     */
    sign: IParam[];
    /**
     * @description 日历项集对象
     * @type {IParam}
     * @memberof IMobCalendarCtrlController
     */
    calendarItems: IParam;
    /**
     * @description 是否展示多选
     * @type {Boolean}
     * @memberof IMobCalendarCtrlController
     */
    isChoose: boolean;
    /**
     * @description 激活日历项
     * @type {String}
     * @memberof IMobCalendarCtrlController
     */
    activeItem: String;
    /**
     * @description 日历样式
     * @type {string}
     * @memberof IMobCalendarCtrlController
     */
    calendarStyle: string;
    /**
     * @description 图标
     * @type {Array<IParam>}
     * @memberof IMobCalendarCtrlController
     */
    illustration: IParam[];
    /**
     * @description 日历显示状态
     * @type {Boolean}
     * @memberof IMobCalendarCtrlController
     */
    show: Boolean;
    /**
     * @description 当前时间
     * @type {*}
     * @memberof IMobCalendarCtrlController
     */
    curTime: IParam;
    /**
     * @description 加载数据
     * @param {IParam} [opts] 行为参数
     * @param {IParam} [args] 额外行为参数
     * @param {boolean} [isSetEvent] 是否设置事程
     * @param {boolean} [loadding] 是否显示loadding效果
     * @return {*}  {Promise<ICtrlActionResult>}
     * @memberof IMobCalendarCtrlController
     */
    load(opts?: IParam, args?: IParam, isSetEvent?: boolean, loadding?: boolean): Promise<ICtrlActionResult>;
    /**
     * @description 删除数据
     * @param {IParam} [opts] 行为参数
     * @param {IParam} [args] 额外行为参数
     * @param {boolean} [showInfo] 是否显示提示信息
     * @param {boolean} [loadding] 是否显示loading效果
     * @return {*}  {Promise<ICtrlActionResult>}
     * @memberof IMobCalendarCtrlController
     */
    remove(opts?: IParam, args?: IParam, showInfo?: boolean, loadding?: boolean): Promise<ICtrlActionResult>;
    /**
     * @description 格式化时间（根据日历样式格式化开始时间和结束时间）
     * @param {*} curtime 当前时间
     * @param {IParam} [opts] 行为参数
     * @param {IParam} [args] 额外行为参数
     * @param {boolean} [isSetEvent] 是否设置事程
     * @param {boolean} [loadding] 是否显示loading效果
     * @return {*}  {Promise<ICtrlActionResult>}
     * @memberof IMobCalendarCtrlController
     */
    formatDate(curtime: IParam, opts?: IParam, args?: IParam, isSetEvent?: boolean, loadding?: boolean): Promise<ICtrlActionResult>;
    /**
     * @description 激活日历项改变
     * @param {string} curActiveItem 当前激活项
     * @memberof IMobCalendarCtrlController
     */
    activeItemChange(curActiveItem: string): void;
    /**
     * @description 打开视图
     * @param {IParam} $event 事件信息
     * @return {*}  {Promise<any>}
     * @memberof IMobCalendarCtrlController
     */
    openView($event: IParam): Promise<any>;
}
