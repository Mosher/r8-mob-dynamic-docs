import { IParam } from '../../common';
import { IAppCtrlControllerBase } from './i-app-ctrl-controller-base';
export interface IAppMDCtrlController extends IAppCtrlControllerBase {
    /**
     * @description 部件行为--fetch
     * @type {string}
     * @memberof IAppMDCtrlController
     */
    fetchAction?: string;
    /**
     * @description 多数据部件数据集合
     * @type {IParam[]}
     * @memberof IAppMDCtrlController
     */
    items: IParam[];
    /**
     * @description 是否支持分页
     * @type {boolean}
     * @memberof IAppMDCtrlController
     */
    isEnablePagingBar: boolean;
    /**
     * @description 排序参数对象
     * @type {IParam}
     * @memberof IAppMDCtrlController
     */
    sort: IParam;
    /**
     * @description 是否禁用排序
     * @type {boolean}
     * @memberof IAppMDCtrlController
     */
    isNoSort: boolean;
    /**
     * @description 排序方向
     * @type {string}
     * @memberof IAppMDCtrlController
     */
    minorSortDir: string;
    /**
     * @description 排序字段
     * @type {string}
     * @memberof IAppMDCtrlController
     */
    minorSortPSDEF: string;
    /**
     * @description 当前页
     * @type {number}
     * @memberof IAppMDCtrlController
     */
    curPage: number;
    /**
     * @description 分页条数
     * @type {number}
     * @memberof IAppMDCtrlController
     */
    limit: number;
    /**
     * @description 总条数
     * @type {number}
     * @memberof IAppMDCtrlController
     */
    totalRecord: number;
    /**
     * @description 是否默认选中第一条数据
     * @type {boolean}
     * @memberof IAppMDCtrlController
     */
    isSelectFirstDefault: boolean;
    /**
     * @description 是否开启分组
     * @type {boolean}
     * @memberof IAppMDCtrlController
     */
    isEnableGroup: boolean;
    /**
     * @description 分组集合
     * @type {IParam[]}
     * @memberof IAppMDCtrlController
     */
    groupDetail: IParam[];
    /**
     * @description 分组模式
     * @type {('AUTO' | 'CODELIST' | null)}
     * @memberof IAppMDCtrlController
     */
    groupMode: 'AUTO' | 'CODELIST' | null;
    /**
     * @description 分组属性
     * @type {string}
     * @memberof IAppMDCtrlController
     */
    groupField: string;
    /**
     * @description 分组数据
     * @type {IParam[]}
     * @memberof IAppMDCtrlController
     */
    groupData: IParam[];
    /**
     * @description 是否多选
     * @type {boolean}
     * @memberof IAppMDCtrlController
     */
    isMultiple: boolean;
    /**
     * @description 选中数据集合
     * @type {IParam[]}
     * @memberof IAppMDCtrlController
     */
    selections: IParam[];
    /**
     * @description 数据映射
     * @type {Map<string, IParam>}
     * @memberof IAppMDCtrlController
     */
    dataMap: Map<string, IParam>;
    /**
     * @description 选中数据改变
     * @param {any[]} selections 选中数组
     * @memberof IAppMDCtrlController
     */
    selectionChange(selections: any[]): void;
    /**
     * @description 项选中
     * @param {IParam} item 项数据
     * @param {MouseEvent} [event] 事件源
     * @memberof IAppMDCtrlController
     */
    onRowSelect(item: IParam, event?: MouseEvent): void;
    /**
     * @description 处理上下文菜单事件
     * @param {string} action 行为
     * @param {IParam} data 数据
     * @memberof IAppMDCtrlController
     */
    handleContextMenuEvent(action: string, data: IParam): void;
    /**
     * @description 处理上下文菜单点击
     * @param {string} name 上下文菜单部件名称
     * @param {IParam} data 行为数据
     * @param {MouseEvent} event 事件源
     * @param {IParam} detail 行为模型数据
     * @memberof IAppMDCtrlController
     */
    handleContextMenuClick(name: string, data: IParam, event: MouseEvent, detail: IParam): void;
}
