import { IPSDEMobMDCtrl } from '@ibiz/dynamic-model-api';
import { ICtrlActionResult } from '../../params';
import { IParam } from '../../common';
import { IAppMDCtrlController } from './i-app-md-ctrl-controller';
export interface IMobMDCtrlController extends IAppMDCtrlController {
    /**
     * @description 移动端多数据部件实例对象
     * @type {IPSDEMobMDCtrl}
     * @memberof IMobMDCtrlController
     */
    controlInstance: IPSDEMobMDCtrl;
    /**
     * @description 是否还需要加载更多
     * @return {*}  {boolean}
     * @memberof IMobMDCtrlController
     */
    get needLoadMore(): boolean;
    /**
     * @description 删除
     * @param {IParam[]} datas 数据集
     * @param {*} args 行为参数
     * @param {boolean} showInfo 是否显示提示信息
     * @param {boolean} loadding 是否显示loadding效果
     * @return {*}  {Promise<ICtrlActionResult>}
     * @memberof IMobMDCtrlController
     */
    remove(datas: IParam[], args: any, showInfo: boolean, loadding: boolean): Promise<ICtrlActionResult>;
    /**
     * @description 加载数据
     * @param {IParam} opts 行为参数
     * @param {IParam} args 额外参数
     * @param {boolean} showInfo 是否显示提示信息
     * @param {boolean} loadding 是否显示loadding效果
     * @return {*}  {Promise<ICtrlActionResult>}
     * @memberof IMobMDCtrlController
     */
    load(opts: IParam, args: IParam, showInfo: boolean, loadding: boolean): Promise<ICtrlActionResult>;
    /**
     * @description 加载更多
     * @return {*}  {Promise<ICtrlActionResult>}
     * @memberof IMobMDCtrlController
     */
    loadBottom(): Promise<ICtrlActionResult>;
}
