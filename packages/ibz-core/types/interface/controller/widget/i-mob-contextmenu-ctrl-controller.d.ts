import { IPSDEContextMenu } from '@ibiz/dynamic-model-api';
import { IParam } from '../../common';
import { IAppCtrlControllerBase } from './i-app-ctrl-controller-base';
/**
 * 移动端上下文菜单控制器接口
 *
 * @exports
 * @interface IMobContextMenuCtrlController
 * @extends IAppCtrlControllerBase
 */
export interface IMobContextMenuCtrlController extends IAppCtrlControllerBase {
    /**
     * @description 上下文菜单部件模型实例
     * @type {IPSDEContextMenu}
     * @memberof IMobContextMenuCtrlController
     */
    controlInstance: IPSDEContextMenu;
    /**
     * @description 上下文菜单行为模型
     * @type {IParam}
     * @memberof IMobContextMenuCtrlController
     */
    contextMenuActionModel: IParam;
    /**
     * @description 触发上下文菜单打开的事件源
     * @type {MouseEvent}
     * @memberof IMobContextMenuCtrlController
     */
    mouseEvent: IParam;
}
