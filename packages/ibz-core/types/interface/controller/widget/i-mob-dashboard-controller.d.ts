import { IPSDashboard } from '@ibiz/dynamic-model-api';
import { ICtrlActionResult, IParam } from '../../../interface';
import { IAppCtrlControllerBase } from './i-app-ctrl-controller-base';
/**
 * 移动端数据看板部件控制器接口
 *
 * @exports
 * @interface IMobDashboardController
 * @extends IAppCtrlControllerBase
 */
export interface IMobDashboardController extends IAppCtrlControllerBase {
    /**
     * @description 数据看板部件
     * @type {IPSDashboard}
     */
    controlInstance: IPSDashboard;
    /**
     * @description 支持看板定制
     * @type {boolean}
     */
    enableCustomized: boolean;
    /**
     * @description 是否具有自定义面板
     * @type {boolean}
     */
    hasCustomized: boolean;
    /**
     * @description 自定义看板模型数据
     * @type {IParam[]}
     */
    customDashboardModelData: IParam[];
    /**
     * @description 处理私人定制按钮
     * @param {IParam} view 面板设计视图对象
     * @param {IParam} event 源对象
     */
    handleCustom(view: IParam, event: any): Promise<ICtrlActionResult>;
}
