import { IPSDETree } from '@ibiz/dynamic-model-api';
import { ICtrlActionResult } from '../../params';
import { IParam } from '../../common';
import { IAppMDCtrlController } from './i-app-md-ctrl-controller';
/**
 * 移动端树部件控制器接口
 *
 * @exports
 * @interface IMobTreeCtrlController
 * @extends IAppMDCtrlController
 */
export interface IMobTreeCtrlController extends IAppMDCtrlController {
    /**
     * @description 移动端树视图部件实例对象
     * @type {IPSDETree}
     * @memberof IMobTreeCtrlController
     */
    controlInstance: IPSDETree;
    /**
     * @description 加载数据
     * @param {IParam} opts 行为参数
     * @param {IParam} args 额外行为参数
     * @param {boolean} showInfo 提示信息
     * @param {boolean} loadding loadding效果
     * @return {*}  {Promise<ICtrlActionResult>}
     * @memberof IMobTreeCtrlController
     */
    load(opts: IParam, args: IParam, showInfo: boolean, loadding: boolean): Promise<ICtrlActionResult>;
    /**
     * @description 全部节点刷新
     * @return {*}  {Promise<ICtrlActionResult>}
     * @memberof IMobTreeCtrlController
     */
    refreshAll(): Promise<ICtrlActionResult>;
    /**
     * @description 刷新父节点
     * @return {*}  {Promise<ICtrlActionResult>}
     * @memberof IMobTreeCtrlController
     */
    refreshParent(): Promise<ICtrlActionResult>;
    /**
     * @description 多选选中
     * @param {string} key 选中数据主键
     * @param {boolean} checked 选中状态
     * @memberof IMobTreeCtrlController
     */
    handleTreeNodeChecked(item: IParam, event: any): void;
    /**
     * @description 单选选中变化
     * @param {*} item 选中数据
     * @param {MouseEvent} event 事件源
     * @memberof IMobTreeCtrlController
     */
    handleTreeNodeSelect(item: any, event: MouseEvent): void;
    /**
     * @description 加载子节点数据
     * @param {IParam} item 父节点
     * @param {MouseEvent} event 事件源
     * @memberof IMobTreeCtrlController
     */
    loadChildNodeData(item: IParam, event: MouseEvent): void;
    /**
     * @description 获取计数器数据
     * @param {IParam} node 节点数据
     * @return {*}  {(string | number | null)}
     * @memberof IMobTreeCtrlController
     */
    getCounterData(node: IParam): string | number | null;
}
