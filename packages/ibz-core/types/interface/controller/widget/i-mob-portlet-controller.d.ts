import { IPSDBPortletPart } from '@ibiz/dynamic-model-api';
import { IParam } from '../../../interface';
import { IAppCtrlControllerBase } from './i-app-ctrl-controller-base';
/**
 * 移动端门户部件控制器接口
 *
 * @exports
 * @interface IMobPortletController
 * @extends IAppCtrlControllerBase
 */
export interface IMobPortletController extends IAppCtrlControllerBase {
    /**
     * @description 门户部件实例对象
     * @type {IPSDBPortletPart}
     * @memberof IMobPortletController
     */
    controlInstance: IPSDBPortletPart;
    /**
     * @description 操作栏模型数据
     * @type {IParam[]}
     * @memberof IMobPortletController
     */
    actionBarModel: IParam[];
    /**
     * @description 操作栏按钮点击
     * @param {string} tag 标识
     * @param {*} event 事件源
     * @memberof IMobPortletController
     */
    actionBarItemClick(tag: string, event: any): void;
    /**
     * @description 处理视图事件
     * @param {string} viewname 视图名
     * @param {string} action 行为
     * @param {*} data 参数
     * @memberof IMobPortletController
     */
    handleViewEvent(viewname: string, action: string, data: any): void;
}
