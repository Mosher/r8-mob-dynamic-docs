import { IPSMapExpBar } from '@ibiz/dynamic-model-api';
import { IAppExpBarCtrlController } from './i-app-exp-bar-ctrl-controller';
/**
 * 移动端地图导航部件接口
 *
 * @export
 * @class IMobMapExpBarController
 */
export interface IMobMapExpBarController extends IAppExpBarCtrlController {
    /**
     * 移动端地图导航部件实例
     *
     * @protected
     * @type {IPSDEMobMapExpBar}
     * @memberof IMobMapExpBarController
     */
    controlInstance: IPSMapExpBar;
}
