import { IPSChartExpBar } from '@ibiz/dynamic-model-api';
import { IAppExpBarCtrlController } from '../../../interface';
/**
 * 移动端图表导航栏接口
 *
 * @export
 * @class IMobChartExpBarController
 */
export interface IMobChartExpBarController extends IAppExpBarCtrlController {
    /**
     * 移动端图表导航栏实例
     *
     * @protected
     * @type {IPSDEMobChartExpBar}
     * @memberof IMobChartExpBarController
     */
    controlInstance: IPSChartExpBar;
}
