import { IParam } from '../../common';
import { ICtrlActionResult } from '../../params';
import { IPSDEChart } from '@ibiz/dynamic-model-api';
import { IAppMDCtrlController } from './i-app-md-ctrl-controller';
export interface IMobChartCtrlController extends IAppMDCtrlController {
    /**
     * @description 移动端图表部件实例对象
     * @type {IPSDEChart}
     * @memberof IMobChartCtrlController
     */
    controlInstance: IPSDEChart;
    /**
     * @description 图表div绑定的id
     * @type {string}
     * @memberof IMobChartCtrlController
     */
    chartId: string;
    /**
     * @description 是否无数据
     * @type {boolean}
     * @memberof IMobChartCtrlController
     */
    isNoData: boolean;
    /**
     * @description echarts图表对象
     * @type {*}
     * @memberof IMobChartCtrlController
     */
    myChart: any;
    /**
     * @description 序列模型
     * @type {IParam}
     * @memberof IMobChartCtrlController
     */
    seriesModel: IParam;
    /**
     * @description 图表自定义参数集合
     * @type {IParam}
     * @memberof IMobChartCtrlController
     */
    chartUserParams: IParam;
    /**
     * @description 图表基础动态模型
     * @type {IParam}
     * @memberof IMobChartCtrlController
     */
    chartBaseOPtion: IParam;
    /**
     * @description 图表最终绘制参数
     * @type {IParam}
     * @memberof IMobChartCtrlController
     */
    chartRenderOption: IParam;
    /**
     * @description 初始化图表所需参数
     * @type {IParam}
     * @memberof IMobChartCtrlController
     */
    chartOption: IParam;
    /**
     * @description 加载数据
     * @param {IParam} [opts] 行为参数
     * @param {*} [args] 额外参数
     * @param {boolean} [showInfo] 是否显示提示信息
     * @param {boolean} [loadding] 是否显示loadding效果
     * @return {*}  {Promise<ICtrlActionResult>}
     * @memberof IMobChartCtrlController
     */
    load(opts?: IParam, args?: any, showInfo?: boolean, loadding?: boolean): Promise<ICtrlActionResult>;
}
