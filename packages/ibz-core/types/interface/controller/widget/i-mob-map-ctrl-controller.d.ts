import { IPSSysMap } from '@ibiz/dynamic-model-api';
import { IAppMDCtrlController } from './i-app-md-ctrl-controller';
export interface IMobMapCtrlController extends IAppMDCtrlController {
    /**
     * 移动端地图部件实例对象
     *
     * @type {IPSDEMobMDCtrl}
     * @memberof IMobMapCtrlController
     */
    controlInstance: IPSSysMap;
    /**
     * 地图数据项模型
     *
     * @type {}
     * @memberof IMobMapCtrlController
     */
    mapItems: any;
}
