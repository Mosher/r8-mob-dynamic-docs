import { IPSAppDEMultiDataView } from '@ibiz/dynamic-model-api';
import { IParam } from '../../common';
import { IAppDEViewController } from './i-app-de-view-controller';
/**
 * @description 多数据视图接口
 * @export
 * @interface IAppDEMultiDataViewController
 * @extends {IAppDEViewController}
 */
export interface IAppDEMultiDataViewController extends IAppDEViewController {
    /**
     * @description 多数据视图基类实例对象
     * @type {IPSAppDEMultiDataView}
     * @memberof IAppDEMultiDataViewController
     */
    viewInstance: IPSAppDEMultiDataView;
    /**
     * @description 是否多选
     * @type {boolean}
     * @memberof IAppDEMultiDataViewController
     */
    isMultiple: boolean;
    /**
     * @description 是否开启快速分组
     * @type {boolean}
     * @memberof IAppDEMultiDataViewController
     */
    isEnableQuickGroup: boolean;
    /**
     * @description 快速分组模型数据
     * @type {IParam[]}
     * @memberof IAppDEMultiDataViewController
     */
    quickGroupModel: IParam[];
    /**
     * @description 是否展开搜索表单
     * @type {boolean}
     * @memberof IAppDEMultiDataViewController
     */
    isExpandSearchForm: boolean;
    /**
     * @description 快速搜索
     * @param {string} query 快速搜索值
     * @memberof IAppDEMultiDataViewController
     */
    quickSearch(query: string): void;
    /**
     * @description 快速分组值变化
     * @param {IParam} event 快速分组激活值
     * @memberof IAppDEMultiDataViewController
     */
    quickGroupValueChange(event: IParam): void;
}
