import { IPSAppDEMobMapExplorerView } from '@ibiz/dynamic-model-api';
import { IAppDEExpViewController } from './i-app-de-exp-view-controller';
/**
 * 移动端地图导航视图接口
 *
 * @export
 * @class IMobCustomViewController
 */
export interface IMobMapExpViewController extends IAppDEExpViewController {
    /**
     * 移动端地图导航视图实例
     *
     * @protected
     * @type {IPSAppDEMobMapExpView}
     * @memberof IMobMapExpViewController
     */
    viewInstance: IPSAppDEMobMapExplorerView;
}
