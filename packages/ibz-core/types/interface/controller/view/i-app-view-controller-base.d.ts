import { Subject } from 'rxjs';
import { IPSAppView } from '@ibiz/dynamic-model-api';
import { IUIDataParam, IUIEnvironmentParam, IViewStateParam } from '../../params';
import { IParam } from '../../common';
import { IAppViewHooks } from '../../hooks';
import { IViewMessageService } from '../../../interface';
/**
 * 视图接口
 *
 * @export
 * @class IAppViewControllerBase
 */
export interface IAppViewControllerBase {
    /**
     * 视图打开模式
     *
     * @type {string}
     * @memberof IAppViewControllerBase
     */
    viewShowMode: 'ROUTE' | 'MODEL' | 'EMBEDDED';
    /**
     * 视图是否加载
     *
     * @type {boolean}
     * @memberof IAppViewControllerBase
     */
    viewIsLoaded: boolean;
    /**
     * 是否显示视图标题栏
     *
     * @type {boolean}
     * @memberof IAppViewControllerBase
     */
    isShowCaptionBar: boolean;
    /**
     * 视图默认加载
     *
     * @type {boolean}
     * @memberof IAppViewControllerBase
     */
    isLoadDefault: boolean;
    /**
     * 视图钩子对象
     *
     * @type {AppViewHooks}
     * @memberof IAppViewControllerBase
     */
    hooks: IAppViewHooks;
    /**
     * 应用上下文
     *
     * @type {IParam}
     * @memberof IAppViewControllerBase
     */
    context: IParam;
    /**
     * 视图参数
     *
     * @type {IParam}
     * @memberof IAppViewControllerBase
     */
    viewParam: IParam;
    /**
     * 视图导航数据
     *
     * @type {Array<IParam>}
     * @memberof IAppViewControllerBase
     */
    navDatas: Array<IParam>;
    /**
     * 视图操作参数
     *
     * @type {*}
     * @memberof IAppViewControllerBase
     */
    viewCtx: IParam;
    /**
     * 视图传递对象
     *
     * @type {Subject}
     * @memberof IAppViewControllerBase
     */
    viewState: Subject<IViewStateParam>;
    /**
     * 视图模型对象
     *
     * @type {IPSAppView}
     * @memberof IAppViewControllerBase
     */
    viewInstance: IPSAppView;
    /**
     * 模型服务对象
     *
     * @type {*}
     * @memberof IAppViewControllerBase
     */
    modelService: IParam;
    /**
     * 视图消息服务
     *
     * @type {*}
     * @memberof IAppViewControllerBase
     */
    viewMessageService: IViewMessageService;
    /**
     * 计数器服务对象集合
     *
     * @type {Array<*>}
     * @memberof IAppViewControllerBase
     */
    counterServiceArray: IParam[];
    /**
     * 界面触发逻辑Map
     *
     * @memberof IAppViewControllerBase
     */
    viewTriggerLogicMap: Map<string, IParam>;
    /**
     * 视图部件引用控制器集合
     *
     * @memberof IAppViewControllerBase
     */
    ctrlRefsMap: Map<string, IParam>;
    /**
     * 获取顶层视图
     *
     * @memberof IAppViewControllerBase
     */
    getTopView(): IParam;
    /**
     * 获取父级视图
     *
     * @memberof IAppViewControllerBase
     */
    getParentView(): IParam;
    /**
     * 通过名称获取指定部件
     *
     * @memberof IAppViewControllerBase
     */
    getCtrlByName(name: string): IParam | undefined;
    /**
     * 获取视图激活数据
     *
     * @public
     * @memberof ViewComponentBase
     */
    getData(): IParam | null;
    /**
     * 获取视图激活数据集
     *
     * @public
     * @memberof ViewComponentBase
     */
    getDatas(): Array<IParam> | null;
    /**
     * 获取逻辑UI数据
     *
     * @returns {IUIDataParam}
     * @memberof IAppViewControllerBase
     */
    getUIDataParam(data: Array<IParam> | null): IUIDataParam;
    /**
     * 获取逻辑环境数据
     *
     * @returns {IUIEnvironmentParam}
     * @memberof IAppViewControllerBase
     */
    getUIEnvironmentParam(): IUIEnvironmentParam;
    /**
     * 视图刷新
     *
     * @memberof IAppViewControllerBase
     */
    refresh(arg?: IParam): void;
    /**
     *  关闭视图
     *
     * @memberof IAppViewControllerBase
     */
    closeView(args?: IParam): void;
    /**
     *  视图初始化
     *
     * @memberof IAppViewControllerBase
     */
    viewInit(): Promise<boolean>;
    /**
     *  视图销毁
     *
     * @memberof IAppViewControllerBase
     */
    viewDestroy(): void;
    /**
     *  处理部件事件
     *
     * @memberof IAppViewControllerBase
     */
    handleCtrlEvent(controlname: string, action: string, data: any): void;
    /**
     * 处理自定义视图导航数据
     *
     * @memberof IAppViewControllerBase
     */
    handleCustomViewData(navContext: IParam, navViewParam: IParam): void;
    /**
     * 处理其他数据(多实例)
     *
     * @memberof IAppViewControllerBase
     */
    handleOtherViewData(navContext: IParam, navViewParam: IParam): void;
}
