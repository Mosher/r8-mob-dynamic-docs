import { IPSAppDEMobCustomView } from '@ibiz/dynamic-model-api';
import { IAppDEViewController } from './i-app-de-view-controller';
/**
 * 自定义视图接口
 *
 * @export
 * @class IMobCustomViewController
 */
export interface IMobCustomViewController extends IAppDEViewController {
    /**
     * 视图实例
     *
     * @protected
     * @type {IPSAppDEMobCustomView}
     * @memberof MobCustomViewController
     */
    viewInstance: IPSAppDEMobCustomView;
}
