import { IAppDEMultiDataViewController } from './i-app-de-multi-data-view-controller';
/**
 * 移动端实体图表视图控制器接口
 *
 * @exports
 * @interface IMobChartViewController
 * @extends IAppDEMultiDataViewController
 */
export interface IMobChartViewController extends IAppDEMultiDataViewController {
    /**
     * 视图实例
     *
     * @protected
     * @type {IPSAppDEMobChartView}
     * @memberof IMobChartViewController
     */
    viewInstance: any;
}
