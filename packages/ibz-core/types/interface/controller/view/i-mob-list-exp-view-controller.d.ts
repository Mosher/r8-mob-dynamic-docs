import { IPSAppDEMobListExplorerView } from '@ibiz/dynamic-model-api';
import { IAppDEExpViewController } from './i-app-de-exp-view-controller';
/**
 * 移动端列表导航视图接口
 *
 * @export
 * @class IMobCustomViewController
 */
export interface IMobListExpViewController extends IAppDEExpViewController {
    /**
     * 移动端列表导航视图实例
     *
     * @protected
     * @type {IPSAppDEMobListExplorerView}
     * @memberof IMobListExpViewController
     */
    viewInstance: IPSAppDEMobListExplorerView;
}
