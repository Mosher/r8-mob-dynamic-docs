import { IPSAppDEMobEditView } from '@ibiz/dynamic-model-api';
import { IAppDEViewController } from './i-app-de-view-controller';
/**
 * 移动端编辑视图接口
 *
 * @export
 * @class IMobCustomViewController
 */
export interface IMobEditViewController extends IAppDEViewController {
    /**
     * 视图实例
     *
     * @protected
     * @type {IPSAppDEMobEditView}
     * @memberof IMobEditViewController
     */
    viewInstance: IPSAppDEMobEditView;
}
