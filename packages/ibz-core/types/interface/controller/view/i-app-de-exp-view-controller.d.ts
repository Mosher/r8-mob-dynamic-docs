import { IPSAppDEExplorerView } from '@ibiz/dynamic-model-api';
import { IAppDEViewController } from './i-app-de-view-controller';
/**
 * @description 移动端实体导航视图控制器接口
 * @export
 * @interface IAppDEExpViewController
 * @extends {IAppDEViewController}
 */
export interface IAppDEExpViewController extends IAppDEViewController {
    /**
     * @description 实体导航视图视图实例对象
     * @type {IPSAppDEExplorerView}
     * @memberof IAppDEExpViewController
     */
    viewInstance: IPSAppDEExplorerView;
}
