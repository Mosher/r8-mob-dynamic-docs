import { IPSAppDEMobWFDynaActionView, IPSDEForm } from '@ibiz/dynamic-model-api';
import { IAppDEViewController } from './i-app-de-view-controller';
/**
 * 移动端动态工作流操作视图接口
 *
 * @export
 * @class IMobCustomViewController
 */
export interface IMobWFDynaActionViewController extends IAppDEViewController {
    /**
     * @description 移动端动态工作流操作视图实例
     * @type {IPSAppDEMobWFDynaActionView}
     * @memberof IMobWFDynaActionViewController
     */
    viewInstance: IPSAppDEMobWFDynaActionView;
    /**
     * @description 编辑表单实例对象
     * @type {IPSDEForm}
     * @memberof IMobWFDynaActionViewController
     */
    editFormInstance: IPSDEForm;
    /**
     * @description 处理确认
     * @memberof IMobWFDynaActionViewController
     */
    handleOk(): void;
    /**
     * @description 处理取消
     * @memberof IMobWFDynaActionViewController
     */
    handleCancel(): void;
}
