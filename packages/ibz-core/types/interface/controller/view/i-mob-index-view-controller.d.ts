import { IPSAppIndexView } from '@ibiz/dynamic-model-api';
import { IAppViewControllerBase } from './i-app-view-controller-base';
/**
 * 应用首页视图接口
 *
 * @export
 * @class IndexViewController
 */
export interface IMobIndexViewController extends IAppViewControllerBase {
    /**
     * 视图实例
     *
     * @protected
     * @type {IPSAppIndexView}
     * @memberof IMobEditViewController
     */
    viewInstance: IPSAppIndexView;
}
