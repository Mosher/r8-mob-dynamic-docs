import { IPSAppDEMobHtmlView } from '@ibiz/dynamic-model-api';
import { IAppDEViewController } from './i-app-de-view-controller';
/**
 * 移动html视图接口
 *
 * @export
 * @class IMobHtmlViewController
 */
export interface IMobHtmlViewController extends IAppDEViewController {
    /**
     * 视图实例
     *
     * @protected
     * @type {IPSAppDEMobHtmlView}
     * @memberof IMobHtmlViewController
     */
    viewInstance: IPSAppDEMobHtmlView;
}
