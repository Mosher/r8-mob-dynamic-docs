import { IPSAppDEMobWizardView } from '@ibiz/dynamic-model-api';
import { IAppDEViewController } from './i-app-de-view-controller';
/**
 * 移动端向导视图接口
 *
 * @export
 * @class IMobCustomViewController
 */
export interface IMobWizardViewController extends IAppDEViewController {
    /**
     * 移动端向导视图实例
     *
     * @protected
     * @type {IPSAppDEMobWizardView}
     * @memberof IMobWizardViewController
     */
    viewInstance: IPSAppDEMobWizardView;
}
