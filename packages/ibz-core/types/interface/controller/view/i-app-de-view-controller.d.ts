import { IPSAppDEView } from '@ibiz/dynamic-model-api';
import { IUIService } from '../../../interface';
import { IParam } from '../../common';
import { IAppViewControllerBase } from './i-app-view-controller-base';
/**
 * 主数据视图接口
 *
 * @export
 * @class IAppDEViewController
 */
export interface IAppDEViewController extends IAppViewControllerBase {
    /**
     * 视图模型对象
     *
     * @type {IPSAppDEView}
     * @memberof IAppDEViewController
     */
    viewInstance: IPSAppDEView;
    /**
     * 数据部件名称
     *
     * @type {string}
     * @memberof IAppDEViewController
     */
    xDataControlName: string;
    /**
     * 实体服务对象
     *
     * @type {*}
     * @memberof IAppDEViewController
     */
    appDataService: IParam;
    /**
     * 实体UI服务对象
     *
     * @type {*}
     * @memberof IAppDEViewController
     */
    appUIService: IUIService;
}
