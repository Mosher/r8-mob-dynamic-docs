import { IMobPickUpViewController } from './i-mob-pickup-view-controller';
/**
 * 移动端多数据选择视图接口
 *
 * @export
 * @class IMobMPickUpViewController
 */
export declare type IMobMPickUpViewController = IMobPickUpViewController;
