import { IPSAppDEMobTabExplorerView } from '@ibiz/dynamic-model-api';
import { IAppDEViewController } from './i-app-de-view-controller';
/**
 * 移动端分页导航视图接口
 *
 * @export
 * @class IMobTabExpViewController
 */
export interface IMobTabExpViewController extends IAppDEViewController {
    /**
     * 视图实例
     *
     * @protected
     * @type {IPSAppDEMobTabExplorerView}
     * @memberof IMobTabExpViewController
     */
    viewInstance: IPSAppDEMobTabExplorerView;
}
