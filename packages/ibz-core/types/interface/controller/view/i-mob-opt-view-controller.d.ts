import { IPSAppDEMobEditView } from '@ibiz/dynamic-model-api';
import { IAppDEViewController } from './i-app-de-view-controller';
/**
 * 移动端选项操作视图接口
 *
 * @export
 * @class IMobOptViewController
 */
export interface IMobOptViewController extends IAppDEViewController {
    /**
     * 移动端选项操作视图实例
     *
     * @protected
     * @type {IPSAppDEMobOptView}
     * @memberof IMobOptViewController
     */
    viewInstance: IPSAppDEMobEditView;
    /**
     * 确定
     *
     * @memberof IMobOptViewController
     */
    ok(): void;
    /**
     * 取消
     *
     * @memberof IMobOptViewController
     */
    cancel(): void;
}
