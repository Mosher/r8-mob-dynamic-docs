import { IPSAppDEMobMDView } from '@ibiz/dynamic-model-api';
import { IMobMDViewController } from './i-mob-md-view-controller';
/**
 * 移动端选择多数据视图接口
 *
 * @export
 * @class IMobPickUpMDViewController
 */
export interface IMobPickUpMDViewController extends IMobMDViewController {
    /**
     * 视图实例
     *
     * @protected
     * @type {IPSAppDEMobMDView}
     * @memberof IMobPickUpMDViewController
     */
    viewInstance: IPSAppDEMobMDView;
}
