import { IPSEditor } from '@ibiz/dynamic-model-api';
import { Subject } from 'rxjs';
import { IParam } from '../../common';
import { IAppEditorHooks } from '../../hooks';
export interface IEditorControllerBase {
    /**
     * 编辑器模型
     *
     * @type {IPSEditor}
     * @memberof IEditorControllerBase
     */
    editorInstance: IPSEditor;
    /**
     * 父级项模型（表单项，表格项）
     *
     * @type {IParam}
     * @memberof IEditorControllerBase
     */
    parentItem: IParam;
    /**
     * 是否禁用
     *
     * @type {IPSEditor}
     * @memberof IEditorControllerBase
     */
    disabled: boolean;
    /**
     * 应用上下文
     *
     * @type {*}
     * @memberof IEditorControllerBase
     */
    context: IParam;
    /**
     * 视图参数
     *
     * @type {*}
     * @memberof IEditorControllerBase
     */
    viewParam: IParam;
    /**
     * 父项数据
     *
     * @type {*}
     * @memberof IEditorControllerBase
     */
    contextData: IParam;
    /**
     * 编辑器上下文通讯对象
     *
     * @type {*}
     * @memberof IEditorControllerBase
     */
    contextState: Subject<IParam>;
    /**
     * 编辑器值
     *
     * @type {*}
     * @memberof IEditorControllerBase
     */
    value: any;
    /**
     * 视图钩子对象
     *
     * @type {AppViewHooks}
     * @memberof IEditorControllerBase
     */
    hooks: IAppEditorHooks;
    /**
     * 模型数据是否加载完成
     *
     * @memberof IEditorControllerBase
     */
    editorIsLoaded: boolean;
    /**
     * 自定义样式的对象
     *
     * @type {*}
     * @memberof IEditorControllerBase
     */
    customStyle: IParam;
    /**
     * 设置自定义props
     *
     * @type {*}
     * @memberof IEditorControllerBase
     */
    customProps: IParam;
    /**
     * 初始化输入数据
     *
     * @public
     * @memberof IEditorControllerBase
     */
    initInputData(opts: IParam): void;
    /**
     * 编辑器初始化
     *
     * @memberof IEditorControllerBase
     */
    editorInit(): Promise<boolean>;
    /**
     * 改变编辑器的值
     *
     * @param {*} value
     * @memberof IEditorControllerBase
     */
    changeValue(value: any): void;
    /**
     * 编辑器销毁
     *
     * @public
     * @memberof IEditorControllerBase
     */
    editorDestroy(): void;
}
