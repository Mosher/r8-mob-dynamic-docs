import { IAppLayoutProps } from './i-app-layout-props';
/**
 * 移动端图表视图视图布局面板传入参数接口
 *
 * @export
 * @interface IAppMobChartViewLayoutProps
 * @extends IAppLayoutProps
 */
export declare type IAppMobChartViewLayoutProps = IAppLayoutProps;
