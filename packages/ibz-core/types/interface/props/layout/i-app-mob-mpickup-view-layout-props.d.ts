import { IAppMobPickUpViewLayoutProps } from './i-app-mob-pickup-view-layout-props';
/**
 * 移动端多数据选择视图视图布局面板输入参数接口
 *
 * @export
 * @interface IAppMobMPickUpViewLayoutProps
 */
export declare type IAppMobMPickUpViewLayoutProps = IAppMobPickUpViewLayoutProps;
