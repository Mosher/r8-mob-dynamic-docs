import { IAppLayoutProps } from './i-app-layout-props';
/**
 * 移动端实体树视图视图面板输入参数接口
 *
 * @export
 * @interface IAppMobTreeViewLayoutProps
 */
export declare type IAppMobTreeViewLayoutProps = IAppLayoutProps;
