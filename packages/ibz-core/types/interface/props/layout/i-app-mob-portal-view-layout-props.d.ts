import { IAppLayoutProps } from './i-app-layout-props';
/**
 * 移动端应用看板视图输入参数接口
 *
 *
 * @interface IAppMobPortalViewLayoutProps
 * @extends IAppLayoutProps
 */
export declare type IAppMobPortalViewLayoutProps = IAppLayoutProps;
