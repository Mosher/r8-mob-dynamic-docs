import { IAppLayoutProps } from './i-app-layout-props';
/**
 * 移动端自定义视图视图布局面板传入参数接口
 *
 * @export
 * @interface iAppMobCustomViewLayoutProps
 * @extends IAppLayoutProps
 */
export declare type IAppMobCustomViewLayoutProps = IAppLayoutProps;
