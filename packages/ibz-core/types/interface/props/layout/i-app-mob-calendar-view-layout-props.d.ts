import { IAppLayoutProps } from './i-app-layout-props';
/**
 * 移动端日历视图视图布局面板传入参数接口
 *
 * @export
 * @interface IAppMobCalendarViewLayoutProps
 * @extends IAppLayoutProps
 */
export declare type IAppMobCalendarViewLayoutProps = IAppLayoutProps;
