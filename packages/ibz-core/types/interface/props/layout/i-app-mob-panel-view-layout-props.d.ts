import { IAppLayoutProps } from './i-app-layout-props';
/**
 * 移动端面板视图布局面板传入参数接口
 *
 * @export
 * @interface IAppMobPanelViewLayoutProps
 * @extends IAppLayoutProps
 */
export declare type IAppMobPanelViewLayoutProps = IAppLayoutProps;
