import { IAppLayoutProps } from './i-app-layout-props';
/**
 * 移动端多表单编辑视图布局面板传入参数接口
 *
 * @export
 * @interface IAppMobMEditViewLayoutProps
 * @extends IAppLayoutProps
 */
export declare type IAppMobMEditViewLayoutProps = IAppLayoutProps;
