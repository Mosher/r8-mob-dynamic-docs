import { IAppDEViewProps } from './i-app-de-view-props';
/**
 * 多数据视图基类输入属性接口
 *
 * @export
 * @class IAppDEMultiDataViewProps
 * @extends IAppDEViewProps
 */
export interface IAppDEMultiDataViewProps extends IAppDEViewProps {
    /**
     * 是否为多选
     * @interface IAppViewProps
     */
    isMultiple: boolean;
}
