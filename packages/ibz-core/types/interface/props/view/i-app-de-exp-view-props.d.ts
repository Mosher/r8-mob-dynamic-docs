import { IAppDEViewProps } from './i-app-de-view-props';
/**
 * @description 移动端实体导航视图输入参数接口
 * @export
 * @interface IAppDEExpViewProps
 * @extends {IAppDEViewProps}
 */
export interface IAppDEExpViewProps extends IAppDEViewProps {
}
