import { IAppMobTreeViewProps } from './i-app-mob-tree-view-props';
/**
 * 移动端选择树视图输入参数接口
 *
 * @export
 * @interface IAppMobPickupTreeViewProps
 */
export declare type IAppMobPickupTreeViewProps = IAppMobTreeViewProps;
