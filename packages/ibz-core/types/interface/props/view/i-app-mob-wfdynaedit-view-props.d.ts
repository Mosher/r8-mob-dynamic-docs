import { IAppDEViewProps } from './i-app-de-view-props';
/**
 * 移动端动态工作流编辑视图输入属性接口
 *
 * @export
 * @interface IAppMobWFDynaEditViewProps
 */
export interface IAppMobWFDynaEditViewProps extends IAppDEViewProps {
}
