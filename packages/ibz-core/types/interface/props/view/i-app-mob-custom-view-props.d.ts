import { IAppDEViewProps } from './i-app-de-view-props';
/**
 * 移动端自定义视图视图输入属性接口
 *
 * @export
 * @interface IAppMobCustomViewProps
 */
export declare type IAppMobCustomViewProps = IAppDEViewProps;
