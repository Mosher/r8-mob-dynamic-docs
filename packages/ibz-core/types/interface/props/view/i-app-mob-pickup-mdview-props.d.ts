import { IAppMobMDViewProps } from './i-app-mob-md-view-props';
/**
 * 移动端选择多数据视图视图输入属性接口
 *
 * @export
 * @interface IAppMobPickUpMDViewProps
 */
export declare type IAppMobPickUpMDViewProps = IAppMobMDViewProps;
