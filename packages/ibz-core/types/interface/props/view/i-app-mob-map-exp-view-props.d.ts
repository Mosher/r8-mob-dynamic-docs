import { IAppDEExpViewProps } from './i-app-de-exp-view-props';
/**
 * 移动端地图导航视图输入属性接口
 *
 * @export
 * @interface IAppMobMapExpViewProps
 */
export interface IAppMobMapExpViewProps extends IAppDEExpViewProps {
}
