import { IAppViewProps } from './i-app-view-props';
/**
 * 移动端应用看板视图输入参数接口
 *
 * @exports
 * @interface IAppPortalViewProps
 * @extend IAppViewProps
 */
export declare type IAppPortalViewProps = IAppViewProps;
