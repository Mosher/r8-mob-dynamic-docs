import { IAppDEMultiDataViewProps } from './i-app-de-multi-data-view-props';
/**
 * 移动端日历视图视图输入属性接口
 *
 * @export
 * @interface IAppMobCalendarViewProps
 */
export declare type IAppMobCalendarViewProps = IAppDEMultiDataViewProps;
