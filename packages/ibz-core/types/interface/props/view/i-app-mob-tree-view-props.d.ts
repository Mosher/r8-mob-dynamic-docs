import { IAppDEMultiDataViewProps } from './i-app-de-multi-data-view-props';
/**
 * 移动端实体树视图输入参数
 *
 * @exports
 * @interface IAppMobTreeViewProps
 * @extends IAppDEMultiDataViewProps
 */
export declare type IAppMobTreeViewProps = IAppDEMultiDataViewProps;
