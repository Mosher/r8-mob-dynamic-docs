import { IAppDEMultiDataViewProps } from './i-app-de-multi-data-view-props';
/**
 * 移动端多表单编辑视图视图输入属性接口
 *
 * @export
 * @interface IAppMobMEditViewProps
 */
export declare type IAppMobMEditViewProps = IAppDEMultiDataViewProps;
