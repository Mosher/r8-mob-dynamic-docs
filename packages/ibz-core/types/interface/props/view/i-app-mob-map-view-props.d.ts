import { IAppDEViewProps } from './i-app-de-view-props';
/**
 * 移动端地图视图视图输入属性接口
 *
 * @export
 * @interface IAppMobMapViewProps
 */
export declare type IAppMobMapViewProps = IAppDEViewProps;
