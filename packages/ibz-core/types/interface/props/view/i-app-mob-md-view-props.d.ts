import { IAppDEMultiDataViewProps } from './i-app-de-multi-data-view-props';
/**
 * 移动端多数据视图视图输入属性接口
 *
 * @export
 * @interface IAppMobMDViewProps
 */
export declare type IAppMobMDViewProps = IAppDEMultiDataViewProps;
