import { IAppCtrlProps } from './i-app-ctrl-props';
/**
 * 移动端搜索表单输入参数接口
 *
 * @export
 * @interface IAppMobSearchFormProps
 */
export declare type IAppMobSearchFormProps = IAppCtrlProps;
