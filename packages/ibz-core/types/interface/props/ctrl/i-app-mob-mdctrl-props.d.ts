import { IAppMDCtrlProps } from './i-app-md-ctrl-props';
/**
 * 移动端多数据部件传入参数接口
 *
 * @interface IAppMobMDCtrlProps
 * @extends IAppMDCtrlProps
 */
export declare type IAppMobMDCtrlProps = IAppMDCtrlProps;
