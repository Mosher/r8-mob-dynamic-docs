import { IAppCtrlProps } from './i-app-ctrl-props';
/**
 * 应用菜单输入参数接口
 *
 * @export
 * @interface IAppMobMenuProps
 */
export declare type IAppMobMenuProps = IAppCtrlProps;
