import { Subject } from 'rxjs';
import { IParam } from '../../common';
/**
 * 应用部件输入参数接口
 *
 * @export
 * @interface IAppCtrlProps
 */
export interface IAppCtrlProps {
    /**
     * 部件模型数据
     *
     * @interface IAppCtrlProps
     */
    controlInstance: IParam;
    /**
     * 导航上下文
     *
     * @interface IAppCtrlProps
     */
    navContext: IParam;
    /**
     * 导航视图参数
     *
     * @interface IAppCtrlProps
     */
    navParam: IParam;
    /**
     * 导航数据
     *
     * @interface IAppCtrlProps
     */
    navDatas: IParam;
    /**
     * 视图传输对象
     *
     * @interface IAppCtrlProps
     */
    viewState: Subject<any>;
    /**
     * 视图上下文数据
     *
     * @interface IAppCtrlProps
     */
    viewCtx: IParam;
    /**
     * 部件显示模式
     *
     * @interface IAppMDCtrlProps
     */
    ctrlShowMode: string;
}
