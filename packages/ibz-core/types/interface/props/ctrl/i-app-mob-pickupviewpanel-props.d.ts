import { IAppCtrlProps } from './i-app-ctrl-props';
/**
 * 移动端选择视图面板输入参数接口
 *
 * @export
 * @interface IAppMobPickUpViewPanelProps
 */
export interface IAppMobPickUpViewPanelProps extends IAppCtrlProps {
    /**
     * @description 是否为多选
     */
    isMultiple: boolean;
}
