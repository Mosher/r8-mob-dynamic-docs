import { IAppCtrlProps } from './i-app-ctrl-props';
/**
 * 移动端上下文菜单输入参数接口
 *
 * @exports
 * @interface IAppMobContextMenuProps
 * @extends IAppCtrlProps
 */
export declare type IAppMobContextMenuProps = IAppCtrlProps;
