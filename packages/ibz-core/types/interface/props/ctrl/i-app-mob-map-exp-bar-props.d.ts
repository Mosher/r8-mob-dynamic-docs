import { IAppExpBarCtrlProps } from './i-app-exp-bar-ctrl-props';
/**
 * 移动端地图导航部件输入属性接口
 *
 * @export
 * @interface IAppMobMapExpBarProps
 */
export interface IAppMobMapExpBarProps extends IAppExpBarCtrlProps {
}
