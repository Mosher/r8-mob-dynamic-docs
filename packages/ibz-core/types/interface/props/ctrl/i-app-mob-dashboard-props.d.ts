import { IAppCtrlProps } from './i-app-ctrl-props';
/**
 * 移动端数据看板部件输入参数
 *
 * @exports
 * @interface IAppMobDashboardProps
 * @extends IAppCtrlProps
 */
export declare type IAppMobDashboardProps = IAppCtrlProps;
