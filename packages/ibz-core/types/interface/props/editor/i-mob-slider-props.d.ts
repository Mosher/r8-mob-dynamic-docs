import { IEditorProps } from './i-editor-props';
/**
 * 应用菜单输入参数接口
 *
 * @export
 * @interface IMobSliderProps
 */
export declare type IMobSliderProps = IEditorProps;
