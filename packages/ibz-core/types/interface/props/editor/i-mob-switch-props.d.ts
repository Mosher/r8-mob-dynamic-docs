import { IEditorProps } from './i-editor-props';
/**
 * 开关输入参数接口
 *
 * @export
 * @interface IMobSwitchProps
 */
export declare type IMobSwitchProps = IEditorProps;
