import { IEditorProps } from './i-editor-props';
/**
 * 标签输入参数接口
 *
 * @export
 * @interface IMobSpanProps
 */
export declare type IMobSpanProps = IEditorProps;
