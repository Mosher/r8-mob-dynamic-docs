import { IEditorProps } from './i-editor-props';
/**
 * 选项框编辑器输入参数接口
 *
 * @export
 * @interface IMobCheckBoxProps
 */
export declare type IMobCheckBoxProps = IEditorProps;
