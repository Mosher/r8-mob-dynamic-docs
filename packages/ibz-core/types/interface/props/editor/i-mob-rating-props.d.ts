import { IEditorProps } from './i-editor-props';
/**
 * 评分器输入参数接口
 *
 * @export
 * @interface IMobRatingProps
 */
export declare type IMobRatingProps = IEditorProps;
