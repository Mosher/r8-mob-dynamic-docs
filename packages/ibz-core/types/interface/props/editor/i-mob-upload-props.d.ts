import { IEditorProps } from './i-editor-props';
/**
 * 移动端文件上传编辑器输入属性接口
 *
 * @export
 * @interface IAppUploadProps
 */
export declare type IAppUploadProps = IEditorProps;
