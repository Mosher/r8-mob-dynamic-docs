/**
 * 视图事件
 *
 * @export
 */
export declare enum AppEvents {
    /**
     * @description 初始化完成
     */
    INITED = "onInited",
    /**
     * @description 销毁完成
     */
    DESTROYED = "onDestroyed"
}
