/**
 * 移动端应用菜单事件
 *
 * @export
 * @enum MobMenuEvents
 */
export declare enum MobMenuEvents {
    /**
     * @description 初始化完成
     */
    INITED = "onInited",
    /**
     * @description 销毁完成
     */
    DESTROYED = "onDestroyed",
    /**
     * @description 部件挂载完成
     */
    MOUNTED = "onMounted",
    /**
     * @description 关闭
     */
    CLOSE = "onClose"
}
