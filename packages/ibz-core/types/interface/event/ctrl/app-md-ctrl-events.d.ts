/**
 * 多数据部件基类事件
 *
 * @export
 * @class AppMDCtrlEvents
 */
export declare enum AppMDCtrlEvents {
    /**
     * @description 部件挂载完成
     */
    MOUNTED = "onMounted",
    /**
     * @description 初始化完成
     */
    INITED = "onInited",
    /**
     * @description 销毁完成
     */
    DESTROYED = "onDestroyed",
    /**
     * @description 关闭
     */
    CLOSE = "onClose",
    /**
     * @description 选中数据改变
     */
    SELECT_CHANGE = "onSelectionChange",
    /**
     * @description 行选中
     */
    ROW_SELECTED = "onRowSelected",
    /**
     * @description 数据加载之前
     */
    BEFORE_LOAD = "onBeforeLoad",
    /**
     * @description 数据加载成功
     */
    LOAD_SUCCESS = "onLoadSuccess",
    /**
     * @description 数据加载异常
     */
    LOAD_ERROR = "onLoadError"
}
