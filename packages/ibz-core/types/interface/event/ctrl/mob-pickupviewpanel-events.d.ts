/**
 * 移动端选择面板部件事件
 *
 * @export
 * @enum MobPickupViewPanelEvents
 */
export declare enum MobPickupViewPanelEvents {
    /**
     * @description 初始化完成
     */
    INITED = "onInited",
    /**
     * @description 销毁完成
     */
    DESTROYED = "onDestroyed",
    /**
     * @description 部件挂载完成
     */
    MOUNTED = "onMounted",
    /**
     * @description 关闭
     */
    CLOSE = "onClose",
    /**
     * @description 面板数据变化
     */
    DATA_CHANGE = "onPanelDataChange",
    /**
     * @description 选中数据改变
     */
    SELECT_CHANGE = "onSelectionChange"
}
