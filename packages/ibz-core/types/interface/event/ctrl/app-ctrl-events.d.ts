/**
 * 部件事件
 *
 * @export
 * @enum AppCtrlEvents
 */
export declare enum AppCtrlEvents {
    /**
     * @description 部件挂载完成
     */
    MOUNTED = "onMounted",
    /**
     * @description 初始化完成
     */
    INITED = "onInited",
    /**
     * @description 销毁完成
     */
    DESTROYED = "onDestroyed",
    /**
     * @description 关闭
     */
    CLOSE = "onClose",
    /**
     * @description 搜索
     */
    SEARCH = "onSearch",
    /**
     * @description 选中数据改变
     */
    SELECT_CHANGE = "onSelectionChange",
    /**
     * @description 加载草稿之前
     */
    BEFORE_LOAD_DRAFT = "onBeforeLoadDraft"
}
