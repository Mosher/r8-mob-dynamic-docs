/**
 * 移动端搜索表单事件
 *
 * @export
 * @enum MobSearchformEvents
 */
export declare enum MobSearchFormEvents {
    /**
     * @description 初始化完成
     */
    INITED = "onInited",
    /**
     * @description 销毁完成
     */
    DESTROYED = "onDestroyed",
    /**
     * @description 部件挂载完成
     */
    MOUNTED = "onMounted",
    /**
     * @description 关闭
     */
    CLOSE = "onClose",
    /**
     * @description 数据加载之前
     */
    BEFORE_LOAD = "onBeforeLoad",
    /**
     * @description 数据加载成功
     */
    LOAD_SUCCESS = "onLoadSuccess",
    /**
     * @description 数据加载异常
     */
    LOAD_ERROR = "onLoadError",
    /**
     * @description 加载草稿之前
     */
    BEFORE_LOAD_DRAFT = "onBeforeLoadDraft",
    /**
     * @description 加载草稿成功
     */
    LOAD_DRAFT_SUCCESS = "onLoadDraftSuccess",
    /**
     * @description 加载草稿失败
     */
    LOAD_DRAFT_ERROR = "onLoadDraftError",
    /**
     * @description 保存之前
     */
    BEFORE_SAVE = "onBeforeSave",
    /**
     * @description 保存成功
     */
    SAVE_SUCCESS = "onSaveSuccess",
    /**
     * @description 保存失败
     */
    SAVE_ERROR = "onSaveError",
    /**
     * @description 删除之前
     */
    BEFORE_REMOVE = "onBeforeRemove",
    /**
     * @description 删除成功
     */
    REMOVE_SUCCESS = "onRemoveSuccess",
    /**
     * @description 删除失败
     */
    REMOVE_ERROR = "onRemoveError",
    /**
     * @description 启动之前
     */
    BEFORE_START = "onBeforeWFStart",
    /**
     * @description 启动成功
     */
    START_SUCCESS = "onWFStartSuccess",
    /**
     * @description 启动失败
     */
    START_ERROR = "onWFStartError",
    /**
     * @description 提交之前
     */
    BEFORE_SUBMIT = "onBeforeWFSubmit",
    /**
     * @description 提交成功
     */
    SUBMIT_SUCCESS = "onWFSubmitSuccess",
    /**
     * @description 提交失败
     */
    SUBMIT_ERROR = "onWFSubmitError",
    /**
     * @description 值变化
     */
    VALUE_CHANGE = "onValueChange",
    /**
     * @description 搜索
     */
    SEARCH = "onSearch",
    /**
     * @description 重置
     */
    RESET = "onReset"
}
