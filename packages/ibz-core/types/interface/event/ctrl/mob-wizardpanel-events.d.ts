export declare enum MobWizardPanelEvents {
    /**
     * @description 完成
     */
    FINISH = "finish",
    /**
     * @description 部件挂载完成
     */
    MOUNTED = "onMounted",
    /**
     * @description 保存成功
     */
    SAVE_SUCCESS = "onSaveSuccess",
    /**
     * @description 数据加载成功
     */
    LOAD_SUCCESS = "onLoadSuccess"
}
