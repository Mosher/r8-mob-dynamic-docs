/**
 * 移动端工具栏事件
 *
 * @export
 * @enum MobToolbarEvents
 */
export declare enum MobToolbarEvents {
    /**
     * @description 初始化完成
     */
    INITED = "onInited",
    /**
     * @description 销毁完成
     */
    DESTROYED = "onDestroyed",
    /**
     * @description 部件挂载完成
     */
    MOUNTED = "onMounted",
    /**
     * @description 关闭
     */
    CLOSE = "onClose",
    /**
     * @description 工具栏按钮点击
     */
    TOOLBAR_CLICK = "onToolbarClick"
}
