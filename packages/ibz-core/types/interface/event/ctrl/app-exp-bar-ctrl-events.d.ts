export declare enum AppExpBarCtrlEvents {
    /**
     * @description 选中数据改变
     */
    SELECT_CHANGE = "onSelectionChange"
}
