/**
 * 视图事件
 *
 * @export
 * @enum AppViewEvent
 */
export declare enum AppViewEvents {
    /**
     * @description 初始化完成
     */
    INITED = "onInited",
    /**
     * @description 销毁完成
     */
    DESTROYED = "onDestroyed",
    /**
     * @description 视图数据变化
     */
    LOAD = "onLoad",
    /**
     * @description 视图数据变化
     */
    DATA_CHANGE = "onDatasChange",
    /**
     * @description 视图关闭
     */
    CLOSE = "onClose",
    /**
     * @description 视图挂载完成
     */
    MOUNTED = "onMounted"
}
