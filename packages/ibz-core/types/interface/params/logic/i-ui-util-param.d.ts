import { IParam } from '../../common';
/**
 * UI工具参数接口
 *
 */
export interface IUIUtilParam {
    /**
     * 应用全局共享参数
     *
     * @memberof IUIUtilParam
     */
    appGlobal: IParam;
    /**
     * 顶层路由全局共享参数
     *
     * @memberof IUIUtilParam
     */
    routeViewGlobal: IParam;
    /**
     * 当前视图共享参数
     *
     * @memberof IUIUtilParam
     */
    viewGlobal: IParam;
    /**
     * 导航数据参数
     *
     * @memberof IUIUtilParam
     */
    viewNavData: IParam;
    /**
     * 当前应用对象
     *
     * @memberof IUIUtilParam
     */
    app: IParam;
    /**
     * 当前环境顶层视图对象
     *
     * @memberof IUIUtilParam
     */
    topview: IParam;
    /**
     * 当前环境父级视图对象
     *
     * @memberof IUIUtilParam
     */
    parentview: IParam;
    /**
     * 当前环境视图对象
     *
     * @memberof IUIUtilParam
     */
    view: IParam;
}
