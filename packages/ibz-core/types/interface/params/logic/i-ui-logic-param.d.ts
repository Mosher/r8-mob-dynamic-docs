import { IParam } from '../../common';
import { IUIDataParam } from './i-ui-data-param';
import { IUIEnvironmentParam } from './i-ui-environment-param';
import { IUIUtilParam } from './i-ui-util-param';
/**
 * UI逻辑参数接口
 *
 */
export interface IUILogicParam {
    /**
     * UI界面数据
     *
     * @memberof IUILogicParam
     */
    arg: IUIDataParam;
    /**
     * UI工具数据
     *
     * @memberof IUILogicParam
     */
    utils: IUIUtilParam;
    /**
     * UI环境数据
     *
     * @memberof IUILogicParam
     */
    environments: IUIEnvironmentParam;
    /**
     * 其他UI附加数据
     *
     * @memberof IUILogicParam
     */
    others?: IParam;
}
