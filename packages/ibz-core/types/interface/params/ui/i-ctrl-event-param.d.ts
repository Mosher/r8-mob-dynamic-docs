/**
 * 部件事件参数
 *
 * @interface ICtrlEventParam
 */
export interface ICtrlEventParam {
    /**
     * 行为名称
     *
     * @type {string}
     * @memberof ICtrlEventParam
     */
    action: string;
    /**
     * 部件名称
     *
     * @type string
     * @memberof ICtrlEventParam
     */
    controlname: string;
    /**
     * 传输数据
     *
     * @type any
     * @memberof ICtrlEventParam
     */
    data: any;
}
