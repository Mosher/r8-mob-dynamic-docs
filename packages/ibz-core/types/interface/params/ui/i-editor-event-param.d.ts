/**
 * 编辑器事件参数
 *
 * @interface IEditorEventParam
 */
export interface IEditorEventParam {
    /**
     * 行为名称
     *
     * @type {string}
     * @memberof IEditorEventParam
     */
    action: string;
    /**
     * 编辑器名称
     *
     * @type string
     * @memberof IEditorEventParam
     */
    editorName: string;
    /**
     * 传输数据
     *
     * @type any
     * @memberof IEditorEventParam
     */
    data: any;
}
