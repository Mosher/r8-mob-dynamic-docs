import { IParam } from '../../common';
/**
 * 视图状态参数
 *
 * @interface IViewStateParam
 */
export interface IViewStateParam {
    /**
     * 标识
     *
     * @type {string}
     * @memberof IViewStateParam
     */
    tag: string;
    /**
     * 触发行为
     *
     * @type string
     * @memberof IViewStateParam
     */
    action: string;
    /**
     * 数据
     *
     * @type {IParam}
     * @memberof IViewStateParam
     */
    data?: IParam;
}
