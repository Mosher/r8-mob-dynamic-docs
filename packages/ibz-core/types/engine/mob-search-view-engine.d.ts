import { ViewEngine } from './view-engine';
/**
 * 搜索视图引擎
 *
 * @export
 * @class SearchViewEngine
 * @extends {ViewEngine}
 */
export declare class MobSearchViewEngine extends ViewEngine {
    /**
     * 表单部件
     *
     * @protected
     * @type {*}
     * @memberof SearchViewEngine
     */
    protected searchForm: any;
    /**
     * 初始化
     *
     * @param {*} options
     * @memberof SearchViewEngine
     */
    init(options: any): void;
    /**
     * 引擎加载
     *
     * @param {*} [opts={}]
     * @memberof SearchViewEngine
     */
    load(opts?: any): void;
    /**
     * 事件处理
     *
     * @param {string} ctrlName
     * @param {string} eventName
     * @param {*} args
     * @memberof SearchViewEngine
     */
    onCtrlEvent(ctrlName: string, eventName: string, args: any): void;
    /**
     * 搜索表单事件
     *
     * @param {string} eventName
     * @param {*} [args={}]
     * @memberof SearchViewEngine
     */
    searchFormEvent(eventName: string, args?: any): void;
    /**
     * 搜索表单加载完成
     *
     * @param {*} [args={}]
     * @memberof SearchViewEngine
     */
    onSearchFormLoad(args?: any): void;
    /**
     * 数据部件加载之前
     *
     * @param {*} [arg={}]
     * @memberof SearchViewEngine
     */
    dataCtrlBeforeLoad(arg?: any): void;
    /**
     * 获取搜索表单
     *
     * @returns {*}
     * @memberof SearchViewEngine
     */
    getSearchForm(): any;
}
