import { MobTreeViewEngine } from './mob-tree-view-engine';
/**
 * 实体移动端选择树视图（部件视图）界面引擎
 *
 * @export
 * @class MobPickupTreeViewEngine
 * @extends {ViewEngine}
 */
export declare class MobPickupTreeViewEngine extends MobTreeViewEngine {
}
