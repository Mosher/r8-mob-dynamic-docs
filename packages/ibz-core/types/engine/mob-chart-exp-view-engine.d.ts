import { ViewEngine } from './view-engine';
/**
 * 实体移动端多数据界面引擎
 *
 * @export
 * @class MDViewEngine
 * @extends {ViewEngine}
 */
export declare class MobChartExpViewEngine extends ViewEngine {
    /**
     * @description 图表部件
     * @type {*}
     * @memberof GridViewEngine
     */
    protected chartexpbar: any;
    /**
     * @description 引擎初始化
     * @param {*} [options={}]
     * @memberof GridViewEngine
     */
    init(options?: any): void;
    /**
     * @description 多数据部件
     * @param {string} ctrlName
     * @param {string} eventName
     * @param {*} args
     * @memberof MobMDViewEngine
     */
    onCtrlEvent(ctrlName: string, eventName: string, args: any): void;
    /**
     * @description 获取多数据部件
     * @returns {*}
     * @memberof MDViewEngine
     */
    getChartExpBar(): any;
    /**
     * @description 引擎加载
     * @param {*} opts
     * @return {*}  {*}
     * @memberof MobChartExpViewEngine
     */
    load(opts: any): any;
}
