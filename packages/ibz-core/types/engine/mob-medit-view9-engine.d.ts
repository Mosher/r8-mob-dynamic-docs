import { ViewEngine } from './view-engine';
/**
 * 实体移动端多表单编辑视图（部件视图）界面引擎
 *
 * @export
 * @class MobMEditView9Engine
 * @extends {ViewEngine}
 */
export declare class MobMEditView9Engine extends ViewEngine {
    /**
     * 多编辑表单面板
     *
     * @protected
     * @type {*}
     * @memberof MobMEditView9Engine
     */
    protected meditviewpanel: any;
    /**
     * Creates an instance of MobMEditView9Engine.
     * @memberof MobMEditView9Engine
     */
    constructor();
    /**
     * 引擎初始化
     *
     * @param {*} [options={}]
     * @memberof MobMEditView9Engine
     */
    init(options?: any): void;
    /**
     * 引擎加载
     *
     * @param {*} [opts={}]
     * @memberof MobMEditView9Engine
     */
    load(opts?: any): void;
    /**
     * 部件事件
     *
     * @param {string} ctrlName
     * @param {string} eventName
     * @param {*} args
     * @memberof MobMEditView9Engine
     */
    onCtrlEvent(ctrlName: string, eventName: string, args: any): void;
    /**
     * 多编辑表单面板事件分发
     *
     * @param {*} [opts={}]
     * @memberof MobMEditView9Engine
     */
    editviewpanelEvent(eventName: string, args: any): void;
    /**
     * 多编辑表单面板部件
     *
     * @returns {*}
     * @memberof MDViewEngine
     */
    getMeditviewpanel(): any;
}
