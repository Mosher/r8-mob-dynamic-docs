import { ViewEngine } from './view-engine';
/**
 * 移动端向导视图引擎
 *
 * @export
 * @class MobWizardViewEngine
 */
export declare class MobWizardViewEngine extends ViewEngine {
    /**
     * 向导面板部件
     *
     * @protected
     * @type {*}
     * @memberof MobWizardViewEngine
     */
    protected wizardpanel: any;
    /**
     * 初始化编辑视图引擎
     *
     * @param {*} [options={}]
     * @memberof MobWizardViewEngine
     */
    init(options?: any): void;
    /**
     * 引擎加载
     *
     * @param {*} [opts={}]
     * @memberof MobWizardViewEngine
     */
    load(opts?: any): void;
    /**
     * 部件事件机制
     *
     * @param {string} ctrlName
     * @param {string} eventName
     * @param {*} args
     * @memberof MobWizardViewEngine
     */
    onCtrlEvent(ctrlName: string, eventName: string, args: any): void;
    /**
     * 事件处理
     *
     * @param {string} eventName
     * @param {any[]} args
     * @memberof MobWizardViewEngine
     */
    wizardPanelEvent(eventName: string, args: any): void;
    /**
     * 完成
     *
     * @param {*} args
     * @memberof MobWizardViewEngine
     */
    onfinish(args: any): void;
    /**
     * 获取向导面板
     *
     * @returns {*}
     * @memberof MobWizardViewEngine
     */
    getWizardPanel(): any;
}
