import { IUIDataParam } from '../interface';
import { DEMultiDataViewEngine } from './de-multi-data-view-engine';
/**
 * 实体移动端多数据视图界面引擎
 *
 * @export
 * @class MobMDViewEngine
 * @extends {ViewEngine}
 */
export declare class MobMDViewEngine extends DEMultiDataViewEngine {
    /**
     * 多数据部件
     *
     * @type {*}
     * @memberof MobMDViewEngine
     */
    protected mdCtrl: any;
    /**
     * 引擎初始化
     *
     * @param {*} [options={}]
     * @memberof MobMDViewEngine
     */
    init(options?: any): void;
    /**
     * 部件事件
     *
     * @param {string} ctrlName
     * @param {string} eventName
     * @param {*} args
     * @memberof MobMDViewEngine
     */
    onCtrlEvent(ctrlName: string, eventName: string, args: any): void;
    /**
     * 多数据部件事件处理
     *
     * @param {string} eventName
     * @param {any[]} args
     * @memberof MobMDViewEngine
     */
    MDCtrlEvent(eventName: string, args: any): void;
    /**
     * 多数据项界面_编辑操作
     *
     * @param {*} [params={}]
     * @returns {void}
     * @memberof MobMDViewEngine
     */
    doEdit(params: IUIDataParam): void;
    /**
     * 获取多数据部件
     *
     * @returns {*}
     * @memberof MobMDViewEngine
     */
    getMDCtrl(): any;
}
