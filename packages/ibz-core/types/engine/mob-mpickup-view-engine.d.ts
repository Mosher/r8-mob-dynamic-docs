import { MobPickupViewEngine } from './mob-pickup-view-engine';
/**
 * 实体移动端多数据选择视图界面引擎
 *
 * @export
 * @class MobMPickupViewEngine
 * @extends {ViewEngine}
 */
export declare class MobMPickupViewEngine extends MobPickupViewEngine {
}
