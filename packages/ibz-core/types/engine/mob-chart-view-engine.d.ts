import { DEMultiDataViewEngine } from './de-multi-data-view-engine';
/**
 * 实体移动端图表视图界面引擎
 *
 * @export
 * @class MobChartViewEngine
 * @extends {ViewEngine}
 */
export declare class MobChartViewEngine extends DEMultiDataViewEngine {
    /**
     * 图表对象
     *
     * @type {*}
     * @memberof MobChartViewEngine
     */
    chart: any;
    /**
     * 图表初始化
     *
     * @param {*} options
     * @memberof MobChartViewEngine
     */
    init(options: any): void;
    /**
     * 部件事件
     *
     * @param {string} ctrlName
     * @param {string} eventName
     * @param {*} args
     * @memberof MobChartViewEngine
     */
    onCtrlEvent(ctrlName: string, eventName: string, args: any): void;
    /**
     * 图表部件事件处理
     *
     * @param {string} eventName
     * @param {any[]} args
     * @memberof MobMDViewEngine
     */
    chartEvent(eventName: string, args: any): void;
    /**
     * 获取图表
     *
     * @returns {*}
     * @memberof MobChartViewEngine
     */
    getChart(): any;
    /**
   * 获取多数据部件
   *
   * @returns {*}
   * @memberof MobChartViewEngine
   */
    getMDCtrl(): any;
}
