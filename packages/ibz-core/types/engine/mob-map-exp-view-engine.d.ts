import { ViewEngine } from './view-engine';
/**
 * 实体移动端地图导航界面引擎
 *
 * @export
 * @class MDViewEngine
 * @extends {ViewEngine}
 */
export declare class MobMapExpViewEngine extends ViewEngine {
    /**
     * @description 地图导航栏部件
     * @type {*}
     * @memberof GridViewEngine
     */
    protected mapexpbar: any;
    /**
     * @description 引擎初始化
     * @param {*} [options={}]
     * @memberof GridViewEngine
     */
    init(options?: any): void;
    /**
     * @description 多数据部件
     * @param {string} ctrlName
     * @param {string} eventName
     * @param {*} args
     * @memberof MobMDViewEngine
     */
    onCtrlEvent(ctrlName: string, eventName: string, args: any): void;
    /**
     * @description 获取多数据部件
     * @returns {*}
     * @memberof MDViewEngine
     */
    getMapExpBar(): any;
    /**
     * @description 引擎加载
     * @param {*} opts
     * @return {*}  {*}
     * @memberof MobMapExpViewEngine
     */
    load(opts: any): any;
}
