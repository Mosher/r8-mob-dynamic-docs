/**
 * 视图引擎
 *
 * @export
 * @class ViewEngine
 */
export declare class ViewEngine {
    /**
     * 视图部件对象
     *
     * @protected
     * @type {*}
     * @memberof ViewEngine
     */
    protected view: any;
    /**
     * 引擎参数
     *
     * @type {*}
     * @memberof ViewEngine
     */
    protected opt: any;
    /**
     *
     *
     * @type {*}
     * @memberof ViewEngine
     */
    protected methods: any;
    /**
     * 是否默认记载
     *
     * @type {boolean}
     * @memberof ViewEngine
     */
    isLoadDefault: boolean;
    /**
     * 实体主键属性
     *
     * @type {(string | undefined)}
     * @memberof ViewEngine
     */
    keyPSDEField: string | undefined;
    /**
     * 实体主信息属性
     *
     * @type {(string | undefined)}
     * @memberof ViewEngine
     */
    majorPSDEField: string | undefined;
    /**
     * Creates an instance of ViewEngine.
     * @memberof ViewEngine
     */
    constructor();
    /**
     * 引擎初始化
     *
     * @param {*} [options={}]
     * @memberof ViewEngine
     */
    init(options?: any): void;
    /**
     * 引擎加载
     *
     * @param {*} [opts={}]
     * @memberof ViewEngine
     */
    load(opts?: any): void;
    /**
     * 部件事件机制
     *
     * @param {string} ctrlName
     * @param {string} eventName
     * @param {*} args
     * @memberof ViewEngine
     */
    onCtrlEvent(ctrlName: string, eventName: string, args: any): void;
    /**
     * 视图事件处理
     *
     * @param {string} eventName 事件tag
     * @param {*} args 事件参数
     * @memberof ViewEngine
     */
    emitViewEvent(eventName: string, args: any): void;
    /**
     * 是否为方法
     *
     * @protected
     * @param {*} func
     * @returns {boolean}
     * @memberof ViewEngine
     */
    protected isFunc(func: any): boolean;
    /**
     * 父数据参数模式
     *
     * @param {{ tag: string, action: string, viewdata: any }} { tag, action, viewdata }
     * @memberof ViewEngine
     */
    setViewState2({ tag, action, viewdata }: {
        tag: string;
        action: string;
        viewdata: any;
    }): void;
    /**
     * 计算工具栏状态
     *
     * @param {boolean} state
     * @param {*} [dataaccaction]
     * @memberof ViewEngine
     */
    calcToolbarItemState(state: boolean, dataaccaction?: any): void;
    /**
     * 计算工具栏权限状态
     *
     * @param {boolean} state
     * @param {*} [dataaccaction]
     * @memberof ViewEngine
     */
    calcToolbarItemAuthState(data: any): void;
}
