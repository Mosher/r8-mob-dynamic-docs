import { MobEditViewEngine } from './mob-edit-view-engine';
/**
 * 实体移动端工作流动态编辑视图引擎基础
 *
 * @export
 * @class WFDynaEditViewEngine
 * @extends {EditViewEngine}
 */
export declare class MobWFDynaEditViewEngine extends MobEditViewEngine {
    /**
     * Creates an instance of WFDynaEditViewEngine.
     * @memberof WFDynaEditViewEngine
     */
    constructor();
    /**
     * 引擎加载
     *
     * @param {*} [opts={}]
     * @memberof WFDynaEditViewEngine
     */
    load(opts?: any): void;
}
