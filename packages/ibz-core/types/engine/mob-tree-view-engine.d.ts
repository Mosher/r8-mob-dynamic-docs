import { DEMultiDataViewEngine } from './de-multi-data-view-engine';
/**
 * 实体移动端树视图界面引擎
 *
 * @export
 * @class MobTreeViewEngine
 * @extends {ViewEngine}
 */
export declare class MobTreeViewEngine extends DEMultiDataViewEngine {
    /**
     * 树部件
     *
     * @protected
     * @type {*}
     * @memberof MobTreeViewEngine
     */
    protected tree: any;
    /**
     * 初始化编辑视图引擎
     *
     * @param {*} [options={}]
     * @memberof MobTreeViewEngine
     */
    init(options?: any): void;
    /**
     * 部件事件机制
     *
     * @param {string} ctrlName
     * @param {string} eventName
     * @param {*} args
     * @memberof MobTreeViewEngine
     */
    onCtrlEvent(ctrlName: string, eventName: string, args: any): void;
    /**
     * 树事件
     *
     * @param {string} eventName
     * @param {*} args
     * @memberof MobTreeViewEngine
     */
    MDCtrlEvent(eventName: string, args: any): void;
    /**
     * 树加载完成
     *
     * @param {*} args
     * @memberof MobTreeViewEngine
     */
    MDCtrlLoad(args: any): void;
    /**
     * 树点击事件
     *
     * @param {*} args
     * @memberof MobTreeViewEngine
     */
    onTreeClick(args: any): void;
    /**
     * 获取树对象
     *
     * @returns {*}
     * @memberof MobTreeViewEngine
     */
    getMDCtrl(): any;
}
