import { ViewEngine } from './view-engine';
import { IUIDataParam } from '../interface';
/**
 * 实体移动端数据选择视图界面引擎
 *
 * @export
 * @class MobPickupViewEngine
 * @extends {ViewEngine}
 */
export declare class MobPickupViewEngine extends ViewEngine {
    /**
     * 选择视图面板
     *
     * @type {*}
     * @memberof MobPickupViewEngine
     */
    pickupViewPanel: any;
    /**
     * Creates an instance of MobPickupViewEngine.
     *
     * @memberof MobPickupViewEngine
     */
    constructor();
    /**
     * 初始化引擎
     *
     * @param {*} options
     * @memberof MobPickupViewEngine
     */
    init(options: any): void;
    /**
     * 引擎加载
     *
     * @memberof MobPickupViewEngine
     */
    load(opts?: any): void;
    /**
     * 部件事件
     *
     * @param {string} ctrlName
     * @param {string} eventName
     * @param {*} args
     * @memberof MobPickupViewEngine
     */
    onCtrlEvent(ctrlName: string, eventName: string, args: any): void;
    /**
     * 值选中变化
     *
     * @param {any[]} args
     * @memberof MobPickupViewEngine
     */
    onSelectionChange(ctrlName: any, args: IUIDataParam): void;
    /**
     * 获取选择视图面板
     *
     * @returns {*}
     * @memberof MobPickupViewEngine
     */
    getPickupViewPanel(): any;
}
