import { IUIDataParam } from '../interface';
import { ViewEngine } from './view-engine';
export declare class MobDEDashboardViewEngine extends ViewEngine {
    /**
     * @description 数据面板部件
     * @type {*}
     * @memberof MobDEDashboardViewEngine
     */
    dashboard: any;
    /**
     * @description 搜索表单部件
     * @protected
     * @type {*}
     * @memberof MobDEDashboardViewEngine
     */
    protected searchForm: any;
    /**
     * @description 初始化引擎
     * @param {*} options 初始化参数
     * @memberof MobDEDashboardViewEngine
     */
    init(options: any): void;
    /**
     * @description 引擎加载
     * @memberof MobDEDashboardViewEngine
     */
    load(opts?: any): void;
    /**
     * @description 部件事件机制
     * @param {string} ctrlName 部件名
     * @param {string} eventName 事件名
     * @param {IUIDataParam} args 参数
     * @memberof MobDEDashboardViewEngine
     */
    onCtrlEvent(ctrlName: string, eventName: string, args: IUIDataParam): void;
    /**
     * @description 实体数据看板事件
     * @param {string} eventName 事件名
     * @param {IUIDataParam} args 参数
     * @memberof MobDEDashboardViewEngine
     */
    dashboardEvent(eventName: string, args: IUIDataParam): void;
    /**
     * @description 搜索表单事件
     * @param {string} eventName
     * @param {*} [args={}]
     * @memberof MobDEDashboardViewEngine
     */
    searchFormEvent(eventName: string, args?: any): void;
    /**
     * @description 搜索表单加载完成
     * @param {*} [args={}]
     * @memberof MobDEDashboardViewEngine
     */
    onSearchFormLoad(args?: any): void;
    /**
     * @description 搜索表单搜索
     * @param {*} [args={}]
     * @memberof MobDEDashboardViewEngine
     */
    onSearchFormSearch(args?: any): void;
    /**
     * @description 搜索表单重置
     * @param {*} [args={}]
     * @memberof MobDEDashboardViewEngine
     */
    onSearchFormReset(args?: any): void;
    /**
     * @description 获取部件对象
     * @return {*}  {*}
     * @memberof MobDEDashboardViewEngine
     */
    getDashboard(): any;
    /**
     * @description 获取搜索表单部件
     * @return {*}  {*}
     * @memberof MobDEDashboardViewEngine
     */
    getSearchForm(): any;
}
