import { ViewEngine } from './view-engine';
/**
 * @description 实体移动端自定义视图界面引擎
 * @export
 * @class MobCustomViewEngine
 * @extends {ViewEngine}
 */
export declare class MobCustomViewEngine extends ViewEngine {
    /**
     * @description 部件引擎集合
     * @type {Array<any>}
     * @memberof MobCustomViewEngine
     */
    ctrlEngineArray: Array<any>;
    /**
     * @description 视图部件Map
     * @type {Map<string, any>}
     * @memberof MobCustomViewEngine
     */
    viewCtrlMap: Map<string, any>;
    /**
     * 引擎初始化
     *
     * @param {*} [options={}]
     * @memberof MobCustomViewEngine
     */
    init(options?: any): void;
    /**
     * @description 初始化引擎Map
     * @param {*} options
     * @memberof MobCustomViewEngine
     */
    initCtrlEngineArray(options: any): void;
    /**
     * @description 初始化视图部件Map
     * @param {*} options
     * @memberof MobCustomViewEngine
     */
    initViewControlMap(options: any): void;
    /**
     * @description 引擎加载
     * @param {*} [opts={}]
     * @memberof MobCustomViewEngine
     */
    load(opts?: any): void;
    /**
     * @description 部件事件机制
     * @param {string} ctrlName 部件名称
     * @param {string} eventName 事件名称
     * @param {*} args 事件参数
     * @memberof MobCustomViewEngine
     */
    onCtrlEvent(ctrlName: string, eventName: string, args: any): void;
    /**
     * @description 处理视图引擎参数
     * @param {*} args 数据
     * @return {*}
     * @memberof MobCustomViewEngine
     */
    handleViewEngineParams(args: any): {
        triggerCtrlName: any;
        triggerType: string;
        targetCtrlName: any;
    } | null | undefined;
    /**
     * @description 处理搜索部件加载并搜索（参数可指定触发部件）
     * @param {*} args 引擎参数
     * @return {*}
     * @memberof MobCustomViewEngine
     */
    CtrlLoadAndSearch(args: any): {
        triggerCtrlName: string;
        triggerType: string;
        targetCtrlName: any;
    } | null;
    /**
     * @description 处理部件加载（参数可指定触发部件）
     * @param {*} args
     * @return {*}
     * @memberof MobCustomViewEngine
     */
    handleCtrlLoad(args: any): {
        triggerCtrlName: any;
        triggerType: string;
        targetCtrlName: any;
    } | null;
}
