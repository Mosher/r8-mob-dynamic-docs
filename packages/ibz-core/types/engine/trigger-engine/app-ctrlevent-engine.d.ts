import { AppUITriggerEngine } from './app-ui-trigger-engine';
/**
 * 界面部件事件触发逻辑引擎
 *
 * @export
 * @class AppCtrlEventEngine
 */
export declare class AppCtrlEventEngine extends AppUITriggerEngine {
    /**
     * Creates an instance of AppCtrlEventEngine.
     * @memberof AppCtrlEventEngine
     */
    constructor(opts: any);
}
