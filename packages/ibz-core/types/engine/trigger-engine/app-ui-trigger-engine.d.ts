import { IUILogicParam } from '../../interface/params';
/**
 * 界面触发引擎
 *
 * @export
 * @class AppUITriggerEngine
 */
export declare class AppUITriggerEngine {
    /**
     * 传入参数
     * @memberof AppUITriggerEngine
     */
    options: any;
    /**
     * 名称
     * @memberof AppUITriggerEngine
     */
    name: string;
    /**
     * 目标逻辑类型
     * @memberof AppUITriggerEngine
     */
    logicType: 'DEUILOGIC' | 'SYSVIEWLOGIC' | 'PFPLUGIN' | 'SCRIPT' | null | undefined;
    /**
     * 执行脚本代码
     * @memberof AppUITriggerEngine
     */
    scriptCode: string | null | undefined;
    /**
     * Creates an instance of AppUITriggerEngine.
     * @memberof AppUITriggerEngine
     */
    constructor(opts: any);
    /**
     * 执行界面逻辑
     * @memberof AppUITriggerEngine
     */
    executeAsyncUILogic(opts: IUILogicParam): Promise<false | void | import("../../interface/params").IUIDataParam>;
    /**
     * 执行界面逻辑
     * @memberof AppUITriggerEngine
     */
    executeUILogic(opts: IUILogicParam): void | import("../../interface/params").IUIDataParam;
    /**
     * 执行实体界面逻辑(异步)
     * @memberof AppUITriggerEngine
     */
    executeAsyncDeUILogic(opts: IUILogicParam): Promise<false | void>;
    /**
     * 执行系统预置界面逻辑(异步)
     * @memberof AppUITriggerEngine
     */
    executeAsyncSysViewLogic(opts: IUILogicParam): Promise<void>;
    /**
     * 执行插件界面逻辑(异步)
     * @memberof AppUITriggerEngine
     */
    executeAsyncPluginLogic(opts: IUILogicParam): Promise<void>;
    /**
     * 执行脚本界面逻辑(异步)
     * @memberof AppUITriggerEngine
     */
    executeAsyncScriptLogic(opts: IUILogicParam): Promise<import("../../interface/params").IUIDataParam | undefined>;
    /**
     * 执行实体界面逻辑
     * @memberof AppUITriggerEngine
     */
    executeDeUILogic(opts: IUILogicParam): void;
    /**
     * 执行系统预置界面逻辑
     * @memberof AppUITriggerEngine
     */
    executeSysViewLogic(opts: IUILogicParam): void;
    /**
     * 执行插件界面逻辑
     * @memberof AppUITriggerEngine
     */
    executePluginLogic(opts: IUILogicParam): void;
    /**
     * 执行脚本界面逻辑
     * @memberof AppUITriggerEngine
     */
    executeScriptLogic(opts: IUILogicParam): import("../../interface/params").IUIDataParam | undefined;
    /**
     * 分发UI逻辑参数
     * @memberof AppUITriggerEngine
     */
    dispenseUILogic(opts: IUILogicParam, scriptCode: string): import("../../interface/params").IUIDataParam | undefined;
    /**
     * 执行默认脚本逻辑
     * @memberof AppUITriggerEngine
     */
    executeDefaultScriptLogic(opts: IUILogicParam, scriptCode: string): import("../../interface/params").IUIDataParam;
    /**
     * 处理自定义脚本
     *
     * @param scriptCode 自定义脚本
     * @param isMergeArg 是否合入到arg的srfret
     * @memberof AppUITriggerEngine
     */
    handleScriptCode(scriptCode: string, isMergeArg?: boolean): string;
}
