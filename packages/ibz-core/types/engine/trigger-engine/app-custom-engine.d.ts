import { AppUITriggerEngine } from './app-ui-trigger-engine';
/**
 * 界面自定义触发逻辑引擎
 *
 * @export
 * @class AppCustomEngine
 */
export declare class AppCustomEngine extends AppUITriggerEngine {
    /**
     * Creates an instance of AppCustomEngine.
     * @memberof AppCustomEngine
     */
    constructor(opts: any);
}
