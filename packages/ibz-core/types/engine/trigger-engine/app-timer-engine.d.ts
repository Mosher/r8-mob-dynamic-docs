import { IUILogicParam } from '../../interface';
import { AppUITriggerEngine } from './app-ui-trigger-engine';
/**
 * 界面自定义触发逻辑引擎
 *
 * @export
 * @class AppTimerEngine
 */
export declare class AppTimerEngine extends AppUITriggerEngine {
    /**
     * 间隔时间
     *
     * @memberof AppTimerEngine
     */
    timer: number;
    /**
     * 计数器标识
     *
     * @memberof AppTimerEngine
     */
    timerId: any;
    /**
     * Creates an instance of AppTimerEngine.
     * @memberof AppTimerEngine
     */
    constructor(opts: any);
    /**
     * 执行界面逻辑
     * @memberof AppTimerEngine
     */
    executeAsyncUILogic(opts: IUILogicParam): Promise<void>;
    /**
     * 执行界面逻辑
     * @memberof AppTimerEngine
     */
    executeUILogic(opts: IUILogicParam): void;
    /**
     * 销毁计数器
     * @memberof AppTimerEngine
     */
    destroyTimer(): void;
}
