import { ViewEngine } from './view-engine';
/**
 * 实体移动端编辑视图界面引擎
 *
 * @export
 * @class MobEditViewEngine
 * @extends {ViewEngine}
 */
export declare class MobEditViewEngine extends ViewEngine {
    /**
     * 表单部件
     *
     * @protected
     * @type {*}
     * @memberof MobEditViewEngine
     */
    protected form: any;
    /**
     * 初始化编辑视图引擎
     *
     * @param {*} [options={}]
     * @memberof MobEditViewEngine
     */
    init(options?: any): void;
    /**
     * 引擎加载
     *
     * @param {*} [opts={}]
     * @memberof MobEditViewEngine
     */
    load(opts?: any): void;
    /**
     * 部件事件机制
     *
     * @param {string} ctrlName
     * @param {string} eventName
     * @param {*} args
     * @memberof MobEditViewEngine
     */
    onCtrlEvent(ctrlName: string, eventName: string, args: any): void;
    /**
     * 表单事件
     *
     * @param {string} eventName
     * @param {*} args
     * @memberof MobEditViewEngine
     */
    formEvent(eventName: string, args: any): void;
    /**
     * 表单加载完成
     *
     * @param {*} args
     * @memberof MobEditViewEngine
     */
    onFormLoad(arg: any): void;
    /**
     * 设置导航标题
     *
     * @memberof EditViewEngine
     */
    setNavCaption(isCreate: boolean): void;
    /**
     * 表单保存完成
     *
     * @param {*} args
     * @memberof MobEditViewEngine
     */
    onFormSave(arg: any): void;
    /**
     * 表单删除完成
     *
     * @param {*} args
     * @memberof MobEditViewEngine
     */
    onFormRemove(arg: any): void;
    /**
     * 获取表单对象
     *
     * @returns {*}
     * @memberof MobEditViewEngine
     */
    getForm(): any;
    /**
     * 转化数据
     *
     * @memberof EditViewEngine
     */
    transformData(arg: any): any;
}
