import { MobMDViewEngine } from './mob-md-view-engine';
/**
 * 地图视图引擎基础
 *
 * @export
 * @class MobMapViewEngine
 * @extends {MDViewEngine}
 */
export declare class MobMapViewEngine extends MobMDViewEngine {
    /**
     * 表格部件
     *
     * @type {*}
     * @memberof MobMapViewEngine
     */
    protected map: any;
    /**
     * Creates an instance of MobMapViewEngine.
     * @memberof MobMapViewEngine
     */
    constructor();
    /**
     * 引擎初始化
     *
     * @param {*} [options={}]
     * @memberof MobMapViewEngine
     */
    init(options?: any): void;
    /**
     * 获取多数据部件
     *
     * @returns {*}
     * @memberof MobMapViewEngine
     */
    getMDCtrl(): any;
    /**
     * 部件事件
     *
     * @param {string} ctrlName
     * @param {string} eventName
     * @param {*} args
     * @memberof MobMapViewEngine
     */
    onCtrlEvent(ctrlName: string, eventName: string, args: any): void;
    /**
     * 事件处理
     *
     * @param {string} eventName
     * @param {*} args
     * @memberof MobMapViewEngine
     */
    MDCtrlEvent(eventName: string, args: any): void;
}
