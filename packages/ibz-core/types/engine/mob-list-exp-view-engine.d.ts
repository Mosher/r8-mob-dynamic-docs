import { ViewEngine } from './view-engine';
/**
 * 实体移动端多数据界面引擎
 *
 * @export
 * @class MDViewEngine
 * @extends {ViewEngine}
 */
export declare class MobListExpViewEngine extends ViewEngine {
    /**
     * @description 列表部件
     * @type {*}
     * @memberof GridViewEngine
     */
    protected listexpbar: any;
    /**
     * @description 引擎初始化
     * @param {*} [options={}]
     * @memberof GridViewEngine
     */
    init(options?: any): void;
    /**
     * @description 多数据部件
     * @param {string} ctrlName
     * @param {string} eventName
     * @param {*} args
     * @memberof MobMDViewEngine
     */
    onCtrlEvent(ctrlName: string, eventName: string, args: any): void;
    /**
     * @description 获取多数据部件
     * @returns {*}
     * @memberof MDViewEngine
     */
    getListExpBar(): any;
    /**
     * @description 引擎加载
     * @param {*} opts
     * @return {*}  {*}
     * @memberof MobListExpViewEngine
     */
    load(opts: any): any;
}
