import { DEMultiDataViewEngine } from './de-multi-data-view-engine';
/**
 * 实体移动端日历视图界面引擎
 *
 * @export
 * @class MobCalendarViewEngine
 * @extends {ViewEngine}
 */
export declare class MobCalendarViewEngine extends DEMultiDataViewEngine {
    /**
     * 日历部件
     *
     * @protected
     * @type {*}
     * @memberof MobCalendarViewEngine
     */
    protected calendar: any;
    /**
     * 初始化日历视图引擎
     *
     * @param {*} [options={}]
     * @memberof MobCalendarViewEngine
     */
    init(options?: any): void;
    /**
     * 部件事件机制
     *
     * @param {string} ctrlName
     * @param {string} eventName
     * @param {*} args
     * @memberof MobCalendarViewEngine
     */
    onCtrlEvent(ctrlName: string, eventName: string, args: any): void;
    /**
     * 日历事件
     *
     * @param {string} eventName
     * @param {*} args
     * @memberof MobCalendarViewEngine
     */
    calendarEvent(eventName: string, args: any): void;
    /**
     * 日历数据加载完成
     *
     * @param {*} args
     * @memberof MobCalendarViewEngine
     */
    onCalendarLoad(arg: any): void;
    /**
     * 获取日历对象
     *
     * @returns {*}
     * @memberof MobCalendarViewEngine
     */
    getCalendar(): any;
    /**
     * 获取多数据部件
     *
     * @returns {*}
     * @memberof MobCalendarViewEngine
     */
    getMDCtrl(): any;
}
