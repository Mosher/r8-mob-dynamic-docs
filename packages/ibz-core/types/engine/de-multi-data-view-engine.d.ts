import { IUIDataParam } from '../interface';
import { ViewEngine } from './view-engine';
/**
 * @description 多数据视图引擎基类
 * @export
 * @class DEMultiDataViewEngine
 * @extends {ViewEngine}
 */
export declare class DEMultiDataViewEngine extends ViewEngine {
    /**
     * @description 搜索表单部件
     * @protected
     * @type {*}
     * @memberof DEMultiDataViewEngine
     */
    protected searchForm: any;
    /**
     * @description 初始化
     * @param {*} options
     * @memberof DEMultiDataViewEngine
     */
    init(options: any): void;
    /**
     * @description 引擎加载
     * @param {*} [opts={}]
     * @memberof DEMultiDataViewEngine
     */
    load(opts?: any): void;
    /**
     * @description 部件事件
     * @param {string} ctrlName
     * @param {string} eventName
     * @param {*} args
     * @memberof DEMultiDataViewEngine
     */
    onCtrlEvent(ctrlName: string, eventName: string, args: any): void;
    /**
     * @description 搜索表单事件
     * @param {string} eventName
     * @param {*} [args={}]
     * @memberof DEMultiDataViewEngine
     */
    searchFormEvent(eventName: string, args?: any): void;
    /**
     * @description 搜索表单加载完成
     * @param {*} [args={}]
     * @memberof DEMultiDataViewEngine
     */
    onSearchFormLoad(args?: any): void;
    /**
     * @description 搜索表单搜索
     * @param {*} [args={}]
     * @memberof DEMultiDataViewEngine
     */
    onSearchFormSearch(args?: any): void;
    /**
     * @description 搜索表单重置
     * @param {*} [args={}]
     * @memberof DEMultiDataViewEngine
     */
    onSearchFormReset(args?: any): void;
    /**
     * @description 多数据部件事件处理
     * @param {string} eventName
     * @param {*} args
     * @memberof DEMultiDataViewEngine
     */
    MDCtrlEvent(eventName: string, args: any): void;
    /**
     * @description 选中变化
     * @param {IUIDataParam} UIDataParam
     * @memberof DEMultiDataViewEngine
     */
    selectionChange(UIDataParam: IUIDataParam): void;
    /**
     * @description 多数据部件加载完成
     * @param {any[]} args
     * @memberof DEMultiDataViewEngine
     */
    MDCtrlLoad(args: any[]): void;
    /**
     * @description 多数据部件加载完成
     * @param {*} [data={}]
     * @return {*}  {void}
     * @memberof DEMultiDataViewEngine
     */
    MDCtrlBeforeLoad(data?: any): void;
    /**
     * @description 获取多数据部件
     * @return {*}  {*}
     * @memberof DEMultiDataViewEngine
     */
    getMDCtrl(): any;
    /**
     * @description 获取搜索表单部件
     * @return {*}  {*}
     * @memberof DEMultiDataViewEngine
     */
    getSearchForm(): any;
}
