import { ViewEngine } from './view-engine';
/**
 * 实体移动端树导航界面引擎
 *
 * @export
 * @class MobTreeExpViewEngine
 * @extends {ViewEngine}
 */
export declare class MobTreeExpViewEngine extends ViewEngine {
    /**
     * @description 树导航部件
     * @type {*}
     * @memberof GridViewEngine
     */
    protected treeexpbar: any;
    /**
     * @description 引擎初始化
     * @param {*} [options={}]
     * @memberof GridViewEngine
     */
    init(options?: any): void;
    /**
     * @description 多数据部件事件
     * @param {string} ctrlName
     * @param {string} eventName
     * @param {*} args
     * @memberof MobMDViewEngine
     */
    onCtrlEvent(ctrlName: string, eventName: string, args: any): void;
    /**
     * @description 获取多数据部件
     * @returns {*}
     * @memberof MDViewEngine
     */
    getTreeExpBar(): any;
    /**
     * @description 引擎加载
     * @param {*} opts
     * @return {*}  {*}
     * @memberof MobChartExpViewEngine
     */
    load(opts?: any): any;
}
