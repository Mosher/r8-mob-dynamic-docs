import { IPSApplication } from '@ibiz/dynamic-model-api';
import { IUIServiceRegister, IAuthServiceRegister, IUtilServiceRegister, IEntityServiceRegister, ICodeListService, ICounterService, IViewMessageService, ILoadingService, IMsgboxService, IActionService, IViewOpenService, IAppNoticeService, IComponentService, IPluginService, IAppCenterService, IAppActionSheetService, IAppFuncService, IAppThemeService, IApp, IContext, IParam, IHttp, IAppStorageService, IAppAuthService } from '../interface';
/**
 * 应用
 *
 * @export
 * @class AppBase
 */
export declare abstract class AppBase implements IApp {
    /**
     * 应用级上下文
     *
     * @type {any}
     * @memberof AppBase
     */
    context: IContext | null;
    /**
     * 应用本地数据
     *
     * @type {any}
     * @memberof AppBase
     */
    localData: IParam | null;
    /**
     * 权限资源数据
     *
     * @type {any}
     * @memberof AppBase
     */
    authResData: IParam | null;
    /**
     * 是否开启权限认证
     *
     * @type {boolean}
     * @memberof AppBase
     */
    enablepermissionvalid: boolean;
    /**
     * 当前激活语言
     *
     * @type {string}
     * @memberof AppBase
     */
    activeLanguage: string;
    /**
     * 应用级环境数据
     *
     * @type {any}
     * @memberof AppBase
     */
    environment: IParam | null;
    /**
     * 当前运行平台类型
     *
     * @private
     * @type {('ios' | 'android' | 'web')}
     * @memberof AppBase
     */
    platFormType: 'ios' | 'android' | 'dd' | 'wx-mini' | 'wx-pn' | 'desktop' | 'web';
    /**
     * 网络请求对象
     *
     * @private
     * @type {Http}
     * @memberof AppBase
     */
    readonly http: IHttp;
    /**
     * 应用模型数据对象
     *
     * @private
     * @type {any}
     * @memberof AppBase
     */
    modelDataObject: any;
    /**
     * Creates an instance of AppBase.
     *
     * @memberof AppBase
     */
    constructor();
    /**
     * 获取应用激活语言
     *
     * @memberof AppBase
     */
    getActiveLanguage(): string;
    /**
     * 设置应用激活语言
     *
     * @memberof AppBase
     */
    setActiveLanguage(lan: string): void;
    /**
     * 获取当前运行平台类型
     *
     * @return {*}  {('ios' ios应用 | 'android'安卓应用 | 'dd' 钉钉应用 | 'wx-mini' 微信小程序 | 'wx-pn' 微信公众号 | 'desktop' 桌面程序 | 'web' web网页)}
     * @memberof AppBase
     */
    getPlatFormType(): 'ios' | 'android' | 'dd' | 'wx-mini' | 'wx-pn' | 'desktop' | 'web';
    /**
     * 设置当前运行平台类型
     *
     * @memberof AppBase
     */
    setPlatFormType(platFormType: 'ios' | 'android' | 'dd' | 'wx-mini' | 'wx-pn' | 'desktop' | 'web'): void;
    /**
     * 获取应用模型数据对象
     *
     * @public
     * @memberof AppBase
     */
    getModel(): IPSApplication;
    /**
     * 设置应用模型数据对象
     *
     * @public
     * @memberof AppBase
     */
    setModel(opts: IPSApplication | null): void;
    /**
     * 获取应用数据数据对象
     *
     * @public
     * @memberof AppBase
     */
    getContext(): IContext | null;
    /**
     * 设置应用上下文数据对象
     *
     * @public
     * @memberof AppBase
     */
    setContext(opts: IContext | null): void;
    /**
     * 获取应用本地数据对象
     *
     * @public
     * @memberof AppBase
     */
    getLocalData(): IParam | null;
    /**
     * 设置应用本地数据对象
     *
     * @public
     * @memberof AppBase
     */
    setLocalData(opts: IParam | null): void;
    /**
     * 获取应用环境数据对象
     *
     * @public
     * @memberof AppBase
     */
    getEnvironment(): IParam | null;
    /**
     * 设置应用环境数据对象
     *
     * @public
     * @memberof AppBase
     */
    setEnvironment(opts: IParam | null): void;
    /**
     * 获取应用权限数据对象
     *
     * @public
     * @memberof AppBase
     */
    getAuthResData(key: string): IParam | null;
    /**
     * 获取应用权限数据对象
     *
     * @public
     * @memberof AppBase
     */
    setAuthResData(opts: IParam | null): void;
    /**
     * @description 获取是否开启权限认证
     * @param {string} key
     * @return {*}  {(IParam | null)}
     * @memberof AppBase
     */
    getEnablePermissionValid(): boolean;
    /**
     * @description 设置是否开启权限认证
     * @param {(IParam | null)} opts
     * @memberof AppBase
     */
    setEnablePermissionValid(enablepermissionvalid: boolean): void;
    /**
     * 获取应用存储对象
     *
     * @public
     * @memberof AppBase
     */
    getStore(): IAppStorageService;
    /**
     * 设置应用存储对象
     *
     * @public
     * @memberof AppBase
     */
    setStore(opts: IAppStorageService): void;
    /**
     * 初始化应用
     *
     * @public
     * @memberof AppBase
     */
    onInit(opts: IParam): void;
    /**
     * 销毁应用
     *
     * @public
     * @memberof AppBase
     */
    onDestroy(): void;
    /**
     * 系统登录
     *
     * @public
     * @memberof AppBase
     */
    onLogin(data: {
        loginname: string;
        password: string;
    }): Promise<IParam>;
    /**
     * 系统登出
     *
     * @public
     * @memberof AppBase
     */
    onLogout(): Promise<any>;
    /**
     * 清除应用数据
     *
     * @public
     * @memberof AppBase
     */
    onClearAppData(): void;
    /**
     * 检查系统更新
     *
     * @public
     * @memberof AppBase
     */
    onCheckAppUpdate(): void;
    /**
     * 跳转预置视图
     *
     * @public
     * @memberof AppBase
     */
    goPresetView(pageTag: 'login' | '404' | '500'): void;
    /**
     * 获取用户信息
     *
     * @public
     * @memberof AppBase
     */
    getUserInfo(): IParam;
    /**
     * 关于系统
     *
     * @public
     * @memberof AppBase
     */
    getSystemInfo(): IParam;
    /**
     * 获取UI服务
     *
     * @public
     * @memberof AppBase
     */
    getUIService(): IUIServiceRegister;
    /**
     * 获取权限服务
     *
     * @public
     * @memberof AppBase
     */
    getAuthService(): IAuthServiceRegister;
    /**
     * 获取工具服务
     *
     * @public
     * @memberof AppBase
     */
    getUtilService(): IUtilServiceRegister;
    /**
     * 获取实体服务
     *
     * @public
     * @memberof AppBase
     */
    getEntityService(): IEntityServiceRegister;
    /**
     * 获取代码表服务
     *
     * @public
     * @memberof AppBase
     */
    getCodeListService(): ICodeListService;
    /**
     * 获取计数器服务
     *
     * @public
     * @memberof AppBase
     */
    getCounterService(): ICounterService;
    /**
     * 获取视图消息服务
     *
     * @public
     * @memberof AppBase
     */
    getViewMSGService(): IViewMessageService;
    /**
     * 获取loadding服务
     *
     * @public
     * @memberof AppBase
     */
    getLoadingService(): ILoadingService;
    /**
     * 获取消息弹框服务
     *
     * @public
     * @memberof AppBase
     */
    getMsgboxService(): IMsgboxService;
    /**
     * 获取实例化界面行为服务
     *
     * @public
     * @memberof AppBase
     */
    getActionService(): IActionService;
    /**
     * 获取视图打开服务
     *
     * @public
     * @memberof AppBase
     */
    getOpenViewService(): IViewOpenService;
    /**
     * 获取消息提示服务
     *
     * @public
     * @memberof AppBase
     */
    getNoticeService(): IAppNoticeService;
    /**
     * 获取组件服务
     *
     * @public
     * @memberof AppBase
     */
    getComponentService(): IComponentService;
    /**
     * 获取插件服务
     *
     * @public
     * @memberof AppBase
     */
    getPluginService(): IPluginService;
    /**
     * 获取应用中心服务
     *
     * @public
     * @memberof AppBase
     */
    getCenterService(): IAppCenterService;
    /**
     * 获取动作面板服务
     *
     * @public
     * @memberof AppBase
     */
    getActionSheetService(): IAppActionSheetService;
    /**
     * 获取应用中心服务
     *
     * @public
     * @memberof AppBase
     */
    getFuncService(): IAppFuncService;
    /**
     * 获取应用主题服务
     *
     * @public
     * @memberof AppBase
     */
    getAppThemeService(): IAppThemeService;
    /**
     * @description 获取应用级注册
     * @return {*}  {IParam}
     * @memberof AppBase
     */
    getUserRegister(): IParam;
    /**
     * @description 获取语言资源i18n
     * @return {*}  {IParam}
     * @memberof AppBase
     */
    getI18n(): IParam;
    /**
     * @description 获取应用权限服务
     * @return {*}  {IParam}
     * @memberof AppBase
     */
    getAppAuthService(): IAppAuthService;
    isPreviewMode(): boolean;
}
