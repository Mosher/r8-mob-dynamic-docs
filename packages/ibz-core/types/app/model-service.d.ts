import { PSModelServiceImpl } from '@ibiz/dynamic-model-api';
/**
 * 模型服务
 *
 * @export
 * @class AppModelService
 * @extends {PSModelServiceImpl}
 */
export declare class AppModelService extends PSModelServiceImpl {
}
/**
 * 获取模型服务
 *
 */
export declare const GetModelService: any;
