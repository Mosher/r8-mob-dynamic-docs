import { IPSAppDataEntity, IPSAppDEField, IPSAppView, IPSEditor, IPSFlexLayout, IPSFlexLayoutPos, IPSGridLayoutPos, IPSDEGrid, IPSDEGridColumn, IPSDEGridEditItem, IPSDEGridDataItem, IPSAppCodeList } from '@ibiz/dynamic-model-api';
/**
 * 模型辅助类
 *
 * @export
 * @class ModelTool
 */
export declare class ModelTool {
    /**
     * 通过类型查找指定部件
     *
     * @memberof ModelTool
     */
    static findPSControlByType(type: string, controls: Array<any> | null): any;
    /**
     * 通过name查找指定部件
     *
     * @memberof ModelTool
     */
    static findPSControlByName(name: string, controls: Array<any> | null): any;
    /**
     * 通过logicCat获取动态逻辑
     *
     * @param {string} logicCat
     * @memberof IBizFormDetailModel
     */
    static findGroupLogicByLogicCat(logicCat: string, groupLogics: Array<any> | null): any;
    /**
     * 获取视图应用实体codeName
     *
     * @memberof ModelTool
     */
    static getViewAppEntityCodeName(viewInstance: IPSAppView): string;
    /**
     * 获取指定应用实体主键属性
     *
     * @memberof ModelTool
     */
    static getAppEntityKeyField(appEntity: IPSAppDataEntity | null): IPSAppDEField | null | undefined;
    /**
     * 获取指定应用实体主信息属性
     *
     * @memberof ModelTool
     */
    static getAppEntityMajorField(appEntity: IPSAppDataEntity | null): IPSAppDEField | null | undefined;
    /**
     * 获取表单所有的表单项成员
     *
     * @static
     * @param {*} form
     * @returns
     * @memberof ModelTool
     */
    static getAllFormItems(form: any): any[];
    /**
     * 通过name获取表单项成员
     *
     * @static
     * @param {*} form 表单
     * @param {string} name 表单成员name
     * @returns
     * @memberof ModelTool
     */
    static getFormDetailByName(form: any, name: string): any;
    /**
     * 获取表单所有的表单项成员
     *
     * @static
     * @param {*} form
     * @returns
     * @memberof ModelTool
     */
    static getAllFormDetails(form: any): any[];
    /**
     * 解析表单，每一个表单项成员调用一次callback回调
     *
     * @static
     * @param {*} form
     * @param {(item: any) => void} callback
     * @memberof ModelTool
     */
    static parseForm(form: any, callback: (item: any) => void): void;
    /**
     * 递归调用表单项成员
     *
     * @static
     * @param {*} formDetail
     * @param {(item: any) => void} callback
     * @memberof ModelTool
     */
    static parserFormDetail(formDetail: any, callback: (item: any) => void): void;
    /**
     * 获取Ac参数
     *
     * @static
     * @param {IPSEditor} editor 编辑器
     * @memberof ModelTool
     */
    static getAcParams(editor: any): {
        serviceName: string;
        interfaceName: string;
    } | undefined;
    /**
     * 获取自填模式sort排序
     *
     * @static
     * @param {IPSEditor} editor 编辑器
     * @memberof ModelTool
     */
    static getAcSort(editor: any): string | undefined;
    /**
     * 获取编辑器的主键名称
     *
     * @param {IPSEditor} editor
     * @memberof ModelTool
     */
    static getEditorKeyName(editor: any): string | undefined;
    /**
     * 获取编辑器的主信息名称
     *
     * @param {IPSEditor} editor
     * @memberof ModelTool
     */
    static getEditorMajorName(editor: any): string | undefined;
    /**
     * 是否是数值属性
     *
     * @static
     * @param {IPSAppDEField} appDeField 应用实体属性
     * @returns
     * @memberof ModelTool
     */
    static isNumberField(appDeField: IPSAppDEField | null): boolean;
    /**
     * 获取精度
     *
     * @param {*} editor 编辑器
     * @param {IPSAppDEField} appDeField 实体属性
     * @memberof ModelTool
     */
    static getPrecision(editor: IPSEditor, appDeField: IPSAppDEField): any;
    /**
     * 获取导航上下文
     *
     * @static
     * @param {*} currentItem 当前模型对象
     * @memberof ParserTool
     */
    static getNavigateContext(currentItem: any): any;
    /**
     * 获取导航参数
     *
     * @static
     * @param {*} currentItem 当前模型对象
     * @memberof ParserTool
     */
    static getNavigateParams(currentItem: any): any;
    /**
     *  获取layout的css样式
     *
     * @static
     * @param {IPSGridLayoutPos} layoutPos
     * @returns
     * @memberof ModelTool
     */
    static getGridOptionsIn24(layoutPos: IPSGridLayoutPos): {
        xs: {
            span: number;
            offset: number;
        };
        sm: {
            span: number;
            offset: number;
        };
        md: {
            span: number;
            offset: number;
        };
        lg: {
            span: number;
            offset: number;
        };
    } | undefined;
    /**
     * 格式化栅格的列宽,对超出范围值的作出修改或设置默认值
     *
     * @param {*} span
     * @param {string} layout
     * @returns
     * @memberof ModelTool
     */
    static formatColSpan(span: any, layout: string): any;
    /**
     *  获取layout的css样式
     *
     * @static
     * @param {IPSFlexLayout} layout
     * @returns
     * @memberof ModelTool
     */
    static getLayoutCss(layout: IPSFlexLayout): string;
    /**
     * 获取layoutPos的css样式
     *
     * @static
     * @param {(IPSLayoutPos | null)} layoutPos
     * @returns
     * @memberof ModelTool
     */
    static getLayoutPosCss(layoutPos: IPSFlexLayoutPos | null): string;
    /**
     * 根据codeName获取表格列
     *
     * @static
     * @param {(IPSLayoutPos | null)} layoutPos
     * @returns
     * @memberof ModelTool
     */
    static getGridItemByCodeName(codeName: string, gridInstance: IPSDEGrid, mode?: string): IPSDEGridColumn | IPSDEGridEditItem | IPSDEGridDataItem | null;
    /**
     * 获取关系视图数据
     *
     * @static
     * @param {IPSAppView} view
     * @memberof ModelTool
     */
    static loadedAppViewRef(view: IPSAppView): Promise<any[]>;
    /**
     * 获取所有门户部件
     *
     * @static
     * @param {*} container
     * @memberof ModelTool
     */
    static getAllPortlets(container: any): any[];
    /**
     * 解析代码表和vlaue
     *
     * @param {any[]} items 代码表项数据
     * @param {*} value 值
     * @param {IPSAppCodeList} codelist 代码表模型
     * @param {*} _this 引用
     * @return {*}
     * @memberof ModelTool
     */
    static getCodelistValue(items: any[], value: any, codelist: IPSAppCodeList, _this: any): any;
    /**
     * 获取代码项
     *
     * @param {any[]} items 代码表项集合
     * @param {*} value 值
     * @param {IPSAppCodeList} codelist 代码表对象
     * @param {*} _this 引用
     * @return {*}  {*}
     * @memberof GridControlBase
     */
    static getItem(items: any[], value: any, codelist: IPSAppCodeList, _this: any): any;
}
