/**
 * @1900-2100区间内的公历、农历互转
 * @charset UTF-8
 * @Author  Jea杨(JJonline@JJonline.Cn)
 * @Time    2014-7-21
 * @Time    2016-8-13 Fixed 2033hex、Attribution Annals
 * @Time    2016-9-25 Fixed lunar LeapMonth Param Bug
 * @Version 1.0.2
 * @公历转农历：calendar.solar2lunar(1987,11,01); //[you can ignore params of prefix 0]
 * @农历转公历：calendar.lunar2solar(1987,09,10); //[you can ignore params of prefix 0]
 */
export declare class calendar {
    /**
     * 农历1900-2100的润大小信息表
     * @Array Of Property
     * @return Hex
     */
    static lunarInfo: any[];
    /**
     * 公历每个月份的天数普通表
     * @Array Of Property
     * @return Number
     */
    static solarMonth: any[];
    /**
     * 天干地支之天干速查表
     * @Array Of Property trans["甲","乙","丙","丁","戊","己","庚","辛","壬","癸"]
     * @return Cn string
     */
    static Gan: any[];
    /**
     * 天干地支之地支速查表
     * @Array Of Property
     * @trans["子","丑","寅","卯","辰","巳","午","未","申","酉","戌","亥"]
     * @return Cn string
     */
    static Zhi: any[];
    /**
     * 天干地支之地支速查表<=>生肖
     * @Array Of Property
     * @trans["鼠","牛","虎","兔","龙","蛇","马","羊","猴","鸡","狗","猪"]
     * @return Cn string
     */
    static Animals: any[];
    /**
     * 24节气速查表
     * @Array Of Property
     * @trans["小寒","大寒","立春","雨水","惊蛰","春分","清明","谷雨","立夏","小满","芒种","夏至","小暑","大暑","立秋","处暑","白露","秋分","寒露","霜降","立冬","小雪","大雪","冬至"]
     * @return Cn string
     */
    static solarTerm: any[];
    /**
     * 1900-2100各年的24节气日期速查表
     * @Array Of Property
     * @return 0x string For splice
     */
    static sTermInfo: any[];
    /**
     * 数字转中文速查表
     * @Array Of Property
     * @trans ['日','一','二','三','四','五','六','七','八','九','十']
     * @return Cn string
     */
    static nStr1: any[];
    /**
     * 日期转农历称呼速查表
     * @Array Of Property
     * @trans ['初','十','廿','卅']
     * @return Cn string
     */
    static nStr2: any[];
    /**
     * 月份转农历称呼速查表
     * @Array Of Property
     * @trans ['正','一','二','三','四','五','六','七','八','九','十','冬','腊']
     * @return Cn string
     */
    static nStr3: any[];
    /**
     * 返回农历y年一整年的总天数
     * @param lunar Year
     * @return Number
     * @eg:var count = calendar.lYearDays(1987) ;//count=387
     */
    static lYearDays(y: any): number;
    /**
     * 返回农历y年闰月是哪个月；若y年没有闰月 则返回0
     * @param lunar Year
     * @return Number (0-12)
     * @eg:var leapMonth = calendar.leapMonth(1987) ;//leapMonth=6
     */
    static leapMonth(y: any): number;
    /**
     * 返回农历y年闰月的天数 若该年没有闰月则返回0
     * @param lunar Year
     * @return Number (0、29、30)
     * @eg:var leapMonthDay = calendar.leapDays(1987) ;//leapMonthDay=29
     */
    static leapDays(y: any): 0 | 29 | 30;
    /**
     * 返回农历y年m月（非闰月）的总天数，计算m为闰月时的天数请使用leapDays方法
     * @param lunar Year
     * @return Number (-1、29、30)
     * @eg:var MonthDay = calendar.monthDays(1987,9) ;//MonthDay=29
     */
    static monthDays(y: any, m: any): -1 | 29 | 30;
    /**
     * 返回公历(!)y年m月的天数
     * @param solar Year
     * @return Number (-1、28、29、30、31)
     * @eg:var solarMonthDay = calendar.leapDays(1987) ;//solarMonthDay=30
     */
    static solarDays(y: any, m: any): any;
    /**
     * 农历年份转换为干支纪年
     * @param  lYear 农历年的年份数
     * @return Cn string
     */
    static toGanZhiYear(lYear: any): any;
    /**
     * 公历月、日判断所属星座
     * @param  cMonth [description]
     * @param  cDay [description]
     * @return Cn string
     */
    static toAstro(cMonth: any, cDay: any): string;
    /**
     * 传入offset偏移量返回干支
     * @param offset 相对甲子的偏移量
     * @return Cn string
     */
    static toGanZhi(offset: any): any;
    /**
     * 传入公历(!)y年获得该年第n个节气的公历日期
     * @param y公历年(1900-2100)；n二十四节气中的第几个节气(1~24)；从n=1(小寒)算起
     * @return day Number
     * @eg:var _24 = calendar.getTerm(1987,3) ;//_24=4;意即1987年2月4日立春
     */
    static getTerm(y: any, n: any): number;
    /**
     * 传入农历数字月份返回汉语通俗表示法
     * @param lunar month
     * @return Cn string
     * @eg:var cnMonth = calendar.toChinaMonth(12) ;//cnMonth='腊月'
     */
    static toChinaMonth(m: any): any;
    /**
     * 传入农历日期数字返回汉字表示法
     * @param lunar day
     * @return Cn string
     * @eg:var cnDay = calendar.toChinaDay(21) ;//cnMonth='廿一'
     */
    static toChinaDay(d: any): any;
    /**
     * 年份转生肖[!仅能大致转换] => 精确划分生肖分界线是“立春”
     * @param y year
     * @return Cn string
     * @eg:var animal = calendar.getAnimal(1987) ;//animal='兔'
     */
    static getAnimal(y: any): any;
    /**
     * 传入阳历年月日获得详细的公历、农历object信息 <=>JSON
     * @param y  solar year
     * @param m  solar month
     * @param d  solar day
     * @return JSON object
     * @eg:console.log(calendar.solar2lunar(1987,11,01));
     */
    static solar2lunar(y: any, m: any, d: any): -1 | {
        lYear: number;
        lMonth: number;
        lDay: number;
        Animal: any;
        IMonthCn: string;
        IDayCn: any;
        cYear: any;
        cMonth: any;
        cDay: any;
        gzYear: any;
        gzMonth: any;
        gzDay: any;
        isToday: boolean;
        isLeap: boolean;
        nWeek: number;
        ncWeek: string;
        isTerm: boolean;
        Term: any;
        astro: string;
    };
    /**
     * 传入农历年月日以及传入的月份是否闰月获得详细的公历、农历object信息 <=>JSON
     * @param y  lunar year
     * @param m  lunar month
     * @param d  lunar day
     * @param isLeapMonth  lunar month is leap or not.[如果是农历闰月第四个参数赋值true即可]
     * @return JSON object
     * @eg:console.log(calendar.lunar2solar(1987,9,10));
     */
    static lunar2solar(y: any, m: any, d: any, isLeapMonth: any): -1 | {
        lYear: number;
        lMonth: number;
        lDay: number;
        Animal: any;
        IMonthCn: string;
        IDayCn: any;
        cYear: any;
        cMonth: any;
        cDay: any;
        gzYear: any;
        gzMonth: any;
        gzDay: any;
        isToday: boolean;
        isLeap: boolean;
        nWeek: number;
        ncWeek: string;
        isTerm: boolean;
        Term: any;
        astro: string;
    };
}
export declare const defaultLunar: {
    '1-1': string;
    '1-15': string;
    '2-2': string;
    '5-5': string;
    '7-7': string;
    '7-15': string;
    '8-15': string;
    '9-9': string;
    '10-1': string;
    '10-15': string;
    '12-8': string;
    '12-23': string;
};
export declare const defaultGregorian: {
    '1-1': string;
    '2-14': string;
    '3-8': string;
    '3-12': string;
    '5-1': string;
    '5-4': string;
    '6-1': string;
    '7-1': string;
    '8-1': string;
    '9-10': string;
    '10-1': string;
    '12-24': string;
    '12-25': string;
};
export declare const isBrowser = true;
export declare const todayString: string;
