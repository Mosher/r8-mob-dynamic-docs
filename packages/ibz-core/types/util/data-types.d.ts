/**
 * 数据类型
 *
 */
export declare class DataTypes {
    static readonly DataTypes: any;
    /**
     * 是否相等（忽略大小写）
     *
     * @protected
     * @param {string} value
     * @param {string} value2
     * @returns
     * @memberof DataTypes
     */
    protected static equalsIgnoreCase(value: string, value2: string): boolean;
    /**
     * 字符串转数值
     *
     * @param strValue
     * @return
     */
    static fromString(strValue: string): number;
    /**
     * 字符串转数值
     *
     * @param strValue
     * @return
     */
    static isNumber(strValue: string): boolean;
    /**
     * 获取字符串名称
     *
     * @param nDataType
     * @return
     */
    static toString(nDataType: number): string;
}
