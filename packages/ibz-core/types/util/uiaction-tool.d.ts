/**
 * 界面行为工具类
 */
export declare class UIActionTool {
    /**
     * 处理应用上下文参数
     *
     * @param actionTarget 数据目标
     * @param args  传入数据对象
     * @param parentContext 父上下文
     * @param parentParams  父参数
     * @param param 传入应用上下数据参数
     */
    static handleContextParam(actionTarget: any, args: any, parentContext: any, parentParams: any, context: any): any;
    /**
     * 处理界面行为参数
     *
     * @param actionTarget 数据目标
     * @param args  传入数据对象
     * @param parentContext 父上下文
     * @param parentParams  父参数
     * @param param 传入界面行为附加参数
     */
    static handleActionParam(actionTarget: any, args: any, parentContext: any, parentParams: any, params: any): any;
    /**
     * 格式化数据
     *
     * @private
     * @static
     * @param {*} actionTarget
     * @param {*} args
     * @param parentContext
     * @param parentParams
     * @param {*} _params
     * @returns {*}
     * @memberof UIActionTool
     */
    private static formatData;
}
