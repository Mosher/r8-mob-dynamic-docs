/**
 * 平台工具类
 *
 * @export
 * @class Util
 */
export declare class Util {
    /**
     * 创建 UUID
     *
     * @static
     * @returns {string}
     * @memberof Util
     */
    static createUUID(): string;
    /**
     * 判断对象是否为空，避免发生数值0误判
     *
     * @param obj
     */
    static isExist(obj: any): boolean;
    /**
     * 字符串不为空并且对象存在
     *
     * @param str
     */
    static isExistAndNotEmpty(str: string | undefined | null): boolean;
    /**
     * @description 字符串转驼峰
     * @static
     * @param {string} name
     * @return {*}
     * @memberof Util
     */
    static formatCamelCase(name: string): string;
    /**
     * 计算单词复数
     *
     * @static
     * @returns {string}
     * @memberof Util
     */
    static srfpluralize(word: string): string;
    /**
     * 创建序列号
     *
     * @static
     * @returns {number}
     * @memberof Util
     */
    static createSerialNumber(): number;
    /**
     * 判断是否为一个函数
     *
     * @static
     * @param {*} func
     * @returns {boolean}
     * @memberof Util
     */
    static isFunction(func: any): boolean;
    /**
     *
     *
     * @static
     * @param {*} [o={}]
     * @memberof Util
     */
    static processResult(o?: any): void;
    /**
     * 下载文件
     *
     * @static
     * @param {string} url
     * @memberof Util
     */
    static download(url: string): void;
    /**
     *
     *
     * @static
     * @param {any} url
     * @param {any} params
     * @returns {string}
     * @memberof Util
     */
    static parseURL2(url: string, params: any): string;
    /**
     * 是否是数字值(包括可转换的,已排除了空值)
     *
     * @param {*} num
     * @returns {boolean}
     * @memberof Util
     */
    static isNumber(num: any): boolean;
    /**
     * 是否未定义
     *
     * @static
     * @param {*} value
     * @returns {boolean}
     * @memberof Util
     */
    static isUndefined(value: any): boolean;
    /**
     * 是否为空
     *
     * @static
     * @param {*} value
     * @returns {boolean}
     * @memberof Util
     */
    static isEmpty(value: any): boolean;
    /**
     * 是否存在数据(空对象和空数组算无值)
     *
     * @static
     * @param {*} value
     * @returns {boolean}
     * @memberof Util
     */
    static isExistData(value: any): boolean;
    /**
     * 转换为矩阵参数
     *
     * @static
     * @param {*} obj
     * @returns {*}
     * @memberof Util
     */
    static formatMatrixStringify(obj: any): any;
    /**
     * 准备路由参数
     *
     * @static
     * @param {*} { route: route, sourceNode: sourceNode, targetNode: targetNode, data: data }
     * @returns {*}
     * @memberof Util
     */
    static prepareRouteParmas({ route: route, sourceNode: sourceNode, targetNode: targetNode, data: data, }: any): any;
    /**
     * 获取当前值类型
     *
     * @static
     * @param {*} obj
     * @returns
     * @memberof Util
     */
    static typeOf(obj: any): string;
    /**
     * 深拷贝(deepCopy)
     *
     * @static
     * @param {*} data
     * @returns {*}
     * @memberof Util
     */
    static deepCopy(data: any): any;
    /**
     * 名称格式化
     *
     * @static
     * @param {string} name
     * @returns {string}
     * @memberof Util
     */
    static srfFilePath2(name: string): string;
    /**
     * 附加参数格式化
     *
     * @static
     * @param {any} arg 表单数据
     * @param {any} parent 外层context或viewparams
     * @param {any} params 附加参数
     * @returns {any}
     * @memberof Util
     */
    static formatData(arg: any, parent: any, params: any): any;
    /**
     * 格式化导航数据
     *
     * @static
     * @param {any} data 导航模型原生数据
     * @memberof Util
     */
    static formatNavParam(navParam: any, isConvertToLower?: boolean): any;
    /**
     * 格式化视图关系参数
     *
     * @static
     * @param {any} context 应用上下文数据
     * @param {any} appDERSPaths 关系路径数据
     * @memberof Util
     */
    static formatAppDERSPath(context: any, appDERSPaths: any): any;
    /**
     * 计算导航数据
     * 先从当前数据目标计算，然后再从当前上下文计算，最后从当前视图参数计算，没有则为null
     *
     * @static
     * @param {any} data 表单数据
     * @param {any} parentContext 外层context
     * @param {any} parentParam 外层param
     * @param {any} params 附加参数
     * @returns {any}
     * @memberof Util
     */
    static computedNavData(data: any, parentContext: any, parentParam: any, params: any): any;
    /**
     * 日期格式化
     *
     * @static
     * @param {string} fmt 格式化字符串
     * @param {any} date 日期对象
     * @returns {string}
     * @memberof Util
     */
    static dateFormat(date: any, fmt?: string): string;
    /**
     * 深度合并对象
     *
     * @param FirstOBJ 目标对象
     * @param SecondOBJ 原对象
     * @returns {Object}
     * @memberof Util
     */
    static deepObjectMerge(FirstOBJ: any, SecondOBJ: any): any;
    /**
     * 表单项校验
     *
     * @param property 表单项属性名
     * @param data 表单数据
     * @param rules 表单值规则
     * @returns {Promise}
     * @memberof Util
     */
    static validateItem(property: string, data: any, rules: any): Promise<void>;
    /**
     * 比较两个对象的属性是否都相等
     *
     * @static
     * @param {*} newVal
     * @param {*} oldVal
     * @returns
     * @memberof Util
     */
    static isFieldsSame(newVal: any, oldVal: any): boolean;
    /**
     * 把context和viewparams转换成视图用的viewdata和viewparam参数
     *
     * @static
     * @param {*} context
     * @param {*} viewparams
     * @returns
     * @memberof Util
     */
    static getViewProps(context: any, viewparams: any, navdatas?: Array<any>): {
        viewdata: string;
        viewparam: string;
        navdatas: any[];
    };
    /**
     * 给定值在数组查找目标元素
     *
     * @static
     * @param {Array<any>} array 目标数组
     * @param {string} field 属性
     * @param {*} value 值
     * @returns
     * @memberof Util
     */
    static findElementByField(array: Array<any>, field: string, value: any): any;
    /**
     * 改变数组的某个元素的位置，其他元素按顺序延后
     *
     * @static
     * @param {*} arr 数组
     * @param {number} oldIndex 改变之前在旧数组中索引
     * @param {number} newIndex 改变之后再新数组中的索引
     * @memberof Util
     */
    static changeIndex(arr: any, oldIndex: number, newIndex: number): void;
    /**
     * 清除附加数据（源数据里面有的数据在目标数据中都清除）
     *
     * @param source 源数据
     * @param target 目标数据
     *
     * @memberof Util
     */
    static clearAdditionalData(source: any, target: any): void;
    /**
     * 是否为空对象（包括值）
     *
     * @param source 源对象
     * @memberof Util
     */
    static isEmptyObject(source: any): boolean;
    /**
     * 计算容器尺寸
     *
     * @param {number} value
     * @return {*}  {string}
     */
    static calcBoxSize(value: number): string;
    /**
     * 上下文替换正则
     *
     * @author chitanda
     * @date 2021-04-23 20:04:01
     * @static
     */
    static contextReg: RegExp;
    /**
     * 数据替换正则
     *
     * @author chitanda
     * @date 2021-04-23 20:04:09
     * @static
     */
    static dataReg: RegExp;
    /**
     * 填充字符串中的数据
     *
     * @author chitanda
     * @date 2021-04-23 20:04:17
     * @static
     * @param {string} str
     * @param {*} [context]
     * @param {*} [data]
     * @return {*}  {string}
     */
    static fillStrData(str: string, context?: any, data?: any): string;
    /**
     * @description 将base64转换为文件
     * @static
     * @param {string} dataurl
     * @param {string} [filename='图片']
     * @return {*}
     * @memberof Util
     */
    static dataURLtoFile(dataurl: string, filename?: string): File;
}
/**
 * 设置sessionStorage数据
 *
 */
export declare const setSessionStorage: Function;
/**
 * 获取sessionStorage数据
 *
 */
export declare const getSessionStorage: Function;
/**
 * 删除sessionStorage数据
 *
 */
export declare const removeSessionStorage: Function;
/**
 * 节流
 *
 * @param {any} fun 函数
 * @param {any} params 参数
 * @param {any} context 方法上下文
 * @param {number} delay 延迟时间，默认300
 * @static
 * @memberof Util
 */
export declare const throttle: Function;
/**
 * 防抖
 *
 * @param {any} fun 函数
 * @param {any} params 参数
 * @param {number} wait 延迟时间，默认300
 * @static
 * @memberof Util
 */
export declare const debounce: Function;
