/**
 * 字符串工具类
 *
 * @author chitanda
 * @date 2021-04-23 20:04:27
 * @export
 * @class StringUtil
 */
export declare class StringUtil {
    /**
     * 上下文替换正则
     *
     * @author chitanda
     * @date 2021-04-23 20:04:01
     * @static
     */
    static contextReg: RegExp;
    /**
     * 数据替换正则
     *
     * @author chitanda
     * @date 2021-04-23 20:04:09
     * @static
     */
    static dataReg: RegExp;
    /**
     * 填充字符串中的数据
     *
     * @author chitanda
     * @date 2021-04-23 20:04:17
     * @static
     * @param {string} str
     * @param {*} [context]
     * @param {*} [data]
     * @return {*}  {string}
     */
    static fillStrData(str: string, context?: any, data?: any): string;
}
