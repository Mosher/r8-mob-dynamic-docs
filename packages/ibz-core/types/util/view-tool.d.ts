import { IUIService } from '../interface';
export declare class ViewTool {
    /**
     * 解析参数，返回viewdata
     *
     * @static
     * @param {any[]} [args] 多项数据
     * @param {*} [viewParam] 视图参数
     * @param {any[]} [deResParameters] 关系实体参数对象
     * @param {any[]} [parameters] 当前应用视图参数对象
     * @param {*} [data] 行为参数
     * @returns
     * @memberof ViewTool
     */
    static getViewdata(viewParam: any, deResParameters: any[], parameters: any[], args: any[], data?: any): any;
    /**
     * 处理路由路径
     *
     * @static
     * @param {*} [context={}] 上下文
     * @param {any[]} deResParameters 关系实体参数对象
     * @param {any[]} parameters 当前应用视图参数对象
     * @param {any[]} args 多项数据
     * @param {*} data 行为参数
     * @param {*} isGlobal 是否为全局
     * @returns {string}
     * @memberof ViewTool
     */
    static buildUpRoutePath(context: any, deResParameters: any[], parameters: any[], args: any[], data: any, isGlobal?: boolean): string;
    /**
     * 获取首页根路由路径
     *
     * @static
     * @param {Route} route 路由对象
     * @returns {string}
     * @param {*} isGlobal 是否为全局
     * @memberof ViewTool
     */
    static getIndexRoutePath(isGlobal?: boolean): string;
    /**
     * 获取关系实体路径
     *
     * @static
     * @param {*} [viewParam={}] 视图参数
     * @param {any[]} deResParameters 关系实体参数对象
     * @param {any[]} args 多项数据
     * @returns {string}
     * @memberof ViewTool
     */
    static getDeResRoutePath(viewParam: any, deResParameters: any[], args: any[]): string;
    /**
     * 当前激活路由路径
     *
     * @static
     * @param {any[]} parameters 当前应用视图参数对象
     * @param {any[]} args 多项数据
     * @param {*} data 行为参数
     * @returns {string}
     * @memberof ViewTool
     */
    static getActiveRoutePath(parameters: any[], args: any[], data: any, viewParam?: any): string;
    /**
     * 格式化路由参数
     *
     * @static
     * @param {*} params
     * @returns {*}
     * @memberof ViewTool
     */
    static formatRouteParams(params: any, route: any, context: any, viewparams: any): void;
    /**
     * 首页路由结构参数
     *
     * @private
     * @static
     * @type {any[]}
     * @memberof ViewTool
     */
    private static indexParameters;
    /**
     * 设置首页路由结构参数
     *
     * @static
     * @param {any[]} parameters
     * @memberof ViewTool
     */
    static setIndexParameters(parameters: any[]): void;
    /**
     * 获取首页路由结构参数
     *
     * @static
     * @returns {any[]}
     * @memberof ViewTool
     */
    static getIndexParameters(): any[];
    /**
     * 首页视图参数
     *
     * @static
     * @type {*}
     * @memberof ViewTool
     */
    static indexViewParam: any;
    /**
     * 设置首页视图参数
     *
     * @static
     * @param {*} [viewParam={}]
     * @memberof ViewTool
     */
    static setIndexViewParam(viewParam?: any): void;
    /**
     * 获取首页视图参数
     *
     * @static
     * @returns {*}
     * @memberof ViewTool
     */
    static getIndexViewParam(): any;
    /**
     * 计算界面行为项权限状态
     *
     * @static
     * @param {*} [data] 传入数据
     * @param {*} [ActionModel] 界面行为模型
     * @param {*} [UIService] 界面行为服务
     * @memberof ViewTool
     */
    static calcActionItemAuthState(data: any, ActionModel: any, UIService: IUIService): any[] | undefined;
    /**
     * 计算界面行为项权限状态（树节点版本）
     *
     * @static
     * @param {*} [data] 传入数据
     * @param {*} [ActionModel] 界面行为模型
     * @param {*} [UIService] 界面行为服务
     * @memberof ViewTool
     */
    static calcTreeActionItemAuthState(data: any, ActionModel: any, UIService: IUIService): any[] | undefined;
    /**
     * 计算重定向上下文
     *
     * @static
     * @param {*} [tempContext] 上下文
     * @param {*} [data] 传入数据
     * @param {*} [redirectAppEntity] 应用实体对象
     * @memberof ViewTool
     */
    static calcRedirectContext(tempContext: any, data: any, redirectAppEntity: any): Promise<void>;
    /**
     * 移动端图标名称解析
     *
     * @param {string} className
     */
    static setIcon(className: string): any;
    /**
     * 打开编辑数据视图
     *
     * @param {any[]} args 数据参数
     * @param {*} actionContext 视图容器对象
     * @param {*} fullargs 全量数据
     * @param {*} params 额外参数
     * @param {*} $event 事件源对象
     * @param {*} xData 数据部件
     */
    static opendata(args: any[], actionContext?: any, fullargs?: any, params?: any, $event?: any, xData?: any): Promise<void>;
    /**
     * 打开新建数据视图
     *
     * @param {any[]} args 数据参数
     * @param {*} actionContext 视图容器对象
     * @param {*} fullargs 全量数据
     * @param {*} params 额外参数
     * @param {*} $event 事件源对象
     * @param {*} xData 数据部件
     */
    static newdata(args: any[], actionContext: any, fullargs?: any, params?: any, $event?: any, xData?: any): Promise<void>;
    /**
     * 打开目标视图
     *
     * @param {*} openView 目标视图模型对象
     * @param {*} tempContext 临时上下文
     * @param {*} data 数据
     * @param {*} xData 数据部件实例
     * @param {*} event 事件源
     * @param {*} deResParameters
     * @param {*} parameters
     * @param {*} args 额外参数
     * @param {Function} callback 回调
     * @memberof MainViewBase
     */
    static openTargtView(openView: any, actionContext: any, tempContext: any, data: any, xData: any, event: any, deResParameters: any, parameters: any, args: any, callback: Function): void;
}
