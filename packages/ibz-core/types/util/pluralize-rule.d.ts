/**
 * 复数变化规则
 *
 *
 */
export declare class PluralizeRule {
    /**
     * 不规则
     *
     * @protected
     * @type {string[]}
     * @memberof PluralizeRule
     */
    protected irregular: Map<string, string>;
    /**
     * 不可数
     *
     * @protected
     * @type {string[]}
     * @memberof PluralizeRule
     */
    protected uncountable: string[];
    /**
     * 初始化pluralizeRule对象
     *
     * @param opts 额外参数
     * @memberof PluralizeRule
     */
    constructor(opts?: any);
    /**
     * 初始化不规则变化
     *
     * @param opts 额外参数
     * @memberof PluralizeRule
     */
    protected initIrregular(): void;
    /**
     * 是否为不可数
     *
     * @param word 单词
     * @returns 返回判断
     * @memberof PluralizeRule
     */
    isUncountable(word: string): boolean;
    /**
     * 不规则变化
     *
     * @param word 单词
     * @returns 返回变化值
     * @memberof PluralizeRule
     */
    irregularChange(word: string): string | undefined;
    /**
     * 规则变化
     *
     * @param word 单词
     * @returns 返回变化值
     * @memberof PluralizeRule
     */
    ruleChange(word: string): string;
}
