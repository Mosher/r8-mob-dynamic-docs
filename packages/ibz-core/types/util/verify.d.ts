/**
 * 校验对象
 *
 * @export
 * @class Verify
 */
export declare class Verify {
    /**
     * 错误提示信息
     *
     * @static
     * @type {string}
     * @memberof Verify
     */
    static errorInfo: string;
    /**
     * 值比较
     *
     * @static
     * @param {*} value
     * @param {*} value2
     * @returns {number}
     * @memberof Verify
     */
    static compare(value: any, value2: any): number;
    /**
     * 字符串比较
     *
     * @static
     * @param {*} value
     * @param {*} value2
     * @returns {number}
     * @memberof Verify
     */
    static compareString(value: any, value2: any): number;
    /**
     * boolean 值比较
     *
     * @static
     * @param {*} value
     * @param {*} value2
     * @returns {number}
     * @memberof Verify
     */
    static compareBoolean(value: any, value2: any): number;
    /**
     * 时间值比较（毫秒数）
     *
     * @static
     * @param {number} value
     * @param {number} value2
     * @returns {number}
     * @memberof Verify
     */
    static compareDate(value: number, value2: number): number;
    /**
     * 是否是时间
     *
     * @static
     * @param {string} value
     * @returns {boolean}
     * @memberof Verify
     */
    static isParseDate(value: string): boolean;
    /**
     * 数值比较
     *
     * @static
     * @param {number} value
     * @param {number} value2
     * @returns {number}
     * @memberof Verify
     */
    static compareNumber(value: number, value2: number): number;
    /**
     * 文本包含
     *
     * @static
     * @param {*} value
     * @param {*} value2
     * @returns {boolean}
     * @memberof Verify
     */
    static contains(value: any, value2: any): boolean;
    /**
     * 比较值
     *
     * @static
     * @param {*} value
     * @param {*} op
     * @param {*} value2
     * @returns {boolean}
     * @memberof Verify
     */
    static testCond(value: any, op: any, value2: any): boolean;
    /**
     * 检查属性常规条件
     *
     * @static
     * @param {*} value 属性值
     * @param {string} op 检测条件
     * @param {*} value2 预定义值
     * @param {string} errorInfo 错误信息
     * @param {string} paramType 参数类型
     * @param {*} form 表单对象
     * @param {boolean} primaryModel 是否必须条件
     * @returns {boolean}
     * @memberof Verify
     */
    static checkFieldSimpleRule(value: any, op: string, value2: any, errorInfo: string, paramType: string, form: any, primaryModel: boolean): boolean;
    /**
     * 检查属性字符长度规则
     *
     * @static
     * @param {*} viewValue
     * @param {number} minLength
     * @param {boolean} indexOfMin
     * @param {number} maxLength
     * @param {boolean} indexOfMax
     * @param {string} errorInfo
     * @param {boolean} primaryModel
     * @returns {boolean}
     * @memberof Verify
     */
    static checkFieldStringLengthRule(viewValue: string, minLength: number, indexOfMin: boolean, maxLength: number, indexOfMax: boolean, errorInfo: string, primaryModel: boolean): boolean;
    /**
     * 检查属性值正则式规则
     *
     * @static
     * @param {string} viewValue 属性值
     * @param {*} strReg 验证正则
     * @param {string} errorInfo 错误信息
     * @param {boolean} primaryModel 是否关键条件
     * @returns {boolean}
     * @memberof Verify
     */
    static checkFieldRegExRule(viewValue: string, strReg: any, errorInfo: string, primaryModel: boolean): boolean;
    /**
     * 检查脚本值规则
     *
     * @static
     * @param {string} value 属性值
     * @param {*} data 数据对象
     * @param {*} scriptCode 脚本内容
     * @param {string} errorInfo 错误信息
     * @param {boolean} primaryModel 是否关键条件
     * @returns {boolean}
     * @memberof Verify
     */
    static checkFieldScriptRule(value: string, data: any, scriptCode: any, errorInfo: string, primaryModel: boolean): {
        isPast: boolean;
        infoMessage: string;
    };
    /**
     * 检查属性值范围规则
     *
     * @static
     * @param {string} viewValue 属性值
     * @param {*} minNumber 最小数值
     * @param {boolean} indexOfMin 是否包含最小数值
     * @param {*} maxNumber 最大数值
     * @param {boolean} indexOfMax 是否包含最大数值
     * @param {string} errorInfo 错误信息
     * @param {boolean} primaryModel 是否关键条件
     * @returns {boolean}
     * @memberof Verify
     */
    static checkFieldValueRangeRule(viewValue: string, minNumber: any, indexOfMin: boolean, maxNumber: any, indexOfMax: boolean, errorInfo: string, primaryModel: boolean): boolean;
    /**
     * 检查属性值系统值范围规则  暂时支持正则表达式
     *
     * @static
     * @param {string} viewValue 属性值
     * @param {*} strReg 正则
     * @param {string} errorInfo  错误信息
     * @param {boolean} primaryModel 是否关键条件
     * @returns {boolean}
     * @memberof Verify
     */
    static checkFieldSysValueRule(viewValue: string, strReg: any, errorInfo: string, primaryModel: boolean): boolean;
    /**
     * 遍历数据并进行逻辑判断，支持&&和||，支持短路
     *
     * @param {any[]} array 数组
     * @param {Function} callback 回调函数
     * @param {string} [operateTag='AND'] 与或操作标识,支持AND、OR
     * @param {boolean} [isReverse=false] 是否取反
     * @returns {boolean}
     * @memberof Verify
     */
    static logicForEach(array: any[], callback: (item: any, index: number) => boolean, operateTag?: string, isReverse?: boolean): boolean;
    /**
     * 校验属性值规则
     *
     * @param {string} name 校验属性值所在字段的名称
     * @param {*} data 数据对象
     * @param {*} condition 规则条件
     * @returns {{ isPast: boolean, infoMessage: string }}
     * @memberof Verify
     */
    static verifyDeRules(name: string, data: any, condition: any): {
        isPast: boolean;
        infoMessage: string;
    };
    /**
     * 构建校验条件
     *
     * @param {*} model 模型数据
     *
     * @memberof Verify
     */
    static buildVerConditions(model: any): any[];
}
