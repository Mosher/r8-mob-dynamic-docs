export declare class LogUtil {
    /**
     * 输出基本信息
     *
     * @static
     * @param {*} args
     * @memberof LogUtil
     */
    static log(...args: any[]): void;
    /**
     * 输出警告信息
     *
     * @static
     * @param {*} args
     * @memberof LogUtil
     */
    static warn(...args: any[]): void;
    /**
     * 输出错误信息
     *
     * @static
     * @param {*} args
     * @memberof LogUtil
     */
    static error(...args: any[]): void;
}
