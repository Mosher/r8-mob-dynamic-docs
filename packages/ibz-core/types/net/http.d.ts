import { IHttp } from '../interface/utils';
/**
 * Http net 对象
 * 调用 getInstance() 获取实例
 *
 * @class Http
 */
export declare class Http implements IHttp {
    /**
     * 单例变量声明
     *
     * @private
     * @static
     * @type {Http}
     * @memberof Http
     */
    private static Http;
    /**
     * 获取 Http 单例对象
     *
     * @static
     * @returns {Http}
     * @memberof Http
     */
    static getInstance(): IHttp;
    /**
     * 网络请求对象
     *
     * @memberof Http
     */
    static getHttp(): import("axios").AxiosStatic;
    /**
     * Creates an instance of Http.
     * 私有构造，拒绝通过 new 创建对象
     *
     * @memberof Http
     */
    private constructor();
    /**
     * post请求
     *
     * @param {string} url
     * @param {*} [params={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Http
     */
    post(url: string, params?: any, isloading?: boolean): Promise<any>;
    /**
     * 获取
     *
     * @param {string} url
     * @param {*} [params={}]
     * @param {boolean} [isloading]
     * @param {number} [serialnumber]
     * @returns {Promise<any>}
     * @memberof Http
     */
    get(url: string, params?: any, isloading?: boolean): Promise<any>;
    /**
     * 获取模型
     *
     * @param {string} url
     * @param {*} [headers={}]
     * @returns {Promise<any>}
     * @memberof Http
     */
    getModel(url: string, headers?: any): Promise<any>;
    /**
     * 删除
     *
     * @param {string} url
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof Http
     */
    delete(url: string, params?: any, isloading?: boolean): Promise<any>;
    /**
     * 修改数据
     *
     * @param {string} url
     * @param {*} data
     * @param {boolean} [isloading]
     * @param {number} [serialnumber]
     * @returns {Promise<any>}
     * @memberof Http
     */
    put(url: string, params: any, isloading?: boolean): Promise<any>;
    /**
     * 处理响应结果
     *
     * @private
     * @param {*} response
     * @param {Function} fn
     * @param {boolean} [isloading]
     * @memberof Http
     */
    private doResponseResult;
    /**
     * 处理请求数据
     *
     * @private
     * @param data
     * @memberof Http
     */
    private handleRequestData;
    /**
     * 处理系统级数据（以srf开始的字段）
     *
     * @private
     * @param url 原始路径
     * @param params 原始参数
     * @memberof Http
     */
    private handleAppParam;
}
