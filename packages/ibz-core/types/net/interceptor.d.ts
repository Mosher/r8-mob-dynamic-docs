/**
 * 拦截器
 *
 * @export
 * @class InterceptorsBase
 */
export declare abstract class InterceptorsBase {
    /**
     * 拦截器实现接口
     *
     * @private
     * @memberof InterceptorsBase
     */
    intercept(): void;
}
