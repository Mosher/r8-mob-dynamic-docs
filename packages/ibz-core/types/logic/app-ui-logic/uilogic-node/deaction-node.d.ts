import { IPSDEUIDEActionLogic } from '@ibiz/dynamic-model-api';
import { UIActionContext } from '../uiaction-context';
import { AppUILogicNodeBase } from './logic-node-base';
/**
 * 实体行为调用节点
 *
 * @export
 * @class AppUILogicDeactionNode
 */
export declare class AppUILogicDeactionNode extends AppUILogicNodeBase {
    constructor();
    /**
     * 执行节点
     *
     * @param {IPSDEUIDEActionLogic} logicNode 逻辑节点模型数据
     * @param {UIActionContext} actionContext 界面逻辑上下文
     * @memberof AppUILogicDeactionNode
     */
    executeNode(logicNode: IPSDEUIDEActionLogic, actionContext: UIActionContext): Promise<any>;
}
