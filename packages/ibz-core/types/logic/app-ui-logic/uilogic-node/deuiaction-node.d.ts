import { IPSDEUIActionLogic } from '@ibiz/dynamic-model-api';
import { UIActionContext } from '../uiaction-context';
import { AppUILogicNodeBase } from './logic-node-base';
/**
 * 实体界面行为调用节点
 *
 * @export
 * @class AppUILogicDeUIActionNode
 */
export declare class AppUILogicDeUIActionNode extends AppUILogicNodeBase {
    constructor();
    /**
     * 执行节点
     *
     * @param {IPSDEUIActionLogic} logicNode 逻辑节点模型数据
     * @param {UIActionContext} actionContext 界面逻辑上下文
     * @memberof AppUILogicDeUIActionNode
     */
    executeNode(logicNode: IPSDEUIActionLogic, actionContext: UIActionContext): Promise<void>;
}
