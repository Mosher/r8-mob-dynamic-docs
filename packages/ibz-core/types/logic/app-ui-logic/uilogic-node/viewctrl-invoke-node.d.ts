import { IPSDEUICtrlInvokeLogic } from '@ibiz/dynamic-model-api';
import { UIActionContext } from '../uiaction-context';
import { AppUILogicNodeBase } from './logic-node-base';
/**
 * 视图部件调用节点
 *
 * @export
 * @class AppUILogicViewctrlInvokeNode
 */
export declare class AppUILogicViewctrlInvokeNode extends AppUILogicNodeBase {
    constructor();
    /**
     * 执行节点
     *
     * @param {IPSDEUICtrlInvokeLogic} logicNode 逻辑节点模型数据
     * @param {UIActionContext} actionContext 界面逻辑上下文
     * @memberof AppUILogicViewctrlInvokeNode
     */
    executeNode(logicNode: IPSDEUICtrlInvokeLogic, actionContext: UIActionContext): Promise<any>;
}
