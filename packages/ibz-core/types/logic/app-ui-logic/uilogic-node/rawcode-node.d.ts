import { IPSDEUIRawCodeLogic } from '@ibiz/dynamic-model-api';
import { UIActionContext } from '../uiaction-context';
import { AppUILogicNodeBase } from './logic-node-base';
/**
 * 直接前台代码节点
 *
 * @export
 * @class AppUILogicRawCodeNode
 */
export declare class AppUILogicRawCodeNode extends AppUILogicNodeBase {
    constructor();
    /**
     * 执行节点
     *
     * @param {IPSDEUIRawCodeLogic} logicNode 逻辑节点模型数据
     * @param {UIActionContext} actionContext 界面逻辑上下文
     * @memberof AppUILogicRawCodeNode
     */
    executeNode(logicNode: IPSDEUIRawCodeLogic, actionContext: UIActionContext): Promise<any>;
}
