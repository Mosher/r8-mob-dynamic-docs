import { IPSDEUIMsgBoxLogic } from '@ibiz/dynamic-model-api';
import { UIActionContext } from '../uiaction-context';
import { AppUILogicNodeBase } from './logic-node-base';
/**
 * 消息弹窗节点
 *
 * @export
 * @class AppUILogicMsgboxNode
 */
export declare class AppUILogicMsgboxNode extends AppUILogicNodeBase {
    constructor();
    /**
     * 执行节点
     *
     * @param {IPSDEUIMsgBoxLogic} logicNode 逻辑节点模型数据
     * @param {UIActionContext} actionContext 界面逻辑上下文
     * @memberof AppUILogicMsgboxNode
     */
    executeNode(logicNode: IPSDEUIMsgBoxLogic, actionContext: UIActionContext): Promise<void>;
    /**
     * 处理响应
     *
     * @param {IPSDEUIMsgBoxLogic} logicNode 逻辑节点模型数据
     * @param {UIActionContext} actionContext 界面逻辑上下文
     * @param {string} result 响应结果
     * @memberof AppUILogicMsgboxNode
     */
    handleResponse(logicNode: IPSDEUIMsgBoxLogic, actionContext: UIActionContext, options: any, result: string): any;
}
