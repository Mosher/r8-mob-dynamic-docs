import { IPSDEUILogicNode, IPSDEUILogicNodeParam } from '@ibiz/dynamic-model-api';
import { UIActionContext } from '../uiaction-context';
import { AppUILogicNodeBase } from './logic-node-base';
/**
 * 准备处理参数节点
 *
 * @export
 * @class AppUILogicPrepareParamNode
 */
export declare class AppUILogicPrepareParamNode extends AppUILogicNodeBase {
    constructor();
    /**
     * 执行节点
     *
     * @param {IPSDEUILogicNode} logicNode 逻辑节点模型数据
     * @param {UIActionContext} actionContext 界面逻辑上下文
     * @memberof AppUILogicPrepareParamNode
     */
    executeNode(logicNode: IPSDEUILogicNode, actionContext: UIActionContext): Promise<any>;
    /**
     * 设置参数(根据配置把源逻辑参数的值赋给目标逻辑参数)
     *
     * @param {IPSDEUILogicNode} logicNode 逻辑节点模型数据
     * @param {UIActionContext} actionContext 界面逻辑上下文
     * @memberof AppUILogicPrepareParamNode
     */
    setParam(logicNode: IPSDEUILogicNode, actionContext: UIActionContext): void;
    /**
     * 计算目标值
     *
     * @param {IPSDEUILogicNodeParam} nodeParam 节点参数
     * @param {*} srcParam  源数据
     * @param {string} srcFieldName  源属性
     * @param {ActionContext} actionContext  逻辑上下文
     * @memberof AppUILogicPrepareParamNode
     */
    computeTargetParam(nodeParam: IPSDEUILogicNodeParam, srcParam: any, srcFieldName: string, actionContext: UIActionContext): any;
    /**
     * 计算表达式值
     *
     * @param {IPSDEUILogicNodeParam} nodeParam 节点参数
     * @param {ActionContext} actionContext  逻辑上下文
     * @memberof AppUILogicPrepareParamNode
     */
    computeExpRessionValue(nodeParam: any, actionContext: UIActionContext): any;
    /**
     * 解析表达式
     *
     * @param {string} expression 表达式
     * @memberof AppUILogicPrepareParamNode
     */
    translateExpression(expression: string): string;
}
