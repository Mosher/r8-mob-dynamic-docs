import { IPSDEUILogicLinkCond } from '@ibiz/dynamic-model-api';
import { UIActionContext } from '../uiaction-context';
/**
 * 处理逻辑节点基类
 *
 * @export
 * @class AppUILogicNodeBase
 */
export declare class AppUILogicNodeBase {
    constructor();
    /**
     * 根据处理连接计算后续逻辑节点
     *
     * @param {*} logicNode 逻辑节点模型数据
     * @param {UIActionContext} actionContext 界面逻辑上下文
     * @memberof AppUILogicNodeBase
     */
    computeNextNodes(logicNode: any, actionContext: UIActionContext): any;
    /**
     * 计算是否通过逻辑连接
     *
     * @param {IPSDEUILogicLinkCond} logicLinkCond 逻辑节点连接条件
     * @param {UIActionContext} actionContext 界面逻辑上下文
     *
     * @memberof AppUILogicNodeBase
     */
    computeCond(logicLinkCond: IPSDEUILogicLinkCond, actionContext: UIActionContext): any;
}
