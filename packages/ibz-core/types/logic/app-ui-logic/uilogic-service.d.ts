import { IUIDataParam, IUIEnvironmentParam, IUIUtilParam } from '../../interface';
import { UIActionContext } from './uiaction-context';
/**
 * 界面逻辑执行对象
 *
 * @export
 * @class AppUILogicService
 */
export declare class AppUILogicService {
    /**
     * 唯一实例
     *
     * @private
     * @static
     * @memberof AppUILogicService
     */
    private static readonly instance;
    /**
     * 获取唯一实例
     *
     * @static
     * @return {*}  {AppUILogicService}
     * @memberof AppUILogicService
     */
    static getInstance(): AppUILogicService;
    /**
     * 执行之前的初始化操作
     *
     * @param {IPSAppDELogic} logic 处理逻辑模型对象
     * @param UIDataParam 操作数据参数
     * @param UIEnvironmentParam 操作环境参数
     * @param UIUtilParam 操作工具参数
     * @memberof AppUILogicService
     */
    beforeExecute(logic: any, UIDataParam: IUIDataParam, UIEnvironmentParam: IUIEnvironmentParam, UIUtilParam: IUIUtilParam): Promise<UIActionContext>;
    /**
     * 执行处理逻辑
     *
     * @param {*} logic 处理逻辑模型对象
     * @param UIDataParam 操作数据参数
     * @param UIEnvironmentParam 操作环境参数
     * @param UIUtilParam 操作工具参数
     * @memberof AppUILogicService
     */
    onExecute(logic: any, UIDataParam: IUIDataParam, UIEnvironmentParam: IUIEnvironmentParam, UIUtilParam: IUIUtilParam): Promise<void>;
    /**
     * 执行界面处理逻辑节点
     *
     * @param {*} logicNode 界面处理逻辑节点
     * @param {UIActionContext} actionContext 界面逻辑执行上下文
     * @memberof AppUILogicService
     */
    executeNode(logicNode: any, actionContext: UIActionContext): Promise<void>;
    /**
     * 执行后续节点集合
     *
     * @param {*} logicNode 界面处理逻辑节点
     * @param {UIActionContext} actionContext 界面逻辑执行上下文
     * @memberof AppUILogicService
     */
    executeNextNodes(nextNodes: any[], actionContext: UIActionContext): Promise<any>;
}
