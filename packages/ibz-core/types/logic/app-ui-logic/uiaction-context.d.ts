import { IParam, IUIDataParam, IUIEnvironmentParam, IUIUtilParam } from '../../interface';
/**
 * 界面处理逻辑上下文参数对象
 *
 * @export
 * @class UIActionContext
 */
export declare class UIActionContext {
    /**
     * 应用上下文
     *
     * @type {IContext}
     * @memberof UIActionContext
     */
    private appContext;
    /**
     * 视图参数
     *
     * @type {IParam}
     * @memberof UIActionContext
     */
    private viewParam;
    /**
     * 数据对象
     *
     * @type {*}
     * @memberof UIActionContext
     */
    data: any;
    /**
     * 其他附加参数
     *
     * @type {*}
     * @memberof UIActionContext
     */
    otherParams: any;
    /**
     * 逻辑处理参数集合
     *
     * @type {Map<string, any>}
     * @memberof UIActionContext
     */
    paramsMap: Map<string, any>;
    /**
     * 默认逻辑处理参数名称
     *
     * @type {string}
     * @memberof UIActionContext
     */
    defaultParamName: string;
    /**
     * 逻辑局部应用上下文参数名称
     *
     * @type {string}
     * @memberof UIActionContext
     */
    navContextParamName: string;
    /**
     * 逻辑局部视图参数名称
     *
     * @type {string}
     * @memberof UIActionContext
     */
    navViewParamParamName: string;
    /**
     * 默认逻辑处理参数
     *
     * @readonly
     * @memberof UIActionContext
     */
    get defaultParam(): any;
    /**
     * 逻辑上下文参数
     *
     * @readonly
     * @memberof UIActionContext
     */
    get navContextParam(): any;
    /**
     * 逻辑视图参数
     *
     * @readonly
     * @memberof UIActionContext
     */
    get navViewParamParam(): any;
    /**
     * 上下文数据（包括应用上下文和逻辑局部上下文参数）
     *
     * @readonly
     * @memberof UIActionContext
     */
    get context(): any;
    /**
     * 视图参数数据（包括外部传入视图参数和逻辑局部视图参数）
     *
     * @readonly
     * @memberof UIActionContext
     */
    get viewparams(): any;
    /**
     * 获取逻辑处理参数
     *
     * @param {string} key 逻辑处理参数的codeName
     * @memberof UIActionContext
     */
    getParam(key: string): any;
    /**
     * 构造函数
     *
     * @param {*} logic 处理逻辑模型对象
     * @param {any[]} args 数据对象
     * @param {*} context 应用上下文
     * @param {*} params 视图参数
     * @param {*} $event 事件源对象
     * @param {*} xData 部件对象
     * @param {*} actioncontext 界面容器对象
     * @param {*} srfParentDeName 关联父应用实体代码名称
     * @memberof UIActionContext
     */
    constructor(logic: any, UIDataParam: IUIDataParam, UIEnvironmentParam: IUIEnvironmentParam, UIUtilParam: IUIUtilParam);
    /**
     * 执行界面逻辑之前（准备参数）
     *
     * @param UIDataParam 操作数据参数
     * @param UIEnvironmentParam 操作环境参数
     * @param UIUtilParam 操作工具参数
     *
     * @memberof UIActionContext
     */
    beforeExecute(UIDataParam: IUIDataParam, UIEnvironmentParam: IUIEnvironmentParam, UIUtilParam: IUIUtilParam): {
        args: any[];
        actionContext: IParam;
        event: IParam | undefined;
        xData: IParam | null;
    };
}
