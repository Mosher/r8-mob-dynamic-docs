import { IPSDERawCodeLogic } from '@ibiz/dynamic-model-api';
import { ActionContext } from '../action-context';
import { AppDeLogicNodeBase } from './logic-node-base';
/**
 * 直接代码节点
 *
 * @export
 * @class AppRawsfcodeNode
 */
export declare class AppRawsfcodeNode extends AppDeLogicNodeBase {
    constructor();
    /**
     * 执行节点
     *
     * @static
     * @param {IPSDELogicNode} logicNode 逻辑节点
     * @param {ActionContext} actionContext 逻辑上下文
     * @memberof AppRawsfcodeNode
     */
    executeNode(logicNode: IPSDERawCodeLogic, actionContext: ActionContext): Promise<any>;
}
