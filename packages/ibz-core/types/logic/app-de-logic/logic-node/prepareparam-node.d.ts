import { IPSDELogicNode, IPSDELogicNodeParam } from '@ibiz/dynamic-model-api';
import { ActionContext } from '../action-context';
import { AppDeLogicNodeBase } from './logic-node-base';
/**
 * 准备参数节点
 *
 * @export
 * @class AppDeLogicPrepareParamNode
 */
export declare class AppDeLogicPrepareParamNode extends AppDeLogicNodeBase {
    constructor();
    /**
     * 执行节点
     *
     * @static
     * @param {IPSDELogicNode} logicNode 逻辑节点
     * @param {ActionContext} actionContext 逻辑上下文
     * @memberof AppDeLogicPrepareParamNode
     */
    executeNode(logicNode: IPSDELogicNode, actionContext: ActionContext): Promise<any>;
    /**
     * 设置参数(根据配置把源逻辑参数的值赋给目标逻辑参数)
     *
     * @param {IPSDELogicNode} logicNode 节点模型数据
     * @param {ActionContext} actionContext  逻辑上下文
     * @memberof AppDeLogicPrepareParamNode
     */
    setParam(logicNode: IPSDELogicNode, actionContext: ActionContext): void;
    /**
     * 计算目标值
     *
     * @param {IPSDELogicNodeParam} nodeParam 节点参数
     * @param {*} srcParam  源数据
     * @param {string} srcFieldName  源属性
     * @param {ActionContext} actionContext  逻辑上下文
     * @memberof AppDeLogicPrepareParamNode
     */
    computeTargetParam(nodeParam: IPSDELogicNodeParam, srcParam: any, srcFieldName: string, actionContext: ActionContext): any;
    /**
     * 计算表达式值
     *
     * @param {IPSDELogicNodeParam} nodeParam 节点参数
     * @param {ActionContext} actionContext  逻辑上下文
     * @memberof AppDeLogicPrepareParamNode
     */
    computeExpRessionValue(nodeParam: IPSDELogicNodeParam, actionContext: ActionContext): any;
    /**
     * 解析表达式
     *
     * @param {string} expression 表达式
     * @memberof AppDeLogicPrepareParamNode
     */
    translateExpression(expression: string): string;
}
