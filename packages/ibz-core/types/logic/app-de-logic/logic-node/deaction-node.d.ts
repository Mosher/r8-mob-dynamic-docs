import { IPSDEDEActionLogic } from '@ibiz/dynamic-model-api';
import { AppDeLogicNodeBase } from './logic-node-base';
import { ActionContext } from '../action-context';
/**
 * 实体行为处理节点
 *
 * @export
 * @class AppDeLogicDeActionNode
 */
export declare class AppDeLogicDeActionNode extends AppDeLogicNodeBase {
    constructor();
    /**
     * 执行节点
     *
     * @static
     * @param {IPSDELogicNode} logicNode 逻辑节点
     * @param {ActionContext} actionContext 逻辑上下文
     * @memberof AppDeLogicDeActionNode
     */
    executeNode(logicNode: IPSDEDEActionLogic, actionContext: ActionContext): Promise<any>;
}
