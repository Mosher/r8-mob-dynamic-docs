import { IPSDELogicNode } from '@ibiz/dynamic-model-api';
import { ActionContext } from '../action-context';
import { AppDeLogicNodeBase } from './logic-node-base';
/**
 * 开始类型节点
 *
 * @export
 * @class AppDeLogicBeginNode
 */
export declare class AppDeLogicBeginNode extends AppDeLogicNodeBase {
    constructor();
    /**
     * 执行节点
     *
     * @static
     * @param {IPSDELogicNode} logicNode 逻辑节点
     * @param {ActionContext} actionContext 逻辑上下文
     * @memberof AppDeLogicBeginNode
     */
    executeNode(logicNode: IPSDELogicNode, actionContext: ActionContext): Promise<any>;
}
