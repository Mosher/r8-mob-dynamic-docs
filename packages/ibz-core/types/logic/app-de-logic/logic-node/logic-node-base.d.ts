import { IPSDELogicLinkCond, IPSDELogicNode } from '@ibiz/dynamic-model-api';
import { ActionContext } from '../action-context';
/**
 * 处理逻辑节点基类
 *
 * @export
 * @class AppDeLogicNodeBase
 */
export declare class AppDeLogicNodeBase {
    constructor();
    /**
     * 根据处理连接计算后续逻辑节点
     *
     * @param {IPSDELogicNode} logicNode 处理逻辑节点
     * @param {IContext} context
     * @memberof AppDeLogicNodeBase
     */
    computeNextNodes(logicNode: IPSDELogicNode, actionContext: ActionContext): any;
    /**
     * 计算是否通过逻辑连接
     *
     * @param {IPSDELogicLinkCond} logicLinkCond
     * @return {*}
     * @memberof AppDeLogicNodeBase
     */
    computeCond(logicLinkCond: IPSDELogicLinkCond, actionContext: ActionContext): any;
}
