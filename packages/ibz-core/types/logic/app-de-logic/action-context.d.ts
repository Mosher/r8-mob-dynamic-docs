import { IPSAppDELogic } from '@ibiz/dynamic-model-api';
import { IContext, IParam } from '../../interface';
/**
 * 实体处理逻辑上下文参数对象
 *
 * @export
 * @class ActionContext
 */
export declare class ActionContext {
    /**
     * 实体行为服务context
     *
     * @type {IContext}
     * @memberof ActionContext
     */
    context: IContext;
    /**
     * 逻辑处理参数集合
     *
     * @type {Map<string, any>}
     * @memberof ActionContext
     */
    paramsMap: Map<string, any>;
    /**
     * 默认逻辑处理参数名称
     *
     * @type {string}
     * @memberof ActionContext
     */
    defaultParamName: string;
    /**
     * 默认逻辑处理参数
     *
     * @readonly
     * @memberof ActionContext
     */
    get defaultParam(): any;
    /**
     * 获取逻辑处理参数
     *
     * @param {string} key 逻辑处理参数的codeName
     * @memberof ActionContext
     */
    getParam(key: string): any;
    /**
     * 构造函数
     *
     * @param {IPSAppDELogic} logic 处理逻辑模型对象
     * @param {IContext} context 实体行为服务context
     * @param {IParam} params 实体行为服务data
     * @memberof ActionContext
     */
    constructor(logic: IPSAppDELogic, context: IContext, params: IParam);
}
