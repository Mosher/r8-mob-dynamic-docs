import { IPSAppDELogic, IPSDELogicNode } from '@ibiz/dynamic-model-api';
import { IContext, IParam } from '../../interface';
import { ActionContext } from './action-context';
/**
 * 实体处理逻辑执行对象
 *
 * @export
 * @class AppDeLogicService
 */
export declare class AppDeLogicService {
    /**
     * 唯一实例
     *
     * @private
     * @static
     * @memberof AppDeLogicService
     */
    private static readonly instance;
    /**
     * 获取唯一实例
     *
     * @static
     * @return {*}  {AppDeLogicService}
     * @memberof AppDeLogicService
     */
    static getInstance(): AppDeLogicService;
    /**
     * 执行之前的初始化操作
     *
     * @param {IPSAppDELogic} logic 处理逻辑模型对象
     * @param {IParams} params
     * @memberof AppDeLogicService
     */
    beforeExecute(logic: IPSAppDELogic, context: IContext, params: IParam): Promise<ActionContext>;
    /**
     * 执行处理逻辑
     *
     * @param {IPSAppDELogic} logic 处理逻辑模型对象
     * @param {IContext} context 应用上下文参数
     * @param {IParams} data 数据参数
     * @return {*}
     * @memberof AppDeLogicService
     */
    onExecute(logic: IPSAppDELogic, context: IContext, data: IParam): Promise<any>;
    /**
     * 执行处理逻辑节点
     *
     * @param {*} logicNode 处理逻辑节点
     * @param {IContext} context
     * @memberof AppDeLogicService
     */
    executeNode(logicNode: any, actionContext: ActionContext): Promise<any>;
    /**
     * 执行后续节点集合
     *
     * @param {IPSDELogicNode[]} nextNodes
     * @param {ActionContext} ActionContext
     * @memberof AppDeLogicService
     */
    executeNextNodes(nextNodes: IPSDELogicNode[], actionContext: ActionContext): Promise<any>;
}
