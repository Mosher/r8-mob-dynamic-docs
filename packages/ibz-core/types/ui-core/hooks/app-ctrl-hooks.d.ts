import { IAppCtrlHooks, ICtrlEventParam, IParam } from '../../interface';
import { SyncSeriesHook } from 'qx-util';
/**
 * 部件hooks基类
 *
 * @export
 * @class AppCtrlHooks
 */
export declare class AppCtrlHooks implements IAppCtrlHooks {
    /**
     * 模型数据加载完成钩子
     *
     * @class AppCtrlHooks
     */
    modelLoaded: SyncSeriesHook<[], {
        arg: IParam;
    }>;
    /**
     * 绑定事件钩子
     *
     * @interface AppCtrlHooks
     */
    onEvent: SyncSeriesHook<[], {
        event: string | string[];
        fn: Function;
    }>;
    /**
     * 解绑事件钩子
     *
     * @interface AppCtrlHooks
     */
    offEvent: SyncSeriesHook<[], {
        event: string | string[];
        fn?: Function;
    }>;
    /**
     * 部件事件钩子
     *
     * @class AppCtrlHooks
     */
    event: SyncSeriesHook<[], ICtrlEventParam>;
    /**
     * 关闭视图钩子
     *
     * @class AppCtrlHooks
     */
    closeView: SyncSeriesHook<[], {
        arg: IParam;
    }>;
    /**
     * 部件挂载之后钩子
     *
     * @class AppCtrlHooks
     */
    mounted: SyncSeriesHook<[], {
        arg: IParam;
    }>;
}
