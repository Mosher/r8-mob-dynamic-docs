import { IAppEditorHooks, IParam } from '../../interface';
import { SyncSeriesHook } from 'qx-util';
/**
 * 编辑器hooks基类
 *
 * @export
 * @class AppEditorHooks
 */
export declare class AppEditorHooks implements IAppEditorHooks {
    /**
     * 模型数据加载完成钩子
     *
     * @class AppEditorHooks
     */
    modelLoaded: SyncSeriesHook<[], {
        arg: IParam;
    }>;
    /**
     * 编辑器值变更事件
     *
     * @class AppEditorHooks
     */
    valueChange: SyncSeriesHook<[], {
        arg: IParam;
    }>;
}
