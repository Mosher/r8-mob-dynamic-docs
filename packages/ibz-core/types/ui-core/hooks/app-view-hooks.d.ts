import { IAppViewHooks, IParam } from '../../interface';
import { SyncSeriesHook } from 'qx-util';
/**
 * 视图hooks基类
 *
 * @export
 * @class AppViewHooks
 */
export declare class AppViewHooks implements IAppViewHooks {
    /**
     * 模型数据加载完成钩子
     *
     * @class AppViewHooks
     */
    modelLoaded: SyncSeriesHook<[], {
        arg: IParam;
    }>;
    /**
     * 绑定事件钩子
     *
     * @interface AppViewHooks
     */
    onEvent: SyncSeriesHook<[], {
        event: string | string[];
        fn: Function;
    }>;
    /**
     * 解绑事件钩子
     *
     * @interface AppViewHooks
     */
    offEvent: SyncSeriesHook<[], {
        event: string | string[];
        fn?: Function;
    }>;
    /**
     * 视图事件钩子
     *
     * @class AppViewHooks
     */
    event: SyncSeriesHook<[], {
        viewName: string;
        action: string;
        data: IParam;
    }>;
    /**
     * 视图刷新钩子
     *
     * @class AppViewHooks
     */
    refresh: SyncSeriesHook<[], {
        arg: IParam;
    }>;
    /**
     * 视图关闭钩子
     *
     * @class AppViewHooks
     */
    closeView: SyncSeriesHook<[], {
        arg: IParam;
    }>;
    /**
     * 视图挂载之后钩子
     *
     * @class AppViewHooks
     */
    mounted: SyncSeriesHook<[], {
        arg: IParam;
    }>;
}
