import { IAppHooks, IParam } from '../../interface';
import { SyncSeriesHook } from 'qx-util';
/**
 * 应用hooks基类
 *
 * @export
 * @class AppHooks
 */
export declare class AppHooks implements IAppHooks {
    /**
     * 模型数据加载完成钩子
     *
     * @class AppHooks
     */
    modelLoaded: SyncSeriesHook<[], {
        arg: IParam;
    }>;
    /**
     * 绑定事件钩子
     *
     * @interface AppHooks
     */
    onEvent: SyncSeriesHook<[], {
        event: string | string[];
        fn: Function;
    }>;
    /**
     * 解绑事件钩子
     *
     * @interface AppHooks
     */
    offEvent: SyncSeriesHook<[], {
        event: string | string[];
        fn?: Function;
    }>;
    /**
     * 请求之前
     *
     * @interface AppHooks
     */
    beforeRequest: SyncSeriesHook<[], {
        arg: IParam;
    }>;
    /**
     * 响应之后
     *
     * @interface AppHooks
     */
    afterResponse: SyncSeriesHook<[], {
        arg: IParam;
    }>;
    /**
     * 鉴权之前
     *
     * @interface AppHooks
     */
    beforeAuth: SyncSeriesHook<[], {
        arg: IParam;
    }>;
    /**
     * 鉴权之后
     *
     * @interface AppHooks
     */
    afterAuth: SyncSeriesHook<[], {
        arg: IParam;
    }>;
    /**
     * 获取应用数据之前
     *
     * @interface AppHooks
     */
    beforeGetAppData: SyncSeriesHook<[], {
        arg: IParam;
    }>;
    /**
     * 获取应用数据之后
     *
     * @interface AppHooks
     */
    afterGetAppData: SyncSeriesHook<[], {
        arg: IParam;
    }>;
    /**
     * 获取租户之前
     *
     * @interface AppHooks
     */
    beforeDcSystem: SyncSeriesHook<[], {
        arg: IParam;
    }>;
    /**
     * 获取租户之后
     *
     * @interface AppHooks
     */
    afterDcSystem: SyncSeriesHook<[], {
        arg: IParam;
    }>;
}
