import { IPSAppFunc } from '@ibiz/dynamic-model-api';
import { AppFuncBase } from './app-func-base';
/**
 * 执行JavaScript
 *
 * @class JavaScriptFunc
 */
export declare class JavaScriptFunc extends AppFuncBase {
    constructor();
    /**
     * 执行功能
     *
     * @param appFunc 应用功能
     * @param context 上下文
     * @param viewParam 视图参数
     * @param container 容器对象
     * @param isGlobal 是否为全局
     * @param arg 额外参数
     * @memberof JavaScriptFunc
     */
    executeFunc(appFunc: IPSAppFunc, context: any, viewParam: any, container: any, isGlobal?: boolean, arg?: any): void;
}
