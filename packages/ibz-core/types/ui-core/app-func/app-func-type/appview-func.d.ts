import { AppFuncBase } from './app-func-base';
import { IPSAppFunc, IPSAppView } from '@ibiz/dynamic-model-api';
/**
 * 打开应用视图
 *
 * @class AppViewFunc
 */
export declare class AppViewFunc extends AppFuncBase {
    constructor();
    /**
     * 参数处理
     *
     * @param context 视图上下文
     * @param appView 应用视图
     * @param deResParameters
     * @param parameters
     */
    handleParams(context: any, appView: IPSAppView, deResParameters: any[], parameters: any[]): void;
    /**
     * 执行功能
     *
     * @param appFunc 应用功能
     * @param context 上下文
     * @param viewParam 视图参数
     * @param container 容器对象
     * @param isGlobal 是否为全局
     * @param arg 额外参数
     * @memberof AppViewFunc
     */
    executeFunc(appFunc: IPSAppFunc, context: any, viewParam: any, container: any, isGlobal?: boolean, arg?: any): string | void;
    /**
     * 打开应用视图
     *
     * @param appView 应用视图
     * @param context 视图上下文
     * @param viewParam 视图参数
     * @param deResParameters
     * @param parameters
     * @param container 容器对象
     * @param isGlobal 是否为全局
     * @param arg 额外参数
     * @memberof AppViewFunc
     */
    openAppView(appView: IPSAppView, context: any, viewParam: any, deResParameters: any[], parameters: any[], container: any, isGlobal?: boolean, arg?: any): string | void;
    /**
     * 用户自定义
     *
     * @param context 上下文
     * @param viewParam 视图参数
     * @param deResParameters
     * @param parameters
     * @param container 容器对象
     * @param isGlobal 是否为全局
     * @param arg 额外参数
     * @memberof AppViewFunc
     */
    openUser(context: any, viewParam: any, deResParameters: any[], parameters: any[], container: any, isGlobal?: boolean, arg?: any): void;
    /**
     * 气泡打开
     *
     * @param context 上下文
     * @param viewParam 视图参数
     * @param appView 应用视图
     * @param container 容器对象
     * @param isGlobal 是否为全局
     * @param arg 额外参数
     * @memberof AppViewFunc
     */
    openPopover(context: any, viewParam: any, appView: IPSAppView, container: any, isGlobal?: boolean, arg?: any): void;
    /**
     * 抽屉打开
     *
     * @param context 上下文
     * @param viewParam 视图参数
     * @param appView 应用视图
     * @param container 容器对象
     * @param isGlobal 是否为全局
     * @param arg 额外参数
     * @memberof AppViewFunc
     */
    openDrawer(context: any, viewParam: any, appView: IPSAppView, container: any, isGlobal?: boolean, arg?: any): void;
    /**
     * 独立程序打开
     *
     * @param context 上下文
     * @param viewParam 视图参数
     * @param deResParameters
     * @param parameters
     * @param container 容器对象
     * @param isGlobal 是否为全局
     * @param arg 额外参数
     * @memberof AppViewFunc
     */
    openApp(context: any, viewParam: any, deResParameters: any[], parameters: any[], container: any, isGlobal?: boolean, arg?: any): void;
    /**
     * 模态打开
     *
     * @param context 上下文
     * @param viewParam 视图参数
     * @param appView 应用视图
     * @param container 容器对象
     * @param isGlobal 是否为全局
     * @param arg 额外参数
     * @memberof AppViewFunc
     */
    openModal(context: any, viewParam: any, appView: IPSAppView, container: any, isGlobal?: boolean, arg?: any): void;
    /**
     * 非模式弹出
     *
     * @param viewParam 视图参数
     * @param deResParameters
     * @param parameters
     * @param container 容器对象
     * @param isGlobal 是否为全局
     * @param arg 额外参数
     * @memberof AppViewFunc
     */
    openPopup(viewParam: any, deResParameters: any[], parameters: any[], container: any, isGlobal?: boolean, arg?: any): void;
    /**
     * 顶级容器分页
     *
     * @param context 上下文
     * @param viewParam 视图参数
     * @param deResParameters
     * @param parameters
     * @param container 容器对象
     * @param isGlobal 是否为全局
     * @param arg 额外参数
     * @memberof AppViewFunc
     */
    openIndexViewTab(context: any, viewParam: any, deResParameters: any[], parameters: any[], container: any, isGlobal?: boolean, arg?: any): string;
}
