/**
 * 应用功能服务
 *
 * @class AppFuncService
 */
export declare class AppFuncService {
    /**
     * 单例变量声明
     *
     * @private
     * @static
     * @memberof AppFuncService
     */
    private static AppFuncService;
    /**
     * 获取 AppFuncService 单例对象
     *
     * @static
     * @return {*}
     * @memberof AppFuncService
     */
    static getInstance(): AppFuncService;
    /**
     * 执行之前
     *
     * @param functionTag 应用功能标识
     * @param context 视图上下文
     * @param viewParam 视图参数
     * @memberof AppFuncService
     */
    private beforeExecute;
    /**
     * 执行应用功能
     *
     * @param functionTag 应用功能标识
     * @param context 视图上下文
     * @param viewParam 视图参数
     * @param container 容器对象
     * @param isGlobalRoute 是否为全局
     * @param arg 额外参数
     * @memberof AppFuncService
     */
    executeAppFunction(functionTag: any, context: any, viewParam: any, container: any, isGlobal?: boolean, arg?: any): Promise<string | void>;
    /**
     * 执行功能
     *
     * @param appFunc 应用功能
     * @param context 视图上下文
     * @param viewParam 视图参数
     * @param container 容器对象
     * @param isGlobalRoute 是否为全局
     * @param arg 额外参数
     * @memberof AppFuncService
     */
    private executeFunc;
}
