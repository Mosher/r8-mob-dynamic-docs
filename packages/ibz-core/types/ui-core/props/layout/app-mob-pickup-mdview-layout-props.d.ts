import { AppLayoutProps } from './app-layout-props';
/**
 * 移动端选择多数据视图视图布局面板传入参数
 *
 * @export
 * @class AppMobPickUpMDViewLayoutProps
 * @extends AppLayoutProps
 */
export declare class AppMobPickUpMDViewLayoutProps extends AppLayoutProps {
}
