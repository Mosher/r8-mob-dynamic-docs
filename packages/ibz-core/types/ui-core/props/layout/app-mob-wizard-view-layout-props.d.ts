import { AppLayoutProps } from './app-layout-props';
/**
 * 移动端向导视图视图布局输入参数
 *
 * @export
 * @class AppMobWizardViewLayoutProps
 */
export declare class AppMobWizardViewLayoutProps extends AppLayoutProps {
}
