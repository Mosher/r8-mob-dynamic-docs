import { IParam } from '../../../interface';
/**
 * 视图布局面板输入参数
 *
 * @export
 * @class AppLayoutProps
 */
export declare class AppLayoutProps {
    viewInstance: IParam;
    navContext: IParam;
    navParam: IParam;
    navDatas: IParam;
    isShowCaptionBar: boolean;
    modelService: IParam;
}
