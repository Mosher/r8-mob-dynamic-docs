import { AppLayoutProps } from "./app-layout-props";
/**
 * 移动端地图导航视图视图布局输入参数
 *
 * @export
 * @class AppMobMapExpViewLayoutProps
 */
export declare class AppMobMapExpViewLayoutProps extends AppLayoutProps {
}
