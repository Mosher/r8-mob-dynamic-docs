import { AppLayoutProps } from './app-layout-props';
/**
 * 移动端动态工作流编辑视图视图布局输入参数
 *
 * @export
 * @class AppMobWFDynaEditViewLayoutProps
 */
export declare class AppMobWFDynaEditViewLayoutProps extends AppLayoutProps {
}
