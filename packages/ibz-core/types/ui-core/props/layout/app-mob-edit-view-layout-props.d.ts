import { AppLayoutProps } from './app-layout-props';
/**
 * 移动端实体编辑视图视图布局面板输入参数
 *
 * @export
 * @class AppMobEditViewLayoutProps
 */
export declare class AppMobEditViewLayoutProps extends AppLayoutProps {
}
