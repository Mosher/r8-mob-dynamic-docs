import { AppLayoutProps } from './app-layout-props';
/**
 * 移动端面板视图布局面板传入参数
 *
 * @export
 * @class AppMobPanelViewLayoutProps
 * @extends AppLayoutProps
 */
export declare class AppMobPanelViewLayoutProps extends AppLayoutProps {
}
