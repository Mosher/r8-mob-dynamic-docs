import { AppLayoutProps } from './app-layout-props';
/**
 * 移动端选项操作视图视图布局输入参数
 *
 * @export
 * @class AppMobOptViewLayoutProps
 */
export declare class AppMobOptViewLayoutProps extends AppLayoutProps {
}
