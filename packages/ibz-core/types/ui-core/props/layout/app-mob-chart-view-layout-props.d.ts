import { AppLayoutProps } from './app-layout-props';
import { IAppMobChartViewLayoutProps } from '../../../interface';
/**
 * 移动端图表视图 视图布局面板传入参数
 *
 * @export
 * @class AppMobChartViewLayoutProps
 * @extends AppLayoutProps
 */
export declare class AppMobChartViewLayoutProps extends AppLayoutProps implements IAppMobChartViewLayoutProps {
}
