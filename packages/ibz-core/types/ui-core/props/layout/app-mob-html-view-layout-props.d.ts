import { AppLayoutProps } from './app-layout-props';
/**
 * 移动端实体Html视图布局面板输入参数
 *
 * @export
 * @class AppMobHtmlViewLayoutProps
 */
export declare class AppMobHtmlViewLayoutProps extends AppLayoutProps {
}
