import { AppLayoutProps } from './app-layout-props';
/**
 * 移动端首页视图视图布局面板输入参数
 *
 * @export
 * @class AppIndexViewProps
 */
export declare class AppIndexViewLayoutProps extends AppLayoutProps {
}
