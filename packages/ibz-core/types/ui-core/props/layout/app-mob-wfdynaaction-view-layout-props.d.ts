import { AppLayoutProps } from './app-layout-props';
/**
 * 移动端动态工作流操作视图视图布局输入参数
 *
 * @export
 * @class AppMobWFDynaActionViewLayoutProps
 */
export declare class AppMobWFDynaActionViewLayoutProps extends AppLayoutProps {
}
