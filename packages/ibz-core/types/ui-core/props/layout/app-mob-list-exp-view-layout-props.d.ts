import { AppLayoutProps } from './app-layout-props';
/**
 * 移动端列表导航视图视图布局输入参数
 *
 * @export
 * @class AppMobListExpViewLayoutProps
 */
export declare class AppMobListExpViewLayoutProps extends AppLayoutProps {
}
