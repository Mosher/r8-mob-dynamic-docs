import { IAppDEExpViewProps } from '../../../interface';
import { AppDEViewProps } from './app-de-view-props';
/**
 * @description 移动端实体导航视图输入参数
 * @export
 * @class AppDEExpViewProps
 * @extends {AppDEViewProps}
 */
export declare class AppDEExpViewProps extends AppDEViewProps implements IAppDEExpViewProps {
}
