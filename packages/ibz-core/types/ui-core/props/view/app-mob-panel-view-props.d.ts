import { IAppMobPanelViewProps } from '../../../interface';
import { AppDEViewProps } from './app-de-view-props';
/**
 * 移动端面板视图视图输入属性
 *
 * @export
 * @class AppMobPanelViewProps
 */
export declare class AppMobPanelViewProps extends AppDEViewProps implements IAppMobPanelViewProps {
}
