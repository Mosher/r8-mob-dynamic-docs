import { IAppMobDEDashboardViewProps } from '../../../interface';
import { AppDEViewProps } from './app-de-view-props';
/**
 * 移动端实体数据看板视图输入参数
 *
 * @exports
 * @class AppMobDEDashboardViewProps
 * @extends AppDEViewProps
 * @implements IAppMobDEDashboardViewProps
 */
export declare class AppMobDEDashboardViewProps extends AppDEViewProps implements IAppMobDEDashboardViewProps {
}
