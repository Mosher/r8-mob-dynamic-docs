import { IAppPortalViewProps } from '../../../interface';
import { AppViewProps } from './app-view-props';
/**
 * 移动端应用看板视图输入参数
 *
 * @exports
 * @class AppPortalViewProps
 * @extends AppViewProps
 */
export declare class AppPortalViewProps extends AppViewProps implements IAppPortalViewProps {
}
