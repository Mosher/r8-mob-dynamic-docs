import { IAppMobWFDynaActionViewProps } from '../../../interface';
import { AppDEViewProps } from './app-de-view-props';
/**
 * 移动端动态工作流操作视图输入属性
 *
 * @export
 * @class AppMobWFDynaActionViewProps
 */
export declare class AppMobWFDynaActionViewProps extends AppDEViewProps implements IAppMobWFDynaActionViewProps {
}
