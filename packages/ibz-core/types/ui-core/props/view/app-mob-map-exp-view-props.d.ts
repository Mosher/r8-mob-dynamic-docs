import { IAppMobMapExpViewProps } from '../../../interface';
import { AppDEExpViewProps } from './app-de-exp-view-props';
/**
 * 移动端地图导航视图输入属性
 *
 * @export
 * @class AppMobMapExpViewProps
 */
export declare class AppMobMapExpViewProps extends AppDEExpViewProps implements IAppMobMapExpViewProps {
}
