import { IAppMobWFDynaEditViewProps } from '../../../interface';
import { AppDEViewProps } from './app-de-view-props';
/**
 * 移动端动态工作流编辑视图输入属性
 *
 * @export
 * @class AppMobWFDynaEditViewProps
 */
export declare class AppMobWFDynaEditViewProps extends AppDEViewProps implements IAppMobWFDynaEditViewProps {
}
