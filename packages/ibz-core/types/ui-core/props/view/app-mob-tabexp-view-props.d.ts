import { IAppMobTabExpViewProps } from '../../../interface';
import { AppDEViewProps } from './app-de-view-props';
/**
 * 移动端分页导航视图视图输入属性
 *
 * @export
 * @class AppMobTabExpViewProps
 */
export declare class AppMobTabExpViewProps extends AppDEViewProps implements IAppMobTabExpViewProps {
}
