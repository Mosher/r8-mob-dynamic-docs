import { IAppMobEditViewProps } from '../../../interface';
import { AppDEViewProps } from './app-de-view-props';
/**
 * 移动端编辑视图视图输入属性
 *
 * @export
 * @class AppMobEditViewProps
 */
export declare class AppMobEditViewProps extends AppDEViewProps implements IAppMobEditViewProps {
}
