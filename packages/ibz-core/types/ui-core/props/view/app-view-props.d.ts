import { IParam } from '../../../interface';
import { IAppViewProps } from '../../../interface';
/**
 * 视图输入属性
 *
 * @export
 * @class AppViewProps
 */
export declare class AppViewProps implements IAppViewProps {
    viewPath: string;
    viewShowMode: string;
    navContext: IParam;
    navParam: IParam;
    navDatas: IParam;
    isShowCaptionBar: boolean;
    isLoadDefault: boolean;
    viewState: IParam;
    modelData: any;
}
