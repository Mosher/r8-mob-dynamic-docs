import { IAppMobWizardViewProps } from '../../../interface';
import { AppDEViewProps } from './app-de-view-props';
/**
 * 移动端向导视图输入属性
 *
 * @export
 * @class AppMobWizardViewProps
 */
export declare class AppMobWizardViewProps extends AppDEViewProps implements IAppMobWizardViewProps {
}
