import { IAppIndexViewProps } from '../../../interface';
import { AppViewProps } from './app-view-props';
/**
 * 应用首页视图输入属性
 *
 * @export
 * @class AppIndexViewProps
 */
export declare class AppIndexViewProps extends AppViewProps implements IAppIndexViewProps {
}
