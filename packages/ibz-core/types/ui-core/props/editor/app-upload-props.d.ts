import { AppEditorProps } from './app-editor-props';
/**
 * 移动端上传编辑器输入参数
 *
 * @class AppUploadProps
 * @extends AppUploadProps
 */
export declare class AppUploadProps extends AppEditorProps {
}
