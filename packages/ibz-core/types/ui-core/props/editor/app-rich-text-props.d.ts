import { AppEditorProps } from './app-editor-props';
/**
 * 富文本编辑器输入参数
 *
 * @export
 * @class AppRichTextProps
 */
export declare class AppRichTextProps extends AppEditorProps {
}
