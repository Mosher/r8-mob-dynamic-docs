import { IParam } from '../../../interface';
/**
 * 应用编辑器输入参数
 *
 * @export
 * @class AppEditorProps
 */
export declare class AppEditorProps {
    editorInstance: IParam;
    navContext: IParam;
    navParam: IParam;
    contextData: IParam;
    contextState: IParam;
    value: any;
    parentItem: IParam;
    disabled: boolean;
    modelService: IParam;
    modelData: any;
}
