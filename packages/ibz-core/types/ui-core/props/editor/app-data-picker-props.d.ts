import { AppEditorProps } from './app-editor-props';
/**
 * 数据选择编辑器输入参数
 *
 * @export
 * @class AppDataPickerProps
 */
export declare class AppDataPickerProps extends AppEditorProps {
}
