import { AppEditorProps } from './app-editor-props';
/**
 * 步进器输入参数
 *
 * @export
 * @class AppStepperProps
 */
export declare class AppStepperProps extends AppEditorProps {
}
