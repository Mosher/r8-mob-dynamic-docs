import { AppEditorProps } from './app-editor-props';
/**
 * 下拉列表编辑器输入参数
 *
 * @export
 * @class AppDropdownListProps
 */
export declare class AppDropdownListProps extends AppEditorProps {
}
