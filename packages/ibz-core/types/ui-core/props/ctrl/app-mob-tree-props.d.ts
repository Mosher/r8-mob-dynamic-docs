import { AppMDCtrlProps } from './app-md-ctrl-props';
/**
 * 移动端树部件驶入参数
 *
 * @exports
 * @class AppMobTreeProps
 * @extends AppMDCtrlProps
 */
export declare class AppMobTreeProps extends AppMDCtrlProps {
}
