import { AppCtrlProps } from './app-ctrl-props';
import { IParam } from '../../../interface';
/**
 * 移动端上下文菜单输入参数
 *
 * @class AppMobContextMenuProps
 * @extends AppCtrlProps
 */
export declare class AppMobContextMenuProps extends AppCtrlProps {
    /**
     * @description 上下文菜单行为模型
     * @type {IParam}
     * @memberof AppMobContextMenuProps
     */
    contextMenuActionModel: IParam;
    /**
     * @description 打开上下文菜单事件源
     * @type {MouseEvent}
     * @memberof AppMobContextMenuProps
     */
    mouseEvent: IParam;
}
