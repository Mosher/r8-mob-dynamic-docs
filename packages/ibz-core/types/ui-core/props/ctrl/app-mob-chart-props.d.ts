import { AppMDCtrlProps } from './app-md-ctrl-props';
/**
 * 移动端图表部件传入参数
 *
 * @class AppMobChartProps
 * @extends AppMDCtrlProps
 */
export declare class AppMobChartProps extends AppMDCtrlProps {
}
