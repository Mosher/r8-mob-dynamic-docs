import { AppExpBarCtrlProps } from './app-exp-bar-ctrl-props';
/**
 * 移动端列表导航部件输入参数
 *
 * @class AppMobListExpBarProps
 * @extends AppCtrlProps
 */
export declare class AppMobListExpBarProps extends AppExpBarCtrlProps {
}
