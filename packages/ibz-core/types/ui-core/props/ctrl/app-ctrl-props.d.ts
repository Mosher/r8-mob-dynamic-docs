import { IParam } from '../../../interface';
/**
 * 应用部件输入参数
 *
 * @export
 * @class AppCtrlProps
 */
export declare class AppCtrlProps {
    controlInstance: IParam;
    navContext: IParam;
    navParam: IParam;
    navDatas: IParam;
    viewState: IParam;
    viewCtx: IParam;
    modelService: IParam;
    /**
     * @description 是否开启下拉刷新
     */
    enablePullDownRefresh: boolean;
    /**
     * @description 部件显示模式
     */
    ctrlShowMode: string;
    /**
     * @description 视图计数器服务数组
     */
    viewCounterServiceArray: IParam[];
    modelData: any;
}
