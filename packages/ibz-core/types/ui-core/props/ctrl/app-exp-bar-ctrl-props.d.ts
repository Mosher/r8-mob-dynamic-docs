import { AppCtrlProps } from './app-ctrl-props';
/**
 * @description 移动端导航部件输入参数
 * @export
 * @class AppExpBarCtrlProps
 * @extends {AppCtrlProps}
 */
export declare class AppExpBarCtrlProps extends AppCtrlProps {
}
