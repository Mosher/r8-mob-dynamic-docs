import { AppCtrlProps } from './app-ctrl-props';
/**
 * 移动端表单输入参数
 *
 * @export
 * @class AppMobFormProps
 */
export declare class AppMobFormProps extends AppCtrlProps {
}
