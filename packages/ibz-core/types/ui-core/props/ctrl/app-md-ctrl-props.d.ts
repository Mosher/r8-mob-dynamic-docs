import { AppCtrlProps } from './app-ctrl-props';
/**
 * 多数据部件基类传入参数
 *
 * @class AppMDCtrlProps
 * @extends AppMainCtrlProps
 */
export declare class AppMDCtrlProps extends AppCtrlProps {
    /**
     * @description 是否多选
     */
    isMultiple: boolean;
}
