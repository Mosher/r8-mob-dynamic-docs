import { AppExpBarCtrlProps } from './app-exp-bar-ctrl-props';
/**
 * 移动端地图导航部件输入参数
 *
 * @class AppMobMapExpBarProps
 * @extends AppExpBarCtrlProps
 */
export declare class AppMobMapExpBarProps extends AppExpBarCtrlProps {
}
