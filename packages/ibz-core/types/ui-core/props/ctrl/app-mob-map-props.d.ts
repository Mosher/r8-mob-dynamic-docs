import { AppMDCtrlProps } from './app-md-ctrl-props';
/**
 * 移动端地图部件传入参数
 *
 * @class AppMobMapProps
 * @extends AppMDCtrlProps
 */
export declare class AppMobMapProps extends AppMDCtrlProps {
}
