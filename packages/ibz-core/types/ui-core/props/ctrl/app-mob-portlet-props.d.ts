import { AppCtrlProps } from './app-ctrl-props';
/**
 * 移动端门户部件输入参数
 *
 * @class AppMobPortletProps
 * @extends AppCtrlProps
 */
export declare class AppMobPortletProps extends AppCtrlProps {
}
