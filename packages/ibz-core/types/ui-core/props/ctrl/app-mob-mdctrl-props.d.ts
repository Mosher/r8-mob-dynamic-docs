import { AppMDCtrlProps } from './app-md-ctrl-props';
/**
 * 移动端多数据部件传入参数
 *
 * @class AppMobMDCtrlProps
 * @extends AppMDCtrlProps
 */
export declare class AppMobMDCtrlProps extends AppMDCtrlProps {
}
