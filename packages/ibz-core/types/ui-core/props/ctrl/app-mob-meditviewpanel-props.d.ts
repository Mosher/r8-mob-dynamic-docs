import { AppMDCtrlProps } from './app-md-ctrl-props';
/**
 * 移动端多表单编辑视图面板部件传入参数
 *
 * @class AppMobMEditViewPanelCtrlProps
 * @extends AppMDCtrlProps
 */
export declare class AppMobMEditViewPanelProps extends AppMDCtrlProps {
}
