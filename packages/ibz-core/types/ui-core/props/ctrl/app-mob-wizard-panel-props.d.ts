import { AppCtrlProps } from './app-ctrl-props';
/**
 * 移动端向导面板部件输入参数
 *
 * @class AppMobWizardPanelProps
 * @extends AppCtrlProps
 */
export declare class AppMobWizardPanelProps extends AppCtrlProps {
}
