import { IParam } from '../../../interface';
import { AppCtrlProps } from './app-ctrl-props';
/**
 * 视图面板、项布局面板部件输入参数
 *
 * @export
 * @class AppMobPanelProps
 */
export declare class AppMobPanelProps extends AppCtrlProps {
    /**
     * @description 是否默认加载
     * @type {boolean}
     * @memberof AppMobPanelProps
     */
    isLoadDefault: boolean;
    /**
     * @description 数据映射
     * @type {Map<string, IParam>}
     * @memberof AppMobPanelProps
     */
    dataMap: Map<string, IParam>;
}
