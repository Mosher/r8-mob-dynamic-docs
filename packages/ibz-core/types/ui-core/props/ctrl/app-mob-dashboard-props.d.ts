import { AppCtrlProps } from './app-ctrl-props';
/**
 * 移动端数据看板部件输入参数
 *
 * @exports
 * @class AppMobDashBoardProps
 * @extend AppCtrlProps
 */
export declare class AppMobDashBoardProps extends AppCtrlProps {
}
