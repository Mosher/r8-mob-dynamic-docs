/**
 * 部件服务基类
 *
 * @export
 * @class ControlServiceBase
 */
export declare class ControlServiceBase {
    /**
     * 应用状态对象
     *
     * @private
     * @type {(any | null)}
     * @memberof ControlServiceBase
     */
    private $store;
    /**
     * 部件模型
     *
     * @private
     * @type {(any | null)}
     * @memberof ControlServiceBase
     */
    controlInstance: any;
    /**
     * 部件模型
     *
     * @type {(any | null)}
     * @memberof ControlServiceBase
     */
    model: any | null;
    /**
     * 代码表服务对象
     *
     * @type {any}
     * @memberof ControlServiceBase
     */
    codeListService: any;
    /**
     * 上下文
     *
     * @type {any}
     * @memberof ControlServiceBase
     */
    context: any;
    /**
     * 是否为从数据模式
     *
     * @type {boolean}
     * @memberof ControlServiceBase
     */
    isTempMode: boolean;
    /**
     * Creates an instance of ControlServiceBase.
     *
     * @param {*} [opts={}]
     * @memberof ControlServiceBase
     */
    constructor(opts?: any, context?: any);
    /**
     * 应用实体codeName
     *
     * @readonly
     * @memberof ControlServiceBase
     */
    get appDeCodeName(): any;
    /**
     * 应用实体映射实体名称
     *
     * @readonly
     * @memberof ControlServiceBase
     */
    get deName(): any;
    /**
     * 应用实体主键属性codeName
     *
     * @readonly
     * @memberof ControlServiceBase
     */
    get appDeKeyFieldName(): string;
    /**
     * 应用实体主信息属性codeName
     *
     * @readonly
     * @memberof ControlServiceBase
     */
    get appDeMajorFieldName(): string;
    /**
     * 获取应用状态对象
     *
     * @returns {(any | null)}
     * @memberof ControlServiceBase
     */
    getStore(): any;
    /**
     * 获取部件模型
     *
     * @returns {*}
     * @memberof ControlServiceBase
     */
    getMode(): any;
    /**
     * 设置零时模式
     *
     * @returns {(any | null)}
     * @memberof ControlServiceBase
     */
    protected setTempMode(): void;
    /**
     * 处理请求数据
     *
     * @param action 行为
     * @param data 数据
     * @memberof ControlServiceBase
     */
    handleRequestData(action: string, context?: any, data?: any, isMerge?: boolean): any;
    /**
     * 处理response
     *
     * @param {string} action
     * @param {*} response
     * @memberof ControlServiceBase
     */
    handleResponse(action: string, response: any, isCreate?: boolean): Promise<void>;
    /**
     * 处理返回数据
     *
     * @param {string} action
     * @param {*} response
     * @memberof ControlServiceBase
     */
    handleResponseData(action: string, data?: any, isCreate?: boolean, codelistArray?: any, index?: number): any;
    /**
     * 处理工作流数据
     *
     * @param data 传入数据
     */
    handleWFData(data: any, isMerge?: boolean): any;
    /**
     * 处理代码表
     *
     * @memberof ControlServiceBase
     */
    handleCodelist(): any[];
    /**
     * 处理部件数据项自定义脚本
     *
     * @param data 当前数据
     * @param scriptCode 自定义脚本
     * @memberof ControlServiceBase
     */
    handleScriptCode(data: any, dataitem: any, scriptCode: string, rowIndex?: number): any;
    /**
     * 获取所有代码表
     *
     * @param codelistArray 代码表模型数组
     * @memberof ControlServiceBase
     */
    getAllCodeList(codelistArray: Array<any>): Promise<any>;
    /**
     * 获取代码表
     *
     * @param codeListObject 传入代码表对象
     * @memberof ControlServiceBase
     */
    getCodeList(codeListObject: any): Promise<any>;
    /**
     * 获取部件模型
     *
     * @param codeListObject 传入代码表对象
     * @memberof ControlServiceBase
     */
    getControlInstance(): any;
    /**
     * 根据后台标识获取数据标识名称
     *
     * @param prop 后台标识
     * @memberof ControlServiceBase
     */
    getNameByProp(prop: any): any;
}
