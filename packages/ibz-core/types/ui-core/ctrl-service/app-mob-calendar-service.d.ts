import { ControlServiceBase } from './ctrl-service-base';
import { IPSDECalendar } from '@ibiz/dynamic-model-api';
export declare class AppMobCalendarService extends ControlServiceBase {
    /**
     * 日历实例对象
     *
     * @memberof AppMobCalendarService
     */
    controlInstance: IPSDECalendar;
    /**
     * 数据服务对象
     *
     * @type {any}
     * @memberof AppMobCalendarService
     */
    appEntityService: any;
    /**
     * 项实体集合
     *
     * @memberof AppMobCalendarService
     */
    private $itemEntityServiceMap;
    /**
     * 初始化服务参数
     *
     * @type {boolean}
     * @memberof AppMobCalendarService
     */
    initServiceParam(): Promise<void>;
    /**
     * Creates an instance of AppMobCalendarService.
     *
     * @param {*} [opts={}]
     * @memberof AppMobCalendarService
     */
    constructor(opts?: any, context?: any);
    /**
     * 部件服务加载
     *
     * @memberof AppMobMDCtrlService
     */
    loaded(): Promise<void>;
    /**
     * 事件配置集合
     *
     * @public
     * @type {any[]}
     * @memberof AppMobCalendarService
     */
    eventsConfig: any[];
    /**
     * 初始化事件配置集合
     *
     * @memberof AppMobCalendarService
     */
    initEventsConfig(): void;
    initItemEntityService(): Promise<void>;
    /**
     * 查询数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof AppMobCalendarService
     */
    search(action: string, context?: any, data?: any, isloading?: boolean): Promise<any>;
    /**
     * 修改数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof AppMobCalendarService
     */
    update(itemType: string, context?: any, data?: any, isloading?: boolean): Promise<any>;
    /**
     * 删除数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isLoading]
     * @returns {Promise<any>}
     * @memberof AppMobCalendarService
     */
    delete(itemType: string, context?: any, data?: any, isloading?: boolean): Promise<any>;
    /**
     * 处理request请求数据
     *
     * @param action 行为
     * @param data 数据
     * @memberof ControlService
     */
    handleRequestData(action: string, context?: any, data?: any, isMerge?: boolean, itemType?: string): any;
    /**
     * 处理response返回数据
     *
     * @param {string} action
     * @param {*} response
     * @memberof ControlService
     */
    handleResponse(action: string, response: any, isCreate?: boolean, itemType?: string): Promise<void>;
    /**
     * 处理返回数据
     *
     * @param {string} action
     * @param {*} response
     * @memberof ControlService
     */
    handleResponseData(action: string, data?: any, isCreate?: boolean, codelistArray?: any): any;
}
