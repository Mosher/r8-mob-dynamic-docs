import { IPSDEEditForm } from '@ibiz/dynamic-model-api';
import { ControlServiceBase } from './ctrl-service-base';
/**
 * 表单部件服务对象
 *
 * @export
 * @class AppMobFormService
 */
export declare class AppMobFormService extends ControlServiceBase {
    /**
     * 表单实例对象
     *
     * @memberof MainModel
     */
    controlInstance: IPSDEEditForm;
    /**
     * 数据服务对象
     *
     * @type {any}
     * @memberof AppMobFormService
     */
    appEntityService: any;
    /**
     * 远端数据
     *
     * @type {*}
     * @memberof AppMobFormService
     */
    private remoteCopyData;
    /**
     * 初始化服务参数
     *
     * @type {boolean}
     * @memberof AppMobFormService
     */
    initServiceParam(): Promise<void>;
    /**
     * Creates an instance of AppMobFormService.
     *
     * @param {*} [opts={}]
     * @memberof AppMobFormService
     */
    constructor(opts?: any, context?: any);
    /**
     * loaded
     *
     * @memberof AppMobFormService
     */
    loaded(): Promise<void>;
    /**
     * 处理数据
     *
     * @private
     * @param {Promise<any>} promise
     * @returns {Promise<any>}
     * @memberof AppMobFormService
     */
    private doItems;
    /**
     * 获取跨实体数据集合
     *
     * @param {string} serviceName 服务名称
     * @param {string} interfaceName 接口名称
     * @param {*} data
     * @param {boolean} [isloading]
     * @returns {Promise<any[]>}
     * @memberof  AppMobFormService
     */
    getItems(serviceName: string, interfaceName: string, context: any, data: any, isloading?: boolean): Promise<any[]>;
    /**
     * 启动工作流
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @param {*} [localdata]
     * @returns {Promise<any>}
     * @memberof AppMobFormService
     */
    wfstart(action: string, context?: any, data?: any, isloading?: boolean, localdata?: any): Promise<any>;
    /**
     * 提交工作流
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @param {*} [localdata]
     * @returns {Promise<any>}
     * @memberof AppMobFormService
     */
    wfsubmit(action: string, context?: any, data?: any, isloading?: boolean, localdata?: any): Promise<any>;
    /**
     * 添加数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @param {boolean} [isWorkflow] 是否在工作流中添加数据
     * @returns {Promise<any>}
     * @memberof AppMobFormService
     */
    add(action: string, context?: any, data?: any, isloading?: boolean, isWorkflow?: boolean): Promise<any>;
    /**
     * 删除数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof AppMobFormService
     */
    delete(action: string, context?: any, data?: any, isloading?: boolean): Promise<any>;
    /**
     * 修改数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @param {boolean} [isWorkflow] 是否在工作流中修改数据
     * @returns {Promise<any>}
     * @memberof AppMobFormService
     */
    update(action: string, context?: any, data?: any, isloading?: boolean, isWorkflow?: boolean): Promise<any>;
    /**
     * 查询数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof AppMobFormService
     */
    get(action: string, context?: any, data?: any, isloading?: boolean): Promise<any>;
    /**
     * 加载草稿
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof AppMobFormService
     */
    loadDraft(action: string, context?: any, data?: any, isloading?: boolean): Promise<any>;
    /**
     * 前台逻辑
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof AppMobFormService
     */
    frontLogic(action: string, context?: any, data?: any, isloading?: boolean): Promise<any>;
    /**
     * 处理请求数据
     *
     * @param action 行为
     * @param data 数据
     * @memberof AppMobFormService
     */
    handleRequestData(action: string, context: any, data?: any, isMerge?: boolean): any;
    /**
     * 通过属性名称获取表单项名称
     *
     * @param name 实体属性名称
     * @memberof AppMobFormService
     */
    getItemNameByDeName(name: string): string;
    /**
     * 重写处理返回数据
     *
     * @param {string} action
     * @param {*} response
     * @memberof AppMobFormService
     */
    handleResponseData(action: string, data?: any, isCreate?: boolean, codelistArray?: any): any;
    /**
     * 设置远端数据
     *
     * @param result 远端请求结果
     * @memberof AppMobFormService
     */
    setRemoteCopyData(result: any): void;
    /**
     * 获取远端数据
     *
     * @memberof AppMobFormService
     */
    getRemoteCopyData(): any;
    /**
     * 加载数据模型
     *
     * @param {string} serviceName
     * @param {*} context
     * @param {*} viewparams
     * @memberof AppMobFormService
     */
    loadModel(serviceName: string, context: any, viewparams: any): Promise<unknown>;
    /**
     * 保存模型
     *
     * @param {string} serviceName
     * @param {*} context
     * @param {*} viewparams
     * @returns
     * @memberof AppMobFormService
     */
    saveModel(serviceName: string, context: any, viewparams: any): Promise<unknown>;
}
