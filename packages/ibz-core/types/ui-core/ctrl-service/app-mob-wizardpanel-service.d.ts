import { IPSDEWizardPanel } from '@ibiz/dynamic-model-api';
import { ControlServiceBase } from './ctrl-service-base';
export declare class AppMobWizardPanelService extends ControlServiceBase {
    /**
    * 表格实例对象
    *
    * @memberof AppMobWizardPanelService
    */
    controlInstance: IPSDEWizardPanel;
    /**
     * 数据服务对象
     *
     * @type {any}
     * @memberof AppMobWizardPanelService
     */
    appEntityService: any;
    /**
     * 初始化服务参数
     *
     * @type {boolean}
     * @memberof AppMobWizardPanelService
     */
    initServiceParam(): Promise<void>;
    /**
     * Creates an instance of AppMobWizardPanelService.
     *
     * @param {*} [opts={}]
     * @memberof AppMobWizardPanelService
     */
    constructor(opts?: any, context?: any);
    loaded(): Promise<void>;
    /**
     * 初始化向导
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ${srfclassname('${ctrl.codeName}')}Service
     */
    init(action: string, context?: any, data?: any, isloading?: boolean): Promise<any>;
    /**
     * 向导结束
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof ${srfclassname('${ctrl.codeName}')}Service
     */
    finish(action: string, context?: any, data?: any, isloading?: boolean): Promise<any>;
}
