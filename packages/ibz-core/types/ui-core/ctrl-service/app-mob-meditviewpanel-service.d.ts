import { IPSDEMultiEditViewPanel } from '@ibiz/dynamic-model-api';
import { ControlServiceBase } from './ctrl-service-base';
/**
 * Main 部件服务对象
 *
 * @export
 * @class AppMobMEditViewPanelService
 */
export declare class AppMobMEditViewPanelService extends ControlServiceBase {
    /**
     * 多编辑视图实例对象
     *
     * @memberof AppMobMEditViewPanelService
     */
    controlInstance: IPSDEMultiEditViewPanel;
    /**
     * 数据服务对象
     *
     * @type {any}
     * @memberof AppMobMEditViewPanelService
     */
    appEntityService: any;
    /**
     * 初始化服务参数
     *
     * @type {boolean}
     * @memberof AppMobMEditViewPanelService
     */
    initServiceParam(): Promise<void>;
    /**
     * Creates an instance of AppMobMEditViewPanelService
     *
     * @param {*} [opts={}]
     * @memberof AppMobMEditViewPanelService
     */
    constructor(opts?: any, context?: any);
    /**
     * 部件服务加载
     *
     * @memberof AppMobMEditViewPanelService
     */
    loaded(): Promise<void>;
    /**
     * 查询数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof AppMobMEditViewPanelService
     */
    get(action: string, context?: any, data?: any, isloading?: boolean): Promise<any>;
    /**
     * 加载草稿
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof AppMobMEditViewPanelService
     */
    loadDraft(action: string, context?: any, data?: any, isloading?: boolean): Promise<any>;
}
