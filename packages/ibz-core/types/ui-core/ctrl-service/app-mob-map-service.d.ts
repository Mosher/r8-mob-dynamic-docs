import { IPSSysMap } from '@ibiz/dynamic-model-api';
import { ControlServiceBase } from './ctrl-service-base';
/**
 * 地图部件服务对象
 *
 * @export
 * @class AppMobMapService
 */
export declare class AppMobMapService extends ControlServiceBase {
    /**
     * 表单实例对象
     *
     * @memberof AppMobMapService
     */
    controlInstance: IPSSysMap;
    /**
     * 地图配置集合
     *
     * @type {*}
     * @memberof AppMobMapService
     */
    mapConfig: any[];
    /**
     * 数据服务对象
     *
     * @type {any}
     * @memberof AppMobMapService
     */
    appEntityService: any;
    /**
     * 地图项服务集合
     *
     * @type {boolean}
     * @memberof AppMobMapService
     */
    private $itemEntityServiceMap;
    /**
     * 初始化地图项服务集合
     *
     * @type {boolean}
     * @memberof AppMobMapService
     */
    initItemEntityService(): Promise<void>;
    /**
     * 初始化服务参数
     *
     * @type {boolean}
     * @memberof AppMobMapService
     */
    initServiceParam(): Promise<void>;
    /**
     * 初始化Map参数
     *
     * @memberof AppMobMapService
     */
    initMapConfig(): void;
    /**
     * Creates an instance of AppMobMapService.
     *
     * @param {*} [opts={}]
     * @memberof AppMobMapService
     */
    constructor(opts?: any, context?: any);
    /**
     * async loaded
     */
    loaded(): Promise<void>;
    /**
     * 查询数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof AppMobMapService
     */
    search(action: string, context?: any, data?: any, isloading?: boolean): Promise<any>;
    /**
     * 处理request请求数据
     *
     * @param action 行为
     * @param data 数据
     * @memberof AppMobMapService
     */
    handleRequestData(action: string, context?: any, data?: any, isMerge?: boolean, itemType?: string): any;
    /**
     * 处理response返回数据
     *
     * @param {string} action
     * @param {*} response
     * @memberof AppMobMapService
     */
    handleResponse(action: string, response: any, isCreate?: boolean, itemType?: string): Promise<void>;
}
