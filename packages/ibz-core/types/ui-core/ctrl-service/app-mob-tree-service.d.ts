import { IPSDETree, IPSDETreeNode, IPSDETreeNodeRS } from '@ibiz/dynamic-model-api';
import { ControlServiceBase } from './ctrl-service-base';
/**
 * 树视图部件服务对象
 *
 * @export
 * @class AppMobTreeService
 * @extends {ControlServiceBase}
 */
export declare class AppMobTreeService extends ControlServiceBase {
    /**
     * 树视图部件实例对象
     *
     * @memberof MainModel
     */
    controlInstance: IPSDETree;
    /**
     * 节点分隔符号
     *
     * @public
     * @type {string}
     * @memberof AppMobTreeService
     */
    TREENODE_SEPARATOR: string;
    /**
     * Creates an instance of AppMobTreeService.
     *
     * @param {*} [opts={}]
     * @memberof AppMobTreeService
     */
    constructor(opts?: any, context?: any);
    /**
     * 初始化服务参数
     *
     * @type {boolean}
     * @memberof AppMobTreeService
     */
    initServiceParam(opts: any): Promise<void>;
    /**
     * 获取节点数据
     *
     * @param {string} action
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof AppMobTreeService
     */
    getNodes(context?: any, data?: any, isloading?: boolean): Promise<any>;
    /**
     * 根据节点关系填充所有子节点
     *
     * @public
     * @param {*} nodeJson
     * @param {any{}} context
     * @param {*} filter
     * @param {any[]} list
     * @returns {Promise<any>}
     * @memberof AppMobTreeService
     */
    fillChildNodes(nodeJson: IPSDETreeNode, context: any, filter: any, list: any[]): Promise<any>;
    /**
     * 填充树视图节点数据内容
     *
     * @public
     * @param {*} nodeJson 节点数据
     * @param {any{}} context
     * @param {*} filter
     * @param {any[]} list
     * @param {*} rsNavContext
     * @param {*} rsNavParams
     * @param {*} rsParams
     * @returns {Promise<any>}
     * @memberof AppMobTreeService
     */
    fillNodeData(nodeJson: IPSDETreeNode, context: any, filter: any, list: any[], rsNavContext?: any, rsNavParams?: any, rsParams?: any): Promise<any>;
    /**
     * 获取查询集合
     *
     * @param {*} [context={}]
     * @param {*} searchFilter
     * @param {*} filter
     * @returns {Promise<any>}
     * @memberof AppMobTreeService
     */
    searchNodeData(nodeJson: IPSDETreeNode, context: any, searchFilter: any, filter: any): Promise<any>;
    /**
     * 转换代码表数据为树节点数据
     *
     * @param {Array<any>} codeItems
     * @param {*} context
     * @param {*} filter 过滤参数
     * @param {*} nodeJson
     * @returns
     * @memberof AppMobTreeService
     */
    transFormCodeListData(codeItems: Array<any>, context: any, filter: any, nodeJson: IPSDETreeNode): any[];
    /**
     * 处理节点关系导航上下文
     *
     * @param context 应用上下文
     * @param filter 参数
     * @param resNavContext 节点关系导航上下文
     *
     * @memberof AppMobTreeService
     */
    handleResNavContext(context: any, filter: any, resNavContext: any): any;
    /**
     * 处理关系导航参数
     *
     * @param context 应用上下文
     * @param filter 参数
     * @param resNavParams 节点关系导航参数
     * @param resParams 节点关系参数
     *
     * @memberof AppMobTreeService
     */
    handleResNavParams(context: any, filter: any, resNavParams: any, resParams: any): any;
    /**
     * 处理自定义节点关系导航数据
     *
     * @param context 应用上下文
     * @param viewparams 参数
     * @param curNavData 节点关系导航参数对象
     * @param tempData 返回数据
     * @param item 节点关系导航参数键值
     * @param parentData 父值
     *
     * @memberof AppMobTreeService
     */
    handleCustomDataLogic(context: any, viewparams: any, curNavData: any, tempData: any, item: string, parentData?: any): void;
    /**
     * 获取树节点关系导航上下文
     *
     * @param noders 节点
     * @memberof AppMobTreeService
     */
    getNavContext(noders: IPSDETreeNodeRS): any;
    /**
     * 获取树节点关系导航参数
     *
     * @param noders 节点
     * @memberof AppMobTreeService
     */
    getNavParams(noders: IPSDETreeNodeRS): any;
    /**
     * 获取树节点关系参数
     *
     * @param noders 节点
     * @memberof AppMobTreeService
     */
    getParams(noders: IPSDETreeNodeRS): any;
    /**
     * 修改数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {*} [service]
     * @param {boolean} [isWorkflow]
     * @returns {Promise<any>}
     * @memberof AppMobTreeService
     */
    update(action: string, context: any, data: any, service: any, isWorkflow?: boolean): Promise<any>;
}
