import { IPSDEMobMDCtrl } from '@ibiz/dynamic-model-api';
import { ControlServiceBase } from './ctrl-service-base';
export declare class AppMobMDCtrlService extends ControlServiceBase {
    /**
     * 多数据部件实例对象
     *
     * @type {IPSDEMobMDCtrl}
     * @memberof AppMobMDCtrlService
     */
    controlInstance: IPSDEMobMDCtrl;
    /**
     * 实体服务对象
     *
     * @type {IPSDEMobMDCtrl}
     * @memberof AppMobMDCtrlService
     */
    appEntityService: any;
    /**
     * 远端数据
     *
     * @type {*}
     * @memberof AppMobMDCtrlService
     */
    private remoteCopyData;
    /**
     * 构造多数据部件部件服务
     *
     * @param opts 多数据部件实例
     * @param context 应用上下文
     * @memberof AppMobMDCtrlService
     */
    constructor(opts?: any, context?: any);
    /**
     * 部件服务加载
     *
     * @memberof AppMobMDCtrlService
     */
    loaded(): Promise<void>;
    /**
     * 初始化服务参数
     *
     * @memberof AppMobMDCtrlService
     */
    initServiceParam(): Promise<void>;
    /**
     * 查询数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof AppListService
     */
    search(action: string, context?: any, data?: any, isloading?: boolean): Promise<any>;
    /**
     * 删除数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof AppListService
     */
    delete(action: string, context?: any, data?: any, isloading?: boolean): Promise<any>;
    /**
     * 添加数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof AppListService
     */
    add(action: string, context?: any, data?: any, isloading?: boolean): Promise<any>;
    /**
     * 修改数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof AppListService
     */
    update(action: string, context?: any, data?: any, isloading?: boolean): Promise<any>;
}
