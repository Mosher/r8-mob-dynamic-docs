import { ControlServiceBase } from './ctrl-service-base';
import { IPSDEChart } from '@ibiz/dynamic-model-api';
/**
 * 图表部件服务对象
 *
 * @export
 * @class AppMobChartService
 */
export declare class AppMobChartService extends ControlServiceBase {
    /**
     * 图表实例对象
     *
     * @memberof AppMobChartService
     */
    controlInstance: IPSDEChart;
    /**
     * 数据服务对象
     *
     * @type {any}
     * @memberof AppMobChartService
     */
    appEntityService: any;
    /**
     * 初始化服务参数
     *
     * @type {boolean}
     * @memberof AppMobChartService
     */
    initServiceParam(): Promise<void>;
    /**
     * Creates an instance of AppMobChartService.
     *
     * @param {*} [opts={}]
     * @memberof AppMobChartService
     */
    constructor(opts?: any, context?: any);
    /**
     * 加载服务参数
     *
     * @type {boolean}
     * @memberof AppMobChartService
     */
    loaded(): Promise<void>;
    /**
     * 查询数据
     *
     * @param {string} action
     * @param {*} [context={}]
     * @param {*} [data={}]
     * @param {boolean} [isloading]
     * @returns {Promise<any>}
     * @memberof AppMobChartService
     */
    search(action: string, context?: any, data?: any, isloading?: boolean): Promise<any>;
}
