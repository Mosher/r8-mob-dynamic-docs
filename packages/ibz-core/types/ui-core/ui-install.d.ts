import { IApp } from '../interface';
/**
 * 安装插件
 *
 * @param config 配置
 */
export declare const UIInstall: (App: IApp) => void;
