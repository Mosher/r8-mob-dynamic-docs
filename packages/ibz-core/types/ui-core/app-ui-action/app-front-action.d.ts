import { IPSAppDEUIAction } from '@ibiz/dynamic-model-api';
import { IUIDataParam, IUIEnvironmentParam, IUIUtilParam } from '../../interface';
import { AppDEUIAction } from './app-ui-action';
export declare class AppFrontAction extends AppDEUIAction {
    /**
     * 初始化AppFrontAction
     *
     * @memberof AppFrontAction
     */
    constructor(opts: IPSAppDEUIAction, context?: any);
    /**
     * 执行界面行为
     *
     * @param UIDataParam 操作数据参数
     * @param UIEnvironmentParam 操作环境参数
     * @param UIUtilParam 操作工具参数
     * @param srfParentDeName 父值参数
     * @param deUIService 界面UI服务
     * @param context 附加上下文
     * @param params 附加参数
     *
     * @memberof AppFrontAction
     */
    execute(UIDataParam: IUIDataParam, UIEnvironmentParam: IUIEnvironmentParam, UIUtilParam: IUIUtilParam, deUIService?: any, context?: any, params?: any): Promise<any>;
    /**
     * 打开视图
     *
     * @param frontPSAppView 打开目标视图
     * @param UIDataParam 操作数据参数
     * @param UIEnvironmentParam 操作环境参数
     * @param UIUtilParam 操作工具参数
     * @param deUIService 界面UI服务
     *
     * @return {*}
     * @memberof AppFrontAction
     */
    oPenView(frontPSAppView: any, UIDataParam: IUIDataParam, UIEnvironmentParam: IUIEnvironmentParam, UIUtilParam: IUIUtilParam, deUIService?: any): Promise<any>;
}
