import { IPSAppDEUIAction } from '@ibiz/dynamic-model-api';
import { IUIDataParam, IUIEnvironmentParam, IUIUtilParam } from '../../interface';
export declare class AppDEUIAction {
    /**
     * 模型数据
     *
     * @memberof AppDEUIAction
     */
    protected actionModel: IPSAppDEUIAction;
    /**
     * 初始化AppDEUIAction
     *
     * @memberof AppDEUIAction
     */
    constructor(opts: any, context?: any);
    /**
     * 执行界面逻辑
     *
     * @param UIDataParam 操作数据参数
     * @param UIEnvironmentParam 操作环境参数
     * @param UIUtilParam 操作工具参数
     *
     * @memberof AppDEUIAction
     */
    executeDEUILogic(UIDataParam: IUIDataParam, UIEnvironmentParam: IUIEnvironmentParam, UIUtilParam: IUIUtilParam): Promise<void>;
    /**
     * 执行界面行为之前（准备参数）
     *
     * @param UIDataParam 操作数据参数
     * @param UIEnvironmentParam 操作环境参数
     * @param UIUtilParam 操作工具参数
     *
     * @memberof AppDEUIAction
     */
    beforeExecute(UIDataParam: IUIDataParam, UIEnvironmentParam: IUIEnvironmentParam, UIUtilParam: IUIUtilParam): {
        args: any[];
        actionContext: import("../../interface").IParam;
        event: import("../../interface").IParam | undefined;
        xData: import("../../interface").IParam | null;
    };
}
