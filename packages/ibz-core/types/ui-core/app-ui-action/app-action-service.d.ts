import { IPSAppDEUIAction } from '@ibiz/dynamic-model-api';
export declare class AppActionService {
    /**
     * 获取界面行为
     *
     * @public
     * @static
     * @memberof AppActionService
     */
    getUIActionInst(modelData: IPSAppDEUIAction, context: any): Promise<any>;
}
