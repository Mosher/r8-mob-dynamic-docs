import { AppDEUIAction } from './app-ui-action';
import { IUIDataParam, IUIEnvironmentParam, IUIUtilParam } from '../../interface';
export declare class AppSysAction extends AppDEUIAction {
    /**
     * Creates an instance of AppSysAction.
     * @param {*} opts 模型数据
     * @param {*} [context] 上下文
     * @memberof AppSysAction
     */
    constructor(opts: any, context?: any);
    /**
     * @description 执行界面行为
     * @param {IUIDataParam} UIDataParam 操作数据参数
     * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
     * @param {IUIUtilParam} UIUtilParam 操作工具参数
     * @param {*} [deUIService] 界面UI服务
     * @param {*} [context={}] 附加上下文
     * @param {*} [params={}] 附加参数
     * @memberof AppSysAction
     */
    execute(UIDataParam: IUIDataParam, UIEnvironmentParam: IUIEnvironmentParam, UIUtilParam: IUIUtilParam, deUIService?: any, context?: any, params?: any): Promise<void>;
}
