import { IUIDataParam, IUIEnvironmentParam, IUIUtilParam } from '../../interface';
import { AppDEUIAction } from './app-ui-action';
export declare class AppBackEndAction extends AppDEUIAction {
    /**
     * 初始化AppBackEndAction
     *
     * @memberof AppBackEndAction
     */
    constructor(opts: any, context?: any);
    /**
     * 执行界面行为
     *
     * @param UIDataParam 操作数据参数
     * @param UIEnvironmentParam 操作环境参数
     * @param UIUtilParam 操作工具参数
     * @param deUIService 界面UI服务
     * @param context 附加上下文
     * @param params 附加参数
     *
     * @memberof AppBackEndAction
     */
    execute(UIDataParam: IUIDataParam, UIEnvironmentParam: IUIEnvironmentParam, UIUtilParam: IUIUtilParam, deUIService?: any, context?: any, params?: any): Promise<void>;
}
