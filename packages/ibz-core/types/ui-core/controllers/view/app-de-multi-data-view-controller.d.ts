import { IPSAppCodeList, IPSAppDEMultiDataView } from '@ibiz/dynamic-model-api';
import { IParam, IAppDEMultiDataViewController } from '../../../interface';
import { AppDEViewController } from './app-de-view-controller';
/**
 * @description 多数据视图控制器基类
 * @export
 * @class AppDEMultiDataViewController
 * @extends {AppDEViewController}
 * @implements {IAppDEMultiDataViewController}
 */
export declare class AppDEMultiDataViewController extends AppDEViewController implements IAppDEMultiDataViewController {
    /**
     * @description 多数据视图基类实例对象
     * @type {IPSAppDEMultiDataView}
     * @memberof AppDEMultiDataViewController
     */
    viewInstance: IPSAppDEMultiDataView;
    /**
     * @description 是否开启快速分组
     * @type {boolean}
     * @memberof AppDEMultiDataViewController
     */
    isEnableQuickGroup: boolean;
    /**
     * @description 快速分组代码表
     * @type {IPSAppCodeList}
     * @memberof AppDEMultiDataViewController
     */
    quickGroupCodeList?: IPSAppCodeList;
    /**
     * @description 快速分组是否有抛值
     * @type {boolean}
     * @memberof AppDEMultiDataViewController
     */
    isEmitQuickGroupValue: boolean;
    /**
     * @description 快速分组模型数据
     * @type {Array<IParam>}
     * @memberof AppDEMultiDataViewController
     */
    quickGroupModel: Array<IParam>;
    /**
     * @description 是否为多选
     * @type {boolean}
     * @memberof AppDEMultiDataViewController
     */
    isMultiple: boolean;
    /**
     * @description 是否展开搜索表单
     * @type {boolean}
     * @memberof AppDEMultiDataViewController
     */
    isExpandSearchForm: boolean;
    /**
     * @description 快速分组存储参数
     * @type {IParam}
     * @memberof AppDEMultiDataViewController
     */
    quickGroupParam: IParam;
    /**
     * @description 快速搜索参数
     * @type {IParam}
     * @memberof AppDEMultiDataViewController
     */
    searchParam: IParam;
    /**
     * @description 初始化输入数据
     * @param {*} opts
     * @memberof AppDEMultiDataViewController
     */
    initInputData(opts: any): void;
    /**
     * @description 视图基础数据初始化
     * @memberof AppDEMultiDataViewController
     */
    viewBasicInit(): void;
    /**
     * @description 初始化快速分组
     * @return {*}
     * @memberof AppDEMultiDataViewController
     */
    initQuickGroup(): Promise<void>;
    /**
     * @description 处理快速分组模型动态数据部分(%xxx%)
     * @param {any[]} items
     * @return {*}
     * @memberof AppDEMultiDataViewController
     */
    handleQuickGroup(items: any[]): any[];
    /**
     * @description 获取快速分组默认选中项
     * @param {any[]} items
     * @return {*}
     * @memberof AppDEMultiDataViewController
     */
    getQuickGroupDefaultSelect(items: any[]): any;
    /**
     * @description 快速搜索
     * @param {string} query
     * @memberof AppDEMultiDataViewController
     */
    quickSearch(query: string): void;
    /**
     * @description 快速分组值变化
     * @param {*} event
     * @memberof AppDEMultiDataViewController
     */
    quickGroupValueChange(event: any): void;
    /**
     * @description 部件事件处理
     * @param {string} controlname 部件名称
     * @param {string} action 行为
     * @param {*} data 数据
     * @memberof AppDEMultiDataViewController
     */
    handleCtrlEvent(controlname: string, action: string, data: any): void;
}
