import { IPSAppDEView } from '@ibiz/dynamic-model-api';
import { IAppDEViewController, IEntityService, IUIService } from '../../../interface';
import { AppViewControllerBase } from './app-view-controller-base';
export declare class AppDEViewController extends AppViewControllerBase implements IAppDEViewController {
    /**
     * @description 视图模型对象
     * @type {IPSAppDEView}
     * @memberof AppDEViewController
     */
    viewInstance: IPSAppDEView;
    /**
     * @description 实体UI服务
     * @type {IUIService}
     * @memberof AppDEViewController
     */
    appUIService: IUIService;
    /**
     * @description 实体服务对象
     * @type {*}
     * @memberof AppViewControllerBase
     */
    appDataService: IEntityService;
    /**
     * @description 视图实体代码标识
     * @readonly
     * @type {string}
     * @memberof AppDEViewController
     */
    get appDeCodeName(): string;
    /**
     * @description 视图基础数据初始化
     * @memberof AppDEViewController
     */
    viewBasicInit(): void;
    /**
     * @description 初始化实体服务对象
     * @memberof AppDEViewController
     */
    initAppDataService(): Promise<void>;
    /**
     * @description 初始化应用界面服务
     * @memberof AppDEViewController
     */
    initAppUIService(): Promise<void>;
}
