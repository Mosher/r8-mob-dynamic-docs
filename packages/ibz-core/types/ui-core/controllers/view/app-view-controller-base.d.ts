import { IPSAppView } from '@ibiz/dynamic-model-api';
import { Subject, Subscription } from 'rxjs';
import { IAppViewControllerBase, IParam, IUIDataParam, IUIEnvironmentParam, IAppViewHooks, IViewStateParam } from '../../../interface';
import { AppUIControllerBase } from '../base';
/**
 * 视图控制器基类
 *
 * @export
 * @class AppViewControllerBase
 */
export declare class AppViewControllerBase extends AppUIControllerBase implements IAppViewControllerBase {
    /**
     * @description 视图模型路径
     * @protected
     * @type {string}
     * @memberof AppViewControllerBase
     */
    protected viewPath: string;
    /**
     * @description 视图默认加载
     * @type {boolean}
     * @memberof AppViewControllerBase
     */
    isLoadDefault: boolean;
    /**
     * @description 是否显示视图标题栏
     * @type {boolean}
     * @memberof AppViewControllerBase
     */
    isShowCaptionBar: boolean;
    /**
     * @description 视图打开模式
     * @type {('ROUTE' | 'MODEL' | 'EMBEDDED')}
     * @memberof AppViewControllerBase
     */
    viewShowMode: 'ROUTE' | 'MODEL' | 'EMBEDDED';
    /**
     * @description 视图钩子对象
     * @type {IAppViewHooks}
     * @memberof AppViewControllerBase
     */
    hooks: IAppViewHooks;
    /**
     * @description 应用上下文
     * @type {IParam}
     * @memberof AppViewControllerBase
     */
    context: IParam;
    /**
     * @description 数据部件名称
     * @type {string}
     * @memberof AppViewControllerBase
     */
    xDataControlName: string;
    /**
     * @description 视图参数
     * @type {IParam}
     * @memberof AppViewControllerBase
     */
    viewParam: IParam;
    /**
     * @description 视图导航数据
     * @type {Array<IParam>}
     * @memberof AppViewControllerBase
     */
    navDatas: Array<IParam>;
    /**
     * @description 视图操作参数集合
     * @type {*}
     * @memberof AppViewControllerBase
     */
    viewCtx: any;
    /**
     * @description 视图传递对象
     * @type {Subject<IViewStateParam>}
     * @memberof AppViewControllerBase
     */
    viewState: Subject<IViewStateParam>;
    /**
     * @description 视图模型对象
     * @type {IPSAppView}
     * @memberof AppViewControllerBase
     */
    viewInstance: IPSAppView;
    modelData?: any;
    /**
     * @description 自定义视图导航上下文集合
     * @protected
     * @type {*}
     * @memberof AppViewControllerBase
     */
    protected customViewNavContexts: any;
    /**
     * @description 自定义视图导航参数集合
     * @protected
     * @type {*}
     * @memberof AppViewControllerBase
     */
    protected customViewParams: any;
    /**
     * @description 模型服务对象
     * @type {*}
     * @memberof AppViewControllerBase
     */
    modelService: any;
    /**
     * @description 视图消息服务
     * @type {*}
     * @memberof AppViewControllerBase
     */
    viewMessageService: any;
    /**
     * @description 视图状态订阅对象
     * @type {(Subscription | undefined)}
     * @memberof AppViewControllerBase
     */
    viewStateEvent: Subscription | undefined;
    /**
     * @description 计数器服务对象集合
     * @type {Array<any>}
     * @memberof AppViewControllerBase
     */
    counterServiceArray: Array<any>;
    /**
     * @description 视图是否完成挂载
     * @type {boolean}
     * @memberof AppViewControllerBase
     */
    hasViewMounted: boolean;
    /**
     * @description 视图是否完成加载
     * @type {boolean}
     * @memberof AppViewControllerBase
     */
    viewIsLoaded: boolean;
    /**
     * @description 界面触发逻辑Map
     * @type {Map<string, any>}
     * @memberof AppViewControllerBase
     */
    viewTriggerLogicMap: Map<string, any>;
    /**
     * @description 挂载状态集合
     * @type {Map<string, boolean>}
     * @memberof AppViewControllerBase
     */
    mountedMap: Map<string, boolean>;
    /**
     * @description 视图部件引用控制器集合
     * @type {Map<string, IParam>}
     * @memberof AppViewControllerBase
     */
    ctrlRefsMap: Map<string, IParam>;
    /**
     * @description 视图引擎
     * @type {*}
     * @memberof AppViewControllerBase
     */
    engine: any;
    /**
     * @description 获取顶层视图
     * @return {*}
     * @memberof AppViewControllerBase
     */
    getTopView(): any;
    /**
     * @description 获取父级视图
     * @return {*}
     * @memberof AppViewControllerBase
     */
    getParentView(): any;
    /**
     * @description 获取视图激活数据
     * @return {*}  {(IParam | null)}
     * @memberof AppViewControllerBase
     */
    getData(): IParam | null;
    /**
     * @description 获取视图激活数据集
     * @return {*}  {(Array<IParam> | null)}
     * @memberof AppViewControllerBase
     */
    getDatas(): Array<IParam> | null;
    /**
     * @description 获取逻辑UI数据
     * @param {(Array<IParam> | null)} data 数据集合
     * @return {*}  {IUIDataParam}
     * @memberof AppViewControllerBase
     */
    getUIDataParam(data: Array<IParam> | null): IUIDataParam;
    /**
     * @description 获取逻辑环境数据
     * @return {*}  {IUIEnvironmentParam}
     * @memberof AppViewControllerBase
     */
    getUIEnvironmentParam(): IUIEnvironmentParam;
    /**
     * @description 通过名称获取部件
     * @param {string} name 部件名称
     * @return {*}  {(IParam | undefined)}
     * @memberof AppViewControllerBase
     */
    getCtrlByName(name: string): IParam | undefined;
    /**
     * Creates an instance of AppViewControllerBase.
     * @param {*} opts 构造参数
     * @memberof AppViewControllerBase
     */
    constructor(opts: any);
    /**
     * @description 初始化输入数据
     * @param {*} opts 输入数据
     * @memberof AppViewControllerBase
     */
    initInputData(opts: any): void;
    /**
     * @description 视图初始化
     * @return {*}
     * @memberof AppViewControllerBase
     */
    viewInit(): Promise<boolean>;
    /**
     * @description 视图基础数据初始化
     * @memberof AppViewControllerBase
     */
    viewBasicInit(): void;
    /**
     * @description 初始化模型服务
     * @memberof AppViewControllerBase
     */
    initModelService(): Promise<void>;
    /**
     * @description 初始化视图实例
     * @memberof AppViewControllerBase
     */
    initViewInstance(): Promise<void>;
    /**
     * @description 初始化挂载状态集合
     * @memberof AppViewControllerBase
     */
    initMountedMap(): void;
    /**
     * @description 初始化视图操作参数
     * @param {*} [args] 视图操作参数
     * @memberof AppViewControllerBase
     */
    initViewCtx(args?: any): void;
    /**
     * @description 初始化视图消息服务
     * @memberof AppViewControllerBase
     */
    initViewMessageService(): Promise<void>;
    /**
     * @description 初始化计数器服务
     * @memberof AppViewControllerBase
     */
    initCounterService(): Promise<void>;
    /**
     * @description 引擎初始化
     * @param {*} [opts={}] 初始化参数
     * @memberof AppViewControllerBase
     */
    engineInit(opts?: any): void;
    /**
     * @description 初始化实体服务对象
     * @memberof AppViewControllerBase
     */
    initAppDataService(): Promise<void>;
    /**
     * @description 初始化应用界面服务
     * @memberof AppViewControllerBase
     */
    initAppUIService(): Promise<void>;
    /**
     * @description 设置已经绘制完成状态
     * @param {string} [name='self'] 部件名
     * @param {*} [data] 数据
     * @memberof AppViewControllerBase
     */
    setIsMounted(name?: string, data?: any): void;
    /**
     * @description 视图挂载
     * @memberof AppViewControllerBase
     */
    viewMounted(): void;
    /**
     * @description 视图销毁
     * @memberof AppViewControllerBase
     */
    viewDestroy(): Promise<void>;
    /**
     * @description 计数器刷新
     * @memberof AppViewControllerBase
     */
    counterRefresh(): void;
    /**
     * @description 初始化沙箱实例
     * @param {*} args 初始化参数
     * @memberof AppViewControllerBase
     */
    initSandBoxInst(args: any): Promise<void>;
    /**
     * @description 处理自定义视图导航数据
     * @param {IParam} context 上下文
     * @param {IParam} viewParam 视图参数
     * @memberof AppViewControllerBase
     */
    handleCustomViewData(context: IParam, viewParam: IParam): void;
    /**
     * @description 处理其他数据(多实例)
     * @param {IParam} context 上下文
     * @param {IParam} viewParam 视图参数
     * @memberof AppViewControllerBase
     */
    handleOtherViewData(context: IParam, viewParam: IParam): void;
    /**
     * @description 处理指定视图控制关系将父键转为父实体上下文
     * @memberof AppViewControllerBase
     */
    handleviewRes(): Promise<void>;
    /**
     * @description 处理自定义视图数据逻辑
     * @param {IParam} context 上下文
     * @param {IParam} viewParam 视图参数
     * @param {*} curNavData 当前导航数据
     * @param {*} tempData 当前数据
     * @param {string} item 数据项名称
     * @memberof AppViewControllerBase
     */
    handleCustomDataLogic(context: IParam, viewParam: IParam, curNavData: any, tempData: any, item: string): void;
    /**
     * @description 处理部件事件
     * @param {string} controlname 部件名称
     * @param {string} action 部件行为
     * @param {*} data 数据
     * @memberof AppViewControllerBase
     */
    handleCtrlEvent(controlname: string, action: string, data: any): void;
    /**
     * @description 处理工具栏点击事件
     * @param {string} tag 标识
     * @param {*} event 事件源
     * @return {*}
     * @memberof AppViewControllerBase
     */
    handleToolBarClick(tag: string, event: MouseEvent): void;
    /**
     * @description 视图刷新
     * @param {*} [arg] 刷新参数
     * @memberof AppViewControllerBase
     */
    refresh(arg?: any): void;
    /**
     * @description 关闭视图
     * @param {*} [args] 关闭参数
     * @memberof AppViewControllerBase
     */
    closeView(args?: any): void;
    /**
     * @description 初始化快速分组
     * @memberof AppViewControllerBase
     */
    initQuickGroup(): Promise<void>;
    /**
     * @description 初始化视图逻辑
     * @memberof AppViewControllerBase
     */
    initViewLogic(): Promise<void>;
    /**
     * @description 处理视图自定义事件
     * @param {string} name 事件名
     * @param {*} data 数据
     * @param {*} args 额外参数
     * @memberof AppViewControllerBase
     */
    handleViewCustomEvent(name: string, data: any, args: any): void;
    /**
     * @description 处理视图定时器逻辑
     * @memberof AppViewControllerBase
     */
    handleTimerLogic(): void;
    /**
     * @description 销毁视图定时器逻辑
     * @memberof AppViewControllerBase
     */
    destroyLogicTimer(): void;
    /**
     * @description 视图事件 抛出
     * @param {string} action 抛出行为
     * @param {IParam} data 抛出数据
     * @memberof AppViewControllerBase
     */
    viewEvent(action: string, data?: IParam): void;
}
