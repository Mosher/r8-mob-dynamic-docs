import { MobChartViewEngine } from '../../../engine';
import { IMobChartViewController } from '../../../interface';
import { AppDEMultiDataViewController } from './app-de-multi-data-view-controller';
export declare class MobChartViewController extends AppDEMultiDataViewController implements IMobChartViewController {
    /**
     * 视图实例
     *
     * @public
     * @type {IPSAppDEMobChartView}
     * @memberof MobChartViewController
     */
    viewInstance: any;
    /**
     * @description 视图引擎
     * @type {MobChartViewEngine}
     * @memberof MobChartViewController
     */
    engine: MobChartViewEngine;
    /**
     * @description 视图引擎初始化
     * @param {*} [opts={}] 初始化参数
     * @memberof MobChartViewController
     */
    engineInit(opts?: any): void;
    /**
     * @description 视图基础数据初始化
     * @memberof MobChartViewController
     */
    viewBasicInit(): void;
}
