import { IPSAppDEMobCustomView } from '@ibiz/dynamic-model-api';
import { MobCustomViewEngine } from '../../../engine';
import { IMobCustomViewController } from '../../../interface';
import { AppDEViewController } from './app-de-view-controller';
export declare class MobCustomViewController extends AppDEViewController implements IMobCustomViewController {
    /**
     * @description 视图实例
     * @type {IPSAppDEMobCustomView}
     * @memberof MobCustomViewController
     */
    viewInstance: IPSAppDEMobCustomView;
    /**
     * @description 视图引擎
     * @type {MobCustomViewEngine}
     * @memberof MobCustomViewController
     */
    engine: MobCustomViewEngine;
    /**
     * @description 视图引擎初始化
     * @param {*} [opts={}]
     * @memberof MobCustomViewController
     */
    engineInit(opts?: any): void;
}
