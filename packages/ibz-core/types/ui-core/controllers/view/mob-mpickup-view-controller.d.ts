import { MobMPickupViewEngine } from '../../../engine';
import { IMobMPickUpViewController } from '../../../interface';
import { MobPickUpViewController } from './mob-pickup-view-controller';
export declare class MobMPickUpViewController extends MobPickUpViewController implements IMobMPickUpViewController {
    /**
     * @description 视图引擎
     * @type {MobMPickupViewEngine}
     * @memberof MobMPickUpViewController
     */
    engine: MobMPickupViewEngine;
}
