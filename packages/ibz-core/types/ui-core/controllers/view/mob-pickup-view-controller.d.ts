import { IPSAppDEMobPickupView } from '@ibiz/dynamic-model-api';
import { MobPickupViewEngine } from '../../../engine';
import { IMobPickUpViewController } from '../../../interface';
import { AppDEViewController } from './app-de-view-controller';
export declare class MobPickUpViewController extends AppDEViewController implements IMobPickUpViewController {
    /**
     * @description 数据选择视图实例
     * @type {IPSAppDEMobPickupView}
     * @memberof MobPickUpViewController
     */
    viewInstance: IPSAppDEMobPickupView;
    /**
     * @description 视图引擎
     * @type {MobPickupViewEngine}
     * @memberof MobPickUpViewController
     */
    engine: MobPickupViewEngine;
    /**
     * @description 视图选中数据
     * @type {any[]}
     * @memberof MobPickUpViewController
     */
    viewSelections: any[];
    /**
     * @description 视图引擎初始化
     * @param {*} [opts={}] 初始化参数
     * @memberof MobPickUpViewController
     */
    engineInit(opts?: any): void;
    /**
     * @description 确定
     * @memberof MobPickUpViewController
     */
    ok(): void;
    /**
     * @description 取消
     * @memberof MobPickUpViewController
     */
    cancel(): void;
}
