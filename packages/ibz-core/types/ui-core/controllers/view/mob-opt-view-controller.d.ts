import { IPSAppDEMobEditView } from '@ibiz/dynamic-model-api';
import { MobOptionViewEngine } from '../../../engine';
import { AppDEViewController } from './app-de-view-controller';
import { IMobEditViewController } from '../../../interface';
export declare class MobOptViewController extends AppDEViewController implements IMobEditViewController {
    /**
     * 移动端选项操作视图实例
     *
     * @public
     * @type { IPSAppDEMobOptView }
     * @memberof MobOptViewController
     */
    viewInstance: IPSAppDEMobEditView;
    /**
     * 视图引擎
     *
     * @public
     * @type {Engine}
     * @memberof MobOptViewController
     */
    engine: MobOptionViewEngine;
    /**
     * 视图引擎初始化
     *
     * @public
     * @memberof MobOptViewController
     */
    engineInit(opts?: any): void;
    /**
     * @description 视图选中数据
     * @type {any[]}
     * @memberof MobOptViewController
     */
    viewSelections: any[];
    /**
     * @description 确定
     * @memberof MobOptViewController
     */
    ok(): Promise<void>;
    /**
     * @description 取消
     * @memberof MobOptViewController
     */
    cancel(): void;
}
