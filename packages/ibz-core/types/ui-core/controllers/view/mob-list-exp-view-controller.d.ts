import { IPSAppDEMobListExplorerView } from '@ibiz/dynamic-model-api';
import { MobListExpViewEngine } from '../../../engine';
import { IMobListExpViewController } from '../../../interface';
import { AppDEExpViewController } from './app-de-exp-view-controller';
export declare class MobListExpViewController extends AppDEExpViewController implements IMobListExpViewController {
    /**
     * 移动端列表导航视图实例
     *
     * @public
     * @type { IPSAppDEMobListExpView }
     * @memberof MobListExpViewController
     */
    viewInstance: IPSAppDEMobListExplorerView;
    /**
     * @description 移动端列表导航视图引擎实例对象
     * @type {MobListExpViewEngine}
     * @memberof MobListExpViewController
     */
    engine: MobListExpViewEngine;
    /**
     * @description 引擎初始化
     * @memberof MobListExpViewController
     */
    engineInit(): void;
}
