import { IPSAppDEMobTabExplorerView } from '@ibiz/dynamic-model-api';
import { MobTabExpViewEngine } from '../../../engine';
import { IMobTabExpViewController } from '../../../interface';
import { AppDEViewController } from '.';
export declare class MobTabExpViewController extends AppDEViewController implements IMobTabExpViewController {
    /**
     * 视图实例
     *
     * @public
     * @type {IPSAppDEMobTabExplorerView}
     * @memberof MobTabExpViewController
     */
    viewInstance: IPSAppDEMobTabExplorerView;
    /**
     * 视图引擎
     *
     * @public
     * @type {MobTabExpViewEngine}
     * @memberof MobTabExpViewController
     */
    engine: MobTabExpViewEngine;
    /**
     * 视图引擎初始化
     *
     * @public
     * @memberof MobTabExpViewController
     */
    engineInit(opts?: any): void;
}
