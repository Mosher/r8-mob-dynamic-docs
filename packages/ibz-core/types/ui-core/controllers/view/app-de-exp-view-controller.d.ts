import { IPSAppDEExplorerView } from '@ibiz/dynamic-model-api';
import { IAppDEExpViewController } from '../../../interface';
import { AppDEViewController } from './app-de-view-controller';
export declare class AppDEExpViewController extends AppDEViewController implements IAppDEExpViewController {
    /**
     * @description 实体导航视图视图实例对象
     * @type {IPSAppDEExplorerView}
     * @memberof IAppDEExpViewController
     */
    viewInstance: IPSAppDEExplorerView;
}
