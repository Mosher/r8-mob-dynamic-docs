import { IPSAppDEMobTreeExplorerView } from '@ibiz/dynamic-model-api';
import { MobTreeExpViewEngine } from '../../../engine';
import { AppDEExpViewController } from './app-de-exp-view-controller';
import { IMobTreeExpViewController } from '../../../interface';
export declare class MobTreeExpViewController extends AppDEExpViewController implements IMobTreeExpViewController {
    /**
     * 移动端树导航视图实例
     *
     * @public
     * @type { IPSAppDEMobTreeExpView }
     * @memberof MobTreeExpViewController
     */
    viewInstance: IPSAppDEMobTreeExplorerView;
    /**
   * @description 移动端列表导航视图引擎实例对象
   * @type {MobChartExpViewEngine}
   * @memberof MobChartExpViewController
   */
    engine: MobTreeExpViewEngine;
    /**
     * @description 引擎初始化
     * @memberof MobChartExpViewController
     */
    engineInit(): void;
}
