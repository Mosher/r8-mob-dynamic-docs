import { IPSAppDEMobMapView } from '@ibiz/dynamic-model-api';
import { MobMapViewEngine } from '../../../engine';
import { IMobMapViewController } from '../../../interface';
import { AppDEMultiDataViewController } from '.';
export declare class MobMapViewController extends AppDEMultiDataViewController implements IMobMapViewController {
    /**
     * 视图实例
     *
     * @public
     * @type {IPSAppDEMobMapView}
     * @memberof MobMapViewController
     */
    viewInstance: IPSAppDEMobMapView;
    /**
     * 视图引擎
     *
     * @public
     * @type {MobMapViewEngine}
     * @memberof MobMapViewController
     */
    engine: MobMapViewEngine;
    /**
     * 视图引擎初始化
     *
     * @public
     * @memberof MobMapViewController
     */
    engineInit(opts?: any): void;
}
