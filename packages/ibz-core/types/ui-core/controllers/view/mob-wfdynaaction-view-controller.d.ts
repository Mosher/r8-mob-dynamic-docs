import { IPSAppDEMobWFDynaActionView, IPSDEForm } from '@ibiz/dynamic-model-api';
import { AppDEViewController } from './app-de-view-controller';
import { IMobWFDynaActionViewController } from '../../../interface';
export declare class MobWFDynaActionViewController extends AppDEViewController implements IMobWFDynaActionViewController {
    /**
     * @description 移动端动态工作流操作视图实例
     * @type {IPSAppDEMobWFDynaActionView}
     * @memberof MobWFDynaActionViewController
     */
    viewInstance: IPSAppDEMobWFDynaActionView;
    /**
     * @description 表单实例
     * @type {IPSDEForm}
     * @memberof MobWFDynaActionViewController
     */
    editFormInstance: IPSDEForm;
    /**
     * @description 初始化挂载集合
     * @memberof MobWFDynaActionViewController
     */
    initMountedMap(): void;
    /**
     * @description 视图挂载
     * @memberof MobWFDynaActionViewController
     */
    viewMounted(): void;
    /**
     * @description 计算激活表单
     * @param {*} inputForm
     * @memberof MobWFDynaActionViewController
     */
    computeActivedForm(inputForm: any): void;
    /**
     * @description 确认
     * @memberof MobWFDynaActionViewController
     */
    handleOk(): void;
    /**
     * @description 取消
     * @memberof MobWFDynaActionViewController
     */
    handleCancel(): void;
}
