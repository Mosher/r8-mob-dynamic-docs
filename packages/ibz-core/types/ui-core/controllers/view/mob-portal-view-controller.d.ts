import { AppViewControllerBase } from './app-view-controller-base';
import { IMobPortalViewController } from '../../../interface';
/**
 * 移动端应用看板视图控制器
 *
 * @class MobPortalViewController
 * @extends AppViewControllerBase
 * @implements IAppMobPortalviewController
 */
export declare class MobPortalViewController extends AppViewControllerBase implements IMobPortalViewController {
}
