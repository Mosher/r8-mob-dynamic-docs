import { IPSAppDEMobWFDynaEditView, IPSDEForm } from '@ibiz/dynamic-model-api';
import { MobWFDynaEditViewEngine } from '../../../engine';
import { AppDEViewController } from './app-de-view-controller';
import { IMobWFDynaEditViewController, IParam } from '../../../interface';
export declare class MobWFDynaEditViewController extends AppDEViewController implements IMobWFDynaEditViewController {
    /**
     * 移动端动态工作流编辑视图实例
     *
     * @public
     * @type { IPSAppDEMobWFDynaEditView }
     * @memberof MobWFDynaEditViewController
     */
    viewInstance: IPSAppDEMobWFDynaEditView;
    /**
     * @description 表单实例
     * @type {IPSDEForm}
     * @memberof MobWFDynaEditViewController
     */
    editFormInstance: IPSDEForm;
    /**
     * @description 工具栏模型数据
     * @type {Array<any>}
     * @memberof MobWFDynaEditViewController
     */
    linkModel: Array<any>;
    /**
     * @description 是否可编辑
     * @type {boolean}
     * @memberof MobWFDynaEditViewController
     */
    isEditable: boolean;
    /**
     * @description 视图引用数据
     * @type {*}
     * @memberof MobWFDynaEditViewController
     */
    viewRefData: any;
    /**
     * @description 工作流附加功能类型映射关系对象
     * @type {*}
     * @memberof MobWFDynaEditViewController
     */
    wfAddiFeatureRef: any;
    /**
     * @description 移动端树视图引擎对象
     * @type {MobWFDynaEditViewEngine}
     * @memberof MobWFDynaEditViewController
     */
    engine: MobWFDynaEditViewEngine;
    /**
     * @description 初始化视图实例对象
     * @memberof MobWFDynaEditViewController
     */
    initViewInstance(): Promise<void>;
    /**
     * @description 初始化挂载集合
     * @memberof MobWFDynaEditViewController
     */
    initMountedMap(): void;
    /**
     * @description 引擎初始化
     * @param {IParam} [opts={}]
     * @memberof MobWFDynaEditViewController
     */
    engineInit(opts?: IParam): void;
    /**
     * @description 视图挂载
     * @memberof MobWFDynaEditViewController
     */
    viewMounted(): void;
    /**
     * @description 计算激活表单
     * @param {*} inputForm
     * @memberof MobWFDynaEditViewController
     */
    computeActivedForm(inputForm: any): void;
    /**
     * @description 设置已经绘制完成状态
     * @param {string} [name='self']
     * @memberof MobWFDynaEditViewController
     */
    setIsMounted(name?: string): void;
    /**
     * @description 获取工具栏按钮
     * @param {*} arg
     * @return {*}  {Promise<any>}
     * @memberof MobWFDynaEditViewController
     */
    getWFLinkModel(arg: any): Promise<any>;
    /**
     * @description 动态工具栏点击
     * @param {*} linkItem
     * @param {*} $event
     * @memberof MobWFDynaEditViewController
     */
    dynamicToolbarClick(linkItem: any, $event: any): void;
    /**
     * @description 处理工作流辅助功能
     * @param {*} linkItem
     * @return {*}
     * @memberof MobWFDynaEditViewController
     */
    handleWFAddiFeature(linkItem: any): void;
    /**
     * @description 提交工作流辅助功能
     * @param {*} linkItem
     * @param {*} submitData
     * @return {*}
     * @memberof MobWFDynaEditViewController
     */
    submitWFAddiFeature(linkItem: any, submitData: any): void;
    /**
     * @description 将待办任务标记为已读
     * @param {*} data 业务数据
     * @memberof MobWFDynaEditViewController
     */
    readTask(data: any): void;
}
