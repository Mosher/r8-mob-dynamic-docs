import { IPSAppDEMobMEditView } from '@ibiz/dynamic-model-api';
import { MobMEditView9Engine } from '../../../engine';
import { IMobMEditViewController } from '../../../interface';
import { AppDEMultiDataViewController } from './app-de-multi-data-view-controller';
export declare class MobMEditViewController extends AppDEMultiDataViewController implements IMobMEditViewController {
    /**
     * 视图实例
     *
     * @public
     * @type {IPSAppDEMobMEditView}
     * @memberof MobMEditViewController
     */
    viewInstance: IPSAppDEMobMEditView;
    /**
     * 视图引擎
     *
     * @public
     * @type {MobMDViewEngine}
     * @memberof MobMEditViewController
     */
    engine: MobMEditView9Engine;
    /**
     * 视图引擎初始化
     *
     * @public
     * @memberof MobMEditViewController
     */
    engineInit(opts?: any): void;
}
