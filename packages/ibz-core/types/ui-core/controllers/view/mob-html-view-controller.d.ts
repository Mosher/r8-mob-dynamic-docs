import { IPSAppDEMobHtmlView } from '@ibiz/dynamic-model-api';
import { AppDEViewController } from './app-de-view-controller';
import { IMobHtmlViewController } from '../../../interface';
export declare class MobHtmlViewController extends AppDEViewController implements IMobHtmlViewController {
    /**
     * @description 视图实例
     * @type {IPSAppDEMobHtmlView}
     * @memberof MobHtmlViewController
     */
    viewInstance: IPSAppDEMobHtmlView;
}
