import { IPSAppDEMobChartExplorerView } from '@ibiz/dynamic-model-api';
import { IMobChartExpViewController } from '../../../interface';
import { AppDEExpViewController } from './app-de-exp-view-controller';
import { MobChartExpViewEngine } from '../../../engine';
export declare class MobChartExpViewController extends AppDEExpViewController implements IMobChartExpViewController {
    /**
     * 移动端图表导航视图实例
     *
     * @public
     * @type { IPSAppDEMobChartExpView }
     * @memberof MobChartExpViewController
     */
    viewInstance: IPSAppDEMobChartExplorerView;
    /**
   * @description 移动端图表导航视图引擎实例对象
   * @type {MobChartExpViewEngine}
   * @memberof MobChartExpViewController
   */
    engine: MobChartExpViewEngine;
    /**
     * @description 引擎初始化
     * @memberof MobChartExpViewController
     */
    engineInit(): void;
}
