import { IPSAppDEMobDashboardView } from '@ibiz/dynamic-model-api';
import { MobDEDashboardViewEngine } from '../../../engine';
import { IMobDEDashboardViewController } from '../../../interface';
import { AppDEViewController } from './app-de-view-controller';
/**
 * 移动端实体数据看板视图控制器
 *
 * @class MobDEDashboardViewController
 * @extends AppDEViewController
 * @implements IMobDEDashboardViewController
 */
export declare class MobDEDashboardViewController extends AppDEViewController implements IMobDEDashboardViewController {
    /**
     * 移动端实体数据看板视图视图实例对象
     *
     * @type {IPSAppDEMobDashboardView}
     * @memberof MobDEDashboardViewController
     */
    viewInstance: IPSAppDEMobDashboardView;
    /**
     * 移动端实体数据看板视图视图引擎
     *
     * @type {MobDEDashboardViewEngine}
     * @memberof MobDEDashboardViewController
     */
    engine: MobDEDashboardViewEngine;
    /**
     * 引擎初始化
     *
     * @memberof MobDEDashboardViewController
     */
    engineInit(): void;
}
