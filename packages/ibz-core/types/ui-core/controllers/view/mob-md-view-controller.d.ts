import { IPSAppDEMobMDView } from '@ibiz/dynamic-model-api';
import { MobMDViewEngine } from '../../../engine';
import { IMobMDViewController } from '../../../interface';
import { AppDEMultiDataViewController } from './app-de-multi-data-view-controller';
export declare class MobMDViewController extends AppDEMultiDataViewController implements IMobMDViewController {
    /**
     * @description 多数据视图实例
     * @type {IPSAppDEMobMDView}
     * @memberof MobMDViewController
     */
    viewInstance: IPSAppDEMobMDView;
    /**
     * @description 视图引擎
     * @type {MobMDViewEngine}
     * @memberof MobMDViewController
     */
    engine: MobMDViewEngine;
    /**
     * @description 视图引擎初始化
     * @param {*} [opts={}] 初始化参数
     * @memberof MobMDViewController
     */
    engineInit(opts?: any): void;
}
