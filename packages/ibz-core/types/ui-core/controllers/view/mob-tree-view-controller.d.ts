import { MobTreeViewEngine } from '../../../engine';
import { IMobTreeViewController } from '../../../interface';
import { AppDEMultiDataViewController } from './app-de-multi-data-view-controller';
/**
 * 移动端树视图控制器
 *
 * @class MobTreeViewController
 * @extends AppDEMultiDataViewController
 * @implements IMobTreeViewController
 */
export declare class MobTreeViewController extends AppDEMultiDataViewController implements IMobTreeViewController {
    /**
     * @description 移动端树视图引擎对象
     * @type {MobTreeViewEngine}
     * @memberof MobTreeViewController
     */
    engine: MobTreeViewEngine;
    /**
     * @description 移动端树视图引擎初始化
     * @param {*} [opts={}] 初始化参数
     * @memberof MobTreeViewController
     */
    engineInit(opts?: any): void;
}
