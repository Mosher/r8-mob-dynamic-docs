import { IPSAppDEMobWizardView } from '@ibiz/dynamic-model-api';
import { AppDEViewController } from './app-de-view-controller';
import { IMobWizardViewController } from '../../../interface';
import { MobWizardViewEngine } from '../../../engine';
/**
 * @description 移动端向导视图控制器
 * @export
 * @class MobWizardViewController
 * @extends {AppDEViewController}
 * @implements {IMobWizardViewController}
 */
export declare class MobWizardViewController extends AppDEViewController implements IMobWizardViewController {
    /**
     * 移动端向导视图实例
     *
     * @public
     * @type { IPSAppDEMobWizardView }
     * @memberof MobWizardViewController
     */
    viewInstance: IPSAppDEMobWizardView;
    /**
     * @description 移动端向导视图引擎
     * @type {MobWizardViewEngine}
     * @memberof MobWizardViewController
     */
    engine: MobWizardViewEngine;
    /**
     * @description 引擎初始化
     * @memberof MobWizardViewController
     */
    engineInit(): void;
}
