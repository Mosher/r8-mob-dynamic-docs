import { IPSAppDEMobMapExplorerView } from '@ibiz/dynamic-model-api';
import { MobMapExpViewEngine } from '../../../engine';
import { AppDEExpViewController } from './app-de-exp-view-controller';
import { IMobMapExpViewController } from '../../../interface';
export declare class MobMapExpViewController extends AppDEExpViewController implements IMobMapExpViewController {
    /**
     * 移动端地图导航视图实例
     *
     * @public
     * @type { IPSAppDEMobMapExpView }
     * @memberof MobMapExpViewController
     */
    viewInstance: IPSAppDEMobMapExplorerView;
    /**
   * @description 移动端地图导航视图引擎实例对象
   * @type {MobChartExpViewEngine}
   * @memberof MobChartExpViewController
   */
    engine: MobMapExpViewEngine;
    /**
     * @description 引擎初始化
     * @memberof MobChartExpViewController
     */
    engineInit(): void;
}
