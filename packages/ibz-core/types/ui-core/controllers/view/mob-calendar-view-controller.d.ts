import { IPSAppDEMobCalendarView } from '@ibiz/dynamic-model-api';
import { MobCalendarViewEngine } from '../../../engine';
import { IMobCalendarViewController } from '../../../interface';
import { AppDEMultiDataViewController } from './app-de-multi-data-view-controller';
export declare class MobCalendarViewController extends AppDEMultiDataViewController implements IMobCalendarViewController {
    /**
     * 视图实例
     *
     * @public
     * @type {IPSAppDEMobMDView}
     * @memberof MobCalendarViewController
     */
    viewInstance: IPSAppDEMobCalendarView;
    /**
     * 视图引擎
     *
     * @public
     * @type {MobMDViewEngine}
     * @memberof MobCalendarViewController
     */
    engine: MobCalendarViewEngine;
    /**
     * 视图引擎初始化
     *
     * @public
     * @memberof MobCalendarViewController
     */
    engineInit(opts?: any): void;
}
