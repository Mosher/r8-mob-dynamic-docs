import { IPSAppIndexView } from '@ibiz/dynamic-model-api';
import { IMobIndexViewController } from '../../../interface';
import { AppViewControllerBase } from './app-view-controller-base';
/**
 * 应用首页视图控制器
 *
 * @export
 * @class IndexViewController
 */
export declare class MobIndexViewController extends AppViewControllerBase implements IMobIndexViewController {
    /**
     * 视图实例
     *
     * @type {IPSAppIndexView}
     * @memberof MobIndexViewController
     */
    viewInstance: IPSAppIndexView;
}
