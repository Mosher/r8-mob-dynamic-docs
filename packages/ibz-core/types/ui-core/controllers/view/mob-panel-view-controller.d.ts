import { IPSAppDEMobPanelView } from '@ibiz/dynamic-model-api';
import { IMobPanelViewController } from '../../../interface';
import { AppDEViewController } from './app-de-view-controller';
export declare class MobPanelViewController extends AppDEViewController implements IMobPanelViewController {
    /**
     * @description 面板视图实例
     * @type {IPSAppDEMobPanelView}
     * @memberof MobPanelViewController
     */
    viewInstance: IPSAppDEMobPanelView;
    /**
     * @description 视图基础数据初始化
     * @memberof MobPanelViewController
     */
    viewBasicInit(): void;
    /**
     * @description 视图刷新
     * @param {*} [arg]
     * @memberof MobPanelViewController
     */
    refresh(arg?: any): void;
}
