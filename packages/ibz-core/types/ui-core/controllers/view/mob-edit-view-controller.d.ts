import { IPSAppDEMobEditView } from '@ibiz/dynamic-model-api';
import { MobEditViewEngine } from '../../../engine';
import { AppDEViewController } from './app-de-view-controller';
import { IMobEditViewController } from '../../../interface';
export declare class MobEditViewController extends AppDEViewController implements IMobEditViewController {
    /**
     * 视图实例
     *
     * @public
     * @type {IPSAppDEMobEditView}
     * @memberof MobEditViewController
     */
    viewInstance: IPSAppDEMobEditView;
    /**
     * 视图引擎
     *
     * @public
     * @type {MobEditViewEngine}
     * @memberof MobEditViewController
     */
    engine: MobEditViewEngine;
    /**
     * 视图引擎初始化
     *
     * @public
     * @memberof MobEditViewController
     */
    engineInit(opts?: any): void;
}
