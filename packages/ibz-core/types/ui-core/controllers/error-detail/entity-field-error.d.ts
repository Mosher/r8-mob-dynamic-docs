/**
 * 错误类型申明
 *
 * @export
 * @class EntityFieldError
 * @extends {Error}
 */
export declare enum EntityFieldErrorCode {
    ERROR_OK = 0,
    ERROR_EMPTY = 1,
    ERROR_DATATYPE = 2,
    ERROR_VALUERULE = 3,
    ERROR_DUPLICATE = 4
}
/**
 * 实体属性错误类
 *
 * @export
 * @class EntityFieldError
 * @extends {Error}
 */
export declare class EntityFieldError {
    /**
     * 属性名称
     *
     * @memberof EntityFieldError
     */
    fieldname: string;
    /**
     * 错误信息
     *
     * @memberof EntityFieldError
     */
    fielderrorinfo: string;
    /**
     * 错误属性类型
     *
     * @memberof EntityFieldError
     */
    fielderrortype: number;
    /**
     * 错误属性逻辑名称
     *
     * @memberof EntityFieldError
     */
    fieldlogicname: string;
    /**
     * i18n
     *
     * @memberof AppError
     */
    i18n: any;
    /**
     * 错误映射
     *
     * @memberof EntityFieldError
     */
    errorMapper: any;
    /**
     * 构造对象
     *
     * @memberof EntityFieldError
     */
    constructor(opts: any);
}
