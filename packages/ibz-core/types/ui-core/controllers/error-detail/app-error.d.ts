/**
 * 错误类型申明
 *
 * @export
 * @class AppError
 * @extends {Error}
 */
export declare enum AppErrorCode {
    OK = 0,
    INTERNALERROR = 1,
    ACCESSDENY = 2,
    INVALIDDATA = 3,
    INVALIDDATAKEYS = 4,
    INPUTERROR = 5,
    DUPLICATEKEY = 6,
    DUPLICATEDATA = 7,
    DELETEREJECT = 8,
    LOGICERROR = 9,
    DATANOTMATCH = 10,
    DELETEDDATA = 11,
    USERCONFIRM = 19,
    NOTIMPL = 20,
    MODELERROR = 21,
    USERERROR = 1000,
    SYSTEMERROR = 2000
}
/**
 * 应用错误类
 *
 * @export
 * @class AppError
 * @extends {Error}
 */
export declare class AppError {
    /**
     * 错误编码
     *
     * @memberof AppError
     */
    code: number;
    /**
     * 错误信息
     *
     * @memberof AppError
     */
    message: string;
    /**
     * 错误详情
     *
     * @memberof AppError
     */
    details: any;
    /**
     * 错误类型
     *
     * @memberof AppError
     */
    type: any;
    /**
     * i18n
     *
     * @memberof AppError
     */
    i18n: any;
    /**
     * 错误映射
     *
     * @memberof AppError
     */
    errorMapper: any;
    /**
     * 构造对象
     *
     * @memberof AppError
     */
    constructor(opts: any);
    /**
     * 处理预置错误
     *
     * @memberof AppError
     */
    handlePreError(opts: any): void;
    /**
     * 处理Entity类型预置错误
     *
     * @memberof AppError
     */
    handleEntityException(opts: any): void;
    /**
     * 处理DataEntityRuntime类型预置错误
     *
     * @memberof AppError
     */
    handleDataEntityRuntimeException(opts: any): void;
}
