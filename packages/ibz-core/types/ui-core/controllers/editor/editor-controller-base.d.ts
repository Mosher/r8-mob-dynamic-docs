import { IPSEditor } from '@ibiz/dynamic-model-api';
import { IAppEditorHooks, IEditorControllerBase, IParam } from '../../../interface';
export declare class EditorControllerBase implements IEditorControllerBase {
    /**
     * 父级项模型（表单项，表格项）
     *
     * @type {*}
     * @memberof EditorControllerBase
     */
    parentItem: any;
    /**
     * 构造 EditorControllerBase 实例
     *
     * @memberof EditorControllerBase
     */
    constructor(opts: any);
    /**
     * 初始化输入数据
     *
     * @public
     * @memberof AppCtrlControllerBase
     */
    initInputData(opts: IParam): void;
    /**
     * 编辑器模型
     *
     * @type {IPSEditor}
     * @memberof EditorControllerBase
     */
    editorInstance: IPSEditor;
    /**
     * 模型服务
     *
     * @type {IPSEditor}
     * @memberof EditorControllerBase
     */
    modelService: IParam;
    /**
     * 是否禁用
     *
     * @type {IPSEditor}
     * @memberof EditorControllerBase
     */
    disabled: boolean;
    /**
     * 应用上下文
     *
     * @type {*}
     * @memberof CtrlControllerBase
     */
    context: any;
    /**
     * 视图参数
     *
     * @type {*}
     * @memberof CtrlControllerBase
     */
    viewParam: any;
    /**
     * 视图参数
     *
     * @type {*}
     * @memberof CtrlControllerBase
     */
    contextData: any;
    /**
     * 编辑器上下文通讯对象
     *
     * @type {*}
     * @memberof CtrlControllerBase
     */
    contextState: any;
    /**
     * 编辑器值
     *
     * @type {*}
     * @memberof CtrlControllerBase
     */
    value: any;
    /**
     * 视图钩子对象
     *
     * @type {AppViewHooks}
     * @memberof ViewControllerBase
     */
    hooks: IAppEditorHooks;
    /**
     * 模型数据是否加载完成
     *
     * @memberof CtrlControllerBase
     */
    editorIsLoaded: boolean;
    /**
     * 自定义样式的对象
     *
     * @type {*}
     * @memberof EditorBase
     */
    customStyle: any;
    /**
     * 设置自定义props
     *
     * @type {*}
     * @memberof EditorBase
     */
    customProps: any;
    /**
     * 编辑器初始化
     *
     * @memberof EditorControllerBase
     */
    editorInit(): Promise<boolean>;
    /**
     * 编辑器模型初始化
     *
     * @memberof EditorControllerBase
     */
    editorModelInit(): Promise<void>;
    /**
     * 编辑器销毁
     *
     * @public
     * @memberof CtrlControllerBase
     */
    editorDestroy(): Promise<void>;
    /**
     * 设置编辑器的自定义高宽
     *
     * @memberof EditorBase
     */
    setCustomStyle(): void;
    /**
     * 设置编辑器导航参数
     *
     * @param keys 编辑器参数key
     * @memberof EditorBase
     */
    setEditorParams(): void;
    /**
     * 设置编辑器的自定义参数
     *
     * @memberof EditorBase
     */
    setCustomProps(): Promise<void>;
    /**
     * 改变编辑器的值
     *
     * @param {*} value
     * @memberof EditorControllerBase
     */
    changeValue(value: any): void;
}
