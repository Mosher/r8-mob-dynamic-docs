import { EditorControllerBase } from './editor-controller-base';
import { IMobUploadEditorController } from '../../../interface';
export declare class MobUploadEditorController extends EditorControllerBase implements IMobUploadEditorController {
    /**
     * @description 设置编辑器的自定义参数
     * @memberof MobUploadEditorController
     */
    setCustomProps(): Promise<void>;
}
