import { IMobCheckBoxEditorController } from '../../../interface';
import { EditorControllerBase } from './editor-controller-base';
export declare class MobCheckBoxEditorController extends EditorControllerBase implements IMobCheckBoxEditorController {
    /**
     * @description 设置编辑器的自定义参数
     * @memberof MobCheckBoxEditorController
     */
    setCustomProps(): Promise<void>;
}
