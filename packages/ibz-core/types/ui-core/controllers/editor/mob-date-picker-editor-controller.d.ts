import { IMobDatePickerEditorController } from '../../../interface';
import { EditorControllerBase } from './editor-controller-base';
/**
 * 日期选择器控制器
 *
 * @export
 * @class MobDatePickerEditorController
 * @extends {EditorControllerBase}
 */
export declare class MobDatePickerEditorController extends EditorControllerBase implements IMobDatePickerEditorController {
    /**
     * @description 设置编辑器的自定义参数
     * @memberof MobDatePickerEditorController
     */
    setCustomProps(): Promise<void>;
}
