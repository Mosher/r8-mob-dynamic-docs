import { IMobSpanEditorController } from '../../../interface';
import { EditorControllerBase } from './editor-controller-base';
export declare class MobSpanEditorController extends EditorControllerBase implements IMobSpanEditorController {
    /**
     * @description 设置编辑器的自定义参数
     * @memberof MobSpanEditorController
     */
    setCustomProps(): Promise<void>;
    /**
     * @description 初始化代码表参数
     * @memberof MobSpanEditorController
     */
    initCodeListParams(): void;
    /**
     * @description 初始化值格式化参数
     * @memberof MobSpanEditorController
     */
    initFormatParams(): void;
}
