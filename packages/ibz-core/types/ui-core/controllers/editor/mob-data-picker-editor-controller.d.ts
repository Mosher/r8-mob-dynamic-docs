import { IMobDataPickerEditorController } from '../../../interface';
import { EditorControllerBase } from './editor-controller-base';
export declare class MobDataPickerEditorController extends EditorControllerBase implements IMobDataPickerEditorController {
    /**
     * @description 设置编辑器的自定义参数
     * @memberof MobDataPickerEditorController
     */
    setCustomProps(): Promise<void>;
    /**
     * @description 改变编辑器的值
     * @param {*} arg
     * @memberof MobDataPickerEditorController
     */
    changeValue(arg: any): void;
}
