import { IMobDropdownListController } from '../../../interface';
import { EditorControllerBase } from './editor-controller-base';
export declare class MobDropdownListController extends EditorControllerBase implements IMobDropdownListController {
    /**
     * @description 设置编辑器的自定义参数
     * @memberof MobDropdownListController
     */
    setCustomProps(): Promise<void>;
}
