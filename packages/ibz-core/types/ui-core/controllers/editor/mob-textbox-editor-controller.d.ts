import { IMobTextboxEditorController } from '../../../interface';
import { EditorControllerBase } from './editor-controller-base';
export declare class MobTextboxEditorController extends EditorControllerBase implements IMobTextboxEditorController {
    /**
     * @description 设置编辑器的自定义参数
     * @memberof MobTextboxEditorController
     */
    setCustomProps(): Promise<void>;
}
