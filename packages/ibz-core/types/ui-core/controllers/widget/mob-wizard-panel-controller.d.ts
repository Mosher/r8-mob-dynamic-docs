import { Subject } from 'rxjs';
import { IPSDEWizardPanel } from '@ibiz/dynamic-model-api';
import { AppCtrlControllerBase } from './app-ctrl-controller-base';
import { IMobWizardPanelController, IParam, IViewStateParam } from '../../../interface';
export declare class MobWizardPanelController extends AppCtrlControllerBase implements IMobWizardPanelController {
    /**
     * 移动端向导面板部件实例
     *
     * @public
     * @type { IPSDEWizardPanel }
     * @memberof MobWizardPanelController
     */
    controlInstance: IPSDEWizardPanel;
    /**
     * @description 初始化行为
     * @protected
     * @type {string}
     * @memberof MobWizardPanelController
     */
    protected initAction: string;
    /**
     * @description 完成行为
     * @protected
     * @type {string}
     * @memberof MobWizardPanelController
     */
    protected finishAction: string;
    /**
     * @description 当前状态
     * @private
     * @type {string}
     * @memberof MobWizardPanelController
     */
    private curState;
    /**
     * @description 当前激活步骤表单
     * @type {string}
     * @memberof MobWizardPanelController
     */
    activeForm: string;
    /**
     * @description 执行过的表单
     * @type {string[]}
     * @memberof MobWizardPanelController
     */
    historyForms: string[];
    /**
     * @description 向导表单集合
     * @type {any[]}
     * @memberof MobWizardPanelController
     */
    wizardForms: any[];
    /**
     * @description 向导步骤集合
     * @type {any[]}
     * @memberof MobWizardPanelController
     */
    steps: any[];
    /**
     * @description 步骤标识集合
     * @type {IParam}
     * @memberof MobWizardPanelController
     */
    stepTags: IParam;
    /**
     * @description 步骤行为集合
     * @type {IParam}
     * @memberof MobWizardPanelController
     */
    stepActions: IParam;
    /**
     * @description 向导表单参数
     * @type {IParam}
     * @memberof MobWizardPanelController
     */
    formParam: IParam;
    /**
     * @description FormLoad是否被阻塞
     * @private
     * @type {boolean}
     * @memberof MobWizardPanelController
     */
    private hasFormLoadBlocked;
    /**
     * @description 视图状态订阅对象
     * @type {Subject<IViewStateParam>}
     * @memberof MobWizardPanelController
     */
    wizardState: Subject<IViewStateParam>;
    /**
     * @description 部件模型数据加载
     * @memberof MobWizardPanelController
     */
    ctrlModelLoad(): Promise<void>;
    /**
     * @description 部件基础数据初始化
     * @memberof MobWizardPanelController
     */
    ctrlBasicInit(): void;
    /**
     * @description 初始化激活表单
     * @protected
     * @memberof MobWizardPanelController
     */
    protected initActiveForm(): void;
    /**
     * @description 部件初始化
     * @memberof MobWizardPanelController
     */
    ctrlInit(): void;
    /**
     * @description 部件挂载
     * @param {*} [args] 额外参数
     * @memberof MobWizardPanelController
     */
    ctrlMounted(args?: any): void;
    /**
     * @description 注册表单步骤行为
     * @protected
     * @memberof MobWizardPanelController
     */
    protected regFormActions(): void;
    /**
     * 注册表单步骤行为
     *
     * @memberof WizardPanelControlBase
     */
    regFormAction(name: string, actionParams: any, stepTag: any): void;
    /**
     * @description 获取步骤标识
     * @param {Array<any>} wizardSteps
     * @param {string} tag
     * @return {*}
     * @memberof MobWizardPanelController
     */
    getStepTag(wizardSteps: Array<any>, tag: string): any;
    /**
     * @description 执行初始化
     * @protected
     * @param {*} data 数据
     * @memberof MobWizardPanelController
     */
    protected doInit(opt?: IParam): void;
    /**
     * @description 表单加载
     * @return {*}
     * @memberof MobWizardPanelController
     */
    formLoad(): void;
    /**
     * 上一步
     *
     * @memberof WizardPanelControlBase
     */
    handlePrevious(): void;
    /**
     * 下一步
     *
     * @memberof WizardPanelControlBase
     */
    handleNext(): void;
    /**
     * 完成
     *
     * @memberof WizardPanelControlBase
     */
    handleFinish(): void;
    /**
     * 完成行为
     *
     * @memberof WizardPanelControlBase
     */
    doFinish(): void;
    /**
     * 获取下一步向导表单
     *
     * @memberof WizardPanelControlBase
     */
    getNextForm(): any;
    /**
     * @description 向导表单加载完成
     * @param {string} name 向导表单名称
     * @param {*} args 数据
     * @memberof MobWizardPanelController
     */
    wizardPanelFormLoad(name: string, args: any): void;
    /**
     * 向导表单保存完成
     *
     * @param {string} name 向导表单名称
     * @param {*} args 数据
     * @memberof WizardPanelControlBase
     */
    wizardPanelFormSave(name: string, args: any): void;
    /**
     * @description 处理部件事件
     * @param {string} controlname 部件名称
     * @param {string} action 行为
     * @param {*} data 数据
     * @memberof MobWizardPanelController
     */
    handleCtrlEvent(controlname: string, action: string, data: any): void;
}
