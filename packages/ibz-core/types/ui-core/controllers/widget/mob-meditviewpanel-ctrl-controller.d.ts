import { IPSDEMultiEditViewPanel } from '@ibiz/dynamic-model-api';
import { AppMDCtrlController } from './app-md-ctrl-controller';
import { IMobMEditViewPanelCtrlController, IParam, ICtrlActionResult } from '../../../interface';
export declare class MobMEditViewPanelCtrlController extends AppMDCtrlController implements IMobMEditViewPanelCtrlController {
    /**
     * @description 多表单编辑视图面板部件模型实例
     * @type {IPSDEMultiEditViewPanel}
     * @memberof MobMEditViewPanelCtrlController
     */
    controlInstance: IPSDEMultiEditViewPanel;
    /**
     * @description 多编辑表单面板样式
     * @type {string}
     * @memberof MobMEditViewPanelCtrlController
     */
    panelStyle: string;
    /**
     * @description 数据集合
     * @type {any[]}
     * @memberof MobMEditViewPanelCtrlController
     */
    items: any[];
    /**
     * @description 计数器
     * @private
     * @type {number}
     * @memberof MobMEditViewPanelCtrlController
     */
    private count;
    /**
     * @description 关系实体参数对象
     * @private
     * @type {any[]}
     * @memberof MobMEditViewPanelCtrlController
     */
    private deResParameters;
    /**
     * @description 当前应用视图参数对象
     * @private
     * @type {any[]}
     * @memberof MobMEditViewPanelCtrlController
     */
    private parameters;
    /**
     * @description 多表单编辑视图面板部件模型数据初始化实例
     * @param {*} [args]
     * @memberof MobMEditViewPanelCtrlController
     */
    ctrlModelInit(args?: any): Promise<void>;
    /**
     * @description 多表单编辑视图面板部件模型数据加载
     * @memberof MobMEditViewPanelCtrlController
     */
    ctrlModelLoad(): Promise<void>;
    /**
     * @description 多表单编辑视图面板部件基础数据初始化
     * @memberof MobMEditViewPanelCtrlController
     */
    ctrlBasicInit(): void;
    /**
     * @description 多编辑视图面板初始化
     * @memberof MobMEditViewPanelCtrlController
     */
    ctrlInit(): void;
    /**
     * @description 初始化嵌入应用视图及实体参数对象
     * @memberof MobMEditViewPanelCtrlController
     */
    initParameters(): Promise<void>;
    /**
     * @description 加载数据
     * @param {IParam} [opts] 行为参数
     * @param {IParam} [args] 额外行为参数
     * @param {boolean} [showInfo=true] 是否显示提示信息
     * @param {boolean} [loadding=true] 是否显示loadding效果
     * @return {*}  {Promise<ICtrlActionResult>}
     * @memberof MobMEditViewPanelCtrlController
     */
    load(opts?: IParam, args?: IParam, showInfo?: boolean, loadding?: boolean): Promise<ICtrlActionResult>;
    /**
     * @description 保存数据
     * @param {IParam} [opts] 行为参数
     * @param {IParam} [args] 额外行为参数
     * @param {boolean} [showInfo=true] 是否显示提示信息
     * @param {boolean} [loadding=true] 是否显示loadding效果
     * @memberof MobMEditViewPanelCtrlController
     */
    save(opts?: IParam, args?: IParam, showInfo?: boolean, loadding?: boolean): void;
    /**
     * @description 新增数据
     * @param {IParam} [opts] 行为参数
     * @param {IParam} [args] 额外行为参数
     * @param {boolean} [showInfo=true] 是否显示提示信息
     * @param {boolean} [loadding=true] 是否显示loadding效果
     * @return {*}  {Promise<ICtrlActionResult>}
     * @memberof MobMEditViewPanelCtrlController
     */
    add(opts?: IParam, args?: IParam, showInfo?: boolean, loadding?: boolean): Promise<ICtrlActionResult>;
    /**
     * @description 嵌入视图数据改变
     * @param {*} $event 事件参数
     * @return {*}
     * @memberof MobMEditViewPanelCtrlController
     */
    viewDataChange($event: any): void;
    /**
     * @description 处理数据
     * @private
     * @param {any[]} datas 数据集
     * @memberof MobMEditViewPanelCtrlController
     */
    private doItems;
}
