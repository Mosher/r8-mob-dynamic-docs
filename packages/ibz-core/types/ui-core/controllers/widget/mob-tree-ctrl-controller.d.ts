import { IPSDETree } from '@ibiz/dynamic-model-api';
import { ICtrlActionResult, IMobTreeCtrlController, IParam } from '../../../interface';
import { AppMDCtrlController } from './app-md-ctrl-controller';
/**
 * 移动端树部件控制器
 *
 * @class MobTreeCtrlController
 * @extends AppMDCtrlController
 * @implements IMobTreeCtrlController
 */
export declare class MobTreeCtrlController extends AppMDCtrlController implements IMobTreeCtrlController {
    /**
     * @description 树视图部件模型实例
     * @type {IPSDETree}
     * @memberof MobTreeCtrlController
     */
    controlInstance: IPSDETree;
    /**
     * @description 节点过滤属性
     * @type {string}
     * @memberof MobTreeCtrlController
     */
    private nodeFilter;
    /**
     * @description 数据展开主键
     * @type {string[]}
     * @memberof MobTreeCtrlController
     */
    private expandedKeys;
    /**
     * @description 当前选中数据项
     * @type {*}
     * @memberof MobTreeCtrlController
     */
    private currentSelectedValue;
    /**
     * @description 枝干节点是否可用（具有数据能力，可抛出）
     * @type {boolean}
     * @memberof MobTreeCtrlController
     */
    private isBranchAvailable;
    /**
     * @description 树部件模型加载
     * @memberof MobTreeCtrlController
     */
    ctrlModelLoad(): Promise<void>;
    /**
     * @description 树部件初始化
     * @memberof MobTreeCtrlController
     */
    ctrlInit(): void;
    /**
     * @description 加载数据
     * @param {IParam} [args={}] 行为参数
     * @param {IParam} [opts] 额外行为参数
     * @param {boolean} [showInfo=this.showBusyIndicator] 是否显示提示信息
     * @param {boolean} [loadding=true] 是否显示loadding效果
     * @return {*}  {Promise<ICtrlActionResult>}
     * @memberof MobTreeCtrlController
     */
    load(args?: IParam, opts?: IParam, showInfo?: boolean, loadding?: boolean): Promise<ICtrlActionResult>;
    /**
     * @description 刷新
     * @param {any[]} args
     * @memberof MobTreeCtrlController
     */
    refresh(opts?: IParam, args?: IParam, showInfo?: boolean, loadding?: boolean): Promise<ICtrlActionResult>;
    /**
     * @description 刷新当前节点
     * @return {*}  {Promise<ICtrlActionResult>}
     * @memberof MobTreeCtrlController
     */
    refreshCurrent(): Promise<ICtrlActionResult>;
    /**
     * @description 格式化数据
     * @param {*} { data: node }
     * @param {IParam[]} items
     * @memberof MobTreeCtrlController
     */
    formatItems({ data: node }: any, items: IParam[]): void;
    /**
     * @description 刷新全部
     * @memberof MobTreeCtrlController
     */
    refreshAll(): Promise<ICtrlActionResult>;
    /**
     * @description 刷新父节点
     * @memberof MobTreeCtrlController
     */
    refreshParent(): Promise<ICtrlActionResult>;
    /**
     * @description 默认展开节点
     * @param {any[]} items 数据集
     * @return {*}  {any[]}
     * @memberof MobTreeCtrlController
     */
    private formatExpanded;
    /**
     * @description 计算当前节点的上下文
     * @param {*} curNode 当前节点
     * @return {*}
     * @memberof MobTreeCtrlController
     */
    private computecurNodeContext;
    /**
     * @description 设置附加标题栏
     * @param {any[]} items 节点集合
     * @returns {any[]}
     * @memberof MobTreeCtrlController
     */
    formatAppendCaption(items: any[]): void;
    /**
     * @description 设置默认选中,回显数项，选中所有子节点
     * @param {any[]} items 当前节点所有子节点集合
     * @param {boolean} [isRoot=false] 是否是加载根节点
     * @param {boolean} [isSelectedAll=false] 是否选中所有子节点
     * @return {*}  {void}
     * @memberof MobTreeCtrlController
     */
    private setDefaultSelection;
    /**
     * @description 选中数组去重
     * @param {any[]} items 数据集
     * @return {*}
     * @memberof MobTreeCtrlController
     */
    private selectionsDeDuplication;
    /**
     * @description 多项选择选中数据
     * @param {string} key 选中数据主键
     * @param {boolean} checked 选中状态
     * @memberof MobTreeCtrlController
     */
    handleTreeNodeChecked(item: IParam, event: any): void;
    /**
     * @description 单选选中数据变化
     * @param {*} item 选中数据
     * @param {*} event 事件源
     * @memberof MobTreeCtrlController
     */
    handleTreeNodeSelect(item: any, event: MouseEvent): void;
    /**
     * @description 加载子节点数据
     * @param {*} item 选中数据
     * @param {*} event 事件源
     * @memberof MobTreeCtrlController
     */
    loadChildNodeData(item: IParam, event: MouseEvent): void;
    /**
     * @description 获取计数器数据
     * @param {IParam} node 节点数据
     * @return {*}
     * @memberof MobTreeCtrlController
     */
    getCounterData(node: IParam): any;
}
