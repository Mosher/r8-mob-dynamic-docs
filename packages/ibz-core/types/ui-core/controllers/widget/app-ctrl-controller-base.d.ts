import { Subscription } from 'rxjs';
import { IPSAppCounterRef } from '@ibiz/dynamic-model-api';
import { AppModelService } from '../../../app';
import { IParam, IUIDataParam, IUIEnvironmentParam, IUIUtilParam, IAppCtrlHooks, IAppCtrlControllerBase, ICtrlActionResult, ILoadingService, IUIService, IEntityService } from '../../../interface';
import { AppUIControllerBase } from '../base';
export declare class AppCtrlControllerBase extends AppUIControllerBase implements IAppCtrlControllerBase {
    /**
     * @description 原生引用控件名称
     * @type {string}
     * @memberof AppCtrlControllerBase
     */
    realCtrlRefName: string;
    /**
     * @description 视图钩子对象
     * @type {IAppCtrlHooks}
     * @memberof AppCtrlControllerBase
     */
    hooks: IAppCtrlHooks;
    /**
     * @description 部件模型实例对象
     * @type {*}
     * @memberof AppCtrlControllerBase
     */
    controlInstance: any;
    /**
     * @description 名称
     * @type {string}
     * @memberof AppCtrlControllerBase
     */
    name: string;
    /**
     * @description 部件类型
     * @type {string}
     * @memberof AppCtrlControllerBase
     */
    ctrlType: string;
    /**
     * @description 部件显示模式
     * @type {string}
     * @memberof AppCtrlControllerBase
     */
    ctrlShowMode: string;
    /**
     * @description 部件样式
     * @type {string}
     * @memberof AppCtrlControllerBase
     */
    ctrlStyle: string;
    /**
     * @description 视图通讯对象
     * @type {*}
     * @memberof AppCtrlControllerBase
     */
    viewState: any;
    /**
     * @description 应用上下文
     * @type {IParam}
     * @memberof AppCtrlControllerBase
     */
    context: IParam;
    /**
     * @description 视图参数
     * @type {IParam}
     * @memberof AppCtrlControllerBase
     */
    viewParam: IParam;
    /**
     * @description 模型数据是否加载完成
     * @type {boolean}
     * @memberof AppCtrlControllerBase
     */
    controlIsLoaded: boolean;
    /**
     * @description 模型服务
     * @type {AppModelService}
     * @memberof AppCtrlControllerBase
     */
    modelService: AppModelService;
    /**
     * @description 部件服务对象
     * @type {*}
     * @memberof AppCtrlControllerBase
     */
    ctrlService: any;
    /**
     * @description 应用数据服务对象
     * @type {*}
     * @memberof AppCtrlControllerBase
     */
    appDataService: IEntityService;
    /**
     * @description 应用界面服务对象
     * @type {*}
     * @memberof AppCtrlControllerBase
     */
    appUIService: IUIService;
    /**
     * @description 计数器服务对象集合
     * @type {Array<any>}
     * @memberof AppCtrlControllerBase
     */
    counterServiceArray: Array<any>;
    /**
     * @description 视图计数器服务数组
     * @type {IParam[]}
     * @memberof AppCtrlControllerBase
     */
    viewCounterServiceArray: IParam[];
    /**
     * @description 外部传入数据对象
     * @type {Array<IParam>}
     * @memberof AppCtrlControllerBase
     */
    navDatas: Array<IParam>;
    /**
     * @description 视图操作参数集合
     * @type {IUIUtilParam}
     * @memberof AppCtrlControllerBase
     */
    viewCtx: IUIUtilParam;
    /**
     * @description 界面触发逻辑Map
     * @type {Map<string, any>}
     * @memberof AppCtrlControllerBase
     */
    ctrlTriggerLogicMap: Map<string, any>;
    /**
     * @description 原生界面触发逻辑集合
     * @type {Array<any>}
     * @memberof AppCtrlControllerBase
     */
    realCtrlTriggerLogicArray: Array<any>;
    /**
     * @description 订阅视图状态事件
     * @type {(Subscription | undefined)}
     * @memberof AppCtrlControllerBase
     */
    viewStateEvent: Subscription | undefined;
    /**
     * @description 挂载状态集合
     * @type {Map<string, boolean>}
     * @memberof AppCtrlControllerBase
     */
    mountedMap: Map<string, boolean>;
    /**
     * @description 部件引用控制器集合
     * @type {Map<string, IParam>}
     * @memberof AppCtrlControllerBase
     */
    ctrlRefsMap: Map<string, IParam>;
    /**
     * @description 是否部件已经完成
     * @type {boolean}
     * @memberof AppCtrlControllerBase
     */
    hasCtrlMounted: boolean;
    /**
     * @description 显示处理提示
     * @type {boolean}
     * @memberof AppCtrlControllerBase
     */
    showBusyIndicator: boolean;
    /**
     * @description 部件行为--load
     * @type {*}
     * @memberof AppCtrlControllerBase
     */
    loadAction: any;
    /**
     * @description 部件行为--loaddraft
     * @type {*}
     * @memberof AppCtrlControllerBase
     */
    loaddraftAction: any;
    /**
     * @description 部件行为--create
     * @type {*}
     * @memberof AppCtrlControllerBase
     */
    createAction?: any;
    /**
     * @description 部件行为--remove
     * @type {*}
     * @memberof AppCtrlControllerBase
     */
    removeAction: any;
    /**
     * @description 部件行为--update
     * @type {*}
     * @memberof AppCtrlControllerBase
     */
    updateAction: any;
    /**
     * @description 部件行为--submit
     * @type {*}
     * @memberof AppCtrlControllerBase
     */
    WFSubmitAction?: any;
    /**
     * @description 部件行为--start
     * @type {*}
     * @memberof AppCtrlControllerBase
     */
    WFStartAction?: any;
    /**
     * @description 界面行为模型
     * @type {IParam}
     * @memberof AppCtrlControllerBase
     */
    actionModel: IParam;
    /**
     * @description 部件加载服务
     * @type {ILoadingService}
     * @memberof AppCtrlControllerBase
     */
    ctrlLoadingService: ILoadingService;
    /**
     * @description 应用状态事件
     * @type {(Subscription | undefined)}
     * @memberof AppCtrlControllerBase
     */
    appStateEvent: Subscription | undefined;
    /**
     * @description 开启下拉刷新
     * @type {boolean}
     * @memberof AppCtrlControllerBase
     */
    enablePullDownRefresh: boolean;
    /**
     * @description 部件UI是否存在权限
     * @type {boolean}
     * @memberof AppCtrlControllerBase
     */
    enableControlUIAuth: boolean;
    /**
     * @description 应用实体codeName
     * @readonly
     * @memberof AppCtrlControllerBase
     */
    get appDeCodeName(): any;
    /**
     * @description 应用实体映射实体名称
     * @readonly
     * @memberof AppCtrlControllerBase
     */
    get deName(): any;
    /**
     * @description 应用实体主键属性codeName
     * @readonly
     * @memberof AppCtrlControllerBase
     */
    get appDeKeyFieldName(): string;
    /**
     * @description 应用实体主信息属性codeName
     * @readonly
     * @memberof AppCtrlControllerBase
     */
    get appDeMajorFieldName(): string;
    /**
     * Creates an instance of AppCtrlControllerBase.
     * @param {*} opts 构建参数
     * @memberof AppCtrlControllerBase
     */
    constructor(opts: any);
    /**
     * @description 初始化输入数据
     * @param {IParam} opts 输入参数
     * @memberof AppCtrlControllerBase
     */
    initInputData(opts: IParam): void;
    /**
     * @description 部件初始化
     * @return {*}  {Promise<boolean>}
     * @memberof AppCtrlControllerBase
     */
    controlInit(): Promise<boolean>;
    /**
     * @description 初始化实体服务对象
     * @memberof AppCtrlControllerBase
     */
    initAppDataService(): Promise<void>;
    /**
     * @description 初始化应用界面服务对象
     * @memberof AppCtrlControllerBase
     */
    initAppUIService(): Promise<void>;
    /**
     * @description 计数器刷新
     * @memberof AppCtrlControllerBase
     */
    counterRefresh(): void;
    /**
     * @description 部件销毁
     * @memberof AppCtrlControllerBase
     */
    controlDestroy(): Promise<void>;
    /**
     * @description 获取部件类型
     * @return {*}  {string}
     * @memberof AppCtrlControllerBase
     */
    getControlType(): string;
    /**
     * @description 获取多项数据
     * @return {*}  {any[]}
     * @memberof AppCtrlControllerBase
     */
    getDatas(): any[];
    /**
     * @description 获取单项数据
     * @return {*}  {*}
     * @memberof AppCtrlControllerBase
     */
    getData(): any;
    /**
     * @description 获取计数器服务
     * @param {IPSAppCounterRef} [appCounterRef] 计数器引用
     * @return {*}
     * @memberof AppCtrlControllerBase
     */
    getCounterService(appCounterRef?: IPSAppCounterRef): any;
    /**
     * @description 获取逻辑UI数据
     * @return {*}  {IUIDataParam}
     * @memberof AppCtrlControllerBase
     */
    getUIDataParam(): IUIDataParam;
    /**
     * @description 获取逻辑环境数据
     * @return {*}  {IUIEnvironmentParam}
     * @memberof AppCtrlControllerBase
     */
    getUIEnvironmentParam(): IUIEnvironmentParam;
    /**
     * @description 初始化挂载状态集合
     * @memberof AppCtrlControllerBase
     */
    initMountedMap(): void;
    /**
     * @description 设置已经绘制完成状态
     * @param {string} [name='self'] 部件类型名
     * @param {*} [data] 数据
     * @memberof AppCtrlControllerBase
     */
    setIsMounted(name?: string, data?: any): void;
    /**
     * @description 部件挂载
     * @param {*} [args] 挂载参数
     * @memberof AppCtrlControllerBase
     */
    ctrlMounted(args?: any): void;
    /**
     * @description 部件模型数据加载
     * @memberof AppCtrlControllerBase
     */
    ctrlModelLoad(): Promise<void>;
    /**
     * @description 初始化计数器服务
     * @return {*}
     * @memberof AppCtrlControllerBase
     */
    initCounterService(): Promise<void>;
    /**
     * @description 初始化部件逻辑
     * @memberof AppCtrlControllerBase
     */
    initControlLogic(): Promise<void>;
    /**
     * @description 部件模型数据初始化实例
     * @param {*} [args] 初始化参数
     * @memberof AppCtrlControllerBase
     */
    ctrlModelInit(args?: any): Promise<void>;
    /**
     * @description 部件初始化
     * @param {*} [args] 初始化参数
     * @memberof AppCtrlControllerBase
     */
    ctrlInit(args?: any): void;
    /**
     * @description 部件基础数据初始化
     * @memberof AppCtrlControllerBase
     */
    ctrlBasicInit(): void;
    /**
     * @description 初始化实体行为
     * @memberof AppCtrlControllerBase
     */
    initActions(): void;
    /**
     * @description 初始化界面行为模型
     * @memberof AppCtrlControllerBase
     */
    initCtrlActionModel(): void;
    /**
     * @description 重置
     * @memberof AppCtrlControllerBase
     */
    onReset(): void;
    /**
     * @description 转化数据
     * @param {*} args
     * @memberof AppCtrlControllerBase
     */
    transformData(args: any): void;
    /**
     * @description 开始加载
     * @memberof AppCtrlControllerBase
     */
    ctrlBeginLoading(): void;
    /**
     * @description 结束加载
     * @memberof AppCtrlControllerBase
     */
    ctrlEndLoading(): void;
    /**
     * @description 保存
     * @memberof AppCtrlControllerBase
     */
    save(data: IParam): void;
    /**
     * 处理部件UI请求
     *
     * @param {string} action 行为名称
     * @param {*} context 上下文
     * @param {*} viewparam 视图参数
     * @param {boolean} loadding 是否显示加载遮罩
     * @memberof AppCtrlControllerBase
     */
    /**
     * @description 处理部件UI请求
     * @param {string} action 行为
     * @param {*} context 上下文
     * @param {*} viewParam 视图参数
     * @param {boolean} [loadding=true] 是否显示loadding效果
     * @memberof AppCtrlControllerBase
     */
    onControlRequset(action: string, context: any, viewParam: any, loadding?: boolean): void;
    /**
     * @description 处理部件UI响应
     * @param {string} action 行为
     * @param {*} response 响应结果
     * @memberof AppCtrlControllerBase
     */
    onControlResponse(action: string, response: any): void;
    /**
     * @description 下拉加载更多数据
     * @return {*}  {Promise<IParam>}
     * @memberof AppCtrlControllerBase
     */
    pullDownRefresh(): Promise<IParam>;
    /**
     * @description 关闭视图
     * @param {*} args 关闭参数
     * @memberof AppCtrlControllerBase
     */
    closeView(args: any): void;
    /**
     * @description 刷新数据
     * @param {IParam} [opts] 行为参数
     * @param {IParam} [args] 额外参数
     * @param {boolean} [showInfo=true] 是否显示提示信息
     * @param {boolean} [loadding=true] 是否显示loadding效果
     * @return {*}  {Promise<ICtrlActionResult>}
     * @memberof AppCtrlControllerBase
     */
    refresh(opts?: IParam, args?: IParam, showInfo?: boolean, loadding?: boolean): Promise<ICtrlActionResult>;
    /**
     * @description 执行部件事件逻辑
     * @param {string} name 逻辑名称
     * @param {IUIDataParam} args { action: 行为名称, sender: 触发源对象，navContext: 导航上下文，navParam: 导航参数, navData: 导航数据, data: 源数据 }
     * @return {*}
     * @memberof AppCtrlControllerBase
     */
    executeCtrlEventLogic(name: string, args: IUIDataParam): Promise<IUIDataParam | undefined>;
    /**
     * @description 处理控件自定义事件
     * @param {string} name 行为名称
     * @param {*} data 数据
     * @param {*} args 参数
     * @memberof AppCtrlControllerBase
     */
    handleCtrlCustomEvent(name: string, data: any, args: any): void;
    /**
     * @description 处理部件界面行为点击
     * @param {*} data 数据
     * @param {*} event 事件源
     * @param {*} detail 行为模型
     * @memberof AppCtrlControllerBase
     */
    handleActionClick(data: IParam, event: MouseEvent, detail: IParam): void;
    /**
     * @description 处理部件定时器逻辑
     * @memberof AppCtrlControllerBase
     */
    handleTimerLogic(): void;
    /**
     * @description 销毁部件定时器逻辑
     * @memberof AppCtrlControllerBase
     */
    destroyLogicTimer(): void;
    /**
     * @description 部件事件 抛出
     * @param {string} action 抛出行为
     * @param {*} data 抛出数据
     * @memberof AppCtrlControllerBase
     */
    ctrlEvent(action: string, data: any): void;
    /**
     * @description 处理部件事件
     * @param {string} controlname 部件名称
     * @param {string} action 部件行为
     * @param {*} data 数据
     * @memberof AppCtrlControllerBase
     */
    handleCtrlEvent(controlname: string, action: string, data: any): void;
}
