import { IMobChartExpBarController } from '../../../interface';
import { IPSChartExpBar } from '@ibiz/dynamic-model-api';
import { AppExpBarCtrlController } from './app-exp-bar-ctrl-controller';
export declare class MobChartExpBarController extends AppExpBarCtrlController implements IMobChartExpBarController {
    /**
     * 移动端图表导航栏实例
     *
     * @public
     * @type { IPSDEMobChartExpBar }
     * @memberof MobChartExpBarController
     */
    controlInstance: IPSChartExpBar;
    handleCtrlEvent(controlname: string, action: string, data: any): void;
    /**
   * 图表部件选中数据变化
   *
   * @memberof ChartExpBarControlBase
   */
    onSelectionChange(args: any): void;
}
