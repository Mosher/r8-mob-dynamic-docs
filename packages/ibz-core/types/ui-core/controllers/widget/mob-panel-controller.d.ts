import { IPSPanel, IPSPanelItem } from '@ibiz/dynamic-model-api';
import { ICodeListService, IMobPanelController, IEditorEventParam, IParam, ICtrlActionResult } from '../../../interface';
import { AppCtrlControllerBase } from './app-ctrl-controller-base';
export declare class MobPanelController extends AppCtrlControllerBase implements IMobPanelController {
    /**
     * @description 面板部件实例对象
     * @type {IPSPanel}
     * @memberof MobPanelController
     */
    controlInstance: IPSPanel;
    /**
     * @description 代码表服务
     * @type {ICodeListService}
     * @memberof MobPanelController
     */
    codeListService: ICodeListService;
    /**
     * @description 面板模式 [layout: 项布局面板，view：视图面板]
     * @type {('layout' | 'view')}
     * @memberof MobPanelController
     */
    panelMode: 'layout' | 'view';
    /**
     * 默认加载
     *
     * @type {boolean}
     * @memberof MobPanelController
     */
    private isLoadDefault;
    /**
     * @description 值规则
     * @type {*}
     * @memberof MobPanelController
     */
    rules: any;
    /**
     * @description 面板数据
     * @type {*}
     * @memberof MobPanelController
     */
    data: any;
    /**
     * @description 数据映射（用于保存列表项相关信息）
     * @type {Map<string, any>}
     * @memberof MobPanelController
     */
    dataMap: Map<string, IParam>;
    /**
     * @description 面板项模型集合
     * @type {*}
     * @memberof MobPanelController
     */
    detailsModel: any;
    /**
     * @description 面板项动态逻辑集合
     * @type {any[]}
     * @memberof MobPanelController
     */
    allPanelItemGroupLogic: any[];
    /**
     * @description 初始化输入数据
     * @param {*} opts
     * @memberof MobPanelController
     */
    initInputData(opts: any): void;
    /**
     * @description 面板部件基础数据初始化
     * @memberof MobPanelController
     */
    ctrlBasicInit(): void;
    /**
     * 部件初始化
     *
     * @param args
     * @memberof MobFormCtrlController
     */
    ctrlInit(args?: any): void;
    /**
     * @description 初始化值规则
     * @param {(IPSPanelItem[] | null)} panelItems
     * @memberof MobPanelController
     */
    initRules(panelItems: IPSPanelItem[] | null): void;
    /**
     * @description 计算UI展示数据
     * @param {*} [newVal]
     * @memberof MobPanelController
     */
    computedUIData(newVal?: any): Promise<void>;
    /**
     * @description 计算数据加载模式
     * @param {number} dataMode
     * @param args 参数
     * @memberof MobPanelController
     */
    computeLoadState(dataMode: number, args?: any): Promise<void>;
    /**
     * @description 刷新
     * @param {*} [args]
     * @memberof MobPanelController
     */
    refresh(opts?: IParam, args?: IParam, showInfo?: boolean, loadding?: boolean): Promise<ICtrlActionResult>;
    /**
     * @description 加载数据
     * @param arg 参数
     * @memberof MobPanelController
     */
    loadPanelData(args?: any): Promise<void>;
    /**
     * @description 初始化面板模型
     * @param {(IPSPanelItem[] | null)} panelItems
     * @memberof MobPanelController
     */
    initDetailsModel(panelItems: IPSPanelItem[] | null): void;
    /**
     * @description 初始化界面行为模型
     * @memberof MobPanelController
     */
    initCtrlActionModel(): void;
    /**
     * @description 初始化面板项的界面行为模型
     * @param {(IPSPanelItem[] | null)} panelItems
     * @memberof MobPanelController
     */
    initItemsActionModel(panelItems: IPSPanelItem[] | null): void;
    /**
     * @description 获取数据项集合
     * @return {*}  {any[]}
     * @memberof MobPanelController
     */
    getDataItems(): any[];
    /**
     * @description 填充面板数据
     * @param {*} data
     * @memberof MobPanelController
     */
    fillPanelData(data: any): void;
    /**
     * @description 面板逻辑
     * @param {{ name: string; newVal: any; oldVal: any }} { name, newVal, oldVal }
     * @memberof MobPanelController
     */
    panelLogic({ name, newVal, oldVal }: {
        name: string;
        newVal: any;
        oldVal: any;
    }): void;
    /**
     * @description 校验动态逻辑结果
     * @param {*} data
     * @param {*} logic
     * @return {*}
     * @memberof MobPanelController
     */
    verifyGroupLogic(data: any, logic: any): boolean;
    /**
     * @description 面板编辑项值变更
     * @param {IParam} event
     * @return {*}  {void}
     * @memberof MobPanelController
     */
    onPanelItemValueChange(event: IParam): void;
    /**
     * @description 处理编辑器事件
     * @param {IEditorEventParam} args
     * @memberof MobPanelController
     */
    handleEditorEvent(args: IEditorEventParam): void;
    /**
     * @description 面板编辑项值变化
     * @param {string} property
     * @param {*} value
     * @memberof MobPanelController
     */
    panelEditItemChange(property: string, value: any): void;
    /**
     * @description 计算面板按钮权限状态
     * @param {*} data
     * @memberof MobPanelController
     */
    computeButtonState(data: any): void;
}
