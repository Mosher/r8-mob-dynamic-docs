import { IPSDEToolbar, IPSDEToolbarItem } from '@ibiz/dynamic-model-api';
import { IMobToolbarCtrlController } from '../../../interface';
import { AppCtrlControllerBase } from './app-ctrl-controller-base';
export declare class MobToolbarCtrlController extends AppCtrlControllerBase implements IMobToolbarCtrlController {
    /**
     * @description 部件模型实例对象
     * @type {IPSDEToolbar}
     * @memberof MobToolbarCtrlController
     */
    controlInstance: IPSDEToolbar;
    /**
     * @description 工具栏模型
     * @type {IPSDEToolbarItem[]}
     * @memberof MobToolbarCtrlController
     */
    toolbarModels: IPSDEToolbarItem[];
    /**
     * 构造MobToolbarCtrlController对象
     *
     * @public
     * @memberof MobToolbarCtrlController
     */
    constructor(opts: any);
    /**
     * @description 部件基础数据初始化
     * @memberof AppCtrlControllerBase
     */
    ctrlBasicInit(): void;
    /**
     * @description 工具栏部件初始化
     * @memberof MobToolbarCtrlController
     */
    ctrlInit(): void;
    /**
     * @description 计算工具栏权限
     * @param {*} data
     * @memberof MobToolbarCtrlController
     */
    calcToolbarItemAuthState(data: any): void;
}
