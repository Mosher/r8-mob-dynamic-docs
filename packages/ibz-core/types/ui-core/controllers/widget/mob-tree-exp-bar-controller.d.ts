import { IPSAppView, IPSTreeExpBar } from '@ibiz/dynamic-model-api';
import { AppExpBarCtrlController } from './app-exp-bar-ctrl-controller';
import { IMobTreeExpBarController, IUIDataParam } from '../../../interface';
export declare class MobTreeExpBarController extends AppExpBarCtrlController implements IMobTreeExpBarController {
    /**
     * 移动端树导航部件实例
     *
     * @public
     * @type { IPSDEMobTreeExpBar }
     * @memberof MobTreeExpBarController
     */
    controlInstance: IPSTreeExpBar;
    /**
     * @description 处理部件事件
     * @param {string} controlname
     * @param {string} action
     * @param {*} data
     * @memberof MobTreeExpBarController
     */
    handleCtrlEvent(controlname: string, action: string, data: any): void;
    /**
     * @description 选中数据变化
     * @protected
     * @param {IUIDataParam} uiDataParam UI数据
     * @return {*}  {void}
     * @memberof AppExpBarCtrlController
     */
    protected onSelectionChange(uiDataParam: IUIDataParam): Promise<void>;
    /**
     * 计算导航参数
     *
     * @param arg  选中数据
     * @memberof TreeExpBarControlBase
     */
    computeNavParams(arg: any): {
        tempContext: any;
        tempViewparam: any;
    };
    /**
     * 获取关系项视图
     *
     * @param {*} [arg={}] 额外参数
     * @returns {*}
     * @memberof TreeExpBarControlBase
     */
    getExpItemView(arg?: any): Promise<{
        viewModelData: IPSAppView | null;
        parentdata: IModel;
        deKeyField: string | undefined;
    } | null>;
    /**
     * 打开视图导航视图
     *
     * @param {IPSDEChartSeries} seriesItem
     * @param {*} tempContext
     * @param {*} args
     * @memberof MobChartExpBarController
     */
    openExplorerView(explorerView: IPSAppView, args: any, tempContext: any, tempViewParam: any): Promise<void>;
}
