import { IAppMDCtrlController, IParam } from '../../../interface';
import { AppCtrlControllerBase } from './app-ctrl-controller-base';
export declare class AppMDCtrlController extends AppCtrlControllerBase implements IAppMDCtrlController {
    /**
     * @description 部件行为--fetch
     * @type {string}
     * @memberof AppMDCtrlController
     */
    fetchAction?: string;
    /**
     * @description 多数据部件数据集合
     * @type {IParam[]}
     * @memberof AppMDCtrlController
     */
    items: IParam[];
    /**
     * @description 当前页
     * @type {number}
     * @memberof AppMDCtrlController
     */
    curPage: number;
    /**
     * @description 是否支持分页
     * @type {boolean}
     * @memberof AppMDCtrlController
     */
    isEnablePagingBar: boolean;
    /**
     * @description 排序对象
     * @type {IParam}
     * @memberof AppMDCtrlController
     */
    sort: IParam;
    /**
     * @description 是否禁用排序
     * @type {boolean}
     * @memberof AppMDCtrlController
     */
    isNoSort: boolean;
    /**
     * @description 排序方向
     * @type {string}
     * @memberof AppMDCtrlController
     */
    minorSortDir: string;
    /**
     * @description 排序属性
     * @type {string}
     * @memberof AppMDCtrlController
     */
    minorSortPSDEF: string;
    /**
     * @description 分页条数
     * @type {number}
     * @memberof AppMDCtrlController
     */
    limit: number;
    /**
     * @description 总条数
     * @type {number}
     * @memberof AppMDCtrlController
     */
    totalRecord: number;
    /**
     * @description 是否默认选中第一条数据
     * @type {boolean}
     * @memberof AppMDCtrlController
     */
    isSelectFirstDefault: boolean;
    /**
     * @description 是否开启分组
     * @type {boolean}
     * @memberof AppMDCtrlController
     */
    isEnableGroup: boolean;
    /**
     * @description 代码表分组集合
     * @type {IParam[]}
     * @memberof AppMDCtrlController
     */
    groupDetail: IParam[];
    /**
     * @description 分组模式
     * @type {('AUTO' | 'CODELIST' | null)}
     * @memberof AppMDCtrlController
     */
    groupMode: 'AUTO' | 'CODELIST' | null;
    /**
     * @description 分组属性
     * @type {string}
     * @memberof AppMDCtrlController
     */
    groupField: string;
    /**
     * @description 分组数据
     * @type {*}
     * @memberof AppMDCtrlController
     */
    groupData: IParam[];
    /**
     * @description 是否多选
     * @type {boolean}
     * @memberof AppMDCtrlController
     */
    isMultiple: boolean;
    /**
     * @description 选中数组
     * @type {Array<any>}
     * @memberof AppMDCtrlController
     */
    selections: IParam[];
    /**
     * @description 数据映射（用于将列表项相关信息传递给面板）
     * @type {Map<string, IParam>}
     * @memberof AppMDCtrlController
     */
    dataMap: Map<string, IParam>;
    /**
     * @description 获取单项数据
     * @return {*}  {IParam}
     * @memberof AppMDCtrlController
     */
    getData(): IParam;
    /**
     * @description 获取多项数据
     * @return {*}  {IParam[]}
     * @memberof AppMDCtrlController
     */
    getDatas(): IParam[];
    /**
     * @description 初始化输入数据
     * @param {*} opts 输入参数
     * @memberof AppMDCtrlController
     */
    initInputData(opts: any): void;
    /**
     * @description 部件基础数据初始化
     * @memberof AppMDCtrlController
     */
    ctrlBasicInit(): void;
    /**
     * @description 数据映射
     * @memberof AppMDCtrlController
     */
    initDataMap(): void;
    /**
     * @description 初始化实体行为
     * @memberof AppMDCtrlController
     */
    initActions(): void;
    /**
     * @description 初始化选中值
     * @memberof AppMDCtrlController
     */
    private initSelects;
    /**
     * @description 初始化分组配置
     * @private
     * @memberof AppMDCtrlController
     */
    private initGroupOptions;
    /**
     * @description 初始化排序
     * @private
     * @memberof AppMDCtrlController
     */
    private initSort;
    /**
     * @description 多数据部件模型加载
     * @memberof AppMDCtrlController
     */
    ctrlModelLoad(): Promise<void>;
    /**
     * @description 初始化代码表分组集合
     * @memberof AppMDCtrlController
     */
    private initGroupDetail;
    /**
     * @description 获取分组数据
     * @memberof AppMDCtrlController
     */
    protected getGroupData(): void;
    /**
     * @description 获取代码表分组数据
     * @param {*} items 多数据部件数据
     * @memberof AppMDCtrlController
     */
    private getGroupDataByCodeList;
    /**
     * @description 获取自动分组数据
     * @param {*} items 多数据部件数据
     * @memberof AppMDCtrlController
     */
    private getGroupDataAuto;
    /**
     * @description 选中数据改变
     * @param {any[]} selections 选中数组
     * @memberof AppMDCtrlController
     */
    selectionChange(selections: any[]): void;
    /**
     * @description 项选中
     * @param {IParam} item 项数据
     * @param {MouseEvent} [event] 数据源
     * @memberof AppMDCtrlController
     */
    onRowSelect(item: IParam, event?: MouseEvent): void;
    /**
     * @description 处理部件事件
     * @param {string} controlname 部件名
     * @param {string} action 部件行为
     * @param {*} data 行为触发数据
     * @memberof AppMDCtrlController
     */
    handleCtrlEvent(controlname: string, action: string, data: IParam): void;
    /**
     * @description 处理上下文菜单部件事件
     * @param {string} action 行为名
     * @param {IParam} data 行为数据
     * @memberof AppMDCtrlController
     */
    handleContextMenuEvent(action: string, data: IParam): void;
    /**
     * @description 处理工具栏点击事件
     * @param {string} action 行为名
     * @param {IParam} data 行为数据
     * @memberof AppMDCtrlController
     */
    handleToolbarEvent(action: string, data: IParam): void;
    /**
     * @description 处理上下文菜单点击事件
     * @param {string} name 上下文菜单部件名
     * @param {IParam} data 行为数据
     * @param {MouseEvent} event 行为事件源
     * @param {IParam} detail 行为模型
     * @memberof AppMDCtrlController
     */
    handleContextMenuClick(name: string, data: IParam, event: MouseEvent, detail: IParam): void;
    /**
     * @description 处理工具栏点击行为
     * @param {IParam} data 行为数据
     * @memberof AppMDCtrlController
     */
    handleToolbarClick(data: IParam): void;
}
