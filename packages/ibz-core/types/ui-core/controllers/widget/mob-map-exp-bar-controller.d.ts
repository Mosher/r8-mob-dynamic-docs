import { IPSMapExpBar } from '@ibiz/dynamic-model-api';
import { IMobMapExpBarController } from '../../../interface';
import { AppExpBarCtrlController } from './app-exp-bar-ctrl-controller';
export declare class MobMapExpBarController extends AppExpBarCtrlController implements IMobMapExpBarController {
    /**
     * 移动端地图导航部件实例
     *
     * @public
     * @type { IPSDEMobMapExpBar }
     * @memberof MobMapExpBarController
     */
    controlInstance: IPSMapExpBar;
    /**
     * @description 处理部件事件
     * @param {string} controlname 部件名称
     * @param {string} action 部件行为
     * @param {*} data 数据
     * @memberof MobMapExpBarController
     */
    handleCtrlEvent(controlname: string, action: string, data: any): void;
    /**
  * 地图部件的选中数据事件
  *
  *
  * @param {any[]} args 选中数据
  * @return {*}  {void}
  * @memberof MapExpBarControlBase
  */
    onSelectionChange(args: any): void;
}
