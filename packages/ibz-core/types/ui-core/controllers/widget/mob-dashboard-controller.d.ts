import { IPSDashboard } from '@ibiz/dynamic-model-api';
import { ICtrlActionResult, IMobDashboardController, IParam, IUtilService } from '../../../interface';
import { AppCtrlControllerBase } from './app-ctrl-controller-base';
/**
 * 移动端数据看板部件控制器
 *
 * @class MobDashboardController
 * @extends AppCtrlControllerBase
 * @implements IMobDashboardController
 */
export declare class MobDashboardController extends AppCtrlControllerBase implements IMobDashboardController {
    /**
     * @description 数据看板部件实例对象
     * @type {IPSDashboard}
     * @memberof MobDashboardController
     */
    controlInstance: IPSDashboard;
    /**
     * @description 支持看板定制
     * @type {boolean}
     * @memberof MobDashboardController
     */
    enableCustomized: boolean;
    /**
     * @description 自定义看板模型数据
     * @type {IParam[]}
     * @memberof MobDashboardController
     */
    customDashboardModelData: IParam[];
    /**
     * @description 是否具有自定义面板
     * @type {boolean}
     * @memberof MobDashboardController
     */
    hasCustomized: boolean;
    /**
     * @description 工具服务名称
     * @type {string}
     * @memberof MobDashboardController
     */
    utilServiceName: string;
    /**
     * 是否支持搜索
     *
     * @public
     * @type {(boolean)}
     * @memberof MobDashboardController
     */
    private isSearchMode;
    /**
     * @description 工具服务
     * @type {IUtilService}
     * @memberof MobDashboardController
     */
    utilService?: IUtilService;
    /**
     * @description 模型id
     * @type {string}
     * @memberof MobDashboardController
     */
    modelId: string;
    /**
     * @description 门户部件集合
     * @type {IParam[]}
     * @memberof MobDashboardController
     */
    portletList: IParam[];
    /**
     * @description 部件基础数据初始化
     * @memberof MobDashboardController
     */
    ctrlBasicInit(): void;
    /**
     * @description 部件模型数据加载
     * @memberof MobDashboardController
     */
    ctrlModelLoad(): Promise<void>;
    /**
     * @description 部件初始化
     * @memberof MobDashboardController
     */
    ctrlInit(): void;
    /**
     * @description 初始化工具服务
     * @memberof MobDashboardController
     */
    private initUtilService;
    /**
     * @description 加载门户部件集合
     * @memberof MobDashboardController
     */
    loadPortletList(): Promise<void>;
    /**
     * @description 设置已完成绘制状态
     * @param name 部件名
     * @param data 数据
     * @memberof MobDashboardController
     */
    setIsMounted(name?: string, data?: any): void;
    /**
     * @description 通知部件加载数据
     * @private
     * @param {*} data 参数
     * @memberof MobDashboardController
     */
    private notifyState;
    /**
     * @description 加载模型
     * @memberof MobDashboardController
     */
    loadModel(): Promise<void>;
    /**
     * @description 获取门户部件实例对象
     * @param model 模型
     * @memberof MobDashboardController
     */
    getPortletInstance(model: any): any;
    /**
     * @description 处理私人定制按钮
     * @param event 源对象
     * @memberof MobDashboardController
     */
    handleCustom(view: IParam, event: any): Promise<ICtrlActionResult>;
    /**
     * @description 处理部件事件
     * @param {string} controlname 部件名称
     * @param {string} action 行为
     * @param {*} data 数据
     */
    handleCtrlEvent(controlname: string, action: string, data: any): void;
}
