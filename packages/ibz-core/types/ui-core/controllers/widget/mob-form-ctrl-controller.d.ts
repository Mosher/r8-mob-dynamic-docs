import { IPSDEEditFormItem, IPSDEFDCatGroupLogic, IPSDEForm } from '@ibiz/dynamic-model-api';
import { IParam, IMobFormCtrlController, ICtrlActionResult, IViewStateParam, IEditorEventParam } from '../../../interface';
import { AppCtrlControllerBase } from './app-ctrl-controller-base';
import { Subject } from 'rxjs';
export declare class MobFormCtrlController extends AppCtrlControllerBase implements IMobFormCtrlController {
    /**
     * 表单模型实例对象
     *
     * @type {*}
     * @memberof MobFormCtrlController
     */
    controlInstance: IPSDEForm;
    /**
     * 表单数据对象
     *
     * @type {*}
     * @memberof MobFormCtrlController
     */
    data: IParam;
    /**
     * 值规则
     *
     * @type {*}
     * @memberof MobFormCtrlController
     */
    rules: IParam;
    /**
     * 详情模型集合
     *
     * @type {*}
     * @memberof MobMobFormCtrlController
     */
    detailsModel: IParam;
    /**
     * @description 是否自动加载
     * @protected
     * @type {boolean}
     * @memberof MobFormCtrlController
     */
    protected isAutoLoad?: boolean;
    /**
     * @description 是否自动保存
     * @protected
     * @type {boolean}
     * @memberof MobFormCtrlController
     */
    protected isAutoSave?: boolean;
    /**
     * 是否可以编辑（主要用于工作流提交不需要先更新，直接提交数据）
     *
     * @type {*}
     * @memberof MobFormCtrlController
     */
    protected isEditable?: any;
    /**
     * 表单状态
     *
     * @type {Subject<any>}
     * @memberof MobFormCtrlController
     */
    formState: Subject<IViewStateParam>;
    /**
     * 表单错误提示信息
     *
     * @type {any[]}
     * @memberof  MobFormCtrlController
     */
    errorMessages: any[];
    /**
     * 忽略表单项值变化
     *
     * @type {boolean}
     * @memberof MobFormCtrlController
     */
    ignorefieldvaluechange: boolean;
    /**
     * 关系界面状态Map
     *
     * @memberof MobFormCtrlController
     */
    protected formDRStateMap: Map<string, boolean>;
    /**
     * @description 表单分组锚点集合
     * @type {IParam[]}
     * @memberof MobFormCtrlController
     */
    groupAnchors: IParam[];
    /**
     * 获取单项数据
     *
     * @returns {*}
     * @memberof MobFormCtrlController
     */
    getData(): any;
    /**
     * 部件模型数据初始化实例
     *
     * @param args 额外参数
     * @memberof MobFormCtrlController
     */
    ctrlModelInit(args?: any): Promise<void>;
    /**
     * 部件基础数据初始化
     *
     * @memberof MobFormCtrlController
     */
    ctrlBasicInit(): void;
    /**
     * 部件初始化
     *
     * @param args
     * @memberof MobFormCtrlController
     */
    ctrlInit(args?: any): void;
    /**
     * 初始化data
     *
     * @memberof MobFormCtrlController
     */
    protected initData(): void;
    /**
     * 初始化表单成员模型
     *
     * @memberof MobFormCtrlController
     */
    protected initDetailsModel(): void;
    /**
     * 初始化关系界面模型
     *
     * @memberof MobFormCtrlController
     */
    protected initFormDRPart(): void;
    /**
     * 响应关系界面数据保存
     *
     * @memberof MobFormCtrlController
     */
    reactiveFormDRSave(name: string): void;
    /**
     * 初始化值规则
     *
     * @memberof MobFormCtrlController
     */
    protected initRules(): void;
    /**
     * 初始化界面行为模型
     *
     * @type {*}
     * @memberof MobFormCtrlController
     */
    initCtrlActionModel(): void;
    /**
     * 表单加载
     *
     * @param opts 行为参数
     * @param args 补充逻辑完成参数
     * @param showInfo 是否显示提示信息
     * @param loadding 是否显示loading效果
     * @returns {Promise<IParam>}
     * @memberof MobFormCtrlController
     */
    load(opts?: IParam, args?: IParam, showInfo?: boolean, loadding?: boolean): Promise<ICtrlActionResult>;
    /**
     * 加载草稿
     *
     * @param opts 行为参数
     * @param args 补充逻辑完成参数
     * @param showInfo 是否显示提示信息
     * @param loadding 是否显示loading效果
     * @returns {Promise<IParam>}
     * @memberof MobFormCtrlController
     */
    loadDraft(opts?: IParam, args?: IParam, showInfo?: boolean, loadding?: boolean): Promise<ICtrlActionResult>;
    /**
     * 表单自动加载
     *
     * @param opts 行为参数
     * @param args 补充逻辑完成参数
     * @param showInfo 是否显示提示信息
     * @param loadding 是否显示loading效果
     * @returns {Promise<IParam>}
     * @memberof MobFormCtrlController
     */
    autoLoad(opts?: IParam, args?: IParam, showInfo?: boolean, loadding?: boolean): Promise<ICtrlActionResult>;
    /**
     * 表单保存
     *
     * @param opts 行为参数
     * @param args 补充逻辑完成参数
     * @param showInfo 是否显示提示信息
     * @param loadding 是否显示loading效果
     * @returns {Promise<IParam>}
     * @memberof MobFormCtrlController
     */
    save(opts?: IParam, args?: IParam, showInfo?: boolean, loadding?: boolean): Promise<ICtrlActionResult>;
    /**
     * 表单删除
     *
     * @param opts 行为参数
     * @param args 补充逻辑完成参数
     * @param showInfo 是否显示提示信息
     * @param loadding 是否显示loading效果
     * @returns {Promise<IParam>}
     * @memberof MobFormCtrlController
     */
    remove(opts?: IParam, args?: IParam, showInfo?: boolean, loadding?: boolean): Promise<ICtrlActionResult>;
    /**
     * 工作流启动
     *
     * @param opts 表单数据
     * @param args 补充逻辑完成参数
     * @param showInfo 是否显示提示信息
     * @param loadding 是否显示loading效果
     * @returns {Promise<IParam>}
     * @memberof MobFormCtrlController
     */
    wfstart(opts?: IParam, args?: IParam, showInfo?: boolean, loadding?: boolean): Promise<ICtrlActionResult>;
    /**
     * 工作流提交
     *
     * @param opts 表单数据
     * @param args 补充逻辑完成参数
     * @param showInfo 是否显示提示信息
     * @param loadding 是否显示loading效果
     * @returns {Promise<IParam>}
     * @memberof MobFormCtrlController
     */
    wfsubmit(opts: IParam, args?: IParam, showInfo?: boolean, loadding?: boolean): Promise<ICtrlActionResult>;
    /**
     * 表单刷新
     *
     * @param opts 行为参数
     * @param args 补充逻辑完成参数
     * @param showInfo 是否显示提示信息
     * @param loadding 是否显示loading效果
     * @returns {Promise<IParam>}
     * @memberof MobFormCtrlController
     */
    refresh(opts?: IParam, args?: IParam, showInfo?: boolean, loadding?: boolean): Promise<ICtrlActionResult>;
    /**
     * 面板行为
     *
     * @param opts 行为参数
     * @param args 补充逻辑完成参数
     * @param showInfo 是否显示提示信息
     * @param loadding 是否显示loading效果
     * @returns {Promise<IParam>}
     * @memberof MobFormCtrlController
     */
    panelAction(opts?: IParam, args?: IParam, showInfo?: boolean, loadding?: boolean): Promise<ICtrlActionResult>;
    /**
     * 表单项更新
     *
     * @param mode 界面行为名称
     * @param data 请求数据
     * @param updateDetails 更新项
     * @param loading 是否显示加载状态
     * @memberof MobFormCtrlController
     */
    protected updateFormItems(mode: string, data: any, updateDetails: string[], loading?: boolean): void;
    /**
     * 重置
     *
     * @memberof MobFormCtrlController
     */
    onReset(): void;
    /**
     * 表单加载完成
     *
     * @param {*} data 表单数据
     * @param {string} action 表单行为
     * @memberof MobFormCtrlController
     */
    protected onFormLoad(data: any, action: string): void;
    /**
     * 设置表单项是否启用
     *
     * @param {*} data 表单数据
     * @memberof MobFormCtrlController
     */
    protected setFormEnableCond(data: any): void;
    /**
     * 计算表单按钮权限状态
     *
     * @param {*} data 表单数据
     * @memberof MobFormCtrlController
     */
    protected computeButtonState(data: any): void;
    /**
     * 值填充
     *
     * @param {*} data 填充数据
     * @param {string} action 表单行为
     * @memberof MobFormCtrlController
     */
    protected fillForm(data: any, action: string): void;
    /**
     * 新建默认值
     *
     * @memberof MobFormCtrlController
     */
    protected createDefault(): void;
    /**
     * 更新默认值
     *
     * @memberof MobFormCtrlController
     */
    protected updateDefault(): void;
    /**
     * 表单校验状态
     *
     * @returns {boolean}
     * @memberof MobFormCtrlController
     */
    protected formValidateStatus(): Promise<boolean>;
    /**
     * 表单项值校验
     *
     * @param {string} name 项名称
     * @param {*} data 数据
     * @memberof MobFormCtrlController
     */
    protected validate(name: string): Promise<any>;
    /**
     * 表单项值变更
     *
     * @param {{ name: string, value: any }} $event 名称，值
     * @returns {void}
     * @memberof MobFormCtrlController
     */
    onFormItemValueChange(event: IParam): void;
    /**
     * 处理编辑器事件
     *
     * @param {args: IEditorEventParam} 参数
     * @returns {void}
     * @memberof MobFormCtrlController
     */
    handleEditorEvent(args: IEditorEventParam): void;
    /**
     * 表单逻辑
     *
     * @public
     * @param {{ name: string }} { name } 名称
     * @memberof MobFormCtrlController
     */
    protected formLogic({ name }: {
        name: string;
    }): Promise<void>;
    /**
     * 表单项检查逻辑
     *
     * @param {string} name 属性名
     * @memberof MobFormCtrlController
     */
    protected checkItem(name: string): Promise<boolean>;
    /**
     * 校验动态逻辑结果
     *
     * @param {*} data 数据对象
     * @param {*} logic 逻辑对象
     * @returns
     * @memberof MobFormCtrlController
     */
    protected verifyGroupLogic(data: any, logic: IPSDEFDCatGroupLogic): boolean;
    /**
     * 重置表单项值
     *
     * @public
     * @param {{ name: string }} { name } 名称
     * @memberof MobFormCtrlController
     */
    protected resetFormData({ name }: {
        name: string;
    }): void;
    /**
     * 复合表单项值规则
     *
     * @param detail 复合表单项
     */
    protected getCompositeItemRules(detail: IPSDEEditFormItem): any[];
    /**
     * 执行按钮行为
     *
     * @param {*} formdetail 行为模型
     * @param {*} data 数据
     * @param {*} event 事件源
     * @memberof MobFormCtrlController
     */
    executeButtonAction(formdetail: IParam, data: IParam, event: MouseEvent): Promise<void>;
    /**
     * 处理部件UI响应
     *
     * @param {string} action 行为名称
     * @param {*} response 响应
     * @memberof MobFormCtrlController
     */
    onControlResponse(action: string, response: any): void;
    /**
     * 通过属性名获取表单项
     *
     * @memberof MobFormCtrlController
     */
    findFormItemByField(fieldName: string): IPSDEEditFormItem | undefined;
}
