import { IMobSearchFormCtrlController, IParam, ICtrlActionResult } from '../../../interface';
import { MobFormCtrlController } from './mob-form-ctrl-controller';
import { IPSDESearchForm } from '@ibiz/dynamic-model-api';
/**
 * @description 搜索表单控制器
 * @export
 * @class MobSearchFormCtrlController
 * @extends {MobFormCtrlController}
 * @implements {IMobSearchFormCtrlController}
 */
export declare class MobSearchFormCtrlController extends MobFormCtrlController implements IMobSearchFormCtrlController {
    /**
     * @description 搜索表单模型实例对象
     * @type {IPSDESearchForm}
     * @memberof MobSearchFormCtrlController
     */
    controlInstance: IPSDESearchForm;
    /**
     * @description 是否展开搜索表单
     * @type {boolean}
     * @memberof MobSearchFormCtrlController
     */
    isExpandSearchForm: boolean;
    /**
     * @description 默认展开搜索表单
     * @type {boolean}
     * @memberof MobSearchFormCtrlController
     */
    expandSearchForm: boolean;
    /**
     * @description 初始化输入数据
     * @param {*} opts 输入参数
     * @memberof MobSearchFormCtrlController
     */
    initInputData(opts: any): void;
    /**
     * @description 获取多项数据
     * @return {*}  {any[]}
     * @memberof MobSearchFormCtrlController
     */
    getDatas(): any[];
    /**
     * @description 加载草稿
     * @param {IParam} [opts] 行为参数
     * @param {IParam} [args] 补充逻辑完成参数
     * @param {boolean} [showInfo=true] 是否显示提示信息
     * @param {boolean} [loadding=true] 是否显示loading效果
     * @return {*}  {Promise<ICtrlActionResult>}
     * @memberof MobSearchFormCtrlController
     */
    loadDraft(opts?: IParam, args?: IParam, showInfo?: boolean, loadding?: boolean): Promise<ICtrlActionResult>;
    /**
     * @description 表单项值变更
     * @param {{ name: string; value: any }} event
     * @memberof MobSearchFormCtrlController
     */
    onFormItemValueChange(event: {
        name: string;
        value: any;
    }): void;
    /**
     * @description 重置
     * @memberof MobSearchFormCtrlController
     */
    onReset(): void;
    /**
     * @description 搜索
     * @memberof MobSearchFormCtrlController
     */
    onSearch(): void;
}
