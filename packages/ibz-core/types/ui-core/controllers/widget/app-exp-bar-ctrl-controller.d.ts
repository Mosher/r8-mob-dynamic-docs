import { IPSDECalendar, IPSControlNavigatable, IPSDETree, IPSDEChart, IPSAppView, IPSSysMap } from '@ibiz/dynamic-model-api';
import { IAppExpBarCtrlController, IParam, IUIDataParam } from '../../../interface';
import { AppCtrlControllerBase } from './app-ctrl-controller-base';
/**
 * @description 移动端导航栏部件控制器基类
 * @export
 * @class AppExpBarCtrlController
 * @extends {AppCtrlControllerBase}
 * @implements {IAppExpBarCtrlController}
 */
export declare class AppExpBarCtrlController extends AppCtrlControllerBase implements IAppExpBarCtrlController {
    /**
     * @description 多数据部件
     * @protected
     * @type {(IPSDECalendar | IPSControlNavigatable | IPSDETree)}
     * @memberof AppExpBarCtrlController
     */
    xDataControl: IPSDECalendar | IPSControlNavigatable | IPSDETree | IPSDEChart | IPSSysMap;
    /**
     * @description 多数据部件名称
     * @type {string}
     * @memberof AppExpBarCtrlController
     */
    xDataControlName: string;
    /**
     * @description 导航视图名称
     * @type {*}
     * @memberof AppExpBarCtrlController
     */
    navView: any;
    /**
     * @description 导航视图组件
     * @type {*}
     * @memberof AppExpBarCtrlController
     */
    navViewComponent: any;
    /**
     * @description 导航参数
     * @type {IParam}
     * @memberof AppExpBarCtrlController
     */
    navParam: IParam;
    /**
     * @description 导航上下文
     * @type {IParam}
     * @memberof AppExpBarCtrlController
     */
    navigateContext: IParam;
    /**
     * @description 导航视图参数
     * @type {IParam}
     * @memberof AppExpBarCtrlController
     */
    navigateParams: IParam;
    /**
     * @description 导航过滤项
     * @type {string}
     * @memberof AppExpBarCtrlController
     */
    navFilter: string;
    /**
     * @description 导航关系
     * @type {string}
     * @memberof AppExpBarCtrlController
     */
    navPSDer: string;
    /**
     * @description 选中数据
     * @type {IParam}
     * @memberof AppExpBarCtrlController
     */
    selection: IParam;
    /**
     * @description 工具栏模型数据
     * @type {IParam}
     * @memberof AppExpBarCtrlController
     */
    toolbarModels: IParam;
    /**
     * @description 快速分组数据对象
     * @type {*}
     * @memberof AppExpBarCtrlController
     */
    quickGroupData: any;
    /**
     * @description 快速分组是否有抛值
     * @type {boolean}
     * @memberof AppExpBarCtrlController
     */
    isEmitQuickGroupValue: boolean;
    /**
     * @description 快速分组模型
     * @type {Array<any>}
     * @memberof AppExpBarCtrlController
     */
    quickGroupModel: Array<any>;
    /**
     * @description 部件模型加载
     * @memberof AppExpBarCtrlController
     */
    ctrlModelLoad(): Promise<void>;
    /**
     * @description 处理多数据部件配置
     * @protected
     * @memberof AppExpBarCtrlController
     */
    protected handleXDataOptions(): Promise<void>;
    /**
     * @description 部件初始化
     * @param {*} args 额外参数
     * @memberof AppExpBarCtrlController
     */
    ctrlInit(args?: any): void;
    /**
     * @description 选中数据变化
     * @protected
     * @param {IUIDataParam} uiDataParam UI数据
     * @return {*}  {void}
     * @memberof AppExpBarCtrlController
     */
    protected onSelectionChange(uiDataParam: IUIDataParam): void;
    /**
     * @description 计算工具栏权限
     * @protected
     * @param {boolean} state
     * @return {*}
     * @memberof AppExpBarCtrlController
     */
    protected calcToolbarItemState(state: boolean): void;
    /**
     * 打开视图导航视图
     *
     * @param {IPSDEChartSeries} seriesItem
     * @param {*} tempContext
     * @param {*} args
     * @memberof MobChartExpBarController
     */
    openExplorerView(explorerView: IPSAppView, args: any, tempContext: any, tempViewParam: any): Promise<void>;
}
