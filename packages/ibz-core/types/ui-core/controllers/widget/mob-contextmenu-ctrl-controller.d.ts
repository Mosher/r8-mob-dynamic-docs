import { IPSDEContextMenu } from '@ibiz/dynamic-model-api';
import { IMobContextMenuCtrlController, IParam } from '../../../interface';
import { AppCtrlControllerBase } from './app-ctrl-controller-base';
/**
 * 移动端上下文菜单控制器
 *
 * @exports
 * @class MobContextMenuCtrlController
 * @extends AppCtrlControllerBase
 * @implements IMobContextMenuCtrlController
 */
export declare class MobContextMenuCtrlController extends AppCtrlControllerBase implements IMobContextMenuCtrlController {
    /**
     * @description 上下文菜单部件模型实例
     * @type {IPSDEContextMenu}
     * @memberof MobContextMenuCtrlController
     */
    controlInstance: IPSDEContextMenu;
    /**
     * @description 上下文菜单行为模型
     * @type {IParam}
     * @memberof MobContextMenuCtrlController
     */
    contextMenuActionModel: IParam;
    /**
     * @description 触发上下文菜单打开的事件源
     * @type {MouseEvent}
     * @memberof MobContextMenuCtrlController
     */
    mouseEvent: IParam;
    /**
     * @description 初始化输入参数
     * @param {IParam} opts
     * @memberof MobContextMenuCtrlController
     */
    initInputData(opts: IParam): void;
}
