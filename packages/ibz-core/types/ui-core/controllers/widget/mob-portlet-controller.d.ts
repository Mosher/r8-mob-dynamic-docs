import { IPSDBPortletPart } from '@ibiz/dynamic-model-api';
import { ICtrlActionResult, IMobPortletController, IParam } from '../../../interface';
import { AppCtrlControllerBase } from './app-ctrl-controller-base';
/**
 * 移动端门户部件控制器
 *
 * @exports
 * @class MobPortletController
 * @extends AppCtrlControllerBase
 * @implements IMobPortletController
 */
export declare class MobPortletController extends AppCtrlControllerBase implements IMobPortletController {
    /**
     * @description 门户部件实例对象
     * @type {IPSDBPortletPart}
     * @memberof MobPortletController
     */
    controlInstance: IPSDBPortletPart;
    /**
     * @description 操作栏模型数据
     * @type {IParam[]}
     * @memberof MobPortletController
     */
    actionBarModel: IParam[];
    /**
     * @description 是否视图已经完成
     * @type {boolean}
     * @memberof MobPortletController
     */
    hasViewMounted: boolean;
    /**
     * @description 部件基础数据初始化
     * @memberof MobPortletController
     */
    ctrlBasicInit(): void;
    /**
     * @description 初始化界面行为模型
     * @memberof MobPortletController
     */
    initActionBarModel(): void;
    /**
     * @description 部件初始化
     * @memberof MobPortletController
     */
    ctrlInit(): void;
    /**
     * @description 通知部件加载
     * @param data
     * @memberof MobPortletController
     */
    private notifyControlLoad;
    /**
     *
     * @description 刷新
     * @param {IParam} args 数据
     * @param {IParam} opts 额外参数
     * @param {boolean} showInfo 是否展示结果
     * @param {boolean} isloadding 是否显示加载效果
     */
    refresh(args?: IParam, opts?: IParam, showInfo?: boolean, isloadding?: boolean): Promise<ICtrlActionResult>;
    /**
     * @description 刷新全部
     * @param {*} data 数据
     */
    refreshAll(data?: any): void;
    /**
     * @description 计算操作栏权限
     * @param data
     * @memberof MobPortletController
     */
    calcUIActionAuthState(data?: IParam): void;
    /**
     * @description 操作栏按钮点击
     * @param {string} tag 标识
     * @param {*} event 源事件对象
     * @memberof MobPortletController
     */
    actionBarItemClick(tag: string, event: any): void;
    /**
     * 处理视图事件
     *
     * @param {string} viewName 视图名称
     * @param {string} action 行为标识
     * @param {*} data 数据
     */
    handleViewEvent(viewName: string, action: string, data: any): void;
    /**
     * @description 部件挂载
     * @param {*} [args]
     * @memberof MobPortletController
     */
    ctrlMounted(args?: any): void;
}
