import { IPSDETabViewPanel } from '@ibiz/dynamic-model-api';
import { ICtrlActionResult, IMobTabViewPanelCtrlController, IParam } from '../../../interface';
import { AppCtrlControllerBase } from './app-ctrl-controller-base';
export declare class MobTabViewPanelCtrlController extends AppCtrlControllerBase implements IMobTabViewPanelCtrlController {
    /**
     * @description 部件模型实例对象
     * @type {IPSDETabViewPanel}
     * @memberof MobTabViewPanelCtrlController
     */
    controlInstance: IPSDETabViewPanel;
    /**
     * 刷新
     *
     * @param opts 行为参数
     * @param args 补充逻辑完成参数
     * @param showInfo 是否显示提示信息
     * @param loadding 是否显示loading效果
     * @returns {Promise<IParam>}
     * @memberof MobTabExpPanelCtrlController
     */
    refresh(opts?: IParam, args?: IParam, showInfo?: boolean, loadding?: boolean): Promise<ICtrlActionResult>;
    /**
     * @description 获取导航参数
     * @return {*}  {{ context: IParam, viewParam: IParam }}
     * @memberof MobTabViewPanelCtrlController
     */
    getNavParams(): {
        context: IParam;
        viewParam: IParam;
    };
}
