import { IPSDEMobMDCtrl } from '@ibiz/dynamic-model-api';
import { IMobMDCtrlController, ICtrlActionResult, IParam } from '../../../interface';
import { AppMDCtrlController } from './app-md-ctrl-controller';
export declare class MobMDCtrlController extends AppMDCtrlController implements IMobMDCtrlController {
    /**
     * @description 移动端多数据部件实例对象
     * @type {IPSDEMobMDCtrl}
     * @memberof MobMDCtrlController
     */
    controlInstance: IPSDEMobMDCtrl;
    /**
     * @description 单选选择值
     * @private
     * @type {string}
     * @memberof MobMDCtrlController
     */
    private selectedValue;
    /**
     * @description 是否需要加载更多
     * @readonly
     * @type {boolean}
     * @memberof MobMDCtrlController
     */
    get needLoadMore(): boolean;
    /**
     * @description 多数据部件模型数据加载
     * @memberof MobMDCtrlController
     */
    ctrlModelLoad(): Promise<void>;
    /**
     * @description 初始化数据映射
     * @memberof MobMDCtrlController
     */
    initDataMap(): void;
    /**
     * @description 部件初始化
     * @param {IParam} [args] 初始化参数
     * @memberof MobMDCtrlController
     */
    ctrlInit(args?: IParam): void;
    /**
     * @description 删除
     * @param {IParam[]} datas 要删除的数据集合
     * @param {IParam} [args] 行为参数
     * @param {boolean} [showInfo=true] 是否显示提示信息
     * @param {boolean} [loadding=true] 是否显示loadding效果
     * @return {*}  {Promise<any>}
     * @memberof MobMDCtrlController
     */
    remove(datas: IParam[], args?: IParam, showInfo?: boolean, loadding?: boolean): Promise<any>;
    /**
     * @description 刷新数据
     * @param {IParam} opts 行为参数
     * @param {IParam} [args] 额外行为参数
     * @param {boolean} [showInfo] 是否显示提示
     * @param {boolean} [loadding] 是否显示loadding效果
     * @return {*}  {Promise<ICtrlActionResult>}
     * @memberof MobMDCtrlController
     */
    refresh(opts: IParam, args?: IParam, showInfo?: boolean, loadding?: boolean): Promise<ICtrlActionResult>;
    /**
     * @description 数据加载
     * @param {IParam} [opts] 行为参数
     * @param {boolean} [showInfo=true] 是否显示提示信息
     * @param {boolean} [loadding=true] 是否
     * @param {*} [args]
     * @return {*}  {Promise<ICtrlActionResult>}
     * @memberof MobMDCtrlController
     */
    load(opts?: IParam, args?: IParam, showInfo?: boolean, loadding?: boolean): Promise<ICtrlActionResult>;
    /**
     * @description 获取界面行为权限状态
     * @private
     * @param {*} data 数据
     * @return {*}
     * @memberof MobMDCtrlController
     */
    private getActionState;
    /**
     * @description 初始化界面行为模型
     * @memberof MobMDCtrlController
     */
    initCtrlActionModel(): void;
    /**
     * @description 初始化界面行为组成员模型
     * @private
     * @param {IPSUIActionGroupDetail} detail 行为组成员模型
     * @return {*}
     * @memberof MobMDCtrlController
     */
    private getCtrlActionModelDetail;
    /**
     * @description 计算列表项左滑右滑禁用状态
     * @private
     * @param {IParam} item 项数据
     * @memberof MobMDCtrlController
     */
    private computeSlidingDisabled;
    /**
     * @description 选中数据改变
     * @param {any[]} selections 选中数组
     * @memberof MobMDCtrlController
     */
    selectionChange(selections: any[]): void;
    /**
     * @description 项选中
     * @param {*} item 项数据
     * @param {*} event 事件源
     * @memberof MobMDCtrlController
     */
    onRowSelect(item: any, event: MouseEvent): void;
    /**
     * @description 上拉加载更多数据
     * @return {*}  {Promise<ICtrlActionResult>}
     * @memberof MobMDCtrlController
     */
    loadBottom(): Promise<ICtrlActionResult>;
}
