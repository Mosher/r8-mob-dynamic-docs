import { IPSDEWizardPanel } from '@ibiz/dynamic-model-api';
import { AppCtrlControllerBase } from './app-ctrl-controller-base';
import { IMobStateWizardPanelController, IParam, IViewStateParam } from '../../../interface';
import { Subject } from 'rxjs';
export declare class MobStateWizardPanelController extends AppCtrlControllerBase implements IMobStateWizardPanelController {
    /**
     * @description 移动端状态向导面板实例对象
     * @type {IPSDEWizardPanel}
     * @memberof MobStateWizardPanelController
     */
    controlInstance: IPSDEWizardPanel;
    /**
     * @description 初始化行为
     * @private
     * @type {string}
     * @memberof MobStateWizardPanelController
     */
    private initAction;
    /**
     * @description 完成行为
     * @private
     * @type {string}
     * @memberof MobStateWizardPanelController
     */
    private finishAction;
    /**
     * @description 状态属性
     * @private
     * @type {string}
     * @memberof MobStateWizardPanelController
     */
    private stateField;
    /**
     * @description FormLoad是否被阻塞
     * @private
     * @type {boolean}
     * @memberof MobWizardPanelController
     */
    private hasFormLoadBlocked;
    /**
     * @description 首表单
     * @type {string}
     * @memberof MobStateWizardPanelController
     */
    firstForm: string;
    /**
     * @description 步骤行为集合
     * @type {IParam}
     * @memberof MobStateWizardPanelController
     */
    stepActions: IParam;
    /**
     * 步骤标识集合
     *
     * @type {IParam}
     * @memberof MobStateWizardPanelController
     */
    stepTags: IParam;
    /**
     * @description 执行过的表单
     * @type {string[]}
     * @memberof MobStateWizardPanelController
     */
    historyForms: string[];
    /**
     * @description 当前激活步骤表单
     * @type {string}
     * @memberof MobStateWizardPanelController
     */
    activeForm: string;
    /**
     * @description 向导表单集合
     * @type {any[]}
     * @memberof MobStateWizardPanelController
     */
    wizardForms: any[];
    /**
     * @description 向导表单参数
     * @type {IParam}
     * @memberof MobStateWizardPanelController
     */
    formParam: IParam;
    /**
     * @description 当前状态
     * @private
     * @type {string}
     * @memberof MobStateWizardPanelController
     */
    private curState;
    /**
     * @description 向导步骤集合
     * @type {IParam[]}
     * @memberof MobStateWizardPanelController
     */
    steps: IParam[];
    /**
     * @description 视图状态订阅对象
     * @type {Subject<IViewStateParam>}
     * @memberof MobStateWizardPanelController
     */
    wizardState: Subject<IViewStateParam>;
    /**
     * @description 部件基础数据初始化
     * @memberof MobStateWizardPanelController
     */
    ctrlBasicInit(): void;
    /**
     * @description 部件模型数据加载
     * @memberof MobStateWizardPanelController
     */
    ctrlModelLoad(): Promise<void>;
    /**
     * @description 部件初始化
     * @param {*} [args]
     * @memberof MobStateWizardPanelController
     */
    ctrlInit(args?: any): void;
    /**
     * @description 初始化首表单
     * @protected
     * @memberof MobStateWizardPanelController
     */
    protected initFirstForm(): void;
    /**
     * @description 注册表单步骤行为
     * @memberof MobStateWizardPanelController
     */
    regFormActions(): void;
    /**
     * @description 注册表单
     * @param {string} name 名称
     * @param {Array<string>} actions 步骤集合
     * @param {*} stepTag 步骤标识
     * @memberof MobStateWizardPanelController
     */
    regFormAction(name: string, actions: IParam, stepTag: any): void;
    /**
     * @description 获取步骤标识
     * @param {Array<any>} wizardSteps 步骤数组
     * @param {string} tag 标识
     * @return {*}
     * @memberof MobStateWizardPanelController
     */
    getStepTag(wizardSteps: Array<any>, tag: string): any;
    /**
     * @description 初始化
     * @param {*} [opt={}] 额外参数
     * @memberof MobStateWizardPanelController
     */
    doInit(opt?: any): void;
    /**
     * @description 计算激活表单
     * @param {*} data
     * @memberof MobStateWizardPanelController
     */
    computedActiveForm(data: any): void;
    /**
     * @description 状态表单加载完成
     * @param {*} args 表单参数
     * @param {string} name 名称
     * @memberof MobStateWizardPanelController
     */
    wizardPanelFormLoad(name: string, args: any): void;
    /**
     * @description 向导表单保存完成
     * @param {*} args 表单参数
     * @param {string} name 名称
     * @memberof MobStateWizardPanelController
     */
    wizardPanelFormSave(name: string, args: any): void;
    /**
     * @description 获取下一步向导表单
     * @return {*}
     * @memberof MobStateWizardPanelController
     */
    getNextForm(): any;
    /**
     * @description 处理上一步
     * @memberof MobStateWizardPanelController
     */
    handlePrevious(): void;
    /**
     * @description 处理下一步
     * @memberof MobStateWizardPanelController
     */
    handleNext(): void;
    /**
     * @description 处理完成步骤
     * @memberof MobStateWizardPanelController
     */
    handleFinish(): void;
    /**
     * @description 执行完成
     * @memberof MobStateWizardPanelController
     */
    doFinish(): void;
    /**
     * @description 表单加载
     * @param {string} name 表单名称
     * @memberof MobStateWizardPanelController
     */
    formLoad(data: any): void;
    /**
     * @description 处理部件事件
     * @param {string} controlname 部件名称
     * @param {string} action 行为
     * @param {*} data 数据
     * @memberof MobStateWizardPanelController
     */
    handleCtrlEvent(controlname: string, action: string, data: any): void;
}
