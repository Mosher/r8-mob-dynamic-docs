import { AppMDCtrlController } from './app-md-ctrl-controller';
import { IMobCalendarCtrlController, IParam, ICtrlActionResult } from '../../../interface';
import { IPSDECalendar } from '@ibiz/dynamic-model-api';
export declare class MobCalendarCtrlController extends AppMDCtrlController implements IMobCalendarCtrlController {
    /**
     * @description 日历部件模型实例
     * @type {IPSDECalendar}
     * @memberof MobCalendarCtrlController
     */
    controlInstance: IPSDECalendar;
    /**
     * @description 开始时间
     * @type {*}
     * @memberof MobCalendarCtrlController
     */
    start: string;
    /**
     * @description 结束时间
     * @type {string}
     * @memberof MobCalendarCtrlController
     */
    end: string;
    /**
     * @description 标记数据
     * @type {IParam[]}
     * @memberof MobCalendarCtrlController
     */
    sign: IParam[];
    /**
     * @description 日历项集合对象
     * @type {*}
     * @memberof MobCalendarCtrlController
     */
    calendarItems: IParam;
    /**
     * @description 是否展示多选
     * @type {boolean}
     * @memberof MobCalendarCtrlController
     */
    isChoose: boolean;
    /**
     * @description 激活项
     * @type {string}
     * @memberof MobCalendarCtrlController
     */
    activeItem: string;
    /**
     * @description 日历数据项模型
     * @private
     * @type {Map<string, any>}
     * @memberof MobCalendarCtrlController
     */
    private calendarItemsModel;
    /**
     * @description 日历样式类型
     * @type {string}
     * @memberof MobCalendarCtrlController
     */
    calendarStyle: string;
    /**
     * @description 图标信息
     * @type {IParam[]}
     * @memberof MobCalendarCtrlController
     */
    illustration: IParam[];
    /**
     * @description 日历显示状态
     * @type {boolean}
     * @memberof MobCalendarCtrlController
     */
    show: boolean;
    /**
     * @description 当前时间
     * @type {*}
     * @memberof MobCalendarCtrlController
     */
    curTime: IParam;
    /**
     * @description 日历部件模型数据初始化实例
     * @param {*} [args] 初始化参数
     * @memberof MobCalendarCtrlController
     */
    ctrlModelInit(args?: any): Promise<void>;
    /**
     * @description 日历部件模型数据加载
     * @memberof MobCalendarCtrlController
     */
    ctrlModelLoad(): Promise<void>;
    /**
     * @description 日历部件基础数据初始化
     * @memberof MobCalendarCtrlController
     */
    ctrlBasicInit(): void;
    /**
     * @description 日历视图部件初始化
     * @memberof MobCalendarCtrlController
     */
    ctrlInit(): void;
    /**
     * @description 初始化日历部件上下文菜单模型
     * @memberof MobCalendarCtrlController
     */
    initCtrlActionModel(): void;
    /**
     * @description 初始化上下菜单项
     * @private
     * @param {IPSDEToolbarItem} toolbarItem 工具栏项
     * @param {IPSSysCalendarItem} item 日历项
     * @param {*} tempModel 模型数据
     * @memberof MobCalendarCtrlController
     */
    private initActionModelItem;
    /**
     * @description 初始化日历数据项模型
     * @private
     * @memberof MobCalendarCtrlController
     */
    private initCalendarItemsModel;
    /**
     * @description 初始化图标信息
     * @private
     * @memberof MobCalendarCtrlController
     */
    private initIllustration;
    /**
     * @description 初始化激活项
     * @private
     * @memberof MobCalendarCtrlController
     */
    private initActiveItem;
    /**
     * @description 加载数据
     * @param {IParam} [opts] 行为参数
     * @param {IParam} [args] 附加行为参数
     * @param {boolean} [isSetEvent=true] 是否设置事程
     * @param {boolean} [loadding=true] 是否显示loadding效果
     * @return {*}  {Promise<ICtrlActionResult>}
     * @memberof MobCalendarCtrlController
     */
    load(opts?: IParam, args?: IParam, isSetEvent?: boolean, loadding?: boolean): Promise<ICtrlActionResult>;
    /**
     * @description 删除
     * @param {IParam} [opts] 行为参数
     * @param {IParam} [args] 附加行为参数
     * @param {boolean} [showInfo=true] 是否显示提示信息
     * @param {boolean} [loadding=true] 是否显示loadding效果
     * @return {*}  {Promise<ICtrlActionResult>}
     * @memberof MobCalendarCtrlController
     */
    remove(opts?: IParam, args?: IParam, showInfo?: boolean, loadding?: boolean): Promise<ICtrlActionResult>;
    /**
     * @description 格式化时间（根据日历样式格式化开始时间和结束时间）
     * @param {*} curtime 当前时间
     * @param {IParam} [opts] 行为参数
     * @param {IParam} [args] 附加行为参数
     * @param {boolean} [isSetEvent=true] 是否设置事程
     * @param {boolean} [loadding=true] 是否显示loadding效果
     * @return {*}  {Promise<ICtrlActionResult>}
     * @memberof MobCalendarCtrlController
     */
    formatDate(curtime: IParam, opts?: IParam, args?: IParam, isSetEvent?: boolean, loadding?: boolean): Promise<ICtrlActionResult>;
    /**
     * @description 刷新数据
     * @param {IParam} opts 行为参数
     * @param {IParam} [args] 附加行为参数
     * @param {boolean} [isSetEvent=true] 是否设置事程
     * @param {boolean} [loadding=true] 是否显示loadding效果
     * @return {*}  {Promise<ICtrlActionResult>}
     * @memberof MobCalendarCtrlController
     */
    refresh(opts: IParam, args?: IParam, isSetEvent?: boolean, loadding?: boolean): Promise<ICtrlActionResult>;
    /**
     * @description 设置事程数据
     * @private
     * @memberof MobCalendarCtrlController
     */
    private setEventData;
    /**
     * @description 设置标记数据
     * @private
     * @param {*} signData
     * @memberof MobCalendarCtrlController
     */
    private setSign;
    /**
     * @description 解析日历时间数据
     * @private
     * @param {string} tag 日历项标识
     * @param {string} mark 开始时间
     * @return {*}  {Array<any>}
     * @memberof MobCalendarCtrlController
     */
    private parsingData;
    /**
     * @description 激活项改变
     * @param {string} curActiveItem 激活项
     * @memberof MobCalendarCtrlController
     */
    activeItemChange(curActiveItem: string): void;
    /**
     * @description 获取编辑视图信息
     * @private
     * @param {string} item 日历项
     * @return {*}
     * @memberof MobCalendarCtrlController
     */
    private getEditView;
    /**
     * @description 打开视图
     * @private
     * @param {*} $event 事件信息
     * @return {*}  {Promise<any>}
     * @memberof MobCalendarCtrlController
     */
    openView($event: IParam): Promise<any>;
}
