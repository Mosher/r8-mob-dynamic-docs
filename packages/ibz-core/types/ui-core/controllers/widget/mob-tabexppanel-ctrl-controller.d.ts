import { IPSDETabViewPanel, IPSTabExpPanel } from '@ibiz/dynamic-model-api';
import { ICtrlActionResult, IMobTabExpPanelCtrlController, IParam } from '../../../interface';
import { AppCtrlControllerBase } from './app-ctrl-controller-base';
export declare class MobTabExpPanelCtrlController extends AppCtrlControllerBase implements IMobTabExpPanelCtrlController {
    /**
     * @description 部件模型实例对象
     * @type {IPSTabExpPanel}
     * @memberof MobTabExpPanelCtrlController
     */
    controlInstance: IPSTabExpPanel;
    /**
     * @description 激活项
     * @type {string}
     * @memberof MobTabExpPanelCtrlController
     */
    activeItem: string;
    /**
     * @description 部件模型加载
     * @memberof MobTabExpPanelCtrlController
     */
    ctrlModelLoad(): Promise<void>;
    /**
     * 部件基础数据初始化
     *
     * @memberof MobTabExpPanelCtrlController
     */
    ctrlBasicInit(): void;
    /**
     * @description 初始化激活项
     * @private
     * @memberof MobTabExpPanelCtrlController
     */
    private initActiveItem;
    /**
     * @description 部件初始化
     * @param {*} [args] 初始化参数
     * @memberof MobTabExpPanelCtrlController
     */
    ctrlInit(args?: any): void;
    /**
     * 刷新
     *
     * @param opts 行为参数
     * @param args 补充逻辑完成参数
     * @param showInfo 是否显示提示信息
     * @param loadding 是否显示loading效果
     * @returns {Promise<IParam>}
     * @memberof MobTabExpPanelCtrlController
     */
    refresh(opts?: IParam, args?: IParam, showInfo?: boolean, loadding?: boolean): Promise<ICtrlActionResult>;
    /**
     * @description 获取计数器数据
     * @param {IPSDETabViewPanel} viewPanel
     * @return {string | number | null}  {(string | number | null)}
     * @memberof MobTabExpPanelCtrlController
     */
    getCounterData(viewPanel: IPSDETabViewPanel): string | number | null;
    /**
     * @description 激活项改变
     * @param {string} activeItem 激活项
     * @memberof MobTabExpPanelCtrlController
     */
    activeItemChange(activeItem: string): void;
}
