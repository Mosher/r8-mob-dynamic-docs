import { IPSSysMap } from '@ibiz/dynamic-model-api';
import { ICtrlActionResult, IMobMapCtrlController, IParam } from '../../../interface';
import { AppMDCtrlController } from './app-md-ctrl-controller';
export declare class MobMapCtrlController extends AppMDCtrlController implements IMobMapCtrlController {
    /**
     * 移动端菜单部件实例对象
     *
     * @type {IPSAppMenu}
     * @memberof MobMapCtrlController
     */
    controlInstance: IPSSysMap;
    /**
     * 获取地址需求AMap插件对象
     *
     * @type {*}
     * @memberof MobMapCtrlController
     */
    geocoder: any;
    /**
     * 当前 window
     *
     * @type {*}
     * @memberof MobMapCtrlController
     */
    win: any;
    /**
     * 获取选中数据
     *
     * @returns {any[]}
     * @memberof MobMapCtrlController
     */
    getSelection(): any[];
    /**
     * 地图数据
     *
     * @type {*}
     * @memberof MobMapCtrlController
     */
    items: Array<any>;
    /**
     * 地图数据项模型
     *
     * @type {}
     * @memberof MobMapCtrlController
     */
    mapItems: any;
    /**
     * 默认排序方向
     *
     * @readonly
     * @memberof MobMapCtrlController
     */
    minorSortDir: any;
    /**
     * 默认排序应用实体属性
     *
     * @readonly
     * @memberof MobMapCtrlController
     */
    minorSortPSDEF: any;
    /**
     * 初始化输入数据
     *
     * @param opts 输入参数
     * @memberof MobMapCtrlController
     */
    initInputData(opts: any): void;
    /**
     * 多数据部件模型数据加载
     *
     * @memberof MobMapCtrlController
     */
    ctrlModelLoad(): Promise<void>;
    /**
     * 部件基础数据初始化
     *
     * @memberof MobMapCtrlController
     */
    ctrlBasicInit(): void;
    /**
     * 初始化
     *
     * @memberof MobMapCtrlController
     */
    ctrlInit(args?: any): void;
    /**
     * 部件挂载完毕
     *
     * @protected
     * @memberof MobMapCtrlController
     */
    ctrlMounted(): void;
    /**
     * 刷新
     *
     * @param {*} [args={}]
     * @memberof MobMapCtrlController
     */
    refresh(opts?: IParam, args?: IParam, showInfo?: boolean, loadding?: boolean): Promise<ICtrlActionResult>;
    /**
     * 地图数据加载
     *
     * @param {*} [data={}] 额外参数
     * @returns {void}
     * @memberof MobMapCtrlController
     */
    load(data?: any): void;
    /**
     * @description 初始化地图部件上下文菜单模型
     * @memberof MobMapCtrlController
     */
    initCtrlActionModel(): void;
    /**
     * @description 初始化上下菜单项
     * @private
     * @param {IPSDEToolbarItem} toolbarItem 工具栏项
     * @param {IPSSysCalendarItem} item 地图数据项
     * @param {*} tempModel 模型数据
     * @memberof MobMapCtrlController
     */
    private initActionModelItem;
}
