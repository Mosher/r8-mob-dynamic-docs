import { IPSDEList, IPSDEMobMDCtrl, IPSListExpBar } from '@ibiz/dynamic-model-api';
import { IMobListExpBarCtrlController } from '../../../interface';
import { AppExpBarCtrlController } from './app-exp-bar-ctrl-controller';
export declare class MobListExpBarController extends AppExpBarCtrlController implements IMobListExpBarCtrlController {
    /**
     * @description 移动端列表导航部件实例
     * @public
     * @type { IPSDEMobListExpBar }
     * @memberof MobListExpBarController
     */
    controlInstance: IPSListExpBar;
    /**
     * @description 多数据部件
     * @type {(IPSDEList | IPSDEMobMDCtrl)}
     * @memberof MobListExpBarController
     */
    xDataControl: IPSDEList | IPSDEMobMDCtrl;
    /**
     * @description 处理多数据部件配置
     * @protected
     * @memberof MobListExpBarController
     */
    protected handleXDataOptions(): Promise<void>;
    /**
     * @description 处理部件事件
     * @param {string} controlname 部件名称
     * @param {string} action 行为标识
     * @param {*} data UI数据
     * @memberof MobListExpBarController
     */
    handleCtrlEvent(controlname: string, action: string, data: any): void;
}
