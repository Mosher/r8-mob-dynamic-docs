import { ICtrlActionResult, IMobChartCtrlController, IParam } from '../../../interface';
import { AppMDCtrlController } from './app-md-ctrl-controller';
import { IPSDEChart } from '@ibiz/dynamic-model-api';
export declare class MobChartCtrlController extends AppMDCtrlController implements IMobChartCtrlController {
    /**
     * @description 图表实例模型对象
     * @type {IPSDEChart}
     * @memberof MobChartCtrlController
     */
    controlInstance: IPSDEChart;
    /**
     * @description 图标唯一id
     * @type {string}
     * @memberof MobChartCtrlController
     */
    chartId: string;
    /**
     * @description 是否无数据
     * @type {boolean}
     * @memberof MobChartCtrlController
     */
    isNoData: boolean;
    /**
     * @description echarts图表对象
     * @type {*}
     * @memberof MobChartCtrlController
     */
    myChart: any;
    /**
     * @description 序列模型
     * @type {IParam}
     * @memberof MobChartCtrlController
     */
    seriesModel: IParam;
    /**
     * @description 图表自定义参数集合
     * @type {IParam}
     * @memberof MobChartCtrlController
     */
    chartUserParams: IParam;
    /**
     * @description 图表基础动态模型
     * @type {IParam}
     * @memberof MobChartCtrlController
     */
    chartBaseOPtion: IParam;
    /**
     * @description 图表绘制最终参数
     * @type {IParam}
     * @memberof MobChartCtrlController
     */
    chartRenderOption: IParam;
    /**
     * @description 初始化图表所需参数
     * @type {IParam}
     * @memberof MobChartCtrlController
     */
    chartOption: IParam;
    /**
    * @description 部件模型数据初始化实例
    * @param {*} [args] 初始化参数
    * @memberof MobChartCtrlController
    */
    ctrlModelInit(args?: any): Promise<void>;
    /**
     * @description 初始化图表基础动态模型
     * @private
     * @memberof MobChartCtrlController
     */
    private initChartBaseOPtion;
    /**
     * @description 部件基础数据初始化
     * @memberof MobChartCtrlController
     */
    ctrlBasicInit(): void;
    /**
     * @description 获取多项数据
     * @return {*}  {any[]}
     * @memberof MobChartCtrlController
     */
    getDatas(): any[];
    /**
     * @description 部件初始化
     * @memberof MobChartCtrlController
     */
    ctrlInit(): void;
    /**
     * @description 刷新
     * @param {IParam} [opts] 行为参数
     * @param {IParam} [args] 额外行为参数
     * @param {boolean} [showInfo] 是否显示提示
     * @param {boolean} [loadding] 是否显示loadding效果
     * @return {*}  {Promise<ICtrlActionResult>}
     * @memberof MobChartCtrlController
     */
    refresh(opts?: IParam, args?: IParam, showInfo?: boolean, loadding?: boolean): Promise<ICtrlActionResult>;
    /**
     * @description 加载
     * @param {IParam} [opts] 行为参数
     * @param {IParam} [args] 额外行为参数
     * @param {boolean} [showInfo=true] 是否显示提示
     * @param {boolean} [loadding=true] 是否显示loadding效果
     * @return {*}  {Promise<ICtrlActionResult>}
     * @memberof MobChartCtrlController
     */
    load(opts?: IParam, args?: IParam, showInfo?: boolean, loadding?: boolean): Promise<ICtrlActionResult>;
}
