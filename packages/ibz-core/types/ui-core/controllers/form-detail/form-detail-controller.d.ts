import { IPSDEFormDetail } from '@ibiz/dynamic-model-api';
import { IParam } from '../../../interface';
import { MobFormCtrlController } from '../widget';
/**
 * 表单成员模型
 *
 * @export
 * @class FormDetailController
 */
export declare class FormDetailController {
    /**
     * 是否有权限
     *
     * @type {boolean}
     * @memberof FormDetailController
     */
    isPower: boolean;
    /**
     * 成员标题
     *
     * @type {string}
     * @memberof FormDetailController
     */
    caption: string;
    /**
     * 成员类型
     *
     * @type {string}
     * @memberof FormDetailController
     */
    detailType: string;
    /**
     * 表单对象
     *
     * @type {*}
     * @memberof FormDetailController
     */
    parentController: MobFormCtrlController;
    /**
     * 成员名称
     *
     * @type {string}
     * @memberof FormDetailController
     */
    name: string;
    /**
     * 成员是否显示
     *
     * @type {boolean}
     * @memberof FormDetailController
     */
    $visible: boolean;
    /**
     * 成员是否显示(旧)
     *
     * @type {boolean}
     * @memberof FormDetailController
     */
    oldVisible: boolean;
    /**
     * 成员是否为受控内容
     *
     * @type {boolean}
     * @memberof FormDetailController
     */
    isControlledContent: boolean;
    /**
     * 成员是否显示标题
     *
     * @type {boolean}
     * @memberof FormDetailController
     */
    isShowCaption: boolean;
    /**
     * 模型数据
     *
     * @type {IParam}
     * @memberof FormDetailController
     */
    model: IPSDEFormDetail;
    /**
     * 应用上下文
     *
     * @type {IParam}
     * @memberof FormDetailController
     */
    context: IParam;
    /**
     * 视图参数
     *
     * @type {IParam}
     * @memberof FormDetailController
     */
    viewParam: IParam;
    /**
     * 视图操作参数集合
     *
     * @type {IParam}
     * @memberof FormDetailController
     */
    viewCtx: IParam;
    /**
     * 绑定样式表
     *
     * @type {string | null}
     * @memberof FormDetailController
     */
    customClass: string | null;
    /**
     * 表单数据
     *
     * @type {string | null}
     * @memberof FormDetailController
     */
    data: IParam;
    /**
     * Creates an instance of FormDetailController.
     * FormDetailController 实例
     *
     * @param {*} [opts={}]
     * @memberof FormDetailController
     */
    constructor(opts?: any);
    /**
     * 初始化输入数据
     *
     * @public
     * @memberof FormDetailController
     */
    initInputData(opts: IParam): void;
    /**
     * 设置成员是否隐藏
     *
     * @memberof FormDetailController
     */
    set visible(val: boolean);
    /**
     * 获取成员是否隐藏
     *
     * @memberof FormDetailController
     */
    get visible(): boolean;
    /**
     * 设置显示与隐藏
     *
     * @param {boolean} state
     * @memberof FormDetailController
     */
    setVisible(state: boolean): void;
    /**
     * 设置显示标题栏
     *
     * @param {boolean} state
     * @memberof FormDetailController
     */
    setShowCaption(state: boolean): void;
}
