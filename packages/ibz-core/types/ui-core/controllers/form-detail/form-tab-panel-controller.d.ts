import { IPSDEFormTabPanel } from '@ibiz/dynamic-model-api';
import { FormDetailController } from './form-detail-controller';
/**
 * 分页部件模型
 *
 * @export
 * @class FormTabPanelController
 * @extends {FormDetailController}
 */
export declare class FormTabPanelController extends FormDetailController {
    /**
     * @description 表单分页部件项模型实例对象
     * @type {IPSDEFormTabPanel}
     * @memberof FormTabPanelController
     */
    model: IPSDEFormTabPanel;
    /**
     * 被激活分页
     *
     * @type {string}
     * @memberof FormTabPanelController
     */
    activatedPage: string;
    /**
     * 选中激活状态
     *
     * @type {string}
     * @memberof FormTabPanelController
     */
    clickActiviePage: string;
    /**
     * 分页子成员
     *
     * @type {any[]}
     * @memberof FormTabPanelController
     */
    tabPages: any[];
    /**
     * Creates an instance of FormTabPanelController.
     * FormTabPanelController 实例
     *
     * @param {*} [opts={}]
     * @memberof FormTabPanelController
     */
    constructor(opts?: any);
    /**
     * 设置激活分页
     *
     * @memberof FormTabPanelController
     */
    setActiviePage(): void;
    /**
     * 选中页面
     *
     * @param {*} $event
     * @returns {void}
     * @memberof FormTabPanelController
     */
    clickPage($event: any): void;
}
