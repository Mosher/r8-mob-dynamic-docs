import { IPSDEFormButton } from '@ibiz/dynamic-model-api';
import { FormDetailController } from './form-detail-controller';
/**
 * 按钮模型
 *
 * @export
 * @class FormButtonController
 * @extends {FormDetailController}
 */
export declare class FormButtonController extends FormDetailController {
    /**
     * @description 表单按钮模型实例对象
     * @type {IPSDEFormButton}
     * @memberof FormButtonController
     */
    model: IPSDEFormButton;
    constructor(opts?: any);
    /**
     * 是否禁用
     *
     * @type {boolean}
     * @memberof FormButtonController
     */
    private $disabled;
    /**
     * 按钮对应的界面行为
     *
     * @type {*}
     * @memberof FormButtonController
     */
    uiaction: any;
    /**
     * 是否启用
     *
     * @type {boolean}
     * @memberof FormButtonController
     */
    get disabled(): boolean;
    /**
     * 设置是否启用
     *
     * @memberof FormButtonController
     */
    set disabled(val: boolean);
    /**
     * 按钮点击
     *
     * @memberof FormButtonController
     */
    buttonClick(event: any): void;
}
