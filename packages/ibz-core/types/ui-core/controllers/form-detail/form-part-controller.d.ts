import { IPSDEFormFormPart } from '@ibiz/dynamic-model-api';
import { FormDetailController } from './form-detail-controller';
/**
 * 表单部件模型
 *
 * @export
 * @class FormPartController
 * @extends {FormDetailController}
 */
export declare class FormPartController extends FormDetailController {
    /**
     * @description 表单部件模型实例对象
     * @type {IPSDEFormFormPart}
     * @memberof FormPartController
     */
    model: IPSDEFormFormPart;
    constructor(opts?: any);
}
