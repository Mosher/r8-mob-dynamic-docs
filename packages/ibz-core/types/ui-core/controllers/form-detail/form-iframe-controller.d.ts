import { IPSDEFormIFrame } from '@ibiz/dynamic-model-api';
import { FormDetailController } from './form-detail-controller';
/**
 * 嵌入成员模型
 *
 * @export
 * @class FormIFrameController
 * @extends {FormDetailController}
 */
export declare class FormIFrameController extends FormDetailController {
    /**
     * @description 表单嵌入成员模型实例对象
     * @type {IPSDEFormIFrame}
     * @memberof FormIFrameController
     */
    model: IPSDEFormIFrame;
    constructor(opts?: any);
}
