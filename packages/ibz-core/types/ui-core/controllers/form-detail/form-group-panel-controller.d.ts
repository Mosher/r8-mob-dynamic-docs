import { IPSDEFormGroupPanel } from '@ibiz/dynamic-model-api';
import { FormDetailController } from './form-detail-controller';
/**
 * 分组面板模型
 *
 * @export
 * @class FormGroupPanelController
 * @extends {FormDetailController}
 */
export declare class FormGroupPanelController extends FormDetailController {
    /**
     * @description 表单分组面板模型实例对象
     * @type {IPSDEFormGroupPanel}
     * @memberof FormGroupPanelController
     */
    model: IPSDEFormGroupPanel;
    /**
     * 实体界面行为组
     *
     * @type {*}
     * @memberof FormGroupPanelController
     */
    uiActionGroup: any;
    /**
     * 受控内容组
     *
     * @type {*}
     * @memberof FormGroupPanelController
     */
    controlledItems: any[];
    /**
     * 支持锚点集合
     *
     * @type {Array}
     * @memberof FormGroupPanelController
     */
    anchorPoints: any[];
    /**
     * 是否为管理容器
     *
     * @type {*}
     * @memberof FormGroupPanelController
     */
    isManageContainer: boolean;
    /**
     * 管理容器状态 true显示 false隐藏
     *
     * @type {*}
     * @memberof FormGroupPanelController
     */
    manageContainerStatus: boolean;
    /**
     * Creates an instance of FormGroupPanelController.
     * 创建 FormGroupPanelController 实例
     *
     * @param {*} [opts={}]
     * @memberof FormGroupPanelController
     */
    constructor(opts?: any);
    /**
     * 设置管理容器状态
     *
     * @param {boolean} state
     * @memberof FormGroupPanelController
     */
    setManageContainerStatus(state: boolean): void;
    /**
     * 处理表单分组界面行为点击
     *
     * @param event
     * @param item
     * @memberof FormGroupPanelController
     */
    onGroupUIActionClick(event: any, item: any): void;
}
