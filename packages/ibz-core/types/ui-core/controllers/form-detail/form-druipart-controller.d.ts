import { IPSAppDEView, IPSDEFormDRUIPart } from '@ibiz/dynamic-model-api';
import { IParam } from '../../../interface';
import { FormDetailController } from './form-detail-controller';
/**
 * 数据关系界面模型
 *
 * @export
 * @class FormDruipartController
 * @extends {FormDetailController}
 */
export declare class FormDruipartController extends FormDetailController {
    /**
     * @description 表单关系界面实例对象
     * @type {IPSDEFormDRUIPart}
     * @memberof FormDruipartController
     */
    model: IPSDEFormDRUIPart;
    /**
     * 临时数据模式：从数据模式:"2"、主数据模式:"1"、无临时数据模式:"0"
     *
     * @type {number}
     * @memberof FormDruipartController
     */
    tempMode: number;
    /**
     * 自定义样式
     *
     * @type {IParam}
     * @memberof FormDruipartController
     */
    customStyle: IParam;
    /**
     * 父数据
     *
     * @type {IParam}
     * @memberof FormDruipartController
     */
    parentdata: IParam;
    /**
     * 内嵌视图
     *
     * @type {IPSAppDEView}
     * @memberof FormDruipartController
     */
    embeddedView: IPSAppDEView;
    /**
     * 附加界面刷新项
     *
     * @type {IPSAppDEView}
     * @memberof FormDruipartController
     */
    refreshitems: string;
    /**
     * 局部导航上下文
     *
     * @type {IParam}
     * @memberof FormDruipartController
     */
    localContext: IParam;
    /**
     * 局部导航参数
     *
     * @type {IParam}
     * @memberof FormDruipartController
     */
    localParam: IParam;
    /**
     * 传入参数项名称
     *
     * @type {string}
     * @memberof FormDruipartController
     */
    paramItem: string;
    /**
     * 视图参数
     *
     * @type {any[]}
     * @memberof FormDruipartController
     */
    parameters: any[];
    /**
     * Creates an instance of FormDruipartController.
     *
     * @param {*} [opts={}]
     * @memberof FormDruipartController
     */
    constructor(opts?: any);
    /**
     * 初始化模型数据
     *
     * @public
     * @memberof FormDruipartController
     */
    initModelData(): void;
    /**
     * 初始化输入数据
     *
     * @public
     * @memberof FormDruipartController
     */
    initInputData(opts: IParam): void;
    /**
     * 关系界面保存完成
     *
     * @public
     * @memberof FormDruipartController
     */
    embedViewDataSaved(): void;
}
