import { IPSDEFormTabPage } from '@ibiz/dynamic-model-api';
import { FormDetailController } from './form-detail-controller';
import { FormTabPanelController } from './form-tab-panel-controller';
/**
 * 分页面板模型
 *
 * @export
 * @class FormTabPageController
 * @extends {FormDetailController}
 */
export declare class FormTabPageController extends FormDetailController {
    /**
     * @description 表单分页面板项模型实例对象
     * @type {IPSDEFormTabPage}
     * @memberof FormTabPageController
     */
    model: IPSDEFormTabPage;
    /**
     * Creates an instance of FormTabPageController.
     * FormTabPageController 实例
     *
     * @param {*} [opts={}]
     * @memberof FormTabPageController
     */
    constructor(opts?: any);
    /**
     * 设置分页是否启用
     *
     * @param {boolean} state
     * @memberof FormTabPageController
     */
    setVisible(state: boolean): void;
    /**
     * 获取分页面板
     *
     * @returns {(FormTabPanelController | null)}
     * @memberof FormTabPageController
     */
    getTabPanelModel(): FormTabPanelController | null;
}
