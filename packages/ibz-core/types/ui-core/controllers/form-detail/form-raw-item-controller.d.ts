import { IPSDEFormRawItem } from '@ibiz/dynamic-model-api';
import { FormDetailController } from './form-detail-controller';
/**
 * 直接内容模型
 *
 * @export
 * @class FormRawItemController
 * @extends {FormDetailController}
 */
export declare class FormRawItemController extends FormDetailController {
    /**
     * @description 表单直接内容项模型实例对象
     * @type {IPSDEFormRawItem}
     * @memberof FormRawItemController
     */
    model: IPSDEFormRawItem;
    constructor(opts?: any);
}
