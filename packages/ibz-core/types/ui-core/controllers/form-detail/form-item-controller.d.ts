import { IPSDEFormItem } from '@ibiz/dynamic-model-api';
import { FormDetailController } from './form-detail-controller';
/**
 * 表单项模型
 *
 * @export
 * @class FormItemController
 * @extends {FormDetailController}
 */
export declare class FormItemController extends FormDetailController {
    /**
     * @description 表单项实例对象
     * @type {IPSDEFormItem}
     * @memberof FormItemController
     */
    model: IPSDEFormItem;
    /**
     * 是否启用
     *
     * @type {boolean}
     * @memberof FormItemController
     */
    disabled: boolean;
    /**
     * 错误信息
     *
     * @type {string}
     * @memberof FormItemController
     */
    error: string;
    /**
     * 表单项启用条件
     *
     * 0 不启用
     * 1 新建
     * 2 更新
     * 3 全部启用
     *
     * @type {(number | 0 | 1 | 2 | 3)}
     * @memberof FormItemController
     */
    enableCond: number | 0 | 1 | 2 | 3;
    /**
     * 是否必填
     *
     * @type {boolean}
     * @memberof FormItemController
     */
    required: boolean;
    /**
     * @description 是否为信息模式
     * @protected
     * @type {boolean}
     * @memberof FormItemController
     */
    protected infoMode: boolean;
    /**
     * Creates an instance of FormItemController.
     * FormItemController 实例
     *
     * @param {*} [opts={}]
     * @memberof FormItemController
     */
    constructor(opts?: any);
    /**
     * 设置是否启用
     *
     * @param {boolean} state
     * @memberof FormItemController
     */
    setDisabled(state: boolean): void;
    /**
     * 设置信息内容
     *
     * @param {string} error
     * @memberof FormItemController
     */
    setError(error: string): void;
    /**
     * 设置是否启用
     *
     * @param {string} srfuf
     * @memberof FormItemController
     */
    setEnableCond(srfuf: string): void;
    /**
     * @description 设置是否为信息模式
     * @param {boolean} mode
     * @memberof FormItemController
     */
    setInfoMode(mode: boolean): void;
    /**
     * @description 获取是否为信息模式
     * @return {*}  {boolean}
     * @memberof FormItemController
     */
    getInfoMode(): boolean;
}
