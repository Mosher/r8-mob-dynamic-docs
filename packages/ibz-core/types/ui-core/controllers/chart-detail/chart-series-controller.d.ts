import { ChartDataSetFieldController } from './chart-dataset-field-controller';
/**
 * 图表序列模型
 *
 * @export
 * @class ChartSeriesController
 */
export declare class ChartSeriesController {
    /**
     * 序列类型
     *
     * @type {string}
     * @memberof ChartSeriesController
     */
    type: string;
    /**
     * 图表对象
     *
     * @type {*}
     * @memberof ChartSeriesController
     */
    chart: any;
    /**
     * 序列名称
     *
     * @type {string}
     * @memberof ChartSeriesController
     */
    name: string;
    /**
     * 序列标题
     *
     * @type {string}
     * @memberof ChartSeriesController
     */
    caption: string;
    /**
     * 序列标识属性
     *
     * @type {string}
     * @memberof ChartSeriesController
     */
    seriesIdField: string;
    /**
     * 序列名称属性
     *
     * @type {string}
     * @memberof ChartSeriesController
     */
    seriesNameField: string;
    /**
     * 序列标识名称键值对
     *
     * @type {any}
     * @memberof ChartSeriesController
     */
    seriesMap: any;
    /**
     * 对象索引
     *
     * @type {number}
     * @memberof ChartSeriesController
     */
    index: number;
    /**
     * 序列映射数据
     *
     * @type {*}
     * @memberof ChartSeriesController
     */
    dataSet: any;
    /**
     * 预置配置
     *
     * @type {number}
     * @memberof ChartSeriesController
     */
    ecxObject: any;
    /**
     * 用户自定义配置
     *
     * @type {number}
     * @memberof ChartSeriesController
     */
    ecObject: any;
    /**
     * 序列索引
     *
     * @type {number}
     * @memberof ChartSeriesController
     */
    seriesIndex: number;
    /**
     * 数据集属性集合
     *
     * @type {Array<ChartDataSetFieldController>}
     * @memberof ChartSeriesController
     */
    dataSetFields: Array<ChartDataSetFieldController>;
    /**
     * 取值方式
     * 'column'：默认，dataset 的列对应于系列，从而 dataset 中每一列是一个维度（dimension）
     * 'row'：dataset 的行对应于系列，从而 dataset 中每一行是一个维度（dimension
     *
     * @type {string}
     * @memberof ChartSeriesController
     */
    seriesLayoutBy: string;
    /**
     * 序列代码表
     *
     * @type {*}
     * @memberof ChartSeriesController
     */
    seriesCodeList: any;
    /**
     * 序列模板
     *
     * @type {*}
     * @memberof ChartSeriesController
     */
    seriesTemp: any;
    /**
     * Creates an instance of ChartSeriesController.
     * ChartSeriesController 实例
     *
     * @param {*} [opts={}]
     * @memberof ChartSeriesController
     */
    constructor(opts?: any);
    /**
     * 设置序列类型
     *
     * @param {string} state
     * @memberof ChartSeriesController
     */
    setType(state: string): void;
    /**
     * 设置序列名称
     *
     * @param {string} state
     * @memberof ChartSeriesController
     */
    setCaption(state: string): void;
    /**
     * 设置序列标题
     *
     * @param {string} state
     * @memberof ChartSeriesController
     */
    setName(state: string): void;
    /**
     * 设置数据集
     *
     * @param {*} state
     * @memberof ChartSeriesController
     */
    setDataSet(state: any): void;
    /**
     * 设置序列标识名称键值对
     *
     * @param {*} state
     * @memberof ChartSeriesController
     */
    setSeriesMap(state: any): void;
    /**
     * 设置序列映射的图表
     *
     * @param {*} state
     * @memberof ChartSeriesController
     */
    setChart(state: any): void;
    /**
     * 设置序列标识属性
     *
     * @param {*} state
     * @memberof ChartSeriesController
     */
    setSeriesIdField(state: any): void;
    /**
     * 设置序列名称属性
     *
     * @param {*} state
     * @memberof ChartSeriesController
     */
    setSeriesNameField(state: any): void;
    /**
     * 设置对象索引
     *
     * @param {number} state
     * @memberof ChartSeriesController
     */
    setIndex(state: number): void;
    /**
     * 设置预置配置
     *
     * @param {*} state
     * @memberof ChartSeriesController
     */
    setECXObject(state: any): void;
    /**
     * 设置用户自定义参数
     *
     * @param {*} state
     * @memberof ChartSeriesController
     */
    setECObject(state: any): void;
    /**
     * 设置取值方式
     *
     * @param {string} state
     * @memberof ChartSeriesController
     */
    setSeriesLayoutBy(state: string): void;
    /**
     * 设置数据集属性集合
     *
     * @param {Array<ChartDataSetFieldController>} state
     * @memberof ChartSeriesController
     */
    setDataSetFields(state: Array<ChartDataSetFieldController>): void;
    /**
     * 设置序列索引
     *
     * @param {number} state
     * @memberof ChartSeriesController
     */
    setSeriesIndex(state: number): void;
    /**
     * 设置序列代码表
     *
     * @param {any} state
     * @memberof ChartSeriesController
     */
    setSeriesCodeList(state: any): void;
    /**
     * 设置序列模板
     *
     * @param {*} state
     * @memberof ChartSeriesController
     */
    setSeriesTemp(state: any): void;
}
