import { ChartSeriesController } from './chart-series-controller';
/**
 * 折线图序列模型
 *
 * @export
 * @class ChartLineSeriesController
 */
export declare class ChartLineSeriesController extends ChartSeriesController {
    /**
     * 分类属性
     *
     * @type {string}
     * @memberof ChartLineSeriesController
     */
    categorField: string;
    /**
     * 值属性
     *
     * @type {string}
     * @memberof ChartLineSeriesController
     */
    valueField: string;
    /**
     * 分类代码表
     *
     * @type {string}
     * @memberof ChartLineSeriesController
     */
    categorCodeList: any;
    /**
     * 维度定义
     *
     * @type {string}
     * @memberof ChartLineSeriesController
     */
    dimensions: Array<string>;
    /**
     * 维度编码
     *
     * @type {*}
     * @memberof ChartLineSeriesController
     */
    encode: any;
    /**
     * Creates an instance of ChartLineSeriesController.
     * ChartLineSeriesController 实例
     *
     * @param {*} [opts={}]
     * @memberof ChartLineSeriesController
     */
    constructor(opts?: any);
    /**
     * 设置分类属性
     *
     * @param {string} state
     * @memberof ChartLineSeriesController
     */
    setCategorField(state: string): void;
    /**
     * 设置序列名称
     *
     * @param {string} state
     * @memberof ChartLineSeriesController
     */
    setValueField(state: string): void;
    /**
     * 分类代码表
     *
     * @param {*} state
     * @memberof ChartLineSeriesController
     */
    setCategorCodeList(state: any): void;
    /**
     * 维度定义
     *
     * @param {*} state
     * @memberof ChartLineSeriesController
     */
    setDimensions(state: any): void;
    /**
     * 设置编码
     *
     * @param {*} state
     * @memberof ChartLineSeriesController
     */
    setEncode(state: any): void;
}
