import { ChartSeriesController } from './chart-series-controller';
/**
 * 漏斗图序列模型
 *
 * @export
 * @class ChartFunnelSeriesController
 */
export declare class ChartFunnelSeriesController extends ChartSeriesController {
    /**
     * 分类属性
     *
     * @type {string}
     * @memberof ChartFunnelSeriesController
     */
    categorField: string;
    /**
     * 值属性
     *
     * @type {string}
     * @memberof ChartFunnelSeriesController
     */
    valueField: string;
    /**
     * 分类代码表
     *
     * @type {string}
     * @memberof ChartFunnelSeriesController
     */
    categorCodeList: any;
    /**
     * 维度定义
     *
     * @type {string}
     * @memberof ChartFunnelSeriesController
     */
    dimensions: Array<string>;
    /**
     * 维度编码
     *
     * @type {*}
     * @memberof ChartFunnelSeriesController
     */
    encode: any;
    /**
     * Creates an instance of ChartFunnelSeriesController.
     * ChartFunnelSeriesController 实例
     *
     * @param {*} [opts={}]
     * @memberof ChartFunnelSeriesController
     */
    constructor(opts?: any);
    /**
     * 设置分类属性
     *
     * @param {string} state
     * @memberof ChartFunnelSeriesController
     */
    setCategorField(state: string): void;
    /**
     * 设置序列名称
     *
     * @param {string} state
     * @memberof ChartFunnelSeriesController
     */
    setValueField(state: string): void;
    /**
     * 分类代码表
     *
     * @param {*} state
     * @memberof ChartFunnelSeriesController
     */
    setCategorCodeList(state: any): void;
    /**
     * 维度定义
     *
     * @param {*} state
     * @memberof ChartFunnelSeriesController
     */
    setDimensions(state: any): void;
    /**
     * 设置编码
     *
     * @param {*} state
     * @memberof ChartFunnelSeriesController
     */
    setEncode(state: any): void;
}
