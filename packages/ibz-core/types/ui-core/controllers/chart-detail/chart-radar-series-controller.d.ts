import { ChartSeriesController } from './chart-series-controller';
/**
 * 雷达图序列模型
 *
 * @export
 * @class ChartRadarSeriesController
 */
export declare class ChartRadarSeriesController extends ChartSeriesController {
    /**
     * 分类属性
     *
     * @type {string}
     * @memberof ChartRadarSeriesController
     */
    categorField: string;
    /**
     * 值属性
     *
     * @type {string}
     * @memberof ChartRadarSeriesController
     */
    valueField: string;
    /**
     * 雷达图指示器
     *
     * @type {string}
     * @memberof ChartRadarSeriesController
     */
    indicator: Array<any>;
    /**
     * 分类代码表
     *
     * @type {string}
     * @memberof ChartRadarSeriesController
     */
    categorCodeList: any;
    /**
     * 维度编码
     *
     * @type {*}
     * @memberof ChartRadarSeriesController
     */
    encode: any;
    /**
     * Creates an instance of ChartRadarSeriesController.
     * ChartRadarSeriesController 实例
     *
     * @param {*} [opts={}]
     * @memberof ChartRadarSeriesController
     */
    constructor(opts?: any);
    /**
     * 设置分类属性
     *
     * @param {string} state
     * @memberof ChartRadarSeriesController
     */
    setCategorField(state: string): void;
    /**
     * 设置序列名称
     *
     * @param {string} state
     * @memberof ChartRadarSeriesController
     */
    setValueField(state: string): void;
    /**
     * 分类代码表
     *
     * @param {*} state
     * @memberof ChartRadarSeriesController
     */
    setCategorCodeList(state: any): void;
    /**
     * 设置编码
     *
     * @param {*} state
     * @memberof ChartRadarSeriesController
     */
    setEncode(state: any): void;
    /**
     * 设置雷达图指示器
     *
     * @param {*} state
     * @memberof ChartRadarSeriesController
     */
    setIndicator(state: any): void;
}
