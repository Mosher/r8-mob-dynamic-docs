/**
 * 图表数据集属性模型
 *
 * @export
 * @class ChartDataSetFieldController
 */
export declare class ChartDataSetFieldController {
    /**
     * 图表属性名称
     *
     * @type {string}
     * @memberof ChartDataSetFieldController
     */
    name: string;
    /**
     * 图表属性代码表
     *
     * @type {*}
     * @memberof ChartDataSetFieldController
     */
    codelist: any;
    /**
     * 是否分组属性
     *
     * @type {boolean}
     * @memberof ChartDataSetFieldController
     */
    isGroupField: boolean;
    /**
     * 分组模式
     *
     * @type {string}
     * @memberof ChartDataSetFieldController
     */
    groupMode: string;
    /**
     * Creates an instance of ChartDataSetFieldController.
     * ChartDataSetFieldController 实例
     *
     * @param {*} [opts={}]
     * @memberof ChartDataSetFieldController
     */
    constructor(opts?: any);
    /**
     * 设置图表属性名称
     *
     * @param {string} state
     * @memberof ChartDataSetFieldController
     */
    setName(state: string): void;
    /**
     * 设置图表属性代码表
     *
     * @param {*} state
     * @memberof ChartDataSetFieldController
     */
    setCodeList(state: any): void;
    /**
     * 设置分组属性
     *
     * @param {boolean} state
     * @memberof ChartDataSetFieldController
     */
    setisGroupField(state: boolean): void;
    /**
     * 设置属性分组模式
     *
     * @param {string} state
     * @memberof ChartDataSetFieldController
     */
    setGroupMode(state: string): void;
}
