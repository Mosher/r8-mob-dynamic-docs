import { ChartSeriesController } from './chart-series-controller';
/**
 * k线图序列模型
 *
 * @export
 * @class ChartCandlestickSeriesController
 */
export declare class ChartCandlestickSeriesController extends ChartSeriesController {
    /**
     * 分类属性
     *
     * @type {string}
     * @memberof ChartCandlestickSeriesController
     */
    categorField: string;
    /**
     * 值属性
     *
     * @type {string}
     * @memberof ChartCandlestickSeriesController
     */
    valueField: string;
    /**
     * 分类代码表
     *
     * @type {string}
     * @memberof ChartCandlestickSeriesController
     */
    categorCodeList: any;
    /**
     * 维度定义
     *
     * @type {string}
     * @memberof ChartCandlestickSeriesController
     */
    dimensions: Array<string>;
    /**
     * 维度编码
     *
     * @type {*}
     * @memberof ChartCandlestickSeriesController
     */
    encode: any;
    /**
     * 序列模板
     *
     * @type {*}
     * @memberof ChartCandlestickSeriesController
     */
    seriesTemp: any;
    /**
     * Creates an instance of ChartCandlestickSeriesController.
     * ChartCandlestickSeriesController 实例
     *
     * @param {*} [opts={}]
     * @memberof ChartCandlestickSeriesController
     */
    constructor(opts?: any);
    /**
     * 设置分类属性
     *
     * @param {string} state
     * @memberof ChartCandlestickSeriesController
     */
    setCategorField(state: string): void;
    /**
     * 设置序列名称
     *
     * @param {string} state
     * @memberof ChartCandlestickSeriesController
     */
    setValueField(state: string): void;
    /**
     * 分类代码表
     *
     * @param {*} state
     * @memberof ChartCandlestickSeriesController
     */
    setCategorCodeList(state: any): void;
    /**
     * 维度定义
     *
     * @param {*} state
     * @memberof ChartCandlestickSeriesController
     */
    setDimensions(state: any): void;
    /**
     * 设置编码
     *
     * @param {*} state
     * @memberof ChartCandlestickSeriesController
     */
    setEncode(state: any): void;
    /**
     * 设置序列模板
     *
     * @param {*} state
     * @memberof ChartCandlestickSeriesController
     */
    setSeriesTemp(state: any): void;
}
