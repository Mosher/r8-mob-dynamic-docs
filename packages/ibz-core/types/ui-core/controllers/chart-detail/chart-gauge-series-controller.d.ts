import { ChartSeriesController } from './chart-series-controller';
/**
 * 仪表盘序列模型
 *
 * @export
 * @class ChartGaugeSeriesController
 */
export declare class ChartGaugeSeriesController extends ChartSeriesController {
    /**
     * 分类属性
     *
     * @type {string}
     * @memberof ChartGaugeSeriesController
     */
    categorField: string;
    /**
     * 值属性
     *
     * @type {string}
     * @memberof ChartGaugeSeriesController
     */
    valueField: string;
    /**
     * 分类代码表
     *
     * @type {string}
     * @memberof ChartGaugeSeriesController
     */
    categorCodeList: any;
    /**
     * 维度定义
     *
     * @type {string}
     * @memberof ChartGaugeSeriesController
     */
    dimensions: Array<string>;
    /**
     * 维度编码
     *
     * @type {*}
     * @memberof ChartGaugeSeriesController
     */
    encode: any;
    /**
     * 序列模板
     *
     * @type {*}
     * @memberof ChartGaugeSeriesController
     */
    seriesTemp: any;
    /**
     * Creates an instance of ChartGaugeSeriesController.
     * ChartGaugeSeriesController 实例
     *
     * @param {*} [opts={}]
     * @memberof ChartGaugeSeriesController
     */
    constructor(opts?: any);
    /**
     * 设置分类属性
     *
     * @param {string} state
     * @memberof ChartGaugeSeriesController
     */
    setCategorField(state: string): void;
    /**
     * 设置序列名称
     *
     * @param {string} state
     * @memberof ChartGaugeSeriesController
     */
    setValueField(state: string): void;
    /**
     * 分类代码表
     *
     * @param {*} state
     * @memberof ChartGaugeSeriesController
     */
    setCategorCodeList(state: any): void;
    /**
     * 维度定义
     *
     * @param {*} state
     * @memberof ChartGaugeSeriesController
     */
    setDimensions(state: any): void;
    /**
     * 设置编码
     *
     * @param {*} state
     * @memberof ChartGaugeSeriesController
     */
    setEncode(state: any): void;
    /**
     * 设置序列模板
     *
     * @param {*} state
     * @memberof ChartGaugeSeriesController
     */
    setSeriesTemp(state: any): void;
}
