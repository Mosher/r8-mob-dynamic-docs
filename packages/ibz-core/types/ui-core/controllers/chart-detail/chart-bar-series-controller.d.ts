import { ChartSeriesController } from './chart-series-controller';
/**
 * 柱状图序列模型
 *
 * @export
 * @class ChartBarSeriesController
 */
export declare class ChartBarSeriesController extends ChartSeriesController {
    /**
     * 分类属性
     *
     * @type {string}
     * @memberof ChartBarSeriesController
     */
    categorField: string;
    /**
     * 值属性
     *
     * @type {string}
     * @memberof ChartBarSeriesController
     */
    valueField: string;
    /**
     * 分类代码表
     *
     * @type {string}
     * @memberof ChartBarSeriesController
     */
    categorCodeList: any;
    /**
     * 维度定义
     *
     * @type {string}
     * @memberof ChartBarSeriesController
     */
    dimensions: Array<string>;
    /**
     * 维度编码
     *
     * @type {*}
     * @memberof ChartBarSeriesController
     */
    encode: any;
    /**
     * 序列模板
     *
     * @type {*}
     * @memberof ChartBarSeriesController
     */
    seriesTemp: any;
    /**
     * Creates an instance of ChartBarSeriesController.
     * ChartBarSeriesController 实例
     *
     * @param {*} [opts={}]
     * @memberof ChartBarSeriesController
     */
    constructor(opts?: any);
    /**
     * 设置分类属性
     *
     * @param {string} state
     * @memberof ChartBarSeriesController
     */
    setCategorField(state: string): void;
    /**
     * 设置序列名称
     *
     * @param {string} state
     * @memberof ChartBarSeriesController
     */
    setValueField(state: string): void;
    /**
     * 分类代码表
     *
     * @param {*} state
     * @memberof ChartBarSeriesController
     */
    setCategorCodeList(state: any): void;
    /**
     * 维度定义
     *
     * @param {*} state
     * @memberof ChartBarSeriesController
     */
    setDimensions(state: any): void;
    /**
     * 设置编码
     *
     * @param {*} state
     * @memberof ChartBarSeriesController
     */
    setEncode(state: any): void;
    /**
     * 设置序列模板
     *
     * @param {*} state
     * @memberof ChartBarSeriesController
     */
    setSeriesTemp(state: any): void;
}
