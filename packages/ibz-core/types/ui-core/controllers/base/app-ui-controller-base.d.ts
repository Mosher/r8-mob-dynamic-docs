import { Subject, Subscription } from 'rxjs';
/**
 * @description 控制器基类
 * @export
 * @class AppUIControllerBase
 */
export declare class AppUIControllerBase {
    /**
     * @description 自定义事件传递对象
     * @type {Subject<{ eventName: string, args: any }>}
     * @memberof AppUIControllerBase
     */
    customEventState: Subject<{
        eventName: string;
        args: any;
    }>;
    /**
     * @description 自定义事件订阅对象
     * @type {(Subscription | undefined)}
     * @memberof AppUIControllerBase
     */
    customStateEvent: Subscription | undefined;
    /**
     * @description 自定义事件Map
     * @type {Map<string, Function>}
     * @memberof AppUIControllerBase
     */
    customEventMap: Map<string, Function>;
    /**
     * Creates an instance of AppUIControllerBase.
     * @param {*} opts
     * @memberof AppUIControllerBase
     */
    constructor(opts: any);
    /**
     * @description 挂载事件
     * @param {string} eventName
     * @param {Function} callback
     * @memberof AppUIControllerBase
     */
    on(eventName: string, callback: Function): void;
    /**
     * @description 触发事件
     * @param {string} eventName
     * @param {Function} callback
     * @memberof AppUIControllerBase
     */
    emit(eventName: string, args: any): void;
    /**
     * @description 执行回调
     * @memberof AppUIControllerBase
     */
    executeCallBack(): void;
    /**
     * @description 取消订阅
     * @memberof AppUIControllerBase
     */
    cancelSubscribe(): void;
}
