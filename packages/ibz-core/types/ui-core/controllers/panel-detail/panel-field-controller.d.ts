import { PanelDetailModelController } from './panel-detail-controller';
/**
 * 面板属性项模型
 *
 * @export
 * @class PanelFieldModelController
 * @extends {PanelDetailModelController}
 */
export declare class PanelFieldModelController extends PanelDetailModelController {
    /**
     * 是否启用
     *
     * @type {boolean}
     * @memberof PanelFieldModelController
     */
    disabled: boolean;
    /**
     * 错误信息
     *
     * @type {string}
     * @memberof PanelFieldModelController
     */
    error: string;
    /**
     * 表单项启用条件
     *
     * 0 不启用
     * 1 新建
     * 2 更新
     * 3 全部启用
     *
     * @type {(number | 0 | 1 | 2 | 3)}
     * @memberof PanelFieldModelController
     */
    enableCond: number | 0 | 1 | 2 | 3;
    /**
     * Creates an instance of PanelFieldModelController.
     * PanelFieldModelController 实例
     *
     * @param {*} [opts={}]
     * @memberof PanelFieldModelController
     */
    constructor(opts?: any);
    /**
     * 设置是否启用
     *
     * @param {boolean} state
     * @memberof PanelFieldModelController
     */
    setDisabled(state: boolean): void;
    /**
     * 设置信息内容
     *
     * @param {string} error
     * @memberof PanelFieldModelController
     */
    setError(error: string): void;
    /**
     * 设置是否启用
     *
     * @param {string} srfuf
     * @memberof PanelFieldModelController
     */
    setEnableCond(srfuf: string): void;
}
