/**
 * 表单成员模型
 *
 * @export
 * @class PanelDetailModelController
 */
export declare class PanelDetailModelController {
    /**
     * 是否有权限
     *
     * @type {boolean}
     * @memberof PanelDetailModelController
     */
    isPower: boolean;
    /**
     * 成员标题
     *
     * @type {string}
     * @memberof PanelDetailModelController
     */
    caption: string;
    /**
     * 成员类型
     *
     * @type {string}
     * @memberof PanelDetailModelController
     */
    itemType: string;
    /**
     * 面板对象
     *
     * @type {*}
     * @memberof PanelDetailModelController
     */
    panel: any;
    /**
     * 成员名称
     *
     * @type {string}
     * @memberof PanelDetailModelController
     */
    name: string;
    /**
     * 成员是否显示
     *
     * @type {boolean}
     * @memberof PanelDetailModelController
     */
    $visible: boolean;
    /**
     * 成员是否显示(旧)
     *
     * @type {boolean}
     * @memberof PanelDetailModelController
     */
    oldVisible: boolean;
    /**
     * 成员是否显示标题
     *
     * @type {boolean}
     * @memberof PanelDetailModelController
     */
    isShowCaption: boolean;
    /**
     * Creates an instance of PanelDetailModelController.
     * PanelDetailModelController 实例
     *
     * @param {*} [opts={}]
     * @memberof PanelDetailModelController
     */
    constructor(opts?: any);
    /**
     * 设置成员是否隐藏
     *
     * @memberof PanelDetailModelController
     */
    set visible(val: boolean);
    /**
     * 获取成员是否隐藏
     *
     * @memberof PanelDetailModelController
     */
    get visible(): boolean;
    /**
     * 设置显示与隐藏
     *
     * @param {boolean} state
     * @memberof PanelDetailModelController
     */
    setVisible(state: boolean): void;
    /**
     * 设置显示标题栏
     *
     * @param {boolean} state
     * @memberof PanelDetailModelController
     */
    setShowCaption(state: boolean): void;
}
