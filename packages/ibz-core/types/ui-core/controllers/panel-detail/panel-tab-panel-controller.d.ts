import { PanelDetailModelController } from './panel-detail-controller';
/**
 * 分页部件模型
 *
 * @export
 * @class PanelTabPanelModelController
 * @extends {PanelDetailModelController}
 */
export declare class PanelTabPanelModelController extends PanelDetailModelController {
    /**
     * 被激活分页
     *
     * @type {string}
     * @memberof PanelTabPanelModelController
     */
    activatedPage: string;
    /**
     * 选中激活状态
     *
     * @type {string}
     * @memberof PanelTabPanelModelController
     */
    clickActiviePage: string;
    /**
     * 分页子成员
     *
     * @type {any[]}
     * @memberof PanelTabPanelModelController
     */
    tabPages: any[];
    /**
     * Creates an instance of PanelTabPanelModelController.
     * PanelTabPanelModelController 实例
     *
     * @param {*} [opts={}]
     * @memberof PanelTabPanelModelController
     */
    constructor(opts?: any);
    /**
     * 设置激活分页
     *
     * @memberof PanelTabPanelModelController
     */
    setActiviePage(): void;
    /**
     * 选中页面
     *
     * @param {*} $event
     * @returns {void}
     * @memberof PanelTabPanelModelController
     */
    clickPage($event: any): void;
}
