import { PanelDetailModelController } from './panel-detail-controller';
/**
 * 用户控件模型
 *
 * @export
 * @class PanelControlModel
 * @extends {PanelDetailModelController}
 */
export declare class PanelControlModelController extends PanelDetailModelController {
    constructor(otps?: any);
}
