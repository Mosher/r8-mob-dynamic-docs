import { PanelDetailModelController } from './panel-detail-controller';
/**
 * 按钮模型
 *
 * @export
 * @class PanelButtonModelController
 * @extends {PanelDetailModelController}
 */
export declare class PanelButtonModelController extends PanelDetailModelController {
    constructor(opts?: any);
    /**
     * 是否禁用
     *
     * @type {boolean}
     * @memberof PanelButtonModelController
     */
    private $disabled;
    /**
     * 按钮对应的界面行为
     *
     * @type {*}
     * @memberof PanelButtonModelController
     */
    uiaction: any;
    /**
     * 是否启用
     *
     * @type {boolean}
     * @memberof PanelButtonModelController
     */
    get disabled(): boolean;
    /**
     * 设置是否启用
     *
     * @memberof PanelButtonModelController
     */
    set disabled(val: boolean);
}
