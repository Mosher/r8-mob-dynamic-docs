import { PanelDetailModelController } from './panel-detail-controller';
import { PanelTabPanelModelController } from './panel-tab-panel-controller';
/**
 * 分页面板模型
 *
 * @export
 * @class PanelTabPageModelController
 * @extends {PanelDetailModel}
 */
export declare class PanelTabPageModelController extends PanelDetailModelController {
    /**
     * Creates an instance of PanelTabPageModelController.
     * PanelTabPageModelController 实例
     *
     * @param {*} [opts={}]
     * @memberof PanelTabPageModelController
     */
    constructor(opts?: any);
    /**
     * 设置分页是否启用
     *
     * @param {boolean} state
     * @memberof PanelTabPageModelController
     */
    setVisible(state: boolean): void;
    /**
     * 获取分页面板
     *
     * @returns {(PanelTabPanelModelController | null)}
     * @memberof PanelTabPageModelController
     */
    getTabPanelModel(): PanelTabPanelModelController | null;
}
