import { IPSDEWizardPanel } from '@ibiz/dynamic-model-api';
export declare class AppMobWizardPanelModel {
    /**
     * 向导面板模型实例
     *
     * @memberof AppMobWizardPanelModel
     */
    controlInstance: IPSDEWizardPanel;
    /**
     * Creates an instance of AppMobWizardPanelModel.
     *
     * @param {*} [opts={}]
     * @memberof AppMobWizardPanelModel
     */
    constructor(opts: any);
    /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof AppMobWizardPanelModel
    */
    getDataItems(): any[];
}
