export declare class AppMobMDCtrlModel {
    /**
     * 多数据部件实例对象
     *
     * @type {IPSDEMobMDCtrl}
     * @memberof AppMobMDCtrlModel
     */
    private MDCtrlInstance;
    /**
     * 构造多数据部件模型
     *
     * @memberof AppMobMDCtrlModel
     */
    constructor(opts?: any);
    /**
     * 获取数据项集合
     *
     * @memberof AppMobMDCtrlModel
     */
    getDataItems(): any[];
}
