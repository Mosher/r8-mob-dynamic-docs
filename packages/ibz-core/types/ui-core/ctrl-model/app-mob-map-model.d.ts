import { IPSSysMap } from '@ibiz/dynamic-model-api';
/**
 * AppMobMapModel 部件模型
 *
 * @export
 * @class AppMobMapModel
 */
export declare class AppMobMapModel {
    /**
     * 地图实例对象
     *
     * @memberof AppMobMapModel
     */
    mapInstance: IPSSysMap;
    /**
     * 地图项类型
     *
     * @returns {string}
     * @memberof AppGridModel
     */
    itemType: string;
    /**
     * Creates an instance of AppMobMapModel.
     *
     * @param {*} [opts={}]
     * @memberof AppMobMapModel
     */
    constructor(opts: any);
    /**
     * 获取数据项集合
     *
     * @returns {any[]}
     * @memberof AppMobMapModel
     */
    getDataItems(): any[];
}
