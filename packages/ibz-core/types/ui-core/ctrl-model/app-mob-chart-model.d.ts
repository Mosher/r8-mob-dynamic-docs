import { IPSDEChart } from "@ibiz/dynamic-model-api";
export declare class AppMobChartModel {
    /**
     * @description 图表实例对象
     * @memberof AppMobChartModel
     */
    chartInstance: IPSDEChart;
    /**
     * Creates an instance of AppMobChartModel.
     *
     * @param {*} [opts={}]
     * @memberof AppMobChartModel
     */
    constructor(opts: any);
    /**
     * @description 获取数据项集合
     * @returns {any[]}
     * @memberof AppMobChartModel
     */
    getDataItems(): any[];
}
