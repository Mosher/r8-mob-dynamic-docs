import { IPSDECalendar } from '@ibiz/dynamic-model-api';
export declare class AppMobCalendarModel {
    /**
     * 日历实例对象
     *
     * @memberof AppMobCalendarModel
     */
    calendarInstance: IPSDECalendar;
    /**
     * 日历项类型
     *
     * @returns {string}
     * @memberof AppMobCalendarModel
     */
    itemType: string;
    /**
     * Creates an instance of AppMobCalendarModel.
     *
     * @param {*} [opts={}]
     * @memberof AppGridModel
     */
    constructor(opts: any);
    getDataItems(): any[];
}
