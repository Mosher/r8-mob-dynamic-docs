import { IPSDEMultiEditViewPanel } from '@ibiz/dynamic-model-api';
/**
 * AppMobMEditViewPanelModel 部件模型
 *
 * @export
 * @class AppMobMEditViewPanelModel
 */
export declare class AppMobMEditViewPanelModel {
    /**
     * 多编辑视图实例对象
     *
     * @memberof AppMobMEditViewPanelModel
     */
    meditViewPanelInstance: IPSDEMultiEditViewPanel;
    /**
     * Creates an instance of AppMobMEditViewPanelModel.
     *
     * @param {*} [opts={}]
     * @memberof AppMobMEditViewPanelModel
     */
    constructor(opts: any);
    /**
     * 获取数据项集合
     *
     * @returns {any[]}
     * @memberof AppMobMEditViewPanelModel
     */
    getDataItems(): any[];
}
