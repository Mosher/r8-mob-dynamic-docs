import { IPSDEEditForm } from '@ibiz/dynamic-model-api';
/**
 * 移动端表单部件模型
 *
 * @export
 * @class AppMobFormModel
 */
export declare class AppMobFormModel {
    /**
     * 表单实例对象
     *
     * @memberof AppMobFormModel
     */
    FormInstance: IPSDEEditForm;
    /**
     * Creates an instance of AppMobFormModel.
     *
     * @param {*} [opts={}]
     * @memberof AppMobFormModel
     */
    constructor(opts: any);
    /**
     * 获取数据项集合
     *
     * @returns {any[]}
     * @memberof AppMobFormModel
     */
    getDataItems(): any[];
}
