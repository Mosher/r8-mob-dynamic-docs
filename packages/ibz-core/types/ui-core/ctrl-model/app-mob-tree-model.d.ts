import { IPSDETree } from '@ibiz/dynamic-model-api';
export declare class AppMobTreeModel {
    /**
     * 树部件实例对象
     *
     * @memberof AppMobTreeModel
     */
    treeInstance: IPSDETree;
    /**
     * Creates an instance of AppMobTreeModel.
     *
     * @param {*} [opts={}]
     * @memberof AppMobTreeModel
     */
    constructor(opts: any);
    getDataItems(): any[];
}
