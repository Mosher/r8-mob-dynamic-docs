import { IUIDataParam, IUIEnvironmentParam, IUIUtilParam, IParam } from '../../interface';
/**
 * 全局界面行为服务
 *
 * @export
 * @class AppGlobalUtil
 */
export declare class AppGlobalUtil {
    /**
     * @description 全局界面行为Map
     * @private
     * @type {Map<string, any>}
     * @memberof AppGlobalUtil
     */
    private static globalPluginAction;
    /**
     * @description 是否初始化全局行为插件
     * @private
     * @static
     * @type {boolean}
     * @memberof AppGlobalUtil
     */
    private static isInitPluginAction;
    /**
     * @description 初始化全局界面行为Map
     * @private
     * @memberof AppGlobalUtil
     */
    private static initGlobalPluginAction;
    /**
     * @description 获取参数
     * @static
     * @param {IUIDataParam} UIDataParam 操作环境参数
     * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
     * @param {IUIUtilParam} UIUtilParam 操作工具参数
     * @return {*}
     * @memberof AppGlobalUtil
     */
    static getParams(UIDataParam: IUIDataParam, UIEnvironmentParam: IUIEnvironmentParam, UIUtilParam: IUIUtilParam): {
        args: any[];
        actionContext: IParam;
        event: IParam | undefined;
        xData: IParam | null;
        view: IParam;
        srfParentDeCodeName: string | null;
        navContext: IParam;
    };
    /**
     * @description 执行全局界面行为
     * @static
     * @param {string} tag 界面行为标识
     * @param {IUIDataParam} UIDataParam 操作环境参数
     * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
     * @param {IUIUtilParam} UIUtilParam 操作工具参数
     * @param {*} [contextJO={}] 行为附加上下文
     * @param {*} [paramJO={}] 附加参数
     * @param {string} [srfParentDeName] 应用实体名称
     * @memberof AppGlobalUtil
     */
    static executeGlobalAction(tag: string, UIDataParam: IUIDataParam, UIEnvironmentParam: IUIEnvironmentParam, UIUtilParam: IUIUtilParam, contextJO?: any, paramJO?: any, srfParentDeName?: string): void;
    /**
     * @description 帮助
     * @static
     * @param {IUIDataParam} UIDataParam 操作环境参数
     * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
     * @param {IUIUtilParam} UIUtilParam 操作工具参数
     * @param {IParam} [contextJO={}] 行为附加上下文
     * @param {IParam} [paramJO={}] 附加参数
     * @memberof AppGlobalUtil
     */
    static HELP(UIDataParam: IUIDataParam, UIEnvironmentParam: IUIEnvironmentParam, UIUtilParam: IUIUtilParam, contextJO?: IParam, paramJO?: IParam): void;
    /**
     * @description 保存
     * @static
     * @param {IUIDataParam} UIDataParam 操作环境参数
     * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
     * @param {IUIUtilParam} UIUtilParam 操作工具参数
     * @param {IParam} [contextJO={}] 行为附加上下文
     * @param {IParam} [paramJO={}] 附加参数
     * @memberof AppGlobalUtil
     */
    static Save(UIDataParam: IUIDataParam, UIEnvironmentParam: IUIEnvironmentParam, UIUtilParam: IUIUtilParam, contextJO?: IParam, paramJO?: IParam): void;
    /**
     * @description 保存并关闭
     * @static
     * @param {IUIDataParam} UIDataParam 操作环境参数
     * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
     * @param {IUIUtilParam} UIUtilParam 操作工具参数
     * @param {IParam} [contextJO={}] 行为附加上下文
     * @param {IParam} [paramJO={}] 附加参数
     * @memberof AppGlobalUtil
     */
    static SaveAndExit(UIDataParam: IUIDataParam, UIEnvironmentParam: IUIEnvironmentParam, UIUtilParam: IUIUtilParam, contextJO?: IParam, paramJO?: IParam): void;
    /**
     * @description 保存并新建
     * @static
     * @param {IUIDataParam} UIDataParam 操作环境参数
     * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
     * @param {IUIUtilParam} UIUtilParam 操作工具参数
     * @param {IParam} [contextJO={}] 行为附加上下文
     * @param {IParam} [paramJO={}] 附加参数
     * @memberof AppGlobalUtil
     */
    static SaveAndNew(UIDataParam: IUIDataParam, UIEnvironmentParam: IUIEnvironmentParam, UIUtilParam: IUIUtilParam, contextJO?: IParam, paramJO?: IParam): void;
    /**
     * @description 保存行
     * @static
     * @param {IUIDataParam} UIDataParam 操作环境参数
     * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
     * @param {IUIUtilParam} UIUtilParam 操作工具参数
     * @param {IParam} [contextJO={}] 行为附加上下文
     * @param {IParam} [paramJO={}] 附加参数
     * @memberof AppGlobalUtil
     */
    static SaveRow(UIDataParam: IUIDataParam, UIEnvironmentParam: IUIEnvironmentParam, UIUtilParam: IUIUtilParam, contextJO?: IParam, paramJO?: IParam): void;
    /**
     * @description 编辑
     * @static
     * @param {IUIDataParam} UIDataParam 操作环境参数
     * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
     * @param {IUIUtilParam} UIUtilParam 操作工具参数
     * @param {IParam} [contextJO={}] 行为附加上下文
     * @param {IParam} [paramJO={}] 附加参数
     * @memberof AppGlobalUtil
     */
    static Edit(UIDataParam: IUIDataParam, UIEnvironmentParam: IUIEnvironmentParam, UIUtilParam: IUIUtilParam, contextJO?: IParam, paramJO?: IParam): void;
    /**
     * @description 查看
     * @static
     * @param {IUIDataParam} UIDataParam 操作环境参数
     * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
     * @param {IUIUtilParam} UIUtilParam 操作工具参数
     * @param {IParam} [contextJO={}] 行为附加上下文
     * @param {IParam} [paramJO={}] 附加参数
     * @memberof AppGlobalUtil
     */
    static View(UIDataParam: IUIDataParam, UIEnvironmentParam: IUIEnvironmentParam, UIUtilParam: IUIUtilParam, contextJO?: IParam, paramJO?: IParam): void;
    /**
     * @description 打印
     * @static
     * @param {IUIDataParam} UIDataParam 操作环境参数
     * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
     * @param {IUIUtilParam} UIUtilParam 操作工具参数
     * @param {IParam} [contextJO={}] 行为附加上下文
     * @param {IParam} [paramJO={}] 附加参数
     * @memberof AppGlobalUtil
     */
    static PRINT(UIDataParam: IUIDataParam, UIEnvironmentParam: IUIEnvironmentParam, UIUtilParam: IUIUtilParam, contextJO?: IParam, paramJO?: IParam): void;
    /**
     * @description 当前流程步骤
     * @static
     * @param {IUIDataParam} UIDataParam 操作环境参数
     * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
     * @param {IUIUtilParam} UIUtilParam 操作工具参数
     * @param {IParam} [contextJO={}] 行为附加上下文
     * @param {IParam} [paramJO={}] 附加参数
     * @memberof AppGlobalUtil
     */
    static ViewWFStep(UIDataParam: IUIDataParam, UIEnvironmentParam: IUIEnvironmentParam, UIUtilParam: IUIUtilParam, contextJO?: IParam, paramJO?: IParam): void;
    /**
     * @description 导出
     * @static
     * @param {IUIDataParam} UIDataParam 操作环境参数
     * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
     * @param {IUIUtilParam} UIUtilParam 操作工具参数
     * @param {IParam} [contextJO={}] 行为附加上下文
     * @param {IParam} [paramJO={}] 附加参数
     * @memberof AppGlobalUtil
     */
    static ExportExcel(UIDataParam: IUIDataParam, UIEnvironmentParam: IUIEnvironmentParam, UIUtilParam: IUIUtilParam, contextJO?: IParam, paramJO?: IParam): void;
    /**
     * @description 第一个记录
     * @static
     * @param {IUIDataParam} UIDataParam 操作环境参数
     * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
     * @param {IUIUtilParam} UIUtilParam 操作工具参数
     * @param {IParam} [contextJO={}] 行为附加上下文
     * @param {IParam} [paramJO={}] 附加参数
     * @memberof AppGlobalUtil
     */
    static FirstRecord(UIDataParam: IUIDataParam, UIEnvironmentParam: IUIEnvironmentParam, UIUtilParam: IUIUtilParam, contextJO?: IParam, paramJO?: IParam): void;
    /**
     * @description 关闭
     * @static
     * @param {IUIDataParam} UIDataParam 操作环境参数
     * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
     * @param {IUIUtilParam} UIUtilParam 操作工具参数
     * @param {IParam} [contextJO={}] 行为附加上下文
     * @param {IParam} [paramJO={}] 附加参数
     * @memberof AppGlobalUtil
     */
    static Exit(UIDataParam: IUIDataParam, UIEnvironmentParam: IUIEnvironmentParam, UIUtilParam: IUIUtilParam, contextJO?: IParam, paramJO?: IParam): void;
    /**
     * @description 过滤
     * @static
     * @param {IUIDataParam} UIDataParam 操作环境参数
     * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
     * @param {IUIUtilParam} UIUtilParam 操作工具参数
     * @param {IParam} [contextJO={}] 行为附加上下文
     * @param {IParam} [paramJO={}] 附加参数
     * @memberof AppGlobalUtil
     */
    static ToggleFilter(UIDataParam: IUIDataParam, UIEnvironmentParam: IUIEnvironmentParam, UIUtilParam: IUIUtilParam, contextJO?: IParam, paramJO?: IParam): void;
    /**
     * @description 开始流程
     * @static
     * @param {IUIDataParam} UIDataParam 操作环境参数
     * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
     * @param {IUIUtilParam} UIUtilParam 操作工具参数
     * @param {IParam} [contextJO={}] 行为附加上下文
     * @param {IParam} [paramJO={}] 附加参数
     * @memberof AppGlobalUtil
     */
    static SaveAndStart(UIDataParam: IUIDataParam, UIEnvironmentParam: IUIEnvironmentParam, UIUtilParam: IUIUtilParam, contextJO?: IParam, paramJO?: IParam): Promise<void>;
    /**
     * @description 拷贝
     * @static
     * @param {IUIDataParam} UIDataParam 操作环境参数
     * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
     * @param {IUIUtilParam} UIUtilParam 操作工具参数
     * @param {IParam} [contextJO={}] 行为附加上下文
     * @param {IParam} [paramJO={}] 附加参数
     * @memberof AppGlobalUtil
     */
    static Copy(UIDataParam: IUIDataParam, UIEnvironmentParam: IUIEnvironmentParam, UIUtilParam: IUIUtilParam, contextJO?: IParam, paramJO?: IParam): void;
    /**
     * @description 删除
     * @static
     * @param {IUIDataParam} UIDataParam 操作环境参数
     * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
     * @param {IUIUtilParam} UIUtilParam 操作工具参数
     * @param {IParam} [contextJO={}] 行为附加上下文
     * @param {IParam} [paramJO={}] 附加参数
     * @memberof AppGlobalUtil
     */
    static Remove(UIDataParam: IUIDataParam, UIEnvironmentParam: IUIEnvironmentParam, UIUtilParam: IUIUtilParam, contextJO?: IParam, paramJO?: IParam): void;
    /**
     * @description 删除并关闭
     * @static
     * @param {IUIDataParam} UIDataParam 操作环境参数
     * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
     * @param {IUIUtilParam} UIUtilParam 操作工具参数
     * @param {IParam} [contextJO={}] 行为附加上下文
     * @param {IParam} [paramJO={}] 附加参数
     * @memberof AppGlobalUtil
     */
    static RemoveAndExit(UIDataParam: IUIDataParam, UIEnvironmentParam: IUIEnvironmentParam, UIUtilParam: IUIUtilParam, contextJO?: IParam, paramJO?: IParam): void;
    /**
     * @description 上一个记录
     * @static
     * @param {IUIDataParam} UIDataParam 操作环境参数
     * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
     * @param {IUIUtilParam} UIUtilParam 操作工具参数
     * @param {IParam} [contextJO={}] 行为附加上下文
     * @param {IParam} [paramJO={}] 附加参数
     * @memberof AppGlobalUtil
     */
    static PrevRecord(UIDataParam: IUIDataParam, UIEnvironmentParam: IUIEnvironmentParam, UIUtilParam: IUIUtilParam, contextJO?: IParam, paramJO?: IParam): void;
    /**
     * @description 树刷新父数据
     * @static
     * @param {IUIDataParam} UIDataParam 操作环境参数
     * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
     * @param {IUIUtilParam} UIUtilParam 操作工具参数
     * @param {IParam} [contextJO={}] 行为附加上下文
     * @param {IParam} [paramJO={}] 附加参数
     * @memberof AppGlobalUtil
     */
    static RefreshParent(UIDataParam: IUIDataParam, UIEnvironmentParam: IUIEnvironmentParam, UIUtilParam: IUIUtilParam, contextJO?: IParam, paramJO?: IParam): void;
    /**
     * @description 树刷新全部节点
     * @static
     * @param {IUIDataParam} UIDataParam 操作环境参数
     * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
     * @param {IUIUtilParam} UIUtilParam 操作工具参数
     * @param {IParam} [contextJO={}] 行为附加上下文
     * @param {IParam} [paramJO={}] 附加参数
     * @memberof AppGlobalUtil
     */
    static RefreshAll(UIDataParam: IUIDataParam, UIEnvironmentParam: IUIEnvironmentParam, UIUtilParam: IUIUtilParam, contextJO?: IParam, paramJO?: IParam): void;
    /**
     * @description 数据导入
     * @static
     * @param {IUIDataParam} UIDataParam 操作环境参数
     * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
     * @param {IUIUtilParam} UIUtilParam 操作工具参数
     * @param {IParam} [contextJO={}] 行为附加上下文
     * @param {IParam} [paramJO={}] 附加参数
     * @memberof AppGlobalUtil
     */
    static Import(UIDataParam: IUIDataParam, UIEnvironmentParam: IUIEnvironmentParam, UIUtilParam: IUIUtilParam, contextJO?: IParam, paramJO?: IParam): void;
    /**
     * @description 刷新
     * @static
     * @param {IUIDataParam} UIDataParam 操作环境参数
     * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
     * @param {IUIUtilParam} UIUtilParam 操作工具参数
     * @param {IParam} [contextJO={}] 行为附加上下文
     * @param {IParam} [paramJO={}] 附加参数
     * @memberof AppGlobalUtil
     */
    static Refresh(UIDataParam: IUIDataParam, UIEnvironmentParam: IUIEnvironmentParam, UIUtilParam: IUIUtilParam, contextJO?: IParam, paramJO?: IParam): void;
    /**
     * @description 下一个记录
     * @static
     * @param {IUIDataParam} UIDataParam 操作环境参数
     * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
     * @param {IUIUtilParam} UIUtilParam 操作工具参数
     * @param {IParam} [contextJO={}] 行为附加上下文
     * @param {IParam} [paramJO={}] 附加参数
     * @memberof AppGlobalUtil
     */
    static NextRecord(UIDataParam: IUIDataParam, UIEnvironmentParam: IUIEnvironmentParam, UIUtilParam: IUIUtilParam, contextJO?: IParam, paramJO?: IParam): void;
    /**
     * @description 新建
     * @static
     * @param {IUIDataParam} UIDataParam 操作环境参数
     * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
     * @param {IUIUtilParam} UIUtilParam 操作工具参数
     * @param {IParam} [contextJO={}] 行为附加上下文
     * @param {IParam} [paramJO={}] 附加参数
     * @memberof AppGlobalUtil
     */
    static New(UIDataParam: IUIDataParam, UIEnvironmentParam: IUIEnvironmentParam, UIUtilParam: IUIUtilParam, contextJO?: IParam, paramJO?: IParam): void;
    /**
     * @description 新建行
     * @static
     * @param {IUIDataParam} UIDataParam 操作环境参数
     * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
     * @param {IUIUtilParam} UIUtilParam 操作工具参数
     * @param {IParam} [contextJO={}] 行为附加上下文
     * @param {IParam} [paramJO={}] 附加参数
     * @memberof AppGlobalUtil
     */
    static NewRow(UIDataParam: IUIDataParam, UIEnvironmentParam: IUIEnvironmentParam, UIUtilParam: IUIUtilParam, contextJO?: IParam, paramJO?: IParam): void;
    /**
     * @description 行编辑
     * @static
     * @param {IUIDataParam} UIDataParam 操作环境参数
     * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
     * @param {IUIUtilParam} UIUtilParam 操作工具参数
     * @param {IParam} [contextJO={}] 行为附加上下文
     * @param {IParam} [paramJO={}] 附加参数
     * @memberof AppGlobalUtil
     */
    static ToggleRowEdit(UIDataParam: IUIDataParam, UIEnvironmentParam: IUIEnvironmentParam, UIUtilParam: IUIUtilParam, contextJO?: IParam, paramJO?: IParam): void;
    /**
     * @description 最后一个记录
     * @static
     * @param {IUIDataParam} UIDataParam 操作环境参数
     * @param {IUIEnvironmentParam} UIEnvironmentParam 操作环境参数
     * @param {IUIUtilParam} UIUtilParam 操作工具参数
     * @param {IParam} [contextJO={}] 行为附加上下文
     * @param {IParam} [paramJO={}] 附加参数
     * @memberof AppGlobalUtil
     */
    static LastRecord(UIDataParam: IUIDataParam, UIEnvironmentParam: IUIEnvironmentParam, UIUtilParam: IUIUtilParam, contextJO?: IParam, paramJO?: IParam): void;
}
