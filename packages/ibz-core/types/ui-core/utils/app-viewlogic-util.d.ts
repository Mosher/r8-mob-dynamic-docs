import { IPSAppViewLogic } from '@ibiz/dynamic-model-api';
import { IParam, IUIDataParam, IUIEnvironmentParam, IUIUtilParam } from '../../interface';
/**
 * 视图逻辑服务
 *
 * @export
 * @class AppViewLogicUtil
 */
export declare class AppViewLogicUtil {
    /**
     * 执行视图逻辑
     *
     * @param item 触发标识
     * @param UIDataParam 操作数据参数
     * @param UIEnvironmentParam 操作环境参数
     * @param UIUtilParam 操作工具参数
     * @param viewlogicData 当前容器视图逻辑集合
     * @param params 附加参数
     * @memberof AppViewLogicUtil
     */
    static executeViewLogic(item: string | IPSAppViewLogic | null, UIDataParam: IUIDataParam, UIEnvironmentParam: IUIEnvironmentParam, UIUtilParam: IUIUtilParam, viewlogicData?: Array<IPSAppViewLogic> | null, params?: IParam): void;
    /**
     * 执行逻辑
     *
     * @param viewLogic 触发逻辑
     * @param UIDataParam 操作数据参数
     * @param UIEnvironmentParam 操作环境参数
     * @param UIUtilParam 操作工具参数
     * @param params 附加参数
     */
    static executeLogic(viewLogic: IPSAppViewLogic, UIDataParam: IUIDataParam, UIEnvironmentParam: IUIEnvironmentParam, UIUtilParam: IUIUtilParam, params?: IParam): Promise<void>;
}
