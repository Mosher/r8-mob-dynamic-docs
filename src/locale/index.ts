import { createI18n } from 'vue-i18n'

import { zh_CN } from 'ibz-ui-ionic';
import { app_zh_CN } from './app_zh_CN';

const messages = {
    'zh-CN': Object.assign(zh_CN(), app_zh_CN()),
};

import { en_US } from 'ibz-ui-ionic';
import { app_en_US } from './app_en_US';

Object.assign(messages, {
    'en-US': Object.assign(en_US(),app_en_US()),
});

// 自动根据浏览器系统语言设置语言
const navLang = localStorage.getItem('local') || navigator.language;
const localLang = (navLang === 'zh-CN' || (navLang === 'en-US' && messages.hasOwnProperty('en-US'))) ? navLang : false;
let lang: string = localLang || 'zh-CN';

const i18n = createI18n({
    locale: lang,
    messages,
    globalInjection:true
});

export default i18n;
