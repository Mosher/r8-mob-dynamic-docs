import { IUIApp } from 'ibz-core';

declare global {
    interface Window {
        App: IUIApp;
        ServiceCache: Map<string, any>;
    }
    const App: IUIApp;
    const ServiceCache: Map<string, any>;
}
