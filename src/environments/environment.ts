export const Environment = {
    // 应用名称
    AppName: 'TestMob',
    // 应用标题
    AppTitle: '移动端测试',
    // 应用基础路径
    BaseUrl: '/api',
    // 系统名称
    SysName: 'StudioPreview',
    // 应用首页路径
    AppIndexViewName:'appindexview',
    // 登录
    RemoteLogin: '/v7/login',
    // 登出
    RemoteLogout: '/v7/logout',
    // 文件导出
    ExportFile: '/ibizutil/download',
    // 文件上传
    UploadFile: '/ibizutil/upload',
    // 数据导入单次上传最大数量
    sliceUploadCnt: 100,
    // 是否为pc端应用
    isAppMode: false,
    // 是否为开发模式
    devMode: true,
    // 是否启用AppData
    enableAppData: false,
    // 是否开启权限认证
    enablePermissionValid: false,
    // 应用动态路径
    appDynaModelFilePath: 'PSSYSAPPS/TestMob/PSSYSAPP.json',
    // 远端动态基础路径
    remoteDynaPath: '',
    // 是否启用动态
    bDynamic: false,
    // 是否预览模式
    isPreviewMode: false,
    // SaaS模式
    SaaSMode: false,
    // 仿真mockDcSystemId
    mockDcSystemId: '',
    // 登录地址
    loginUrl: '',
    // 门户地址
    portalUrl: '',
    // 应用是否支持多语言
    isEnableMultiLan:true,
    // 刷新token即将到期时间间隔(默认10分钟，单位：ms)
    refreshTokenTime: 600000
};
// 挂载外部配置文件
if ((window as any).Environment) {
    Object.assign(Environment, (window as any).Environment);
}