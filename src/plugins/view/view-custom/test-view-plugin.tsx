
import { AppViewProps } from "ibz-core";
import { GenerateComponent, ViewComponentBase } from "ibz-ui-ionic";
import { shallowReactive } from "vue";
import { DEMultiDataViewComponentBase } from 'ibz-ui-ionic';
import { AppMobMapViewProps, IMobMapViewController, MobMapViewController } from 'ibz-core';
import '../subview-style.scss';

/**
 * 测试视图插件
 *
 * @export
 * @class TestViewPlugin
 */
export class TestViewPlugin extends DEMultiDataViewComponentBase<AppMobMapViewProps> {
            setup() {
        const targetController = this.getViewControllerByType('DEMOBMAPVIEW');
        if(targetController){
            this.c = shallowReactive(targetController) as any;
            super.setup();
        }
    }    

	public render(){
        if (!this.viewIsLoaded || !this.c.viewInstance) {
            return null;
        }
        return <div>视图test前端模板插件</div>;
    }
}
// 测试视图插件组件
export const TestViewPluginComponent = GenerateComponent(TestViewPlugin, new AppViewProps());