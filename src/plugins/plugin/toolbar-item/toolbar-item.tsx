

import { IParam } from "ibz-core";


/**
 * 工具栏项插件
 *
 * @export
 * @class ToolbarItem
 */
export class ToolbarItem {

    

    /**
     * 绘制项数据
     * 
     * @param {IParam} item 项数据
     * @param {*} controller 部件控制器
     * @param {*} container 部件组件容器
     * @memberof ToolbarItem
     */
    public renderItem(item: IParam, controller: any, container: any) {
        return <div>工具栏项插件暂未实现</div>
    }
}
