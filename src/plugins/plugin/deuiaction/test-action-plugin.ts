
import { IPSAppDEUIAction } from "@ibiz/dynamic-model-api";
import { IUIDataParam, IUIEnvironmentParam, IUIUtilParam } from "ibz-core";


/**
 * 测试界面行为插件
 *
 * @export
 * @class TestActionPlugin
 * @class TestActionPlugin
 */
export default class TestActionPlugin {

    /**
     * 模型数据
     * 
     * @memberof TestActionPlugin
     */
    private actionModel !: IPSAppDEUIAction;

     

    /**
     * 初始化 TestActionPlugin
     * 
     * @memberof TestActionPlugin
     */
    constructor(opts: any, context?: any) {
        this.actionModel = opts;
    }

    /**
     * 执行界面行为
     *
     * @param context 附加上下文
     * @param params 附加参数
     * @param UIDataParam 操作数据参数
     * @param UIEnvironmentParam 操作环境参数
     * @param UIUtilParam 操作工具参数
     * @param srfParentDeName 父值参数
     * @param deUIService 界面UI服务
     *
     * @memberof TestActionPlugin
     */
    public async execute(
        context: any = {},
        params: any = {},
        UIDataParam: IUIDataParam,
        UIEnvironmentParam: IUIEnvironmentParam,
        UIUtilParam: IUIUtilParam,
        srfParentDeName?: string,
        deUIService?: any
    ) {
        alert('全局界面行为插件')
    }

}
