

import { IParam } from "ibz-core";


/**
 * 前端模板插件
 *
 * @export
 * @class PreviewHome
 */
export class PreviewHome {

    

    /**
     * 绘制项数据
     * 
     * @param {IParam} item 项数据
     * @param {*} controller 部件控制器
     * @param {*} container 部件组件容器
     * @memberof PreviewHome
     */
    public renderItem(item: IParam, controller: any, container: any) {
        return <div>前端模板插件暂未实现</div>
    }
}
