

import { shallowReactive } from "vue";
import { CtrlComponentBase, GenerateComponent } from "ibz-ui-ionic";
import { AppCtrlProps } from "ibz-core";



/**
 * 嵌入视图
 *
 * @export
 * @class  EmbedView
 * @extends {CtrlComponentBase}
 */
export class  EmbedView extends CtrlComponentBase<AppCtrlProps> {

    /**
     * 设置响应式
     *
     * @public
     * @memberof  EmbedView
     */
    setup() {
        const targetController = this.getCtrlControllerByType('PORTLET');
        if(targetController){
            this.c = shallowReactive(targetController);
            super.setup();
        }
    }

    /**
     * 绘制工具栏
     *
     * @returns {*}
     * @memberof  EmbedView
     */
    public render(): any {
        return <div>移动端部件模板插件测试</div>
    }
}

// 嵌入视图组件
export const EmbedViewComponent = GenerateComponent(EmbedView, new AppCtrlProps());

