

import { shallowReactive } from "vue";
import { CtrlComponentBase, GenerateComponent } from "ibz-ui-ionic";
import { AppCtrlProps } from "ibz-core";



/**
 * 图表导航插件测试
 *
 * @export
 * @class  Chartexptest
 * @extends {CtrlComponentBase}
 */
export class  Chartexptest extends CtrlComponentBase<AppCtrlProps> {

    /**
     * 设置响应式
     *
     * @public
     * @memberof  Chartexptest
     */
    setup() {
        const targetController = this.getCtrlControllerByType('CHARTEXPBAR');
        if(targetController){
            this.c = shallowReactive(targetController);
            super.setup();
        }
    }

    /**
     * 绘制工具栏
     *
     * @returns {*}
     * @memberof  Chartexptest
     */
    public render(): any {
        return <div>移动端部件模板插件测试</div>
    }
}

// 图表导航插件测试组件
export const ChartexptestComponent = GenerateComponent(Chartexptest, new AppCtrlProps());

