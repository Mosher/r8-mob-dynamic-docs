

import { shallowReactive } from "vue";
import { CtrlComponentBase, GenerateComponent } from "ibz-ui-ionic";
import { AppCtrlProps } from "ibz-core";



/**
 * 数据看板容器
 *
 * @export
 * @class  DashboardPlugin
 * @extends {CtrlComponentBase}
 */
export class  DashboardPlugin extends CtrlComponentBase<AppCtrlProps> {

    /**
     * 设置响应式
     *
     * @public
     * @memberof  DashboardPlugin
     */
    setup() {
        const targetController = this.getCtrlControllerByType('APPMENU');
        if(targetController){
            this.c = shallowReactive(targetController);
            super.setup();
        }
    }

    /**
     * 绘制工具栏
     *
     * @returns {*}
     * @memberof  DashboardPlugin
     */
    public render(): any {
        return <div>移动端部件模板插件测试</div>
    }
}

// 数据看板容器组件
export const DashboardPluginComponent = GenerateComponent(DashboardPlugin, new AppCtrlProps());

