

import { shallowReactive } from "vue";
import { CtrlComponentBase, GenerateComponent } from "ibz-ui-ionic";
import { AppCtrlProps } from "ibz-core";



/**
 * 数据看板容器
 *
 * @export
 * @class  CUSTOMe24b2a8140
 * @extends {CtrlComponentBase}
 */
export class  CUSTOMe24b2a8140 extends CtrlComponentBase<AppCtrlProps> {

    /**
     * 设置响应式
     *
     * @public
     * @memberof  CUSTOMe24b2a8140
     */
    setup() {
        const targetController = this.getCtrlControllerByType('DASHBOARD');
        if(targetController){
            this.c = shallowReactive(targetController);
            super.setup();
        }
    }

    /**
     * 绘制工具栏
     *
     * @returns {*}
     * @memberof  CUSTOMe24b2a8140
     */
    public render(): any {
        return <div>移动端部件模板插件测试</div>
    }
}

// 数据看板容器组件
export const CUSTOMe24b2a8140Component = GenerateComponent(CUSTOMe24b2a8140, new AppCtrlProps());

