

import { IParam } from "ibz-core";


/**
 * 测试自定义部件插件
 *
 * @export
 * @class TestUserControlPlugin
 */
export class TestUserControlPlugin {

    

    /**
     * 绘制项数据
     * 
     * @param {IParam} item 项数据
     * @param {*} controller 部件控制器
     * @param {*} container 部件组件容器
     * @memberof TestUserControlPlugin
     */
    public renderItem(item: IParam, controller: any, container: any) {
        return <div>测试自定义部件插件暂未实现</div>
    }
}
