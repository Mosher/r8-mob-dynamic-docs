

import { IParam } from "ibz-core";


/**
 * 面板自定义部件
 *
 * @export
 * @class MobPanelCustonCtrl
 */
export class MobPanelCustonCtrl {

    

    /**
     * 绘制项数据
     * 
     * @param {IParam} item 项数据
     * @param {*} controller 部件控制器
     * @param {*} container 部件组件容器
     * @memberof MobPanelCustonCtrl
     */
    public renderItem(item: IParam, controller: any, container: any) {
        return <div>面板自定义部件暂未实现</div>
    }
}
