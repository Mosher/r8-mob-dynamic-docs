

import { shallowReactive } from "vue";
import { CtrlComponentBase, GenerateComponent } from "ibz-ui-ionic";
import { AppCtrlProps } from "ibz-core";



/**
 * 菜单栏标题
 *
 * @export
 * @class  MenuTitle
 * @extends {CtrlComponentBase}
 */
export class  MenuTitle extends CtrlComponentBase<AppCtrlProps> {

    /**
     * 设置响应式
     *
     * @public
     * @memberof  MenuTitle
     */
    setup() {
        const targetController = this.getCtrlControllerByType('APPMENU');
        if(targetController){
            this.c = shallowReactive(targetController);
            super.setup();
        }
    }

    /**
     * 绘制工具栏
     *
     * @returns {*}
     * @memberof  MenuTitle
     */
    public render(): any {
        return <div>移动端部件模板插件测试</div>
    }
}

// 菜单栏标题组件
export const MenuTitleComponent = GenerateComponent(MenuTitle, new AppCtrlProps());

