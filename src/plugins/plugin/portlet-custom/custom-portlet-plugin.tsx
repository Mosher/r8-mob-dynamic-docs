

import { shallowReactive } from "vue";
import { CtrlComponentBase, GenerateComponent } from "ibz-ui-ionic";
import { AppCtrlProps } from "ibz-core";



/**
 * 自定义门户部件插件
 *
 * @export
 * @class  CustomPortletPlugin
 * @extends {CtrlComponentBase}
 */
export class  CustomPortletPlugin extends CtrlComponentBase<AppCtrlProps> {

        /**
     * 设置响应式
     *
     * @public
     * @memberof CustomPortletPlugin
     */
    setup() {
        const targetController = this.getCtrlControllerByType('PORTLET');
        if (targetController) {
            this.c = shallowReactive(targetController);
            super.setup();
        }
    }

    /**
     * 绘制内容
     *
     * @public
     * @memberof CustomPortletPlugin
     */
    render() {
        return <div>自定义门户部件插件</div>
    }
}

// 自定义门户部件插件组件
export const CustomPortletPluginComponent = GenerateComponent(CustomPortletPlugin, new AppCtrlProps());

