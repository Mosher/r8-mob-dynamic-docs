

import { IParam } from "ibz-core";


/**
 * 直接内容
 *
 * @export
 * @class RawhtmlPlugin
 */
export class RawhtmlPlugin {

    

    /**
     * 绘制项数据
     * 
     * @param {IParam} item 项数据
     * @param {*} controller 部件控制器
     * @param {*} container 部件组件容器
     * @memberof RawhtmlPlugin
     */
    public renderItem(item: IParam, controller: any, container: any) {
        return <div>直接内容暂未实现</div>
    }
}
