

import { IParam } from "ibz-core";


/**
 * 测试编辑器插件
 *
 * @export
 * @class EDITOR_CUSTOMSTYLEb217f3e900
 */
export class EDITOR_CUSTOMSTYLEb217f3e900 {

    

    /**
     * 绘制项数据
     * 
     * @param {IParam} item 项数据
     * @param {*} controller 部件控制器
     * @param {*} container 部件组件容器
     * @memberof EDITOR_CUSTOMSTYLEb217f3e900
     */
    public renderItem(item: IParam, controller: any, container: any) {
        <div>测试</div>
    }
}
