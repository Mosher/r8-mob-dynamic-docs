

import { shallowReactive } from "vue";
import { CtrlComponentBase, GenerateComponent } from "ibz-ui-ionic";
import { AppCtrlProps } from "ibz-core";



/**
 * 工具栏拓展插件
 *
 * @export
 * @class  ToolbarPlugin
 * @extends {CtrlComponentBase}
 */
export class  ToolbarPlugin extends CtrlComponentBase<AppCtrlProps> {

    /**
     * 设置响应式
     *
     * @public
     * @memberof  ToolbarPlugin
     */
    setup() {
        const targetController = this.getCtrlControllerByType('TOOLBAR');
        if(targetController){
            this.c = shallowReactive(targetController);
            super.setup();
        }
    }

    /**
     * 绘制工具栏
     *
     * @returns {*}
     * @memberof  ToolbarPlugin
     */
    public render(): any {
        return <div>移动端部件模板插件测试</div>
    }
}

// 工具栏拓展插件组件
export const ToolbarPluginComponent = GenerateComponent(ToolbarPlugin, new AppCtrlProps());

