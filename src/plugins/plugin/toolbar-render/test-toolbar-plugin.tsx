

import { shallowReactive } from "vue";
import { CtrlComponentBase, GenerateComponent } from "ibz-ui-ionic";
import { AppCtrlProps } from "ibz-core";



/**
 * 测试工具栏插件
 *
 * @export
 * @class  TestToolbarPlugin
 * @extends {CtrlComponentBase}
 */
export class  TestToolbarPlugin extends CtrlComponentBase<AppCtrlProps> {

    /**
     * 设置响应式
     *
     * @public
     * @memberof  TestToolbarPlugin
     */
    setup() {
        const targetController = this.getCtrlControllerByType('TOOLBAR');
        if(targetController){
            this.c = shallowReactive(targetController);
            super.setup();
        }
    }

    /**
     * 绘制工具栏
     *
     * @returns {*}
     * @memberof  TestToolbarPlugin
     */
    public render(): any {
        return <div>移动端部件模板插件测试</div>
    }
}

// 测试工具栏插件组件
export const TestToolbarPluginComponent = GenerateComponent(TestToolbarPlugin, new AppCtrlProps());

