

import { IParam } from "ibz-core";


/**
 * 日历分组前端模板插件
 *
 * @export
 * @class CalendarGroupItem
 */
export class CalendarGroupItem {

    

    /**
     * 绘制项数据
     * 
     * @param {IParam} item 项数据
     * @param {*} controller 部件控制器
     * @param {*} container 部件组件容器
     * @memberof CalendarGroupItem
     */
    public renderItem(item: IParam, controller: any, container: any) {
        return <div>日历分组前端模板插件暂未实现</div>
    }
}
