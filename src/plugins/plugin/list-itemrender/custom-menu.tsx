

import { IParam } from "ibz-core";


/**
 * 菜单栏拓展插件
 *
 * @export
 * @class CustomMenu
 */
export class CustomMenu {

    

    /**
     * 绘制项数据
     * 
     * @param {IParam} item 项数据
     * @param {*} controller 部件控制器
     * @param {*} container 部件组件容器
     * @memberof CustomMenu
     */
    public renderItem(item: IParam, controller: any, container: any) {
        return <div>菜单栏拓展插件暂未实现</div>
    }
}
