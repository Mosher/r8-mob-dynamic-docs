// 注册视图插件
import { TestViewPluginComponent } from './view/view-custom/test-view-plugin';
// 注册部件插件
import { CustomPortletPluginComponent } from './plugin/portlet-custom/custom-portlet-plugin';
// 注册编辑器插件
import { Auto6Component } from './editor/mobdate/auto6';
import { DZQMComponent } from './editor/mobpicture/dzqm';
import { TestEdiotrStyleComponent } from './editor/mobtext/test-ediotr-style';
import { Auto1Component } from './editor/mobdate/auto1';
import { Auto3Component } from './editor/mobdate/auto3';
import { Auto8Component } from './editor/mobdate/auto8';
export const PluginRegister = {
    install(v: any, opt: any) {
        // 注册视图插件
        v.component('app-view-custom-test-view-plugin',TestViewPluginComponent);
        // 注册部件插件
        v.component('app-portlet-custom-custom-portlet-plugin',CustomPortletPluginComponent);
       // 注册编辑器插件
        v.component('app-mobdate-auto6', Auto6Component);
        v.component('app-mobpicture-dzqm', DZQMComponent);
        v.component('app-mobtext-test-ediotr-style', TestEdiotrStyleComponent);
        v.component('app-mobdate-auto1', Auto1Component);
        v.component('app-mobdate-auto3', Auto3Component);
        v.component('app-mobdate-auto8', Auto8Component);
    }
}