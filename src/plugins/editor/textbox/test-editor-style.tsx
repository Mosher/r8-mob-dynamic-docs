
import { shallowReactive } from 'vue';
import { AppEditorProps } from 'ibz-core';
import { EditorComponentBase, GenerateComponent } from 'ibz-ui-ionic';



/**
 * 测试编辑器样式
 *
 * @export
 * @class TestEditorStyle
 * @extends {EditorComponentBase}
 */
export class TestEditorStyle extends EditorComponentBase<AppEditorProps> {

    /**
    * 设置响应式
    *
    * @public
    * @memberof TestEditorStyle
    */
    setup() {
        const targetController = this.getEditorControllerByType('TEXTBOX');
        if(targetController){
            this.c = shallowReactive(targetController);
            super.setup();
        }
    }

    /**
    * 绘制内容
    *
    * @public
    * @memberof TestEditorStyle
    */
    render() {
        return <div>测试编辑器样式</div>
    }
}
// 测试编辑器样式组件
export const TestEditorStyleComponent = GenerateComponent(TestEditorStyle, new AppEditorProps());
