
import { shallowReactive } from 'vue';
import { AppEditorProps } from 'ibz-core';
import { EditorComponentBase, GenerateComponent } from 'ibz-ui-ionic';



/**
 * 测试编辑器样式
 *
 * @export
 * @class TestEdiotrStyle
 * @extends {EditorComponentBase}
 */
export class TestEdiotrStyle extends EditorComponentBase<AppEditorProps> {

    /**
    * 设置响应式
    *
    * @public
    * @memberof TestEdiotrStyle
    */
    setup() {
        const targetController = this.getEditorControllerByType('MOBTEXT');
        if(targetController){
            this.c = shallowReactive(targetController);
            super.setup();
        }
    }

    /**
    * 绘制内容
    *
    * @public
    * @memberof TestEdiotrStyle
    */
    render() {
        return <div>测试编辑器样式</div>
    }
}
// 测试编辑器样式组件
export const TestEdiotrStyleComponent = GenerateComponent(TestEdiotrStyle, new AppEditorProps());
