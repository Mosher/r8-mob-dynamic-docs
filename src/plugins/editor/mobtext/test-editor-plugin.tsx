
import { shallowReactive } from 'vue';
import { AppEditorProps } from 'ibz-core';
import { EditorComponentBase, GenerateComponent } from 'ibz-ui-ionic';



/**
 * 测试编辑器插件
 *
 * @export
 * @class TestEditorPlugin
 * @extends {EditorComponentBase}
 */
export class TestEditorPlugin extends EditorComponentBase<AppEditorProps> {

    /**
    * 设置响应式
    *
    * @public
    * @memberof TestEditorPlugin
    */
    setup() {
        const targetController = this.getEditorControllerByType('MOBTEXT');
        if(targetController){
            this.c = shallowReactive(targetController);
            super.setup();
        }
    }

    /**
    * 绘制内容
    *
    * @public
    * @memberof TestEditorPlugin
    */
    render() {
        return <div>测试编辑器插件</div>
    }
}
// 测试编辑器插件组件
export const TestEditorPluginComponent = GenerateComponent(TestEditorPlugin, new AppEditorProps());
