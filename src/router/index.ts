import qs from 'qs';
import { RouteRecordRaw } from 'vue-router';
import { AuthGuard } from '@/auth-guard/auth-guard';
import { createRouter, createWebHashHistory } from '@ionic/vue-router';
import { globalRoutes, indexRoutes } from './custom-route';
import { AppRouteShellComponent, AppIndexContainerShellComponent, AppViewContainerShellComponent, AppLoginComponent, App404Component, App500Component, AppSettingComponent } from '@/components';
const routes: RouteRecordRaw[] = [
    {
        path: '/appindexview/:appindexview?',
        beforeEnter: (to: any, from: any, next: any) => {
            const routerParamsName = 'appindexview';
            const params: any = {};
            if (to.params && to.params[routerParamsName]) {
                Object.assign(params, qs.parse(to.params[routerParamsName], { delimiter: ';' }));
            }
            const auth: Promise<any> = AuthGuard.getInstance().authGuard('/appdata', params, router);
            auth.then((result) => {
                next(result ? true : false);
            }).catch(() => {
                next(false);
            });
        },
        meta: {  
            captionTag: '',
            caption: '应用首页视图',
            info:'',
            viewType: 'APPINDEX',
            dynaModelFilePath:'PSSYSAPPS/TestMob/PSAPPINDEXVIEWS/AppIndexView.json',
            parameters: [
                { pathName: 'views', parameterName: 'appindexview' },
            ],
            requireAuth: false,
        },
        component: AppIndexContainerShellComponent,
        children: [
            {
                path: 'views/appportalview',
                meta: {
                    captionTag: '',
                    caption: '',
                    info:'',
                    imgPath: '',
                    iconCls: '',
                    dynaModelFilePath:'PSSYSAPPS/TestMob/PSAPPPORTALVIEWS/AppPortalView.json',
                    parameters: [
                        { pathName: 'appindexview', parameterName: 'appindexview' },
                        { pathName: 'views', parameterName: 'view' },
                    ],
                    requireAuth: false,
                },
                component: AppRouteShellComponent,
            },
            ...indexRoutes,
        ],
    },
        {
        path: '/app/:app?',
        beforeEnter: (to: any, from: any, next: any) => {
            const routerParamsName = 'appindexview';
            const params: any = {};
            if (to.params && to.params[routerParamsName]) {
                Object.assign(params, qs.parse(to.params[routerParamsName], { delimiter: ';' }));
            }
            const auth: Promise<any> = AuthGuard.getInstance().authGuard('/appdata', params, router);
            auth.then((result) => {
                next(result ? true : false);
            }).catch(() => {
                next(false);
            });
        },
        meta: {
            parameters: [
                { pathName: 'app', parameterName: 'app' },
            ],
        },
        component: AppViewContainerShellComponent,
        children: [
            {
                path: 'virtualentity02s/:virtualentity02?/views/:view?',
                meta: {
                    captionTag: '',
                    caption: '',
                    info:'',
                    imgPath: '',
                    iconCls: '',
                    parameters: [
                        { pathName: 'app', parameterName: 'app' },
                        { pathName: 'virtualentity02s', parameterName: 'virtualentity02' },
                        { pathName: 'views', parameterName: 'view' },
                    ],
                    resource:'virtualentity02',
                    requireAuth: false,
                },
                component: AppRouteShellComponent
            },
            {
                path: 'virtualentitymerges/:virtualentitymerge?/views/:view?',
                meta: {
                    captionTag: '',
                    caption: '',
                    info:'',
                    imgPath: '',
                    iconCls: '',
                    parameters: [
                        { pathName: 'app', parameterName: 'app' },
                        { pathName: 'virtualentitymerges', parameterName: 'virtualentitymerge' },
                        { pathName: 'views', parameterName: 'view' },
                    ],
                    resource:'virtualentitymerge',
                    requireAuth: false,
                },
                component: AppRouteShellComponent
            },
            {
                path: 'customs/:custom?/views/:view?',
                meta: {
                    captionTag: '',
                    caption: '',
                    info:'',
                    imgPath: '',
                    iconCls: '',
                    parameters: [
                        { pathName: 'app', parameterName: 'app' },
                        { pathName: 'customs', parameterName: 'custom' },
                        { pathName: 'views', parameterName: 'view' },
                    ],
                    resource:'custom',
                    requireAuth: false,
                },
                component: AppRouteShellComponent
            },
            {
                path: 'inheritedentities/:inheritedentity?/views/:view?',
                meta: {
                    captionTag: '',
                    caption: '',
                    info:'',
                    imgPath: '',
                    iconCls: '',
                    parameters: [
                        { pathName: 'app', parameterName: 'app' },
                        { pathName: 'inheritedentities', parameterName: 'inheritedentity' },
                        { pathName: 'views', parameterName: 'view' },
                    ],
                    resource:'inheritedentity',
                    requireAuth: false,
                },
                component: AppRouteShellComponent
            },
            {
                path: 'projects/:project?/bxds/:bxd?/views/:view?',
                meta: {
                    captionTag: '',
                    caption: '',
                    info:'',
                    imgPath: '',
                    iconCls: '',
                    parameters: [
                        { pathName: 'app', parameterName: 'app' },
                        { pathName: 'projects', parameterName: 'project' },
                        { pathName: 'bxds', parameterName: 'bxd' },
                        { pathName: 'views', parameterName: 'view' },
                    ],
                    resource:'bxd',
                    requireAuth: false,
                },
                component: AppRouteShellComponent,
            },
            {
                path: 'bxdlbs/:bxdlb?/bxds/:bxd?/views/:view?',
                meta: {
                    captionTag: '',
                    caption: '',
                    info:'',
                    imgPath: '',
                    iconCls: '',
                    parameters: [
                        { pathName: 'app', parameterName: 'app' },
                        { pathName: 'bxdlbs', parameterName: 'bxdlb' },
                        { pathName: 'bxds', parameterName: 'bxd' },
                        { pathName: 'views', parameterName: 'view' },
                    ],
                    resource:'bxd',
                    requireAuth: false,
                },
                component: AppRouteShellComponent,
            },
            {
                path: 'bxds/:bxd?/views/:view?',
                meta: {
                    captionTag: '',
                    caption: '',
                    info:'',
                    imgPath: '',
                    iconCls: '',
                    parameters: [
                        { pathName: 'app', parameterName: 'app' },
                        { pathName: 'bxds', parameterName: 'bxd' },
                        { pathName: 'views', parameterName: 'view' },
                    ],
                    resource:'bxd',
                    requireAuth: false,
                },
                component: AppRouteShellComponent
            },
            {
                path: 'virtualentity03s/:virtualentity03?/views/:view?',
                meta: {
                    captionTag: '',
                    caption: '',
                    info:'',
                    imgPath: '',
                    iconCls: '',
                    parameters: [
                        { pathName: 'app', parameterName: 'app' },
                        { pathName: 'virtualentity03s', parameterName: 'virtualentity03' },
                        { pathName: 'views', parameterName: 'view' },
                    ],
                    resource:'virtualentity03',
                    requireAuth: false,
                },
                component: AppRouteShellComponent
            },
            {
                path: 'inheritedchildentities/:inheritedchildentity?/views/:view?',
                meta: {
                    captionTag: '',
                    caption: '',
                    info:'',
                    imgPath: '',
                    iconCls: '',
                    parameters: [
                        { pathName: 'app', parameterName: 'app' },
                        { pathName: 'inheritedchildentities', parameterName: 'inheritedchildentity' },
                        { pathName: 'views', parameterName: 'view' },
                    ],
                    resource:'inheritedchildentity',
                    requireAuth: false,
                },
                component: AppRouteShellComponent
            },
            {
                path: 'projects/:project?/views/:view?',
                meta: {
                    captionTag: '',
                    caption: '',
                    info:'',
                    imgPath: '',
                    iconCls: '',
                    parameters: [
                        { pathName: 'app', parameterName: 'app' },
                        { pathName: 'projects', parameterName: 'project' },
                        { pathName: 'views', parameterName: 'view' },
                    ],
                    resource:'project',
                    requireAuth: false,
                },
                component: AppRouteShellComponent
            },
            {
                path: 'virtualentity01s/:virtualentity01?/views/:view?',
                meta: {
                    captionTag: '',
                    caption: '',
                    info:'',
                    imgPath: '',
                    iconCls: '',
                    parameters: [
                        { pathName: 'app', parameterName: 'app' },
                        { pathName: 'virtualentity01s', parameterName: 'virtualentity01' },
                        { pathName: 'views', parameterName: 'view' },
                    ],
                    resource:'virtualentity01',
                    requireAuth: false,
                },
                component: AppRouteShellComponent
            },
            {
                path: 'bxdlbs/:bxdlb?/views/:view?',
                meta: {
                    captionTag: '',
                    caption: '',
                    info:'',
                    imgPath: '',
                    iconCls: '',
                    parameters: [
                        { pathName: 'app', parameterName: 'app' },
                        { pathName: 'bxdlbs', parameterName: 'bxdlb' },
                        { pathName: 'views', parameterName: 'view' },
                    ],
                    resource:'bxdlb',
                    requireAuth: false,
                },
                component: AppRouteShellComponent
            },
            {
                path: 'logicaldeletionentities/:logicaldeletionentity?/views/:view?',
                meta: {
                    captionTag: '',
                    caption: '',
                    info:'',
                    imgPath: '',
                    iconCls: '',
                    parameters: [
                        { pathName: 'app', parameterName: 'app' },
                        { pathName: 'logicaldeletionentities', parameterName: 'logicaldeletionentity' },
                        { pathName: 'views', parameterName: 'view' },
                    ],
                    resource:'logicaldeletionentity',
                    requireAuth: false,
                },
                component: AppRouteShellComponent
            },
            {
                path: 'bxdmxes/:bxdmx?/views/:view?',
                meta: {
                    captionTag: '',
                    caption: '',
                    info:'',
                    imgPath: '',
                    iconCls: '',
                    parameters: [
                        { pathName: 'app', parameterName: 'app' },
                        { pathName: 'bxdmxes', parameterName: 'bxdmx' },
                        { pathName: 'views', parameterName: 'view' },
                    ],
                    resource:'bxdmx',
                    requireAuth: false,
                },
                component: AppRouteShellComponent
            },
            {
                path: 'dynadashboards/:dynadashboard?/views/:view?',
                meta: {
                    captionTag: '',
                    caption: '',
                    info:'',
                    imgPath: '',
                    iconCls: '',
                    parameters: [
                        { pathName: 'app', parameterName: 'app' },
                        { pathName: 'dynadashboards', parameterName: 'dynadashboard' },
                        { pathName: 'views', parameterName: 'view' },
                    ],
                    resource:'dynadashboard',
                    requireAuth: false,
                },
                component: AppRouteShellComponent
            },
            {
                path: 'views/:view?',
                meta: {
                    captionTag: '',
                    caption: '',
                    info:'',
                    imgPath: '',
                    iconCls: '',
                    parameters: [
                        { pathName: 'app', parameterName: 'app' },
                        { pathName: 'views', parameterName: 'view' },
                    ],
                    requireAuth: false,
                },
                component: AppRouteShellComponent,
            },
            ...indexRoutes,
        ],
    },
    ...globalRoutes,
    {
        path: '/',
        redirect: 'appindexview'
    },
    {
        path: '/login/:login?',
        name: 'login',
        meta: {  
            caption: '登录',
            viewType: 'login',
            requireAuth: false,
            ignoreAddPage: true,
        },
        component: AppLoginComponent,
    },
    {
        path: '/404',
        name: '404',
        component: App404Component
    },
    {
        path: '/500',
        name: '500',
        component: App500Component
    },
    {
        path: '/setting',
        name: 'Setting',
        component: AppSettingComponent
    }
];

const router = createRouter({
  history: createWebHashHistory(""),
  routes,
});

export default router;
