import { createApp } from 'vue';
import App from './App';
import router from './router';
import { IonicVue } from '@ionic/vue';
/* Core CSS required for Ionic components to work properly */
import '@ionic/vue/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/vue/css/normalize.css';
import '@ionic/vue/css/structure.css';
import '@ionic/vue/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/vue/css/padding.css';
import '@ionic/vue/css/float-elements.css';
import '@ionic/vue/css/text-alignment.css';
import '@ionic/vue/css/text-transformation.css';
import '@ionic/vue/css/flex-utils.css';
import '@ionic/vue/css/display.css';

//import "ibz-ui-ionic/lib/ibz-ui-ionic.css";
// 本地调试使用，打包后需删除（TODO）
import "ibz-ui-ionic/src/styles/index.scss";
import '@/styles/index.scss';

import { Register } from './register';
import Store from './core/store';
import { ComponentRegister, translate } from 'ibz-ui-ionic';
import { Interceptors } from './core/app/interceptor';
import i18n from './locale';

Interceptors.getInstance();

const app = createApp(App)
  .use(IonicVue)
  .use(router)
  .use(Register)
  .use(ComponentRegister)
  .use(Store)
  .use(i18n)

// 添加全局翻译api
app.config.globalProperties.$tl = function (key: string, value?: string){
  return translate(key,this,value);
};

// vue警告配置，开发环境下生效
app.config.warnHandler = (msg, vm, trace) => {
  if (!msg.startsWith("Extraneous non-emits") && !msg.startsWith("Extraneous non-props")) {
    console.warn(msg, trace);
  }
}

router.isReady().then(() => {
  app.mount('#app');
});
