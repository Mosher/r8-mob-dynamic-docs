import { ComponentBase, GenerateComponent } from 'ibz-ui-ionic';

/**
 * 应用
 *
 * @author chitanda
 * @date 2021-08-05 13:08:49
 * @export
 * @class App
 * @extends {ComponentBase}
 */
export class App extends ComponentBase {
  /**
   * @description 生命周期
   * @memberof App
   */
  setup() {
    const body = document.querySelector('body');
    if (body) {
      body.classList.remove('loading-container');
    }
    // 当屏幕缩小时会触发该事件
    window.onresize = () => {
      this.resize();
    };
    this.resize();
  }

  /**
   * @description windows重绘事件
   * @memberof App
   */
  public resize() {
    const designWidth = 750; // 移动端设计稿标准宽度
    const baseFontSize = 100; // 设计像素参考标准
    const width = window.innerWidth; // 屏幕宽度
    const currentFontSize = (width / designWidth) * baseFontSize;
    document.documentElement.style.fontSize = currentFontSize + 'px';
  }

  render() {
    return (
      <ion-app>
        <ion-router-outlet />
      </ion-app>
    );
  }
}

export default GenerateComponent(App);
