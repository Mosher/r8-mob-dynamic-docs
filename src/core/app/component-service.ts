import { AppComponentService } from 'ibz-ui-ionic';
import {
    //  视图插件
    TestViewPluginComponent,
    //  部件插件
    CustomPortletPluginComponent,
    //  编辑器插件
    TestEdiotrStyleComponent,
} from '../../plugins';
//  视图插件

/**
 * 应用组件服务
 * 
 * @memberof ComponentService
 */
export class ComponentService extends  AppComponentService {

    /**
     * @description 单例变量声明
     *
     * @private
     * @static
     * @type {ComponentService}
     * @memberof ComponentService
     */
    private static ComponentService: ComponentService;

    /**
     * 获取 ComponentService 单例对象
     *
     * @static
     * @returns {AppComponentService}
     * @memberof AppComponentService
     */
    static getInstance(): ComponentService {
        if (!ComponentService.ComponentService) {
            ComponentService.ComponentService = new ComponentService();
        }
        return this.ComponentService;
    }

    /**
     * @description 注册视图类型组件
     * @return {*} 
     * @memberof ComponentService
     */
    protected registerViewTypeComponents() {
        super.registerViewTypeComponents();
        // 注册视图插件
        this.viewTypeMap.set("DEMOBMAPVIEW_testViewPlugin", TestViewPluginComponent);
    }

    /**
     * @description 注册部件组件
     * @return {*} 
     * @memberof ComponentService
     */
    protected registerControlComponents() {
        super.registerControlComponents();
        // 注册部件插件标识
        this.controlMap.set("PORTLET_CustomPortletPlugin", CustomPortletPluginComponent);
    }

    /**
     * @description 注册编辑器组件
     * @protected
     * @memberof ComponentService
     */
    protected registerEditorComponents() {
        super.registerEditorComponents();
        // 注册编辑器插件
        this.editorMap.set("MOBTEXT_testEdiotrStyle", TestEdiotrStyleComponent);
    }

}