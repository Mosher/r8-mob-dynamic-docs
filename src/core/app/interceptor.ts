import { getCookie, setCookie, SyncSeriesHook } from 'qx-util';
import { getSessionStorage, Http, InterceptorsBase } from 'ibz-core';
import { Environment } from '@/environments/environment';
import router from '../../router'
import { App } from './app';


/**
 * 拦截器
 *
 * @export
 * @class Interceptors
 */
export class Interceptors extends InterceptorsBase {

    /**
     *  单列对象
     *
     * @public
     * @static
     * @type { Interceptors }
     * @memberof Interceptors
     */
    private static readonly instance: Interceptors = new Interceptors();

    /**
     * Creates an instance of Interceptors.
     * 私有构造，拒绝通过 new 创建对象
     * 
     * @memberof Interceptors
     */
    private constructor() {
        super();
        if (Interceptors.instance) {
            return Interceptors.instance;
        } else {
            this.intercept();
        }
    }

    /**
     * 获取 Interceptors 单例对象
     *
     * @static
     * @param {Router} route
     * @param {Store<any>} store
     * @returns {Interceptors}
     * @memberof Interceptors
     */
    public static getInstance(): Interceptors {
        return this.instance;
    }

    /**
     * 执行钩子(请求、响应)
     *
     * @memberof Interceptors
     */
    public static hooks = {
        request: new SyncSeriesHook<[], { config: any }>(),
        response: new SyncSeriesHook<[], { response: any }>()
    };

    /**
     * 拦截器实现接口
     *
     * @public
     * @memberof Interceptors
     */
    public intercept(): void {
        Http.getHttp().interceptors.request.use((config: any) => {
            Interceptors.hooks.request.callSync({ config: config });
            let appdata: any = App.getInstance().getContext();
            if (appdata && appdata.context) {
                config.headers['srforgsectorid'] = appdata.context.srforgsectorid;
            }
            if (Environment.SaaSMode) {
                let activeOrgData = getSessionStorage('activeOrgData');
                config.headers['srforgid'] = activeOrgData?.orgid;
                config.headers['srfsystemid'] = activeOrgData?.systemid;
            }
            if (getCookie('ibzuaa-token')) {
                config.headers['Authorization'] = `Bearer ${getCookie('ibzuaa-token')}`;
            } else {
                // 第三方应用打开免登
                if (sessionStorage.getItem("srftoken")) {
                    const token = sessionStorage.getItem('srftoken');
                    config.headers['Authorization'] = `Bearer ${token}`;
                }
            }
            // config.headers['Accept-Language'] = i18n.locale;
            if (!Object.is(Environment.BaseUrl, "") && !config.url.startsWith('https://') && !config.url.startsWith('http://') && !config.url.startsWith('./assets')) {
                config.url = Environment.BaseUrl + config.url;
            }
            return config;
        }, (error: any) => {
            return Promise.reject(error);
        });

        Http.getHttp().interceptors.response.use((response: any) => {
            Interceptors.hooks.response.callSync({ response: response });
            return response;
        }, (error: any) => {
            error = error ? error : { response: {} };
            let { response: res } = error;
            let { data: _data } = res;
            // 处理异常
            if (res.headers && res.headers['x-ibz-error']) {
                res.data.errorKey = res.headers['x-ibz-error'];
            }
            if (res.headers && res.headers['x-ibz-params']) {
                res.data.entityName = res.headers['x-ibz-params'];
            }
            if (res.status === 401) {
                this.doNoLogin(res, _data.data);
            }
            if (res.status === 403) {
                if (res.data && res.data.status && Object.is(res.data.status, "FORBIDDEN")) {
                    let alertMessage: string = "非常抱歉，您无权操作此数据，如需操作请联系管理员！";
                    Object.assign(res.data, { localizedMessage: alertMessage, message: alertMessage });
                }
            }
            return res;
        });
    }

    /**
     * 处理未登录异常情况
     *
     * @private
     * @param {*} [data={}]
     * @memberof Interceptors
     */
    private doNoLogin(response: any, data: any = {}): void {
        // 交由路由守卫自行处理
        if (response && response.config && response.config.url && Object.is(response.config.url, `${Environment.BaseUrl}appdata`)) {
            return;
        }
        App.getInstance().onClearAppData();
        if (Environment.loginUrl) {
            window.location.href = `${Environment.loginUrl}?redirect=${window.location.href}`;
        } else {
            if (Object.is(router.currentRoute.value.name, 'login')) {
                return;
            }
            router.push({ name: 'login', query: { redirect: router.currentRoute.value.fullPath } });
        }
    }

}