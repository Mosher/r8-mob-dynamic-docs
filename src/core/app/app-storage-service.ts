import { IAppStorageService, IParam } from "ibz-core";
import Store from "../store";


/**
 * 应用存储
 *
 * @export
 * @class AppStorageService
 */
export class AppStorageService implements IAppStorageService {

    /**
     * 获取 AppStorageService 单例对象
     *
     * @static
     * @returns {AppStorageService}
     * @memberof AppStorageService
     */
    static getInstance(): AppStorageService {
        if (!AppStorageService.AppStorageService) {
            AppStorageService.AppStorageService = new AppStorageService();
        }
        return this.AppStorageService;
    }

    /**
     * 单例变量声明
     *
     * @private
     * @static
     * @type {AppStorageService}
     * @memberof AppStorageService
     */
    private static AppStorageService: AppStorageService;

    /**
     * 获取应用存储指定数据
     *
     * @memberof AppStorageService
     */
    get(key: string): IParam {
        return Store.getters['getAppStoreData'](key);
    }

    /**
     * 设置应用存储指定数据
     *
     * @memberof AppStorageService
     */
    set(key: string, data: IParam): void {
        Store.commit('setAppStoreData', { key, data });
    }

    /**
     * 清空应用存储指定数据
     *
     * @memberof AppStorageService
     */
    delete(key: string): void {
        Store.commit('clearAppStoreData', key);
    }

    /**
     * 清空应用存储所有数据
     *
     * @memberof AppStorageService
     */
    clear(): void {
        throw new Error("Method not implemented.");
    }

}