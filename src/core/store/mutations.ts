import { IParam } from "ibz-core";

/**
 * 设置指定键值数据
 * 
 * @param state 
 */
export const setAppStoreData = (state: any, { key, data }: { key: string, data: IParam }) => {
    return state.app[key] = data;
}

/**
 * 清空指定键值数据
 * 
 * @param state 
 */
export const clearAppStoreData = (state: any, key: string) => {
    delete state.app[key];
}