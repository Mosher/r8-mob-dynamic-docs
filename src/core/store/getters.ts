/**
 * 获取指定键值数据
 * 
 * @param state 
 */
export const getAppStoreData = (state: any) => (key:string) => {
    return state.app[key];
}