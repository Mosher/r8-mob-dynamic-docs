import { createLogger, createStore } from "vuex";
import * as mutations from './mutations';
import * as getters from './getters';
// import * as actions from './actions';

const state = {
    app: {}
}

const Store = createStore({
    state,
    getters,
    // actions,
    mutations
})

export default Store;