
import { ComponentBase, GenerateComponent } from "ibz-ui-ionic";
import "./app-404.scss"

export class App404 extends ComponentBase {

  /**
   * 返回上一步
   */
  public back() {
    if (window.history.length <= 1) {
      this.router.push({ path: "/" });
      this.hiddenContent();
      return false;
    } else {
      this.router.go(-1);
      this.hiddenContent();
    }
  }
  /**
   * 返回首页
   */
  public backindex() {
    this.router.push({ path: "/" });
    this.hiddenContent();
  }

  /**
   * 隐藏内容
   */
  public hiddenContent() {
    let el: Element | null = document.querySelector(".app-404");
    if (el) { el.classList.add("app-404-hidden") }
  }

  /**
   * 绘制内容
   *
   * @memberof App500
   */
  render() {
    return <ion-content class="app-404" fullscreen scrollY={false}>
      <img src="assets/images/404.jpg" alt="404" />
      <div class="app-404-btnbox">
        <ion-button class="backbtn" onClick={() => this.back()}>{this.$tl('404.previous','上一步')}</ion-button>
        <ion-button class="backbtn" onClick={() => this.backindex()}>{this.$tl('404.homepage','首页')}</ion-button>
      </div>
    </ion-content>
  }
}
export const App404Component = GenerateComponent(App404);