import { defineComponent } from 'vue';
import { localList } from '../../locale/util';
import { AppThemeComponent } from '../app-theme/app-theme';
import { IParam } from 'ibz-core';
import { App } from '../../core';
import './app-setting.scss';
const AppSettingProps = {};

export const AppSettingComponent = defineComponent({
  name: 'AppSetting',
  props: AppSettingProps,
  components: {
    'app-theme': AppThemeComponent,
  },
  setup(props: any) {
    const application = App.getInstance();
    return {
      application,
    };
  },
  methods: {
    /**
     * @description 多语言切换
     * @param {*} item
     */
    changeLanguage(item: any) {
      App.getInstance().setActiveLanguage(item.type);
    },

    /**
     * @description 打开消息
     */
    openNotice() {
      //todo 待补充
      this.application.getNoticeService().info('你点击了消息');
    },

    /**
     * @description 渲染头部
     * @return {*}
     */
    renderHeader() {
      return (
        <ion-header class={'setting-header view-header'}>
          <ion-toolbar>
            <ion-title>{(this as any).$tl('setting.setting', '系统设置')}</ion-title>
          </ion-toolbar>
        </ion-header>
      );
    },

    /**
     * @description 渲染内容区
     * @return {*}
     */
    renderContent() {
      return (
        <ion-item-group>
          <ion-item-divider>
            <ion-label>{(this as any).$tl('setting.systeminforms', '系统通知')}</ion-label>
          </ion-item-divider>
          <ion-item>
            <ion-label
              v-badge={{ count: 5 }}
              onClick={() => {
                this.openNotice();
              }}
            >
              {(this as any).$tl('setting.notice', '通知')}
            </ion-label>
          </ion-item>
          <ion-item>
            <ion-label
              v-badge={{ count: 5 }}
              onClick={() => {
                this.openNotice();
              }}
            >
              {(this as any).$tl('setting.message', '消息')}
            </ion-label>
          </ion-item>
          <ion-item-divider>
            <ion-label>{(this as any).$tl('setting.language', '选择语言')}</ion-label>
          </ion-item-divider>
          <ion-radio-group value={App.getInstance().getActiveLanguage()}>
            {localList.map((item: IParam) => {
              return (
                <ion-item
                  onClick={() => {
                    this.changeLanguage(item);
                  }}
                >
                  <ion-label>{item.name}</ion-label>
                  <ion-radio slot='end' color='primary' value={item.type}></ion-radio>
                </ion-item>
              );
            })}
          </ion-radio-group>
          <ion-item-divider>
            <ion-label>{(this as any).$tl('setting.theme', '选择主题')}</ion-label>
          </ion-item-divider>
          <app-theme></app-theme>
        </ion-item-group>
      );
    },
  },

  render() {
    return (
      <ion-page class={{ 'ion-page': true, 'app-setting': true, 'app-view': true }}>
        {this.renderHeader()}
        <ion-content class={'setting-content'}>{this.renderContent()}</ion-content>
      </ion-page>
    );
  },
});
