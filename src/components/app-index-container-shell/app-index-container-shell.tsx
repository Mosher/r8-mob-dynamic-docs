import { GenerateComponent } from "ibz-ui-ionic";
import { AppRouteShell } from "../app-route-shell/app-route-shell";
export class AppIndexContainerShell extends AppRouteShell {

  /**
   * 计算视图动态路径
   *
   * @memberof AppIndexContainerShell
   */
  public async computeDynaModelFilePath() {
    if (this.route && this.route.matched && (this.route.matched.length > 0)) {
      const indexRoute = this.route.matched[0];
      if (indexRoute && indexRoute.meta && indexRoute.meta.dynaModelFilePath) {
        this.dynaModelFilePath = indexRoute.meta.dynaModelFilePath as string;
        await this.loadDynamicModelData();
      }
    }
  }
}
export const AppIndexContainerShellComponent = GenerateComponent(AppIndexContainerShell);