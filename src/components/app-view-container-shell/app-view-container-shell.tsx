import { ComponentBase, GenerateComponent } from "ibz-ui-ionic";
export class AppViewContainerShell extends ComponentBase {

  /**
   * 绘制内容
   *
   * @memberof AppViewContainerShell
   */
  render() {
    return <ion-page>
      <ion-router-outlet></ion-router-outlet>
    </ion-page>
  }
}
export const AppViewContainerShellComponent = GenerateComponent(AppViewContainerShell);