/**
 * @description 预置主题
 */
 export const PresetTheme = [
  {
    name: 'default',
    title: '默认',
    lanResTag: 'theme.default',
    color: '#f4f5f8',
  },
  {
    name: 'dark',
    title: '灰暗',
    lanResTag: 'theme.dark',
    color: '#343d46',
  },
];

/**
 * @description 主题配置项
 */
export const ThemeOptions = [
  {
    name: 'color',
    title: '基础色',
    lanResTag: 'theme.basecolor',
    items: [
      {
        name: 'Primary',
        title: '主要色',
        lanResTag: 'theme.maincolor',
        default: '',
        shade: '',
        tint: '',
      },
      {
        name: 'Secondary',
        title: '次要色',
        lanResTag: 'theme.secondcolor',
        default: '',
        shade: '',
        tint: '',
      },
      {
        name: 'Tertiary',
        title: '三次色',
        lanResTag: 'theme.thirdcolor',
        default: '',
        shade: '',
        tint: '',
      },
      {
        name: 'Success',
        title: '成功',
        lanResTag: 'theme.success',
        default: '',
        shade: '',
        tint: '',
      },
      {
        name: 'Warning',
        title: '警告',
        lanResTag: 'theme.warning',
        default: '',
        shade: '',
        tint: '',
      },
      {
        name: 'Danger',
        title: '危险',
        lanResTag: 'theme.danger',
        default: '',
        shade: '',
        tint: '',
      },
      {
        name: 'Dark',
        title: '暗色',
        lanResTag: 'theme.darkcolor',
        default: '',
        shade: '',
        tint: '',
      },
      {
        name: 'Medium',
        title: '中色',
        lanResTag: 'theme.medium',
        default: '',
        shade: '',
        tint: '',
      },
      {
        name: 'Light',
        title: '亮色',
        lanResTag: 'theme.light',
        default: '',
        shade: '',
        tint: '',
      },
    ],
  },
  {
    name: 'tab-bar',
    title: '分页栏',
    lanResTag: 'theme.tabbar',
    items: [
      {
        name: 'background',
        title: '背景色',
        lanResTag: 'theme.background',
        default: '',
      },
      {
        name: 'background-focused',
        title: '背景色（聚焦）',
        lanResTag: 'theme.focucolor',
        default: '',
      },
      {
        name: 'color',
        title: '颜色',
        lanResTag: 'theme.color',
        default: '',
      },
      {
        name: 'color-selected',
        title: '选中色',
        lanResTag: 'theme.selectcolor',
        default: '',
      },
    ],
  },
  {
    name: 'toolbar',
    title: '工具栏',
    lanResTag: 'theme.toolbar',
    items: [
      {
        name: 'background',
        title: '背景色',
        lanResTag: 'theme.background',
        default: '',
      },
      {
        name: 'color',
        title: '颜色',
        lanResTag: 'theme.color',
        default: '',
      },
    ],
  },
];
